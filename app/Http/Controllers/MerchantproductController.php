<?php
namespace App\Http\Controllers;
use DB;
use Session;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Products;
use App\Auction;
use App\Customer;
use App\Transactions;
use App\Merchantadminlogin;
use App\Merchantproducts;
use App\Merchantsettings;
use App\Brand;
use Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
class MerchantproductController extends Controller
{
    
    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */
public function __construct(){
        parent::__construct();
       
        // set admin Panel language
        $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleMerchant();
                    return $next($request);
    });
       
    }
    
    public function add_product()
    {
         $merchantid = Session::get('merchantid');
		 if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_PRODUCTS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_PRODUCTS');
			}
        $adminheader          = view('sitemerchant.includes.merchant_header')->with('routemenu', $session_message);
        $adminleftmenus       = view('sitemerchant.includes.merchant_left_menu_product');
        $adminfooter          = view('sitemerchant.includes.merchant_footer');
        $productcategory      = Products::get_product_category();
        $productcolor         = Products::get_product_color();
        $productsize          = Products::get_product_size();
        $productspecification = Products::get_product_specification();
        $mer_commission       = Products::mer_commission($merchantid);
        //$brand_details        = Brand::view_brand_commission();    //brand
         $spec_group = Products::get_spec_group();
         
        return view('sitemerchant.add_product')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('productcategory', $productcategory)->with('productcolor', $productcolor)->with('productsize', $productsize)->with('productspecification', $productspecification)
        //->with('brand_details',$brand_details)
         ->with('spec_group',$spec_group)
        ->with('mer_commission',$mer_commission); //brand
    }
    
    public function mer_edit_product($id)
    {
        
        if (Session::get('merchantid')) {
            $merchantid = Session::get('merchantid');
            
            $merid          = Session::get('merchantid');
			if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_PRODUCTS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_PRODUCTS');
			}
            $adminheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', $session_message);
            $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_product');
            $adminfooter    = view('sitemerchant.includes.merchant_footer');
            
            $category              = Products::get_product_category();
            $product_list          = Products::get_product($id);
            $productcolor          = Products::get_product_color();
            $merchantdetails       = Products::get_merchant_details();
            $productsize           = Products::get_product_size();
            $productspecification  = Products::get_product_specification();
            $existingspecification = Products::get_product_exist_specification($id);
            $existingcolor         = Products::get_product_exist_color($id);
            $existingsize          = Products::get_product_exist_size($id);
			//$mer_commission       = Products::mer_commission($merchantid);
            $product_return = Products::get_induvidual_product_detail_merchant($id, $merid);
			/* $spec_group = Products::get_spec_group(); */
			$main_cat		=	$product_list[0]->pro_mc_id;
			$sec_main_cat	=	$product_list[0]->pro_smc_id;
            $spec_group = Products::get_specification_group_product($main_cat,$sec_main_cat);
            $sp_group=array();
			if(count($spec_group)>0)
			{
				foreach($spec_group as $group )
				{
					array_push($sp_group,$group->spg_id);
				}
				$productspecification=DB::table('nm_specification')->whereIn('sp_spg_id', $sp_group)->get();
			}
			else{
				$productspecification = Products::get_product_specification();
			}
          //  $brand_details        = Brand::view_brand_commission();    //brand
           // print_r($product_return);exit();
            if(count($product_return)>0) {
                return view('sitemerchant.edit_product')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('productcolor', $productcolor)->with('category', $category)->with('product_list', $product_list)->with('merchantdetails', $merchantdetails)->with('productspecification', $productspecification)->with('productsize', $productsize)->with('existingspecification', $existingspecification)->with('existingcolor', $existingcolor)->with('existingsize', $existingsize)
                ->with('spec_group',$spec_group);
                // ->with('brand_details',$brand_details); //brand
                
            }
        } else {
            return Redirect::to('sitemerchant');
        }
    
    }
    
    public function manage_product()
    {
        $merchant_id = Session::get('merchantid');
        
        $from_date       = Input::get('from_date');
        $to_date         = Input::get('to_date');
        $allprod_reports = Merchantproducts::allprod_reports($from_date, $to_date, $merchant_id);
		if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_PRODUCTS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_PRODUCTS');
			}
        $adminheader     = view('sitemerchant.includes.merchant_header')->with('routemenu', $session_message);
        $adminleftmenus  = view('sitemerchant.includes.merchant_left_menu_product');
        $adminfooter     = view('sitemerchant.includes.merchant_footer');
        $details         = Merchantproducts::get_product_details($merchant_id);
        $delete_product  = Merchantproducts::get_order_details();
        return view('sitemerchant.manage_product')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('product_details', $details)->with('allprod_reports', $allprod_reports)->with('delete_product', $delete_product)->with('from_date',$from_date)->with('to_date',$to_date);
    }
    
    public function sold_product()
    {
        if (Session::has('merchantid')) {
            
            $merchant_id = Session::get('merchantid');
            
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_PRODUCTS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_PRODUCTS');
			}
            
            $adminheader          = view('sitemerchant.includes.merchant_header')->with('routemenu', $session_message);
            $adminleftmenus       = view('sitemerchant.includes.merchant_left_menu_product');
            $adminfooter          = view('sitemerchant.includes.merchant_footer');
            $details              = Merchantproducts::get_product_details($merchant_id);
            $merchant_soldreports = Merchantproducts::merchant_soldreports($from_date, $to_date, $merchant_id);
            
            return view('sitemerchant.sold_products')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('product_details', $details)->with('merchant_soldreports', $merchant_soldreports) ->with('from_date',$from_date)->with('to_date',$to_date);
        } else {
            
            return Redirect::to('sitemerchant');
            
        }
    }
		public function get_spec_related_to_cat(){
        
        if (Session::has('merchantid')) {
            if(Lang::has(Session::get('mer_lang_file').'.BACK_PRODUCTS')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.BACK_PRODUCTS');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.BACK_PRODUCTS');
			}
			
            /*$sec_main_cat = Input::get('sec_main_cat');
            $main_cat = Input::get('main_cat');
			$specification_group = Products::get_specification_group_product($main_cat,$sec_main_cat);
			if($specification_group!="")
			{
				echo'<option  value="0">-- Select --</option>';
				foreach($specification_group as $sp_group)
				{
					echo '<option value="'.$sp_group->spg_id.'">'.$sp_group->spg_name.'</option>';
				}
			}
			else{
				echo'<option  value="0">-- Select --</option>';
			}*/
			//print_r($specification_group);
            //->with('brand_details',$brand_details); //brand

            $sec_main_cat = Input::get('sec_main_cat');
            $main_cat = Input::get('main_cat');
            
            $specification_group = Products::get_specification_group_product($main_cat,$sec_main_cat);
            if(count($specification_group)>0)
            {
                echo'<option  value="0">-- Select --</option>';
                foreach($specification_group as $sp_group)
                {
                    echo '<option value="'.$sp_group->spg_id.'">'.$sp_group->spg_name.'</option>';
                }
            }
            else{
                echo "1";
                exit;
                //echo'<option  value="0">-- Select --</option>';
            }
            
        }
        
        else {
            
            return Redirect::to('sitemerchant');
            
        }
        
    }
    public function mer_add_product_submitnew()
    {
        
        $merid = Session::get('merchantid');
        
        
        if ($merid) {
            
            
            $meridd = Session::get('merchantid');
            
            $date1 = date('m/d/Y');
            $data  = Input::except(array(
                '_token'
            ));
            
            $count = Input::get('count');
           
            $selectedcolors = Input::get('co');
            
            $trimmedselectedcolors = trim($selectedcolors, ",");
            
            $colorarray = explode(",", $trimmedselectedcolors);
            
            $colorcount         = count($colorarray) - 1;
            $specificationcount = Input::get('specificationcount');
            
            $selectedsizes = Input::get('si');
           
            $trimmedsizes = trim($selectedsizes, ",");
            
            $sizearray = explode(",", $trimmedsizes);
            
            $productsizecount = Input::get('productsizecount');
          
            $filename_new_get = "";
            
            for ($i = 0; $i < $count; $i++) {
                
                $file_more = Input::file('file_more' . $i);
                
                $file_more_name = $file_more->getClientOriginalName();
                
                $move_more_img = explode('.', $file_more_name);
                
                $filename_new = $move_more_img[0] . str_random(8) . "." . strtolower($file_more->getClientOriginalExtension());
                
                $newdestinationPath = './public/assets/product/';
                
                $uploadSuccess_new = Input::file('file_more' . $i)->move($newdestinationPath, $filename_new);
                
                $filename_new_get .= $filename_new . "/**/";
                
            }
            $Product_SubCategory = "";
            $Product_SecondSubCategory = "";
           

            $now = date('Y-m-d H:i:s');
            
            $inputs = Input::all();
            
            $file = Input::file('file');
            
            $filename = $file->getClientOriginalName();
            
            $move_img = explode('.', $filename);
            
            $filename = $move_img[0] . str_random(8) . "." . strtolower($file->getClientOriginalExtension());
            
            $destinationPath = './public/assets/product/';
            
            $uploadSuccess = Input::file('file')->move($destinationPath, $filename);
            
            $file_name_insert = $filename . "/**/" . $filename_new_get;
            
            $Product_Title = Input::get('Product_Title');
            
            $Product_Category = Input::get('Product_Category');
            
            $Product_MainCategory = Input::get('Product_MainCategory');
            
           if(Input::get('Product_SubCategory') != ""){
                $Product_SubCategory = Input::get('Product_SubCategory');  
                }   
                if(Input::get('Product_SecondSubCategory') != ""){          
                $Product_SecondSubCategory = Input::get('Product_SecondSubCategory');  
                }   
            $Original_Price = Input::get('Original_Price');
            
            $Discounted_Price = Input::get('Discounted_Price');
/* calculate product discount percentage */
            $product_saving_price        = Input::get('Original_Price') - Input::get('Discounted_Price');
            
            $Product_discount_percentage = (($product_saving_price / Input::get('Original_Price')) * 100);
            $Product_discount_percentage=round($Product_discount_percentage);
/* calculate product discount percentage */
            
            $Shipping_Amount = Input::get('Shipping_Amount');
            
            if ($Shipping_Amount == "") {
                
                $Shipping_Amount = 0;
            
            }
            
            $Description = Input::get('Description');
            
            $pquantity = Input::get('Quantity_Product');
          
            
            $Delivery_Days = Input::get('Delivery_Days');
            
            $Delivery_Policy = Input::get('Delivery_Policy');
            
            $Meta_Keywords = Input::get('Meta_Keywords');
            
            $Meta_Description = Input::get('Meta_Description');
            
            $Select_Merchant = Input::get('Select_Merchant');
            
            $Select_Shop = Input::get('Select_Shop');
            
            $inc_tax = Input::get('inctax');
            
            $add_spec = Input::get('specification');
            
            $postfb = Input::get('postfb');
            
            $img_count = Input::get('count');
        
            for ($i = 0; $i <= $specificationcount; $i++) {
                
                if (Input::get('spec' . $i) == 0 || Input::get('spectext' . $i == "")) {
                    
                    $add_spec = 2;
            
                    
                }
                
            }
            
          
            $admin = '0';
            
            $entry = array(
                
                
                'pro_title' => $Product_Title,
                
                'pro_mc_id' => $Product_Category,
                
                'pro_smc_id' => $Product_MainCategory,
                
                'pro_sb_id' => $Product_SubCategory,
                
                'pro_ssb_id' => $Product_SecondSubCategory,
                
                
                'pro_price' => $Original_Price,
                
                'pro_disprice' => $Discounted_Price,

                'pro_discount_percentage' => $Product_discount_percentage,
                
                'pro_inctax' => $inc_tax,
                
                'pro_shippamt' => $Shipping_Amount,
                
                'pro_desc' => $Description,
                
                'pro_isspec' => $add_spec,
                
                'pro_delivery' => $Delivery_Days,
                
                'pro_mr_id' => $meridd,
                
                'pro_sh_id' => $Select_Shop,
                
                'pro_mkeywords' => $Meta_Keywords,
                
                'pro_mdesc' => $Meta_Description,
                
                'pro_Img' => $file_name_insert,
                
                'pro_image_count' => $img_count,
                
                'pro_qty' => $pquantity,
                
                'created_date' => $date1
                
                
                
            );
            
            $productid = Products::insert_product($entry);
            
            
            
            if ($productid) {
                
                if ($colorcount > 0) {
                    
                    for ($i = 0; $i < $colorcount; $i++) {
                        
                        $val = Input::get('colorcheckbox' . $colorarray[$i]);
                        
                        
                        
                        if ($val == 1) {
                            
                            $colorentry = array(
                                'pc_pro_id' => $productid,
                                
                                'pc_co_id' => $colorarray[$i]
                                
                            );
                            
                            Products::insert_product_color_details($colorentry);
                            
                        }
                        
                        else {
                   
                            
                        }
                        
                    }
                    
                }
                
             
                if ($add_spec == 1) {
               
                    for ($i = 0; $i <= $specificationcount; $i++) {
                        
                        
                        
                        if (Input::get('spec' . $i) == 0 || Input::get('spectext' . $i == "")) {
                            
                        }
                        
                        else {
                            
                            $specificationentry = array(
                                'spc_pro_id' => $productid,
                                 'spc_spg_id' => Input::get('spec_grp' . $i),
                                
                                'spc_sp_id' => Input::get('spec' . $i),
                                
                                'spc_value' => Input::get('spectext' . $i)
                                
                            );
                            
                            Products::insert_product_specification_details($specificationentry);
                    
                            
                        }
            
                        
                    }
            
                }
                
                if ($productsizecount > 0) {
              
                    for ($i = 0; $i < $productsizecount; $i++) {
                        
                        $val = Input::get('sizecheckbox' . $sizearray[$i]);
                        
                        if ($val == 1) {
                            
                            if (Input::get('quantity' . $sizearray[$i]) == "") {
                        
                                $productsizeentry = array(
                                    'ps_pro_id' => $productid,
                                    
                                    'ps_si_id' => $sizearray[$i],
                                    
                                    'ps_volume' => 0
                                    
                                );
                                
                            }
                            
                            else {
                                
                                $productsizeentry = array(
                                    'ps_pro_id' => $productid,
                                    
                                    'ps_si_id' => $sizearray[$i],
                                    
                                    'ps_volume' => Input::get('quantity' . $sizearray[$i])
                                    
                                );
                                
                            }
                            
                            Products::insert_product_size_details($productsizeentry);
                            
                        }
                        
                        else {
                            
                        }
                        
                    }
                    
                }
                
            }
        
            return Redirect::to('manage_product');
         
        }
     
    }
    
    public function mer_add_product_submit()
    {
		$get_active_lang=$this->get_active_language;
		//print_r($get_active_lang);die;
        if (Session::get('merchantid')) {
            
            $files = Input::file('file');
			$filesCount = count($files);
            $data = Input::except(array(
                '_token'
            ));
			for ($i=0; $i < $filesCount; $i++)
			{
				$file = $files[$i];
				$input = array(
				'product_image' => $files[$i]
			);
			$rules = array(
			'product_image' => 'required|image|mimes:jpeg,png,jpg,gif|image_size:'.$this->product_width.','.$this->product_height.''
				);
				$validator = Validator::make($input, $rules);
			}		
			if ($validator->fails())
			{
				return Redirect::to('mer_add_product')->withErrors($validator->messages())->withInput();
			}


            //Product Policy
            $allow_cancel   = Input::get('allow_cancel');
            $allow_return   = Input::get('allow_return');
            $allow_replace  = Input::get('allow_replace');

            $cancel_policy  = (Input::get('cancellation_policy')!=NUll)?Input::get('cancellation_policy'):'';
            $return_policy  = (Input::get('return_policy')!=NUll)?Input::get('return_policy'):'';
            $replace_policy = (Input::get('replacement_policy')!=NUll)?Input::get('replacement_policy'):'';

            $cancel_days    = (Input::get('cancellation_days')!=NUll)?Input::get('cancellation_days'):'';
            $return_days    = (Input::get('return_days')!=NUll)?Input::get('return_days'):'';
            $replace_days   = (Input::get('replace_days')!=NUll)?Input::get('replace_days'):'';

            $rules = $inputs = $policy_lang_entry = array();

            if($allow_cancel=='1')
            {
                $cancel_rules = array('cancellation_policy' => 'required','cancellation_days' => 'required|numeric');
                $rules = array_merge($rules,$cancel_rules);
                $inputs = array_merge($inputs,array('cancellation_policy' =>$cancel_policy,'cancellation_days'=>$cancel_days ));
            }if($allow_return=='1')
            {
                $return_rules = array('return_policy' => 'required','return_days' => 'required|numeric');
                $rules = array_merge($rules,$return_rules);
                $inputs = array_merge($inputs,array('return_policy' =>$return_policy,'return_days'=>$return_days ));
            }if($allow_replace=='1')
            {
                $replace_rules = array('replace_policy' => 'required','replace_days' => 'required|numeric');
                $rules = array_merge($rules,$replace_rules);
                $inputs = array_merge($inputs,array('replace_policy' =>$replace_policy,'replace_days'=>$replace_days ));
            }

            $get_active_lang =  $this->get_active_language; 
                $p_lang_entry = array();
                if(!empty($get_active_lang)) { 
                    foreach($get_active_lang as $get_lang) {
                        $get_lang_name = $get_lang->lang_name;
                        $get_lang_code = $get_lang->lang_code;
                
                        $policy_lang_entry = array(
                        'cancel_policy_'.$get_lang_code => (Input::get('cancellation_policy_'.$get_lang_name)!=NUll)?Input::get('cancellation_policy_'.$get_lang_name):'',
                        'return_policy_'.$get_lang_code => (Input::get('return_policy_'.$get_lang_name)!=NUll)?Input::get('return_policy_'.$get_lang_name):'',
                        'replace_policy_'.$get_lang_code => (Input::get('replacement_policy_'.$get_lang_name)!=NUll)?Input::get('replacement_policy_'.$get_lang_name):'',
                        
                        );

                        if($allow_cancel=='1')
                        {
                            $cancel_rules = array('cancellation_policy_'.$get_lang_name => 'required','cancellation_days' => 'required|numeric');
                            $rules = array_merge($rules,$cancel_rules);
                            $inputs = array_merge($inputs,array('cancellation_policy_'.$get_lang_name =>$policy_lang_entry['cancel_policy_'.$get_lang_code ]));
                        }if($allow_return=='1')
                        {
                            $return_rules = array('return_policy_'.$get_lang_name => 'required','return_days' => 'required|numeric');
                            $rules = array_merge($rules,$return_rules);
                            $inputs = array_merge($inputs,array('return_policy_'.$get_lang_name =>$policy_lang_entry['replace_policy_'.$get_lang_code ]));
                        }if($allow_replace=='1')
                        {
                            $replace_rules = array('replace_policy_'.$get_lang_name => 'required','replace_days' => 'required|numeric');
                            $rules = array_merge($rules,$replace_rules);
                            $inputs = array_merge($inputs,array('replace_policy_'.$get_lang_name =>$policy_lang_entry['replace_policy_'.$get_lang_code ]));
                        }
                        $p_lang_entry  = array_merge($p_lang_entry,$policy_lang_entry);
                    }
                }
            
           // print_r($inputs);exit();

            $validator = Validator::make($inputs, $rules);
            
            if ($validator->fails())
            {
                return Redirect::to('mer_add_product')->withErrors($validator->messages())->withInput();
            }

            /*colors*/
            $count = Input::get('count');
            $selectedcolors        = Input::get('co');
            $trimmedselectedcolors = trim($selectedcolors, ",");
            $colorarray            = explode(",", $trimmedselectedcolors);
            $colorcount            = count($colorarray) - 1;
            
            $date1 = date('Y-m-d');
           
            /*Product Size*/
            $selectedsizes      = Input::get('si');
            $trimmedsizes       = trim($selectedsizes, ",");
            $sizearray          = explode(",", $trimmedsizes);
            //$productsizecount   = Input::get('productsizecount');
            $productsizecount   = count(Input::get('Product_Size'));
            $pro_siz  = Input::get('pro_siz');
            $pro_color  = Input::get('productcolor');

            /*Product Image*/
             $field_values_array = Input::file('file');
            $count = count($field_values_array);

            $filename_new_get = "";
                $i=1;
                
                foreach($field_values_array as $value){
                    //your Image Upload into directory goes here
                    
                    $image_name = $value->getClientOriginalName();
                    
                    $move_more_img = explode('.', $image_name);
                
                    $filename_new = $move_more_img[0] . str_random(8) . "." . strtolower($value->getClientOriginalExtension());

               
                    $newdestinationPath = './public/assets/product/';
                
                    $uploadSuccess_new = $value->move($newdestinationPath, $filename_new);

                    $filename_new_get .= $filename_new . "/**/";
                }
                 $file_name_insert = $filename_new_get;
           /* $filename_new_get = "";
            for ($i = 0; $i < $count; $i++) {
                $file_more          = Input::file('file_more' . $i);
                $file_more_name     = $file_more->getClientOriginalName();
                $move_more_img      = explode('.', $file_more_name);
                $filename_new       = $move_more_img[0] . str_random(8) . "." . $move_more_img[1];
                $newdestinationPath = './public/assets/product/';
                $uploadSuccess_new  = Input::file('file_more' . $i)->move($newdestinationPath, $filename_new);*/
               // $filename_new_get .= $filename_new . "/**/";
           // }
            $now              = date('Y-m-d H:i:s');
            $inputs           = Input::all();
            /*$file             = Input::file('file');
            $filename         = $file->getClientOriginalName();
            $move_img         = explode('.', $filename);
            $filename         = $move_img[0] . str_random(8) . "." . $move_img[1];
            $destinationPath  = './public/assets/product/';
            $uploadSuccess    = Input::file('file')->move($destinationPath, $filename);*/
            //$file_name_insert = $filename . "/**/" . $filename_new_get;
            

            $Product_Title             = Input::get('Product_Title');
            $Product_Category          = Input::get('Product_Category');
            $Product_MainCategory      = Input::get('Product_MainCategory');
           $Product_SubCategory = "";
        $Product_SecondSubCategory = "";
               
          if(Input::get('Product_SubCategory') != ""){
                    $Product_SubCategory = Input::get('Product_SubCategory');  
                    }   
                    if(Input::get('Product_SecondSubCategory') != ""){          
                    $Product_SecondSubCategory = Input::get('Product_SecondSubCategory');  
                    } 
            $Original_Price            = Input::get('Original_Price');
            $Discounted_Price          = Input::get('Discounted_Price');
            $cash_price                = Input::get('cash_price');

/* calculate product discount percentage */
            $product_saving_price        = Input::get('Original_Price') - Input::get('Discounted_Price');
            
            $Product_discount_percentage = ($product_saving_price / Input::get('Original_Price')* 100);
            $Product_discount_percentage=floor($Product_discount_percentage);
            
/* calculate product discount percentage */
            
            /*Shipping*/
            $Shipping_Amount           = Input::get('Shipping_Amount');
            if ($Shipping_Amount == "") {
                $Shipping_Amount = 0;
            }
           
            $Description      = Input::get('Description');
            $pquantity        = Input::get('Quantity_Product');
          
            $Delivery_Days    = Input::get('Delivery_Days');
            $Delivery_Policy  = Input::get('Delivery_Policy');
            $Meta_Keywords    = Input::get('Meta_Keywords');
			$Meta_Keywords_ar = Input::get('Meta_Keywords_ar');
            $Meta_Description = Input::get('Meta_Description');
            $Select_Merchant  = Input::get('Select_Merchant');
            $Select_Shop      = Input::get('Select_Shop');
            $inc_tax          = Input::get('inctax');
            $add_spec         = Input::get('specification');
            $postfb           = Input::get('postfb');
            $img_count        = Input::get('count');
             
             /*specification*/
            $specificationcount = Input::get('specificationcount');

            for ($i = 0; $i <= $specificationcount; $i++) {
                if (Input::get('spec' . $i) == 0 || Input::get('spectext' . $i == "") || Input::get('fr_spectext' . $i == "")) {
                    $add_spec = 2;
                }
            }

            /*Product Table*/
                $entry     = array(
                'pro_title'  => $Product_Title,
                'pro_mc_id'  => $Product_Category,
                'pro_smc_id' => $Product_MainCategory,
                'pro_sb_id'  => $Product_SubCategory,
                'pro_ssb_id' => $Product_SecondSubCategory,
                //'product_brand_id' => Input::get('product_brand'), //brand
                'pro_price'    => $Original_Price,
                'pro_disprice' => $Discounted_Price,
                'pro_discount_percentage' => $Product_discount_percentage,
                'pro_inctax'   => $inc_tax,
                'pro_shippamt' => $Shipping_Amount,
                'pro_desc'     => $Description,
                'pro_isspec'   => $add_spec,
                'pro_is_size'  => $pro_siz,
                'pro_is_color' => $pro_color,
                'pro_delivery' => $Delivery_Days,
                'pro_mr_id'    => $Select_Merchant,
                'pro_sh_id'    => $Select_Shop,
                'pro_mkeywords'=> $Meta_Keywords,
                'pro_mdesc'    => $Meta_Description,
                'pro_Img'      => $file_name_insert,
                'pro_image_count' => $img_count,
                'pro_qty'      => $pquantity,
                'created_date' => $date1,
                'cash_pack'    => Input::get('cash_price'),
                //product policy starts
                    'allow_cancel'      => $allow_cancel,
                    'allow_return'      => $allow_return,
                    'allow_replace'     => $allow_replace,
                    'cancel_policy'     => $cancel_policy,
                    'return_policy'     => $return_policy,
                    'replace_policy'    => $replace_policy,
                    'cancel_days'       => $cancel_days,
                    'return_days'       => $return_days,
                    'replace_days'      => $replace_days,
                    
                    //product policy ends
                );
                $entry = array_merge($entry,$p_lang_entry); //Policy merge

				$lang_entry = array();
				if(!empty($get_active_lang))
				{
					foreach($get_active_lang as $get_lang)
					{
						$get_lang_code = $get_lang->lang_code;

						$lang_entry = array(
						'pro_title_'.$get_lang_code.'' => Input::get('Product_Title_'.$get_lang_code),
						'pro_desc_'.$get_lang_code.'' => Input::get('Description_'.$get_lang_code),
						'pro_mkeywords_'.$get_lang_code.'' => Input::get('Meta_Keywords_'.$get_lang_code),
						'pro_mdesc_'.$get_lang_code.'' => Input::get('Meta_Description_'.$get_lang_code)
						); 
						$entry  = array_merge($entry,$lang_entry);
					}
				}
            $productid = Products::insert_product($entry);
            
            /*Product Color*/
            if ($productid) {
                if ($colorcount > 0) {
                    for ($i = 0; $i < $colorcount; $i++) {
                        $val = Input::get('colorcheckbox' . $colorarray[$i]);
                        if ($val == 1) {
                            $colorentry = array(
                                'pc_pro_id' => $productid,
                                'pc_co_id' => $colorarray[$i]
                            );
                            Products::insert_product_color_details($colorentry);
                        } else {
                            
                        }
                    }
                }

                /*Product Specification*/
                if($add_spec == 1) {
                    for($i = 0; $i <= $specificationcount; $i++) {
                        if (Input::get('spec' . $i) != 0 || Input::get('spectext' . $i != "") || Input::get('fr_spectext' . $i == "")) {
                            $specificationentry = array(
                                'spc_pro_id' => $productid,
                                'spc_spg_id' => Input::get('spec_grp' . $i),
                                'spc_sp_id' => Input::get('spec' . $i),
                                'spc_value' => Input::get('spectext' . $i),
								
                            );
							$lang_entry_specification=array();
							if(!empty($get_active_lang))
							{
								foreach($get_active_lang as $get_lang)
								{
									$get_lang_code = $get_lang->lang_code;

									$lang_entry_specification = array(
									'spc_value_'.$get_lang_code.'' => Input::get($get_lang_code.'_spectext'.$i)
									); 
									$specificationentry  = array_merge($specificationentry,$lang_entry_specification);
								}
							}
                            Products::insert_product_specification_details($specificationentry);
                        }
                    }
                }

                /*Product Size*/
                $size = Input::get('Product_Size');
                $product_size_count = count($size);

               if(($productsizecount > 0) &&($pro_siz==0)){
                foreach($size as $product_size){
                    $productsizeentry = array(
                                    'ps_pro_id' => $productid,
                                    'ps_si_id'  => $product_size,
                                    'ps_volume' => 1
                   );
                   Products::insert_product_size_details($productsizeentry);
                }
               }else if($pro_siz==1){  //else if they select no sizes
                    $check_no_size = Products::check_no_size();
                    if($check_no_size==0){  //there is no size
                   
                        $array       = array('si_name'=>'no size');
                        $insert      = Products::insert_no_size($array);
                        $pro_size_id = $insert;
                    }else{                  // if already having no size
                    
                        $get_size_id = Products::get_size_id();
                        $pro_size_id = $get_size_id[0]->si_id;
                    }
                     $productsizeentry = array(
                                    'ps_pro_id' => $productid,
                                    
                                    'ps_si_id' => $pro_size_id,
                                    
                                    'ps_volume' => 1
                                    
                                );
                    Products::insert_product_size_details($productsizeentry);
                }
            }
			 if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_ADDED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_PRODUCT_ADDED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_PRODUCT_ADDED_SUCCESSFULLY');
			}
            return Redirect::to('mer_manage_product')->with('block_message', $session_message);
        } else {
            return Redirect::to('sitemerchant');
        }
    }

    public function mer_edit_product_submit(Request $request)
    {
        $get_active_lang=$this->get_active_language;
        $now    = date('Y-m-d H:i:s');
		$id    = Input::get('product_edit_id');
		$files = Input::file('new_file');
			$files_exit = Input::file('file');
			$filesExitCount = count($files_exit);
			$filesCount = count($files);
            $data = Input::except(array(
                '_token'
            ));
			if($files !=0)
			{
			for ($i=0; $i < $filesCount; $i++)
			{
				$file = $files[$i];
				$input = array(
				'product_image' => $files[$i]
			);
			$rules = array(
			'product_image' => 'required|image|mimes:jpeg,png,jpg,gif|image_size:'.$this->product_width.','.$this->product_height.''
				);
				$validator = Validator::make($input, $rules);
			}
					
			if ($validator->fails())
			{
				return Redirect::to('mer_edit_product/'.$id)->withErrors($validator->messages())->withInput();
			}
			}
				
			for ($j=0; $j < $filesExitCount; $j++)
			{
				
				if($files_exit[$j] !='')
				{
				$file = $files_exit[$j];
				$input = array(
				'upload_exit' => $files_exit[$j]
				);
				$rules = array(
					'upload_exit' => 'required|image|mimes:jpeg,png,jpg|image_size:'.$this->product_width.','.$this->product_height.''
				);
				$validator = Validator::make($input, $rules);
				
				if ($validator->fails())
				{
					return Redirect::to('mer_edit_product/'.$id)->withErrors($validator->messages())->withInput();
				}
				}
			}
            //Product Policy
            $allow_cancel   = Input::get('allow_cancel');
            $allow_return   = Input::get('allow_return');
            $allow_replace  = Input::get('allow_replace');

            $cancel_policy  = (Input::get('cancellation_policy')!=NUll)?Input::get('cancellation_policy'):'';
            $return_policy  = (Input::get('return_policy')!=NUll)?Input::get('return_policy'):'';
            $replace_policy = (Input::get('replacement_policy')!=NUll)?Input::get('replacement_policy'):'';

            $cancel_days    = (Input::get('cancellation_days')!=NUll)?Input::get('cancellation_days'):'';
            $return_days    = (Input::get('return_days')!=NUll)?Input::get('return_days'):'';
            $replace_days   = (Input::get('replace_days')!=NUll)?Input::get('replace_days'):'';

            $rules = $inputs = $policy_lang_entry = array();

            if($allow_cancel=='1')
            {
                $cancel_rules = array('cancellation_policy' => 'required','cancellation_days' => 'required|numeric');
                $rules = array_merge($rules,$cancel_rules);
                $inputs = array_merge($inputs,array('cancellation_policy' =>$cancel_policy,'cancellation_days'=>$cancel_days ));
            }else{
                $cancel_policy ='';
                $cancel_days ='';

            }
            if($allow_return=='1')
            {
                $return_rules = array('return_policy' => 'required','return_days' => 'required|numeric');
                $rules = array_merge($rules,$return_rules);
                $inputs = array_merge($inputs,array('return_policy' =>$return_policy,'return_days'=>$return_days ));
            }else{
                $return_policy ='';
                $return_days ='';
                
            }
            if($allow_replace=='1')
            {
                $replace_rules = array('replace_policy' => 'required','replace_days' => 'required|numeric');
                $rules = array_merge($rules,$replace_rules);
                $inputs = array_merge($inputs,array('replace_policy' =>$replace_policy,'replace_days'=>$replace_days ));
            }else{
                $replace_policy ='';
                $replace_days ='';
                
            }

            $get_active_lang =  $this->get_active_language; 
                $p_lang_entry = array();
                if(!empty($get_active_lang)) { 
                    foreach($get_active_lang as $get_lang) {
                        $get_lang_name = $get_lang->lang_name;
                        $get_lang_code = $get_lang->lang_code;
                
                        $policy_lang_entry = array(
                        'cancel_policy_'.$get_lang_code => ($allow_cancel=='1'?((Input::get('cancellation_policy_'.$get_lang_name)!=NUll)?Input::get('cancellation_policy_'.$get_lang_name):''):''),
                        'return_policy_'.$get_lang_code => ($allow_return=='1'?((Input::get('return_policy_'.$get_lang_name)!=NUll)?Input::get('return_policy_'.$get_lang_name):''):''),
                        'replace_policy_'.$get_lang_code => ($allow_replace=='1'?((Input::get('replacement_policy_'.$get_lang_name)!=NUll)?Input::get('replacement_policy_'.$get_lang_name):''):''),
                        
                        );

                        if($allow_cancel=='1')
                        {
                            $cancel_rules = array('cancellation_policy_'.$get_lang_name => 'required','cancellation_days' => 'required|numeric');
                            $rules = array_merge($rules,$cancel_rules);
                            $inputs = array_merge($inputs,array('cancellation_policy_'.$get_lang_name =>$policy_lang_entry['cancel_policy_'.$get_lang_code ]));
                        }
                        if($allow_return=='1')
                        {
                            $return_rules = array('return_policy_'.$get_lang_name => 'required','return_days' => 'required|numeric');
                            $rules = array_merge($rules,$return_rules);
                            $inputs = array_merge($inputs,array('return_policy_'.$get_lang_name =>$policy_lang_entry['return_policy_'.$get_lang_code ]));
                        }if($allow_replace=='1')
                        {
                            $replace_rules = array('replace_policy_'.$get_lang_name => 'required','replace_days' => 'required|numeric');
                            $rules = array_merge($rules,$replace_rules);
                            $inputs = array_merge($inputs,array('replace_policy_'.$get_lang_name =>$policy_lang_entry['replace_policy_'.$get_lang_code ]));
                        }
                        $p_lang_entry  = array_merge($p_lang_entry,$policy_lang_entry);
                    }
                }
                
           // print_r($rules);exit();

            $validator = Validator::make($inputs, $rules);
            
            if ($validator->fails())
            {
                return Redirect::to('edit_product/'.$id)->withErrors($validator->messages())->withInput();
            }

        $inputs = Input::all();
        
        
       
        $productid             = Input::get('product_edit_id');
        $selectedcolors        = Input::get('co');
        $trimmedselectedcolors = trim($selectedcolors, ",");
        $colorarray            = explode(",", $trimmedselectedcolors);
        $colorcount            = count($colorarray);
        
        
        $specificationcount = Input::get('specificationcount');
        $returncolor = Products::delete_product_color($id);
        $returnsize  = Products::delete_product_size($id);
        $returnspec  = Products::delete_product_spec($id);
        
        $selectedsizes = Input::get('si');
        
        $trimmedsizes = trim($selectedsizes, ",");
        $sizearray    = explode(",", $trimmedsizes);
        
        $productsizecount = Input::get('productsizecount');
        
        /*product image update*/
            $filename_new_get = "";
                $j=1;

                $file_get      = $request->input('file_get');            // (pro_img => old_image ie.before edit) getting as hidden input
                $file_get_path =  explode("/**/",$file_get,-1);          // explode that array of file names
                $field_values_array = $request->file('file');            // getting replace image files
                $k=0; 
                if(count($field_values_array) > 0)
                {    
                foreach($field_values_array as $value){                   //your Image Upload into directory goes here
                    
                    $a = $j++;                                          // $j is for calculation position of images
                    if(isset($value)){                                   // if there is a replace file, get into the loop
                            
                             $image_name   = $value->getClientOriginalName();      // renameing the image

                             $move_more_img = explode('.', $image_name);
							 $time=time();
                             $filename_new = 'Product_'.$time.'.' . strtolower($value->getClientOriginalExtension());
                             $newdestinationPath = './public/assets/product/';
							 Image::make($value)->save('./public/assets/product/'.$filename_new,$this->image_compress_quality);        // moving the image to directory
                             $image_old = Input::get('image_old');
							
							 if(file_exists($newdestinationPath.$image_old[$k]))
							 {
								@unlink($newdestinationPath.$image_old[$k]);
							 }
                             $filename_new_get.= $filename_new . "/**/";                                    // concatenating the new replace files 
                            
                    }   //isset value
                    else {                                                  // else there is no replace image is set it comes here
              
                        $filename_new_get.= $file_get_path[$a-1]. "/**/";   // now here we are concatenating the old image name
                    
                    } //else 
					$k++;		
                } //foreach    
                   }
                   else
                   {
                    $filename_new_get = $file_get;
                   } 
                /*if isset new product images it goes here */

                $new_field_values_array = Input::file('new_file');


                 $new_file_count = count($new_field_values_array);

                if($new_file_count > 0){
                    
                  foreach($new_field_values_array as $new_value){
                    //your New Product Image Upload into directory goes here
                    
                     $product_new_name   = $new_value->getClientOriginalName();
                    
                    $new_more_img       = explode('.', $product_new_name);
                
                    $time=rand(); //time();
                     $filename_new = 'Product_'.$time.'.' . strtolower($new_value->getClientOriginalExtension());
                    Image::make($new_value)->save('./public/assets/product/'.$filename_new,$this->image_compress_quality);
                    
                     $filename_new_get .= $filename_new . "/**/"; 
                   
                    

                  }//foreach

                } 
       $file_name_insert = $filename_new_get;  
       
        $id               = Input::get('product_edit_id');
        
        $Product_Title             = Input::get('Product_Title');
        $Product_Category          = Input::get('category');
        $Product_MainCategory      = Input::get('maincategory');
        $Product_SubCategory = "";
        $Product_SecondSubCategory = "";

               
          if(Input::get('Product_SubCategory') != ""){
                    $Product_SubCategory = Input::get('Product_SubCategory');  
                    }   
                    if(Input::get('Product_SecondSubCategory') != ""){          
                    $Product_SecondSubCategory = Input::get('Product_SecondSubCategory');  
                    } 

        $Original_Price            = Input::get('Original_Price');
        $Discounted_Price          = Input::get('Discounted_Price');
/* calculate product discount percentage */
            $product_saving_price        = Input::get('Original_Price') - Input::get('Discounted_Price');
            
            $Product_discount_percentage = ($product_saving_price / Input::get('Original_Price') * 100);
             $Product_discount_percentage=floor($Product_discount_percentage);
/* calculate product discount percentage */
        $Shipping_Amount           = Input::get('Shipping_Amount');
        if ($Shipping_Amount == "") {
            $Shipping_Amount = 0;
            
        }
        $Description      = Input::get('Description');
        $Delivery_Days    = Input::get('Delivery_Days');
        $Delivery_Policy  = Input::get('Delivery_Policy');
        $Meta_Keywords    = Input::get('Meta_Keywords');
        $Meta_Description = Input::get('Meta_Description');
        $Select_Merchant  = Input::get('Select_Merchant');
        $Select_Shop      = Input::get('Select_Shop');
        $inc_tax          = Input::get('inctax');
        $add_spec         = Input::get('specification');
        $postfb           = Input::get('postfb');
        $img_count        = Input::get('count');
        $pquantity        = Input::get('Quantity_Product');
        $sold_out         = 1;     
       
        $entry  = array(
            'pro_title'     => $Product_Title,
            'pro_mc_id'     => $Product_Category,
            'pro_smc_id'    => $Product_MainCategory,
            'pro_sb_id'     => $Product_SubCategory,
            'pro_ssb_id'    => $Product_SecondSubCategory,
            //'product_brand_id' => Input::get('product_brand'), //brand
            'pro_price'     => $Original_Price,
            'pro_disprice'  => $Discounted_Price,
            'pro_discount_percentage' => $Product_discount_percentage,
            'pro_inctax'    => $inc_tax,
            'pro_shippamt'  => $Shipping_Amount,
            'pro_desc'      => $Description,
            'pro_isspec'    => $add_spec,
            'pro_is_size'   => Input::get('pro_siz'),
            'pro_is_color'  => Input::get('productcolor'),
            'pro_delivery'  => $Delivery_Days,
            'pro_mr_id'     => $Select_Merchant,
            'pro_sh_id'     => $Select_Shop,
            'pro_mkeywords' => $Meta_Keywords,
            'pro_mdesc'     => $Meta_Description,
            'pro_Img'       => $file_name_insert,
            'pro_image_count' => $img_count,
            'pro_qty'      => $pquantity,
            'sold_status'  => $sold_out,
            'cash_pack'    => Input::get('cash_price'),
            //product policy starts
            'allow_cancel'      => $allow_cancel,
            'allow_return'      => $allow_return,
            'allow_replace'     => $allow_replace,
            'cancel_policy'     => $cancel_policy,
            'return_policy'     => $return_policy,
            'replace_policy'    => $replace_policy,
            'cancel_days'       => $cancel_days,
            'return_days'       => $return_days,
            'replace_days'      => $replace_days,
            
            //product policy ends
        );

        $entry = array_merge($entry,$p_lang_entry); //Policy merge
		$lang_entry = array();
		if(!empty($get_active_lang))
		{
			foreach($get_active_lang as $get_lang)
			{
				$get_lang_code = $get_lang->lang_code;

				$lang_entry = array(
				'pro_title_'.$get_lang_code.'' => Input::get('Product_Title_'.$get_lang_code),
				'pro_desc_'.$get_lang_code.'' => Input::get('Description_'.$get_lang_code),
				'pro_mkeywords_'.$get_lang_code.'' => Input::get('Meta_Keywords_'.$get_lang_code),
				'pro_mdesc_'.$get_lang_code.'' => Input::get('Meta_Description_'.$get_lang_code)
				); 
				$entry  = array_merge($entry,$lang_entry);
			}
		}
       
        
        $return = Products::edit_product($entry, $id);
        /*product color*/
            $old_color  = Input::get('is_pro_color');  // value of pro_is_size before editing
            $edit_color = Input::get('productcolor'); // value of pro_is_color after editing

            if(($old_color==0)&&($edit_color==1)){      // if before editing there is a size && now they select is no size
                $destory_color = Products::destory_color($productid);
            }

        if ($colorcount > 0) {
            for ($i = 0; $i < $colorcount; $i++) {
                $val = Input::get('colorcheckbox' . $colorarray[$i]);
                
                if ($val == 1) {
                    $colorentry = array(
                        'pc_pro_id' => $productid,
                        'pc_co_id' => $colorarray[$i]
                    );
                    Products::insert_product_color_details($colorentry);
                    
                } else {
                    
                }
            }
        }
        for ($i = 0; $i <= $specificationcount; $i++) {
            
            if (Input::get('spec' . $i) == 0 || Input::get('spectext' . $i == "")) {
                
                
                
            } else {
                $specificationentry = array(
                    'spc_pro_id' => $productid,
                    'spc_spg_id' => Input::get('spec_grp' . $i),
                    'spc_sp_id' => Input::get('spec' . $i),
                    'spc_value' => Input::get('spectext' . $i),
                );
				$lang_entry_specification=array();
				if(!empty($get_active_lang))
				{
					foreach($get_active_lang as $get_lang)
					{
						$get_lang_code = $get_lang->lang_code;

						$lang_entry_specification = array(
						'spc_value_'.$get_lang_code.'' => Input::get($get_lang_code.'_spectext'.$i)
						); 
						$specificationentry  = array_merge($specificationentry,$lang_entry_specification);
					}
				}
                Products::insert_product_specification_details($specificationentry);
                
            }
            
        }
        $old = Input::get('is_pro_siz');  // value of pro_is_size before editing
            $edit = Input::get('pro_siz');  // value of pro_is_size after editing
            if(($old==0)&&($edit==1)){      // if before editing there is a size && now they select is no size
                $destory_size = Products::destory_size($productid);

            }
        if(($productsizecount > 0)&&($edit==0)) {
            for ($i = 0; $i < $productsizecount; $i++) {
                $val = Input::get('sizecheckbox' . $sizearray[$i]);
                if ($val == 1) {
                    $productsizeentry = array(
                        'ps_pro_id' => $productid,
                        'ps_si_id' => $sizearray[$i],
                        'ps_volume' => Input::get('quantity' . $sizearray[$i])
                    );
                    Products::insert_product_size_details($productsizeentry);
                } else {
                    
                }
            }//for
        }else{
                    $check_no_size = Products::check_no_size();
                    
                    if($check_no_size==0){  //there is no size
                   
                        $array       = array('si_name'=>'no size');
                        $insert      = Products::insert_no_size($array);
                        $pro_size_id = $insert;
                    }else{                  // if already having no size
                    
                        $get_size_id = Products::get_size_id();
                        $pro_size_id = $get_size_id[0]->si_id;
                    }
                     $productsizeentry = array(
                                    'ps_pro_id' => $productid,
                                    
                                    'ps_si_id' => $pro_size_id,
                                    
                                    'ps_volume' => 1
                                    
                                );
                    Products::insert_product_size_details($productsizeentry);
            }
         if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_UPDATED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_PRODUCT_UPDATED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_PRODUCT_UPDATED_SUCCESSFULLY');
			}
        return Redirect::to('mer_manage_product')->with('block_message', $session_message);
    }

    public function mer_delete_product_img($pro_id,$image){

        $return = Merchantproducts::mer_delete_product_img($pro_id,$image);

		if(Lang::has(Session::get('mer_lang_file').'.MER_IMAGE_REMOVED_SUCCESSFULLY')!= ''){ 
			$session_message = trans(Session::get('mer_lang_file').'.MER_IMAGE_REMOVED_SUCCESSFULLY');
		}else{ 
			$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_IMAGE_REMOVED_SUCCESSFULLY');
		}	
       
        return Redirect::back()->with('message',$session_message);

    }

    public function product_getmaincategory()
    {
        $categoryid = $_GET['id'];
        
        $main_category = Merchantproducts::load_maincategory_ajax($categoryid);
    
        if ($main_category) {
            $maincategoryresult = "";
			if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_MAIN_CATEGORY')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_SELECT_MAIN_CATEGORY');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SELECT_MAIN_CATEGORY');
			}
            $maincategoryresult .= "<option value='0'> -$session_message- </option>";
            foreach ($main_category as $main_category_ajax) {
            $maincategoryresult .= "<option value='" . $main_category_ajax->smc_id . "'> " . $main_category_ajax->smc_name . " </option>";
            }
            echo $maincategoryresult;
        } else {
			 if (Lang::has(Session::get('mer_lang_file').'.MER_NO_LIST_AVAILABLE_IN_THE_CATEGORY')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_NO_LIST_AVAILABLE_IN_THE_CATEGORY');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_NO_LIST_AVAILABLE_IN_THE_CATEGORY');
			}
            echo $maincategoryresult = "<option value='0'>$session_message </option>";
        }
        
    }

    public function product_getsubcategory()
    {
        $categoryid = $_GET['id'];
        
        $sub_category = Merchantproducts::load_subcategory_ajax($categoryid);
        if ($sub_category) {
            $subcategoryresult = "";
			 if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_SUB_CATEGORY')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_SELECT_SUB_CATEGORY');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SELECT_SUB_CATEGORY');
			}
            $subcategoryresult = "<option value='0'> --$session_message -- </option>";
            foreach ($sub_category as $sub_category_ajax) {
                $subcategoryresult .= "<option value='" . $sub_category_ajax->sb_id . "'> " . $sub_category_ajax->sb_name . " </option>";
            }
            echo $subcategoryresult;
        } else {
			 if (Lang::has(Session::get('mer_lang_file').'.MER_NO_LIST_AVAILABLE_IN_THE_CATEGORY')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_NO_LIST_AVAILABLE_IN_THE_CATEGORY');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_NO_LIST_AVAILABLE_IN_THE_CATEGORY');
			}
            echo $subcategoryresult = "<option value='0'>$session_message </option>";
        }
    }
    
    public function product_getsecondsubcategory()
    {
        $categoryid         = $_GET['id'];
        $secondsub_category = Merchantproducts::get_second_sub_category_ajax($categoryid);
        if ($secondsub_category) {
            $secondsubcategoryresult = "";
			 if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_SECOND_SUB_CATEGORY')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_SELECT_SECOND_SUB_CATEGORY');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SELECT_SECOND_SUB_CATEGORY');
			}
            $secondsubcategoryresult = "<option value='0'> --$session_message -- </option>";
            foreach ($secondsub_category as $second_sub_category_ajax) {
                $secondsubcategoryresult .= "<option value='" . $second_sub_category_ajax->ssb_id . "'> " . $second_sub_category_ajax->ssb_name . " </option>";
            }
            echo $secondsubcategoryresult;
        } else {
			 if (Lang::has(Session::get('mer_lang_file').'.MER_NO_LIST_AVAILABLE_IN_THE_CATEGORY')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_NO_LIST_AVAILABLE_IN_THE_CATEGORY');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_NO_LIST_AVAILABLE_IN_THE_CATEGORY');
			}
            echo $secondsubcategoryresult = "<option value='0'>$session_message </option>";
        }
    }
    
    public function product_getcolor()
    {
        
        $colorid          = $_GET['id'];
        $result_colorname = Merchantproducts::get_colorname_ajax($colorid);
        
        foreach ($result_colorname as $result_colorname_ajax) {
            
            $returnvalue = $result_colorname_ajax->co_name;
            
        }
        echo $returnvalue;
    }
    
    public function product_edit_getmaincategory()
    {
        $id       = $_GET['edit_id'];
        $main_cat = Merchantproducts::get_main_category_ajax_edit($id);
        if ($main_cat) {
            $return = "";
            foreach ($main_cat as $main_cat_ajax) {
                $return = "<option value='" . $main_cat_ajax->smc_id . "' selected> " . $main_cat_ajax->smc_name . " </option>";
            }
            echo $return;
        } else {
			 if (Lang::has(Session::get('mer_lang_file').'.MER_NO_DATAS_FOUND')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_NO_DATAS_FOUND');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_NO_DATAS_FOUND');
			}
            echo $return = "<option value='0'> $session_message </option>";
        }
    }
    
    public function product_edit_getsubcategory()
    {
        $id       = $_GET['edit_sub_id'];
        $main_cat = Merchantproducts::get_sub_category_ajax_edit($id);
        if ($main_cat) {
            $return = "";
            foreach ($main_cat as $main_cat_ajax) {
                $return = "<option value='" . $main_cat_ajax->sb_id . "' selected> " . $main_cat_ajax->sb_name . " </option>";
            }
            echo $return;
        } else {
			if (Lang::has(Session::get('mer_lang_file').'.MER_NO_DATAS_FOUND')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_NO_DATAS_FOUND');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_NO_DATAS_FOUND');
			}
            echo $return = "<option value='0'> $session_message  </option>";
        }
    }
    
    public function Product_edit_getsecondsubcategory()
    {
        $id = $_GET['edit_second_sub_id'];
        
        $main_cat = Merchantproducts::get_second_sub_category_ajax_edit($id);
        if ($main_cat) {
            $return = "";
            foreach ($main_cat as $main_cat_ajax) {
                $return = "<option value='" . $main_cat_ajax->ssb_id . "' selected> " . $main_cat_ajax->ssb_name . " </option>";
            }
            echo $return;
        } else {
			if (Lang::has(Session::get('mer_lang_file').'.MER_NO_DATAS_FOUND')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_NO_DATAS_FOUND');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_NO_DATAS_FOUND');
			}
            echo $return = "<option value='0'> $session_message </option>";
        }
    }
    
    public function block_product($id, $status)
    {
        $entry = array(
            'pro_status' => $status
        );
        Merchantproducts::block_product_status($id, $entry);
        if ($status == 1) {
			 if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_UNBLOCKED')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_PRODUCT_UNBLOCKED');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_PRODUCT_UNBLOCKED');
			}
            return Redirect::to('mer_manage_product')->with('block_message',$session_message);
        } else if ($status == 0) {
			 if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_BLOCKED')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_PRODUCT_BLOCKED');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_PRODUCT_BLOCKED');
			}
            return Redirect::to('mer_manage_product')->with('block_message', $session_message);
        }
    }
    
    public function product_details($id)
    {
		 if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_PRODUCTS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_PRODUCTS');
			}
        $adminheader    = view('sitemerchant.includes.merchant_header')->with('routemenu',$session_message);
        $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_product');
        $adminfooter    = view('sitemerchant.includes.merchant_footer');
        $return         = Merchantproducts::get_product_view($id);
        $product_color_details        = Home::get_selected_product_color_details($return);
            $product_size_details         = Home::get_selected_product_size_details($return);
            $product_spec_details         = Home::get_selected_product_spec_details($return);
        return view('sitemerchant.product_details')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('product_list', $return)
        ->with('product_spec_details',$product_spec_details)
            ->with('product_color_details',$product_color_details)
            ->with('product_size_details', $product_size_details);
    }
    
    public function manage_product_shipping_details()
    {
		if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_PRODUCTS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_PRODUCTS');
			}
        $adminheader      = view('sitemerchant.includes.merchant_header')->with('routemenu', $session_message);
        $adminleftmenus   = view('sitemerchant.includes.merchant_left_menu_product');
        $adminfooter      = view('sitemerchant.includes.merchant_footer');
        $merid            = Session::get('merchantid');
        
            $shippingdetails = Merchantproducts::get_shipping_details($merid);
        
        
        return view('sitemerchant.shipping_list')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('shippingdetails', $shippingdetails);
    }
    public function mer_product_payu_shipping_details()
    {
        if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS')!= '')
            { 
                $session_message =  trans(Session::get('mer_lang_file').'.MER_PRODUCTS');
            }  
            else 
            { 
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_PRODUCTS');
            }
        $adminheader      = view('sitemerchant.includes.merchant_header')->with('routemenu', $session_message);
        $adminleftmenus   = view('sitemerchant.includes.merchant_left_menu_product');
        $adminfooter      = view('sitemerchant.includes.merchant_footer');
        $merid            = Session::get('merchantid');
        
            $shippingdetails = Merchantproducts::get_payu_shipping_details($merid);
        
        
        return view('sitemerchant.mer_payu_shipping_list')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('shippingdetails', $shippingdetails);
    }

    public function manage_cashondelivery_details()
    {
        if(Session::has('merchantid')){
		    if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_PRODUCTS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_PRODUCTS');
			}
            $adminheader      = view('sitemerchant.includes.merchant_header')->with('routemenu', $session_message);
            $adminleftmenus   = view('sitemerchant.includes.merchant_left_menu_product');
            $adminfooter      = view('sitemerchant.includes.merchant_footer');
           
            $merid            = Session::get('merchantid');
    		
           
            $coddetails  = Merchantproducts::get_cod_details($merid);
                
            	
            return view('sitemerchant.cod_list')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('coddetail', $coddetails);
        }else{
            redirect::to('sitemerchant');
        }
    }
	
    public function delete_product($id){
       
		if (Session::get('merchantid'))
			{
			if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_PRODUCTS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_PRODUCTS');
			}
			 $adminheader 		= view('sitemerchant.includes.merchant_header')->with('routemenu',$session_message);	
			 $adminleftmenus	= view('sitemerchant.includes.merchant_left_menu_product');
			 $adminfooter 		= view('sitemerchant.includes.merchant_footer');

		 $del_pro = Merchantproducts::delete_product($id);
			 if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_DELETED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_PRODUCT_DELETED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_PRODUCT_DELETED_SUCCESSFULLY');
			}
		return Redirect::to('mer_manage_product')->with('product Deleted',$session_message);	
		}
		else
        {
        return Redirect::to('sitemerchant');
        }	
	}

    //Save Edited Image
    public function mer_CropNdUpload_product(Request $request){
        //echo 'jhj';
       // var_dump($request->input('_token'));
        $data = Input::except(array(
                '_token'
            ));
       // print_r($data);exit();
        $product_id     = Input::get('product_id');
        $img_id         = Input::get('img_id');
        $imgfileName    = Input::get('imgfileName'); //old image file name

        $imageData = Input::get('base64_imgData');
        $img_dat = explode(',',Input::get('base64_imgData'));
        $new_name = 'Product_'.time().rand().'.jpg';

         //$imge =  file_put_contents('public/assets/product/'.$new_name, base64_decode($img_dat[1]));
        $imageData = base64_decode($img_dat[1]);
        //imagepng($source,'public/assets/product/'.$new_name,6);

        //$input = imagecreatefrompng($input_file);
        

        $file_path = './public/assets/product/'.$new_name;
        //Upload image with compression
        $img = Image::make(imagecreatefromstring($imageData))->save($file_path);
        //jpg background color black remove
        list($width, $height) = getimagesize($file_path);
        $output = imagecreatetruecolor($width, $height);
        $white = imagecolorallocate($output,  255, 255, 255);
        imagefilledrectangle($output, 0, 0, $width, $height, $white);
        imagecopy($output, imagecreatefromstring($imageData), 0, 0, 0, 0, $width, $height);
        imagejpeg($output, $file_path);
        //image compression
        $img = Image::make($output)->save($file_path,$this->image_compress_quality);
       

        if(file_exists('public/assets/product/'.$new_name)){

            //upload small image
            list($width,$height)=getimagesize('public/assets/product/'.$new_name);     
            

            //unlink old files starts
            if(file_exists('public/assets/product/'.$imgfileName))
                unlink("public/assets/product/".$imgfileName);
            //unlink old files ends

            //update in image table 
            $product_list = Products::get_product($product_id);

            if(count($product_list)>0){
                foreach ($product_list as $prd) {
                    
                }
                $prod_imgAr = explode('/**/', $prd->pro_Img);
                if(in_array($imgfileName,$prod_imgAr))
                {    
                    $key = array_search($imgfileName,$prod_imgAr);
                    $prod_imgAr[$key] = $new_name;
                }else {
                    $key = count($prod_imgAr);
                    $prod_imgAr[$key] = $new_name;
                }
                $prod_img = implode('/**/',$prod_imgAr);  
            }else{
                $prod_img = $new_name.'/**/';
            }

                $entry = array('pro_Img' => $prod_img );

             $return     = Products::edit_product($entry, $product_id);
             if(Lang::has(Session::get('mer_lang_file').'.MER_IMAGE_UPDATED_SUCCESSFULLY')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_IMAGE_UPDATED_SUCCESSFULLY');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_IMAGE_UPDATED_SUCCESSFULLY');
            }
             return Redirect::to('mer_edit_product/'.$product_id)->with('block_message', $session_message);
        }

        exit();
    }
    /* Image  Crop , rorate and mamipulation ends */


    public function product_mer_shop_selected(){
         $mer_id   = $_GET['mer_id'];
         $store_id = $_GET['store_id'];

         $shop_det = DB::table('nm_store')->where('stor_merchant_id', '=', $mer_id)->where('stor_status', '=', 1)->get();
 
        if ($shop_det) {
            
            $return = "";
        
            foreach ($shop_det as $shop_det_ajax) {
             
            if($shop_det_ajax->stor_id==$store_id){ $select = "selected";}else{ $select =""; }
              $return.= "<option value=".$shop_det_ajax->stor_id." ".$select.">".$shop_det_ajax->stor_name."</option>";
             }
            
            
            echo $return;
            
        }

        
        else {
            if(Lang::has(Session::get('admin_lang_file').'.BACK_NO_DATAS_FOUND')!= '') 
            {
                $session_message = trans(Session::get('admin_lang_file').'.BACK_NO_DATAS_FOUND');
            }
            else 
            {
                $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_NO_DATAS_FOUND');
            }
            echo $return = "<option value='0'> ".$session_message." </option>";
            
        }
    }
    

}

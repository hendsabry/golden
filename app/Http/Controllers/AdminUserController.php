<?php
namespace App\Http\Controllers;
use DB;
use Session;
use App\Http\Models;
use App\AdminUser;
use App\AdminUserGroup;

use Lang;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
class AdminUserController  extends Controller {

	public function __construct(){
        parent::__construct();
        // set admin Panel language
        $this->setLanguageLocaleAdmin();
    }
       
       /*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
    /*admin->add brand & its commission*/


		/*Admin user manage*/


    public function manage_user(){
  	    if(Session::has('userid')){


			if(Lang::has(Session::get('admin_lang_file').'.BACK_UASR')!= '') 
			{ 
				$session_message = trans(Session::get('admin_lang_file').'.BACK_UASR');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER');
			}
	        $adminheader    = view('siteadmin.includes.admin_header')->with("routemenu",$session_message);	
	        $adminleftmenus	= view('siteadmin.includes.admin_left_menus');
	        $adminfooter    = view('siteadmin.includes.admin_footer');
	        $userlist   = AdminUser::all();
           
	        return view('siteadmin.manage_admin_user')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('manage_user', $userlist);	
        }else{
	        return Redirect::to('siteadmin');
	    }	
    }


    public function add_user(){
	    if(Session::has('userid')){
			if(Lang::has(Session::get('admin_lang_file').'.BACK_UASR')!= '') 
			{ 
				$session_message = trans(Session::get('admin_lang_file').'.BACK_UASR');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_UASR');
			}	
			$usergrouplist   = AdminUserGroup::all();
	        $adminheader    = view('siteadmin.includes.admin_header')->with("routemenu",$session_message);	
	        $adminleftmenus	= view('siteadmin.includes.admin_left_menus');
	        $adminfooter    = view('siteadmin.includes.admin_footer');
	        return view('siteadmin.add_admin_user')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('usergrouplist',$usergrouplist);	
	    }else{
	       		 return Redirect::to('siteadmin');
	    }	
    }




     public function add_user_submit(){
	    if(Session::has('userid')){
	        $data = Input::except(array('_token')) ;
	        $rule = array(
	            'first_name'   => 'required',
	            'last_name'    => 'required' ,
	            'email'	       => 'required|email',
	            'password'	       => 'required'	            
	        );
	
	        $validator = Validator::make($data,$rule);			
	        if ($validator->fails()){
	            return Redirect::to('adduser')->withErrors($validator->messages())->withInput();
	        }else{
               
	            $first_name       = Input::get('first_name');
	            $last_name        = Input::get('last_name');
	            $email       	  = Input::get('email');
	            $mobile_number     	  = Input::get('mobile_number');
	            $usergroup     	  = Input::get('usergroup');
	            $user_address      	  = Input::get('user_address');
	            $password         = Input::get('password');

	            
	
	            /*if(($brand_image[0] > "400" || $brand_image[1] > "200")) {
	                return Redirect::to('add_brand')->with('error','Error : image size must be  400 x 200 pixels')->withInput();
	            }elseif(($brand_image[0] < "400" || $brand_image[1] < "200")) {
	                return Redirect::to('add_brand')->with('error','Error : image size must be  400 x 200 pixels')->withInput();
	            }else*/
	                $entry = array(
	                    'adm_fname'       => $first_name,
	                    'adm_lname' 	  => $last_name,
	                    'adm_email'       => $email,
	                    'access_group_id' => $usergroup,
	                    'adm_phone'       => $mobile_number,
	                    'adm_address'     => $user_address,
	                    'adm_password'    => $password,
	                    );
                        //print_r($entry);
                        
	                DB::table('nm_admin')->insert($entry);

			if(Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_INSERTED_SUCCESSFULLY')!= '') 
			{ 
				$session_message = trans(Session::get('admin_lang_file').'.BACK_RECORD_INSERTED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_RECORD_INSERTED_SUCCESSFULLY');
			}	
	                return Redirect::to('adminuser')->with('success',$session_message);	

	        }
	    }else{
	        return Redirect::to('siteadmin');
	    }	
    }//function
    
    


    /*admin->edit brand & its commission*/
    public function edit_user($userid){
  	    if(Session::has('userid')){
			if(Lang::has(Session::get('admin_lang_file').'.BACK_USER')!= '') 
			{ 
				$session_message = trans(Session::get('admin_lang_file').'.BACK_USER');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER');
			}
	        $adminheader    = view('siteadmin.includes.admin_header')->with("routemenu",$session_message);	
	        $adminleftmenus	= view('siteadmin.includes.admin_left_menus');
	        $adminfooter    = view('siteadmin.includes.admin_footer');
	        //$user_detail   = AdminUser::where($userid);
	        $users   = AdminUser::where('adm_id',$userid)->get();
	        //$users = DB::table('nm_admin')->where('adm_id',$userid)->get();
			$usergrouplist   = AdminUserGroup::all();
            return view('siteadmin.edit_user')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('user_detail', $users)->with('usergrouplist',$usergrouplist);	
        }else{
	        return Redirect::to('siteadmin');
	    }	
    }





       public function edit_user_submit(){
		if(Session::has('userid')){
			$data = Input::except(array('_token')) ;
			$id   = Input::get('admin_user_id');
			$rule = array(
					'first_name'   => 'required',
	            	'last_name'    => 'required' ,
					);
	
			$validator = Validator::make($data,$rule);			
			if ($validator->fails()){
				return Redirect::to('edit_user/'.$id)->withErrors($validator->messages())->withInput();
			}else{



	            $first_name       = Input::get('first_name');
	            $last_name        = Input::get('last_name');
	            $email       	  = Input::get('email');
	            $mobile_number    = Input::get('mobile_number');
	            $usergroup     	  = Input::get('usergroup');
	            $user_address     = Input::get('user_address');
	            $password         = Input::get('password');
				$admin_user_id    = Input::get('admin_user_id');
							
				$entry=array();
				if($password!=''){
                   
						$entry = array(
						'adm_fname'       => $first_name,
	                    'adm_lname' 	  => $last_name,
	                    'adm_email'       => $email,
	                    'access_group_id' => $usergroup,
	                    'adm_phone'       => $mobile_number,
	                    'adm_address'     => $user_address,
	                    'adm_password'    => $password,
						);
					
				}else{   //else  image has
				        $entry = array(
							'adm_fname'       => $first_name,
	                    'adm_lname' 	  => $last_name,
	                    'adm_email'       => $email,
	                    'access_group_id' => $usergroup,
	                    'adm_phone'       => $mobile_number,
	                    'adm_address'     => $user_address,
	                   
						);
				} //else

			DB::table('nm_admin')
            ->where('adm_id',$admin_user_id)
            ->update($entry);


				if(Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_UPDATED_SUCCESSFULLY')!= '') 
				{ 
					$session_message = trans(Session::get('admin_lang_file').'.BACK_RECORD_UPDATED_SUCCESSFULLY');
				}  
				else 
				{ 
					$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_RECORD_UPDATED_SUCCESSFULLY');
				}	
 			    return Redirect::to('adminuser')->with('success',$session_message);	
		    } //else
	} //if session has
	else{
		return Redirect::to('siteadmin');
	}	
  } //function

	/*delete banner in admin*/
    public function delete_user_submit($user_id){
	    if(Session::has('userid')){
			if(Lang::has(Session::get('admin_lang_file').'.BACK_USER')!= '') 
			{ 
				$session_message = trans(Session::get('admin_lang_file').'.BACK_USER');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER');
			}
	        $adminheader    = view('siteadmin.includes.admin_header')->with("routemenu",$session_message);	
	        $adminleftmenus	= view('siteadmin.includes.admin_left_menus');
	        $adminfooter    = view('siteadmin.includes.admin_footer');
 				 AdminUser::where('adm_id',$user_id)->delete();;
	        //DB::table('nm_admin')->where('adm_id', $user_id)->delete();

 				 if(Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_DELETE_SUCCESSFULLY')!= '') 
				{ 
					$session_message = trans(Session::get('admin_lang_file').'.BACK_RECORD_DELETE_SUCCESSFULLY');
				}  
				else 
				{ 
					$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_RECORD_DELETE_SUCCESSFULLY');
				}	
				return Redirect::to('adminuser')->with('success',$session_message);	

	      
	    }else{
	        return Redirect::to('siteadmin');
	    }	
    }

    public function update_user_status($id,$status){
    	if(Session::has('userid')){

    		DB::table('nm_admin')
            ->where('adm_id',$id)
            ->update(['status'=>$status]);

			if ($status == 0) {
				if(Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_UNBLOCKED_SUCCESSFULLY')!= '') 
				{ 
					$session_message = trans(Session::get('admin_lang_file').'.BACK_RECORD_UNBLOCKED_SUCCESSFULLY');
				}  
				else 
				{ 
					$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_RECORD_UNBLOCKED_SUCCESSFULLY');
				}	
                return Redirect::to('adminuser')->with('success', $session_message);
            }else if ($status == 1) {
				if(Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_BLOCKED_SUCCESSFULLY')!= '') 
				{ 
					$session_message = trans(Session::get('admin_lang_file').'.BACK_RECORD_BLOCKED_SUCCESSFULLY');
				}  
				else 
				{ 
					$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_RECORD_BLOCKED_SUCCESSFULLY');
				}	
                return Redirect::to('adminuser')->with('success', $session_message);
            }
		}else{
			return Redirect::to('siteadmin');
		}	
    }

   
    /*admin->add brand submit*/
   

 



} //class

<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Coupon;
use App\Http\Requests;

use App\BusinessOccasionType;
use App\City;

use MyPayPal;
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
use App\Formbusinesstype;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\JWT;
use Tymon\JWTAuth\JWTGuard;
use JWTAuth;
use App\User;
use Auth;
use Response;

use Exception;
use App\Country;
use Storage;
use App\Order;
use App\ProductsMerchant;

class MyAccountController extends Controller
{
   public function __construct(){
        parent::__construct();
        // set frontend language 
         $this->middleware(function ($request, $next){
                    $this->setLanguageLocaleFront();
                    return $next($request);
        });
     }
  
  
   public function myaccount()
   {  
      //$getPagelimit = config('app.paginate');
      if(Session::has('customerdata.user_id')) 
      {        
        $userid       = Session::get('customerdata.user_id');        
        $order        = DB::table('nm_order')->where('order_cus_id',$userid)->orderBy('id','DESC')->paginate(5)->appends(request()->query());
        $getImagelist = DB::table('nm_occasions')->where('posted_by',$userid)->orderBy('id','DESC')->paginate(1)->appends(request()->query());
        $getReview    = DB::table('nm_review')->where('customer_id',$userid)->paginate(2)->appends(request()->query());
        return view('my-account-dashboard',compact('order','getImagelist','getReview'));  
      } 
      else
      {
          return Redirect::to('login-signup');
      }   
   }  

   public function myaccountprofile()
   {  
      if(Session::has('customerdata.user_id')) 
      { 
        $userid  = Session::get('customerdata.user_id');
        $customerdetails = DB::table('nm_customer')->where('cus_id',$userid)->first();
        $getCountry = Country::where('co_status', 1)->orderby('co_name')->get();
        return view('my-account-profile',compact('customerdetails','getCountry')); 
      } 
      else
      {
          return Redirect::to('login-signup');
      }      
   }
   
   public function serviceorder($cus_id,$product_type,$product_sub_type,$order_id,$category_id,$product_id,$merchant_id,$shop_id)
   {

     if(Session::has('customerdata.user_id')) 
     {   
       $userid            = Session::get('customerdata.user_id');
       //$userid            = $userid;
       $product_type      = $product_type;
       $product_sub_type  = $product_sub_type;
       $order_id          = $order_id;
       $category_id       = $category_id;
       $product_id        = $product_id;
       $merchant_id       = $merchant_id;
       $shop_id           = $shop_id; 
       $lang              = Session::get('lang_file');
       //////////////////////////// Electronic Invitation Order start here///////////////
       if($product_type=='occasion' && $product_sub_type=='invitations')
       { 
        $productdetails=DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getInvitations = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($getInvitations as $value)
        {
           $moredetail = DB::table('nm_order_invitation')->where('customer_id',$userid)->where('order_id',$order_id)->first();
           $value->bookingdetail = $moredetail; 
        }
      }
      else
      {
         $getInvitations = array(); 
         
      }
       }
      //////////////////////////// Electronic Invitation Order start end here///////////////
       //////////////////////////// Car Rental order start here///////////////
       if($product_type=='car_rental' && $product_sub_type=='car')
       { 
       $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();     
      $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getOrderCarRental = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($getOrderCarRental as $value)
        {
           $moredetail = DB::table('nm_order_product_rent')->where('product_type','car_rental')->where('cus_id',$userid)->where('order_id',$order_id)->where('product_id',$value->product_id)->get();
           $value->bookingdetail = $moredetail; 
        }
   
      }
      else
      {
         $getOrderCarRental = array(); 
         
      }
    }
      //////////////////////////// Car Rental order end here///////////////

      //////////////////////////// Travel Agency order start here///////////////
       if($product_type=='travel' && $product_sub_type=='travel')
       {  
       $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
      $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getOrderTravel = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($getOrderTravel as $value)
        {
           $moredetail = DB::table('nm_order_product_rent')->where('product_type','travel')->where('cus_id',$userid)->where('order_id',$order_id)->where('product_id',$value->product_id)->first();
           $value->bookingdetail = $moredetail; 
        }
   
      }
      else
      {
         $getOrderTravel = array(); 
         
      }
       }
      //////////////////////////// Car Rental Order end here///////////////  
       
      //////////////////////////// Beauty Center Order start here///////////////
       if($product_type=='beauty' && $product_sub_type=='beauty_centers')
       {   
       $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();   
      $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getOrderBeautyCenters = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($getOrderBeautyCenters as $value)
        {
           $moredetail = DB::table('nm_order_services_staff')->where('product_type','beauty')->where('cus_id',$userid)->where('order_id',$order_id)->where('service_id',$value->product_id)->first();
           $value->bookingdetail = $moredetail; 
        }
   
      }
      else
      {
         $getOrderBeautyCenters = array(); 
         
      }
       }
      //////////////////////////// Beauty Center Order end here///////////////

      //////////////////////////// Beauty Spa Order start here///////////////
       if($product_type=='beauty' && $product_sub_type=='spa')
       {  
       $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
      $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getOrderSpa = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($getOrderSpa as $value)
        {
           $moredetail = DB::table('nm_order_services_staff')->where('product_type','beauty')->where('cus_id',$userid)->where('order_id',$order_id)->where('service_id',$value->product_id)->first();
           $value->bookingdetail = $moredetail; 
        }
   
      }
      else
      {
         $getOrderSpa = array(); 
         
      }
       }
      //////////////////////////// Beauty Spa Order end here///////////////   
      
      //////////////////////////// Barber Order start here///////////////
       if($product_type=='beauty' && $product_sub_type=='men_saloon')
       {   
       $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();   
      $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getOrderBarber = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($getOrderBarber as $value)
        {
           $moredetail = DB::table('nm_order_services_staff')->where('product_type','beauty')->where('cus_id',$userid)->where('order_id',$order_id)->where('service_id',$value->product_id)->first();
           $value->bookingdetail = $moredetail; 
        }
   
      }
      else
      {
         $getOrderBarber = array(); 
         
      }
       }
      //////////////////////////// Barber Order end here///////////////

      //////////////////////////// Makeup Artist Order start here///////////////
       if($product_type=='beauty' && $product_sub_type=='makeup_artists')
       {   
       $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();   
      $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getOrderMakeupArtist = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get(); 
        foreach($getOrderMakeupArtist as $value)
        {
           $moredetail = DB::table('nm_order_services_staff')->where('product_type','beauty')->where('cus_id',$userid)->where('order_id',$order_id)->where('service_id',$value->product_id)->first();
           $value->bookingdetail = $moredetail; 
        }
      }
      else
      {
         $getOrderMakeupArtist = array();         
      }
       }
      //////////////////////////// Makeup Artist Order end here///////////////
      
      //////////////////////////// Makeup Product Order start here///////////////
       if($product_type=='beauty' && $product_sub_type=='makeup')
       {   
       $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();   
      $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getOrderMakeup = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($getOrderMakeup as $value)
        {
           $moredetail = DB::table('nm_order_services_staff')->where('product_type','beauty')->where('cus_id',$userid)->where('order_id',$order_id)->where('service_id',$value->product_id)->first();
           $value->bookingdetail = $moredetail; 
        }
   
      }
      else
      {
         $getOrderMakeup = array(); 
         
      }
       }
      //////////////////////////// Makeup Product Order end here///////////////

      //////////////////////////// Clinic Cosmetics And Laser Order start here///////////////
       if($product_type=='clinic' && $product_sub_type=='cosmetic')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getCosmetic = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($getCosmetic as $value)
        {
           $moredetail = DB::table('nm_order_services_staff')->where('product_type','clinic')->where('cus_id',$userid)->where('order_id',$order_id)->where('service_id',$value->product_id)->first();
           $value->bookingdetail = $moredetail; 
        }
   
      }
      else
      {
         $getCosmetic = array(); 
         
      }
       }
      //////////////////////////// Clinic Cosmetics And Laser Order start end here///////////////
       //////////////////////////// Dental And Dermatology Order start here///////////////
       if($product_type=='clinic' && $product_sub_type=='skin')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getSkin = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($getSkin as $value)
        {
           $moredetail = DB::table('nm_order_services_staff')->where('product_type','clinic')->where('cus_id',$userid)->where('order_id',$order_id)->where('service_id',$value->product_id)->first();
           $value->bookingdetail = $moredetail; 
        }
   
      }
      else
      {
         $getSkin = array(); 
         
      }
       }
      //////////////////////////// Dental And Dermatology Order start end here/////////////// 


       //////////////////////////// Jewellery & Gold Order start here///////////////
       if($product_type=='shopping' && $product_sub_type=='gold')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getJewellery = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
      }
      else
      {
         $getJewellery = array(); 
         
      }
       }
      //////////////////////////// Jewellery & Gold Order start end here/////////////// 

      //////////////////////////// Perfumes Order start here///////////////
       if($product_type=='shopping' && $product_sub_type=='perfume')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
          if($countCart>0)
          {
            $lang  = Session::get('lang_file');
            $getPerfume = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
          }
          else
          {
             $getPerfume = array(); 
             
          }
       }
      //////////////////////////// Perfumes Order start end here///////////////   

      //////////////////////////// Hotal Halls Order start here///////////////
       if($product_type=='hall' && $product_sub_type=='hall')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
          if($countCart>0)
          {
            $lang  = Session::get('lang_file');
            $getHotalHalls = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
            foreach($getHotalHalls as $value)
            {
              if(isset($lang) && $lang == 'ar_lang')
              {
                $moredetail = DB::table('nm_order_internal_food_dish')->where('cus_id',$userid)->where('order_id',$order_id)->where('product_id',$value->product_id)->select('cus_id','order_id','product_id','internal_food_menu_id','internal_food_dish_id','container_id','container_title','container_image','quantity','price','internal_dish_name_ar as internal_dish_name','dish_image')->get();
                $value->bookingdetail = $moredetail;
              }
              else
              {
                $moredetail = DB::table('nm_order_internal_food_dish')->where('cus_id',$userid)->where('order_id',$order_id)->where('product_id',$value->product_id)->select('cus_id','order_id','product_id','internal_food_menu_id','internal_food_dish_id','container_id','container_title','container_image','quantity','price','internal_dish_name','dish_image')->get();
                $value->bookingdetail = $moredetail;
              }           
            }   
          }
          else
          {
             $getHotalHalls = array(); 
             
          }
       }
      //////////////////////////// Hotal Halls Order start end here/////////////// 

       //////////////////////////// Buffets Order start here///////////////
       if($product_type=='food' && $product_sub_type=='buffet')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
          if($countCart>0)
          {
            $lang  = Session::get('lang_file');
            $getBuffets = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get(); 
            foreach($getBuffets as $value)
            {
                $moredetail = DB::table('nm_order_external_food_dish')->where('external_food_dish_id',$value->product_id)->where('cus_id',$userid)->where('order_id',$order_id)->get();
                $value->bookingdetail = $moredetail;
            }          
          }
          else
          {
             $getBuffets = array(); 
             
          }
          //dd($getBuffets);
       }
      //////////////////////////// Buffets Order start end here/////////////// 

       //////////////////////////// Dates start here///////////////
       if($product_type=='food' && $product_sub_type=='dates')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
          if($countCart>0)
          {
            $lang  = Session::get('lang_file');
            $getDates = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();           
          }
          else
          {
             $getDates = array(); 
             
          }
       }
      //////////////////////////// Dates Order start end here/////////////// 

      //////////////////////////// Desserts Order here///////////////
       if($product_type=='food' && $product_sub_type=='dessert')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
          if($countCart>0)
          {
            $lang  = Session::get('lang_file');
            $getDesserts = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();           
          }
          else
          {
             $getDesserts = array(); 
             
          }
       }
      //////////////////////////// Desserts Order start end here/////////////// 
      
      //////////////////////////// Tailors Order start here///////////////
       if($product_type=='shopping' && $product_sub_type=='tailor')
       {   
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();   
          $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
          if($countCart>0)
          {
            $lang  = Session::get('lang_file');
            $getOrderTailor = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();

            foreach($getOrderTailor as $value)
            {
               $moredetail = DB::table('nm_order_body_measurement')->where('customer_id',$userid)->where('order_id',$order_id)->where('product_id',$value->product_id)->first();               
               $value->bookingdetail = $moredetail; 
            }
       
          }
          else
          {
             $getOrderTailor = array(); 
             
          }
      }
      //////////////////////////// Tailors Order end here///////////////
      
      //////////////////////////// Dresses Order start here///////////////
       if($product_type=='shopping' && $product_sub_type=='dress')
       {   
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();   
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
          if($countCart>0)
          {
            $lang  = Session::get('lang_file');
            $getOrderDress = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
            foreach($getOrderDress as $value)
            {
               $moredetail = DB::table('nm_order_product_rent')->where('cus_id',$userid)->where('order_id',$order_id)->where('product_id',$value->product_id)->where('product_type','shopping')->first();
               $value->bookingdetail = $moredetail; 
            }
       
          }
          else
          {
             $getOrderDress = array(); 
             
          }
      }
      //////////////////////////// Dresses Order end here///////////////
      //////////////////////////// Abaya Order start here///////////////
       if($product_type=='shopping' && $product_sub_type=='abaya')
       {   
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();
        
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
          if($countCart>0)
          {
            $lang  = Session::get('lang_file');
            $getOrderAbaya = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
            foreach($getOrderAbaya as $value)
            {
               $moredetail = DB::table('nm_order_body_measurement')->where('customer_id',$userid)->where('order_id',$order_id)->where('product_id',$value->product_id)->first();
               $value->bookingdetail = $moredetail; 
            }
       
          }
          else
          {
             $getOrderAbaya = array();              
          }  
      }
      //////////////////////////// Abaya Order end here///////////////
      
      //////////////////////////// Sound Sysytems Acoustic Order start here///////////////
       if($product_type=='music' && $product_sub_type=='acoustic')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang = Session::get('lang_file');
        $SoundSysytemsAcoustic = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($SoundSysytemsAcoustic as $value)
        {
           $moredetail = DB::table('nm_order_product_rent')->where('product_type','music')->where('cus_id',$userid)->where('order_id',$order_id)->where('product_id',$value->product_id)->first();
           $value->bookingdetail = $moredetail; 
        }
          
      }
      else
      {
         $SoundSysytemsAcoustic = array(); 
         
      }
       }
      //////////////////////////// Sound Sysytems Acoustic Order start end here/////////////// 
      
      //////////////////////////// Kosha Order start here///////////////
       if($product_type=='occasion' && $product_sub_type=='cosha')
       {   
       $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();   
      $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('showcartproduct',0)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getOrderKosha = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('showcartproduct',0)->where('shop_id',$shop_id)->get();  
      }
      else
      {
         $getOrderKosha = array(); 
         
      }
       }
      //////////////////////////// Kosha Order end here///////////////
       //////////////////////////// Photography Studio Order start here///////////////
       if($product_type=='occasion' && ($product_sub_type=='photography' || $product_sub_type=='video'))
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->whereIn('product_sub_type',['photography','video'])->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $photographyStudio = DB::table('nm_order_product')->where('product_type',$product_type)->whereIn('product_sub_type',['photography','video'])->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
          
      }
      else
      {
         $photographyStudio = array(); 
         
      }
       }
      //////////////////////////// Photography Studio Order start end here/////////////// 
       //////////////////////////// Reception & Hospitality Order start here///////////////
       if($product_type=='occasion' && $product_sub_type=='hospitality')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang = Session::get('lang_file');
        $ReceptionHospitality = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
          
      }
      else
      {
         $ReceptionHospitality = array(); 
         
      }
       }
      //////////////////////////// Reception & Hospitality Order start end here/////////////// 

        //////////////////////////// Roses Order start here///////////////
       if($product_type=='occasion' && $product_sub_type=='roses')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang = Session::get('lang_file');
        $orderRoses = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
          
      }
      else
      {
         $orderRoses = array(); 
         
      }
       }
      //////////////////////////// Roses Order start end here/////////////// 

      //////////////////////////// Special Event Order start here///////////////
       if($product_type=='occasion' && $product_sub_type=='events')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang = Session::get('lang_file');
        $orderSpecialEvent = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
          
      }
      else
      {
         $orderSpecialEvent = array(); 
         
      }
       }
      //////////////////////////// Special Event Order start end here/////////////// 
       //////////////////////////// Singer Order start here///////////////
       if($product_type=='singer' && $product_sub_type=='singer')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getSinger = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
      }
      else
      {
         $getSinger = array(); 
         
      }
       }
      //////////////////////////// Singer Order start end here///////////////
       //////////////////////////// Singer Order start here///////////////
       if($product_type=='band' && $product_sub_type=='band')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getBand = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
      }
      else
      {
         $getBand = array(); 
         
      }
       }
      //////////////////////////// Singer Order start end here///////////////

       //////////////////////////// Sound Sysytem form Order start here///////////////
       if($product_type=='recording' && $product_sub_type=='recording')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getRecording = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('cus_id',$userid)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
      }
      else
      {
         $getRecording = array(); 
         
      }
       }
      //////////////////////////// Sound Sysytem form Order start end here///////////////


	   
       return view('serviceorder',compact('productdetails','cus_id','product_type','product_sub_type','order_id','category_id','product_id','merchant_id','shop_id','productdetails','getOrderCarRental','getOrderTravel','getOrderBeautyCenters','getOrderSpa','getCosmetic','getSkin','getOrderBarber','getOrderMakeupArtist','getOrderMakeup','getJewellery','getPerfume','getHotalHalls','getBuffets','getDates','getDesserts','getOrderTailor','getOrderDress','getOrderAbaya','getOrderKosha','photographyStudio','ReceptionHospitality','SoundSysytemsAcoustic','orderRoses','orderSpecialEvent','getSinger','getBand','getRecording','getInvitations'));   
      }
        else
        {
          return Redirect::to('login-signup');
        } 
   }

   public function updatemyprofile(Request $request)
  {      
        $userid  = Session::get('customerdata.user_id');
          $lang = Session::get('lang_file');
          if(isset($lang) && $lang == 'ar_lang') 
          {
             $msg = 'يرجى تحميل صورة صحيحة ention jpg، jpeg، png، gif | الحد الأقصى: 2048 كيلوبايت.';
          }
          else
          {
             $msg = 'Please upload image valid entation jpg,jpeg,png,gif | max:2048 kb.';  
          }
            $validatedData = $request->validate([
                'name'=>'required|max:100',
                  'email'=>'required|max:100|unique:nm_customer,email,'.$userid.',cus_id',
                  'telephone_number' =>'required|numeric',
                  'gender' =>'required',
                  'dob' =>'required',
                  'address' =>'required',
                  //'credit_card' =>'required',
                  'city' =>'required',
                  //'customer_pic.*' => 'required|image|mimes:jpg,jpeg,png,gif',                   
                  'pin_code' =>'required|numeric',
                  'customer_pic' => 'image|mimes:jpg,jpeg,png,gif|max:2048',
                  'bank_name' =>'required',
                  'bank_account' =>'required|numeric'           
                ],[
              'dob.required' => 'Please select your date of birth.',
              'customer_pic.image' => $msg
            ]);  

          //upload Image
          if($request->file('customer_pic'))
          {       
                $file                = $request->customer_pic;                      
                $extension           = $file->getClientOriginalExtension();
                $Image               = rand(11111,99999).'_'.time().'.'.$extension;               
                $imageRealPath       = $file->getRealPath();
                $thumbName           = 'thumb-'. $Image;       
                $img                 = Image::make($imageRealPath); // use this if you want facade style code
                $thumb_width         = 150;
                list($width,$height) = getimagesize($imageRealPath);
                $thumb_height        = ($thumb_width/$width) * $height;
                $img->resize($thumb_width,$thumb_height);
                $img->save('public/assets/adimage'.'/'.$thumbName);
                $file->move('public/assets/adimage/', $Image);
                $pic_url = url('').'/public/assets/adimage/'.$thumbName;
                if($request->oldpic)
                {
                  $fullurl = explode('-',$request->oldpic);
                  @unlink('public/assets/adimage/'.$fullurl[1]);
                  @unlink('public/assets/adimage/thumb-'.$fullurl[1]);        
                }
            }
            else
            {
               $pic_url  = $request->oldpic;
            }
            
            if($request->file('address_image'))
            {       
                $file                = $request->address_image;                      
                $extension           = $file->getClientOriginalExtension();
                $Image               = rand(11111,99999).'_'.time().'.'.$extension;               
                $imageRealPath       = $file->getRealPath();
                $thumbName           = 'thumb-'. $Image;       
                $img                 = Image::make($imageRealPath); // use this if you want facade style code
                $thumb_width         = 150;
                list($width,$height) = getimagesize($imageRealPath);
                $thumb_height        = ($thumb_width/$width) * $height;
                $img->resize($thumb_width,$thumb_height);
                $img->save('public/assets/adimage'.'/'.$thumbName);
                $file->move('public/assets/adimage/', $Image);
                $address_image_url = url('').'/public/assets/adimage/'.$thumbName;
                if($request->oldaddress_image)
                {
                  $fullurl = explode('-',$request->oldaddress_image);
                  @unlink('public/assets/adimage/'.$fullurl[1]);
                  @unlink('public/assets/adimage/thumb-'.$fullurl[1]);        
                }
            }
            else
            {
               $address_image_url  = $request->oldaddress_image;
            }

            //echo '<pre>';print_r($_REQUEST);die;
            $name              = $request->name;
            $email             = $request->email;
            $country_code      = $request->country_code;
            $telephone_number  = $request->telephone_number;
            $gender            = $request->gender;
            $address_latitude   = $request->address_latitude;
            $address_longitude = $request->address_longitude;
            $dob               = $request->dob;
            $address           = $request->address;
            $address2          = $request->address2;
            $credit_card       = $request->credit_card;
            $city              = $request->city;
            $pin_code          = $request->pin_code;
            $bank_name         = $request->bank_name;
            $bank_account      = $request->bank_account;
            $map_url           = $request->map_url;
            $updateData = DB::table('nm_customer')->where('cus_id',$userid)->update(
              ['cus_name'=>$name,
               'country_code'=>$country_code,
               'email'=>$email,
               'cus_phone'=>'+'.$country_code.'-'.$telephone_number,
                 'gender'=>$gender,
                 'dob'=>$dob,
                 'address_latitude'=>$address_latitude,
                 'address_longitude'=>$address_longitude,
                 'cus_address1'=>$address,
                 'cus_address2'=>$address2,
                 'cus_city'=>$city,
                 'ship_postalcode'=>$pin_code,
                 'cus_pic'=>$pic_url,
                 'bank_name'=>$bank_name,
                 'map_url'=>$map_url,
                 'bank_account'=>$bank_account,
                 'address_image'=>$address_image_url,
                 'credit_card'=>$credit_card
              ]);
            if($updateData)
            { 
              // language for display message //
        if(\Config::get('app.locale') == 'ar'){
        Session::flash('message', "تم تحديث الملف الشخصي بنجاح");
        }
        else
        {
        Session::flash('message', "Profile updated successfully");
        }
        // language for display message // 
        return Redirect::back();  
            }
            else
            {
              return Redirect::to('my-account-profile');
            }     
     }

     public function myaccountocassion(Request $request)
     {  
        $search_key   = $request->search_key;
        $getPagelimit = config('app.paginate');
        if(Session::has('customerdata.user_id')) 
        {   
           $userid  = Session::get('customerdata.user_id');
            

           if(isset($search_key) && $search_key=='date'){
            $order = DB::table('nm_order')->where('order_cus_id',$userid)->orderBy('occasion_date','DESC')->paginate($getPagelimit)->appends(request()->query());
           }elseif (isset($search_key) && $search_key=='all') {
              $order = DB::table('nm_order')->where('order_cus_id',$userid)->orderBy('id','DESC')->paginate($getPagelimit)->appends(request()->query());
           }
           else{
            $order = DB::table('nm_order')->where('order_cus_id',$userid)->orderBy('id','DESC')->paginate($getPagelimit)->appends(request()->query());
           }

           return view('my-account-ocassion')->with('setOrder',$order);  
        } 
        else
        {
          return Redirect::to('login-signup');
        }   
     }
    
    /*public function myaccountocassionwithajax()
    {  
        //print_r($_REQUEST['search_key']);die;
        $getPagelimit = config('app.paginate');
        $userid  = Session::get('customerdata.user_id');
        $getOdrder = DB::table('nm_order')->where('order_cus_id',$userid)->where('order_date',$_REQUEST['search_key'])->orderBy('order_date','desc')->paginate($getPagelimit)->appends(request()->query());
           echo '<pre>';print_r($getOdrder);die;
           return view('my-account-ocassion')->with('setOrder',$getOdrder);  
           
     }*/


     public function myaccountocassiondetail()
     {  
        //echo $id;die;
        if(Session::has('customerdata.user_id')) 
        {   
           return view('my-account-ocassion-detail');  
        } 
        else
        {
          return Redirect::to('login-signup');
        }   
     }

     public function myaccountstudio()
     {
        if(Session::has('customerdata.user_id')) 
        { 
          $userid  = Session::get('customerdata.user_id');
          if(isset($_REQUEST['gallery_city']) && $_REQUEST['gallery_city']!='')
          {
            $cityId = $_REQUEST['gallery_city'];
            $getImagelist = DB::table('nm_occasions')->where('posted_by',$userid)->where('city_id',$cityId)->orderBy('id','DESC')->get();
          }
          else
          {
            $getImagelist = DB::table('nm_occasions')->where('posted_by',$userid)->orderBy('id','DESC')->get();
          }
          
          return view('my-account-studio')->with('setImagelist',$getImagelist);  
        } 
        else
        {
          return Redirect::to('login-signup');
        }   
     }

     public function insertstudio(Request $request)
     {
        $userid  = Session::get('customerdata.user_id');
        if(isset($lang) && $lang == 'ar_lang') 
        {
           $msg = 'يرجى تحميل صورة صحيحة ention jpg، jpeg، png، gif | الحد الأقصى: 2048 كيلوبايت.';
        }
        else
        {
           $msg = 'Please upload image valid entation jpg,jpeg,png,gif | max:2048 kb.';  
        }
        $validatedData = $request->validate([
                  'city'=>'required',
                  'studio_date'=>'required',
                  'occasion' =>'required',
                  'venue' =>'required',
                  'upload_your_pic' => 'required|image|mimes:jpg,jpeg,png,gif'           
                ],[
                 'upload_your_pic.image' => $msg
            ]); 

            //upload Image
            if($request->file('upload_your_pic'))
            {           
                $file                = $request->upload_your_pic;                      
                $extension           = $file->getClientOriginalExtension();
                $Image               = rand(11111,99999).'_'.time().'.'.$extension;               
                $imageRealPath       = $file->getRealPath();
                $thumbName           = 'thumb-'. $Image;       
                $img                 = Image::make($imageRealPath); // use this if you want facade style code
                $thumb_width         = 300;
                list($width,$height) = getimagesize($imageRealPath);
                $thumb_height        = ($thumb_width/$width) * $height;
                $img->resize($thumb_width,$thumb_height);
                $img->save('public/studio_img'.'/'.$thumbName);
                $file->move('public/studio_img/', $Image);
                $pic_url = url('').'/public/studio_img/'.$thumbName;                
            }
            
            $getOcType = DB::table('nm_business_occasion_type')->where('id',$request->occasion)->first();
            $dataArr   = array(
                            'city_id'        => $request->city,
                            'studio_date'    => $request->studio_date,
                            'occasion_name'  => $getOcType->title,
                            'occasion_type'  => $request->occasion,
                            'occasion_venue' => $request->venue,
                            'posted_by'      => $userid,
                            'images'         =>$pic_url);

            $getOcList = DB::table('nm_occasions')->where('posted_by',$userid)->where('occasion_type',$request->occasion)->where('occasion_venue',$request->venue)->where('city_id',$request->city)->where('studio_date',$request->studio_date)->first();
            if(isset($getOcList) && !empty($getOcList))
            {
              $occId = $getOcList->id;
              DB::table('nm_occasions_gallery')->insert(array('occasion_id'=>$occId,'image_url'=>$pic_url));
              if($files=$request->file('uploadyourpicmore'))
              {
                  foreach($files as $file)
                  {
                    $name                = str_replace(' ','_', time().'_'.$file->getClientOriginalName());
                    $extension           = $file->getClientOriginalExtension();
                    $fileName            = rand(11111,99999).'_'.time().'.'.$extension;
                    $imageRealPath       = $file->getRealPath();
                    $thumbName           = 'thumb-'. $fileName;       
                    $img                 = Image::make($imageRealPath); // use this if you want facade style code
                    $thumb_width         = 300;
                    list($width,$height) = getimagesize($imageRealPath);
                    $thumb_height        = ($thumb_width/$width) * $height;
                    $img->resize($thumb_width,$thumb_height);                  
                    $img->save('public/studio_img' . '/'. $thumbName);
                    $file->move('public/studio_img/', $fileName);
                    $shop_Img = url('').'/public/studio_img/'.$thumbName; 
                       
                    /*Insert data to db*/
                    DB::table('nm_occasions_gallery')->insert(array('occasion_id'=>$insertData,'image_url'=>$shop_Img));
                   }  

              }
              
              // language for display message //
              if(\Config::get('app.locale') == 'ar'){
              Session::flash('message', "تم إدخاله بنجاح");
              }
              else
              {
                Session::flash('message', "Inserted successfully");
              }
              // language for display message // 
              return Redirect::to('my-account-studio');
            }
            else
            {
            $insertData = DB::table('nm_occasions')->insertGetId($dataArr);
            if($insertData)
            { 
              DB::table('nm_occasions_gallery')->insert(array('occasion_id'=>$insertData,'image_url'=>$pic_url));
              if($files=$request->file('uploadyourpicmore'))
              {
                  foreach($files as $file)
                  {
                    $name                = str_replace(' ','_', time().'_'.$file->getClientOriginalName());
                    $extension           = $file->getClientOriginalExtension();
                    $fileName            = rand(11111,99999).'_'.time().'.'.$extension;
                    $imageRealPath       = $file->getRealPath();
                    $thumbName           = 'thumb-'. $fileName;       
                    $img                 = Image::make($imageRealPath); // use this if you want facade style code
                    $thumb_width         = 300;
                    list($width,$height) = getimagesize($imageRealPath);
                    $thumb_height        = ($thumb_width/$width) * $height;
                    $img->resize($thumb_width,$thumb_height);                  
                    $img->save('public/studio_img' . '/'. $thumbName);
                    $file->move('public/studio_img/', $fileName);
                    $shop_Img = url('').'/public/studio_img/'.$thumbName; 
                       
                    /*Insert data to db*/
                    DB::table('nm_occasions_gallery')->insert(array('occasion_id'=>$insertData,'image_url'=>$shop_Img));
                   }  

              }
              
              // language for display message //
              if(\Config::get('app.locale') == 'ar'){
              Session::flash('message', "تم إدخاله بنجاح");
              }
              else
              {
                Session::flash('message', "Inserted successfully");
              }
              // language for display message // 
              return Redirect::to('my-account-studio');  
            }
            else
            {
              return Redirect::to('my-account-studio');
            } 
          }
     }
     
     public function myaccountsecurity()
     {  
        if(Session::has('customerdata.user_id')) 
        {   
           $userid  = Session::get('customerdata.user_id');         
           $productdetails = DB::table('nm_order')
           ->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')
           ->where('nm_order.order_cus_id',$userid)->where('nm_order_product.insurance_amount','!=','')->orderBy('order_date','DESC')->paginate(50)->appends(request()->query());
           return view('my-account-security',compact('productdetails'));  
        } 
        else
        {
          return Redirect::to('login-signup');
        }   
     }
     
     public function myaccountwallet()
     {  
        if(Session::has('customerdata.user_id')) 
        {   
           return view('my-account-wallet');  
        } 
        else
        {
          return Redirect::to('login-signup');
        }   
     }

     public function myrequestaquote(Request $request)
     {
        $getPagelimit = config('app.paginate');
        $search_key   = $request->search_key;
        if(Session::has('customerdata.user_id')) 
        {   
           $userid  = Session::get('customerdata.user_id');
           if(isset($search_key) && $search_key=='singer')
           {
            $getallinquiry = DB::table('nm_services_inquiry')->where('user_id',$userid)->where('quote_for',$search_key)->orderBy('created_at','DESC')->paginate($getPagelimit)->appends(request()->query());
           }
           elseif(isset($search_key) && $search_key=='band') 
           {
              $getallinquiry = DB::table('nm_services_inquiry')->where('user_id',$userid)->where('quote_for',$search_key)->orderBy('created_at','DESC')->paginate($getPagelimit)->appends(request()->query());
           }
           elseif(isset($search_key) && $search_key=='recording') 
           {
              $getallinquiry = DB::table('nm_services_inquiry')->where('user_id',$userid)->where('quote_for',$search_key)->orderBy('created_at','DESC')->paginate($getPagelimit)->appends(request()->query());
           }
           else
           {
            $getallinquiry = DB::table('nm_services_inquiry')->where('user_id',$userid)->orderBy('created_at','DESC')->paginate($getPagelimit)->appends(request()->query());
           }          
           return view('my-request-a-quote',compact('getallinquiry'));  
        } 
        else
        {
          return Redirect::to('login-signup');
        }   
     }

    public function requestaquoteview($id)
    {
        if(Session::has('customerdata.user_id')) 
        {   
           $userid  = Session::get('customerdata.user_id'); 
           $allinquiry = DB::table('nm_services_inquiry')->where('user_id',$userid)->where('id',$id)->first();
           $vendoramt = DB::table('nm_vendor_comment')->where('user_id',$userid)->where('enq_id',$id)->first();
           return view('request-a-quote-view',compact('id','allinquiry','vendoramt'));  
        } 
        else
        {
          return Redirect::to('login-signup');
        }   
     }
     
    public function reviewdisplay($shopid,$orderid,$vendorid)
    {
      if(Session::has('customerdata.user_id')) 
        {   
           $userid  = Session::get('customerdata.user_id');


          $allreviewsC = DB::table('nm_review')->where('shop_id',$shopid)->where('order_id',$orderid)->where('customer_id',$userid)->where('vandor_id',$vendorid)->count();
          if( $allreviewsC >= 1)
          {

          $allreview = DB::table('nm_review')->where('shop_id',$shopid)->where('order_id',$orderid)->where('customer_id',$userid)->where('vandor_id',$vendorid)->orderBy('review_type','DESC')->get();
          }
          else
          {
          $allreview = DB::table('nm_review')->where('shop_id',$shopid)->where('order_id',$orderid)->where('customer_id',$userid)->where('vandor_id',$vendorid)->orderBy('review_type','DESC')->get();

          }
 
        
          // $allreview = DB::table('nm_review')->where('shop_id',$shopid)->where('order_id',$orderid)->where('customer_id',$userid)->where('vandor_id',$vendorid)->orderBy('comment_id','DESC')->get();

           $getsingleorder = DB::table('nm_order_product')->where('shop_id',$shopid)->where('order_id',$orderid)->where('cus_id',$userid)->where('merchant_id',$vendorid)->first();
           //dd($getsingleorder);
           return view('review_rating_view',compact('shopid','orderid','vendorid','allreview','getsingleorder'));  
        } 
        else
        {
          return Redirect::to('login-signup');
        } 
    }

    public function updaterequestaquoteview()
    {
        if(Session::has('customerdata.user_id')) 
        {   
          $userid  = Session::get('customerdata.user_id');
          $last_id  = $_REQUEST['last_id'];  
          $submit  = $_REQUEST['submit'];   
          $deny = array('Deny','أنكر');
          $status = 2;
          if(in_array($submit, $deny)){ $status = 4;}  

          DB::table('nm_services_inquiry')->where('user_id',$userid)->where('id',$last_id)->update(['status'=>$status,'payment_status'=>0]);
 

          //language for display message // 
          if($status==2)
          {           
          Session::put('occasionid',$last_id);
          $getInfo =  DB::table('nm_services_inquiry')->where('user_id',$userid)->where('id',$last_id)->first();
          $merchant_id = $getInfo->merchant_id;
          $relate_with =  $getInfo->relate_with;
          $user_id = $getInfo->user_id;
          $city_id = $getInfo->city_id;
          $quote_for = $getInfo->quote_for;
          $totalprice = DB::table('nm_vendor_comment')->where('enq_id',$last_id)->first();

           $total_price = $totalprice->price;
          $id = $totalprice->id;
          $singerid=$getInfo->music_id;


          $getInfoc =  DB::table('nm_cart')->where('user_id',$userid)->count();
          if($getInfoc >=1)
          {
          $getInfos =  DB::table('nm_cart')->where('user_id',$userid)->first();
          $cart_id =$getInfos->id; 
          }
          else
          {
           $getcID =  DB::table('nm_cart')->insert(array('user_id' => $userid)); 
          $cart_id = DB::getPdo()->lastInsertId(); 
          }         

           DB::table('nm_cart_product')->where('cart_type',$quote_for)->where('merchant_id',$merchant_id)->where('shop_id',$id)->where('product_id',$singerid)->where('status',1)->delete();

          DB::table('nm_cart_product')->insert(array('merchant_id' => $merchant_id,'shop_id' => $singerid,'shop_vendor_id' => $id,'cart_type' => $quote_for,'cart_id' => $cart_id,'category_id' => $id,'product_id' => $singerid,'total_price' => $total_price,'cart_sub_type' => $quote_for,'status'=>1,'enquiry_id'=>$last_id));
          return Redirect::to('checkout');  
          }
          else
          {
          if(\Config::get('app.locale') == 'ar'){
          Session::flash('message', "تم إدخاله بنجاح");
          }
          else
          {
          Session::flash('message', "Deny successfully");
          }
          // language for display message //

           return Redirect::to('requestaquoteview/'.$last_id); 
          }

 
          
        } 
        else
        {
          return Redirect::to('login-signup');
        }   
     }
    
     public function myaccountreview()
     {       
        if(Session::has('customerdata.user_id')) 
        {           
           $userid  = Session::get('customerdata.user_id');

           $getReview  = $productdetails = DB::table('nm_order')
           ->leftjoin('nm_order_product', 'nm_order.order_id','=','nm_order_product.order_id')
           ->where('nm_order.order_cus_id',$userid)->groupBy('nm_order.order_id')->orderBy('nm_order.id','DESC')->paginate(25)->appends(request()->query());
           //dd($getReview);
           //$getReview  = DB::table('nm_review')->where('customer_id',$userid)->groupBy('shop_id')->get();           
           return view('my-account-review')->with('setRewiev',$getReview);   
        } 
        else
        {
          return Redirect::to('login-signup');
        }   
     }

     public function changepassword()
     {  
        if(Session::has('customerdata.user_id')) 
        { 
          $userid  = Session::get('customerdata.user_id');  
          $getDb = DB::table('nm_customer')->where('cus_id',$userid)->first();
          return view('change-password')->with('customerdetails',$getDb); 
        } 
        else
        {
              return Redirect::to('login-signup');
        }   
     }

     public function changepasswordupdate(Request $request)
   {  
      if(Session::has('customerdata.user_id')) 
      { 
          $userid  = Session::get('customerdata.user_id');
        $validatedData = $request->validate([
                'old_password'=>'required', 
                'new_password'=>'required|min:8',
                'confirm_password'=>'required|same:new_password'         
                ]); 

             $data = $request->all();
            $user = DB::table('nm_customer')->where('cus_id',$userid)->first();
            if(!Hash::check($data['old_password'], $user->password))
            {
                if(\Config::get('app.locale') == 'ar')
                {
                    return back()->with('error','كلمة المرور لا تتطابق مع كلمة مرور قاعدة البيانات');
                } 
                else
                {
                    return back()->with('error','Please enter correct password.');
                }               
            }
            else
            {
               $new_password = $data['new_password'];
               $updatePassword = DB::table('nm_customer')->where('cus_id',$userid)->update(['password'=>bcrypt($new_password)]);
               if($updatePassword)
               { 
                // language for display message //
                if(\Config::get('app.locale') == 'ar')
                {
                   Session::flash('message', "تم تحديث كلمة السر بنجاح");
                }
                else
                {
                   Session::flash('message', "Password changed successfully");
                }
                // language for display message // 
                return Redirect::back();  
            }
            }  
    } 
    else
    {
          return Redirect::to('login-signup');
    }   
     }

     public function insertreview(Request $request)
     {  
        if(Session::has('customerdata.user_id')) 
        { 
               $userid  = Session::get('customerdata.user_id');
               /*$validatedData = $request->validate([
                  'comment'=>'required'       
               ]);*/ 
               $updated = '';
               $inserted = '';
               $data = $request->all();
               $updatID = $data['commentId'];
               $singlereview  = DB::table('nm_review')->where('comment_id',$updatID)->count();
               if($singlereview > 0)
               {
                 //$updated = DB::table('nm_review')->where('comment_id',$updatID)->update(['comments'=>$data['comment'],'ratings'=>$data['rating'],'status'=>0]);
               } 
               else
               {
                 $shop_id = $data['shop_id']; 
                 if(isset($data['worker_id']) && $data['worker_id']!='')
                 {
                    $workerid = $data['worker_id'];
                 }
                 else
                 {
                    $workerid = '';
                 }
                 if(isset($data['rating3']) && $data['rating3']!='')
                 {
                  if($data['review_type3']==''){
                    $rtype3='shop';
                  }else{
                    $rtype3=$data['review_type3'];
                  }
                  DB::table('nm_review')->insert(array(
                                'comments'     => $data['wcomment'],
                                'worker_id'    => $workerid,
                                'product_id'   => $data['product_id'],
                                'order_id'     => $data['order_id'],
                                'shop_id'      => $shop_id,
                                'vandor_id'    => $data['vendor_id'],
                                'review_type'  => $rtype3,
                                'customer_id'  => $userid,
                                'ratings'      => $data['rating3'],
                                'parentId'     => $updatID)); 
                 }
                 if($data['review_type']==''){
                    $rtype='shop';
                  }else{
                    $rtype=$data['review_type'];
                  }
                 $inserted = DB::table('nm_review')->insert(array(
                                'comments'     => $data['comment'],
                                'worker_id'    => $workerid,
                                'product_id'   => $data['product_id'],
                                'order_id'     => $data['order_id'],
                                'shop_id'      => $shop_id,
                                'vandor_id'    => $data['vendor_id'],
                                'review_type'  => $rtype,
                                'customer_id'  => $userid,
                                'ratings'      => $data['rating'],
                                'parentId'     => $updatID)); 
               }           
               
                if($inserted)
                {

                  // language for display message //
                  if(\Config::get('app.locale') == 'ar')
                  {
                     Session::flash('message', "تمت إضافة المراجعة بنجاح");
                  }
                  else
                  {
                     Session::flash('message', "Review added successfully.");
                  }
                  // language for display message // 
                }
                elseif($updated)
                {
                  // language for display message //
                  if(\Config::get('app.locale') == 'ar')
                  {
                     Session::flash('message', "تمت إضافة المراجعة بنجاح");
                  }
                  else
                  {
                     Session::flash('message', "Review updated successfully.");
                  }
                  // language for display message // 
                }
                
                return Redirect::back();  
        } 
        else
        {
          return Redirect::to('login-signup');
        }   
     }  

       public function ocassionmoreimage($id)
       { 
          if(Session::has('customerdata.user_id')) 
          { 
            $userid  = Session::get('customerdata.user_id');  
            $getImagelist = DB::table('nm_occasions')->where('posted_by',$userid)->where('id',$id)->first();
            $getOccasionImage = DB::table('nm_occasions_gallery')->where('occasion_id',$id)->get();

            return view('ocassion-more-image',compact('getOccasionImage','getImagelist')); 
          } 
          else
          {
                return Redirect::to('login-signup');
          }   
       }

     public function deletemyoccasionimagewithajax() 
     {
        $id = $_REQUEST['occasion_id'];
        $getpath  = DB::table('nm_occasions')->where('id',$id)->first();
        $dtRecord = DB::table('nm_occasions')->where('id',$id)->delete();
        if($dtRecord)
        {
          $getpathmore = DB::table('nm_occasions_gallery')->where('occasion_id',$id)->get();
          foreach($getpathmore as $value)
          {
            $fullurlmore = explode('-',$value->image_url);
            @unlink('public/studio_img/'.$fullurlmore[1]);
            @unlink('public/studio_img/thumb-'.$fullurlmore[1]);
            DB::table('nm_occasions_gallery')->where('occasion_id',$id)->delete();
          }          
          
          $fullurl = explode('-',$getpath->images);
          @unlink('public/studio_img/'.$fullurl[1]);
          @unlink('public/studio_img/thumb-'.$fullurl[1]);            
          echo $id;
        }
     }
 

     public function changepassimagewithajax() 
     {
        $userid    = Session::get('customerdata.user_id');
        $password  = $_REQUEST['password'];
        $user = DB::table('nm_customer')->where('cus_id',$userid)->first();
        if(!Hash::check($password, $user->password))
        {
            if(\Config::get('app.locale') == 'ar')
            {
                echo '<span style="color:red;">كلمة المرور لا تتطابق مع كلمة مرور قاعدة البيانات</span>';
            } 
            else
            {
                echo '<span style="color:red;">Please enter correct password.</span>';
            }               
        }
     }


     public function deletemyoccasiononeimagewithajax() 
     {
        $id = $_REQUEST['id'];
        $getpath  = DB::table('nm_occasions_gallery')->where('id',$id)->first();
        $dtRecord = DB::table('nm_occasions_gallery')->where('id',$id)->delete();
        if($dtRecord)
        {
          $fullurl = explode('-',$getpath->image_url);
          @unlink('public/studio_img/'.$fullurl[1]);
          @unlink('public/studio_img/thumb-'.$fullurl[1]);            
          echo $id;
        }
     }
     
     public function orderDetails(Request $request) 
     {
        if(Session::has('customerdata.user_id')) 
        {         
         $order_id      = $request->id;
         $userid        = Session::get('customerdata.user_id');
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->groupBy('nm_order_product.shop_id')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order.order_cus_id',$userid)->where('nm_order_product.showcartproduct','=',0)->where('nm_order_product.order_id',$order_id)->selectRaw('*,sum(nm_order_product.total_price) as sum')->get();  
        //dd($productdetails);       
        return view('my-order-detail',compact('productdetails','order_id'));  
        } else {
           return Redirect::back();  

        }             
     }



      public function invitationattendedlist(Request $request) 
     {
        if(Session::has('customerdata.user_id')) 
        {         
         $order_id      = $request->id;
         $userid        = Session::get('customerdata.user_id');
         $attenddetails = DB::table('nm_invitation_list')->orderBy('invite_name')->where('order_id',$order_id)->paginate(10)->appends(request()->query());;  
        //dd($productdetails);       
        return view('invitation-attended-list',compact('attenddetails','order_id'));  
        } else {
           return Redirect::back();  

        }             
     }




  public function updatewalletdashboard(Request $request)
  {
    if(Session::has('customerdata.user_id') && $request->_token!='' && (request()->amount!=''|| request()->amount!=0)) 
    {
            $userid  = Session::get('customerdata.user_id');                         
            $amount = request()->amount;    
            $returnURls = url('/').'/my-account-wallet-success';
            $returnErrorURls = url('/').'/my-account-wallet';
            $paygateway_url = "https://test.myfatoorah.com/pg/PayGatewayServiceV2.asmx";
            $user = 'testapi@myfatoorah.com';
            $password = 'E55D0';
            $code = "999999";
            
            //$checkvendorInfo = Merchant::where('mer_id',$userid)->first();

            $checkvendorInfo = DB::table('nm_customer')->where('cus_id',$userid)->first();
            //echo '<pre>';print_r($checkvendorInfo);die;
            $productName = 'Wallet Amount';
            $name = $checkvendorInfo->cus_name;
            $email = $checkvendorInfo->email;
            $phone = $checkvendorInfo->cus_phone;
            if($phone)
            {           
            $subscription_start_date = date("Y-m-d");
            $subscription_end_date = date("Y-m-d");
            $subscription_amount = $amount;
            $productdata = "";
            $productdata .= '<ProductDC>';
            $productdata .= '<product_name>'.$productName.'</product_name>';
            $productdata .= '<unitPrice>' . $amount . '</unitPrice>';
            $productdata .= '<qty>1</qty>';
            $productdata .= '</ProductDC>';
            // Soap xml
                $post_string = '<?xml version="1.0" encoding="windows-1256"?>
                <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
                <soap12:Body>
                <PaymentRequest xmlns="http://tempuri.org/">
                <req>
                <CustomerDC>
                <Name>' . $name . '</Name>
                <Email>' . $email . '</Email>
                <Mobile>' . $phone . '</Mobile>
                </CustomerDC>
                <MerchantDC>
                <merchant_code>' . $code . '</merchant_code>
                <merchant_username>' . $user . '</merchant_username>
                <merchant_password>' . $password . '</merchant_password>
                <merchant_ReferenceID>' . time() . '</merchant_ReferenceID>
                <ReturnURL>'.$returnURls.'</ReturnURL>
                <merchant_error_url>'.$returnErrorURls.'</merchant_error_url>
                </MerchantDC>
                <lstProductDC>' . $productdata . '</lstProductDC>
                <totalDC>
                <subtotal>' . $amount . '</subtotal>
                </totalDC>
                <paymentModeDC>
                <paymentMode>BOTH</paymentMode>
                </paymentModeDC>
                <paymentCurrencyDC>
                <paymentCurrrency>SAR</paymentCurrrency>
                </paymentCurrencyDC>
                </req>
                </PaymentRequest>
                </soap12:Body>
                </soap12:Envelope>';

            $soap_do = curl_init();
            curl_setopt($soap_do, CURLOPT_URL, $paygateway_url);
            curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
            curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($soap_do, CURLOPT_POST, true);
            curl_setopt($soap_do, CURLOPT_POSTFIELDS, $post_string);
            curl_setopt($soap_do, CURLOPT_HTTPHEADER, array(
            'Content-Type: text/xml; charset=utf-8',
            'Content-Length: ' . strlen($post_string)
            ));
            
            curl_setopt($soap_do, CURLOPT_USERPWD, $user . ":" . $password);
            $result = curl_exec($soap_do);
            $err = curl_error($soap_do);
            $file_contents = htmlspecialchars($result);
            curl_close($soap_do);
            $doc = new \DOMDocument();
            if ($doc != null) {
            $doc->loadXML(html_entity_decode($file_contents));
            $ResponseCode = $doc->getElementsByTagName("ResponseCode");
            $ResponseCode = $ResponseCode->item(0)->nodeValue;
            $paymentUrl = $doc->getElementsByTagName("paymentURL");
            $paymentUrl = $paymentUrl->item(0)->nodeValue;
            $referenceID = $doc->getElementsByTagName("referenceID");
            $referenceID = $referenceID->item(0)->nodeValue;
            $ResponseMessage = $doc->getElementsByTagName("ResponseMessage");
            $ResponseMessage = $ResponseMessage->item(0)->nodeValue;
             } else {
            // language for display message //
            if(\Config::get('app.locale') == 'ar'){
            Session::flash('message', "هناك خطأ ما");
            }
            else
            {
            Session::flash('message', "Something went wrong....");
            }
            // language for display message //
            return Redirect::back();
            }
            if($ResponseCode == 0)
            {
            $transaction_id = str_replace('=','',strstr($paymentUrl,'='));
            $status = 1;  

            DB::table('nm_wallet_transaction')->insert(['user_id'=>$userid,'vendor_id'=>0,'amount'=>$amount,'transaction_type'=>1,'status'=>1,'transction_id'=>$transaction_id]);

            //$totalamt = $checkvendorInfo->wallet+$amount;
            //DB::table('nm_customer')->where('cus_id',$userid)->update(['wallet'=>$totalamt]);
            echo '<script language="javascript">window.location.href="' . $paymentUrl . '";</script>';
            }
          }
          else
          {
            if(\Config::get('app.locale') == 'ar'){
            Session::flash('message', "يرجى تحديث تفاصيل الملف الشخصي");
            }
            else
            {
            Session::flash('message', "Please update profile details");
            }
            // language for display message //
            return Redirect::back();
          }
      
    }
    else
    {
      return Redirect::to('sitemerchant');   
    }

  }

  public function myaccountwalletsuccess(Request $request)
  {
   if (Session::has('customerdata.user_id') && $request->id!='') 
   {
      $userid          = Session::get('customerdata.user_id');   
      $id              = $request->id;   
      $status          = 2;     

      DB::table('nm_wallet_transaction')->where('user_id',$userid)->where('transction_id',$id)->update(['status' => $status]);

      $userwallet = DB::table('nm_customer')->where('cus_id',$userid)->first();
      $useramt    = $userwallet->wallet;

      $wallettransaction = DB::table('nm_wallet_transaction')->where('transction_id',$id)->where('user_id',$userid)->first();      
      $totalamt        = $useramt+$wallettransaction->amount;

      DB::table('nm_customer')->where('cus_id',$userid)->update(['wallet'=>$totalamt]);
   
      // language for display message //
      if (\Config::get('app.locale') == 'ar'){
      Session::flash('message', "المبلغ المضافة في محفظتك");
      }
      else
      {
      Session::flash('message', "Amount added in your wallet");
      }
      // language for display message //
      return Redirect::to('my-account-wallet');
      }
      else
      {
       return Redirect::to('my-account-wallet');
      } 
  }
//-- Display mywallet page end --//
} 


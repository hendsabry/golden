<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Models;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon;
use File;
use App\Reviewrating;
use App\Categorylist;
use Intervention\Image\ImageManagerStatic as Image; 
use Auth;
use Lang;

 

class MerchantPopularBandController extends Controller
{
      public function __construct(){
            parent::__construct();
            // set admin Panel language
            $this->middleware(function ($request, $next) {
                        $this->setLanguageLocaleMerchant();
                        Session::get('mer_lang_code');
                        return $next($request);
           });
     }
    
 
//Add Shop info

// Start For Popular band info
   
    public function popularBandInfo(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.popular-band.popular-band-info', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 
   
   // Store Popular band information

   public function addPopularsingerinfo(Request $request)
{

 if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
               
                $singer_name = $request->singer_name;
                $singer_name_ar = $request->singer_name_ar;                
                $singer_city = $request->singer_city;
                $about_singer = $request->about_singer;
                $about_singer_ar = $request->about_singer_ar;
                $tnc = $request->tnc;
                $singer_image = $request->singer_image;
                $city = $request->singer_city;
                $mc_status = $request->mc_status;


                if($file=$request->file('singer_image')){  

                $extension = $file->getClientOriginalExtension();
                $Adressimgname = rand(11111,99999).'_'.time().'.'.$extension;               
                $imageRealPath  =   $file->getRealPath();
                $thumbName      =   'thumb_'. $Adressimgname;       
                $img = Image::make($imageRealPath); // use this if you want facade style code
                $thumb_width = 150;
                list($width,$height) = getimagesize($imageRealPath);
          $thumb_height = ($thumb_width/$width) * $height;
          $img->resize($thumb_width,$thumb_height);


                $img->save('singer' . '/'. $thumbName);
                $file->move('singer/', $Adressimgname);
                $thumbNameurl = url('').'/singer/'.$thumbName;
                }
                else
                {
               $thumbNameurl =  $request->singer_image_url;
                }


                if($tnc=$request->file('tnc')){  
                  $tncname =   $tnc->getClientOriginalName(); 
                $extension = $tnc->getClientOriginalExtension();
                $tncs = rand(11111,99999).'_'.time().'.'.$extension;              
                $tnc->move('singer/certificate/', $tncs);
                $tncsurl = url('').'/singer/certificate/'.$tncs;
                }
                else
                {
               $tncsurl =  $request->tnc_url;
                 $tncname = $request->tnc_name;
                }
 if($tnc=$request->file('tnc_ar')){  
                  $tncname_ar =   $tnc->getClientOriginalName(); 
                $extension = $tnc->getClientOriginalExtension();
                $tncs_ar = rand(11111,99999).'_'.time().'.'.$extension;              
                $tnc->move('singer/certificate/', $tncs_ar);
                $tncsurl_ar = url('').'/singer/certificate/'.$tncs_ar;
                }
                else
                {
               $tncsurl_ar =  $request->tnc_url_ar;
                 $tncname_ar = $request->tnc_name_ar;
                }
 
                if($file=$request->file('address_image')){  

                $extension = $file->getClientOriginalExtension();
                $Adressimgname = rand(11111,99999).'_'.time().'.'.$extension;               
                $imageRealPath  =   $file->getRealPath();
                $thumbName      =   'thumb_'. $Adressimgname;       
                $img = Image::make($imageRealPath); // use this if you want facade style code
                
$thumb_width = 150;
                list($width,$height) = getimagesize($imageRealPath);
          $thumb_height = ($thumb_width/$width) * $height;
          $img->resize($thumb_width,$thumb_height);


                $img->save('singer' . '/'. $thumbName);
                $file->move('singer/', $Adressimgname);
                $thumbNameurls = url('').'/singer/'.$thumbName;
                }
                else
                {
               $thumbNameurls =  $request->address_image_name;
                }
              $address = $request->address;
              $address_ar = $request->address_ar;
              $google_map_url = $request->google_map_url;

                $getC = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_id', $id)->count();
                if($getC < 1){                    
              $Q =  DB::table('nm_music')->insert([
                ['category_id' => $id, 'vendor_id' => $mer_id,'name_ar' => $singer_name_ar,'name' => $singer_name,'about' => $about_singer,'about_ar'=>$about_singer_ar,'category_type'=>'band','status'=>$mc_status,'image'=>$thumbNameurl,'terms_conditions'=>$tncsurl,'city_id' => $city,'terms_condition_name' => $tncname,'address' => $address,'address_ar' => $address_ar,'terms_conditions_ar'=>$tncsurl_ar,'terms_condition_name_ar' => $tncname_ar,'google_map_url' => $google_map_url,'address_image' => $thumbNameurls],
                ]); 
                   $hid = DB::getPdo()->lastInsertId();
                }
                else
                {
                   
                
                DB::table('nm_music')->where('category_type','band')->where('vendor_id',$mer_id)->where('category_id',$id)->update( ['name_ar'=>  $singer_name_ar,'name' => $singer_name,'about' => $about_singer,'status'=>$mc_status,'about_ar'=>$about_singer_ar,'image'=>$thumbNameurl,'terms_conditions'=>$tncsurl,'city_id' => $city,'terms_condition_name' => $tncname,'address' => $address,'terms_conditions_ar'=>$tncsurl_ar,'terms_condition_name_ar' => $tncname_ar,'google_map_url' => $google_map_url,'address_image' => $thumbNameurls,'address_ar' => $address_ar,]); 
 
                }
                


                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();
 


 

                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تمت إضافة الخدمة");
                    }
                    else
                    {
                    Session::flash('message', "Info successfully saved");
                    }
                    // language for display message // 

            return redirect()->route('popular-band-info',['id' => $id,'hid' => $hid]); 
                      
            } else {
                 return Redirect::to('sitemerchant');
            }

} 



   public function popularBandOrderList(Request $request)
     {
        if(Session::has('merchantid')) 
        {
                $mer_id         = Session::get('merchantid');
                $id             = $request->id;
                $hid            = $request->hid;
                $searchkeyword  = $request->searchkeyword;
                $date_to        = $request->date_to;
                $End_Date       = $request->from_to;
                $order_days     = $request->order_days;
                $status         = $request->status;                
                $serachfirstfrm = $request->serachfirstfrm;

                  $getorderedproducts = DB::table('nm_order_product')->where('product_type','band')->where('product_sub_type','band');
                  if($searchkeyword!=''){
                  $getorderedproducts = $getorderedproducts->where('order_id',$searchkeyword);
                  }

                  if($status!=''){
                  $getorderedproducts = $getorderedproducts->where('status',$status);
                  } 

                  if($date_to!='' && $End_Date!=''){
                  $getorderedproducts = $getorderedproducts->whereDate('created_at', '>=', $date_to)->whereDate('created_at', '<=', $End_Date);
                  } 

                  $getorderedproducts = $getorderedproducts->where('merchant_id',$mer_id)->groupBy('order_id')->orderBy('created_at','DESC')->selectRaw('*,sum(nm_order_product.total_price) as sum')->get();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                
                return view('sitemerchant.popular-band.popular-band-order-list', compact('merchantheader','merchantfooter','id','hid','getorderedproducts','mer_id'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }

    public function getorderdetailpopularband(Request $request)
    {
     if(Session::has('merchantid')) 
     {
         $mer_id         = Session::get('merchantid');
         $id             = $request->id;
         $hid            = $request->hid;
         $aid            = $request->aid;
         $cusid          = $request->cusid;
         $orderid        = $request->orderid;
         $merchantheader = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
         $merchantfooter = view('sitemerchant.includes.merchant_footer');
         
         $productdata = DB::table('nm_order_product')->where('product_type','band')->where('order_id',$orderid)->where('product_sub_type','band')->where('cus_id',$cusid)->where('order_id',$orderid)->orderBy('created_at','DESC')->get();       
        return view('sitemerchant.popular-band.order-detailpopularband', compact('merchantheader','merchantfooter','id','hid','aid','cusid','orderid','productdata'));       
    } 
    else 
    {
         return Redirect::to('sitemerchant');
    }
  }


    public function popularBandPhotovideo(Request $request)
     {
          if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 

              $getDbC = DB::table('nm_music_gallery')->where('vendor_id', $mer_id)->where('music_id', $hid)->get();

             $getVideos = DB::table('nm_music')->where('vendor_id', $mer_id)->where('id', $hid)->first();
           return view('sitemerchant.popular-band.popular-band-photo-video', compact('merchantheader','merchantfooter','id','getDbC','getVideos'));       
            } else {
                return Redirect::to('sitemerchant');
            }
       }  


       public function addPopularpicture(Request $request)
  {
           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;

                 if(!$request->file('hallpics') && trim($request->youtubevideo)=='' &&   trim($request->about)==''){
                $validatedData = $request->validate([
                'youtubevideo' => 'required', 
                ]);
                }


                
                $youtubevideo = str_replace('youtu.be','www.youtube.com/embed',$request->youtubevideo);
                if($files=$request->file('hallpics')){
                foreach($files as $file){
                $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());            
                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111,99999).'_'.time().'.'.$extension;                        
                $imageRealPath  =   $file->getRealPath();
                $thumbName      =   'thumb_'. $fileName;       
                $img = Image::make($imageRealPath); // use this if you want facade style code
                $thumb_width = 100;
                list($width,$height) = getimagesize($imageRealPath);
          $thumb_height = ($thumb_width/$width) * $height;
          $img->resize($thumb_width,$thumb_height);
                $img->save('singer' . '/images/'. $thumbName);
                $file->move('singer/images/', $fileName);
                $thumbNameUrl = url('').'/singer/images/'.$thumbName;
                DB::table('nm_music_gallery')->insert([
                ['vendor_id' => $mer_id,'image' => $thumbNameUrl,'music_id' => $hid],
                ]); 
                }
                }
                $about = $request->about;
                $about_ar = $request->about_ar;
               
                DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_id', $id)->update( ['video_url'=>  $youtubevideo,'video_description'=>  $about,'video_description_ar'=>  $about_ar]); 
              

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                // language for display message //
                if (\Config::get('app.locale') == 'ar'){
                Session::flash('message', "تمت إضافة الخدمة");
                }
                else
                {
                Session::flash('message', "Record saved successfully");
                }
                // language for display message // 

                return Redirect::back();
                      
            } else {
                 return Redirect::to('sitemerchant');
            }

} 
     
    




    public function popularBandReview(Request $request)
     {
         
       $getPagelimit = config('app.paginate');
        if (Session::has('merchantid')) 
        {
        $merid  = Session::get('merchantid'); 
        $hid = $request->hid;
        $this->setLanguageLocaleMerchant();
        $mer_id              = Session::get('merchantid');             
        $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
        $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
        $reviewrating = Reviewrating::where('vandor_id',$mer_id)->where('product_id',$hid)->where('review_type','shop')->paginate($getPagelimit)->appends(request()->query());
        return view('sitemerchant.popular-band.popular-band-comments-review', compact('merchantheader','merchantfooter','reviewrating'));       
        } else {
        return Redirect::to('sitemerchant');
        }
        
     }


    public function popularbandquotes(Request $request)
     {
       
             if (Session::has('merchantid')) 
             {
                $getPagelimit = config('app.paginate');
                 $mer_id   = Session::get('merchantid');
                  $id = $request->id;
                  $hid = $request->hid;
                  $getPagelimit = config('app.paginate');
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');
                 $getusers = DB::table('nm_services_inquiry')
            ->join('nm_customer', 'nm_services_inquiry.user_id', '=', 'nm_customer.cus_id')
            ->select('nm_customer.cus_name','nm_customer.email','nm_customer.cus_phone','nm_services_inquiry.created_at','nm_services_inquiry.language_type','nm_services_inquiry.id','nm_services_inquiry.status')
            ->where('nm_services_inquiry.merchant_id', $mer_id)
             ->where('nm_services_inquiry.quote_for','band')->orderBy('nm_services_inquiry.id','DESC');;
           
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');

           

           
            if(isset($_REQUEST['acceptance']) && $_REQUEST['acceptance']!='')
            {
            $srchs = $_REQUEST['acceptance'];
             $getusers =   $getusers ->where('nm_services_inquiry.status', '=', $srchs);
            }
            if(isset($_REQUEST['search']) && $_REQUEST['search']!='')
            {
                $srch = $_REQUEST['search'];
            $getusers =   $getusers ->where('nm_customer.cus_name','like', '%'.$srch.'%');
            }

            if(isset($_REQUEST['status']) && $_REQUEST['status']!='')
            {  $status = $_REQUEST['status'];
               $getusers =   $getusers ->where('nm_services_inquiry.status', $status);   
            }

           $getusers = $getusers->paginate($getPagelimit)->appends(request()->query());
 


           return view('sitemerchant.popular-band.popular-band-quoted-requested-list', compact('merchantheader','merchantfooter','id','getusers'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
        
     }
 

    public function popularbandquoteview(Request $request)
     {
       if(Session::has('merchantid')) 
       {
                $mer_id   = Session::get('merchantid');
                 $id = $request->id;
                $hid = $request->hid;
                $itmid = $request->itmid;
                 $autoid = $request->autoid;
               $getCities = DB::table('nm_city')->where('ci_status',1)->orderBy('ci_name','ASC')->get();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');
             $getquote  = DB::table('nm_vendor_comment')->where('id',$autoid)->first();
             $getusers = DB::table('nm_services_inquiry')
            ->join('nm_customer', 'nm_services_inquiry.user_id', '=', 'nm_customer.cus_id')
            ->select('nm_customer.cus_name','nm_customer.email','nm_customer.cus_phone','nm_services_inquiry.created_at','nm_services_inquiry.location','nm_services_inquiry.city_id','nm_services_inquiry.hall','nm_services_inquiry.user_comment','nm_services_inquiry.occasion_type','nm_services_inquiry.duration','nm_services_inquiry.time','nm_services_inquiry.date','nm_services_inquiry.status','nm_services_inquiry.language_type','nm_services_inquiry.user_id','nm_services_inquiry.id')
            ->where('nm_services_inquiry.id',  $itmid)
            ->where('nm_services_inquiry.merchant_id',$mer_id)
            ->first();

            return view('sitemerchant.popular-band.popular-band-quoted-requested-list-view', compact('merchantheader','merchantfooter','id','hid','itmid','getusers','getCities','getquote','autoid'));       
            } else {
                return Redirect::to('sitemerchant');
            }
       
      }
   
 	// for displaying comment and reviews
	public function singerCommentsReview()
	{
		 $getPagelimit = config('app.paginate');
		if (Session::has('merchantid')) 
		{
		$merid  = Session::get('merchantid');
		$this->setLanguageLocaleMerchant();
		$mer_id              = Session::get('merchantid');             
		$merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
		$merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
		$reviewrating = Reviewrating::where('vandor_id',$mer_id)->paginate($getPagelimit)->appends(request()->query());
		return view('sitemerchant.singer.singer-comments-review', compact('merchantheader','merchantfooter','reviewrating'));       
		} else {
		return Redirect::to('sitemerchant');
		}


	}
 	// for displaying Quoted Requested List 
 public function singerQuotedRequestedList(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');

            $getusers = DB::table('nm_services_inquiry')
            ->join('nm_customer1', 'nm_services_inquiry.user_id', '=', 'nm_customer.cus_id')
            ->select('nm_customer.cus_name','nm_customer.email','nm_customer.cus_phone','nm_services_inquiry.created_at','nm_services_inquiry.id')
            ->where('nm_services_inquiry.merchant_id', $mer_id)
            ->get();


           return view('sitemerchant.popular-band.popular-band-quoted-requested-list', compact('merchantheader','merchantfooter','id','getusers'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }  


      public function storeCommentBand(Request $request)
      {
             $customercurrency = 'SAR';
             $enqueryid = $request->enqueryid;
             $comment = $request->comments;
             $catid = $request->catid;
             $hid = $request->hid;
             $itmid = $request->itmid;
             $user_id = $request->userid;
             $price = $request->price;
             $customer_name = $request->customer_name;
             $mer_id   = Session::get('merchantid');
             $autoid = $request->autoid;
             $languagetype = $request->languagetype;
            
             $email = $request->email;



             if($autoid!=''){

               DB::table('nm_services_inquiry')->where('id',$itmid)->update([
                'status' => '2'
                ]);
               $Q =  DB::table('nm_vendor_comment')->where('id',$autoid)->update([
                'cat_id' => $catid, 
                'vendor_id' => $mer_id,
                'user_id' => $user_id,
                'enq_id' => $itmid,
                'comment' => $comment,
                'price' => $price,
                'type' => 'singer',
                'status' => '2'
                ]);

                 /*$email = 'himanshu@wisitech.com';
                 $username = $request->customername;
                 $comment = $request->comments;
                 $price = $request->price;
                Mail::send('emails.vendor-band-reply', ['username' => $username,'languagetype' => $languagetype,'comment'=>$comment,'price'=>$price], function ($m) use ($username,$comment,$price,$email,$languagetype) {
                 $m->from('info@goldencages.com', 'Goldencages');
                 $m->to($email, $username)->subject('Band Pricing Quote');
         
                  });
                  */
                $subject = 'Popular Band Pricing Quote is Successfully Approval ! Golden Cage'; 
                $status = 'Approved';

                $email    = $request->email;
                $username = $request->customername;
                $comment  = $request->comments;
                $price    = $customercurrency.' '.$request->price;
                                
                Mail::send('emails.vendor-singer-reply',
                array(
                'username' => $username,
                'languagetype' => $languagetype,
                'comment' => $comment,
                'price' => $price,
                'status' => $status
                ), function($message) use($email,$subject)
                {
                  $message->to($email,'Golden Cage')->subject($subject);
                }); 

            


             } else {

             DB::table('nm_services_inquiry')->where('id',$itmid)->update([
                'status' => '2'
                ]);

             $Q =  DB::table('nm_vendor_comment')->insert([
                'cat_id' => $catid, 
                'vendor_id' => $mer_id,
                'user_id' => $user_id,
                'enq_id' => $itmid,
                'comment' => $comment,
                'price' => $price,
                'type' => 'singer',
                'status' => '2'
                ]);


                $subject = 'Popular Band Pricing Quote is Successfully Approval ! Golden Cage'; 
                $status = 'Approved';

                $email    = $request->email;
                $username = $request->customername;
                $comment  = $request->comments;
                $price    = $customercurrency.' '.$request->price;
                                
                Mail::send('emails.vendor-singer-reply',
                array(
                'username' => $username,
                'languagetype' => $languagetype,
                'comment' => $comment,
                'price' => $price,
                'status' => $status
                ), function($message) use($email,$subject)
                {
                  $message->to($email,'Golden Cage')->subject($subject);
                }); 
                 
         }
            //Mail Content


         //Mail Implementations Code

            if($autoid =='') {
           
             $autoid = DB::getPdo()->lastInsertId();

             
        }



                  // language for display message //
                if (\Config::get('app.locale') == 'ar'){
                Session::flash('message', "ارسل التعليق بنجاح");
                }
                else
                {
                Session::flash('message', "Comment successfully sent");
                }
                // language for display message //
               return redirect()->route('popular-band-quoted-requested-list-view',['id' => $catid,'hid' => $hid,'itmid' =>$itmid,'autoid'=>$autoid]); 
          

      }

                 
            
            

      }
   




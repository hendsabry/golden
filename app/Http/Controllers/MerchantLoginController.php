<?php

namespace App\Http\Controllers;

use DB;

use Session;

use App\Http\Models;

use App\Register;

use App\Home;

use App\Footer;

use App\Settings;

use App\Merchant;

use App\Blog;

use App\Dashboard;

use App\Admodel;

use App\Deals;

use App\Auction;

use App\Customer;

use App\Transactions;

use App\Merchantadminlogin;

use App\Merchantproducts;

use Lang;

use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Validator;

class MerchantloginController extends Controller

{

    

    /*

    |--------------------------------------------------------------------------

    | Default Home Controller

    |--------------------------------------------------------------------------

    |

    | You may wish to use controllers instead of, or in addition to, Closure

    | based routes. That's great! Here is an example controller method to

    | get you started. To route to this controller, just add the route:

    |

    |	Route::get('/', 'HomeController@showWelcome');

    |

    */

    

	public function __construct(){

        parent::__construct();

       

        // set admin Panel language

      



                     $this->middleware(function ($request, $next) {

                    $this->setLanguageLocaleFront();

                    return $next($request);





    });

       

    }					 

    public function merchant_login()

    {

       if (Session::has('merchantid')) {

         return redirect::to('sitemerchant_dashboard');

       }else{

         return view('sitemerchant.merchant_login');

       }

    }

    

    public function merchant_login_check()
    {
        $inputs = Input::all();
        $username = Input::get('mer_user');
        $pwd      = Input::get('mer_pwd');
        if($username=='')
        {
            return Redirect::to('sitemerchant')->with('error', 'Enter an merchant Id!');
        }
        if($pwd=='')
        {
          return Redirect::to('sitemerchant')->with('error', 'Enter a Password!');  
        }
        $logincheck = Merchantadminlogin::checkmerchantlogin($username,$pwd);

        if ($logincheck == 1) 

        {

             /*if (Lang::has(Session::get('mer_lang_file').'.MER_LOGIN_SUCCESS')!= '')

 			{ 

				$session_message =  trans(Session::get('mer_lang_file').'.MER_LOGIN_SUCCESS');

			}  

			else 

			{ 

				$session_message =  trans($this->OUR_LANGUAGE.'.MER_LOGIN_SUCCESS');

			}*/

            return Redirect::to('vendor-dashboard')->with('success', 'Login Success');

        } else if($logincheck == -1) {

             /*if (Lang::has(Session::get('mer_lang_file').'.MER_INVALID_USERNAME_AND_PASSWORD')!= '')

 			{ 

				$session_message =  trans(Session::get('mer_lang_file').'.MER_INVALID_USERNAME_AND_PASSWORD');

			}  

			else 

			{ 

				$session_message =  trans($this->OUR_LANGUAGE.'.MER_INVALID_USERNAME_AND_PASSWORD');

			}*/

            return Redirect::to('sitemerchant')->with('error', 'Invalid UserName');

        }

		 else if($logincheck == -2)

		 {

			 return Redirect::to('sitemerchant')->with('error', 'Invalid Password')->withInput();

		 }

		 else

	  {

		   

		   return Redirect::to('sitemerchant')->with('login_error','User not available');

	  }

        

    }



    

    public function merchant_logout()

    {

         

        Session::forget('merchantid');

        Session::forget('merchantname');

		$mer_lang_file = Session::get('mer_lang_file');

		

        Session::flush();

		

		 if (Lang::has(Session::get('mer_lang_file').'.MER_LOGOUT_SUCCESS')!= '')

 			{ 

				$session_message =  trans(Session::get('mer_lang_file').'.MER_LOGOUT_SUCCESS');

			}  

			else 

			{ 

				$session_message =  '';

			}

        return Redirect::to('sitemerchant');

    }

    

    public function merchant_forgot_check()

    {

		/* print_r($_POST);die; */

        $inputs         = Input::all();

        $merchant_email = Input::get('merchant_email'); 

        

       $encode_email = base64_encode(base64_encode(base64_encode(($merchant_email))));

        

       $check_valid_email = Merchantadminlogin::checkvalidemail($merchant_email);

        

        if (isset($check_valid_email[0])) {

            $forgot_check = Merchantadminlogin::forgot_check_details_merchant($merchant_email);           

            $name = 'merchant';          

            $send_mail_data = array(

                'name' => $forgot_check[0]->mer_fname,

                'email' => $merchant_email,

                'encodeemail' => $encode_email

            );



            # It will show these lines as error but no issue it will work fine Line no 119 - 122

            Mail::send('emails.merchant_passwordrecoverymail', $send_mail_data, function($message)

            {

				 if (Lang::has(Session::get('mer_lang_file').'.MER_PASSWORD_RECOVERY_DETAILS')!= '')

 			{ 

				$session_message =  trans(Session::get('mer_lang_file').'.MER_PASSWORD_RECOVERY_DETAILS');

			}  

			else 

			{ 

				$session_message =  trans($this->OUR_LANGUAGE.'.MER_PASSWORD_RECOVERY_DETAILS');

			}

                $message->to(Input::get('merchant_email'))->subject($session_message);

            });

			 if (Lang::has(Session::get('mer_lang_file').'.MER_MAIL_SEND_SUCCESSFULLY')!= '')

 			{ 

				$session_message =  trans(Session::get('mer_lang_file').'.MER_MAIL_SEND_SUCCESSFULLY');

			}  

			else 

			{ 

				$session_message =  trans($this->OUR_LANGUAGE.'.MER_MAIL_SEND_SUCCESSFULLY');

			}

			#sathyaseelan

			//echo $session_message.":0";

             return Redirect::to('sitemerchant')->with('login_success', $session_message ); 

        } else {

			 if (Lang::has(Session::get('mer_lang_file').'.MER_INVALID_EMAIL')!= '')

 			{ 

				$session_message =  trans(Session::get('mer_lang_file').'.MER_INVALID_EMAIL');

			}  

			else 

			{ 

				$session_message =  trans($this->OUR_LANGUAGE.'.MER_INVALID_EMAIL');

			}

			#sathyaseelan

			//echo $session_message.":1";

             return Redirect::to('sitemerchant')->with('forgot_error', $session_message); 

            

        }

     

    }

    

    public function forgot_pwd_email($email)

    {

        $merchat_decode_email = base64_decode(base64_decode(base64_decode($email)));

        

        $merchantdetails = Merchantadminlogin::get_merchant_details($merchat_decode_email);

        

        return view('sitemerchant.forgot_pwd_mail')->with('merchantdetails', $merchantdetails);

        

    }

    

    public function forgot_pwd_email_submit()

    {

        $inputs      = Input::all();

        $merchant_id = Input::get('merchant_id');

        $pwd         = Input::get('pwd');

        $confirmpwd  = Input::get('confirmpwd');

        

        Merchantadminlogin::update_newpwd($merchant_id, $confirmpwd);

         if (Lang::has(Session::get('mer_lang_file').'.MER_PASSWORD_CHANGED_SUCCESSFULLY')!= '')

 			{ 

				$session_message =  trans(Session::get('mer_lang_file').'.MER_PASSWORD_CHANGED_SUCCESSFULLY');

			}  

			else 

			{ 

				$session_message =  trans($this->OUR_LANGUAGE.'.MER_PASSWORD_CHANGED_SUCCESSFULLY');

			}

        return Redirect::to('sitemerchant')->with('login_success', $session_message);

        

    }

}


<?php
namespace App\Http\Controllers;
use DB;
use Session;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\MerchantDeal;
use App\Auction;
use App\Customer;
use App\Transactions;
use App\Merchantadminlogin;
use App\Merchantproducts;
use App\Merchantsettings;
use App\Merchantdeals;
use Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
class MerchantdealsController extends Controller
{
    
    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */
   public function __construct(){
        parent::__construct();
       
        // set admin Panel language
        $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleMerchant();
                    return $next($request);
    });
       
    }
    public function add_deals()
    {
		
        if(Session::has('merchantid')){
        $merchant_id    = Session::get('merchantid');
		 if (Lang::has(Session::get('mer_lang_file').'.MER_DEALS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_DEALS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_DEALS');
			}
        $adminheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', $session_message);
        $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_deals');
        $adminfooter    = view('sitemerchant.includes.merchant_footer');
        $category       = Merchantdeals::get_category();
        $get_mer_store  = Merchant::view_store_details($merchant_id);
        
        return view('sitemerchant.add_deals')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('action', 'save')->with('category', $category)
        ->with('get_mer_store',$get_mer_store);
        }else{
             return Redirect::to('sitemerchant');
        }
    }
    
    public function add_deals_submit()
    {

		$get_active_lang = $this->get_active_language;
        $count            = Input::get('count');
        $filename_new_get = "";
			$file = Input::file('file');
			
			$count = count($file);
			$data = Input::except(array(
                '_token'
            ));
			for ($i=0; $i < $count; $i++)
			{
				$files = $file[$i];
				$input = array(
				'deal_image' => $file[$i]
				);
				$rules = array(
			
				'deal_image' => 'required|image|mimes:jpeg,png,jpg,gif|image_size:'.$this->deal_width.','.$this->deal_height.''
				);
				$validator = Validator::make($input, $rules);
			}
            if ($validator->fails())
            {
                return Redirect::to('mer_add_deals')->withErrors($validator->messages())->withInput();
            }
			 $lang_entry_vali = array();
			/* if(!empty($get_active_lang)) { 
				foreach($get_active_lang as $get_lang) {
					$get_lang_name = $get_lang->lang_code;
					$lang_entry_vali[] = array(
					'title_'.$get_lang_name.'' => Input::get('title_'.$get_lang_name),
					'description_'.$get_lang_name.'' => Input::get('description_'.$get_lang_name),
					'metakeyword_'.$get_lang_name.'' => Input::get('metakeyword_'.$get_lang_name),
					'metadescription_'.$get_lang_name.'' => Input::get('metadescription_'.$get_lang_name)
					); 
				}
			}  */
			
			$input1=array(
			'title' => Input::get('title'),
			
            'category' => Input::get('category'),
            'maincategory' => Input::get('maincategory'),
            'originalprice' => Input::get('originalprice'),
            'discountprice' => Input::get('discountprice'),
			
			'startdate' => Input::get('startdate'),
            'enddate' => Input::get('enddate'),
			'description' => Input::get('description'),
			'maxlimit' => Input::get('maxlimit')
			);
           /*echo  $startdate = Input::get('startdate');
           echo  $enddate = Input::get('enddate');

           echo '<br> '.$startdate_formated = date('Y-m-d H:i:s', strtotime(Input::get('startdate')));
           echo '<br> '.$enddate_formated = date('Y-m-d H:i:s', strtotime(Input::get('enddate')));
           exit();*/
			#sathyaseelan
			if(!empty($get_active_lang))
			{ 
				foreach($get_active_lang as $get_lang)
				{
					$get_lang_name = $get_lang->lang_name;

					$lang_entry_vali = array(
					'title_'.$get_lang_name.'' => Input::get('title_'.$get_lang_name),
					'description_'.$get_lang_name.'' => Input::get('description_'.$get_lang_name),
					'metakeyword_'.$get_lang_name.'' => Input::get('metakeyword_'.$get_lang_name),
					'metadescription_'.$get_lang_name.'' => Input::get('metadescription_'.$get_lang_name)
					); 
					$input1=array_merge($input1,$lang_entry_vali);
				}
			}
			$rules1 = array(
				'title' => 'Required',
				'category' => 'Required|not_in:0',
				'maincategory' => 'Required|not_in:0',
				'originalprice' => 'Required|numeric',
				'discountprice' => 'Required|numeric',
				'startdate' => 'Required|date',
				'enddate' => 'Required|date|after:startdate',
				'description' => 'Required',
				'maxlimit' => 'Required|numeric'
			);
			#sathyaseelan
			if(!empty($get_active_lang))
			{ 
				foreach($get_active_lang as $get_lang)
				{
					$get_lang_name = $get_lang->lang_name;

					$rules1vali = array(
					'title_'.$get_lang_name.'' => 'Required',
					'description_'.$get_lang_name.'' => 'Required',
					'metakeyword_'.$get_lang_name.'' => 'Required',
					'metadescription_'.$get_lang_name.'' => 'Required'
					); 
					$rules1=array_merge($rules1,$rules1vali);
				}
			}
			
			//echo'<pre>';print_r($input1);print_r($rules1);die;
			$validator1 = Validator::make($input1, $rules1);
			if(!empty($get_active_lang)) { 
				foreach($get_active_lang as $get_lang) {
					$get_lang_name = $get_lang->lang_code;
				
					
					$rule['title_'.$get_lang_name] = 'required';
					$rule['description_'.$get_lang_name] = 'required';
					/* $rule['metakeyword_'.$get_lang_name] = 'required';
					$rule['metadescription_'.$get_lang_name] = 'required'; */
			
				}
			}
			
			if ($validator1->fails())
			{
				return Redirect::to('mer_add_deals')->withErrors($validator1->messages())->withInput();
			}
			if ($validator->fails())
			{
				return Redirect::to('mer_add_deals')->withErrors($validator->messages())->withInput();
			}


			//Product Policy
            $allow_cancel   = Input::get('allow_cancel');
            $allow_return   = Input::get('allow_return');
            $allow_replace  = Input::get('allow_replace');

            $cancel_policy  = (Input::get('cancellation_policy')!=NUll)?Input::get('cancellation_policy'):'';
            $return_policy  = (Input::get('return_policy')!=NUll)?Input::get('return_policy'):'';
            $replace_policy = (Input::get('replacement_policy')!=NUll)?Input::get('replacement_policy'):'';

            $cancel_days    = (Input::get('cancellation_days')!=NUll)?Input::get('cancellation_days'):'';
            $return_days    = (Input::get('return_days')!=NUll)?Input::get('return_days'):'';
            $replace_days   = (Input::get('replace_days')!=NUll)?Input::get('replace_days'):'';

            $rules = $inputs = $policy_lang_entry = array();

            if($allow_cancel=='1')
            {
                $cancel_rules = array('cancellation_policy' => 'required','cancellation_days' => 'required|numeric');
                $rules = array_merge($rules,$cancel_rules);
                $inputs = array_merge($inputs,array('cancellation_policy' =>$cancel_policy,'cancellation_days'=>$cancel_days ));
            }if($allow_return=='1')
            {
                $return_rules = array('return_policy' => 'required','return_days' => 'required|numeric');
                $rules = array_merge($rules,$return_rules);
                $inputs = array_merge($inputs,array('return_policy' =>$return_policy,'return_days'=>$return_days ));
            }if($allow_replace=='1')
            {
                $replace_rules = array('replace_policy' => 'required','replace_days' => 'required|numeric');
                $rules = array_merge($rules,$replace_rules);
                $inputs = array_merge($inputs,array('replace_policy' =>$replace_policy,'replace_days'=>$replace_days ));
            }

            $get_active_lang =  $this->get_active_language; 
                $p_lang_entry = array();
                if(!empty($get_active_lang)) { 
                    foreach($get_active_lang as $get_lang) {
                        $get_lang_name = $get_lang->lang_name;
                        $get_lang_code = $get_lang->lang_code;
                
                        $policy_lang_entry = array(
                        'cancel_policy_'.$get_lang_code => (Input::get('cancellation_policy_'.$get_lang_name)!=NUll)?Input::get('cancellation_policy_'.$get_lang_name):'',
                        'return_policy_'.$get_lang_code => (Input::get('return_policy_'.$get_lang_name)!=NUll)?Input::get('return_policy_'.$get_lang_name):'',
                        'replace_policy_'.$get_lang_code => (Input::get('replacement_policy_'.$get_lang_name)!=NUll)?Input::get('replacement_policy_'.$get_lang_name):'',
                        
                        );

                        if($allow_cancel=='1')
                        {
                            $cancel_rules = array('cancellation_policy_'.$get_lang_name => 'required','cancellation_days' => 'required|numeric');
                            $rules = array_merge($rules,$cancel_rules);
                            $inputs = array_merge($inputs,array('cancellation_policy_'.$get_lang_name =>$policy_lang_entry['cancel_policy_'.$get_lang_code ]));
                        }if($allow_return=='1')
                        {
                            $return_rules = array('return_policy_'.$get_lang_name => 'required','return_days' => 'required|numeric');
                            $rules = array_merge($rules,$return_rules);
                            $inputs = array_merge($inputs,array('return_policy_'.$get_lang_name =>$policy_lang_entry['replace_policy_'.$get_lang_code ]));
                        }if($allow_replace=='1')
                        {
                            $replace_rules = array('replace_policy_'.$get_lang_name => 'required','replace_days' => 'required|numeric');
                            $rules = array_merge($rules,$replace_rules);
                            $inputs = array_merge($inputs,array('replace_policy_'.$get_lang_name =>$policy_lang_entry['replace_policy_'.$get_lang_code ]));
                        }
                        $p_lang_entry  = array_merge($p_lang_entry,$policy_lang_entry);
                    }
                }
                

            
           // print_r($inputs);exit();

            $validator = Validator::make($inputs, $rules);
            
            if ($validator->fails())
            {
                return Redirect::to('mer_add_deals')->withErrors($validator->messages())->withInput();
            }


			foreach($file as $value)
			{
				$filename         = $value->getClientOriginalName();
				$move_img         = explode('.', $filename);
				//$time=time();
				$time=time().rand();
				$filename_new 	  = 'Deal_'.$time.'.' . strtolower($value->getClientOriginalExtension());
				$destinationPath  = './public/assets/deals/';
				Image::make($value)->save('./public/assets/deals/'.$filename_new,$this->image_compress_quality);
				$filename_new_get .= $filename_new . "/**/";
			}
			$file_name_insert = $filename_new_get;
        /*$filename_new_get = "";
        for ($i = 0; $i < $count; $i++) {
            $file_more          = Input::file('file_more' . $i);
            $file_more_name     = $file_more->getClientOriginalName();
            $move_more_img      = explode('.', $file_more_name);
            $filename_new       = $move_more_img[0] . str_random(8) . "." . $move_more_img[1];
            $newdestinationPath = './public/assets/deals/';
            $uploadSuccess_new  = Input::file('file_more' . $i)->move($newdestinationPath, $filename_new);*/
            //$filename_new_get .= $filename_new . "/**/";
        /*}
        $filename_new_get;
        $now                      = date('Y-m-d H:i:s');
        $inputs                   = Input::all();
        $file                     = Input::file('file');
        $filename                 = $file->getClientOriginalName();
        $move_img                 = explode('.', $filename);
        $filename                 = $move_img[0] . str_random(8) . "." . $move_img[1];
        $destinationPath          = './public/assets/deals/';
        $uploadSuccess            = Input::file('file')->move($destinationPath, $filename);*/
       // $file_name_insert         = $filename . "/**/" . $filename_new_get;

     $now                      = date('Y-m-d H:i:s');

        $deal_saving_price        = Input::get('originalprice') - Input::get('discountprice');

        $deal_discount_percentage = ($deal_saving_price / Input::get('originalprice') * 100);
        //$deal_discount_percentage = round(($deal_saving_price / Input::get('originalprice')) * 100, 2);
        $deal_discount_percentage= floor($deal_discount_percentage);
        
        $date1                    = date('m/d/Y');
        
        $Shipping_Amount = Input::get('Shipping_Amount');
            
            if ($Shipping_Amount == "") {
                
                $Shipping_Amount = 0;
            
            }

             $Product_SubCategory = "";
                  $Product_SecondSubCategory = "";

                 if(Input::get('subcategory') != ""){
                $Product_SubCategory = Input::get('subcategory');  
                }   
                if(Input::get('secondsubcategory') != ""){          
                $Product_SecondSubCategory = Input::get('secondsubcategory');  
                } 
        $inc_tax = Input::get('inctax');

        $entry  = array(
            'deal_title' => Input::get('title'),
			
            'deal_category' => Input::get('category'),
            'deal_main_category' => Input::get('maincategory'),
            'deal_sub_category' => $Product_SubCategory,
            'deal_second_sub_category' => $Product_SecondSubCategory,
            'deal_original_price' => Input::get('originalprice'),
            'deal_discount_price' => Input::get('discountprice'),
            'deal_discount_percentage' => $deal_discount_percentage,
            'deal_saving_price' => $deal_saving_price,
            'deal_inctax'       => $inc_tax,
            'deal_shippamt'     => $Shipping_Amount,

            'deal_start_date' => date('Y-m-d H:i:s', strtotime(Input::get('startdate'))),
            'deal_end_date' => date('Y-m-d H:i:s', strtotime(Input::get('enddate'))),
            'deal_expiry_date' => date('Y-m-d H:i:s', strtotime(Input::get('enddate'))),

            'deal_description' => Input::get('description'),
			
            'deal_merchant_id' => Input::get('merchant'),
            'deal_shop_id' => Input::get('shop'),
            'deal_meta_keyword' => Input::get('metakeyword'),
			
            'deal_meta_description' => Input::get('metadescription'),
			
           // 'deal_min_limit' => Input::get('minlimt'),
            'deal_max_limit' => Input::get('maxlimit'),
            'deal_image_count' => Input::get('count'),
            'deal_image' => $file_name_insert,
            'deal_posted_date' => $now,
            'created_date' => $date1,
             //product policy starts
            'allow_cancel'      => $allow_cancel,
            'allow_return'      => $allow_return,
            'allow_replace'     => $allow_replace,
            'cancel_policy'     => $cancel_policy,
            'return_policy'     => $return_policy,
            'replace_policy'    => $replace_policy,
            'cancel_days'       => $cancel_days,
            'return_days'       => $return_days,
            'replace_days'      => $replace_days,
            
            //product policy ends
        );
        $entry = array_merge($entry,$p_lang_entry); //Policy merge
		#sathyaseelan
		$lang_entry = array();
		if(!empty($get_active_lang))
		{ 
			foreach($get_active_lang as $get_lang)
			{
				$get_lang_name = $get_lang->lang_name;
				$get_lang_code = $get_lang->lang_code;

				$lang_entry = array(
				'deal_title_'.$get_lang_code.'' => Input::get('title_'.$get_lang_name),
				'deal_description_'.$get_lang_code.'' => Input::get('description_'.$get_lang_name),
				'deal_meta_keyword_'.$get_lang_code.'' => Input::get('metakeyword_'.$get_lang_name),
				'deal_meta_description_'.$get_lang_code.'' => Input::get('metadescription_'.$get_lang_name)
				); 
				$entry  = array_merge($entry,$lang_entry);
			}
		}
        $return = Merchantdeals::save_deal($entry);
        return Redirect::to('mer_manage_deals')->with('block_message', 'Deal Inserted Successfully');
    }
    
    public function edit_deals($id)
    {
        if(Session::has('merchantid')){
            $merchant_id      = Session::get('merchantid');
			if (Lang::has(Session::get('mer_lang_file').'.MER_DEALS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_DEALS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_DEALS');
			}
            $adminheader      = view('sitemerchant.includes.merchant_header')->with('routemenu', $session_message);
            $adminleftmenus   = view('sitemerchant.includes.merchant_left_menu_deals');
            $adminfooter      = view('sitemerchant.includes.merchant_footer');
            $category         = Merchantdeals::get_category();
            $merchant_details = Merchantdeals::get_merchant_details();
            $deal_list        = Merchantdeals::get_deals($id);
            $get_mer_store    = Merchant::view_store_details($merchant_id);
            return view('sitemerchant.edit_deals')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('action', 'update')->with('category', $category)->with('deal_list', $deal_list)->with('merchant_details', $merchant_details)
            ->with('get_mer_store',$get_mer_store);
        }else{
            return Redirect::to('sitemerchant');
        }
    }
    
    public function edit_deals_submit(Request $request)
    {
       
		$get_active_lang = $this->get_active_language;
        $now    = date('Y-m-d H:i:s');
		$id = Input::get('deal_edit_id');
		$files = Input::file('new_file');
		 
		
			$files_exit = Input::file('file');
			$filesExitCount = count($files_exit);
			$filesCount = count($files);
            $data = Input::except(array(
                '_token'
            ));
			if($files !=0)
			{
			for ($i=0; $i < $filesCount; $i++)
			{
				$file = $files[$i];
				$input = array(
				'deal_image' => $files[$i]
			);
			$rules = array(
			'deal_image' => 'required|image|mimes:jpeg,png,jpg,gif|image_size:'.$this->deal_width.','.$this->deal_height.''
				);
				$validator = Validator::make($input, $rules);
			}
			
			
					
			if ($validator->fails())
			{
				return Redirect::to('mer_edit_deals/'.$id)->withErrors($validator->messages())->withInput();
			}
			}

			//Product Policy
            $allow_cancel   = Input::get('allow_cancel');
            $allow_return   = Input::get('allow_return');
            $allow_replace  = Input::get('allow_replace');

            $cancel_policy  = (Input::get('cancellation_policy')!=NUll)?Input::get('cancellation_policy'):'';
            $return_policy  = (Input::get('return_policy')!=NUll)?Input::get('return_policy'):'';
            $replace_policy = (Input::get('replacement_policy')!=NUll)?Input::get('replacement_policy'):'';

            $cancel_days    = (Input::get('cancellation_days')!=NUll)?Input::get('cancellation_days'):'';
            $return_days    = (Input::get('return_days')!=NUll)?Input::get('return_days'):'';
            $replace_days   = (Input::get('replace_days')!=NUll)?Input::get('replace_days'):'';

            $rules = $inputs = $policy_lang_entry = array();

            if($allow_cancel=='1')
            {
                $cancel_rules = array('cancellation_policy' => 'required','cancellation_days' => 'required|numeric');
                $rules = array_merge($rules,$cancel_rules);
                $inputs = array_merge($inputs,array('cancellation_policy' =>$cancel_policy,'cancellation_days'=>$cancel_days ));
            }else{
                $cancel_policy ='';
                $cancel_days ='';

            }
            if($allow_return=='1')
            {
                $return_rules = array('return_policy' => 'required','return_days' => 'required|numeric');
                $rules = array_merge($rules,$return_rules);
                $inputs = array_merge($inputs,array('return_policy' =>$return_policy,'return_days'=>$return_days ));
            }else{
                $return_policy ='';
                $return_days ='';
                
            }
            if($allow_replace=='1')
            {
                $replace_rules = array('replace_policy' => 'required','replace_days' => 'required|numeric');
                $rules = array_merge($rules,$replace_rules);
                $inputs = array_merge($inputs,array('replace_policy' =>$replace_policy,'replace_days'=>$replace_days ));
            }else{
                $replace_policy ='';
                $replace_days ='';
                
            }

             $get_active_lang =  $this->get_active_language; 
                $p_lang_entry = array();
                if(!empty($get_active_lang)) { 
                    foreach($get_active_lang as $get_lang) {
                        $get_lang_name = $get_lang->lang_name;
                        $get_lang_code = $get_lang->lang_code;
                
                        $policy_lang_entry = array(
                        'cancel_policy_'.$get_lang_code => ($allow_cancel=='1'?((Input::get('cancellation_policy_'.$get_lang_name)!=NUll)?Input::get('cancellation_policy_'.$get_lang_name):''):''),
                        'return_policy_'.$get_lang_code => ($allow_return=='1'?((Input::get('return_policy_'.$get_lang_name)!=NUll)?Input::get('return_policy_'.$get_lang_name):''):''),
                        'replace_policy_'.$get_lang_code => ($allow_replace=='1'?((Input::get('replacement_policy_'.$get_lang_name)!=NUll)?Input::get('replacement_policy_'.$get_lang_name):''):''),
                        
                        );

                        if($allow_cancel=='1')
                        {
                            $cancel_rules = array('cancellation_policy_'.$get_lang_name => 'required','cancellation_days' => 'required|numeric');
                            $rules = array_merge($rules,$cancel_rules);
                            $inputs = array_merge($inputs,array('cancellation_policy_'.$get_lang_name =>$policy_lang_entry['cancel_policy_'.$get_lang_code ]));
                        }
                        if($allow_return=='1')
                        {
                            $return_rules = array('return_policy_'.$get_lang_name => 'required','return_days' => 'required|numeric');
                            $rules = array_merge($rules,$return_rules);
                            $inputs = array_merge($inputs,array('return_policy_'.$get_lang_name =>$policy_lang_entry['return_policy_'.$get_lang_code ]));
                        }if($allow_replace=='1')
                        {
                            $replace_rules = array('replace_policy_'.$get_lang_name => 'required','replace_days' => 'required|numeric');
                            $rules = array_merge($rules,$replace_rules);
                            $inputs = array_merge($inputs,array('replace_policy_'.$get_lang_name =>$policy_lang_entry['replace_policy_'.$get_lang_code ]));
                        }
                        $p_lang_entry  = array_merge($p_lang_entry,$policy_lang_entry);
                    }
                }
                
           // print_r($rules);exit();

            $validator = Validator::make($inputs, $rules);
            
            if ($validator->fails())
            {
                return Redirect::to('mer_edit_deals/'.$id)->withErrors($validator->messages())->withInput();
            }



				
			for ($j=0; $j < $filesExitCount; $j++)
			{
				
				if($files_exit[$j] !='')
				{
				$file = $files_exit[$j];
				$input = array(
				'upload_exit' => $files_exit[$j]
				);
				$rules = array(
					'upload_exit' => 'required|image|mimes:jpeg,png,jpg|image_size:'.$this->deal_width.','.$this->deal_height.''
				);
				$validator = Validator::make($input, $rules);
				
				if ($validator->fails())
				{
					return Redirect::to('mer_edit_deals/'.$id)->withErrors($validator->messages())->withInput();
				}
				}
			}



					$input1=array(
			'title' => Input::get('title'),
			
            'category' => Input::get('category'),
            'maincategory' => Input::get('maincategory'),
            'originalprice' => Input::get('originalprice'),
            'discountprice' => Input::get('discountprice'),
			
			'startdate' => Input::get('startdate'),
            'enddate' => Input::get('enddate'),
			'description' => Input::get('description'),
			'maxlimit' => Input::get('maxlimit')
			);
			if(!empty($get_active_lang))
			{ 
				foreach($get_active_lang as $get_lang)
				{
					$get_lang_name = $get_lang->lang_name;

					$lang_entry_vali = array(
					'title_'.$get_lang_name.'' => Input::get('title_'.$get_lang_name),
					'description_'.$get_lang_name.'' => Input::get('description_'.$get_lang_name),
					'metakeyword_'.$get_lang_name.'' => Input::get('metakeyword_'.$get_lang_name),
					'metadescription_'.$get_lang_name.'' => Input::get('metadescription_'.$get_lang_name)
					); 
					$input1=array_merge($input1,$lang_entry_vali);
				}
			}
			#sathyaseelan
			$lang_entry_vali=array();
			if(!empty($get_active_lang)) { 
				foreach($get_active_lang as $get_lang) {
					$get_lang_name = $get_lang->lang_name;
					$lang_entry_vali['title_'.$get_lang_name.''] = Input::get('title_'.$get_lang_name);
					$lang_entry_vali['description_'.$get_lang_name.''] = Input::get('description_'.$get_lang_name);
					$lang_entry_vali['metakeyword_'.$get_lang_name.''] = Input::get('metakeyword_'.$get_lang_name);
					$lang_entry_vali['metadescription_'.$get_lang_name.''] = Input::get('metadescription_'.$get_lang_name);
				}
			}
			$input1  = array_merge($input1,$lang_entry_vali);
			$rules1 = array(
				'title' => 'Required',
				'category' => 'Required|not_in:0',
				'maincategory' => 'Required|not_in:0',
				'originalprice' => 'Required|numeric',
				'discountprice' => 'Required|numeric',
				'startdate' => 'Required|date',
				'enddate' => 'Required|date|after:startdate',
				'description' => 'Required',
				'maxlimit' => 'Required|numeric'
			);
			#sathyaseelan
			if(!empty($get_active_lang))
			{ 
				foreach($get_active_lang as $get_lang)
				{
					$get_lang_name = $get_lang->lang_name;

					$rules1vali = array(
					'title_'.$get_lang_name.'' => 'Required',
					'description_'.$get_lang_name.'' => 'Required',
					'metakeyword_'.$get_lang_name.'' => 'Required',
					'metadescription_'.$get_lang_name.'' => 'Required'
					); 
					$rules1=array_merge($rules1,$rules1vali);
				}
			}
			
			/*  echo'<pre>';print_r($input1);print_r($rules1);die;  */
			$validator1 = Validator::make($input1, $rules1);
			if ($validator1->fails())
			{
				return Redirect::to('mer_edit_deals/'.$id)->withErrors($validator1->messages())->withInput();
			}
        $inputs = Input::all();
        
        $count            = Input::get('count');

        $j=1;	
            $filename_new_get = "";
			$filename = Input::get('file_get');
			$file_get_path =  explode("/**/",$filename,-1);
			 
				$field_values_array = $request->file('file');
				 
				
				 $file = Input::file('file');
				 $k=0; 
				 if(count($field_values_array)>0){   
				 foreach($field_values_array as $value)
				 {                  
					$a = $j++;                                         
					if(isset($value))
					{  
						$filename        = $value->getClientOriginalName(); 
                        $move_img        = explode('.', $filename);
						$time=time();
                        $filename_new = 'Deal_'.$time.'.' . strtolower($value->getClientOriginalExtension());
						$destinationPath = './public/assets/deals/';
						Image::make($value)->save('./public/assets/deals/'.$filename_new,$this->image_compress_quality); 
						$image_old = Input::get('image_old');
						if(file_exists($destinationPath.$image_old[$k]))
						{
							@unlink($destinationPath.$image_old[$k]);
						}
                        
                        $filename_new_get.= $filename_new . "/**/";   
					
					}						
				 else 
					{                                            
						$filename_new_get.= $file_get_path[$a-1]. "/**/";  
					}
					$k++;
				} 
				 }	
                 else
                 {
                    $filename_new_get = $filename;
                 }			

			/*if isset new product images it goes here */

                $new_field_values_array = Input::file('new_file');
				

                $new_file_count = count($new_field_values_array);

                if($new_file_count > 0){
                    
                  foreach($new_field_values_array as $new_value){
                    //your New Product Image Upload into directory goes here
                    
                   $product_new_name   = $new_value->getClientOriginalName();
					
                    
                    $new_more_img       = explode('.', $product_new_name);
					//$time=time();
					$time=time().rand();
                    $filename_new = 'Deal_'.$time.'.' . strtolower($new_value->getClientOriginalExtension());
                    
                    Image::make($new_value)->save('./public/assets/deals/'.$filename_new,$this->image_compress_quality);
                    
                    $filename_new_get .= $filename_new . "/**/";

                  }//foreach

                } //if		
            $file_name_insert = $filename_new_get;

        /*$filename_new_get = "";
        for ($i = 0; $i < $count; $i++) {
            $file_more = Input::file('file_more' . $i);
            if ($file_more == "") {
                $dile_name_new_get = Input::get('file_more_new' . $i);*/
                //$filename_new_get .= $dile_name_new_get . "/**/";
            /*} else {
                $file_more_name     = $file_more->getClientOriginalName();
                $move_more_img      = explode('.', $file_more_name);
                $filename_new       = $move_more_img[0] . str_random(8) . "." . $move_more_img[1];
                $newdestinationPath = './public/assets/deals/';
                $uploadSuccess_new  = Input::file('file_more' . $i)->move($newdestinationPath, $filename_new);*/
                //$filename_new_get .= $filename_new . "/**/";
            //}
            
       // }
        
       /* $file = Input::file('file');
        if ($file == "") {
            $filename = Input::get('file_new');
        } else {
            $filename        = $file->getClientOriginalName();
            $move_img        = explode('.', $filename);
            $filename        = $move_img[0] . str_random(8) . "." . $move_img[1];
            $destinationPath = './public/assets/deals/';
            $uploadSuccess   = Input::file('file')->move($destinationPath, $filename);
        }*/
        
        //$file_name_insert         = $filename . "/**/" . $filename_new_get;
       
        $deal_saving_price        = Input::get('originalprice') - Input::get('discountprice');
        //$deal_discount_percentage = round(($deal_saving_price / Input::get('originalprice')) * 100, 2);
        $deal_discount_percentage = ($deal_saving_price / Input::get('originalprice')* 100);

        $deal_discount_percentage= floor($deal_discount_percentage);
        
        
        $Shipping_Amount           = Input::get('Shipping_Amount');
            if ($Shipping_Amount == "") {
                $Shipping_Amount = 0;
            }
                  $Product_SubCategory = "";
                  $Product_SecondSubCategory = "";

                 if(Input::get('subcategory') != ""){
                $Product_SubCategory = Input::get('subcategory');  
                }   
                if(Input::get('secondsubcategory') != ""){          
                $Product_SecondSubCategory = Input::get('secondsubcategory');  
                } 
        $inc_tax          = Input::get('inctax');

        $entry                    = array(
            'deal_title' => Input::get('title'),
            'deal_category' => Input::get('category'),
            'deal_main_category' => Input::get('maincategory'),
           'deal_sub_category' =>  $Product_SubCategory,
            'deal_second_sub_category' => $Product_SecondSubCategory,
            'deal_original_price' => Input::get('originalprice'),
            'deal_discount_price' => Input::get('discountprice'),
            'deal_discount_percentage' => $deal_discount_percentage,
            'deal_saving_price' => $deal_saving_price,
            'deal_inctax'   => $inc_tax,
            'deal_shippamt' => $Shipping_Amount,

            'deal_start_date'   => date('Y-m-d H:i:s', strtotime(Input::get('startdate'))),
            'deal_end_date'     => date('Y-m-d H:i:s', strtotime(Input::get('enddate'))),
            'deal_expiry_date'  => date('Y-m-d H:i:s', strtotime(Input::get('enddate'))),

            'deal_description' => Input::get('description'),
            'deal_merchant_id' => Input::get('merchant'),
            'deal_shop_id' => Input::get('shop'),
            'deal_meta_keyword' => Input::get('metakeyword'),
            'deal_meta_description' => Input::get('metadescription'),
          //  'deal_min_limit' => Input::get('minlimt'),
            'deal_max_limit' => Input::get('maxlimit'),
            
            'deal_image_count' => Input::get('count'),
            'deal_image' => $file_name_insert,
            'deal_posted_date' => $now,
             //product policy starts
                'allow_cancel'      => $allow_cancel,
                'allow_return'      => $allow_return,
                'allow_replace'     => $allow_replace,
                'cancel_policy'     => $cancel_policy,
                'return_policy'     => $return_policy,
                'replace_policy'    => $replace_policy,
                'cancel_days'       => $cancel_days,
                'return_days'       => $return_days,
                'replace_days'      => $replace_days,
                
                //product policy ends
        );
        $entry = array_merge($entry,$p_lang_entry); //Policy merge	

#sathyaseelan
		$lang_entry = array();
		if(!empty($get_active_lang))
		{ 
			foreach($get_active_lang as $get_lang)
			{
				$get_lang_name = $get_lang->lang_name;
				$get_lang_code = $get_lang->lang_code;

				$lang_entry = array(
				'deal_title_'.$get_lang_code.'' => Input::get('title_'.$get_lang_name),
				'deal_description_'.$get_lang_code.'' => Input::get('description_'.$get_lang_name),
				'deal_meta_keyword_'.$get_lang_code.'' => Input::get('metakeyword_'.$get_lang_name),
				'deal_meta_description_'.$get_lang_code.'' => Input::get('metadescription_'.$get_lang_name)
				); 
				$rule  = array_merge($entry,$lang_entry);
			}
		}
		
		
		/* if(!empty($get_active_lang)) { 
			foreach($get_active_lang as $get_lang) {
				$get_lang_code = $get_lang->lang_code;
				$get_lang_name = $get_lang->lang_name;
				$lang_entry['deal_title_'.$get_lang_code.''] = Input::get('title_'.$get_lang_name);
				$lang_entry['deal_description_'.$get_lang_code.''] = Input::get('description_'.$get_lang_name);
				$lang_entry['deal_meta_keyword_'.$get_lang_code.''] = Input::get('metakeyword_'.$get_lang_name);
				$lang_entry['deal_meta_description_'.$get_lang_code.''] = Input::get('metadescription_'.$get_lang_name);
				
			}
		} */
		$entry  = array_merge($entry,$lang_entry);	
//echo'<pre>';print_r($entry);die;		
        $return                   = Merchantdeals::edit_deal($entry, $id);
		if (Lang::has(Session::get('mer_lang_file').'.MER_DEAL_UPDATED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_DEAL_UPDATED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_DEAL_UPDATED_SUCCESSFULLY');
			}
        return Redirect::to('mer_manage_deals')->with('block_message',$session_message);
    }
    
    public function deals_select_main_cat()
    {
        $id       = $_GET['id'];
        $main_cat = Merchantdeals::get_main_category_ajax($id);
        if ($main_cat) {
            $return = "";
			 if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_SELECT');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SELECT');
			}
            $return = "<option value='0'> --$session_message -- </option>";
            foreach ($main_cat as $main_cat_ajax) {
                $return .= "<option value='" . $main_cat_ajax->smc_id . "'> " . $main_cat_ajax->smc_name . " </option>";
            }
            echo $return;
        } else {
			 if (Lang::has(Session::get('mer_lang_file').'.MER_NO_DATAS_FOUND')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_NO_DATAS_FOUND');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_NO_DATAS_FOUND');
			}
            echo $return = "<option value='0'> $session_message </option>";
        }
    }
    
    public function deals_select_sub_cat()
    {
        $id       = $_GET['id'];
        $main_cat = Merchantdeals::get_sub_category_ajax($id);
        if ($main_cat) {
            $return = "";
			 if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_SELECT');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SELECT');
			}
            $return = "<option value='0'> -- $session_message -- </option>";
            foreach ($main_cat as $main_cat_ajax) {
                $return .= "<option value='" . $main_cat_ajax->sb_id . "'> " . $main_cat_ajax->sb_name . " </option>";
            }
            echo $return;
        } else {
			 if (Lang::has(Session::get('mer_lang_file').'.MER_NO_DATAS_FOUND')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_NO_DATAS_FOUND');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_NO_DATAS_FOUND');
			}
            echo $return = "<option value='0'> $session_message </option>";
        }
    }
    
    public function deals_select_second_sub_cat()
    {
        $id       = $_GET['id'];
        $main_cat = Merchantdeals::get_second_sub_category_ajax($id);
        if ($main_cat) {
            $return = "";
			if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_SELECT');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SELECT');
			}
            $return = "<option value='0'> -- $session_message -- </option>";
            foreach ($main_cat as $main_cat_ajax) {
                $return .= "<option value='" . $main_cat_ajax->ssb_id . "'> " . $main_cat_ajax->ssb_name . " </option>";
            }
            echo $return;
        } else {
			if (Lang::has(Session::get('mer_lang_file').'.MER_NO_DATAS_FOUND')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_NO_DATAS_FOUND');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_NO_DATAS_FOUND');
			}
            echo $return = "<option value='0'> $session_message</option>";
        }
    }
    
    public function deals_edit_select_main_cat()
    {
        $id       = $_GET['edit_id'];
        $main_cat = Merchantdeals::get_main_category_ajax_edit($id);
        if ($main_cat) {
            $return = "";
            foreach ($main_cat as $main_cat_ajax) {
                $return = "<option value='" . $main_cat_ajax->smc_id . "' selected> " . $main_cat_ajax->smc_name . " </option>";
            }
            echo $return;
        } else {
			if (Lang::has(Session::get('mer_lang_file').'.MER_NO_DATAS_FOUND')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_NO_DATAS_FOUND');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_NO_DATAS_FOUND');
			}
            echo $return = "<option value='0'> $session_message </option>";
        }
    }
    
    public function deals_edit_select_sub_cat()
    {
        $id       = $_GET['edit_sub_id'];
        $main_cat = Merchantdeals::get_sub_category_ajax_edit($id);
        if ($main_cat) {
            $return = "";
            foreach ($main_cat as $main_cat_ajax) {
                $return = "<option value='" . $main_cat_ajax->sb_id . "' selected> " . $main_cat_ajax->sb_name . " </option>";
            }
            echo $return;
        } else {
			if (Lang::has(Session::get('mer_lang_file').'.MER_NO_DATAS_FOUND')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_NO_DATAS_FOUND');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_NO_DATAS_FOUND');
			}
            echo $return = "<option value='0'> $session_message </option>";
        }
    }
    
    public function deals_edit_second_sub_cat()
    {
        $id       = $_GET['edit_secnd_sub_id'];
        $main_cat = Merchantdeals::get_second_sub_category_ajax_edit($id);
        if ($main_cat) {
            $return = "";
            foreach ($main_cat as $main_cat_ajax) {
                $return = "<option value='" . $main_cat_ajax->ssb_id . "' selected> " . $main_cat_ajax->ssb_name . " </option>";
            }
            echo $return;
        } else {
			if (Lang::has(Session::get('mer_lang_file').'.MER_NO_DATAS_FOUND')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_NO_DATAS_FOUND');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_NO_DATAS_FOUND');
			}
            echo $return = "<option value='0'> $session_message </option>";
        }
    }
    
    public function check_title_exist()
    {
        $id          = $_GET['title'];
        $exist_title = Merchantdeals::check_title_exist_ajax($id);
        if ($exist_title) {
            echo 1;
        } else {
            echo 0;
        }
    }
    
    public function check_title_exist_edit()
    {
        $title       = $_GET['title'];
        $id          = $_GET['dealid'];
        $exist_title = Merchantdeals::check_title_exist_ajax_edit($id, $title);
        if ($exist_title) {
            echo 1;
        } else {
            echo 0;
        }
    }
    
    
    public function manage_deals()
    {
        $date   = date('Y-m-d H:i:s');
        $mer_id = Session::get('merchantid');
        
        $from_date        = Input::get('from_date');
        $to_date          = Input::get('to_date');
        $merchant_dealrep = Merchantdeals::merchant_dealrep($from_date, $to_date, $mer_id);
        $delete_deals 	  = Merchantdeals::get_order_deals_details();
        $adminheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', "deals");
        $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_deals');
        $adminfooter    = view('sitemerchant.includes.merchant_footer');
        $return         = Merchantdeals::get_deal_details($date, $mer_id);
        return view('sitemerchant.manage_deals')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('deal_details', $return)->with('merchant_dealrep', $merchant_dealrep)->with('delete_deals', $delete_deals)
        ->with('from_date',$from_date)->with('to_date',$to_date);
    }
    
    public function expired_deals()
    {
        $date   = date('Y-m-d H:i:s');
        $mer_id = Session::get('merchantid');
        
        $from_date           = Input::get('from_date');
        $to_date             = Input::get('to_date');
        $merchantexp_dealrep = Merchantdeals::merchantexp_dealrep($from_date, $to_date, $mer_id, $date);
        
        $adminheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', "deals");
        $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_deals');
        $adminfooter    = view('sitemerchant.includes.merchant_footer');
        $return         = Merchantdeals::get_expired_deals($date, $mer_id);
        return view('sitemerchant.expired_deals')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('deal_details', $return)->with('merchantexp_dealrep', $merchantexp_dealrep)
        ->with('from_date',$from_date)->with('to_date',$to_date);
    }
	
	
	public function sold_deals()
    {
        if(Session::has('merchantid')){
		$date   = date('Y-m-d H:i:s');
        $mer_id = Session::get('merchantid');
        
        $from_date           = Input::get('from_date');
        $to_date             = Input::get('to_date');
        $merchantexp_dealrep = Merchantdeals::merchantsold_dealrep($from_date, $to_date, $mer_id, $date);
        
        $adminheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', "deals");
        $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_deals');
        $adminfooter    = view('sitemerchant.includes.merchant_footer');
        $return         = Merchantdeals::get_sold_deals($date, $mer_id);
        return view('sitemerchant.sold_deals')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('deal_details', $return)->with('merchantexp_dealrep', $merchantexp_dealrep)
        ->with('from_date',$from_date)->with('to_date',$to_date);
		}
		else
		{
			return Redirect::to('sitemerchant');
		}
    }
    
    public function block_deals($id, $status)
    {
        $entry = array(
            'deal_status' => $status
        );
        Merchantdeals::block_deal_status($id, $entry);
        if ($status == 1) {
			 if (Lang::has(Session::get('mer_lang_file').'.MER_DEAL_ACTIVATED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_DEAL_ACTIVATED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_DEAL_ACTIVATED_SUCCESSFULLY');
			}
            return Redirect::to('mer_manage_deals')->with('block_message',$session_message);
        } else if ($status == 0) {
			 if (Lang::has(Session::get('mer_lang_file').'.MER_DEAL_BLOCKED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_DEAL_BLOCKED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_DEAL_BLOCKED_SUCCESSFULLY');
			}
            return Redirect::to('mer_manage_deals')->with('block_message', $session_message);
        }
    }
 
       
    public function deal_details($id)
    {
		 if (Lang::has(Session::get('mer_lang_file').'.BACK_DEALS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.BACK_DEALS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.BACK_DEALS');
			}
        $adminheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', $session_message);
        $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_deals');
        $adminfooter    = view('sitemerchant.includes.merchant_footer');
        $return         = Merchantdeals::get_deals_view($id);
        return view('sitemerchant.deal_details')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('deal_list', $return);
    }
    
    public function validate_coupon_code()
    {
		if (Lang::has(Session::get('mer_lang_file').'.BACK_DEALS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.BACK_DEALS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.BACK_DEALS');
			}
        $adminheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', $session_message);
        $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_deals');
        $adminfooter    = view('sitemerchant.includes.merchant_footer');
        
        return view('sitemerchant.validate_coupon')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter);
    }
    
    public function redeem_coupon_list()
    {
		if (Lang::has(Session::get('mer_lang_file').'.BACK_DEALS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.BACK_DEALS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.BACK_DEALS');
			}
        $adminheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', $session_message);
        $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_deals');
        $adminfooter    = view('sitemerchant.includes.merchant_footer');
        
        return view('sitemerchant.redeem_coupon')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter);
    }
	
	public function mer_delete_deals($id)
	{
		if(Session::has('merchantid')){
			if (Lang::has(Session::get('mer_lang_file').'.BACK_DEALS')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.BACK_DEALS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.BACK_DEALS');
			}
	     $adminheader 		= view('sitemerchant.includes.merchant_header')->with('routemenu',$session_message);	
		 $adminleftmenus	= view('sitemerchant.includes.merchant_left_menu_deals');
		 $adminfooter 		= view('sitemerchant.includes.merchant_footer');
		 $del_deals = Merchantdeals::delete_deals($id);
			 if (Lang::has(Session::get('mer_lang_file').'.MER_DEAL_DELETED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('mer_lang_file').'.MER_DEAL_DELETED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_DEAL_DELETED_SUCCESSFULLY');
			}
		return Redirect::to('mer_manage_deals')->with('block_message',$session_message);	
		}
		else
        {
        return Redirect::to('sitemerchant');
        }	
     }
	public function ajaxcall(){

	echo '<pre>';
	
	print_r($_POST);
	
	echo '</pre>';
	
	exit;


	}

    public function mer_deals_select_main_cat()

    {

        $id       = $_GET['id'];

        $main_cat = Deals::get_main_category_ajax($id);

        if ($main_cat) {

            $return = "";
			
            $return = "<option value='0'>-- Select --</option>";

            foreach ($main_cat as $main_cat_ajax) {

                $return .= "<option value='" . $main_cat_ajax->smc_id . "'> " . $main_cat_ajax->smc_name;
                /* $return .= "(".$main_cat_ajax->smc_name_ar.")"; */
                $return .= "</option>";

            }

            echo $return;

        } else {
			
            echo $return = "<option value='0'> --No Datas Found-- </option>";

        }

    }

    public function mer_deals_select_sub_cat()

    {

        $id       = $_GET['id'];

        $main_cat = Deals::get_sub_category_ajax($id);

        if (count($main_cat)>0) {

            $return = "";
			
            $return = "<option value='0'> -- Select -- </option>";

            foreach ($main_cat as $main_cat_ajax) {

                $return .= "<option value='" . $main_cat_ajax->sb_id . "'> " . $main_cat_ajax->sb_name;
                /* $return .= "(".$main_cat_ajax->sb_name_ar.")"; */
                $return .= "</option>";

            }

            echo $return;

        } else {
			
            echo $return = "<option value='0'>--No Datas Found-- </option>";

        }

    }

    public function mer_deals_select_second_sub_cat()

    {

        $id       = $_GET['id'];

        $main_cat = Deals::get_second_sub_category_ajax($id);

        if (count($main_cat)>0) {

            $return = "";
			
            $return = "<option value='0'> -- Select -- </option>";

            foreach ($main_cat as $main_cat_ajax) {

                $return .= "<option value='" . $main_cat_ajax->ssb_id . "'> " . $main_cat_ajax->ssb_name;
               /*  $return .= "(".$main_cat_ajax->ssb_name_ar.") </option>"; */
                $return .= "</option>";

            }

            echo $return;

        } else {
			
            echo $return = "<option value='0'> --No Datas found-- </option>";

        }

    }
	//kathir
	public function mer_manage_deal_cashondelivery_details()
    {
		
        if (Session::has('merchantid')) {
            $date        = date('Y-m-d H:i:s');
            $from_date   = Input::get('from_date');
            $to_date     = Input::get('to_date');
            $exdeals_rep = Deals::exdeals_rep($from_date, $to_date, $date);               
			if(Lang::has(Session::get('mer_lang_file').'.MER_DEALS')!= '') 
			{ 
				$session_message = trans(Session::get('mer_lang_file').'.MER_DEALS');
			}  
			else 
			{ 
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_DEALS');
			}
			$merid             = Session::get('merchantid');
			
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_deals');
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $returnvalue    = MerchantDeal::get_dealcashon_delivery_details($merid);
			
            return view('sitemerchant.manage_deal_cashon_delivery_details')->with('merchantheader', $merchantheader)->with('merchantleftmenus', $merchantleftmenus)->with('merchantfooter', $merchantfooter)->with('deal_cod', $returnvalue)->with('exdeals_rep', $exdeals_rep);
        } else {
            return Redirect::to('sitemerchant');
        }
    }
	public function mer_manage_deal_shipping_details()
    {
		
	if (Session::has('merchantid')) 
	{
        $date        = date('Y-m-d H:i:s');
        $from_date   = Input::get('from_date');
        $to_date     = Input::get('to_date');
        $exdeals_rep = Deals::exdeals_rep($from_date, $to_date, $date);                  
		if(Lang::has(Session::get('mer_lang_file').'.MER_DEALS')!= '') 
		{ 
			$session_message = trans(Session::get('mer_lang_file').'.MER_DEALS');
		}  
		else 
		{ 
			$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_DEALS');
		}
        $merid             = Session::get('merchantid');
			
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_deals');
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $returnvalue    = MerchantDeal::get_deal_shipping_details($merid);
       
        return view('sitemerchant.mer_deals_shipping_details')->with('merchantheader', $merchantheader)->with('merchantleftmenus', $merchantleftmenus)->with('merchantfooter', $merchantfooter)->with('deal_shipping', $returnvalue)->with('exdeals_rep', $exdeals_rep);
        } 
		else 
		{
            return Redirect::to('sitemerchant');
        }
    }
    public function mer_manage_deal_payu_details()
    {
        
    if (Session::has('merchantid')) 
    {
        $date        = date('Y-m-d H:i:s');
        $from_date   = Input::get('from_date');
        $to_date     = Input::get('to_date');
        $exdeals_rep = Deals::exdeals_rep($from_date, $to_date, $date);                  
        if(Lang::has(Session::get('mer_lang_file').'.MER_DEALS')!= '') 
        { 
            $session_message = trans(Session::get('mer_lang_file').'.MER_DEALS');
        }  
        else 
        { 
            $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_DEALS');
        }
        $merid             = Session::get('merchantid');
            
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_deals');
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $returnvalue    = MerchantDeal::get_deal_payu_details($merid);
       
        return view('sitemerchant.mer_manage_deal_payu_details')->with('merchantheader', $merchantheader)->with('merchantleftmenus', $merchantleftmenus)->with('merchantfooter', $merchantfooter)->with('deal_shipping', $returnvalue)->with('exdeals_rep', $exdeals_rep);
        } 
        else 
        {
            return Redirect::to('sitemerchant');
        }
    }

    //Save Edited Image
    public function CropNdUpload_deal(Request $request){
        //echo 'jhj';
       // var_dump($request->input('_token'));
        $data = Input::except(array(
                '_token'
            ));
       // print_r($data);exit();
        $product_id     = Input::get('product_id');
        $img_id         = Input::get('img_id');
        $imgfileName    = Input::get('imgfileName'); //old image file name

        $imageData = Input::get('base64_imgData');
        $img_dat = explode(',',Input::get('base64_imgData'));
        $new_name = 'Deal_'.time().rand().'.jpg';

        //$imge =  file_put_contents('public/assets/deals/'.$new_name, base64_decode($img_dat[1]));
        $imageData = base64_decode($img_dat[1]);
        //imagepng($source,'public/assets/deals/'.$new_name,6);
        $file_path = './public/assets/deals/'.$new_name;
        //Upload image with compression
        $img = Image::make(imagecreatefromstring($imageData))->save($file_path);
        //jpg background color black remove
        list($width, $height) = getimagesize($file_path);
        $output = imagecreatetruecolor($width, $height);
        $white = imagecolorallocate($output,  255, 255, 255);
        imagefilledrectangle($output, 0, 0, $width, $height, $white);
        imagecopy($output, imagecreatefromstring($imageData), 0, 0, 0, 0, $width, $height);
        imagejpeg($output, $file_path);
        //image compression
        $img = Image::make($output)->save($file_path,$this->image_compress_quality);

        if(file_exists('public/assets/deals/'.$new_name)){

            //upload small image
            list($width,$height)=getimagesize('public/assets/deals/'.$new_name);     
            

            //unlink old files starts
            if(file_exists('public/assets/deals/'.$imgfileName))
                unlink("public/assets/deals/".$imgfileName);
            if(file_exists('public/assets/deals/'.$imgfileName))
                unlink("public/assets/deals/".$imgfileName);
            //unlink old files ends

            //update in image table 
            $product_list = Deals::get_deals($product_id);

            if(count($product_list)>0){
                foreach ($product_list as $prd) {
                    
                }
                $prod_imgAr = explode('/**/', $prd->deal_image);
                if(in_array($imgfileName,$prod_imgAr))
                {    
                    $key = array_search($imgfileName,$prod_imgAr);
                    $prod_imgAr[$key] = $new_name;
                }else {
                    $key = count($prod_imgAr);
                    $prod_imgAr[$key] = $new_name;
                }
                $prod_img = implode('/**/',$prod_imgAr);  
            }else{
                $prod_img = $new_name.'/**/';
            }

                $entry = array('deal_image' => $prod_img );

             $return     = Deals::edit_deal($entry, $product_id);
             if(Lang::has(Session::get('mer_lang_file').'.MER_IMAGE_UPDATED_SUCCESSFULLY')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_IMAGE_UPDATED_SUCCESSFULLY');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_IMAGE_UPDATED_SUCCESSFULLY');
            }
             return Redirect::to('mer_edit_deals/'.$product_id)->with('block_message', $session_message);
        }

        exit();
    }
    /* Image  Crop , rorate and mamipulation ends */

    /* Delete deal image */
    public function delete_deal_image($deal_id,$image)
        {
         echo "fg";
        if(Session::has('merchantid')) 
        { 
            $return = Deals::delete_deal_image($deal_id,$image);
            if(Lang::has(Session::get('mer_lang_file').'.MER_DELETED_SUCCESSFULLY')!= '') 
            { 
                $session_message = trans(Session::get('mer_lang_file').'.MER_DELETED_SUCCESSFULLY');
            }  
            else 
            { 
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_DELETED_SUCCESSFULLY');
            }
            return Redirect::back()->with('message',$session_message);
        }
        else {

            return Redirect::to('siteadmin');

        }
        }

}

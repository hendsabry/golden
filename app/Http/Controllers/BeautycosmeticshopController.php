<?php 
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//

use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;
use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\CartServiceStaff;
use App\Formbusinesstype;
use App\Shoplisthall;
use App\Foodcategorylist;
use App\Beautycosmeticshop;
//------------wisitech code end -----------//
class BeautycosmeticshopController extends Controller
{  
    public function __construct(){
        parent::__construct();
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
     
    public function beautyshopandservices(Request $request,$halltype,$typeofhallid,$shopid)
    {
		$halltype=$halltype;
		$subcategory_id=$halltype;
		$shop_id=$shopid;
        $branchid=$shopid;

        
        

		$city_id=Session::get('searchdata.cityid');
		if(Session::get('searchdata.basecategoryid')!=''){
		$basecategory_id=Session::get('searchdata.basecategoryid');
		}else {
		$basecategory_id=Session::get('searchdata.maincategoryid');
		}
		 $category_id=$typeofhallid;
         $subsecondcategoryid=$halltype;
		 $typeofhallid=$typeofhallid;
		 $budget=Session::get('searchdata.budget');
		$lang=Session::get('lang_file');
		
		//$request->session()->push('searchdata.typeofhallid', $category_id);
		
		 $foodsessioninfo = array('maincategory_id' => $halltype, "category_id" => $category_id, "shop_id" => $shopid); 
				Session::put('fooddata', $foodsessioninfo);
		
		
		$Formbusinesstype= new Formbusinesstype();
		  $bussinesstype=$Formbusinesstype->Getbussinesstype($basecategory_id,$lang);	
			 $value = $request->session()->get('searchdata');
		       $getsubcategories = '';
			   
			   $foodShoplisthall= new Foodcategorylist();
			   $foodShopdate= new Beautycosmeticshop();
			 
			 $foodshopunderbugetincity=$foodShoplisthall->Getfoodshoplistbycity($category_id,$city_id,$budget,$lang);
			// print_r($foodshopunderbugetincity);
			 $fooddateshopdetails=$foodShopdate->Getfoodshopdate($branchid,$lang);
			  $fooddateshopgallery=$foodShopdate->Getgalleryimages($branchid);
			  $fooddateshopreview=$foodShopdate->GetshopReviews($branchid);			 
			   $fooddateshopproducts=$foodShopdate->Getfoodshopdateproduct($branchid,$lang);
			  $beautyshopleftproduct=$foodShopdate->Getfoodshopdatefirstproduct($branchid,$lang);
        if(isset($beautyshopleftproduct->pro_id) && $beautyshopleftproduct->pro_id!='')
        {
         $prod_id=$beautyshopleftproduct->pro_id; 
        }
        else
        {
         return Redirect::back()->with('msg','No product found');  
        }
              

 $tblfield='service_id';
                 

                    if ($branchid != '' && $lang == 'ar_lang') {
                        $servicecategoryAndservices = DB::table('nm_services_attribute')->where('status', '=', 1)->where('services_id', '=', $branchid)->select('id', 'attribute_title_ar as attribute_title','services_id','vendor_id','image')->orderBy('attribute_title')->get();
                   }else{
                         $servicecategoryAndservices = DB::table('nm_services_attribute')->where('status', '=', 1)->where('services_id', '=', $branchid)->select('id', 'attribute_title','services_id','vendor_id','image')->orderBy('attribute_title')->get();
                }
                
                $getPagelimit = config('app.paginate');
                foreach ($servicecategoryAndservices as $sericesdata) {
                       
                     if ($lang == 'ar_lang') {
                        $productbeautyshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $branchid)->where('attribute_id', '=', $sericesdata->id)->where('packege','!=', 'yes')->where('packege','!=', 'yes')->select('pro_id', 'pro_title_ar as pro_title', 'pro_price', 'pro_Img','pro_disprice')->orderBy('pro_title')->get();
                   }else{
                         $productbeautyshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $branchid)->where('attribute_id', '=', $sericesdata->id)->where('packege','!=', 'yes')->select('pro_id', 'pro_title', 'pro_Img','pro_price','pro_disprice')->orderBy('pro_title')->get();
                          }
                        $sericesdata->serviceslist=$productbeautyshopinfo;
                }


                
                /////////////////// end package ///////////
          
        return view('beautyandelegance.beautycosmeticshop', compact('getsubcategories','bussinesstype','foodshopunderbugetincity','halltype','fooddateshopdetails','category_id','subcategory_id','subsecondcategoryid','fooddateshopgallery','fooddateshopreview','fooddateshopproducts','beautyshopleftproduct','shop_id','servicecategoryAndservices','branchid','shop_id'));
 
    }



    public function newbeautycosmaticshop(Request $request)
    {
 $page = $request->page;
 $branchid = $request->branchid;
 $attribute_id = $request->attribute_id;
 $tbl_field=$request->tblfield;
 $lang=Session::get('lang_file');
 if ($lang == 'ar_lang') {
 $productbeautyshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $branchid)->where('attribute_id', '=', $attribute_id)->where('packege','!=', 'yes')->select('pro_id', 'pro_title_ar as pro_title', 'pro_price', 'pro_Img','pro_disprice')->orderBy('pro_title')->paginate(9);
}else{
  $productbeautyshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $branchid)->where('attribute_id', '=', $attribute_id)->where('packege','!=', 'yes')->select('pro_id', 'pro_title', 'pro_price', 'pro_Img','pro_disprice')->orderBy('pro_title')->paginate(9);
  //$productbeautyshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('packege','!=', 'yes')->orderBy('pro_title')->paginate(9);
}
 //

$tabid = $request->tabid;

 return view('beautyandelegance.beautycosmaticpageshop', compact('productbeautyshopinfo','tabid','branchid','tbl_field'));

    }



    function getcartproduct(Request $request){
    			$pro_id = $request->product_id;
                $branchid=$request->branchid;
                $tblfld=$request->tblfld;
    			$lang   = Session::get('lang_file');
    			$Subdatefoodfunc = new Beautycosmeticshop();
		$getsubproduct=$Subdatefoodfunc->getajaxdatefoodproduct($pro_id,$lang,$branchid,$tblfld);	
			return response()->json($getsubproduct);

    }

    function addcartproduct(Request $request){
       // echo "test";
    	//print_r($request->all());
    	//die;
    	$branch_id           = $request->branch_id;
    	$shop_id             = $request->shop_id;
    	$vendor_id           = $request->vendor_id;
      $product_id          = $request->product_id;
      $product_price       = $request->product_price;
      $itemqty             = $request->itemqty;
      $cart_sub_type       = $request->cart_sub_type;
      $category_id         = $request->category_id;
      $subsecondcategoryid = $request->subsecondcategoryid;
    	$userid              = Session::get('customerdata.user_id');
      $productinfo         = new Beautycosmeticshop();
      $lang                = Session::get('lang_file');
      $cartproductdata     = $productinfo->Getproductforcart($product_id,$lang);
      $pro_title           = $cartproductdata->pro_title;
      $pro_Img             = $cartproductdata->pro_Img;
      $pro_title_ar        = $cartproductdata->pro_title_ar;  

      if(isset($cartproductdata->pro_disprice) && $cartproductdata->pro_disprice>0){
          $produnitprice=$cartproductdata->pro_disprice;
      }else{
          $produnitprice=$cartproductdata->pro_price;
      }



      $cart                = Cart::where('user_id',$userid)->first();
      if(!empty($cart)) 
      {        
        $cart_pro = CartProduct::where('cart_type','beauty')->where('cart_id',$cart->id)->where('product_id',$product_id)->delete();
        $cart_service_attribute = CartServiceAttribute::where('cart_type','beauty')->where('cart_id',$cart->id)->where('product_id',$product_id)->delete();
      } 
      else
      {
          $cart_data = array();
          $cart_data['user_id'] = $userid;
          $cart = Cart::create($cart_data); //cart entery
      }
                        if(!empty($cart)) 
                        {
                            $cart_product_data = array();
                            $cart_product_data['cart_type']       = 'beauty';
                            $cart_product_data['cart_sub_type']   = $cart_sub_type;
                            $cart_product_data['cart_id']         = $cart->id;
                            $cart_product_data['product_id']      = $product_id;
                            $cart_product_data['pro_title']       = $pro_title;
                            $cart_product_data['pro_Img']         = $pro_Img;
                            $cart_product_data['pro_title_ar']    = $pro_title_ar;
                            $cart_product_data['total_price']     = $product_price; 
                            $cart_product_data['pro_price']     = $produnitprice; 
                            $cart_product_data['merchant_id']     = $vendor_id;
                            $cart_product_data['category_id']     = $shop_id; 
                            $cart_product_data['branch_id']       = $branch_id;
                            $cart_product_data['quantity']        = $itemqty; 
                            $cart_product_data['shop_id']         = $shop_id;                    
                            $cart_product_data['status']          = 1;
                            $cart_product_data['review_type']     = 'shop';
                            $cart_product = CartProduct::create($cart_product_data); //cart product entry

                     if(\Config::get('app.locale') == 'ar')
                    {
                    Session::flash('status', "وأضاف المنتج في العربة.");
                    }
                    else
                    {
                    Session::flash('status', "Item added in the cart.");
                    }
                    // language for display message //                
                        return Redirect::back();

                        return redirect('beautycosmeticshop/'.$category_id.'/'.$subsecondcategoryid.'/'.$shop_id);

                          
                        } 
                
                
//die;

    }
   







}

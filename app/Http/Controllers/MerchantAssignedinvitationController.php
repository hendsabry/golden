<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Models;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon;
use File; 
use Auth;
use Lang;
use App\Category;
use App\Merchant;
use Khill\Lavacharts\Lavacharts;
use App\ServiceShipping;
use App\OrderInvitation;
use App\InvitationList;


class MerchantAssignedinvitationController extends Controller
{
      public function __construct(){
            parent::__construct();
            // set admin Panel language
            $this->middleware(function ($request, $next) {
                        $this->setLanguageLocaleMerchant();
                        Session::get('mer_lang_code');
                        return $next($request);
           });
     }
    
   




public function manage_assigned_invitation(Request $request)
{

   if (Session::has('merchantid')) 
    {

      $merid  = Session::get('merchantid');
      $getorderinvitation = OrderInvitation::where('vendor_id',$merid)->orderBy('order_id','desc')->paginate(10);;
 
      $this->setLanguageLocaleMerchant();
      $mer_id              = Session::get('merchantid');             
      $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
      $merchantfooter     = view('sitemerchant.includes.merchant_footer');  

 



       return view('sitemerchant.assingedinvitation.invitation-request-list', compact('merchantheader','merchantfooter','getorderinvitation'));       
    } else {
       return Redirect::to('sitemerchant');
    }


}

public function manage_invitees_list(Request $request)
{

   if (Session::has('merchantid')) 
    {
	 $orderid = $request->orderid;
      $merid  = Session::get('merchantid');
      $getorderinvitation = InvitationList::where('order_id',$orderid)->orderBy('invite_name')->paginate(10);;
 
      $this->setLanguageLocaleMerchant();
      $mer_id              = Session::get('merchantid');             
      $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
      $merchantfooter     = view('sitemerchant.includes.merchant_footer');  

 



       return view('sitemerchant.assingedinvitation.invited-invitees-list', compact('merchantheader','merchantfooter','getorderinvitation'));       
    } else {
       return Redirect::to('sitemerchant');
    }


}



 } // end 

<?php
namespace App\Http\Controllers;
use DB;
use Session;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Dashboard;
use App\Admodel;
use App\Auction;
use App\Categorylist;
use App\Customer;
use App\Merchantproducts;
use App\MerchantTransactions;
use App\Merchantdeals;
use App\Reviewrating;
use App\Beautystaffvalidation;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Auth;
use Illuminate\Http\Request;
use Image;
class HallController extends Controller
{
  
	public function __construct(){
			parent::__construct();       
			// set admin Panel language
			$this->middleware(function ($request, $next) {
			$this->setLanguageLocaleMerchant();
			return $next($request);
			});
	}

	 //for listing  hall info 
	 public function hallinfo()
	 {
	  if(Session::has('merchantid')) 
	  {
            $merid  = Session::get('merchantid');
            $this->setLanguageLocaleMerchant();
            $mer_id             = Session::get('merchantid');             
            $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
            $merchantfooter     = view('sitemerchant.includes.merchant_footer');                
			if(isset($_REQUEST['hid']) && $_REQUEST['hid'])
			{
			    $hid = 	$_REQUEST['hid'];
			}
			else
			{
				$hid = 	0;
			}		

			$getDbC = DB::table('nm_product')->where('pro_mr_id', $merid)->where('pro_id',$hid)->count(); 
			if($getDbC >=1)
			{
			    $getDb = DB::table('nm_product')->where('pro_mr_id', $merid)->where('pro_id',$hid)->first(); 
			}
			else
			{
			    $getDb = array();	
			}
			return view('sitemerchant.hall_info', compact('merchantheader','merchantfooter','getDb','getDbC'));       
        } 
        else 
        {
            return Redirect::to('sitemerchant');
        }

	 }
	 // for update/insert  hall info 
	 public function addhallinfo(Request $request)
	 {
			if (Session::has('merchantid')) 
	            {
 

					$validatedData = $request->validate([
					'hallimage' => 'mimes:jpeg,gif,png,jpg',
					'hall_addressimg' => 'mimes:jpeg,gif,png,jpg',
					]);


	                $this->setLanguageLocaleMerchant();
	                $mer_id              = Session::get('merchantid');             
	                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
	                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
	 				$external = ''; $internal = ''; $men=''; $women='';
	 				$pro_id = $request->pro_id;
					$hallname = $request->hallname;
					$hallname_ar = $request->hallname_ar;					 
					$google_mapaddress = $request->google_map_address;	
					$hallcapicity = $request->hallcapicity;	
					$Insuranceamount = $request->Insuranceamount;	
					$about = $request->about;	
					$about_ar = $request->about_ar;	
					$hall_length = $request->hall_length;
					$hall_width = $request->hall_width;
					$hall_area =  $request->hall_area;
					$discount = $request->discount;
					$category = $request->category;
					$category = implode (",", $category);
					$price =   $request->price;
					if($discount!='' && $discount!='0') {
                     $discountprice =   $price-($price * ($discount/100));
					} else {
                       $discountprice = 0;

					}
 							
						if($discountprice!='' && $discountprice!='0') {
							$totalnetprice=$discountprice;
						}else{
								$totalnetprice=$price;
						}

 					 $hallfoodmenu =  $request->hallfoodmenu;	
                      foreach($hallfoodmenu as $vals)
					 {
						${$vals} = $vals;

					 }
 
   

					 $halltype = $request->halltype;
					 foreach($halltype as $vals)
					 {
					  ${$vals} = $vals;
					 }
					$branchID =  $request->bid;
					if($men!='' && $women!='')
					{
					$halltype = 'both';
					}
					elseif($men!='')
					{
					$halltype = 'men';	
					}
					else
					{
					$halltype = 'women';
					}
 
					$price =  $request->price;
					$halldimension = $hall_length.'X'.$hall_width.'X'.$hall_area;
					if($internal!='' && $external!='')
					{
					$FoodType = 'both';
					}
					elseif($internal!='')
					{
					$FoodType = 'internal';	
					}
					else
					{
					$FoodType = 'external';
					}

 					if($file=$request->file('hall_addressimg')){  



				 
					$extension = $file->getClientOriginalExtension();
					$Adressimgname = rand(11111,99999).'_'.time().'.'.$extension;
					//$file->move('hallpics', $fileName);               
					$imageRealPath  =   $file->getRealPath();
					$thumbName      =   'thumb_'. $Adressimgname;       
					$img = Image::make($imageRealPath); // use this if you want facade style code


					// $thumb_width = 150;
					// list($width,$height) = getimagesize($imageRealPath);
					// $thumb_height = ($thumb_width/$width) * $height;
					// $img->resize($thumb_width,$thumb_height);


					$thumb_height = 135;
					list($width,$height) = getimagesize($imageRealPath);
					$thumb_width = ($thumb_height/$height) * $width;
					$img->resize($thumb_width,$thumb_height);


					 
					$img->save('hallpics' . '/'. $thumbName);
					$file->move('hallpics/', $Adressimgname);

 				 $Adressimgname = url('').'/hallpics/'.$thumbName;
					 }
					 else
					 {
 					$getInfoC = DB::table('nm_product')->where('pro_id',$pro_id)->count();
 					if($getInfoC >=1)
 					{
					$getInfo = DB::table('nm_product')->where('pro_id',$pro_id)->first();
					 $Adressimgname = $getInfo->hall_address_image;	
					}
					else
					{
						 $Adressimgname ='';
					 }
					}

					//Insert images in folder		  
					if($files=$request->file('hallimage')){	  
				

  
					$extension = $files->getClientOriginalExtension();
					$imgname = rand(11111,99999).'_'.time().'.'.$extension;
					//$file->move('hallpics', $fileName);               
					$imageRealPath  =   $files->getRealPath();
					$thumbName      =   'thumb_'. $imgname;       
					$img = Image::make($imageRealPath); // use this if you want facade style code


					// $thumb_width = 150;
					// list($width,$height) = getimagesize($imageRealPath);
					// $thumb_height = ($thumb_width/$width) * $height;
					// $img->resize($thumb_width,$thumb_height);


					$thumb_height = 135;
					list($width,$height) = getimagesize($imageRealPath);
					$thumb_width = ($thumb_height/$height) * $width;
					$img->resize($thumb_width,$thumb_height);

 
					$img->save('hallpics' . '/'. $thumbName);
					$files->move('hallpics/', $imgname);
 					$imgname = url('').'/hallpics/'.$thumbName;

					 }
					 else
					 {

					 	$getInfoC = DB::table('nm_product')->where('pro_id',$pro_id)->count();
 					if($getInfoC >=1)
 					{


					 $getInfo = DB::table('nm_product')->where('pro_id',$pro_id)->first();
					 $imgname = $getInfo->pro_Img;	
					}
					else
					{
					 $imgname = '';		
					}
				 
					 }
					$hallcapicity = $request->hallcapicity;	
						  $Insuranceamount = $request->Insuranceamount;	
				
	  
					if(isset($pro_id) && $pro_id=='')			 
					{
					/*Insert data to db*/
					$Hall_sv= DB::table('nm_product')->insert( [
					'pro_title' => $hallname,
					'pro_title_ar' => $hallname_ar,
					'pro_Img'=> $imgname,
					'pro_qty'=> 999,
					'pro_price'=> $price,
					'food_type'=> $FoodType,
					'allow_cancel'=> 0,
					'hall_dimension'=> $halldimension,
					'hall_address_image'=> $Adressimgname,
					'google_map_address'=> $google_mapaddress,
					'hall_type'=> $halltype,
					'pro_mc_id'=> $branchID,
					'pro_mr_id'=> $mer_id,
					'hallcapicity'=>$hallcapicity,
					'about'=>$about,
					'option_id'=>'1,4',
					'about_ar'=>$about_ar,
					'pro_disprice'=>$discountprice,
					'pro_netprice'=>$totalnetprice,
					'pro_discount_percentage' =>$discount,
                    'hall_category_type'=>$category,
                    'Insuranceamount'=>$Insuranceamount,
 
					]);

			   		$pro_id = DB::getPdo()->lastInsertId();
					}
					else
					{
					 /*Update data to db*/
						DB::table('nm_product')->where('pro_id',$pro_id)->where('pro_mr_id',$mer_id)->update( ['pro_title'=>  $hallname,'pro_title_ar'=>  $hallname_ar,'pro_price'=>  $price,'hall_dimension'=> $halldimension,'food_type'=> $FoodType,'pro_Img'=> $imgname,'hall_address_image'=> $Adressimgname,'google_map_address'=> $google_mapaddress,'hall_type'=> $halltype,'hallcapicity'=> $hallcapicity,'about'=> $about,'about_ar'=> $about_ar,'pro_disprice'=>$discountprice,'pro_netprice'=>$totalnetprice,'pro_discount_percentage' =>$discount,'hall_category_type'=>$category,'Insuranceamount'=>$Insuranceamount]);	
						
					}

	 				// language for display message //
					if (\Config::get('app.locale') == 'ar'){
					Session::flash('message', "تمت إضافة الخدمة الخدمة");
					}
					else
					{
					Session::flash('message', "Hall info successfully saved");
					}
					// language for display message //   
		 			 
 					$previousUrl = app('url')->previous();

					return redirect()->to($previousUrl.'&'. http_build_query(['hid'=>$pro_id]));
		 			//return redirect()->route('hall-info'); 
	       
			        } else {
			                return Redirect::to('sitemerchant');
			        }
 }

	 // for listing  hall images  

	public function hall_picture()
	{
	 if (Session::has('merchantid')) 
	            {
	                $merid  = Session::get('merchantid');
	                $this->setLanguageLocaleMerchant();
	                $mer_id              = Session::get('merchantid');             
	                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
	                $merchantfooter     = view('sitemerchant.includes.merchant_footer');                
					 
 					$proid = $_REQUEST['hid'];
					$getDbC = DB::table('nm_product_gallery')->where('vendor_id', $merid)->where('product_id', $proid)->count();
					
					$getVideos = DB::table('nm_product')->where('pro_id',$proid)->where('pro_mr_id',$merid)->first();

					if($getDbC >=1)
					{
					$getDb = DB::table('nm_product_gallery')->where('product_id', $proid)->where('vendor_id',$merid)->where('product_id', $merid)->get(); 
					$getDb = DB::table('nm_product_gallery')->where('product_id', $proid)->where('vendor_id',$merid)->get();


					}
					else
					{
					$getDb = '';	
					}


	  			  return view('sitemerchant.hall_picture', compact('merchantheader','merchantfooter','getCatlists','GetAlreadyServices','getDb','getDbC','getVideos'));       
	            } else {
	                return Redirect::to('sitemerchant');
	            }

	}
	// for add  hall images services
	public function add_hallpicture(Request $request)
	{

	if (Session::has('merchantid')) 
	            {
		if(!$request->file('hallpics') && !$request->youtubevideo && !$request->about)
		{
		$validatedData = $request->validate([
			            		'hallpics' => 'required',
			            		'youtubevideo' => 'required',
		    					 'hallpics.*' => 'image|mimes:jpg,jpeg,png,gif'
		 
							]); 

		}
	            	
 

	                $merid  = Session::get('merchantid');
	                $this->setLanguageLocaleMerchant();
	                $mer_id              = Session::get('merchantid');             
	                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
	                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
			  
					//Insert images in folder
				 
				 	$proid = $request->pro_id;
					if($files=$request->file('hallpics')){
					foreach($files as $file){
					$name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
					//$file->move('hallpics',$name); 
					$extension = $file->getClientOriginalExtension();
					$fileName = rand(11111,99999).'_'.time().'.'.$extension;
					//$file->move('hallpics', $fileName);               
					$imageRealPath  =   $file->getRealPath();
					$thumbName      =   'thumb_'. $fileName;       
					$img = Image::make($imageRealPath); // use this if you want facade style code
					
					$thumb_width = 100;
					list($width,$height) = getimagesize($imageRealPath);
					$thumb_height = ($thumb_width/$width) * $height;
					$img->resize($thumb_width,$thumb_height);
 
					$img->save('hallpics' . '/'. $thumbName);
					$file->move('hallpics/', $fileName);
					  $urld = url('').'/hallpics/'.$thumbName;
				 

					/*Insert data to db*/
					DB::table('nm_product_gallery')->insert( [
					'image'=>  $urld,
					'product_id' =>$proid,
					'vendor_id' => $merid,
					]);

					 

					} 
					
					}
					  $proid = $request->pro_id;	
					$youtubevideoa = $request->youtubevideo;
					$about = $request->about;
					$about_ar = $request->about_ar;
				 
					/*Update data to db*/
					$youtubevideoa = str_replace('youtu.be', 'www.youtube.com/embed', $youtubevideoa);


  					DB::table('nm_product')->where('pro_id',$proid)->where('pro_mr_id',$merid)->update( ['video_url'=>  $youtubevideoa,'mc_video_description_ar'=>  $about_ar,'mc_video_description'=>  $about]);
				 
					// language for display message //
					if (\Config::get('app.locale') == 'ar'){
					Session::flash('message', "تم تحديث الصور والفيديو بنجاح");
					}
					else
					{
					Session::flash('message', "Pictures and Video' updated successfully");
					}
					// language for display message //   
		 			return Redirect::back();      
		        } else {
		                return Redirect::to('sitemerchant');
		        }


	}
	// for delete hall images services
	public function deletehallimages(Request $request)
	{
		if (Session::has('merchantid')) 
		{
		$getThePicvals = $request->pid;
		$merid  = Session::get('merchantid');
  
		$GetPics = DB::table('nm_product_gallery')->where('image',$getThePicvals)->where('vendor_id',$merid)->delete();
		 

	  // language for display message //
	                if (\Config::get('app.locale') == 'ar'){
	                Session::flash('message', "تمت إضافة الخدمة");
	                }
	                else
	                {
	                Session::flash('message', "Picture successfully deleted");
	                }
	// language for display message //   
			return Redirect::back();        
		    } else {
		        return Redirect::to('sitemerchant');
		    }

		
	}


	// for update/insert hall videos services
	public function addhallvideos(Request $request)
	{
	if (Session::has('merchantid')) 
		{
		   	$youtubevideoa = $request->youtubevideo;

 
		   		$validatedData = $request->validate([
	            		
    					  
					]); 

 

		 	$merid  = Session::get('merchantid');
			$proid = $request->pro_id;	
			$youtubevideoa = $request->youtubevideo;
			if($youtubevideoa !=''){
			/*Update data to db*/
			DB::table('nm_product')->where('pro_id',$proid)->where('pro_mr_id',$merid)->update( ['video_url'=>  $youtubevideoa]);
			} 

			// language for display message //
			if (\Config::get('app.locale') == 'ar'){
			Session::flash('message', "تمت إضافة الخدمة");
			}
			else
			{
			Session::flash('message', "Video successfully added");
			}
		 
			// language for display message //   
			return Redirect::back();        
			} else {
			return Redirect::to('sitemerchant');
			}
	}
	// for listing free services

	 public function addhall_freeservices()
	        { 
	         if (Session::has('merchantid')) 
	            {



	                $merid  = Session::get('merchantid');
	                $this->setLanguageLocaleMerchant();
	                $mer_id              = Session::get('merchantid');             
	                $merchantheader    	 = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
	                $merchantfooter   	 = view('sitemerchant.includes.merchant_footer'); 
	                $getCatlists 		 = DB::table('nm_category')->where('parent_id','0')->get();
					
					$hid = $_REQUEST['hid'];
					$getProductInfo = DB::table('nm_product')->where('pro_mr_id', $merid)->where('pro_id',$hid)->first(); 
					$getOptionid = $getProductInfo->option_id;
					if($getOptionid != '')
					{
					$getOptions =	explode(',', $getOptionid);
					}


	                $GetAlreadyServices = DB::table('nm_services')->where('stor_merchant_id',$merid)->get();

					$getDbC = DB::table('nm_product_option_value')->where('vandor_id', $merid)->where('product_option_id',1)->where('product_id',$hid)->count(); 
					if($getDbC >=1)
					{
					$getDb = DB::table('nm_product_option_value')->where('vandor_id', $merid)->where('product_option_id',1)->where('product_id',$hid)->get(); 
					}
					else
					{
					$getDb = '';	
					}


	  			  return view('sitemerchant.hall_services_free', compact('merchantheader','merchantfooter','getCatlists','GetAlreadyServices','getDb','getDbC'));       
	            } else {
	                return Redirect::to('sitemerchant');
	            }
	        }

	// for listing paid services

	   public function addhall_paidservices()
	        { 
	          if (Session::has('merchantid')) 
	            {
	                $merid  = Session::get('merchantid');
	                $this->setLanguageLocaleMerchant();
	                $mer_id              = Session::get('merchantid');
	                $hid      = $_REQUEST['hid'];       
	                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
	                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
	                $getCatlists = DB::table('nm_category')->where('parent_id','0')->get();
	                $GetAlreadyServices = DB::table('nm_services')->where('stor_merchant_id',$merid)->get();


					$getDbC = DB::table('nm_product_option_value')->where('vandor_id', $merid)->where('product_option_id',4)->where('product_id',$hid)->count(); 
					if($getDbC >=1)
					{
					$getDb = DB::table('nm_product_option_value')->where('vandor_id', $merid)->where('product_option_id',4)->where('product_id',$hid)->get(); 
					}
					else
					{
					$getDb = '';	
					}


	    return view('sitemerchant.hall_services_paid', compact('merchantheader','merchantfooter','getCatlists','GetAlreadyServices','getDb','getDbC'));       
	            } else {
	                return Redirect::to('sitemerchant');
	            }
	        }





	// for update/insert free services

	public function add_freeservices(Request $request)
	{
	if (Session::has('merchantid')) 
	            {
 					$hid = $request->hid;
					$servicename = $request->servicename;
					$servicenotes = $request->servicenotes;
					$servicename_ar = $request->servicename_ar;
					$servicenotes_ar = $request->servicenotes_ar;
					$product_option_id = $request->product_option_id;

					//print_r($servicenotes_ar);die();
					 
					$optiontitle = 'Free service';	
					 
					$count = $request->count;	
					$price = $request->price;
	                $merid  = Session::get('merchantid');
	                $this->setLanguageLocaleMerchant();
	                $mer_id              = Session::get('merchantid');             
	                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
	                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
					DB::table('nm_product_option_value')->where('product_option_id', $product_option_id)->where('vandor_id', $merid)->where('product_id',$hid)->delete();
			              
	                if(count($servicename) >=1)
	                {	


	                foreach ($servicename as $key => $value) {
	                		     	# code...
	               	if(isset($servicename[$key])!='')
						{
						$ServiceNm = $servicename[$key];
						$ServiceNt = $servicenotes[$key];
						$ServiceNm_ar = $servicename_ar[$key];
						$ServiceNt_ar = $servicenotes_ar[$key];
						$priceD =  0;
					
		 		 
					DB::table('nm_product_option_value')->insert([
					['product_option_id' => $product_option_id, 'vandor_id' => $merid,'option_title' => $ServiceNm,'option_title_ar' => $ServiceNm_ar,'value' => $ServiceNm,'value_ar'=>$ServiceNm_ar,'option_note_value'=>$ServiceNt,'option_note_value_ar'=>$ServiceNt_ar,'status'=>1,'price'=> $priceD,'product_id'=> $hid],
					]); 

					 }
					
					}
					 
	                } 
	  // language for display message //
	                        if (\Config::get('app.locale') == 'ar'){
	                        Session::flash('message', "تمت إضافة الخدمة");
	                        }
	                        else
	                        {
	                        Session::flash('message', "Service added");
	                        }
	        // language for display message //   
	 			return Redirect::back();      
	            } else {
	                return Redirect::to('sitemerchant');
	            }

	} 


	// for update/insert paid services
	public function add_paidservices(Request $request)
	{
	if (Session::has('merchantid')) 
	            {

					$servicename = $request->servicename;
					$servicenotes = $request->servicenotes;
					$servicename_ar = $request->servicename_ar;
					$servicenotes_ar = $request->servicenotes_ar;
					$product_option_id = $request->product_option_id;
					$hid = $request->hid;
					 
					$optiontitle = 'Paid service';	
					 
					$count = $request->count;	
					$price = $request->price;
	                $merid  = Session::get('merchantid');
	                $this->setLanguageLocaleMerchant();
	                $mer_id              = Session::get('merchantid');             
	                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
	                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
					DB::table('nm_product_option_value')->where('product_option_id', $product_option_id)->where('vandor_id', $merid)->where('product_id',$hid)->delete();
			              
	                if(count($servicename) >=1)
	                {		

	                foreach ($servicename as $key => $value){
						if(isset($servicename[$key])!='')
						{
							
						$ServiceNm = $servicename[$key];
						$ServiceNt = $servicenotes[$key];
						$ServiceNm_ar = $servicename_ar[$key];
						$ServiceNt_ar = $servicenotes_ar[$key];
						$priceD =  $price[$key];
					
				 
					DB::table('nm_product_option_value')->insert([
					['product_option_id' => $product_option_id, 'vandor_id' => $merid,'option_title' => $ServiceNm,'option_title_ar' => $ServiceNm_ar,'value' => $ServiceNm,'value_ar'=>$ServiceNm_ar,'option_note_value'=>$ServiceNt,'option_note_value_ar'=>$ServiceNt_ar,'status'=>1,'price'=> $priceD,'product_id'=> $hid],
					]); 

					 }
					
					}
					 
	                } 

	  // language for display message //
	                        if (\Config::get('app.locale') == 'ar'){
	                        Session::flash('message', "تمت إضافة الخدمة");
	                        }
	                        else
	                        {
	                        Session::flash('message', "Service added");
	                        }
	        // language for display message //   

	 			return Redirect::back();      
	            } else {
	                return Redirect::to('sitemerchant');
	            }

	}
	// for displaying comment and reviews
	public function reviewcomments(Request $request)
	{
		 $getPagelimit = config('app.paginate');
		if (Session::has('merchantid')) 
		{
		$merid  = Session::get('merchantid');
		  $hid =  $request->hid;
		$bid =  $request->bid;
		$this->setLanguageLocaleMerchant();
		$mer_id              = Session::get('merchantid');             
		$merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
		$merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
		$reviewrating = Reviewrating::where('product_id',$hid)->where('shop_id',$bid)->orderBy('created_at','desc')->paginate($getPagelimit)->appends(request()->query());
		return view('sitemerchant.hall_comments_review', compact('merchantheader','merchantfooter','reviewrating'));       
		} else {
		return Redirect::to('sitemerchant');
		}


	}
	// for displaying order-details
	public function hallOrdersDetail(Request $request)
	{
		 $getPagelimit = config('app.paginate');
		if (Session::has('merchantid')) 
		{
		$merid  = Session::get('merchantid');
		$this->setLanguageLocaleMerchant();
		$mer_id              = Session::get('merchantid');   

		$oid=$request->orderid;
                 $cusid=$request->cust_id;
          $proid=$request->hid;

          $getoccasiondetail = DB::table('nm_search_occasion')->where('order_id',$oid)->where('user_id',$cusid)->first();

		$gethall_ordered_services = DB::table('nm_order_option_value')->where('product_type','hall')->where('order_id',$oid)->where('cus_id',$cusid)->where('product_option_id',4)->where('product_id',$proid)->orderBy('created_at','DESC')->get();
			
		$gethall_internal_food_services = DB::table('nm_order_internal_food_dish')->where('order_id',$oid)->where('cus_id',$cusid)->where('product_id',$proid)->orderBy('created_at','DESC')->get();
		

		$merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
		$merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
		$reviewrating = Reviewrating::where('vandor_id',$mer_id)->paginate($getPagelimit)->appends(request()->query());
		return view('sitemerchant.hall_orders_details', compact('merchantheader','merchantfooter','reviewrating','gethall_ordered_services','gethall_internal_food_services','getoccasiondetail'));       
		} else {
		return Redirect::to('sitemerchant');
		}


	}
	// for displaying hall orders
	public function hallorder(Request $request)
	{
		if(Session::has('merchantid')) 
		{
		   $mer_id         = Session::get('merchantid');
		   $merid          = Session::get('merchantid');
		   $bid            = $request->bid;
           $hid            = $request->hid;
           $searchkeyword  = $request->searchkeyword;
           $date_to        = $request->date_to;
           $End_Date       = $request->from_to;
           $order_days     = $request->order_days;
           $status         = $request->status;                
           $serachfirstfrm = $request->serachfirstfrm;
           $getorderedproducts = DB::table('nm_order_product')->where('product_type','hall');

          if($searchkeyword!='')
          {
             $getorderedproducts = $getorderedproducts->where('order_id',$searchkeyword);
          }

          if($status!='')
          {
            $getorderedproducts = $getorderedproducts->where('status',$status);
          } 

          if($date_to!='' && $End_Date!='')
          {
            $getorderedproducts = $getorderedproducts->whereDate('created_at', '>=', $date_to)->whereDate('created_at', '<=', $End_Date);
          } 

          $getorderedproducts = $getorderedproducts->where('shop_id',$bid)->where('product_id',$hid)->orderBy('created_at','DESC')->get();


		$this->setLanguageLocaleMerchant();
		$mer_id             = Session::get('merchantid');             
		$merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
		$merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
		$getCatlists        = DB::table('nm_category')->where('parent_id','0')->get();
		$GetAlreadyServices = DB::table('nm_services')->where('stor_merchant_id',$merid)->get();

		$getDbC = DB::table('nm_product_option_value')->where('vandor_id', $merid)->where('product_option_id',2)->count(); 
		if($getDbC >=1)
		{
		$getDb = DB::table('nm_product_option_value')->where('vandor_id', $merid)->where('product_option_id',2)->get(); 
		}
		else
		{
		$getDb = '';	
		}

        //dd($getorderedproducts);
		return view('sitemerchant.hall_orders', compact('merchantheader','merchantfooter','getCatlists','GetAlreadyServices','getDb','getDbC','bid','hid','mer_id','getorderedproducts'));       
		} else {
		return Redirect::to('sitemerchant');
		}

	}


	// for displaying hall offers
	public function halloffers()
	{
		if (Session::has('merchantid')) 
		{
		$merid  = Session::get('merchantid');
		$this->setLanguageLocaleMerchant();
		$mer_id              = Session::get('merchantid');             
		$merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
		$merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
		$getCatlists = DB::table('nm_category')->where('parent_id','0')->get();
		$GetAlreadyServices = DB::table('nm_services')->where('stor_merchant_id',$merid)->get();


		$getDbC = DB::table('nm_product_option_value')->where('vandor_id', $merid)->where('product_option_id',2)->count(); 
		if($getDbC >=1)
		{
		$getDb = DB::table('nm_product_option_value')->where('vandor_id', $merid)->where('product_option_id',2)->get(); 
		}
		else
		{
		$getDb = '';	
		}


		return view('sitemerchant.hall_offers', compact('merchantheader','merchantfooter','getCatlists','GetAlreadyServices','getDb','getDbC'));       
		} else {
		return Redirect::to('sitemerchant');
		}

	}

	//change the status common


public function changestatus(Request $request)
	{
		if (Session::has('merchantid')) 
		{
		$merid  = Session::get('merchantid');
			if ($request->ajax()) {
					$id = $request->id;
					$activestatus = $request->activestatus;
					if($activestatus==1) {
					$status = 0;
					} else {
					$status = 1;
					}
					$Cfrom = $request->from;

					//If come from Hotel-list page //
					if($Cfrom == 'hotellist')
					{
					DB::table('nm_category')->where('vendor_id',$merid)->where('mc_id',$id)->update( ['mc_status'=>  $status]);	
					echo 1;die;      
					}
	            if($Cfrom == 'buffetlist')
					{
					DB::table('nm_category')->where('vendor_id',$merid)->where('mc_id',$id)->update( ['mc_status'=>  $status]);	
					echo 1;die;      
					}

					  if($Cfrom == 'fabric')
					{
					DB::table('nm_fabric')->where('vendor_id',$merid)->where('id',$id)->update( ['status'=>  $status]);	
					echo 1;die;      
					}



					  if($Cfrom == 'wraping')
					{
					DB::table('nm_product_option_value')->where('vandor_id',$merid)->where('id',$id)->update( ['status'=>  $status]);	
					echo 1;die;      
					}




                     //Makeup artist
					 if($Cfrom == 'prosize')
					{
					DB::table('nm_products_size')->where('vendor_id',$merid)->where('id',$id)->update( ['status'=>  $status]);	
					echo 1;die;      
					}


                     //Makeup artist
					 if($Cfrom == 'makeupartistcategorystatus')
					{
					DB::table('nm_services_attribute')->where('vendor_id',$merid)->where('id',$id)->update( ['status'=>  $status]);	
					echo 1;die;      
					}

					//Makeup artist worker status
					  if($Cfrom == 'makeupartistworker')
						{
							if($status==0){
								$Beautystaffvalidation= new Beautystaffvalidation();
								$getprodt=DB::table('nm_staff_experties')->where('staff_id',$id)->get();
								foreach ($getprodt as $productidfromproducttble) {
				                   $productid=$productidfromproducttble->product_id;
				                   $Beautystaffvalidation->inactivebeautyandeleganceproduct($productid);
				                }
							}
							DB::table('nm_services_staff')->where('id',$id)->update( ['status'=>  $status]);	
							echo 1;die;      
						}
     
					//End worker status

			      //Product attributes
			      if($Cfrom == 'clinicskinteetch')
						{
							DB::table('nm_product_attribute')->where('id',$id)->delete();	
							echo 1;die;      
						}			
			      			
                  
                  //Buffet container status
                    if($Cfrom == 'buffetcontainer')
					{
					DB::table('nm_product_option_value')->where('vandor_id',$merid)->where('id',$id)->update( ['status'=>  $status]);	
					echo 1;die;      
					}
                   
					//Buffet dis status
					if($Cfrom == 'buffetdish')
					{
					DB::table('nm_product')->where('pro_mr_id',$merid)->where('pro_id',$id)->update( ['pro_status'=>  $status]);	
					echo 1;die;      
					}

					if($Cfrom == 'doctorstatus')
					{
					DB::table('nm_product')->where('pro_mr_id',$merid)->where('pro_id',$id)->update( ['pro_status'=>  $status]);	
					echo 1;die;      
					}

					if($Cfrom == 'skinteetch')
					{
					DB::table('nm_product')->where('pro_mr_id',$merid)->where('pro_id',$id)->update( ['pro_status'=>  $status]);	
					echo 1;die;      
					}

					
					
					//Makeupartistservice
					if($Cfrom == 'makeupartistservice')
					{
					DB::table('nm_product')->where('pro_mr_id',$merid)->where('pro_id',$id)->update( ['pro_status'=>  $status]);	
					echo 1;die;      
					}
					
					//If come from merchantlist-list page //
					if($Cfrom == 'merchantlist')
					{
					DB::table('nm_merchant')->where('mer_id',$id)->update( ['mer_staus'=>  $status]);	
					echo 1;die;      
					}


					//If come from halllist-list page //
					if($Cfrom == 'halllist')
					{
					DB::table('nm_product')->where('pro_id',$id)->where('pro_mr_id',$merid)->update( ['pro_status'=>  $status]);	
					echo 1;die;      
					} 


	//If come from deleterecord page //
					if($Cfrom == 'deleterecord')
					{
					DB::table('nm_product_option_value')->where('id',$id)->where('vandor_id',$merid)->delete();	
					echo 1;die;      
					} 


//If come from reciption category -list page //
					if($Cfrom == 'reciptioncategorylist')
					{
					DB::table('nm_services_attribute')->where('id',$id)->where('vendor_id',$merid)->update( ['status'=>  $status]);	
					echo 1;die;      
					} 


			
//If come from servicelist -list page //
					if($Cfrom == 'servicelist')
					{
					DB::table('nm_services')->where('service_id',$id)->where('stor_merchant_id',$merid)->update( ['status'=>  $status]);	
					echo 1;die;      
					} 		

			

//If come from reciption item -list page //
					if($Cfrom == 'reception_item_list')
					{
					DB::table('nm_product')->where('pro_id',$id)->where('pro_mr_id',$merid)->update( ['pro_status'=>  $status]);	
					echo 1;die;      
					} 

      //Buffet category status						
                if($Cfrom == 'buffetcategorystatus')
					{
					DB::table('nm_services_attribute')->where('id',$id)->where('vendor_id',$merid)->update( ['status'=>  $status]);	
					echo 1;die;      
					} 


//Buffet category status						
                if($Cfrom == 'offer')
					{
					DB::table('nm_product_offer')->where('id',$id)->where('vendor_id',$merid)->update( ['status'=>  $status]);	
					echo 1;die;      
					} 




//If come from reciption item -list page //
					if($Cfrom == 'deletehallpics')
					{

		$GetPics = DB::table('nm_product_gallery')->where('image',$id)->where('vendor_id',$merid)->delete();
					echo 1;die;      
					} 		

//If come from singer item -list page //
					if($Cfrom == 'deletesingerpics')
					{

		$GetPics = DB::table('nm_music_gallery')->where('id',$id)->where('vendor_id',$merid)->delete();
					echo 1;die;      
					} 		




			}
		} else {
					return Redirect::to('sitemerchant');
			}
	}
	 
}

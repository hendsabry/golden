<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//

use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;
use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\Formbusinesstype;
use App\Shoplisthall;
use App\Foodcategorylist;
//------------wisitech code end -----------//
class FoodshopbranchController extends Controller
{  
    public function __construct(){
        parent::__construct();
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
     
    public function listallfoodshopbranch(Request $request,$halltype,$typeofhallid,$branch_id)
    {
		$halltype=$halltype;
		$subcategory_id=$halltype;
		$city_id=Session::get('searchdata.cityid');
		if(Session::get('searchdata.basecategoryid')!=''){
		$basecategory_id=Session::get('searchdata.basecategoryid');
		}else {
		$basecategory_id=Session::get('searchdata.maincategoryid');
		}
		if($basecategory_id==''){ $basecategory_id= Session::get('searchdata.mainselectedvalue');}
		 $category_id=$typeofhallid;
		  $typeofhallid=$typeofhallid;
		 $budget=Session::get('searchdata.budget');
		$lang=Session::get('lang_file');
		
		//$request->session()->push('searchdata.typeofhallid', $category_id);
		
		 $foodsessioninfo = array('maincategory_id' => $halltype, "category_id" => $category_id,"branch_id"=>$branch_id); 
				Session::put('fooddata', $foodsessioninfo);
		
		
		$Formbusinesstype= new Formbusinesstype();
		  $bussinesstype=$Formbusinesstype->Getbussinesstype($basecategory_id,$lang);	
			 $value = $request->session()->get('searchdata');
		       $getsubcategories = '';
			   
			   $foodShoplisthall= new Foodcategorylist();
			 
			 $foodshopunderbugetincity=$foodShoplisthall->GetfoodshoplistbycityNew($branch_id,$city_id,$budget,$lang);
			// print_r($foodshopunderbugetincity);
			 $foodmaincategory=$foodShoplisthall->Getfoodparentcategory($branch_id,$lang);
		
        return view('food.allbranch', compact('getsubcategories','bussinesstype','foodshopunderbugetincity','halltype','foodmaincategory','category_id','subcategory_id','typeofhallid','branch_id'));
 
    }
   
}

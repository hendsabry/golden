<?php
namespace App\Http\Controllers;
use DB;
use Session;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\customer;
use App\Products;
use Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
class MerchantController extends Controller
{
    
    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
	public function __construct(){
        parent::__construct();
       
        // set admin Panel language
        $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleMerchant();
                    return $next($request);
    });
       
    }
    public function merchant_dashboard()
    {
        if (Session::has('userid')) {
			 if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_MERCHANT');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT');
			}
            $adminheader         = view('siteadmin.includes.admin_header')->with("routemenu",$session_message);
            $adminleftmenus      = view('siteadmin.includes.admin_left_menu_merchant');
            $adminfooter         = view('siteadmin.includes.admin_footer');
            $merchant_count      = Merchant::get_merchant_count();
            $get_mer_register_count  = Merchant::get_mer_register_count();
			$website_merchant    = Merchant::get_website_merchant();
			$admin_merchant      = Merchant::get_admin_merchant();
			
			$active_merchant      = Merchant::get_active_merchant();
			$inactive_merchant    = Merchant::get_inactive_merchant();
            
            $store_cnt           = Merchant::get_store_cnt();
            $admin_stores_cnt    = Merchant::get_admin_stores();
            $merchant_stores_cnt = Merchant::get_merchant_stores();
			
			$merchanttoday    = Merchant::get_today_merchant();
            $merchant7days    = Merchant::get_7days_merchant();
            $merchant30days   = Merchant::get_30days_merchant();
            $merchant12mnth   = Merchant::get_12months_merchant();
            
            return view('siteadmin.merchant_dashboard')
            ->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)
            ->with('merchant_count', $merchant_count)->with('store_cnt', $store_cnt)->with('admin_stores_cnt', $admin_stores_cnt)->with('merchant_stores_cnt', $merchant_stores_cnt)->with('merchanttoday',$merchanttoday)
            ->with('merchant7days',$merchant7days)->with('merchant30days',$merchant30days)->with('merchant12mnth',$merchant12mnth)
            ->with('get_mer_register_count',$get_mer_register_count)->with('website_merchant',$website_merchant)->with('admin_merchant',$admin_merchant)->with('active_merchant',$active_merchant)->with('inactive_merchant',$inactive_merchant);
        } else {
            return Redirect::to('siteadmin');
        }
        
    }
    
    public function add_merchant()
    {
        if (Session::has('userid')) {
			 if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_MERCHANT');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT');
			}
            $adminheader    = view('siteadmin.includes.admin_header')->with("routemenu", $session_message);
            $adminleftmenus = view('siteadmin.includes.admin_left_menu_merchant');
            $adminfooter    = view('siteadmin.includes.admin_footer');
            $country_return = Merchant::get_country_detail();
            $city_return    = Merchant::get_city_detail();
            return view('siteadmin.add_merchant')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('country_details', $country_return)->with('city_details', $city_return);
        } else {
            return Redirect::to('siteadmin');
        }
    }
    
    public function manage_merchant()
    {
        if (Session::has('userid')) {
			
            $data      = Input::except(array(
                '_token'
            ));
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            
            $merchantrep   = Merchant::get_merchantreports($from_date, $to_date);
             if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_MERCHANT');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT');
			}
            $adminheader                   = view('siteadmin.includes.admin_header')->with("routemenu", $session_message);
            $adminleftmenus                = view('siteadmin.includes.admin_left_menu_merchant');
            $adminfooter                   = view('siteadmin.includes.admin_footer');
            $merchant_return               = Merchant::view_merchant_details();
            $store_count                   = Merchant::get_store_count($merchant_return);
            $merchant_is_or_not_in_deals   = Merchant::merchant_is_or_not_in_deals($merchant_return);
            $merchant_is_or_not_in_product = Merchant::merchant_is_or_not_in_product($merchant_return);
            $merchant_is_or_not_in_auction = Merchant::merchant_is_or_not_in_auction($merchant_return);
            return view('siteadmin.manage_merchant')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('merchant_return', $merchant_return)->with('store_count', $store_count)->with('merchant_is_or_not_in_deals', $merchant_is_or_not_in_deals)->with('merchant_is_or_not_in_product', $merchant_is_or_not_in_product)->with('merchant_is_or_not_in_auction', $merchant_is_or_not_in_auction)->with('merchantrep', $merchantrep)
            ->with('from_date',$from_date)->with('to_date',$to_date);
        } else {
            return Redirect::to('siteadmin');
        }
    }
		
    //Product Review

    public function manage_store_review()
    {

        if (Session::has('userid')) {
            if(Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT')!= '') 
            {
                $session_message = trans(Session::get('admin_lang_file').'.BACK_MERCHANT');
            }
            else 
            {
                $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT');
            }
            $adminheader = view('siteadmin.includes.admin_header')->with("routemenu", $session_message);
            
            $adminleftmenus = view('siteadmin.includes.admin_left_menu_merchant');
            
            $adminfooter = view('siteadmin.includes.admin_footer');

            $get_review = Merchant::get_store_review();

             return view('siteadmin.store_manage_review')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('get_review', $get_review);
        }
        else{
            return Redirect::to('siteadmin');
        }
    }

    public function edit_store_review($id)
    {

        
        if (Session::has('userid')) {
            if(Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT')!= '') 
            {
                $session_message = trans(Session::get('admin_lang_file').'.BACK_MERCHANT');
            }
            else 
            {
                $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT');
            }
            $adminheader = view('siteadmin.includes.admin_header')->with("routemenu", $session_message);
            
            $adminleftmenus = view('siteadmin.includes.admin_left_menu_merchant');
            
            $adminfooter = view('siteadmin.includes.admin_footer');
            
            $result = Merchant::edit_review($id);
     
            return view('siteadmin.edit_store_review')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('result',$result);
            
        }
        
        else {
            
            return Redirect::to('siteadmin');
            
        }
        
    }

    public function edit_store_review_submit()
    {
        if (Session::has('userid')) {
            $now = date('Y-m-d H:i:s');
            
            $inputs = Input::all();
            $review_id = Input::get('comment_id');
            $review_title = Input::get('review_title');
            $review_comment = Input::get('review_comment');

            $entry = array(
                
                'title' => $review_title,
                
                'comments' => $review_comment,
             
            );
            $return = Merchant::update_review($entry, $review_id);
            return Redirect::to('manage_store_review');
        }
        else{
            return Redirect::to('siteadmin');
        }
    }
    public function delete_review($id)
    {
        if(Session::has('userid'))
        {
            if(Lang::has(Session::get('admin_lang_file').'.BACK_SETTINGS')!= '') 
            {
                $session_message = trans(Session::get('admin_lang_file').'.BACK_SETTINGS');
            }
            else 
            {
                $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SETTINGS');
            }
         $adminheader       = view('siteadmin.includes.admin_header')->with("routemenu",$session_message);  
         $adminleftmenus    = view('siteadmin.includes.admin_left_menus');
         $adminfooter       = view('siteadmin.includes.admin_footer');
         $del_review = Merchant::delete_review($id);
         if(Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_DELETED_SUCCESSFULLY')!= '') 
            {
                $rev_session_message = trans(Session::get('admin_lang_file').'.BACK_REVIEW_DELETED_SUCCESSFULLY');
            }
            else 
            {
                $rev_session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_DELETED_SUCCESSFULLY');
            }
         return Redirect::to('manage_store_review')->with('product Deleted',$rev_session_message); 
        }
        else
        {
         return Redirect::to('siteadmin');
        }
    }
    public function block_review($id, $status){
        
        
        if (Session::has('userid')) {
            
            $entry = array(
                
                'status' => $status
                
            );
            
            Merchant::block_review_status($id, $entry);
            
            if ($status == 0) {
                if(Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_UNBLOCKED')!= '') 
                {
                    $session_message = trans(Session::get('admin_lang_file').'.BACK_REVIEW_UNBLOCKED');
                }
                else 
                {
                    $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_UNBLOCKED');
                }
                 
                return Redirect::to('manage_store_review')->with('block_message', $session_message);
                
            }elseif ($status == 1) {
                
                if(Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_BLOCKED')!= '') 
                {
                    $session_message = trans(Session::get('admin_lang_file').'.BACK_REVIEW_BLOCKED');
                }
                else 
                {
                    $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_BLOCKED');
                }
                
                return Redirect::to('manage_store_review')->with('block_message', $session_message);
                
            }
            
        }
        
        else {
            
            return Redirect::to('siteadmin');
            
        }
        
    }  




	
    public function ajax_select_city(){
        
        $cityid    = $_GET['city_id'];
        $city_ajax = Merchant::get_city_detail_ajax($cityid);
        if ($city_ajax) {
            $return = "";
			 if (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_SELECT');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SELECT');
			}
            $return = "<option value=''> $session_message </option>";
            foreach ($city_ajax as $fetch_city_ajax) {
				if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 
						$ci_name = 'ci_name';
				}else {  $ci_name = 'ci_name_'.Session::get('lang_code'); }
                
				
                $return .= "<option value='" . $fetch_city_ajax->ci_id . "'> " . $fetch_city_ajax->$ci_name . " </option>";
          
        }
            echo $return;
        } else {
			 if (Lang::has(Session::get('admin_lang_file').'.BACK_NO_DATAS_FOUND')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_NO_DATAS_FOUND');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_NO_DATAS_FOUND');
			}
            echo $return = "<option value=''> $session_message </option>";
        }
    }
    
    public function add_merchant_submit()
    {
		 
        if (Session::has('userid')) {
            $data = Input::except(array(
                '_token'
            ));
            $date = date('m/d/Y');
            $current_date = date('Y-m-d');
            $rule = array( 
                'first_name' => 'required|regex:/^[A-Z a-z 0-9_-]+$/',
                'last_name' => 'required|regex:/^[A-Z a-z 0-9_-]+$/',
                'email_id' => 'required|email',
                'select_mer_country' => 'required',
                'select_mer_city' => 'required',
                'phone_no' => 'required|numeric',
                'addreess_one' => 'required',
                'address_two' => 'required',
                'store_name' => 'required',
                'store_pho' => 'required|numeric',
                'store_address_one' => 'required',
                'store_address_two' => 'required',
                'select_country' => 'required',
                'select_city' => 'required',
                'zip_code' => 'required|numeric',
               /* 'meta_keyword' => 'required',
                'meta_description' => 'required',*/
				
                'website' => 'url|sometimes',
                'commission' => 'required|numeric|max:99',
                'file' => 'image|mimes:jpeg,png,jpg'
            );
			
			$get_active_lang = 	$this->get_active_language; 
			$lang_rule = array();
			if(!empty($get_active_lang)) { 
				foreach($get_active_lang as $get_lang) {
					$get_lang_name = $get_lang->lang_name;
				
					$lang_rule = array(
					'store_name_'.$get_lang_name => 'required',
					'store_address_one_'.$get_lang_name => 'required',
					'store_address_two_'.$get_lang_name => 'required',
					'meta_keyword_'.$get_lang_name => 'required',
					'meta_description_'.$get_lang_name => 'required',
					); 
					
					$rule  = array_merge($rule,$lang_rule);
			
				}
			}
			
            
            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('add_merchant')->withErrors($validator->messages())->withInput();
            } else {
                $mer_email         = Input::get('email_id');
                $check_merchant_id = Merchant::check_merchant_email($mer_email);
                if ($check_merchant_id) {
					 if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT_EMAIL_EXIST')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_MERCHANT_EMAIL_EXIST');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT_EMAIL_EXIST');
			}
                    return Redirect::to('add_merchant')->with('mail_exist', $session_message)->withInput();
                } else {
                    $file             = Input::file('file');
                    $time			  = time();
                    $filename 		  = 'Store_' .$time.'.'. strtolower($file->getClientOriginalExtension());
                    $destinationPath  = './public/assets/storeimage/';
                    Image::make($file)->save('./public/assets/storeimage/'.$filename,$this->image_compress_quality);
                    $get_new_password = Merchant::randomPassword();
                    $merchantid       = Session::get('merchantid');
                    $admin            = '0';
                    if ($merchantid) {
                        $merchant_entry = array(
                            'addedby' => $merchantid,
                            'mer_fname' => Input::get('first_name'),
                            'mer_lname' => Input::get('last_name'),
                            'mer_password' => $get_new_password,
                            'mer_email' => Input::get('email_id'),
                            'mer_phone' => Input::get('phone_no'),
                            'mer_address1' => Input::get('addreess_one'),
                            'mer_address2' => Input::get('address_two'),
                            'mer_co_id' => Input::get('select_mer_country'),
                            'mer_ci_id' => Input::get('select_mer_city'),
                            'mer_payment' => Input::get('payment_account'),
                            'mer_commission' => Input::get('commission'),
                            'created_date' => $current_date,
                            'mer_pro_status'=>1,
                            'mer_logintype'=>1,
                            'mer_payu_key'=>Input::get('mer_payu_key'),
                            'mer_payu_salt'=>Input::get('mer_payu_salt'),
                        );
                    } else {
                        $merchant_entry = array(
                            'addedby' => $admin,
                            'mer_fname' => Input::get('first_name'),
                            'mer_lname' => Input::get('last_name'),
                            'mer_password' => $get_new_password,
                            'mer_email' => Input::get('email_id'),
                            'mer_phone' => Input::get('phone_no'),
                            'mer_address1' => Input::get('addreess_one'),
                            'mer_address2' => Input::get('address_two'),
                            'mer_co_id' => Input::get('select_mer_country'),
                            'mer_ci_id' => Input::get('select_mer_city'),
                            'mer_payment' => Input::get('payment_account'),
                            'mer_commission' => Input::get('commission'),
                            'created_date' => $current_date,
                            'mer_pro_status'=>1,
                            'mer_logintype'=>1,
                            'mer_payu_key'=>Input::get('mer_payu_key'),
                            'mer_payu_salt'=>Input::get('mer_payu_salt'),
                        );
                    }
                    
                    $inserted_merchant_id = Merchant::insert_merchant($merchant_entry);
                    $store_entry          = array(
                        'stor_name' => Input::get('store_name'),
                        'stor_merchant_id' => $inserted_merchant_id,
                        'stor_phone' => Input::get('store_pho'),
                        'stor_address1' => Input::get('store_address_one'),
                        'stor_address2' => Input::get('store_address_two'),
                        'stor_country' => Input::get('select_country'),
                        'stor_city' => Input::get('select_city'),
                        'stor_zipcode' => Input::get('zip_code'),
                        'stor_metakeywords' => Input::get('meta_keyword'),
                        'stor_metadesc' => Input::get('meta_description'),
                        'stor_website' => Input::get('website'),
                        'stor_latitude' => Input::get('latitude'),
                        'stor_longitude' => Input::get('longitude'),
                        'stor_img' => $filename,
                        'created_date' => $date
                    );
					
					$lang_entry = array();
					if(!empty($get_active_lang)) { 
					foreach($get_active_lang as $get_lang) {
						$get_lang_name = $get_lang->lang_name;
						$get_lang_code = $get_lang->lang_code;
				
						 $lang_entry = array(
						'stor_name_'.$get_lang_code => Input::get('store_name_'.$get_lang_name),
						'stor_address1_'.$get_lang_code => Input::get('store_address_one_'.$get_lang_name),
						'stor_address2_'.$get_lang_code => Input::get('store_address_two_'.$get_lang_name),
						'stor_metakeywords_'.$get_lang_code => Input::get('meta_keyword_'.$get_lang_name),
						'stor_metadesc_'.$get_lang_code => Input::get('meta_description_'.$get_lang_name),
						); 
						
						$store_entry  = array_merge($store_entry,$lang_entry);
					}
				}
				
					 if(Session::get('admin_lang_file'))
					{
				
						$lang ='mer_en_lang';
					}
                   
                    Merchant::insert_store($store_entry);
                    $send_mail_data = array(
                        'name' => Input::get('email_id'),
                        'password' => $get_new_password,
						'mer_email'=> $mer_email,
						'lang' => $lang,
						'LANUAGE' => $this->ADMIN_OUR_LANGUAGE
                    );
                    # It will show these lines as error but no issue it will work fine Line no 119 - 122
                    Mail::send('emails.merchantmail', $send_mail_data, function($message)
                    {
			if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT_ACCOUNT_CREATED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_MERCHANT_ACCOUNT_CREATED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT_ACCOUNT_CREATED_SUCCESSFULLY');
			}
                        $message->to(Input::get('email_id'), Input::get('first_name'))->subject($session_message);
                    });
            if (Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_ADDED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_RECORD_ADDED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_RECORD_ADDED_SUCCESSFULLY');
			}
                    return Redirect::to('manage_merchant')->with('result',$session_message);
                    
                }
            }
        } else {
            return Redirect::to('siteadmin');
            
        }
    }
    
    public function edit_merchant_submit()
    {
		
        if (Session::has('userid')) {
            $mer_id = Input::get('mer_id');
            $data   = Input::except(array(
                '_token'
            ));
            $rule   = array(
                'first_name' => 'required|regex:/^[A-Z a-z 0-9_-]+$/|max:50',
                'last_name' => 'required|regex:/^[A-Z a-z 0-9_-]+$/|max:50',
                'email_id' => 'required|email',
                'select_mer_country' => 'required',
                'select_mer_city' => 'required',
                'phone_no' => 'required|numeric',
                'addreess_one' => 'required',
                'address_two' => 'required',
                'commission' => 'required|numeric|max:99',
                //'website' => 'url|required',
            );
            
            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('edit_merchant/' . $mer_id)->withErrors($validator->messages())->withInput();
            } else {
                $mer_email         = Input::get('email_id');

                $check_merchant_id = Merchant::check_merchant_email_edit($mer_email, $mer_id);

                if (count($check_merchant_id)) {
					 if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT_EMAIL_EXIST')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_MERCHANT_EMAIL_EXIST');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT_EMAIL_EXIST');
			}
                    return Redirect::to('edit_merchant/' . $mer_id)->with('mail_exist',$session_message)->withInput();
                } else {
                    
                    $merchant_entry       = array(
                        'mer_fname' => Input::get('first_name'),
                        'mer_lname' => Input::get('last_name'),
                        'mer_email' => Input::get('email_id'),
                        'mer_phone' => Input::get('phone_no'),
                        'mer_address1' => Input::get('addreess_one'),
                        'mer_address2' => Input::get('address_two'),
                        'mer_co_id' => Input::get('select_mer_country'),
                        'mer_ci_id' => Input::get('select_mer_city'),
                        'mer_payment' => Input::get('payment_account'),
                        'mer_payu_key' => Input::get('payumoney_key'),
                        'mer_payu_salt' => Input::get('payumoney_salt'),
                        'mer_commission' => Input::get('commission')
						
                    );
                    $inserted_merchant_id = Merchant::edit_merchant($merchant_entry, $mer_id);
			if (Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_UPDATED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_RECORD_UPDATED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_RECORD_UPDATED_SUCCESSFULLY');
			}
                    return Redirect::to('manage_merchant')->with('result',$session_message);
                    
                }
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }
    
    public function edit_merchant($id)
    {
        if (Session::has('userid')) {
			 if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_MERCHANT');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT');
			}
            $adminheader     = view('siteadmin.includes.admin_header')->with("routemenu",$session_message);
            $adminleftmenus  = view('siteadmin.includes.admin_left_menu_merchant');
            $adminfooter     = view('siteadmin.includes.admin_footer');
            $country_return  = Merchant::get_country_all_detail();  // when edit need to display all block/unblock datas
            $merchant_return = Merchant::get_induvidual_merchant_detail($id);
            return view('siteadmin.edit_merchant')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('country_details', $country_return)->with('merchant_details', $merchant_return);
        } else {
            return Redirect::to('siteadmin');
        }
    }

     public function ajax_select_city_edit()
    {
         $cityid    = $_GET['city_id_ajax'];
         $countryid    = $_GET['country_id_ajax'];
         $city_ajax = Merchant::get_city_detail_ajax_edit($cityid);
         $country_ajax = Merchant::get_city_detail_ajax($countryid);
         if ($city_ajax) {
                    $return = "";
          foreach ($city_ajax as $fetch_city_ajax) {
                        $return .= "<option value='" . $fetch_city_ajax->ci_id . "' selected > " . $fetch_city_ajax->ci_name . " </option>";
                    } 
         
           if($country_ajax) {       
        foreach ($country_ajax as $city_list) {

               if($cityid!=$city_list->ci_id) {
                        $return .= "<option value='" . $city_list->ci_id . "'> " . $city_list->ci_name . " </option>";
                  }

                    }
                }

                  
                    echo $return;
                } else {
                     if (Lang::has(Session::get('admin_lang_file').'.BACK_NO_DATAS_FOUND')!= '')
                    { 
                        $session_message =  trans(Session::get('admin_lang_file').'.BACK_NO_DATAS_FOUND');
                    }  
                    else 
                    { 
                        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_NO_DATAS_FOUND');
                    }
                    echo $return = "<option value=''> $session_message </option>";
                }
            } 
			
			
    
   /* public function ajax_select_city_edit()
    {
        $cityid    = $_GET['city_id_ajax'];
        $city_ajax = Merchant::get_city_detail_ajax_edit($cityid);
        if ($city_ajax) {
            $return = "";
            foreach ($city_ajax as $fetch_city_ajax) {
                $return .= "<option value='" . $fetch_city_ajax->ci_id . "' selected > " . $fetch_city_ajax->ci_name . " </option>";
            }
            echo $return;
        } else {
			 if (Lang::has(Session::get('admin_lang_file').'.BACK_NO_DATAS_FOUND')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_NO_DATAS_FOUND');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_NO_DATAS_FOUND');
			}
            echo $return = "<option value=''> $session_message </option>";
        }
    }*/
    
    
    public function block_merchant($id, $status)
    {
		
        if (Session::has('userid')) {
            $check_country_status = Merchant::check_country_status($id);
            if($check_country_status>0){    //if country is active you can change status otherwise cant able to change mer status
                if ($status == 1) {  $mer_country_status = 'A'; }elseif($status==0){ $mer_country_status = 'B'; }
                $entry = array(
                'mer_staus' => $status,
                'mer_country_status'=>$mer_country_status,
                );
                Merchant::block_merchant_status($id, $entry);
                if ($status == 1) {  //to unblock
				    if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT_ACTIVATED_SUCCESSFULLY')!= ''){ 
				    $session_message =  trans(Session::get('admin_lang_file').'.BACK_MERCHANT_ACTIVATED_SUCCESSFULLY');
			        }else { 
				    $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT_ACTIVATED_SUCCESSFULLY');
			        }
                    return Redirect::to('manage_merchant')->with('result',$session_message);
                } elseif($status==0) { //to block
				     if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT_BLOCKED_SUCCESSFULLY')!= ''){ 
				        $session_message =  trans(Session::get('admin_lang_file').'.BACK_MERCHANT_BLOCKED_SUCCESSFULLY');
			         }else{ 
				        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT_BLOCKED_SUCCESSFULLY');
			         }
                    return Redirect::to('manage_merchant')->with('result',$session_message);
                }
            }//if country is active you can change status otherwise cant able to change mer status
            else{
                return Redirect::to('manage_merchant')->with('result','Merchant country status is Blocked, So you cant update status');
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }

        public function block_merchant_product($id, $status)
    {
        if (Session::has('userid')) {
            $entry = array(
                'mer_pro_status' => $status
            );
            Merchant::block_merchant_status($id, $entry);
            if ($status == 1) {
				 if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT_PRODUCT_ACTIVATED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_MERCHANT_PRODUCT_ACTIVATED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT_PRODUCT_ACTIVATED_SUCCESSFULLY');
			}
                return Redirect::to('manage_merchant')->with('result', $session_message);
            } else {
				 if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT_PRODUCT_BLOCKED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_MERCHANT_PRODUCT_BLOCKED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT_PRODUCT_BLOCKED_SUCCESSFULLY');
			}
                return Redirect::to('manage_merchant')->with('result',$session_message);
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }
    
    public function manage_store($id)
    {
        if (Session::has('userid')) {
			 if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_MERCHANT');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT');
			}
            $adminheader                = view('siteadmin.includes.admin_header')->with("routemenu", $session_message);
            $adminleftmenus             = view('siteadmin.includes.admin_left_menu_merchant');
            $adminfooter                = view('siteadmin.includes.admin_footer');
            $store_return               = Merchant::view_store_details($id);
			//print_r($store_return); exit;
            $store_is_or_not_in_deals   = Merchant::store_is_or_not_in_deals($store_return);
            $store_is_or_not_in_product = Merchant::store_is_or_not_in_product($store_return);
            $store_is_or_not_in_auction = Merchant::store_is_or_not_in_auction($store_return);
           return view('siteadmin.manage_store')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('store_return', $store_return)->with('store_is_or_not_in_deals', $store_is_or_not_in_deals)->with('store_is_or_not_in_product', $store_is_or_not_in_product)->with('store_is_or_not_in_auction', $store_is_or_not_in_auction);
        } else {
            return Redirect::to('siteadmin');
        }
    }
    
    public function add_store($id)
    {
        if (Session::has('userid')) {
			 if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_MERCHANT');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT');
			}
            $adminheader    = view('siteadmin.includes.admin_header')->with("routemenu", $session_message);
            $adminleftmenus = view('siteadmin.includes.admin_left_menu_merchant');
            $adminfooter    = view('siteadmin.includes.admin_footer');
            $country_return = Merchant::get_country_detail();
            return view('siteadmin.add_store')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('country_details', $country_return)->with('id', $id);
        } else {
            return Redirect::to('siteadmin');
        }
    }
    
    public function add_store_submit()
    {
        if (Session::has('userid')) {
            $merchant_id = Input::get('store_merchant_id');
            $data        = Input::except(array(
                '_token'
            ));
            $rule        = array(
                'store_name' => 'required',
                'store_pho' => 'required|numeric',
                'store_add_one' => 'required',
                'store_add_two' => 'required',
                'select_country' => 'required',
                'select_city' => 'required',
                'zip_code' => 'required|numeric',
           //     'meta_keyword' => 'required',
			//	'meta_description' => 'required',
                //'website' => 'required',
                'latitude' => 'required',
                'longitude' => 'required',
                'file' => 'required|image|mimes:jpeg,png,jpg|image_size:'.$this->store_width.','.$this->store_height.''
            );
			
			$get_active_lang = 	$this->get_active_language; 
			$lang_rule = array();
			if(!empty($get_active_lang)) { 
				foreach($get_active_lang as $get_lang) {
					$get_lang_name = $get_lang->lang_name;
				
					$lang_rule = array(
					'store_name_'.$get_lang_name => 'required',
					'store_add_one_'.$get_lang_name => 'required',
					'store_add_two_'.$get_lang_name => 'required',
				//	'meta_keyword_'.$get_lang_name => 'required',
				//	'meta_description_'.$get_lang_name => 'required',
					); 
					
					$rule  = array_merge($rule,$lang_rule);
			
				}
			}
            
            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('add_store/' . $merchant_id)->withErrors($validator->messages())->withInput();
            } else {
                $mer_email       = Input::get('email_id');
                $file            = Input::file('file');
                $time			 = time();
                $filename 		 = 'Store_' .$time.'.'. strtolower($file->getClientOriginalExtension());
                $destinationPath = './public/assets/storeimage/';
                Image::make($file)->save('./public/assets/storeimage/'.$filename,$this->image_compress_quality);               
                
                $store_entry = array(
                    'stor_name' => Input::get('store_name'),
                    'stor_merchant_id' => $merchant_id,
                    'stor_phone' => Input::get('store_pho'),
                    'stor_address1' => Input::get('store_add_one'),
                    'stor_address2' => Input::get('store_add_two'),
                    'stor_country' => Input::get('select_country'),
                    'stor_city' => Input::get('select_city'),
                    'stor_zipcode' => Input::get('zip_code'),
                    'stor_metakeywords' => Input::get('meta_keyword'),
                    'stor_metadesc' => Input::get('meta_description'),
                    'stor_website' => Input::get('website'),
                    'stor_latitude' => Input::get('latitude'),
                    'stor_longitude' => Input::get('longitude'),
                    'stor_img' => $filename
                );
				
				$lang_entry = array();
				if(!empty($get_active_lang)) { 
					foreach($get_active_lang as $get_lang) {
						$get_lang_name = $get_lang->lang_name;
						$get_lang_code = $get_lang->lang_code;
				
						 $lang_entry = array(
						'stor_name_'.$get_lang_code => Input::get('store_name_'.$get_lang_name),
						'stor_address1_'.$get_lang_code => Input::get('store_add_one_'.$get_lang_name),
						'stor_address2_'.$get_lang_code => Input::get('store_add_two_'.$get_lang_name),
						'stor_metakeywords_'.$get_lang_code => Input::get('meta_keyword_'.$get_lang_name),
						'stor_metadesc_'.$get_lang_code => Input::get('meta_description_'.$get_lang_name),
						); 
						
						$store_entry  = array_merge($store_entry,$lang_entry);
					}
				}
                
                Merchant::insert_store($store_entry);
				 if (Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_ADDED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_RECORD_ADDED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_RECORD_ADDED_SUCCESSFULLY');
			}
                return Redirect::to('manage_store/' . $merchant_id)->with('result',$session_message);
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }
    
    public function edit_store($id, $mer_id)
    {
        if (Session::has('userid')) {
			 if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_MERCHANT');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT');
			}
            $adminheader    = view('siteadmin.includes.admin_header')->with("routemenu", $session_message);
            $adminleftmenus = view('siteadmin.includes.admin_left_menu_merchant');
            $adminfooter    = view('siteadmin.includes.admin_footer');
            $country_return = Merchant::get_country_all_detail();
            $store_return   = Merchant::get_induvidual_store_detail($id);
            return view('siteadmin.edit_store')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('country_details', $country_return)->with('id', $id)->with('store_return', $store_return)->with('mer_id', $mer_id);
        } else {
            return Redirect::to('siteadmin');
        }
    }
    
    public function edit_store_submit()
    {
        if (Session::has('userid')) {
            $merchant_id = Input::get('mer_id');
            $store_id    = Input::get('store_id');
            $data        = Input::except(array(
                '_token'
            ));
            $rule        = array(
                'store_name' => 'required',
                'store_pho' => 'required|numeric',
                'store_add_one' => 'required',
                'select_country' => 'required',
                'select_city' => 'required',
                'zip_code' => 'required|numeric',
               // 'meta_keyword' => 'required',
              //  'meta_description' => 'required',
                'latitude' => 'required',
                'longitude' => 'required',
				'file' => 'image|mimes:jpeg,png,jpg|image_size:'.$this->store_width.','.$this->store_height.''
            );
            
			$get_active_lang = 	$this->get_active_language; 
			$lang_rule = array();
			if(!empty($get_active_lang)) { 
				foreach($get_active_lang as $get_lang) {
					$get_lang_name = $get_lang->lang_name;

					$lang_rule = array(
					'store_name_'.$get_lang_name => 'required',
					'store_add_one_'.$get_lang_name => 'required',
					'store_add_two_'.$get_lang_name => 'required',
				//	'meta_keyword_'.$get_lang_name => 'required',
				//	'meta_description_'.$get_lang_name => 'required',
					); 
					
					$rule  = array_merge($rule,$lang_rule);
			
				}
			}
			
            $validator = Validator::make($data, $rule);
            if ($validator->fails()) {
                return Redirect::to('edit_store/'.$store_id.'/'.$merchant_id)->withErrors($validator->messages())->withInput();
            } else {
                
                $file = Input::file('file');
                if ($file == '') {
                    $filename = Input::get('file_new');
                } else {
                    $time=time();
					$filename = 'Store_' .$time.'.'. strtolower($file->getClientOriginalExtension());
                    $destinationPath = './public/assets/storeimage/';
                    $old_store_image = Input::get('file_new');
					if(file_exists($destinationPath.$old_store_image))
					{
						@unlink($destinationPath.$old_store_image);
					}
                    Image::make($file)->save('./public/assets/storeimage/'.$filename,$this->image_compress_quality);
                }
                $store_entry = array(
                    'stor_name' => Input::get('store_name'),
                    'stor_phone' => Input::get('store_pho'),
                    'stor_address1' => Input::get('store_add_one'),
                    'stor_address2' => Input::get('store_add_two'),
                    'stor_country' => Input::get('select_country'),
                    'stor_city' => Input::get('select_city'),
                    'stor_zipcode' => Input::get('zip_code'),
                    'stor_metakeywords' => Input::get('meta_keyword'),
                    'stor_metadesc' => Input::get('meta_description'),
                    'stor_website' => Input::get('website'),
                    'stor_latitude' => Input::get('latitude'),
                    'stor_longitude' => Input::get('longitude'),
                    'stor_img' => $filename
                );
				
				$lang_entry = array();
				if(!empty($get_active_lang)) { 
					foreach($get_active_lang as $get_lang) {
						$get_lang_name = $get_lang->lang_name;
						$get_lang_code = $get_lang->lang_code;
				
						 $lang_entry = array(
						'stor_name_'.$get_lang_code => Input::get('store_name_'.$get_lang_name),
						'stor_address1_'.$get_lang_code => Input::get('store_add_one_'.$get_lang_name),
						'stor_address2_'.$get_lang_code => Input::get('store_add_two_'.$get_lang_name),
						'stor_metakeywords_'.$get_lang_code => Input::get('meta_keyword_'.$get_lang_name),
						'stor_metadesc_'.$get_lang_code => Input::get('meta_description_'.$get_lang_name),
						); 
						
						$store_entry  = array_merge($store_entry,$lang_entry);
					}
				}
                
                Merchant::edit_store($store_id, $store_entry);
				 if (Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_UPDATED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_RECORD_UPDATED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_RECORD_UPDATED_SUCCESSFULLY');
			}
                return Redirect::to('manage_store/' . $merchant_id)->with('result', $session_message);
            }
        } else {
            return Redirect::to('siteadmin');
        }
    }
    
    public function block_store($id, $status, $mer_id)
    {
        if (Session::has('userid')) {
            $check_country_status = Merchant::check_city_status($id);
            if($check_country_status>0){    //if country is active you can change status otherwise cant able to change mer status
                if ($status == 1) {  $store_city_status = 'A'; }elseif($status==0){ $store_city_status = 'B'; }
                $entry = array(
                'stor_status' => $status,
                'store_city_status'=>$store_city_status,
                );
            
            Merchant::block_store_status($id, $entry);
            if ($status == 1) {
				 if (Lang::has(Session::get('admin_lang_file').'.BACK_STORE_ACTIVATED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_STORE_ACTIVATED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_STORE_ACTIVATED_SUCCESSFULLY');
			}
                return Redirect::to('manage_store/' . $mer_id)->with('result', $session_message);
            } else {
				 if (Lang::has(Session::get('admin_lang_file').'.BACK_STORE_BLOCKED_SUCCESSFULLY')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_STORE_BLOCKED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_STORE_BLOCKED_SUCCESSFULLY');
			}
                return Redirect::to('manage_store/' . $mer_id)->with('result',$session_message);
            }
          }//if city is active only they can able to change able to block/unblock status of store
          else{
              return Redirect::to('manage_store/' . $mer_id)->with('result','Store City is blocked so we cant able to change the status of store');
          }
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function manage_enquiry()
    {
        if (Session::has('userid')) {
			 if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_MERCHANT');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT');
			}
            $adminheader                   = view('siteadmin.includes.admin_header')->with("routemenu", $session_message);
            $adminleftmenus                = view('siteadmin.includes.admin_left_menu_merchant');
            $adminfooter                   = view('siteadmin.includes.admin_footer');
            $merchant_return               = Merchant::view_merchant_details();
            $store_count                   = Merchant::get_store_count($merchant_return);
            $merchant_is_or_not_in_deals   = Merchant::merchant_is_or_not_in_deals($merchant_return);
            $merchant_is_or_not_in_product = Merchant::merchant_is_or_not_in_product($merchant_return);
            $merchant_is_or_not_in_auction = Merchant::merchant_is_or_not_in_auction($merchant_return);
            $enquiry_return                = Merchant::view_enquiry_details();
            $enquiry_count                 = Merchant::get_enquiry_count($enquiry_return);
            return view('siteadmin.manage_enquiry')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('merchant_return', $merchant_return)->with('store_count', $store_count)->with('merchant_is_or_not_in_deals', $merchant_is_or_not_in_deals)->with('merchant_is_or_not_in_product', $merchant_is_or_not_in_product)->with('merchant_is_or_not_in_auction', $merchant_is_or_not_in_auction)->with('enquiry_return', $enquiry_return)->with('enquiry_count', $enquiry_count);
        } else {
            return Redirect::to('siteadmin');
        }
    }

    public function check_mer_email(){
        $mer_email            = Input::get('email_id');
       
        $check_merchant_email = Merchant::check_merchant_email($mer_email);
        if($check_merchant_email!=0){
			 if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANT_EMAIL_ID_ALREADY_EXISTS')!= '')
 			{ 
				$session_message =  trans(Session::get('admin_lang_file').'.BACK_MERCHANT_EMAIL_ID_ALREADY_EXISTS');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_MERCHANT_EMAIL_ID_ALREADY_EXISTS');
			}
            echo $session_message;
        }
        exit();
    }

    public function check_store_exists(){
 
         $mer_id    =  $_GET['mer_id'];
         $longitude =  $_GET['longitude'];
         $latitude  =  $_GET['latitude'];

        $check_store_exist = Merchant::check_store_exist($mer_id,$latitude,$longitude);

        if($check_store_exist!=0){
            echo '1';
        }else if($check_store_exist==0){
            echo '0';
        }
        exit();
    }

    public function check_store_exists_edit(){
 
         $mer_id    =  $_GET['mer_id'];
         $longitude =  $_GET['longitude'];
         $latitude  =  $_GET['latitude'];
         $store_id  =  $_GET['store_id'];


        $check_store_exist_edit = Merchant::check_store_exist_edit($store_id,$mer_id,$latitude,$longitude);
        if($check_store_exist_edit){
            foreach ($check_store_exist_edit as $key ) {
                 $exist_store_id=$key->stor_id;
                }
             
        if($exist_store_id==$store_id){
        echo '0';
        exit();
        }
        }
        else {
        
            $check_store_exist = Merchant::check_store_exist($mer_id,$latitude,$longitude);
        if($check_store_exist!=0){
            echo '1';
        }else if($check_store_exist==0){
            echo '0';
        }
            }
        exit();
    }



    //Save Edited Image
    public function CropNdUpload_store(){
        //echo 'jhj';
       // var_dump($request->input('_token'));
        $data = Input::except(array(
                '_token'
            ));
       // print_r($data);exit();
        $store_id       = Input::get('product_id');
        $img_id         = Input::get('img_id');
        $mer_id         = Input::get('mer_id');
        $imgfileName    = Input::get('imgfileName'); //old image file name

        $imageData = Input::get('base64_imgData');
        $img_dat = explode(',',Input::get('base64_imgData'));
        $new_name = 'Deal_'.time().rand().'.png';

        $imageData         = base64_decode($img_dat[1]);

        $file_path = './public/assets/storeimage/'.$new_name;
        //Upload image with compression
        $img = Image::make(imagecreatefromstring($imageData))->save($file_path);
        //jpg background color black remove
        list($width, $height) = getimagesize($file_path);
        $output = imagecreatetruecolor($width, $height);
        $white = imagecolorallocate($output,  255, 255, 255);
        imagefilledrectangle($output, 0, 0, $width, $height, $white);
        imagecopy($output, imagecreatefromstring($imageData), 0, 0, 0, 0, $width, $height);
        imagejpeg($output, $file_path);
        //image compression
        $img = Image::make($output)->save($file_path,$this->image_compress_quality);

        if(file_exists('public/assets/storeimage/'.$new_name)){

            //upload small image
            list($width,$height)=getimagesize('public/assets/storeimage/'.$new_name);     
            

            //unlink old files starts
            if(file_exists('public/assets/storeimage/'.$imgfileName))
                unlink("public/assets/storeimage/".$imgfileName);
            if(file_exists('public/assets/storeimage/'.$imgfileName))
                unlink("public/assets/storeimage/".$imgfileName);
            //unlink old files ends

            //update in image table 
            $view_store_details = Merchant::get_induvidual_store_detail($store_id);

            $entry = array('stor_img' => $new_name );

             $return     = Merchant::edit_store($store_id, $entry);
             if(Lang::has(Session::get('admin_lang_file').'.BACK_IMAGE_UPDATED_SUCCESSFULLY')!= '') 
            {
                $session_message = trans(Session::get('admin_lang_file').'.BACK_IMAGE_UPDATED_SUCCESSFULLY');
            }
            else 
            {
                $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_IMAGE_UPDATED_SUCCESSFULLY');
            }
            return Redirect::to('edit_store/'.$store_id.'/'.$mer_id)->with('block_message', $session_message);
        }

        exit();
    }
	
	
             //merchant multiple block
	public function block_status_merchant_submit()
	{
		 if (Session::has('userid'))
        {
         $data=Input::get('val');
         $status=0;
         for($i=0;$i<count($data);$i++){
             $id=$data[$i];
              $array=array('stor_status' => 0); 
             $block_Merchant_admin=Merchant::status_merchant($id, $status);
            
                   }
            if($block_Merchant_admin>0)
            {
            echo "1";
                }else {
            echo "0";
                      }

        }	
	}
	
	public function unblock_status_merchant_submit()
	{
		if (Session::has('userid'))
        {
         $data=Input::get('val');
         $status=1;
         for($i=0;$i<count($data);$i++){
             $id=$data[$i];
              $array=array('stor_status' => 0); 
             $unblock_Merchant_admin=Merchant::status_merchant($id, $status);
            
                   }
            if($unblock_Merchant_admin>0)
            {
            echo "1";
                }else {
            echo "0";
                      }

        }	
	}
	
    /* Image  Crop , rorate and mamipulation ends */
    
}

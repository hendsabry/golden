<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Models;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon;
use File;
use App\Reviewrating;
use App\Categorylist;
use Intervention\Image\ImageManagerStatic as Image; 
use Auth;
use Lang;
use App\ProductsMerchant;
use App\HallOffer;
use App\ServiceStaff;
use App\StaffExperties;
use App\NmServicesAttribute;
use App\City;
use App\NmProductGallery;
class MerchantTravelController extends Controller
{
      public function __construct(){
            parent::__construct();
            // set admin Panel language
            $this->middleware(function ($request, $next) {
                        $this->setLanguageLocaleMerchant();
                        Session::get('mer_lang_code');
                        return $next($request);
           });
     }
    
 
        public function travelInfo(Request $request)
        {
        if (Session::has('merchantid')) 
        {   
            $mer_id   = Session::get('merchantid');
                $id = $request->id;
                 $itemid = $request->sid;
                 $fetchdata = array();
                $city = City::where('ci_con_id',10)->get();
                $getshopid = '';


                $ispackage = DB::table('nm_product')->where('pro_mc_id',$mer_id)->where('pro_status',1)->count();
                if($ispackage<1){
                  DB::table('nm_category')->where('mc_id', $mer_id)->update(['mc_status'=>0]);
                }

                $fetchdatacount = Categorylist::where('mc_id',$itemid)->where('vendor_id',$mer_id)->count();
                if($fetchdatacount >0) {
               $fetchdata = Categorylist::where('mc_id',$itemid)->where('vendor_id',$mer_id)->first();
               //$getshopid = $fetchdata->mc_id;
              }

               
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getPagelimit = config('app.paginate');
                
                

                return view('sitemerchant.travel-agency.travel-info', compact('merchantheader','merchantfooter','id','itemid','fetchdata','city'));  
    
        } else {
        return Redirect::to('sitemerchant');
        }
        }

        public function travelPictures(Request $request)
        {

        if (Session::has('merchantid')) 
        {
            $merid  = Session::get('merchantid');
            $parentid = $request->id;
            $itemid = $request->sid;
            $this->setLanguageLocaleMerchant();
            $mer_id              = Session::get('merchantid');             
            $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
            $merchantfooter     = view('sitemerchant.includes.merchant_footer');           
            
            $getDbC = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->count();
            
            $getVideos = DB::table('nm_category')->where('mc_id',$itemid)->where('vendor_id',$merid)->first();
            if($getDbC >=1)
            {
          
            $getDb = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->get();

            }
            else
            {
            $getDb = '';    
            }

          return view('sitemerchant.travel-agency.travel-picture', compact('merchantheader','merchantfooter','getCatlists','GetAlreadyServices','getDb','getDbC','getVideos','parentid','itemid'));       
        } else {
            return Redirect::to('sitemerchant');
    }
        }

        public function travelItems(Request $request)
        {
            if (Session::has('merchantid')) 
             {
                 $mer_id   = Session::get('merchantid');
                 $id = $request->id;
                 $sid = $request->sid;
                 $status = $request->status;
                 $search = $request->search;
                 $catname = $request->catid; 
                 $getPagelimit = config('app.paginate');

                 $productdata        = ProductsMerchant::where('pro_mc_id',$sid)->where('pro_mr_id',$mer_id);
                if($catname != '')
                {
                $productdata = $productdata->where('attribute_id',$catname); 
                }
                if($status != '')
                {
                $productdata = $productdata->where('pro_status',$status);
                }
                if($search != '')
                {
                    
                    if (\Config::get('app.locale') == 'ar'){
                    $productdata = $productdata->where('pro_title_ar','like', '%'.$search.'%');
                    }
                    else
                    {
                    $productdata = $productdata->where('pro_title','like', '%'.$search.'%');
                    }


                
                }

                $productdata = $productdata->orderBy('pro_id','desc')->paginate($getPagelimit)->appends(request()->query());
                  foreach ($productdata as $getproductid) {
                      $pro_id=$getproductid->pro_id;
                      $packagecreated = DB::table('nm_product_attribute')->where('product_id', '=', $pro_id)->where('attribute_title', '=', 'Package Date')->count();
                    if($packagecreated<1){
                       DB::table('nm_product')->where('pro_id', $pro_id)->update(['pro_status'=>0]);
                    }
                  }


                $attrcat            = NmServicesAttribute::where('vendor_id',$mer_id)->where('services_id',$sid)->get();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities          = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();


             
           


                return view('sitemerchant.travel-agency.travel-items', compact('merchantheader','merchantfooter','id','getCities','productdata','sid','attrcat','status','search','catname'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
        }

        public function travelOrders(Request $request)
        {
            if(Session::has('merchantid')) 
             {
                 $subproducttype = 'travel';
                 $mer_id         = Session::get('merchantid');
                 $id             = $request->id;
                 $sid            = $request->sid;
                 $searchkeyword  = $request->searchkeyword;
                 $date_to        = $request->date_to;
                 $End_Date       = $request->from_to;
                 $order_days     = $request->order_days;
                 $status         = $request->status;                
                 $serachfirstfrm = $request->serachfirstfrm;
                 $getorderedproducts = DB::table('nm_order_product')->where('product_type','travel')->where('product_sub_type','travel');
                  if($searchkeyword!='')
                  {
                    $getorderedproducts = $getorderedproducts->where('order_id',$searchkeyword);
                  }
                  if($status!='')
                  {
                    $getorderedproducts = $getorderedproducts->where('status',$status);
                  } 
                  if($date_to!='' && $End_Date!='')
                  {
                    $getorderedproducts = $getorderedproducts->whereDate('created_at', '>=', $date_to)->whereDate('created_at', '<=', $End_Date);
                  } 
                  $getorderedproducts = $getorderedproducts->where('merchant_id',$mer_id)->groupBy('order_id')->orderBy('created_at','DESC')->selectRaw('*,sum(nm_order_product.total_price) as sum')->get();      

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');               
 
                return view('sitemerchant.travel-agency.travel-order', compact('merchantheader','merchantfooter','id','sid','getorderedproducts','mer_id','searchkeyword'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
        }


       //ORDER DETAILS

         public function getTravelorderdetail(Request $request)
         {
          if(Session::has('merchantid')) 
          {
                 $mer_id    = Session::get('merchantid');
                 $id        = $request->id;
                 $sid       = $request->sid;
                 $proid     = $request->proid;
                 $cusid     = $request->cusid;
                 $ordid     = $request->ordid;
                 $productid = $request->productid;
                 $productdata = DB::table('nm_order_product')->where('product_type','travel')->where('order_id',$ordid)->where('product_sub_type','travel')->where('cus_id',$cusid)->where('order_id',$ordid)->orderBy('created_at','DESC')->get();
                 $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                 $merchantfooter     = view('sitemerchant.includes.merchant_footer');

                return view('sitemerchant.travel-agency.travelorderdetail', compact('merchantheader','merchantfooter','id','sid','proid','cusid','ordid','productdata','productid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
      }
     






        public function travelOffers(Request $request)
        {
            if (Session::has('merchantid')) 
             {   $getPagelimit = config('app.paginate');
                $id = $request->id;
                $sid = $request->sid;
                $autoid = $request->autoid;
                $status = $request->status;
                $search = $request->search;

             $mer_id   = Session::get('merchantid');
            $hallofferlist =  HallOffer::where('vendor_id',$mer_id)->where('pro_id',$sid);
            if($status!='')
            {
            $hallofferlist = $hallofferlist->where('status',$status);
            }
            if($search !='')
            {
            $mer_selected_lang_code = Session::get('mer_lang_code'); 
            if($mer_selected_lang_code !='en')
            {
            $hallofferlist = $hallofferlist->where('title_ar','LIKE','%' .$search.'%');
            }
            else
            {
            $hallofferlist = $hallofferlist->where('title','LIKE','%' .$search.'%');
 
            }
            }   
            $hallofferlist =  $hallofferlist->orderBy('id','DESC')->paginate($getPagelimit);

                $this->setLanguageLocaleMerchant();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
            return view('sitemerchant.travel-agency.travel-offer', compact('merchantheader','merchantfooter','hallofferlist','status','autoid','sid','id','search'));       
            } else {
                return Redirect::to('sitemerchant');
            }
        }



        public function travelReviews(Request $request)
        {
            if (Session::has('merchantid')) 
             {
                $mer_id          = Session::get('merchantid');
                $id              = $request->id;
                $getPagelimit    = config('app.paginate');
                $sid             = $request->sid;
                $merchantheader  = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter  = view('sitemerchant.includes.merchant_footer'); 
                $reviewrating    = Reviewrating::where('vandor_id',$mer_id)->where('shop_id',$sid)->orderBy('created_at','desc')->where('status',1)->paginate($getPagelimit)->appends(request()->query());

 
                return view('sitemerchant.travel-agency.travel-reviews-and-comments', compact('merchantheader','merchantfooter','id','reviewrating'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
        }




//Store shop Info
            public function storetravelShopInfo(Request $request)

            {
                $merid  = Session::get('merchantid');
                $id = $request->id;
                $itemid = $request->sid;
               if($itemid){
                  $savebranch =Categorylist::where('mc_id',$itemid)->first();
                } else { 
                 $savebranch = new Categorylist;
               }
           if($request->file('branchimage')){ 
                  $file = $request->branchimage;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                 $img = Image::make($imageRealPath); // use this if you want facade style code
                   $thumb_width = 220;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('uploadimage/travel' . '/'. $thumbName);
                  $file->move('uploadimage/travel/', $Image);
                  $savebranch->mc_img = url('').'/uploadimage/travel/'.$thumbName;
                }
                //Addressimage
                if($request->file('address_image')){ 
                  $file = $request->address_image;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                  $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('uploadimage/travel' . '/'. $thumbName);
                  $file->move('uploadimage/travel/', $Image);
                  $savebranch->address_image = url('').'/uploadimage/travel/'.$thumbName;
                }
              //Termaand condition
               if(!empty($request->mc_tnc)){ 
                  $file = $request->mc_tnc;
                  $orgname =  $file->getClientOriginalName();
                  $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                  $extension = $file->getClientOriginalExtension();
                  $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                  $file->move('./public/assets/storeimage/', $filename_c);                      
                  $Cname =   url('').'/public/assets/storeimage/'.$filename_c;
                  $savebranch->terms_conditions =$Cname;
                  
               }
               else {
                    $orgname = $request->tmcvalue; 
                }

  if(!empty($request->mc_tnc_ar)){ 
                  $file = $request->mc_tnc_ar;
                  $orgname_ar =  $file->getClientOriginalName();
                  $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                  $extension = $file->getClientOriginalExtension();
                  $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                  $file->move('./public/assets/storeimage/', $filename_c);                      
                  $Cname_ar =   url('').'/public/assets/storeimage/'.$filename_c;
                  $savebranch->terms_conditions_ar =$Cname_ar;
                  
               }
               else {
                    $orgname_ar = $request->tmcvalue_ar; 
                }

                  $savebranch ->mc_status = $request->mc_status;
                  $savebranch ->longitude = $request->longitude;
                  $savebranch ->latitude = $request->latitude;

                  $savebranch ->terms_condition_name_ar = $orgname_ar;
                  $savebranch ->mc_name = $request->mc_name;
                  $savebranch ->mc_name_ar = $request->mc_name_ar;
                  $savebranch ->vendor_id = $merid;
                  $savebranch ->google_map_address = $request->google_map_address;
                  $savebranch ->address = $request->address;
                  $savebranch ->address_ar = $request->address_ar;
                  $savebranch ->city_id = $request->city_id;
                  $savebranch ->parent_id = $id;
                  $savebranch ->mc_discription = $request->about;
                  $savebranch ->mc_discription_ar = $request->about_ar;
                  $savebranch ->city_id = $request->city_id;
                  $savebranch ->terms_condition_name = $orgname;
 
   
                  
                if($savebranch->save()){
                

            if($itemid==''){
            $itemid = $savebranch->mc_id;
            }
            if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "تم حفظ معلومات المتجر بنجاح");
             }
             else
             {
             Session::flash('message', "Shop information successfully saved");
             }

                 return redirect()->route('travel-info',['id' => $id,'sid' =>$itemid]);
                }   
            }

   
public function traveladdItems(Request $request)
{
   if (Session::has('merchantid')) 
              {
                  $mer_id   = Session::get('merchantid');
                  $id = $request->id;
                  $itemid = $request->sid;
                  $autoid = $request->autoid;
                  $fetchdata = ProductsMerchant::where('pro_mr_id',$mer_id)->where('pro_id',$autoid)->first();
                  $attributesname =  NmServicesAttribute::where('services_id',$itemid)->get(); 
                  $proid = $request->id;
                  $getDbC = DB::table('nm_product_gallery')->where('vendor_id', $mer_id)->where('product_id', $autoid)->count();
                  $getVideos = DB::table('nm_product')->where('pro_id',$autoid)->where('pro_mr_id',$mer_id)->first();
                  if($getDbC >=1)
                  {
                  $getDb = DB::table('nm_product_gallery')->where('vendor_id', $mer_id)->where('product_id', $autoid)->get();
                  }
                  else
                  {
                  $getDb = '';    
                  }
   

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 

                $Location = DB::table('nm_product_attribute')->where('vendor_id', $mer_id)->where('product_id', $autoid)->where('attribute_title', 'Location')->first();   

                $PackageDuration = DB::table('nm_product_attribute')->where('vendor_id', $mer_id)->where('product_id', $autoid)->where('attribute_title', 'Package Duration')->first();   

                $ExtraService = DB::table('nm_product_attribute')->where('vendor_id', $mer_id)->where('product_id', $autoid)->where('attribute_title', 'Extra Service')->first();   
                
                 $PackageDate = DB::table('nm_product_attribute')->where('vendor_id', $mer_id)->where('product_id', $autoid)->where('attribute_title', 'Package Date')->first();   
 


 $productGallery = NmProductGallery::where('vendor_id',$mer_id)->where('product_id',$autoid)->get();
                return view('sitemerchant.travel-agency.travel-add-items', compact('merchantheader','merchantfooter','id','itemid','getDbC','getDb','attributesname','autoid','fetchdata','Location','PackageDuration','ExtraService','PackageDate','productGallery'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
}

 
 
 //End Store
    public function traveladdoffers(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $getPagelimit = config('app.paginate');
                $sid = $request->sid;
                $autoid = $request->autoid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
               $reviewrating = Reviewrating::where('vandor_id',$mer_id)->paginate($getPagelimit)->appends(request()->query());
                $categorysave = HallOffer::where('id',$autoid)->where('vendor_id',$mer_id)->first(); 
 
                return view('sitemerchant.travel-agency.travel-add-offer', compact('merchantheader','merchantfooter','id','sid','autoid','reviewrating','categorysave'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     } 

      //STORE OFFER
       public function storeMakeOffer(Request $request)
       {
            if (Session::has('merchantid')) 
        {      
        $mer_id   = Session::get('merchantid');
        $id = $request->parentid;
        $sid = $request->sid;
        $autoid = $request->autoid;
        if($autoid =='')
        {
        $categorysave = new HallOffer; 
        }
        else
        {
        $categorysave = HallOffer::where('id',$autoid)->where('vendor_id',$mer_id)->first();
        }
        $categorysave->vendor_id    = $mer_id; 
        $categorysave->title        =$request->title;
        $categorysave->title_ar    = $request->title_ar;
        $categorysave->start_date     = $request->Date_from;
        $categorysave->end_date    = $request->date_to;
        $categorysave->discount   = $request->discount;
        $categorysave->coupon   = $request->coupon;
        $categorysave->pro_id   = $sid;
        $categorysave->status    = 1;
        if (\Config::get('app.locale') == 'ar'){
        Session::flash('message', "تمت إضافة العرض بنجاح  ");
        }
        else
        {
        Session::flash('message', "Offer successfully saved");
        }
        $categorysave->save(); 
        return redirect()->route('travel-offer',['id' => $id,'itemid' => $sid]);
       } else {
      return Redirect::to('sitemerchant');
      } 
       }
     //END OFFER  

            //Store picture and  video url
      public function storePictureVideourl(Request $request)
      {
          
         if (Session::has('merchantid')) 
                {

                    $validatedData = $request->validate([
                          //'image' => 'required',
                         //'youtubevideo' => 'required',
                        // 'image.*' => 'image|mimes:jpg,jpeg,png,gif'
 
                    ]); 
 

                    $merid  = Session::get('merchantid');
                    $this->setLanguageLocaleMerchant();
                    $mer_id              = Session::get('merchantid');  
                    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              
                    //Insert images in folder
                          $catid = $request->parent_id;
                          $sid = $request->itemid;

                    if($files=$request->file('image')){
                    foreach($files as $file){
                    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
                    //$file->move('hallpics',$name); 
                    $extension = $file->getClientOriginalExtension();
                    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
                    //$file->move('hallpics', $fileName);               
                    $imageRealPath  =   $file->getRealPath();
                    $thumbName      =   'thumb_'. $fileName;       
                    $img = Image::make($imageRealPath); // use this if you want facade style code
                     $thumb_width = 100;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);

                  
                    $img->save('uploadimage/travel/picturs' . '/'. $thumbName);
                    $file->move('uploadimage/travel/picturs/', $fileName);
                    $shop_Img = url('/').'/uploadimage/travel/picturs/'.$thumbName; 
                       
                    /*Insert data to db*/
                    DB::table('nm_category_gallery')->insert( [
                    'image'=>   $shop_Img,
                    'category_id' =>$sid,
                    'vendor_id' => $mer_id,
                    ]);
                   }  

                 } 
                   
                    $youtubevideoa = str_replace('youtu.be','www.youtube.com/embed',$request->youtubevideo);
                    $mc_video_description = $request->about_video;
                    $mc_video_description_ar = $request->about_video_ar;


                    /*Update data to db*/
                    DB::table('nm_category')->where('mc_id',$sid)->where('vendor_id',$mer_id)->update( ['mc_video_url'=>$youtubevideoa,'mc_video_description'=> $mc_video_description,'mc_video_description_ar'=> $mc_video_description_ar]);


                    if($mc_video_description != '' || $mc_video_description_ar != '' || $youtubevideoa != '' || $request->file('image') !='')
                    {
                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تمت إضافة الخدمة");
                    }
                    else
                    {
                    Session::flash('message', "Record saved successfully");
                    }
                    // language for display message //   

                    }
 

                  return Redirect::back();      
                } else {
                        return Redirect::to('sitemerchant');
                }
      }  


//Store services
     public function storeService(Request $request)
      {        
        if (Session::has('merchantid')) 
                {
              $merid  = Session::get('merchantid');
              $model_name = $request->model_name;
              $model_name_ar = $request->model_name_ar;
              $price = $request->price;
              $location = $request->location;
              $package_duration = $request->package_duration;
              $extra_service = $request->extra_service;

              $location_ar = $request->location_ar;
              $package_duration_ar = $request->package_duration_ar;
              $extra_service_ar = $request->extra_service_ar;

              $packageschedule = $request->package_schedule;
               $packageschedulde = implode(',', $packageschedule); 
            
              $description = $request->description;
              $description_ar = $request->description_ar;
              $parent_id = $request->id;
              $itemid = $request->sid;
              $autoid = $request->autoid;

               $discount = $request->discount;
                 if($discount!='' && $discount!='0') {
                     $discountprice =   $price-($price * ($discount/100));
                    } else {
                       $discountprice = 0;

                     }
  

              
               if($autoid){
                  $savebranch =ProductsMerchant::where('pro_id',$autoid)->first();
                } else { 
                  $savebranch = new ProductsMerchant;
                }
             if($request->file('stor_img')){ 
                  $file = $request->stor_img;                  
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                 $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 272;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('uploadimage/travel' . '/'. $thumbName);
                  $file->move('uploadimage/travel/', $Image);
                  $savebranch->pro_Img = url('').'/uploadimage/travel/'.$thumbName;
                }

                   
                  $savebranch ->pro_title = $request->model_name;
                  $savebranch ->pro_title_ar = $request->model_name_ar;
                  $savebranch ->pro_mr_id = $merid;               
                  $savebranch ->pro_price = $price;            
                  $savebranch ->pro_desc = $request->description;
                  $savebranch ->pro_desc_ar = $request->description_ar;
                  $savebranch ->pro_mc_id = $itemid;

                 $savebranch ->pro_disprice = $discountprice;
                 $savebranch ->pro_discount_percentage = $discount;

                   if($autoid==''){
                      $savebranch->pro_status  = 1;
                    }

                if($savebranch->save()){
                   $id = $request->id;
                    $pro_id = $savebranch->pro_id;
                   }    
 
            DB::table('nm_product_attribute')->where('product_id',$pro_id)->where('vendor_id',$merid)->delete();
            DB::table('nm_product_attribute')->insert(['product_id' => $pro_id, 'attribute_title' => 'Location','vendor_id' =>$merid, 'attribute_title_ar' => 'موقعك ','value' => $location,'value_ar' => $location_ar,'status' => 1]);
            DB::table('nm_product_attribute')->insert(['product_id' => $pro_id, 'attribute_title' => 'Package Duration','vendor_id' => $merid, 'attribute_title_ar' => 'مدة الحزم ','value' => $package_duration,'value_ar' => $package_duration_ar,'status' => 1]);
            DB::table('nm_product_attribute')->insert(['product_id' => $pro_id, 'attribute_title' => 'Extra Service','vendor_id' => $merid, 'attribute_title_ar' => 'خدمة إضافية ','value' => $extra_service,'value_ar' => $extra_service_ar,'status' => 1]);
             DB::table('nm_product_attribute')->insert(['product_id' => $pro_id, 'attribute_title' => 'Package Date','vendor_id' => $merid, 'attribute_title_ar' => 'تاريخ الحزمة ','value' => $packageschedulde,'status' => 1]);
                     




// FOR PRODUCT MULTIPLE IMAGES UPLOAD //
    $files=$request->file('image');
    $newFiles = array();
    $merid  = Session::get('merchantid');
    if($files=$request->file('image')){
    foreach($files as $key=>$val){
    array_push($newFiles, $key);
    }

    $privius=$request->privius;
    $newFilesMatch = array();
    if(isset($privius) && $privius!='')
    {
    foreach($privius as $key=>$val){
    array_push($newFilesMatch, $key);
    }  
    }

    $CheckInfo = array_intersect($newFiles, $newFilesMatch);
    if(isset($privius) && $privius!='')
    {
    foreach($privius as $key=>$val){
    if(in_array($key, $CheckInfo))
    {
    DB::table('nm_product_gallery')->where('id',$val)->where('vendor_id',$merid)->delete(); 
    }
    }
    } 
    foreach($files as $file){
    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
    $extension = $file->getClientOriginalExtension();
    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
    $imageRealPath  =   $file->getRealPath();
    $thumbName      =   'thumb_'. $fileName;       
    $img = Image::make($imageRealPath); // use this if you want facade style code
    $thumb_width = 272;
    list($width,$height) = getimagesize($imageRealPath);
    $thumb_height = ($thumb_width/$width) * $height;
    $img->resize($thumb_width,$thumb_height);                  
    $img->save('uploadimage/travel' . '/'. $thumbName);
    $file->move('uploadimage/travel/', $fileName);
    $shop_Img = url('/').'/uploadimage/travel/'.$thumbName; 
    /*Insert data to db*/
    DB::table('nm_product_gallery')->insert( [
    'image'=>   $shop_Img,
    'product_id' =>$pro_id,
    'status' => 1,
    'vendor_id' => $merid,
    ]);
    }  
        } 


 
// FOR PRODUCT MULTIPLE IMAGES UPLOAD END //




                     
            if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "تم حفظ الحزمة بنجاح");
             }
             else
             {
             Session::flash('message', "Package successfully saved");
             }
            return redirect()->route('travel-items',['id' => $parent_id,'sid' =>$itemid]);
               
        }
        else
        {
          return Redirect::to('sitemerchant');
        }
  }

 } // end 

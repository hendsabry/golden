<?php
namespace App\Http\Controllers;

use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Coupon;
use App\Http\Requests;

use App\BusinessOccasionType;
use App\City;

use MyPayPal;
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
use App\Formbusinesstype;

use App\StaticContent;
use CodeItNow\BarcodeBundle\Utils\QrCode;
use App\InvitationList;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\JWT;
use Tymon\JWTAuth\JWTGuard;
use JWTAuth;
use App\User;
use Auth;
use Response;
 
use Exception;

use Storage;

class FrontendController extends Controller
{
		public function __construct(){
        parent::__construct();
        // set frontend language 
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });
       
    } 
	 public function index(Request $request){
	//print_r(Session::get('customerdata'));
		$getbasecategory         = Home::getbasecategory();	
		$bussinesstype=array(); 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://www.wisitech.in/testindex.php?userip='.$_SERVER['REMOTE_ADDR']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
		$data = curl_exec($ch);		
		$getcityidAttr = DB::table('nm_city')->where('ci_name', $data)->where('ci_status', '1')->first();
		if(isset($getcity) && count($getcityidAttr)>0){
			$cityautoid=$getcityidAttr->ci_id;
		}else{
			$cityautoid='';
		}
		//Session::put('user_currentcity', $data);
		Session::put('user_currentcity', $cityautoid);
		curl_close($ch);
		$this->clearcart();
		//$this->checkCardemo();
//die;

		 $user_currentcity   = Session::get('user_currentcity'); 
		return view('newWebsite.index',compact('getbasecategory','business_occasion','cities','bussinesstype'));
      
    }



public function checkCardemo(Request $request)
{
 		$product_id = $request->product_id;
 		$bookingf = $request->bookingdate_from;
 		$bookingt = $request->bookingdate_to;
 		//From front end
		$CarBookingdate_from =date("Y-m-d", strtotime($bookingf));
		$CarBookingdate_to = date("Y-m-d", strtotime($bookingt)); 
		$getcarQty = DB::table('nm_product')->select('pro_qty')->where('pro_id',$product_id)->first();
		$carTotalQty = $getcarQty->pro_qty;
		$Cbdate_from = strtotime($CarBookingdate_from); 
		$Cbdate_to = strtotime($CarBookingdate_to);

		if($Cbdate_to < $Cbdate_from)
		{
		echo "99";
		die;
		} 

		$CarDatebook = array();
		 for ($i=$Cbdate_from; $i<=$Cbdate_to; $i+=86400) {  
		 array_push($CarDatebook, date("Y-m-d", $i));  
		} 
		//From front end 
		$getInfocount = DB::table('nm_order_product_rent')->where('product_id',$product_id)->where('product_type','car_rental')->count();

	$alreadyBook = array();
	if($getInfocount >=1)
	{
		$getInfo = DB::table('nm_order_product_rent')->where('product_id',$product_id)->where('product_type','car_rental')->get();
		$datetoarray = array();
		$datefromarray = array();
		$k=1;
		foreach($getInfo as $getinf)
		{
		 

		$dateChk = date("Y-m-d", strtotime($getinf->rental_date)); 
		$dateChka = date("Y-m-d", strtotime($getinf->return_date)); 
		array_push($datetoarray, $dateChka);
		array_push($datefromarray, $dateChk);
		}

		$dateFix = array();
		for($j=0; $j<count($datefromarray);$j++)
		{
		$Fromdate = strtotime($datefromarray[$j]);
		$Todate = strtotime($datetoarray[$j]);
		for ($M=$Fromdate; $M<=$Todate; $M+=86400) {  
		array_push($dateFix, date("Y-m-d", $M));  
		} 
		} 

		$getArry = array_count_values($dateFix); 
		foreach($CarDatebook as $bookd)
		{
			foreach($getArry as $key=>$val)
			{
			if($key==$bookd)
			{
 			array_push($alreadyBook, $val);
			}
			}
		}
	} 


		if(count($alreadyBook) >=1){
		  $totalQtyBook = max($alreadyBook);
		}
		else
		{
		$totalQtyBook =0;
		}
 
		 if($carTotalQty > $totalQtyBook)
		 {
		 	echo "1";
		 }
		 else
		 {
		 	echo "0";
		 }

}


 

public function checkDressrent(Request $request)
{
 		$product_id = $request->product_id;
 		$product_size = $request->product_size;
 		$bookingf = $request->bookingdate_from;
 		$bookingt = $request->bookingdate_to;
 		//From front end
		$CarBookingdate_from =date("Y-m-d H:i A", strtotime($bookingf));
		$CarBookingdate_to = date("Y-m-d H:i A", strtotime($bookingt)); 
		$getcarQty = DB::table('nm_product')->select('pro_qty')->where('pro_id',$product_id)->first();

$getcarQtya = DB::table('nm_product_option_value')->select('value_ar as value')->where('option_title',$product_size)->where('product_id',$product_id)->first();
 

		  $carTotalQty = $getcarQtya->value;
		$Cbdate_from = strtotime($CarBookingdate_from); 
		$Cbdate_to = strtotime($CarBookingdate_to);

		if($Cbdate_to < $Cbdate_from)
		{
		echo "99";
		die;
		} 

		$CarDatebook = array();
		 for ($i=$Cbdate_from; $i<=$Cbdate_to; $i+=3600) {  
		 array_push($CarDatebook, date("Y-m-d H:i A", $i));  
		} 
		//From front end 
		$getInfocount = DB::table('nm_order_product_rent')->where('product_id',$product_id)->where('product_type','shopping')->count();

	$alreadyBook = array();
	if($getInfocount >=1)
	{
		$getInfo = DB::table('nm_order_product_rent')->where('product_id',$product_id)->where('product_type','shopping')->get();
		$datetoarray = array();
		$datefromarray = array();
		$k=1;
		foreach($getInfo as $getinf)
		{

			$dateChk = date("Y-m-d H:i A", strtotime($getinf->rental_time)); 
		$dateChka = date("Y-m-d H:i A", strtotime($getinf->return_time)); 
		array_push($datetoarray, $dateChka);
		array_push($datefromarray, $dateChk);


		}

		$dateFix = array();
		for($j=0; $j<count($datefromarray);$j++)
		{
		$Fromdate = strtotime($datefromarray[$j]);
		$Todate = strtotime($datetoarray[$j]);
		for ($M=$Fromdate; $M<=$Todate; $M+=3600) {  
		array_push($dateFix, date("Y-m-d H:i A", $M));  
		} 
		} 
 
		$getArry = array_count_values($dateFix); 
		foreach($CarDatebook as $bookd)
		{
			foreach($getArry as $key=>$val)
			{
			if($key==$bookd)
			{
 			array_push($alreadyBook, $val);
			}
			}
		}
	} 
 
		if(count($alreadyBook) >=1){
		  $totalQtyBook = max($alreadyBook);
		}
		else
		{
		$totalQtyBook =0;
		}
 
		 if($carTotalQty > $totalQtyBook)
		 {
		 	echo "1";
		 }
		 else
		 {
		 	echo "0";
		 }

}




public function checkAcosticsrent(Request $request)
{
 		$product_id = $request->product_id;
 		$product_size = $request->product_size;
 		$bookingf = $request->bookingdate_from;
 		$bookingt = $request->bookingdate_to;
 		//From front end
		$CarBookingdate_from =date("Y-m-d H:i A", strtotime($bookingf));
		$CarBookingdate_to = date("Y-m-d H:i A", strtotime($bookingt)); 
		$getcarQty = DB::table('nm_product')->select('pro_qty')->where('pro_id',$product_id)->first();
$getcarQtya = DB::table('nm_product_option_value')->select('value')->where('option_title','Quantity in stock')->where('product_id',$product_id)->first();



		  $carTotalQty = $getcarQtya->value;
		$Cbdate_from = strtotime($CarBookingdate_from); 
		$Cbdate_to = strtotime($CarBookingdate_to);

		if($Cbdate_to < $Cbdate_from)
		{
		echo "99";
		die;
		} 

		$CarDatebook = array();
		 for ($i=$Cbdate_from; $i<=$Cbdate_to; $i+=3600) {  
		 array_push($CarDatebook, date("Y-m-d H:i A", $i));  
		} 
		//From front end 
		$getInfocount = DB::table('nm_order_product_rent')->where('product_id',$product_id)->where('product_type','music')->count();

	$alreadyBook = array();
	if($getInfocount >=1)
	{
		$getInfo = DB::table('nm_order_product_rent')->where('product_id',$product_id)->where('product_type','music')->get();
		$datetoarray = array();
		$datefromarray = array();
		$k=1;
		foreach($getInfo as $getinf)
		{

			$Froms = $getinf->rental_date.' '.$getinf->rental_time;
$tos = $getinf->return_date.' '.$getinf->return_time;
			$dateChk = date("Y-m-d H:i A", strtotime($Froms)); 
		$dateChka = date("Y-m-d H:i A", strtotime($tos)); 
		array_push($datetoarray, $dateChka);
		array_push($datefromarray, $dateChk);


		}

		$dateFix = array();
		for($j=0; $j<count($datefromarray);$j++)
		{
		$Fromdate = strtotime($datefromarray[$j]);
		$Todate = strtotime($datetoarray[$j]);
		for ($M=$Fromdate; $M<=$Todate; $M+=3600) {  
		array_push($dateFix, date("Y-m-d H:i A", $M));  
		} 
		} 
 
		$getArry = array_count_values($dateFix); 
		foreach($CarDatebook as $bookd)
		{
			foreach($getArry as $key=>$val)
			{
			if($key==$bookd)
			{
 			array_push($alreadyBook, $val);
			}
			}
		}
	} 
 
		if(count($alreadyBook) >=1){
		  $totalQtyBook = max($alreadyBook);
		}
		else
		{
		$totalQtyBook =0;
		}
 
		 if($carTotalQty > $totalQtyBook)
		 {
		 	echo "1";
		 }
		 else
		 {
		 	echo "0";
		 }

}








public function clearcart()
		{
		  	    $lesstime	= date("Y-m-d H:i:s", strtotime("-12 hours")); 
				$getInfo = DB::table('nm_cart')->where('created_at', '<', $lesstime)->get();
				foreach($getInfo as $val)
				{
				$cartId = $val->id;
					if($cartId !='')
					{
					DB::table('nm_cart')->where('id', $cartId)->delete();
					DB::table('nm_cart_body_measurement')->where('cart_id', $cartId)->delete();
					DB::table('nm_cart_external_food_dish')->where('cart_id', $cartId)->delete();
					DB::table('nm_cart_internal_food_dish')->where('cart_id', $cartId)->delete();
					DB::table('nm_cart_option')->where('cart_id', $cartId)->delete();
					DB::table('nm_cart_option_value')->where('cart_id', $cartId)->delete();
					DB::table('nm_cart_product')->where('cart_id', $cartId)->delete();
					DB::table('nm_cart_product_attribute')->where('cart_id', $cartId)->delete();
					DB::table('nm_cart_product_rent')->where('cart_id', $cartId)->delete();
					DB::table('nm_cart_product_to_package')->where('cart_id', $cartId)->delete();
					DB::table('nm_cart_reception_hospitality')->where('cart_id', $cartId)->delete();
					DB::table('nm_cart_services_attribute')->where('cart_id', $cartId)->delete();
					DB::table('nm_cart_services_staff')->where('cart_id', $cartId)->delete();
					}
				} 
		}


	
	public function getbussinessbycategoryid(Request $request){
				$categoryid=$request->category_id;
				if(Session::get('lang_file')=='ar_lang'){
	          $business_occasion = BusinessOccasiontype::select('id', 'title_ar as title')->where('top_category', '=', $categoryid)->orderBy('title', 'ASC')->get();
                     
		}else{
				$business_occasion = BusinessOccasiontype::select('id', 'title')->where('top_category', '=', $categoryid)->orderBy('title', 'ASC')->get();
                   
		}
		
		
		 return response()->json($business_occasion);
	
}
	public function electbarcode($id)
	{




				$qrCode = new QrCode();

                    $barcode_value = $id;

               $qrCode->setText($barcode_value)
                ->setSize(300)
                ->setPadding(10)
                ->setErrorCorrection('high')
                ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
                ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
                ->setLabel('Scan Qr Code')
                ->setLabelFontSize(16)
                ->setImageType(QrCode::IMAGE_TYPE_PNG)
                ;
                $barcode = 'data:'.$qrCode->getContentType().';base64,'.$qrCode->generate();
                $png_url = "barcode-".time().".png";
                $path = './public/assets/adimage/' . $png_url;
                $imageUploadSuccess =  Image::make(file_get_contents($barcode))->save($path);
                  

		 
			return view('elect-barcode',compact('id','path'));
	}
	public function aboutus()
	{
			$staticcontent= new StaticContent();
			$lang=Session::get('lang_file');
			$cmscontent=$staticcontent->Getcmsstaticcontent('16',$lang);
			return view('newWebsite.about-us',compact('cmscontent'));
	}
	public function testimonials()
	{
		$staticcontent= new StaticContent();
			$lang=Session::get('lang_file');
			$cmscontent=$staticcontent->Getcmsstaticcontent('15',$lang);
		return view('newWebsite.testimonial',compact('cmscontent'));
	}




public function occasionsdetails(Request $request)
{
	$uid=$request->uid;
	$cid=$request->cid;
	$infy = array();
   	$getOccsaions = DB::table('nm_occasions')->where('posted_by', '=', 0)->orderBy('created_at','DESC')->paginate(112); 
	$infy = array();
 	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://www.wisitech.in/testindex.php?userip='.$_SERVER['REMOTE_ADDR']);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
	$data = curl_exec($ch);

	Session::put('user_currentcity', $data);
	curl_close($ch);
	$this->clearcart();
	$user_currentcity   = Session::get('user_currentcity'); 
    $cityname =$user_currentcity;
    $getCitycount = DB::table('nm_city')->where('ci_name', '=', $cityname)->count();  
    if($getCitycount>=1)
    {
    $getCity = DB::table('nm_city')->where('ci_name', '=', $cityname)->first();
    $cid = $getCity->ci_id;
    $getCatID = array();

    $getOccsaioncount = DB::table('nm_occasions')->where('city_id', '=', $cid)->count();
	if($getOccsaioncount >=1)
	{

	$getOcc = $request->occasion;
	 
	$getOccsaions = DB::table('nm_occasions')->where('city_id', '=', $cid)->where('posted_by', '=', $uid)->groupBy('posted_by')->orderBy('created_at','DESC')->paginate(2); 	
	  

foreach ($getOccsaions as $vlue)
 {
   $vlue = $vlue->posted_by;
	$getOccsaion = DB::table('nm_occasions')->where('posted_by', '=', $vlue)->where('city_id', '=', $cid)->orderBy('created_at','DESC')->get(); 
    foreach($getOccsaion as $val)
    {
        $gets = array();
        $getts = array();

        $getCatID = $val->id;
        $posted_by = $val->posted_by;
        $getCinfoc = DB::table('nm_customer')->where('cus_id', '=', $posted_by)->count();
        if($getCinfoc >=1)
        {
        $getCinfo = DB::table('nm_customer')->where('cus_id', '=', $posted_by)->first();
        $val->posted_by_name = $getCinfo->cus_name;  
        }
        else
        {
        $val->posted_by_name ='';  
        }

        $getPhotos = DB::table('nm_occasions_gallery')->where('occasion_id', $getCatID)->get(); 
        foreach($getPhotos as $valu)
        {      
        array_push($gets, $valu->image_url);
        array_push($getts, $valu->created_at);
        }
        $val->image_url = $gets;
        $val->created_at = $getts;
     }
 array_push($infy, $getOccsaion);
 }
}
}
 
 return view('occasions_details',compact('cmscontent','infy','getOccsaions'));
 
}



	public function occasions(Request $request)
	{
		$staticcontent= new StaticContent();
			$lang=Session::get('lang_file');
			$cmscontent=$staticcontent->Getcmsstaticcontent('6',$lang);
 $getOccsaions = DB::table('nm_occasions')->where('posted_by', '=', 0)->where('status', '=', 1)->orderBy('created_at','DESC')->paginate(112); 
	$infy = array();
 	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://www.wisitech.in/testindex.php?userip='.$_SERVER['REMOTE_ADDR']);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
	$data = curl_exec($ch);
	
	Session::put('user_currentcity', $data);
	curl_close($ch);
	$this->clearcart();
	$user_currentcity   = Session::get('user_currentcity'); 
 
 	$cityname =$user_currentcity;  
 	//$cityname = 'Hofuf';
    $getCitycount = DB::table('nm_city')->where('ci_name', '=', $cityname)->count(); 

    if($getCitycount>=1)
    {
    $getCity = DB::table('nm_city')->where('ci_name', '=', $cityname)->first();
     $cid = $getCity->ci_id;
    $getCatID = array();

    $getOccsaioncount = DB::table('nm_occasions')->where('city_id', '=', $cid)->where('status', '=', 1)->count();
	if($getOccsaioncount >=1)
	{

	$getOcc = $request->occasion;
	if($getOcc!='')
	{
	$getOccsaions = DB::table('nm_occasions')->where('occasion_type', '=', $getOcc)->where('status', '=', 1)->where('city_id', '=', $cid)->groupBy('posted_by')->orderBy('created_at','DESC')->paginate(2); 
	}
	else
	{
	$getOccsaions = DB::table('nm_occasions')->where('city_id', '=', $cid)->where('status', '=', 1)->groupBy('posted_by')->orderBy('created_at','DESC')->paginate(2); 	
	}  

foreach ($getOccsaions as $vlue)
 {
   $vlue = $vlue->posted_by;
	$getOccsaion = DB::table('nm_occasions')->where('posted_by', '=', $vlue)->where('status', '=', 1)->where('city_id', '=', $cid)->orderBy('created_at','DESC')->get(); 
    foreach($getOccsaion as $val)
    {
        $gets = array();
        $getts = array();

        $getCatID = $val->id;
        $posted_by = $val->posted_by;
        $getCinfoc = DB::table('nm_customer')->where('cus_id', '=', $posted_by)->count();
        if($getCinfoc >=1)
        {
        $getCinfo = DB::table('nm_customer')->where('cus_id', '=', $posted_by)->first();
        $val->posted_by_name = $getCinfo->cus_name;  
        }
        else
        {
        $val->posted_by_name ='';  
        }

        $getPhotos = DB::table('nm_occasions_gallery')->where('occasion_id', $getCatID)->take(4)->get(); 
        foreach($getPhotos as $valu)
        {      
        array_push($gets, $valu->image_url);
        array_push($getts, $valu->created_at);
        }
        $val->image_url = $gets;
        $val->created_at = $getts;
     }
 array_push($infy, $getOccsaion);
 }
}
}
 
 return view('newWebsite.occasions',compact('cmscontent','infy','getOccsaions'));
	}
	public function privacypolicy()
	{
		$staticcontent= new StaticContent();
			$lang=Session::get('lang_file');
			$cmscontent=$staticcontent->Getcmsstaticcontent('17',$lang);
		return view('newWebsite.privacy-policy',compact('cmscontent'));
	}
	public function contactus(Request $request)
	{  
		$forapp = $request->forapp;
		$staticcontent= new StaticContent();
			$lang=Session::get('lang_file');
			$cmscontent=$staticcontent->Getcmsstaticcontent('19',$lang);
		return view('newWebsite.contact-us',compact('cmscontent','forapp'));
	}
  
  public function returnPolicy()
  {
  	$staticcontent= new StaticContent();
			$lang=Session::get('lang_file');
			$cmscontent=$staticcontent->Getcmsstaticcontent('18',$lang);
  	return view('newWebsite.return-policy',compact('cmscontent'));
  }


public function hashoccasion(Request $request)
{

 try {
    //$cityname = $request->cityname;
    $cityname ='Hofuf';
    $getCitycount = DB::table('nm_city')->where('ci_name', '=', $cityname)->count();  
    if($getCitycount>=1)
    {
    $getCity = DB::table('nm_city')->where('ci_name', '=', $cityname)->first();
    $cid = $getCity->ci_id;
    $getCatID = array();

    $getOccsaioncount = DB::table('nm_occasions')->where('city_id', '=', $cid)->count();
	if($getOccsaioncount >=1)
	{
 
    $getOccsaion = DB::table('nm_occasions')->where('city_id', '=', $cid)->groupBy('posted_by')->get();  


    foreach($getOccsaion as $val)
    {
        $gets = array();
        $getts = array();

        $getCatID = $val->id;
        $posted_by = $val->posted_by;
        $getCinfoc = DB::table('nm_customer')->where('cus_id', '=', $posted_by)->count();
        if($getCinfoc >=1)
        {
        $getCinfo = DB::table('nm_customer')->where('cus_id', '=', $posted_by)->first();
        $val->posted_by_name = $getCinfo->cus_name;  
        }
        else
        {
        $val->posted_by_name ='';  
        }

        $getPhotos = DB::table('nm_occasions_gallery')->where('occasion_id', $getCatID)->get(); 
        foreach($getPhotos as $valu)
        {      
        array_push($gets, $valu->image_url);
        array_push($getts, $valu->created_at);
        }
        $val->image_url = $gets;
        $val->created_at = $getts;
    	}
    return response()->json(['status' => "success",'code' => 200, 'data' => $getOccsaion]);
	 }
    else
    {
    return response()->json(['status' => "success",'code' => 401,  'message' => "No Occsaion found in current city"]);  
    }
    
    }
    else
    {  
    return response()->json(['status' => "success",'code' => 401,  'message' => "No city found in database"]);   
    }


    } catch (Exception $e) {
    return response()->json(['status' => false,'error_code' => 401, 'message' => "Something went wrong"]);
    }

}

public function sendContactenquiry(Request $request)
{

	$Ename = $request->Ename;
	$Email = $request->Email;
	$Message = $request->Message;
	$token = $request->_token;

	if($Email!='' && $Ename!='' && $token!='' && $Message!='')
	{
 
        Mail::send('emails.contact-enquiry', ['Ename' => $Ename,'Email' => $Email,'Message' => $Message], function ($m) use ($Ename,$Email) {
            $m->from($Email, 'Goldencages');
            $m->to('info@goldencages.com', 'Goldencages')->subject('New contact enquiry');
        });
  
		if(Session::get('lang_file')=='ar_lang'){
	return	Redirect::back()->with('message','شكرا لكم! تم ارسال رسالتك بنجاح.');
		}
		else
		{
	return	Redirect::back()->with('message','Thank you! Your message has been sent successfully.');
		}

	}
	else
	{
return Redirect::back()->with('message','Something went wrong');
	}

}

} // end  return Redi Thank you! Your message has been sent successfully. – Swiss Life ...  ', 'The Message']);



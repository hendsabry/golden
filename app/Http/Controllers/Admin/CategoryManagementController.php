<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Excel;
use Session;
use Lang;
use Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Models;
use App\AdminModel;
Use App\Category;
use App\AdminMenu;
use App\AdminUserGroup;

class CategoryManagementController extends Controller {

public $confirmation_user_email;
public $confirmation_user_name;

public function __construct(){
parent::__construct();
// set admin Panel language
$this->setLanguageLocaleAdmin();
}       

public function index() 
{
    if(Session::has('userid')) 
    {
        if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
        {
          $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
        }
        else 
        {
          $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
        }

        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail

        $categoryManagement =  Category::where('parent_id',0)->where('show_admin_status',1)->orderBy('created_at','desc')->paginate(10); // list of Category
        //echo '<pre>';print_r($categoryManagement);die;
        foreach ($categoryManagement as $key => $value) 
        {
          $value['vendor_count'] = Category::where('parent_id',$value->mc_id )->where('show_admin_status',1)->count();
        }
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();   
        return view('admin.category.category_management',compact('categoryManagement'))->with("routemenu",$session_message)->with('user',$user)->with('admin_menu',$admin_menu);
    }
    else 
    {
        return Redirect::to('siteadmin');
    } 
}

public function adddropdown() 
{
    $categoryManagement =  Category::where('parent_id','=', 0)->paginate(10);
    if (Session::has('userid')) 
    {
    if(Lang::has(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT')!= '') 
    {
    $session_message = trans(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT');
    }
    else 
    {
    $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_CATEGORIES_MANAGEMENT');
    }

    $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail

    $categoryManagement =  Category::where('parent_id','=', 0)->get(); //list of Category
    $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
    $group =  explode(",",$groups->group_permission);
    $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
    return view('admin.category.add_category_management',compact('categoryManagement'))->with("routemenu",$session_message)->with('user',$user)->with('admin_menu',$admin_menu);                
    }
    else 
    {
    return Redirect::to('siteadmin');
    } 
}

public function addCategory(Request $request)
    {
      if (Session::has('userid')) {
      $categoryManagement =  Category::where('parent_id','=', 0)->paginate(10);

      if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
      {
      $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
      }
      else 
      {
      $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
      }
      if ($request->isMethod('post'))
      { 
      $validator = Validator::make($request->all(), [
      'mc_name' => 'required',
      'mc_name_ar' => 'required',
      'mc_discription' => 'required',
      'mc_discription_ar' => 'required',
      ]);
      if ($validator->passes()) {
      $input = $request->all();

      if (isset($input['mc_img'])) //image upload functioning 
      {
      $uploaded_thumb_rules = array(
      'mc_img' => 'required|mimes:png,jpg,jpeg'
      );

      $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

      if ($thumbnail_validator->passes()) {

      $uploaded_partner_thumbnail = time() . '_' . $request->file('mc_img')->getClientOriginalName();      

      $imageUploadSuccess =  Image::make($input['mc_img'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);

      if ($imageUploadSuccess) {
      $input['mc_img'] ='/public/assets/adimage/' . $uploaded_partner_thumbnail;
      } else {
      return Redirect::to('/admin/category_management/add')->with('message', 'Thumbnail was not uploaded')->withInput();
      }
      } else {
      return Redirect::to('/admin/category_management/add')->withErrors($thumbnail_validator)->withInput();
      }
    }

    if (isset($input['mc_img_ar'])) //image upload functioning 
    {
    $uploaded_thumb_rules = array(
    'mc_img_ar' => 'required|mimes:png,jpg,jpeg'
    );

    $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

    if ($thumbnail_validator->passes()) {

    $uploaded_partner_thumbnail = time() . '_' . $request->file('mc_img_ar')->getClientOriginalName();      

    $imageUploadSuccess =  Image::make($input['mc_img_ar'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);

    if ($imageUploadSuccess) {
    $input['mc_img_ar'] ='/public/assets/adimage/'.$uploaded_partner_thumbnail;
    } else {
    return Redirect::to('/admin/category_management/add')->with('message', 'Thumbnail was not uploaded')->withInput();
    }
    } else {
    return Redirect::to('/admin/category_management/add')->withErrors($thumbnail_validator)->withInput();
    }
    }

    unset($input['_token']);                         
    $sub_ad = Category::create($input); 
    Session::put('msg', 'Successfully added a new Event.');
    return Redirect::to('/admin/category_management');
    }else{

    return Redirect::to('/admin/category_management/add')
    ->with('message','Something went wrong please try again')
    ->withErrors($validator)->withInput();

    }
    }
    $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
    $group =  explode(",",$groups->group_permission);
    $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
    return view('admin.category.add_category_management')->with("routemenu",$session_message)->with('categoryManagement',$categoryManagement)->with('admin_menu',$admin_menu);
    }else{
    return Redirect::to('siteadmin');
    }
}

public function deleteCms($id)
{
  $categoryManagement =  Category::where('mc_id','=',$id)->first();

  $flag = Category::where('mc_id','=',$id)->delete();//delete page
  if (isset($categoryManagement->mc_img) && file_exists('/var/www/html'.$categoryManagement->mc_img) && $categoryManagement->mc_img != '') 
  {
  unlink('/var/www/html'.$categoryManagement->mc_img);
  } 
  if (isset($categoryManagement->mc_img_ar) && file_exists('/var/www/html'.$categoryManagement->mc_img_ar) && $categoryManagement->mc_img_ar != '') 
  {
  unlink('/var/www/html'.$categoryManagement->mc_img_ar);
  } 
  return Redirect::to('/admin/category_management');
}

public function EditCategory(Request $request,$id){
    if (Session::has('userid')) {

    if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
    {
    $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
    }
    else 
    {
    $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
    }
    $static_content =  Category::where('mc_id','=',$id)->first();
    $static_content_arr =  Category::where('parent_id','=', 0)->get();
    if ($request->isMethod('post'))
    { 
    $validator = Validator::make($request->all(), [
    'mc_name' => 'required',
    'mc_name_ar' => 'required',
    // 'mc_discription' => 'required',
    // 'mc_discription_ar' => 'required',

    ]);
    if ($validator->passes()) {
    $input = $request->all();
    if (isset($input['mc_img'])) //image upload functioning 
    {
    $uploaded_thumb_rules = array(
    'mc_img' => 'required|mimes:png,jpg,jpeg'
    );

    $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

    if ($thumbnail_validator->passes()) {

    $uploaded_partner_thumbnail = time() . '_' . $request->file('mc_img')->getClientOriginalName();      

    $imageUploadSuccess =  Image::make($input['mc_img'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);

    if ($imageUploadSuccess) {
    $input['mc_img'] ='/public/assets/adimage/'.$uploaded_partner_thumbnail;
    if (isset($static_content->mc_img) && file_exists('/var/www/html'.$static_content->mc_img) && $static_content->mc_img != '') {
    unlink('/var/www/html'.$static_content->mc_img);
    } 
    } else {
    return Redirect::to('/admin/category_management/edit/'.$id)->with('message', 'Thumbnail was not uploaded')->withInput();
    }
    } else {
    return Redirect::to('/admin/category_management/add/'.$id)->withErrors($thumbnail_validator)->withInput();
    }
    }

    if (isset($input['mc_img_ar'])) //image upload functioning 
    {
    $uploaded_thumb_rules = array(
    'mc_img_ar' => 'required|mimes:png,jpg,jpeg'
    );

    $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

    if ($thumbnail_validator->passes()) {

    $uploaded_partner_thumbnail = time() . '_' . $request->file('mc_img_ar')->getClientOriginalName();      

    $imageUploadSuccess =  Image::make($input['mc_img_ar'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);

    if ($imageUploadSuccess) {
    $input['mc_img_ar'] ='/public/assets/adimage/' . $uploaded_partner_thumbnail;
    if (isset($static_content->mc_img_ar) && file_exists('/var/www/html'.$static_content->mc_img_ar) && $static_content->mc_img_ar != '') {
    unlink('/var/www/html'.$static_content->mc_img_ar);
    } 
    } else {
    return Redirect::to('/admin/category_management/edit/'.$id)->with('message', 'Thumbnail was not uploaded')->withInput();
    }
    } else {
    return Redirect::to('/admin/category_management/edit/'.$id)->withErrors($thumbnail_validator)->withInput();
    }
    }

    unset($input['_token']);                         
    $sub_ad = Category::where('mc_id','=',$id)->update($input); 
    Session::put('msg', 'Successfully added a Page.');
    return Redirect::to('/admin/category_management');
    }else{

    return Redirect::to('/admin/category_management/edit/'.$id)
    ->with('message','Something went wrong please try again')
    ->withErrors($validator)->withInput();
    }
    }
    $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
    $group =  explode(",",$groups->group_permission);
    $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
    return view('admin.category.edit_category_management')->with("routemenu",$session_message)->with('static_content',$static_content)->with('static_content_arr', $static_content_arr)->with('admin_menu',$admin_menu);
    }else{
    return Redirect::to('siteadmin');
    }
}

public function subCategoryList($mc_id) 
{
    if (Session::has('userid')) 
    {
    if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
    {
    $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
    }
    else 
    {
    $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
    }
    $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail

    $subCategory_main  =  Category::where('mc_id','=', $mc_id)->where('show_admin_status',1)->first();
    $subcategory  =  Category::where('parent_id','=', $mc_id)->where('show_admin_status',1)->orderBy('created_at','desc')->paginate(10);

    //dd($subcategory)->where('show_admin_status',1)

    foreach($subcategory as $key => $value) 
    {
       $value['vendor_count'] = Category::where('parent_id','=', $value->mc_id)->where('show_admin_status',1)->count();
    }
    $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
    $group =  explode(",",$groups->group_permission);
    $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
    return view('admin.category.sub_category_list',compact('subcategory'))->with("routemenu",$session_message)->with('user',$user)->with('subCategory_main',$subCategory_main)->with('admin_menu',$admin_menu);
    }
    else 
    {
    return Redirect::to('siteadmin');
    } 
}

  public function mainCategoryList($mc_id)
  {
    if (Session::has('userid')) 
    {
        if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
        }
        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail

        $subCategory_main  =  Category::where('mc_id','=', $mc_id)->first();
        $subcategory  =  Category::where('parent_id','=', $mc_id)->orderBy('created_at','desc')->paginate(10);
        foreach($subcategory as $key => $value) 
        {
         $value['vendor_count'] = Category::where('parent_id','=', $value->mc_id)->count();
        }
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group  =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        return view('admin.category.sub_main_category_list',compact('subcategory','mc_id'))->with("routemenu",$session_message)->with('user',$user)->with('subCategory_main',$subCategory_main)->with('admin_menu',$admin_menu);
    }
    else 
    {
        return Redirect::to('siteadmin');
    } 
  }

public function searchData(Request $request)
{
    $search = trim($request->get('search_keyword'));
    if(isset($search))
    {
    $this->search_keyword = $search;
    }
    else
    {
    $this->search_keyword = '';
    }             
    $categoryManagement =  Category::where('mc_name','LIKE','%'.$this->search_keyword .'%')->where('parent_id', '=',0)->orderBy('created_at','desc')->paginate(10);//search list with   
    return compact('categoryManagement'); 
}

public function activeVendorList($id,$id2)
{
    if (Session::has('userid')) 
    {
    if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
    {
    $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
    }
    else 
    {
    $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
    }
    $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail    
     $subCategory_main  =  Category::where('mc_id','=', $id2)->first(); 
         
     $subcategory  =  Category::select('mc_id')->where('parent_id','=', $id)->get()->toArray(); 
    // $category = array_map('current', $subcategory);    
    // $orders = Category::select('mc_id')->where('parent_id','=', $category)->get()->toArray();    
    //             $orders_type = array_map('current', $orders);               
     $vendorlist  =  Category::where('parent_id','=', $id2)->with('vendor')->orderBy('created_at','desc')->paginate(10);      
    $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
    $group =  explode(",",$groups->group_permission);
    $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
    return view('admin.category.active_vendor_list',compact('vendorlist'))->with("routemenu",$session_message)->with('user',$user)->with('admin_menu',$admin_menu)->with('id',$id)->with('id2',$id2)->with('vendorlist',$vendorlist)->with('subCategory_main',$subCategory_main)->with('subcategory',$subcategory);
    }
    else 
    {
    return Redirect::to('siteadmin');
    } 
}

public function changeStatus(Request $request)
{
    $mc_id = $request->get('mc_id');
    $status = $request->get('status');
    $dta= array();
    $dta['mc_status'] = $status;
    Category::where('mc_id','=',$mc_id)->update($dta); // change status of particulare category 
    return 1 ;
}

 public function searchCSVData(Request $request)
                  {
                  return (new \App\Http\Controllers\Admin\Csv\CategoryExport)->forYear($request)->download('category.xlsx');
                  }
		  
 public function viewVendor($id)
                  {
                  if (Session::has('userid')) 
                  {
                  if(Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_TRANSACTION')!= '') 
                  {
                  $session_message = trans(Session::get('admin_lang_file').'.BACK_PAYMENT_TRANSACTION');
                  }
                  else 
                  {
                  $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_TRANSACTION');
                  }
                  $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
                  $vendor_detail = Category::where('vendor_id','=',$id)->with('vendor')->orderBy('created_at','desc')->first();
                  $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                  $group =  explode(",",$groups->group_permission);
                  $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                  return view('admin.category.view_vendor')->with("routemenu",$session_message)->with('user',$user)->with('admin_menu',$admin_menu)->with('vendor_detail',$vendor_detail);
                  }else {
                  return Redirect::to('siteadmin');
                  } 
              }


  public function EditSubCategory(Request $request,$id){
    if (Session::has('userid')) {

    if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
    {
    $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
    }
    else 
    {
    $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
    }
    $not_budget = 0;
     $not_budget = DB::table("nm_category")->whereNotIn('parent_id', [17,121,28])->whereNotIn('mc_id', [17,121,28])->count();
    // dd($not_budget);
    $static_content =  Category::where('mc_id','=',$id)->first();
    $static_content_arr =  Category::where('parent_id',0)->get();
    $data = DB::table("nm_category")->where('within_budget','=',1)->where('mc_id','!=',$id)->sum('budget');
    if ($request->isMethod('post'))
    { 
    $validator = Validator::make($request->all(), [
    'mc_name' => 'required',
    'mc_name_ar' => 'required',
    // 'mc_discription' => 'required',
    // 'mc_discription_ar' => 'required',

    ]);
    if ($validator->passes()) {
    $input = $request->all();
    if (isset($input['mc_img'])) //image upload functioning 
    {
    $uploaded_thumb_rules = array(
    'mc_img' => 'required|mimes:png,jpg,jpeg'
    );

    $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

    if ($thumbnail_validator->passes()) {

    $uploaded_partner_thumbnail = time() . '_' . $request->file('mc_img')->getClientOriginalName();      

    $imageUploadSuccess =  Image::make($input['mc_img'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);

    if ($imageUploadSuccess) {
    $input['mc_img'] ='/public/assets/adimage/'.$uploaded_partner_thumbnail;
    if (isset($static_content->mc_img) && file_exists('/var/www/html'.$static_content->mc_img) && $static_content->mc_img != '') {
    unlink('/var/www/html'.$static_content->mc_img);
    } 
    } else {
    return Redirect::to('/admin/category_management/sub_category/edit/'.$id)->with('message', 'Thumbnail was not uploaded')->withInput();
    }
    } else {
    return Redirect::to('/admin/category_management/sub_category/edit/'.$id)->withErrors($thumbnail_validator)->withInput();
    }
    }

    if (isset($input['mc_img_ar'])) //image upload functioning 
    {
    $uploaded_thumb_rules = array(
    'mc_img_ar' => 'required|mimes:png,jpg,jpeg'
    );

    $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

    if ($thumbnail_validator->passes()) {

    $uploaded_partner_thumbnail = time() . '_' . $request->file('mc_img_ar')->getClientOriginalName();      

    $imageUploadSuccess =  Image::make($input['mc_img_ar'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);

    if ($imageUploadSuccess) {
    $input['mc_img_ar'] ='/public/assets/adimage/' . $uploaded_partner_thumbnail;
    if (isset($static_content->mc_img_ar) && file_exists('/var/www/html'.$static_content->mc_img_ar) && $static_content->mc_img_ar != '') {
    unlink('/var/www/html'.$static_content->mc_img_ar);
    } 
    } else {
    return Redirect::to('/admin/category_management/sub_category/edit/'.$id)->with('message', 'Thumbnail was not uploaded')->withInput();
    }
    } else {
    return Redirect::to('/admin/category_management/sub_category/edit/'.$id)->withErrors($thumbnail_validator)->withInput();
    }
    }

    unset($input['_token']); 
    if (isset($input['within_budget'])) {
       $input['within_budget'] = 1;
    }else{
        $input['within_budget'] = 0;
    }
    $sum = $data + $input['budget'];
    if($sum >= 100)
    {
        
    }
    else{
         $sub_ad = Category::where('mc_id','=',$id)->update($input); 
    }
    Session::put('msg', 'Successfully updated subcategory.');
    return Redirect::to('/admin/category_management/'.$static_content->parent_id);
    }else{

    return Redirect::to('/admin/category_management/sub_category/edit/'.$id)
    ->with('message','Something went wrong please try again')
    ->withErrors($validator)->withInput();
    }
    }
    $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
    $group =  explode(",",$groups->group_permission);
    $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
    return view('admin.category.edit_sub_category')->with("routemenu",$session_message)->with('static_content',$static_content)->with('static_content_arr', $static_content_arr)->with('admin_menu',$admin_menu)->with('data', $data);
    }else{
    return Redirect::to('siteadmin');
    }
}

public function EditMainCategory(Request $request,$id){
    if (Session::has('userid')) {
    if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
    {
    $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
    }
    else 
    {
    $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
    }
     $not_budget = 0;
     $not_budget = DB::table("nm_category")->whereNotIn('parent_id', [17,121,28])->whereNotIn('mc_id', [17,121,28])->count();
    $static_content =  Category::where('mc_id','=',$id)->first();
    $category_1 =  Category::select('mc_id')->where('parent_id','=', 0)->get()->toArray();
    $static_content_arr =  Category::whereIn('parent_id', $category_1)->orderBy('mc_name','ASC')->get();
     $data = DB::table("nm_category")->where('within_budget', '=' , 1)->sum('budget');
    // dd($static_content_arr);
    if ($request->isMethod('post'))
    { 
    $validator = Validator::make($request->all(), [
    'mc_name' => 'required',
    'mc_name_ar' => 'required',
    // 'mc_discription' => 'required',
    // 'mc_discription_ar' => 'required',

    ]);
    if ($validator->passes()) {
    $input = $request->all();
    if (isset($input['mc_img'])) //image upload functioning 
    {
    $uploaded_thumb_rules = array(
    'mc_img' => 'required|mimes:png,jpg,jpeg'
    );

    $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

    if ($thumbnail_validator->passes()) {

    $uploaded_partner_thumbnail = time() . '_' . $request->file('mc_img')->getClientOriginalName();      

    $imageUploadSuccess =  Image::make($input['mc_img'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);

    if ($imageUploadSuccess) {
    $input['mc_img'] ='/public/assets/adimage/'.$uploaded_partner_thumbnail;
    if (isset($static_content->mc_img) && file_exists('/var/www/html'.$static_content->mc_img) && $static_content->mc_img != '') {
    unlink('/var/www/html'.$static_content->mc_img);
    } 
    } else {
    return Redirect::to('/admin/category_management/sub_category/edit/'.$id)->with('message', 'Thumbnail was not uploaded')->withInput();
    }
    } else {
    return Redirect::to('/admin/category_management/sub_category/edit/'.$id)->withErrors($thumbnail_validator)->withInput();
    }
    }

    if (isset($input['mc_img_ar'])) //image upload functioning 
    {
    $uploaded_thumb_rules = array(
    'mc_img_ar' => 'required|mimes:png,jpg,jpeg'
    );

    $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

    if ($thumbnail_validator->passes()) {

    $uploaded_partner_thumbnail = time() . '_' . $request->file('mc_img_ar')->getClientOriginalName();      

    $imageUploadSuccess =  Image::make($input['mc_img_ar'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);

    if ($imageUploadSuccess) {
    $input['mc_img_ar'] ='/public/assets/adimage/' . $uploaded_partner_thumbnail;
    if (isset($static_content->mc_img_ar) && file_exists('/var/www/html'.$static_content->mc_img_ar) && $static_content->mc_img_ar != '') {
    unlink('/var/www/html'.$static_content->mc_img_ar);
    } 
    } else {
    return Redirect::to('/admin/category_management/sub_category/main_categroy/edit/'.$id)->with('message', 'Thumbnail was not uploaded')->withInput();
    }
    } else {
    return Redirect::to('/admin/category_management/sub_category/main_categroy/edit/'.$id)->withErrors($thumbnail_validator)->withInput();
    }
    }

    unset($input['_token']); 
    if (isset($input['within_budget'])) {
       $input['within_budget'] = 1;
    }else{
        $input['within_budget'] = 0;
    }
     $sum = $data + $input['budget'];
   //dd($sum);
    if($sum >= 100)
    {
        
    }
    else{
         $sub_ad = Category::where('mc_id','=',$id)->update($input); 
    }                        
   // $sub_ad = Category::where('mc_id','=',$id)->update($input); 
    Session::put('msg', 'Successfully updated subcategory.');
    return Redirect::to('/admin/category_management/main_category/'.$static_content->parent_id);
    }else{

    return Redirect::to('/admin/category_management/sub_category/main_categroy/edit/'.$id)
    ->with('message','Something went wrong please try again')
    ->withErrors($validator)->withInput();
    }
    }
    $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
    $group =  explode(",",$groups->group_permission);
    $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
    return view('admin.category.edit_sub_main_category')->with("routemenu",$session_message)->with('static_content',$static_content)->with('static_content_arr', $static_content_arr)->with('admin_menu',$admin_menu)->with('data', $data);
    }else{
    return Redirect::to('siteadmin');
    }
}


}
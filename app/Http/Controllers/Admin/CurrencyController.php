<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB;
use Excel;
use Session;
use Lang;
use Response;
use Auth;
use Exception;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Models;
use App\AdminModel;
use App\AdminMenu;
use App\AdminUserGroup;
use App\Country;
use App\Currency;
use App\User;


class CurrencyController extends Controller {
  public function __construct(){
    parent::__construct();
// set admin Panel language
    $this->setLanguageLocaleAdmin();
  }  

               public function currencylist() 
                  {

                    if (Session::has('userid')) 
                      {
                        if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                          {
                            $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');

                          }
                          else 
                          {
                            $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                          }

                          $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail

                        $countries = Country::where('co_status','=',1)->orderBy('co_name', 'asc')->get();  
                        $currency = Currency::with('country')->orderBy('cur_name', 'asc')->paginate(10);
                       // dd($currency); 

                      foreach ($currency as $key => $value) 
                        {
                        if($value->cur_default=='1')
                        {
                        $currency[$key]['cur_default'] = "Yes";

                        }
                        else
                        {
                        $currency[$key]['cur_default'] = "No";
                        }
                        } 
                           foreach ($currency as $key => $value) 
                        {
                            $value['last_update'] = date("M d, Y", strtotime($value['updated_at']));
                         }  
                          $groups =  AdminUserGroup::where('level','=',2)->get();
                          $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                          $group =  explode(",",$groups_1->group_permission);
                          $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get(); 
                          //dd($currency);
                          return view('admin.currency.currency', compact('currency'))->with("routemenu",$session_message)->with('user',$user)->with('groups',$groups)->with('admin_menu',$admin_menu)->with('countries',$countries);
                      }else 
                      {
                        return Redirect::to('siteadmin');
                      } 
                }                  

                public function changeStatus(Request $request)
                {
                    $id = $request->get('id');
                    //dd($id);
                    $status = $request->get('status');                    
                    $dta= array();
                    $dta['cur_status'] = $status;
                    Currency::where('cur_id','=',$id)->update($dta); // change status of sales rep 
                    return 1 ;
                }
         
                 public function addCurrency(Request $request) 
                {
                  if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                    {
                      $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
                    }
                    else 
                    {
                      $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                    }
                    $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();                    
                    $countries = Country::where('co_status','=',1)->orderBy('co_name', 'asc')->get();   
                    $groups =  AdminUserGroup::where('level','=',2)->get();
                    //get admin groups                   
                    
                    if ($request->isMethod('post'))
                    {
                      $validator = Validator::make($request->all(), [
                        'cur_name'    => 'required',
                        'cur_symbol' => 'required',
                        'cur_default'    => 'required',
                        'cur_status' => 'required',  

                         ]);
                      if ($validator->passes()) {
                        $input = $request->all();                                  
                      unset($input['_token']);                      
                      $offer = Currency::Create($input); 
                       Session::put('msg', 'Successfully added a new Sales Rep.');
                       return Redirect::to('/admin/currency');
                      }else{
                        return Redirect::to('/admin/currency/add')
                        ->with('message','Something went wrong please try again')
                        ->withErrors($validator)->withInput();
                      }
                      }
                   
                      $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                      $group =  explode(",",$groups_1->group_permission);
                      $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                      return view('admin.currency.add_currency')->with("routemenu",$session_message)->with('groups',$groups)->with('admin_menu',$admin_menu)->with('countries' ,$countries)->with('user' ,$user);              
              }

               public function editCurrency(Request $request,$id)
                {
                  if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                    {
                      $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
                    }
                    else 
                    {
                      $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                    }
                     $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first(); 
                     $countries = Country::where('co_status','=',1)->orderBy('co_name', 'asc')->get();
                    $groups =  AdminUserGroup::where('level','=',2)->get();
                    $users = Currency::where('cur_id','=',$id)->with('country')->first();                    
                    if ($request->isMethod('post'))
                    {
                          $validator = Validator::make($request->all(), [
                            'cur_name'    => 'required',
                            'cur_symbol' => 'required',
                            'cur_default'    => 'required',
                            'cur_status' => 'required',              
                          ]);
                          if ($validator->passes()) 
                          {
                              $input = $request->all();
                              unset($input['_token']);                              
                              $sub_ad = Currency::where('cur_id','=',$id)->update($input); 
                              //sub admin entry create 
                              Session::put('msg', 'Successfully added a new Event.');
                              return Redirect::to('/admin/currency');
                            }
                            else
                            {
                              return Redirect::to('/admin/currency/edit/'.$id)
                              ->with('message','Something went wrong please try again')
                              ->withErrors($validator)->withInput();
                            }
                      }
                  $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                  $group =  explode(",",$groups_1->group_permission);
                  $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                  return view('admin.currency.edit_currency')->with("routemenu",$session_message)->with('groups',$groups)->with('users',$users)->with('admin_menu',$admin_menu)->with('countries',$countries);
                }
             
              public function viewCurrncy($id)
                {
                  if (Session::has('userid')) 
                  {
                    if(Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') 
                      {
                        $session_message = trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT');
                      }
                      else 
                      {
                        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT');
                      }

                      $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first(); 
                      $sub_admin =  Currency::where('cur_id','=',$id)->first();
                      if($sub_admin->cur_status=='1')
                      {
                      $sub_admin['cur_status'] = 'Active';
                      } 
                      else 
                      {
                      $sub_admin['cur_status'] = 'In-Active' ;
                      }
                      if($sub_admin->cur_default=='1')
                      {
                      $sub_admin['cur_default'] = 'Yes';
                      } 
                      else 
                      {
                      $sub_admin['cur_default'] = 'No' ;
                      } 
                      $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                      $group =  explode(",",$groups->group_permission);
                      $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                      return view('admin.currency.view_currency')->with("routemenu",$session_message)->with('user',$user)->with('sub_admin',$sub_admin)->with('admin_menu',$admin_menu);;
                   }
                    else 
                    {
                      return Redirect::to('siteadmin');
                    } 
                }

                public function deleteCurrncy($id)
                {
                      $sub_admin = Currency::where('cur_id','=',$id)->first(); 
                    $flag = Currency::where('cur_id','=',$id)->delete();//delete lead
                    return Redirect::to('/admin/currency');
                }   

                 public function searchData(Request $request)
                {
                  $search = trim($request->get('search_keyword'));
                  if(isset($search))
                  {
                    $this->search_keyword = $search;
                  }
                  else
                  {
                    $this->search_keyword = '';
                  }   
                  $status  = trim($request->get('status'));
                  //dd($status);

                  if( $status != "" &&  $this->search_keyword != ''){
                  $users =  Offer::where('offer_name','LIKE','%'.$this->search_keyword .'%')->where('status','=',$status)->orderBy('created_at','desc')->paginate(10);//search list with keyword and status 
                  
                }else if($this->search_keyword != '' && $status == "")
                { 
                  $users =  Offer::where('offer_name','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->paginate(10);
                  // search list with keyword
                }else if ($this->search_keyword == '' && $status != "" ) {
                  $users =  Offer::where('status','=',$status)->orderBy('created_at','desc')->paginate(10);//search list with status 
                }else{
                  $users =  Offer::orderBy('created_at','desc')->paginate(10);
                }
                foreach ($users as $key => $value) 
                        {
                            $value['updated'] = date("M d, Y", strtotime($value['created_at']));
                         }  
                return compact('users'); 
              }  
                
}
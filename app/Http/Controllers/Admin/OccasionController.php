<?php
namespace App\Http\Controllers\Admin;

  use App\Http\Controllers\Controller;
  use DB;
  use Excel;
  use Session;
  use Lang;
  use Response;
  use Illuminate\Support\Facades\Input;
  use Illuminate\Http\Request;
  use Illuminate\Support\Facades\Redirect;
  use Illuminate\Support\Facades\Validator;
  use App\Http\Models;
  use App\Occasion;
  use App\OccasionImage;
  use App\AdminModel;
  use App\BusinessOccasionType;
  use App\AdminMenu;
  use App\AdminUserGroup;

  class OccasionController extends Controller {

      public function __construct(){
          parent::__construct();
          // set admin Panel language
          $this->setLanguageLocaleAdmin();
      }       

      public function index() 
      {
          if (Session::has('userid')) 
          {
              if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
              {
                 $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
              }
              else 
              {
                 $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
              }

              $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail

              $occasionlist =  Occasion::orderBy('created_at','desc')->with('getUser')->paginate(10); //list of sub admin
              foreach ($occasionlist as $key => $value) {
                   $value['occasion_date'] = date('M d, Y', strtotime($value->occasion_date));
              }
              $occasion_type =  BusinessOccasionType::get();
              //dd($groups);
              $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
              $group =  explode(",",$groups->group_permission);
              $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
              return view('admin.occasion.list',compact('occasionlist'))->with("routemenu",$session_message)->with('user',$user)->with('occasion_type',$occasion_type)->with('admin_menu',$admin_menu);
          }
          else 
          {
              return Redirect::to('siteadmin');
          } 
      }

      public function occasionFilter(Request $request)
      {
          $id = $request->get('occasion_type_id');
          if(isset($id))
          {
              $occasionlist =  Occasion::where('occasion_type',$id)->orderBy('created_at','desc')->with('getUser')->paginate(10);
              foreach ($occasionlist as $key => $value) {
                   $value['occasion_date'] = date('M d, Y', strtotime($value->occasion_date));
              }
              return compact('occasionlist');
          }else
          {

              $occasionlist =  Occasion::orderBy('created_at','desc')->with('getUser')->paginate(10);
              foreach ($occasionlist as $key => $value) {
                   $value['occasion_date'] = date('M d, Y', strtotime($value->occasion_date));
              }
             return compact('occasionlist');
          }
      }

      public function occasionSearch(Request $request)
      {
          $search_keyword = $request->get('search_keyword');

          if($search_keyword == "")
          { 
              $occasionlist =  Occasion::with('getUser')->paginate(10);  
          }
          else
          {

              $occasionlist = Occasion::where('occasion_name','LIKE','%'.$search_keyword .'%')
              ->orwhere('occasion_name_ar','LIKE','%'.$search_keyword .'%');
              $occasionlist = $occasionlist->orderBy('created_at','desc')->with('getUser')->paginate(10);  
          }
          foreach ($occasionlist as $key => $value) {
                   $value['occasion_date'] = date('M d, Y', strtotime($value->occasion_date));
          }
          return compact('occasionlist');
      }

      public function occasionPaginate(Request $request)
      {
          $input = $request->all();
          $search_keyword = $input['search_keyword'];
          $occasion_type_id = $input['occasion_type_id'];

          if($input['search_keyword'] == "" && $input['occasion_type_id'])
          { 
             $occasionlist =  Occasion::orderBy('created_at','desc')->with('getUser')->paginate(10);  
          }
          elseif($input['search_keyword'] != "" && $input['occasion_type_id'] == "")
          {

              $occasionlist = Occasion::where('occasion_name','LIKE','%'.$search_keyword .'%')
              ->orwhere('occasion_name_ar','LIKE','%'.$search_keyword .'%');
             $occasionlist = $occasionlist->orderBy('created_at','desc')->with('getUser')->paginate(10);
          }elseif($input['search_keyword'] == "" && $input['occasion_type_id'] != "")
          {

              $occasionlist =  Occasion::where('occasion_type',$occasion_type_id)->orderBy('created_at','desc')->with('getUser')->paginate(10);

          }
          else
          {

              $occasionlist = Occasion::where('occasion_type',$occasion_type_id)->where('occasion_name','LIKE','%'.$search_keyword .'%')
              ->orwhere('occasion_name_ar','LIKE','%'.$search_keyword .'%');
              $occasionlist = $occasionlist->orderBy('created_at','desc')->with('getUser')->paginate(10);

          }
          foreach ($occasionlist as $key => $value) {
                   $value['occasion_date'] = date('M d, Y', strtotime($value->occasion_date));
              }
          return compact('occasionlist');
      }

      public function csvDownload(Request $request)
      {
        return (new \App\Http\Controllers\Admin\Csv\OccasionExport)->forYear($request)->download('occasion.xlsx');
      }

      public function viewOccasions($id){
          if (Session::has('userid')) {

              if(Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') 
              {
                  $session_message = trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT');

              }
              else 
              {
                  $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT');
              }

                  $occasion =  Occasion::where('id','=',$id)->with('getUser')->first(); //

                  $occasionImage = OccasionImage::where('occasion_id','=',$id)->get();

                  $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
                  $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                  $group =  explode(",",$groups->group_permission);
                  $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                  return view('admin.occasion.view')->with("routemenu",$session_message)->with('user',$user)->with('occasion',$occasion)->with('occasionImage',$occasionImage)->with('admin_menu',$admin_menu);;
          }else {
                  return Redirect::to('siteadmin');
          } 
      }

      public function deleteOccasions($id){
          Occasion::where('id','=',$id)->delete();//delete user
          OccasionImage::where('occasion_id','=',$id)->delete();
          return Redirect::to('/admin/occasions');
      }

      public function changeStatus(Request $request)
    {
            $id = $request->get('id');
            $status = $request->get('status');
            $dta= array();
            $dta['status'] = $status;
            Occasion::where('id','=',$id)->update($dta); // change status of particulare subadmin 
            return 1 ;
    }


  }
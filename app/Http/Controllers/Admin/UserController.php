<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Excel;
use Session;
use Lang;
use Response;
use Auth;
use Config;
use File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Models;
use App\AdminModel;
use App\Country;
use App\City;
use App\User;
use App\AdminMenu;
use App\AdminUserGroup;

class UserController extends Controller {
public $confirmation_user_email;
public $confirmation_user_name;
public $collection;
    public function __construct(){
      parent::__construct();
      // set admin Panel language
      $this->setLanguageLocaleAdmin();
    }       

    public function UserManagement() {

        if (Session::has('userid')) {

        if(Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT');

        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT');
        }

        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail

        $users =  User::with('getCity')->with('getCountry')->orderBy('created_at','desc')->paginate(10); //list of user

        foreach ($users as $key => $value) {
            if($value->last_log_in!=''){
                    $value->last_log_in=date("d M Y h:i A",strtotime($value->last_log_in));
            }
            
        }



        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();

        return view('admin.management.user_management',compact('users','users'))->with("routemenu",$session_message)->with('user',$user)->with('admin_menu',$admin_menu);;
        }else {
        return Redirect::to('siteadmin');
        } 
    }

    public function changeStatus(Request $request)
    {
        $id = $request->get('id');
        $status = $request->get('status');
        $dta= array();
        $dta['cus_status'] = $status;
        User::where('cus_id','=',$id)->update($dta); // change status of particulare user 
        $sub_ad = User::where('cus_id','=',$id)->first();
        $email = $sub_ad->email;
        $baseurl = url('/') ;
        if (isset($sub_ad->cus_name)) {
        $username = $sub_ad->cus_name;
        }else{
        $username = '';
        }
        if (isset($sub_ad->gender)) {
        if($sub_ad->gender == '0'){
        $respect = 'Mr';
        }else if ($sub_ad->gender == 'female') {
        $respect = 'Ms';
        }else{
        $respect = '';
        }
        }else{
        $respect = '';
        }
        $this->confirmation_user_email = $email;
        $this->confirmation_user_name = "Golden Cage";
        $status = '';
        if ($sub_ad->cus_status == 1) {
          $subject = 'Your Account is Successfully Activated ! Golden Cage'; 
                  $status = 'Activated';
        }else{
            $subject = 'Your Account is InActivated ! Golden Cage'; 
            $status = 'InActivated';
        }
                                  

        Mail::send('admin.mail.active_status',
        array(
        'user_name' => $username,
        'user_email' => $email,
        'site_url' => $baseurl,
        'status' => $status,
        'respect' => $respect,
        ), function($message) use($subject)
        {
        $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
        });
        return 1 ;
    }


    public function searchData(Request $request)
    {
        $search = trim($request->get('search_keyword'));
        if(isset($search))
        {
        $this->search_keyword = $search;
        }
        else
        {
        $this->search_keyword = '';
        }        

        $status  = trim($request->get('status'));

        if( $status != "" &&  $this->search_keyword != ''){
        $users =  User::where('cus_name','LIKE','%'.$this->search_keyword .'%')->orWhere('email','LIKE','%'.$this->search_keyword .'%')->where('cus_status','=',$status)->orderBy('created_at','desc')->with('getCity')->with('getCountry')->paginate(10);//search list with keyword and status 
        }else if($this->search_keyword != '' && $status == "")
        { 
        $users =  User::where('cus_name','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->with('getCity')->with('getCountry')->paginate(10);
        // search list with keyword
        }else if ($this->search_keyword == '' && $status != "" ) {
        $users =  User::where('cus_status','=',$status)->orderBy('created_at','desc')->with('getCity')->with('getCountry')->paginate(10);//search list with status 
        }else{
        $users =  User::with('getCity')->with('getCountry')->orderBy('created_at','desc')->paginate(10);
        }

         foreach ($users as $key => $value) {
            if($value->last_log_in!=''){
                    $value->last_log_in=date("d M Y h:i A",strtotime($value->last_log_in));
            }
            
        }

        return compact('users'); 
    }

    // public function searchCSVData(Request $request)
    // {
    //   return (new \App\Http\Controllers\Admin\Csv\InvoicesExport)->forYear($request)->download('users.xlsx');
    // }

    public function searchCSVData(Request $request)
    {
         return (new \App\Http\Controllers\Admin\Csv\InvoicesExport)->foryear($request)->download('users.xlsx');
    }

    public function deleteUser($id){
        $customer =  User::where('cus_id','=',$id)->first();
        $flag = User::where('cus_id','=',$id)->delete();//delete user
        if (isset($customer->cus_pic) && $customer->cus_pic != "" && file_exists('/var/www/html'.$customer->cus_pic)) {
        unlink('/var/www/html'.$customer->cus_pic);
        } 
        return Redirect::to('/admin/user_management');
    }

    public function viewUser($id) {

        if (Session::has('userid')) {

        if(Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT');

        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT');
        }

        $users =  User::where('cus_id','=',$id)->with('getCity')->with('getCountry')->first();//get user detail
        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();

        return view('admin.management.user_view')->with("routemenu",$session_message)->with('user',$user)->with('users',$users)->with('admin_menu',$admin_menu);;
        }else {
        return Redirect::to('siteadmin');
        } 
    }

    public function addUser(Request $request){
        if (Session::has('userid')) {
        if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
        }

        $selected_city  = City::select('ci_con_id')->where('ci_status','=',1)->distinct()->orderBy('ci_name', 'asc')->get()->toArray();
        $selected_country = array();
        foreach ($selected_city as $key => $value) {
        $selected_country[] = $value['ci_con_id'];
        }
        $countries = Country::whereIn('co_id',$selected_country)->where('co_status','=',1)->groupBy('co_name')->orderBy('co_name', 'asc')->get();
        $select_city = array();
        if (isset($countries[0])) {
        $select_city =  City::where('ci_con_id','=',$countries[0]->co_id)->where('ci_status','=',1)->where('ci_status','=',1)->orderBy('ci_name', 'asc')->get();
        }
        if ($request->isMethod('post'))
        {  
        $validator = Validator::make($request->all(), [
        
        'email' => 'required|unique:nm_customer,email'
        ]);
        if ($validator->passes()) {
        $input = $request->all();
        $checkUser =  User::where('email','=',$input['email'])->first();
        if(!$checkUser)
        {
        if (isset($input['national_id'])) //image upload functioning 
        {
        $uploaded_thumb_rules = array(
        'national_id' => 'required|mimes:png,jpg,jpeg'
        );

        $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

        if ($thumbnail_validator->passes()) {

        $uploaded_partner_thumbnail = time() . '_' . $request->file('national_id')->getClientOriginalName();      

        $imageUploadSuccess =  Image::make($input['national_id'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);

        if ($imageUploadSuccess) {
        $input['national_id'] ='/public/assets/adimage/' . $uploaded_partner_thumbnail;
        } else {
        return Redirect::to('/admin/user_management/add')->with('message', 'Thumbnail was not uploaded')->withInput();
        }
        } else {
        return Redirect::to('/admin/user_management/add')->withErrors($thumbnail_validator)->withInput();
        }
        }

        unset($input['_token']);
        $password = substr(md5(mt_rand()), 0, 8);
        $input['password']  = Hash::make($password);
        $input['dob'] = date("Y-m-d", strtotime($input['dob']));
        $input['cus_logintype'] = 2;
        $sub_user = User::create($input); 
        $this->sendpassword($sub_user, $password);
        //user entry create 
        Session::put('msg', 'Successfully added a new user.');
        return Redirect::to('/admin/user_management');
        }else{
        //Session::put('warning', 'This email is already registered.');
        return Redirect::to('/admin/user_management/add')->with('warning', 'This email is already registered.')->withInput();
        }
        }else{

        return Redirect::to('/admin/user_management/add')->with('message','Something went wrong please try again')->withErrors($validator)->withInput();
        }
        }
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();

        return view('admin.management.add_user')->with("routemenu",$session_message)->with('countries',$countries)->with('select_city',$select_city)->with('admin_menu',$admin_menu);;
        }else{
        return Redirect::to('siteadmin');
        }
    }

    public function sendpassword($sub_ad,$password){
        $email = $sub_ad->email;
        $baseurl = url('/') ;
        if (isset($sub_ad->cus_name)) {
        $username = $sub_ad->cus_name;
        }else{
        $username = '';
        }
        if (isset($sub_ad->gender)) {
        if($sub_ad->gender == '0'){
        $respect = 'Mr';
        }else if ($sub_ad->gender == 'female') {
        $respect = 'Ms';
        }else{
        $respect = '';
        }
        }else{
        $respect = '';
        }
        $this->confirmation_user_email = $email;
        $this->confirmation_user_name = "Golden Cage";

        $subject = 'Your Account is Successfully Created ! Golden Cage';                           

        Mail::send('admin.mail.send_password_user',
        array(
        'user_name' => $username,
        'user_email' => $email,
        'site_url' => $baseurl,
        'password' => $password,
        'respect' => $respect,
        ), function($message) use($subject)
        {
        $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
        });
    }

    public function editUser(Request $request,$id){

        if (Session::has('userid')) {
        if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
        }

        $selected_city  = City::select('ci_con_id')->where('ci_status','=',1)->orderBy('ci_name', 'asc')->distinct()->get()->toArray();
        $selected_country = array();
        foreach ($selected_city as $key => $value) {
        $selected_country[] = $value['ci_con_id'];
        }
        $countries = Country::whereIn('co_id',$selected_country)->where('co_status','=',1)->orderBy('co_name', 'asc')->groupBy('co_name')->get();
        $customer =  User::where('cus_id','=',$id)->first();
        $cities  = City::where('ci_con_id','=',$customer->cus_country)->where('ci_status','=',1)->orderBy('ci_name', 'asc')->get();
        if ($request->isMethod('post'))
        {
        //dd($request->all());
        $validator = Validator::make($request->all(), [
        'cus_name' => 'required',
        'email' => 'required',
        'cus_phone' => 'required',
        'dob'       => 'required',
        ]);        

        if($validator->passes()) {
        $input = $request->all();
        $customer =  User::where('cus_id','=',$id)->first();               
        if (isset($input['national_id'])) //image upload functioning 
        {
          $uploaded_thumb_rules = array('national_id' => 'required|mimes:png,jpg,jpeg'
        );

        $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);
        if($thumbnail_validator->passes()) 
        {
           $uploaded_partner_thumbnail = time() . '_' . $request->file('national_id')->getClientOriginalName();      
           $imageUploadSuccess =  Image::make($input['national_id'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);
           if($request->file('address_image'))
           {
               $address_image_thumbnail = time() . '_' . $request->file('address_image')->getClientOriginalName(); 
               $imageUploadSuccessadd = Image::make($input['address_image'])->save('./public/assets/adimage/'.$address_image_thumbnail);
               if($imageUploadSuccessadd)
               {
                  $input['address_image'] ='/public/assets/adimage/' . $address_image_thumbnail;
                  if(isset($customer->address_image) && $customer->address_image != "" && file_exists('/var/www/html'.$customer->address_image)) 
                  {
                     unlink('/var/www/html'.$customer->address_image);
                  } 
               }
           }
        if($imageUploadSuccess) 
        {
            $input['national_id'] ='/public/assets/adimage/' . $uploaded_partner_thumbnail;
            if(isset($customer->national_id) && $customer->national_id != "" && file_exists('/var/www/html'.$customer->national_id)) 
            {
              unlink('/var/www/html'.$customer->national_id);
            } 
        } 
        else {
        return Redirect::to('/admin/user_management/edit/'.$id)->with('message', 'Thumbnail was not uploaded')->withInput();
        }
        } else {
        return Redirect::to('/admin/user_management/edit/'.$id)->withErrors($thumbnail_validator)->withInput();
        }
        }

        unset($input['_token']);

        $input['dob'] = date("Y-m-d", strtotime($input['dob']));
       

   if($input['upassword']=='' && $input['cpassword']==''){
             unset($input['upassword']);
             unset($input['cpassword']);
        }else{
           $input['password'] = bcrypt($request->input('upassword')); 
           unset($input['upassword']); 
            unset($input['cpassword']);
        } 
            unset($input['submit']);
        $sub_user = User::where('cus_id','=',$id)->update($input); 
        //                      $this->sendpassword($sub_user, $password);
        //user entry create 
        Session::put('msg', 'Successfully added a new user.');
        return Redirect::to('/admin/user_management');
        }else{
        return Redirect::to('/admin/user_management/edit/'.$id)
        ->with('message','Something went wrong please try again')
        ->withErrors($validator)->withInput();

        }
        }
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        return view('admin.management.edit_user')->with("routemenu",$session_message)->with('countries',$countries)->with('customer',$customer)->with('cities',$cities)->with('admin_menu',$admin_menu);;
        }else{
        return Redirect::to('siteadmin');
        }
    }

    public function flushSession(Request $request){
      $request->session()->forget('warning');
      return 1;
    }
}
<?php 
namespace App\Http\Controllers\Admin\Csv;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Session;
use App\Occasion;

class OccasionExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
	use Exportable;

    public function forYear(Request $request)
    {

    	
         $this->input = $request->all();;

         return $this;
    }
    public function headings(): array
    {
        if(Session::get('admin_lang_file') == 'admin_ar_lang'){
            return [
            'هوية شخصية',
            'اسم المناسبة',
            'تاريخ المناسبة',
            'مكان المناسبة',
            'منشور من طرف',
        ];
        }else{
            return [
            'Id',
            'Occasion Name',
            'Occasion Date',
            'Occasion Venue',
            'PostedBy',
        ];
        }
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:N1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            },
        ];
    }
    
    public function collection()
    {
            $input = $this->input;

            $search = trim($input['search_keyword']);
            if(isset($search))
            {
                $this->search_keyword = $search;
            }
            else
            {
                $this->search_keyword = '';
            }

            if (isset($input['occasion_type_id'])) {
                $occasion_type_id  = trim($input['occasion_type_id']);
            }else{
                $occasion_type_id  ="";
            }

            if($this->search_keyword == "" && $occasion_type_id =="")
            { 
                    $userdata = Occasion::query()->leftjoin('nm_customer','nm_occasions.posted_by','=','nm_customer.cus_id')
                    ->orderBy('nm_occasions.created_at','desc');

                $abc =  $userdata->select('nm_occasions.id','nm_occasions.occasion_name','nm_occasions.occasion_date','nm_occasions.occasion_venue','nm_occasions.occasion_name_ar','nm_customer.cus_name as postedBy')->get();
            }
            elseif($this->search_keyword != "" && $occasion_type_id == "")
            {

                    $userdata = Occasion::query()->leftjoin('nm_customer','nm_occasions.posted_by','=','nm_customer.cus_id')->where('nm_occasions.occasion_name','LIKE','%'.$this->search_keyword.'%')
                    ->orwhere('nm_occasions.occasion_name_ar','LIKE','%'.$this->search_keyword.'%');

                    $abc =  $userdata->select('nm_occasions.id','nm_occasions.occasion_name','nm_occasions.occasion_date','nm_occasions.occasion_venue','nm_occasions.occasion_name_ar','nm_customer.cus_name as postedBy')->get();
            }elseif($this->search_keyword == "" && $occasion_type_id != "")
            {
                    $userdata = Occasion::query()->leftjoin('nm_customer','nm_occasions.posted_by','=','nm_customer.cus_id')
                    ->where('nm_occasions.occasion_type',$occasion_type_id)->orderBy('nm_occasions.created_at','desc');

                   $abc =  $userdata->select('nm_occasions.id','nm_occasions.occasion_name','nm_occasions.occasion_date','nm_occasions.occasion_venue','nm_occasions.occasion_name_ar','nm_customer.cus_name as postedBy')->get();
            }
            else
            {
                    $userdata = Occasion::query()->leftjoin('nm_customer','nm_occasions.posted_by','=','nm_customer.cus_id')->where('nm_occasions.occasion_type',$occasion_type_id)->where('nm_occasions.occasion_name','LIKE','%'.$this->search_keyword .'%')
                    ->orwhere('nm_occasions.occasion_name_ar','LIKE','%'. $this->search_keyword .'%')
                    ->orderBy('nm_occasions.created_at','desc');

                    $abc =  $userdata->select('nm_occasions.id','nm_occasions.occasion_name','nm_occasions.occasion_date','nm_occasions.occasion_venue','nm_occasions.occasion_name_ar','nm_customer.cus_name as postedBy')->get();
            }
            $output = array();
            foreach ($abc as $key => $value) {
            $internal_output = array();
            $internal_output[] = $value->id;
            if(Session::get('admin_lang_file') == 'admin_ar_lang'){
                $internal_output[] = $value->occasion_name_ar;
            }else{
                $internal_output[] = $value->occasion_name; 
            }
            $internal_output[] = date("d,M Y ,g:i A", strtotime($value->occasion_date));
            $internal_output[] = $value->occasion_venue;
            $internal_output[] = $value->postedBy;
            $output[]=  $internal_output;
            }
            $collect_abc = collect($output);
            return $collect_abc;
    }
}
<?php 
namespace App\Http\Controllers\Admin\Csv;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Session;
use Illuminate\Http\Request;
use App\subscription;

class SubscriptionExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
	use Exportable;

    public function forYear(Request $request)
    {
         $this->input = $request->all();;

         return $this;
    }
    public function headings(): array
    {
        if(Session::get('admin_lang_file') == 'admin_ar_lang'){
             return [
            'اسم الخدمة',
            'نوع الخدمة',
            'مبلغ / النسبة المئوية',
            'الحالة',
            'آخر تعديل على'
        ];
        }else{
             return [
            'Service Name',
            'Service Type',
            'Amount/Percentage',
            'Status',
            'Last Modified on'
        ];
        }
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:N1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            },
        ];
    }
     public function collection()
    {  
            $input = $this->input;
            if(isset($input['services_id']))
            {
            $services_id = $input['services_id'];
            }else{
            $services_id = '';
            }
            if (isset($input['status'])) {
                $status  = trim($input['status']);
            }else{
                $status  ='';
            }
            if($status != '' &&  $services_id != ''){

             $userdata =  subscription::query()->leftjoin('nm_category','nm_subscription_plan.cat_id','=','nm_category.mc_id')->where('nm_subscription_plan.services_id','=',$services_id)->where('nm_subscription_plan.status','=',$status)->orderBy('nm_subscription_plan.created_at','desc');
                $abc =  $userdata->select('nm_category.mc_name','nm_category.mc_name_ar','nm_subscription_plan.services_id','nm_subscription_plan.payment','nm_subscription_plan.status','nm_subscription_plan.created_at')->get();
                
            }else if($status != '' &&  $services_id == '')
            {
                $userdata =  subscription::query()->leftjoin('nm_category','nm_subscription_plan.cat_id','=','nm_category.mc_id')->where('status','=',$status)->orderBy('created_at','desc');
               $abc =  $userdata->select('nm_category.mc_name','nm_category.mc_name_ar','nm_subscription_plan.services_id','nm_subscription_plan.payment','nm_subscription_plan.status','nm_subscription_plan.created_at')->get();
            }else if ($status == '' &&  $services_id != '' ) {
                $userdata =  subscription::query()->leftjoin('nm_category','nm_subscription_plan.cat_id','=','nm_category.mc_id')->where('nm_subscription_plan.services_id','=',$services_id)->orderBy('nm_subscription_plan.created_at','desc');
                $abc = $userdata->select('nm_category.mc_name','nm_category.mc_name_ar','nm_subscription_plan.services_id','nm_subscription_plan.payment','nm_subscription_plan.status','nm_subscription_plan.created_at')->get();
            }else{
                 $userdata =  subscription::query()->leftjoin('nm_category','nm_subscription_plan.cat_id','=','nm_category.mc_id')->orderBy('nm_subscription_plan.created_at','desc');
                $abc =  $userdata->select('nm_category.mc_name','nm_category.mc_name_ar','nm_subscription_plan.services_id','nm_subscription_plan.payment','nm_subscription_plan.status','nm_subscription_plan.created_at')->get();
            }

            $output = array();
            foreach ($abc as $key => $value) {
            $internal_output = array();
            if(Session::get('admin_lang_file') == 'admin_ar_lang'){
            $internal_output[] = $value->mc_name_ar;
            if ($value->services_id == 1) {
              $internal_output[] = 'عمولة';
            }else if($value->services_id == 2){
                 $internal_output[] = 'الرسوم مستندة';
            }else{
                $internal_output[] = '';
            }
            }else{
                if ($value->services_id == 1) {
              $internal_output[] = 'Commission';
            }else if($value->services_id == 2){
                 $internal_output[] = 'Fee Based';
            }else{
                $internal_output[] = '';
            }
                 $internal_output[] = $value->mc_name;
            }
            
            $internal_output[] = $value->payment;
            if(Session::get('admin_lang_file') == 'admin_ar_lang'){
            if ($value->status == 1) {
            $internal_output[] = 'نشيط';
            }else{
            $internal_output[] = 'غير نشط';
            }
            }else{
            if ($value->status == 1) {
            $internal_output[] = 'Active';
            }else{
            $internal_output[] = 'Inactive';
            }
            }
            $internal_output[] = date("d,M Y", strtotime($value->created_at));
            $output[]=  $internal_output;
            }
            $collect_abc = collect($output);
            return $collect_abc;
            
    }
}
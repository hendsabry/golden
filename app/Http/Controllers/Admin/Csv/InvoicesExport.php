<?php 
namespace App\Http\Controllers\Admin\Csv;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Http\Request;
use Session;
use App\User;
use Illuminate\Support\Collection;

class InvoicesExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
	use Exportable;

    public function forYear(Request $request)
    {
         $this->input = $request->all();
         return $this;
    }
    public function headings(): array
    {
        if(Session::get('admin_lang_file') == 'admin_ar_lang'){
            return [
            'هوية شخصية',
            'اسم',
            'البريد الإلكتروني',
            'البريد الإلكتروني',
            'آخر تسجيل',
            'مبلغ المحفظة',
            'عنوان',
            'الهوية الوطنية',
            'تاريخ الولادة',
            'الحالة',
        ];
        }else{
            return [
            'Id',
            'Name',
            'Email',
            'Phone No',
            'Last Logged',
            'Wallet Amount',
            'Address',
            'National Id',
            'Date of Birth',
            'Status',
        ];
        }
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:N1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            },
        ];
    }
    

     public function collection()
    {
       $input = $this->input;
       if(isset($input['search_keyword']))
            {
            $this->search_keyword = trim($input['search_keyword']);
            }
            else
            {
            $this->search_keyword = '';
            }        
            if (isset($input['status'])) {
                $status  = trim($input['status']);
            }else{
                $status  ="";
            }

            if( $status != "" &&  $this->search_keyword != ''){
             $abc =  User::query()->select('cus_id','cus_name','email','cus_phone','cus_address1','dob','cus_status','last_log_in','wallet','national_id')->where('cus_name','LIKE','%'.$this->search_keyword .'%')->where('cus_status','=',$status)->get();//search list with keyword and status 
            }else if($this->search_keyword != '' && $status == "")
            { 
            $abc =  User::query()->select('cus_id','cus_name','email','cus_phone','cus_address1','dob','cus_status','last_log_in','wallet','national_id')->where('cus_name','LIKE','%'.$this->search_keyword .'%')->get();
            // search list with keyword
            }else if ($this->search_keyword == '' && $status != "" ) {
             $abc =  User::query()->select('cus_id','cus_name','email','cus_phone','cus_address1','dob','cus_status','last_log_in','wallet','national_id')->where('cus_status','=',$status)->get();//search list with status 
            }else{
            $abc = User::query()->select('cus_id','cus_name','email','cus_phone','cus_address1','dob','cus_status','last_log_in','wallet','national_id')->get();
            }
            $output = array();
            foreach ($abc as $key => $value) {
            $internal_output = array();
            $internal_output[] = $value->cus_id;
            $internal_output[] = $value->cus_name;
            $internal_output[] = $value->email;
            $internal_output[] = $value->cus_phone;
            if ($value->last_log_in != '') {
                 $internal_output[] = date("d,M Y", strtotime($value->last_log_in));
            }else{
                 $internal_output[] = 'N/A';
            }
             $internal_output[] = $value->wallet;
            $internal_output[] = $value->cus_address1;
            $internal_output[] = $value->national_id;
            $internal_output[] = date("d,M Y", strtotime($value->dob));
            if(Session::get('admin_lang_file') == 'admin_ar_lang'){
            if ($value->cus_status == 1) {
            $internal_output[] = 'نشيط';
            }else{
            $internal_output[] = 'غير نشط';
            }
            }else{
            if ($value->cus_status == 1) {
            $internal_output[] = 'Active';
            }else{
            $internal_output[] = 'Inactive';
            }
            }
            $output[]=  $internal_output;
            }
            $collect_abc = collect($output);
            return $collect_abc;
    }
}
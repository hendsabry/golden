<?php 
namespace App\Http\Controllers\Admin\Csv;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Session;
use Illuminate\Http\Request;
use App\Order;
use App\OrderSingleProduct;
use App\Merchant;

class CreditExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
	use Exportable;

    public function forYear(Request $request)
    {
         $this->input = $request->all();;

         return $this;
    }
    public function headings(): array
    {
        if(Session::get('admin_lang_file') == 'admin_ar_lang'){
                 return [
            'معرف المعاملة',
            'اسم المدفوع',
            'دفع ضد',
            'كمية',
            'تاريخ',
            'طريقة الدفع',
            'اسم المورد المرتبط',
        ];
        }else{
                 return [
            'Transaction Id',
            'Payer Name',
            'Payment Against',
            'Amount',
            'Date',
            'Payment Mode',
            'Associated Vendor Name',
        ];
        }
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:N1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            },
        ];
    }
     public function collection()
    {       $input = $this->input;
            if(isset($input['created_date']) && $input['created_date']!='')
            {
            $created_date = date("Y-m-d", strtotime($input['created_date']));
            }else{
            $created_date = '';
            } 
            if (isset($input['order_type'])) {
               $order_type = $input['order_type'];
            }else{
                $order_type = '';
            }
            if($order_type != '' &&  $created_date != ''){
            $userdata = Order::query()->leftjoin('nm_customer','nm_order.order_cus_id','=','nm_customer.cus_id')->leftjoin('nm_merchant','nm_order.order_cus_id','=','nm_merchant.mer_id')->where('nm_order.order_type','=',$order_type)->where('nm_order.created_date','like',$created_date.'%')->orderBy('nm_order.order_id','desc');

               $abc =   $userdata->select('nm_order.transaction_id','nm_customer.cus_name','nm_order.order_id','nm_order.order_amt','nm_order.created_date','nm_order.order_date','nm_order.order_paytype','nm_merchant.mer_fname','nm_merchant.mer_lname')->get();
                
            }else if($order_type != '' &&  $created_date == '')
            {
                $userdata = Order::query()->leftjoin('nm_customer','nm_order.order_cus_id','=','nm_customer.cus_id')->leftjoin('nm_merchant','nm_order.order_cus_id','=','nm_merchant.mer_id')->where('nm_order.order_type','=',$order_type)->orderBy('nm_order.order_id','desc');

                $abc =   $userdata->select('nm_order.transaction_id','nm_customer.cus_name','nm_order.order_id','nm_order.order_amt','nm_order.created_date','nm_order.order_date','nm_order.order_paytype','nm_merchant.mer_fname','nm_merchant.mer_lname')->get();
            }else if ($order_type == '' &&  $created_date != '' ) {
                 
             $userdata = Order::query()->leftjoin('nm_customer','nm_order.order_cus_id','=','nm_customer.cus_id')->leftjoin('nm_merchant','nm_order.order_cus_id','=','nm_merchant.mer_id')->where('nm_order.created_date','like',$created_date.'%')->orderBy('nm_order.order_id','desc');

                $abc =   $userdata->select('nm_order.transaction_id','nm_customer.cus_name','nm_order.order_id','nm_order.order_amt','nm_order.created_date','nm_order.order_date','nm_order.order_paytype','nm_merchant.mer_fname','nm_merchant.mer_lname')->get();
            }else{
                
            $userdata = Order::query()->leftjoin('nm_customer','nm_order.order_cus_id','=','nm_customer.cus_id')->leftjoin('nm_merchant','nm_order.order_cus_id','=','nm_merchant.mer_id')->orderBy('nm_order.order_id','desc');

               $abc =   $userdata->select('nm_order.transaction_id','nm_customer.cus_name','nm_order.order_id','nm_order.order_amt','nm_order.created_date','nm_order.order_date','nm_order.order_paytype','nm_merchant.mer_fname','nm_merchant.mer_lname')->get();
                

            }
               $output = array();
            foreach ($abc as $key => $value) {
            $total_merchant = OrderSingleProduct::select('merchant_id')->where('order_id', '=', $value->order_id)->groupBy('merchant_id')->get();

            foreach ($total_merchant as $key => $merchant_value) {

              
                $associated_vendor = Merchant::select('mer_id','mer_fname','mer_lname','mer_email')->where('mer_id','=',$merchant_value->merchant_id)->first();

            }  
            if(!empty($associated_vendor)){           
                $value->merchant_name = $associated_vendor->mer_fname.' '.$associated_vendor->mer_lname;
            }else{
                $value->merchant_name = 'NA';
            }    
            $internal_output = array();
            $internal_output[] = $value->transaction_id;
            $internal_output[] = $value->cus_name;
            $internal_output[] = $value->order_id;
            $internal_output[] = $value->order_amt;
            $internal_output[] = date("d,M Y", strtotime($value->order_date));
            $internal_output[] = $value->order_paytype;
            $internal_output[] = $value->merchant_name;
            $output[]=  $internal_output;
            }
            $collect_abc = collect($output);
            return $collect_abc;
           
    }
}
<?php 
namespace App\Http\Controllers\Admin\Csv;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Session;
use Illuminate\Http\Request;
use App\AdminModel;

class SubAdminExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
	use Exportable;

    public function forYear(Request $request)
    {

    	
         $this->input = $request->all();;

         return $this;
    }
    public function headings(): array
    {
        if(Session::get('admin_lang_file') == 'admin_ar_lang'){
              return [
            'هوية شخصية',
            'اسم',
            'البريد الإلكتروني',
            'رقم الهاتف',
            'عنوان',
            'تاريخ الولادة',
            'وظيفة',
           'آخر تسجيل',
            'الحالة',
        ]; 
        }else{
              return [
            'Id',
            'Name',
            'Email',
            'Phone No',
            'Address',
            'Date of Birth',
            'Role',
           'Last Logged',
            'Status',
        ]; 
        }

    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:N1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            },
        ];
    }
     public function collection()
    {
    	$input = $this->input;

            $search = trim($input['search_keyword']);
                if(isset($search))
                {
                    $this->search_keyword = $search;
                }
                else
                {
                    $this->search_keyword = '';
                }
                    
                if (isset($input['access_group_id'])) {
                $access_group_id  = trim($input['access_group_id']);
                }else{
                $access_group_id  ="";
                }

                if (isset($input['status'])) {
                $status  = trim($input['status']);
                }else{
                $status  ="";
                }

                if( $status != "" &&  $access_group_id != '' && $this->search_keyword != ''){
                    $userdata = AdminModel::query()->leftjoin('admin_user_group','nm_admin.access_group_id','=','admin_user_group.id')
                    ->where('nm_admin.status','=',$status)->where('nm_admin.admin_type','=',2)->where('nm_admin.access_group_id','!=',6)->where('nm_admin.access_group_id','=',$access_group_id)->where('nm_admin.adm_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('nm_admin.created_at','desc');

                 $abc =  $userdata->select('nm_admin.adm_id','nm_admin.adm_fname','nm_admin.adm_lname','nm_admin.adm_email','nm_admin.adm_phone','nm_admin.adm_address1','nm_admin.dob','admin_user_group.group_name as groupname','nm_admin.status','nm_admin.last_log_in')->get();

                    
                }else if ($status != "" &&  $access_group_id != '' && $this->search_keyword == '') {
                        $userdata = AdminModel::query()->leftjoin('admin_user_group','nm_admin.access_group_id','=','admin_user_group.id')
                    ->where('nm_admin.status','=',$status)->where('nm_admin.admin_type','=',2)->where('nm_admin.access_group_id','!=',6)->where('nm_admin.access_group_id','=',$access_group_id)->orderBy('nm_admin.created_at','desc');

                 $abc =  $userdata->select('nm_admin.adm_id','nm_admin.adm_fname','nm_admin.adm_lname','nm_admin.adm_email','nm_admin.adm_phone','nm_admin.adm_address1','nm_admin.dob','admin_user_group.group_name as groupname','nm_admin.status','nm_admin.last_log_in')->get();

                } else if ($status != "" &&  $access_group_id == '' && $this->search_keyword != '') {
                     $userdata = AdminModel::query()->leftjoin('admin_user_group','nm_admin.access_group_id','=','admin_user_group.id')
                    ->where('nm_admin.status','=',$status)->where('nm_admin.admin_type','=',2)->where('nm_admin.access_group_id','!=',6)->where('nm_admin.adm_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('nm_admin.created_at','desc');

                 $abc =  $userdata->select('nm_admin.adm_id','nm_admin.adm_fname','nm_admin.adm_lname','nm_admin.adm_email','nm_admin.adm_phone','nm_admin.adm_address1','nm_admin.dob','admin_user_group.group_name as groupname','nm_admin.status','nm_admin.last_log_in')->get();
                } else if ($status != "" &&  $access_group_id == '' && $this->search_keyword == '') {
                    $userdata = AdminModel::query()->leftjoin('admin_user_group','nm_admin.access_group_id','=','admin_user_group.id')
                    ->where('nm_admin.status','=',$status)->where('nm_admin.admin_type','=',2)->where('nm_admin.access_group_id','!=',6)->orderBy('nm_admin.created_at','desc');

                $abc =  $userdata->select('nm_admin.adm_id','nm_admin.adm_fname','nm_admin.adm_lname','nm_admin.adm_email','nm_admin.adm_phone','nm_admin.adm_address1','nm_admin.dob','admin_user_group.group_name as groupname','nm_admin.status','nm_admin.last_log_in')->get();

                }else if ($status == "" &&  $access_group_id != '' && $this->search_keyword != '') {

                $userdata = AdminModel::query()->leftjoin('admin_user_group','nm_admin.access_group_id','=','admin_user_group.id')->where('nm_admin.access_group_id','!=',6)
                    ->where('nm_admin.admin_type','=',2)->where('nm_admin.access_group_id','=',$access_group_id)->where('nm_admin.adm_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('nm_admin.created_at','desc');

                 $abc =  $userdata->select('nm_admin.adm_id','nm_admin.adm_fname','nm_admin.adm_lname','nm_admin.adm_email','nm_admin.adm_phone','nm_admin.adm_address1','nm_admin.dob','admin_user_group.group_name as groupname','nm_admin.status','nm_admin.last_log_in')->get();
                }else if ($status == "" &&  $access_group_id != '' && $this->search_keyword == '') {
                    $userdata = AdminModel::query()->leftjoin('admin_user_group','nm_admin.access_group_id','=','admin_user_group.id')
                    ->where('nm_admin.admin_type','=',2)->where('nm_admin.access_group_id','=',$access_group_id)->where('nm_admin.access_group_id','!=',6)->orderBy('nm_admin.created_at','desc');

                 $abc =  $userdata->select('nm_admin.adm_id','nm_admin.adm_fname','nm_admin.adm_lname','nm_admin.adm_email','nm_admin.adm_phone','nm_admin.adm_address1','nm_admin.dob','admin_user_group.group_name as groupname','nm_admin.status','nm_admin.last_log_in')->get();

                }else if ($status == "" &&  $access_group_id == '' && $this->search_keyword != '') {
                    $userdata = AdminModel::query()->leftjoin('admin_user_group','nm_admin.access_group_id','=','admin_user_group.id')
                    ->where('nm_admin.admin_type','=',2)->where('nm_admin.access_group_id','!=',6)->where('nm_admin.adm_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('nm_admin.created_at','desc');

                $abc =  $userdata->select('nm_admin.adm_id','nm_admin.adm_fname','nm_admin.adm_lname','nm_admin.adm_email','nm_admin.adm_phone','nm_admin.adm_address1','nm_admin.dob','admin_user_group.group_name as groupname','nm_admin.status','nm_admin.last_log_in')->get();

                }else{
                    $userdata = AdminModel::query()->leftjoin('admin_user_group','nm_admin.access_group_id','=','admin_user_group.id')
                    ->where('nm_admin.admin_type','=',2)->where('nm_admin.access_group_id','!=',6)->orderBy('nm_admin.created_at','desc');

                 $abc =  $userdata->select('nm_admin.adm_id','nm_admin.adm_fname','nm_admin.adm_lname','nm_admin.adm_email','nm_admin.adm_phone','nm_admin.adm_address1','nm_admin.dob','admin_user_group.group_name as groupname','nm_admin.status','nm_admin.last_log_in')->get();
                }
            $output = array();
            foreach ($abc as $key => $value) {
            $internal_output = array();
            $internal_output[] = $value->adm_id;
            $internal_output[] = $value->adm_fname.' '.$value->adm_lname;
            $internal_output[] = $value->adm_email;
            $internal_output[] = $value->adm_phone;
            $internal_output[] = $value->adm_address1;
            $internal_output[] = date("d,M Y", strtotime($value->dob));
            $internal_output[] = $value->groupname;
            if ($value->last_log_in != '') {
                 $internal_output[] = date("d,M Y", strtotime($value->last_log_in));
            }else{
                 $internal_output[] = 'N/A';
            }
            if(Session::get('admin_lang_file') == 'admin_ar_lang'){
            if ($value->status == 1) {
            $internal_output[] = 'نشيط';
            }else{
            $internal_output[] = 'غير نشط';
            }
            }else{
            if ($value->status == 1) {
            $internal_output[] = 'Active';
            }else{
            $internal_output[] = 'Inactive';
            }
            }
            $output[]=  $internal_output;
            }
            $collect_abc = collect($output);
            return $collect_abc;
    }
}
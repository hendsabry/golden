<?php 
namespace App\Http\Controllers\Admin\Csv;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Session;
use Illuminate\Http\Request;
use App\Category;
use DB;

class CategoryExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
	use Exportable;

    public function forYear(Request $request)
    {   	
         $this->input = $request->all();;

         return $this;
    }
    public function headings(): array
    {
         if(Session::get('admin_lang_file') == 'admin_ar_lang'){
             return [
           'هوية شخصية',
            'اسم التصنيف',
            'وصف',
            'الباعة النشطون',
            'القسم الرئيسي',
            'الحالة',        
        ];
         }else{
             return [
           'Id',
            'Category Name',
            'Description',
            'Active Vendors',
            'Parent Category',
            'Status',        
        ];
         }
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:N1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            },
        ];
    }

     public function collection()
    {
        $input = $this->input;
            if(isset($input['search_keyword']))
            {
            $this->search_keyword = trim($input['search_keyword']);
            }
            else
            {
            $this->search_keyword = '';
            }       
            $abc =  Category::query()->select('mc_id','mc_name','mc_name_ar','mc_discription','parent_id','mc_status','mc_discription_ar')->where('mc_name','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->get();
             $output = array();
            foreach ($abc as $key => $value) {
            $internal_output = array();
            $internal_output[] = $value->mc_id;
            if(Session::get('admin_lang_file') == 'admin_ar_lang'){
                 $internal_output[] = $value->mc_name_ar;
                 $internal_output[] = $value->mc_discription_ar;
            }else{
                 $internal_output[] = $value->mc_name;
                 $internal_output[] = $value->mc_discription;
            }
             $internal_output[] = Category::query()->where('parent_id','=', $value->mc_id )->count();

            if(Session::get('admin_lang_file') == 'admin_ar_lang'){
            if ($value->parent_id != 0) {
            $parnet =  Category::query()->select('mc_name_ar')->where('mc_id','=', $value->parent_id)->first();
             if (isset($parnet)) {
            $internal_output[] = $parnet->mc_name_ar;
            }else{
            $internal_output[] = '';
            }
            }else{
            $internal_output[] ='';
            }
            if ($value->mc_status == 1) {
            $internal_output[] = 'نشيط';
            }else{
            $internal_output[] = 'غير نشط';
            }
            }else{
            if ($value->parent_id != 0) {
            $parnet =  Category::query()->select('mc_name')->where('mc_id','=', $value->parent_id)->first();
            if (isset($parnet)) {
            $internal_output[] = $parnet->mc_name;
            }else{
            $internal_output[] = '';
            }
            }else{
            $internal_output[] ='';
            }
            if ($value->mc_status == 1) {
            $internal_output[] = 'Active';
            }else{
            $internal_output[] = 'Inactive';
            }
            }
            $output[]=  $internal_output;
            }
            $collect_abc = collect($output);
            return $collect_abc;

        
    }
}
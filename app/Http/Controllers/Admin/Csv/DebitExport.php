<?php 
namespace App\Http\Controllers\Admin\Csv;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Session;
use Illuminate\Http\Request;
use App\Order;
use App\MerchantPaymentHistory;

class DebitExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
	use Exportable;

    public function forYear(Request $request)
    {
         $this->input = $request->all();;

         return $this;
    }
    public function headings(): array
    {
        if(Session::get('admin_lang_file') == 'admin_ar_lang'){
                 return [
            'معرف المعاملة',
            'اسم المدفوع',
            'دفع ضد',
            'كمية',
            'تاريخ',
            'طريقة الدفع',
            'اسم المورد المرتبط',
        ];
        }else{
                 return [
            'Transaction Id',
            'Payer Name',
            'Payment Against',
            'Amount',
            'Date',
            'Payment Mode',
            'Associated Vendor Name',
        ];
        }
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:N1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            },
        ];
    }
     public function collection()
    {
            $input = $this->input;
            if(isset($input['created_date']) && $input['created_date'] !='')
            {
            $created_date = date("Y-m-d", strtotime($input['created_date']));
            }else{
            $created_date = '';
            } 
            if (isset($input['order_type'])) {
               $order_type = $input['order_type'];
            }else{
                $order_type = '';
            }
            if($order_type != '' &&  $created_date != ''){
                $orders = Order::select('order_id')->where('order_type','=',$order_type)->get()->toArray();
                $orders_type = array_map('current', $orders);
                $userdata = MerchantPaymentHistory::query()->leftjoin('nm_merchant','nm_merchant_payment_history.merchant_id','=','nm_merchant.mer_id')->whereIn('nm_merchant_payment_history.order_id',$orders_type)->leftjoin('nm_order','nm_merchant_payment_history.order_id','=','nm_order.order_id')->leftjoin('nm_customer','nm_order.order_cus_id','nm_customer.cus_id')->where('nm_merchant_payment_history.created_at','like',$created_date.'%')->orderBy('nm_merchant_payment_history.created_at','desc');
                $abc =  $userdata->select('nm_merchant_payment_history.transation_id','nm_merchant_payment_history.status','nm_merchant_payment_history.order_id','nm_merchant_payment_history.amount','nm_merchant_payment_history.created_at','nm_merchant.mer_fname','nm_merchant.mer_lname','nm_order.order_paytype')->get();

            }else if($order_type != '' &&  $created_date == '')
            { $orders = Order::select('order_id')->where('order_type','=',$order_type)->get()->toArray();
                $orders_type = array_map('current', $orders);
                $userdata = MerchantPaymentHistory::query()->leftjoin('nm_merchant','nm_merchant_payment_history.merchant_id','=','nm_merchant.mer_id')->leftjoin('nm_order','nm_merchant_payment_history.order_id','=','nm_order.order_id')->leftjoin('nm_customer','nm_order.order_cus_id','nm_customer.cus_id')->whereIn('nm_merchant_payment_history.order_id',$orders_type)->orderBy('nm_merchant_payment_history.created_at','desc');
                $abc =   $userdata->select('nm_merchant_payment_history.transation_id','nm_merchant_payment_history.status','nm_merchant_payment_history.order_id','nm_merchant_payment_history.amount','nm_merchant_payment_history.created_at','nm_merchant.mer_fname','nm_merchant.mer_lname','nm_order.order_paytype')->get();
            }else if ($order_type == '' &&  $created_date != '' ) {
                $userdata = MerchantPaymentHistory::query()->leftjoin('nm_merchant','nm_merchant_payment_history.merchant_id','=','nm_merchant.mer_id')->leftjoin('nm_order','nm_merchant_payment_history.order_id','=','nm_order.order_id')->leftjoin('nm_customer','nm_order.order_cus_id','nm_customer.cus_id')->where('nm_merchant_payment_history.created_at','like',$created_date.'%')->orderBy('nm_merchant_payment_history.created_at','desc');
                $abc =   $userdata->select('nm_merchant_payment_history.transation_id','nm_merchant_payment_history.status','nm_merchant_payment_history.order_id','nm_merchant_payment_history.amount','nm_merchant_payment_history.created_at','nm_merchant.mer_fname','nm_merchant.mer_lname','nm_order.order_paytype')->get();
            }else{
                $userdata = MerchantPaymentHistory::query()->leftjoin('nm_merchant','nm_merchant_payment_history.merchant_id','=','nm_merchant.mer_id')->leftjoin('nm_order','nm_merchant_payment_history.order_id','=','nm_order.order_id')->leftjoin('nm_customer','nm_order.order_cus_id','nm_customer.cus_id')->orderBy('nm_merchant_payment_history.created_at','desc');
                $abc =   $userdata->select('nm_merchant_payment_history.transation_id','nm_merchant_payment_history.status','nm_merchant_payment_history.order_id','nm_merchant_payment_history.amount','nm_merchant_payment_history.created_at','nm_merchant.mer_fname','nm_merchant.mer_lname','nm_order.order_paytype')->get();
            }
            $output = array();
            foreach ($abc as $key => $value) {
            $internal_output = array();
            $internal_output[] = $value->transation_id;
            if(Session::get('admin_lang_file') == 'admin_ar_lang'){
                if ($value->status == 2) {
                   $internal_output[] = 'مشرف';
                }else{
                    $internal_output[] ='';
                }
            }else{
                  if ($value->status == 2) {
                   $internal_output[] = 'Admin';
                }else{
                    $internal_output[] ='';
                }

            }
            $internal_output[] = $value->order_id;
            $internal_output[] = $value->amount;
            $internal_output[] = date("d,M Y", strtotime($value->created_at));
            $internal_output[] = $value->order_paytype;
            $internal_output[] = $value->mer_fname.' '.$value->mer_lname;
            $output[]=  $internal_output;
            }
            $collect_abc = collect($output);
            return $collect_abc;
    }
}
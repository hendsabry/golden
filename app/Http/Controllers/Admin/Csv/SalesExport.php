<?php 
namespace App\Http\Controllers\Admin\Csv;
use Session;
use Lang;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\AdminModel;
use App\Merchant;
use DB;

class SalesExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
	use Exportable;

    public function forYear(Request $request)
    {

    	
         $this->input = $request->all();;

         return $this;
    }
    public function headings(): array
    {
        if(Session::get('admin_lang_file') == 'admin_ar_lang'){
             return [
           'هوية شخصية',
            'اسم',
            'البريد الإلكتروني',
            'رقم الهاتف',
            'تاريخ الولادة', 
            'عنوان',
            'الهدف (لكل شهر)',
            'الهدف المنشود (الشهر الحالي)',
            'الحالة',                  
        ];
        }else{
             return [
           'Id',
            'Name',
            'Email',
            'Phone No',
            'Date of Birth', 
            'Address',
            'Target (Per Month)',
            'Target Achieved (Current Month)',
            'Status',                  
        ];
        }
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:N1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            },
        ];
    }
     public function collection()
    {
         if (Session::has('userid')) {
                    if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                    {
                    $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');

                    }
                    else 
                    {
                    $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                    }

                    $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail
    	    $input = $this->input;
            if(isset($input['search_keyword']))
            {
            $this->search_keyword = trim($input['search_keyword']);
            }
            else
            {
            $this->search_keyword = '';
            }        
            if (isset($input['status'])) {
            	$status  = trim($input['status']);
            }else{
            	$status  ="";
            }

            if( $status != "" &&  $this->search_keyword != ''){
                        if (Session::get('access_group_id') == 1) {

                         $abc =  AdminModel::query()->select('adm_id','adm_fname','adm_lname','adm_email','adm_phone','dob','adm_address1','target','code','status')->where('adm_fname','LIKE','%'.$this->search_keyword .'%')->where('status','=',$status)->orderBy('created_at','desc')->get(); 
                        }else{

                          //   $userdata =  AdminModel::query()->leftjoin('nm_merchant','nm_admin.code','=','nm_merchant.sales_rep_code')->where('nm_admin.sales_manager_id', '=', $user->adm_id)->where('nm_admin.adm_fname','LIKE','%'.$this->search_keyword .'%')->where('nm_admin.status','=',$status)->orderBy('nm_admin.created_at','desc');
                          // return  $userdata->select('nm_admin.adm_id','nm_admin.adm_fname','nm_admin.adm_lname','nm_admin.adm_email','nm_admin.adm_phone','nm_admin.dob','nm_admin.adm_address1','nm_admin.target', DB::raw('count(nm_merchant.mer_id) as count'));
                                $abc = AdminModel::query()->select('adm_id','adm_fname','adm_lname','adm_email','adm_phone','dob','adm_address1','target','code','status')->where('sales_manager_id', '=', $user->adm_id)->where('adm_fname','LIKE','%'.$this->search_keyword .'%')->where('status','=',$status)->orderBy('created_at','desc')->get();
                        }
            //search list with keyword and status 
            }else if($this->search_keyword != '' && $status == "")
            { 
                if (Session::get('access_group_id') == 1) {

                         // $userdata =  AdminModel::query()->leftjoin('nm_merchant','nm_admin.code','=','nm_merchant.sales_rep_code')->where('nm_admin.sales_manager_id', '!=', 0)->where('nm_admin.adm_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('nm_admin.created_at','desc');
                         //  return  $userdata->select('nm_admin.adm_id','nm_admin.adm_fname','nm_admin.adm_lname','nm_admin.adm_email','nm_admin.adm_phone','nm_admin.dob','nm_admin.adm_address1','nm_admin.target', DB::raw('count(nm_merchant.mer_id) as count'));


                                $abc =  AdminModel::query()->select('adm_id','adm_fname','adm_lname','adm_email','adm_phone','dob','adm_address1','code','status')->where('sales_manager_id', '!=', 0)->where('adm_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->get();
                        }else{

                          //   $userdata =  AdminModel::query()->leftjoin('nm_merchant','nm_admin.code','=','nm_merchant.sales_rep_code')->where('nm_admin.sales_manager_id', '=', $user->adm_id)->where('nm_admin.adm_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('nm_admin.created_at','desc');
                          // return  $userdata->select('nm_admin.adm_id','nm_admin.adm_fname','nm_admin.adm_lname','nm_admin.adm_email','nm_admin.adm_phone','nm_admin.dob','nm_admin.adm_address1','nm_admin.target', DB::raw('count(nm_merchant.mer_id) as count'));

                                $abc =  AdminModel::query()->select('adm_id','adm_fname','adm_lname','adm_email','adm_phone','dob','adm_address1','code','status')->where('sales_manager_id', '=', $user->adm_id)->where('adm_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->get();
                        }
            
            // search list with keyword
            }else if ($this->search_keyword == '' && $status != "" ) {
                if (Session::get('access_group_id') == 1) {

                            // $userdata =  AdminModel::query()->leftjoin('nm_merchant','nm_admin.code','=','nm_merchant.sales_rep_code')->where('nm_admin.sales_manager_id', '!=', 0)->where('nm_admin.status','=',$status)->orderBy('nm_admin.created_at','desc');
                            //     return  $userdata->select('nm_admin.adm_id','nm_admin.adm_fname','nm_admin.adm_lname','nm_admin.adm_email','nm_admin.adm_phone','nm_admin.dob','nm_admin.adm_address1','nm_admin.target', DB::raw('count(nm_merchant.mer_id) as count'));

                                $abc = AdminModel::query()->select('adm_id','adm_fname','adm_lname','adm_email','adm_phone','dob','adm_address1','target','code','status')->where('sales_manager_id', '!=', 0)->where('status','=',$status)->orderBy('created_at','desc')->get();
                        }else{

                          //    $userdata =  AdminModel::query()->leftjoin('nm_merchant','nm_admin.code','=','nm_merchant.sales_rep_code')->where('nm_admin.sales_manager_id', '=', $user->adm_id)->where('nm_admin.status','=',$status)->orderBy('nm_admin.created_at','desc');
                          // return  $userdata->select('nm_admin.adm_id','nm_admin.adm_fname','nm_admin.adm_lname','nm_admin.adm_email','nm_admin.adm_phone','nm_admin.dob','nm_admin.adm_address1','nm_admin.target', DB::raw('count(nm_merchant.mer_id) as count'));

                                    $abc = AdminModel::query()->select('adm_id','adm_fname','adm_lname','adm_email','adm_phone','dob','adm_address1','target','code','status')->where('sales_manager_id', '=', $user->adm_id)->where('status','=',$status)->orderBy('created_at','desc')->get();
                        }
            //search list with status 
            }else{
                if (Session::get('access_group_id') == 1) {
                         // $userdata =  AdminModel::query()->where('sales_manager_id', '!=', 0)->orderBy('created_at','desc');
                         //     return $userdata->select('adm_id','adm_fname','adm_lname','adm_email','adm_phone','dob','adm_address1','target','code')->get();
                        
                            $abc =  AdminModel::query()->select('adm_id','adm_fname','adm_lname','adm_email','adm_phone','dob','adm_address1','target','code','status')->where('sales_manager_id', '!=', 0)->orderBy('created_at','desc')->get();
                        }else{
                          //   $userdata =  AdminModel::query()->leftjoin('nm_merchant','nm_admin.code','=','nm_merchant.sales_rep_code')->where('nm_admin.sales_manager_id', '=', $user->adm_id)->orderBy('nm_admin.created_at','desc');
                          // return  $userdata->select('nm_admin.adm_id','nm_admin.adm_fname','nm_admin.adm_lname','nm_admin.adm_email','nm_admin.adm_phone','nm_admin.dob','nm_admin.adm_address1','nm_admin.target');

                           $abc =  AdminModel::query()->select('adm_id','adm_fname','adm_lname','adm_email','adm_phone','dob','adm_address1','target','code','status')->where('sales_manager_id', '=', $user->adm_id)->orderBy('created_at','desc')->get();
                        }
            }
            $output = array();
            foreach ($abc as $key => $value) {
            $internal_output = array();
            $internal_output[] = $value->adm_id;
            $internal_output[] = $value->adm_fname.' '.$value->adm_lname;
            $internal_output[] = $value->adm_email;
            $internal_output[] = $value->adm_phone;
            $internal_output[] = date("d,M Y", strtotime($value->dob));
            $internal_output[] = $value->adm_address1;
            $internal_output[] = $value->target;
            if ($value->code != '') {
             $internal_output[] =  Merchant::query()->where('sales_rep_code','=',$value->code)->count();
            }else{
              $internal_output[] = 0;
            }
            if(Session::get('admin_lang_file') == 'admin_ar_lang'){
            if ($value->status == 1) {
            $internal_output[] = 'نشيط';
            }else{
            $internal_output[] = 'غير نشط';
            }
            }else{
            if ($value->status == 1) {
            $internal_output[] = 'Active';
            }else{
            $internal_output[] = 'Inactive';
            }
            }
            $output[]=  $internal_output;
            }
            $collect_abc = collect($output);
            return $collect_abc;

        }
    }
}
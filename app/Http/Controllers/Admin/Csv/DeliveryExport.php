<?php 
namespace App\Http\Controllers\Admin\Csv;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Session;
use Illuminate\Http\Request;
use App\Delivery;

class DeliveryExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
	use Exportable;

    public function forYear(Request $request)
    {

    	
         $this->input = $request->all();;

         return $this;
    }
    public function headings(): array
    {
        if(Session::get('admin_lang_file') == 'admin_ar_lang'){
              return [
            'هوية شخصية',
            'اسم',
            'البريد الإلكتروني',
            'رقم الهاتف',
            'تاريخ الولادة',
            'عنوان',
            'الحالة',
        ];
        }else{
              return [
            'Id',
            'Name',
            'Email',
            'Phone No',
            'Date of Birth',
            'Address',
            'Status',
        ];
        }
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:N1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            },
        ];
    }
     public function collection()
    {
    	$input = $this->input;
            if(isset($input['search_keyword']))
            {
            $this->search_keyword = trim($input['search_keyword']);
            }
            else
            {
            $this->search_keyword = '';
            }        
            if (isset($input['status'])) {
            	$status  = trim($input['status']);
            }else{
            	$status  ="";
            }

            if( $status != "" &&  $this->search_keyword != ''){
            $abc =  Delivery::query()->select('id','name','email','phone_no','dob','address','status')->where('name','LIKE','%'.$this->search_keyword .'%')->where('status','=',$status)->orderBy('created_at','desc')->get();//search list with keyword and status 
            }else if($this->search_keyword != '' && $status == "")
            { 
           $abc =  Delivery::query()->select('id','name','email','phone_no','dob','address','status')->where('name','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->get();
            // search list with keyword
            }else if ($this->search_keyword == '' && $status != "" ) {
           $abc = Delivery::query()->select('id','name','email','phone_no','dob','address','status')->where('status','=',$status)->orderBy('created_at','desc')->get();//search list with status 
            }else{
           $abc = Delivery::query()->select('id','name','email','phone_no','dob','address','status')->orderBy('created_at','desc')->get();
            }
             $output = array();
            foreach ($abc as $key => $value) {
            $internal_output = array();
            $internal_output[] = $value->id;
            $internal_output[] = $value->name;
            $internal_output[] = $value->email;
            $internal_output[] = $value->phone_no;
            $internal_output[] = date("d,M Y", strtotime($value->dob));
            $internal_output[] = $value->address;
            if(Session::get('admin_lang_file') == 'admin_ar_lang'){
            if ($value->status == 1) {
            $internal_output[] = 'نشيط';
            }else{
            $internal_output[] = 'غير نشط';
            }
            }else{
            if ($value->status == 1) {
            $internal_output[] = 'Active';
            }else{
            $internal_output[] = 'Inactive';
            }
            }
            $output[]=  $internal_output;
            }
            $collect_abc = collect($output);
            return $collect_abc;
    }
}
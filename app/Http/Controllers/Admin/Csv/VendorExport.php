<?php 
namespace App\Http\Controllers\Admin\Csv;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Http\Request;
use Session;
use App\AdminModel;
use App\Merchant;

class VendorExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
	use Exportable;

    public function forYear(Request $request)
    {

    	
         $this->input = $request->all();;

         return $this;
    }
    public function headings(): array
    {
       if(Session::get('admin_lang_file') == 'admin_ar_lang'){
         return [
            'هوية شخصية',
            'اسم',
            'البريد الإلكتروني',
            'رقم الهاتف',
            'آخر تسجيل الدخول',
            'مبلغ المحفظة',
            'الحالة',
        ];
       }else{
         return [
            'Id',
            'Name',
            'Email',
            'Phone No',
            'Last Logged in',
            'Wallet Amount',
            'Status',
        ];
       }
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:N1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            },
        ];
    }
     public function collection()
    {
    	$input = $this->input;
            if(isset($input['search_keyword']))
            {
            $this->search_keyword = trim($input['search_keyword']);
            }
            else
            {
            $this->search_keyword = '';
            }        
            if (isset($input['status'])) {
            	$status  = trim($input['status']);
            }else{
            	$status  ="";
            }


            if( $status != "" &&  $this->search_keyword != ''){
                        if (Session::get('access_group_id') ==1) {
                          $abc =  Merchant::query()->select('mer_id','mer_fname','mer_lname','mer_email','mer_phone','wallet','mer_staus')->where('mer_fname','LIKE','%'.$this->search_keyword .'%')->where('mer_staus','=',$status)->orderBy('created_at','desc')->get();

                        }elseif (Session::get('access_group_id') ==4) {
                           $all_rep = AdminModel::select('adm_id')->where('sales_manager_id','=',Session::get('userid'))->get()->toArray();
                          $all_code = AdminModel::select('code')->whereIn('adm_id',$all_rep)->get()->toArray();
                          $abc =  Merchant::query()->select('mer_id','mer_fname','mer_lname','mer_email','mer_phone','wallet','mer_staus')->whereIn('sales_rep_code',$all_code)->where('mer_fname','LIKE','%'.$this->search_keyword .'%')->where('mer_staus','=',$status)->orderBy('created_at','desc')->get();
                          # code...
                        }elseif (Session::get('access_group_id') ==6) {
                           $all_code = AdminModel::select('code')->where('adm_id','=',Session::get('userid'))->get()->toArray();
                         $abc =  Merchant::query()->select('mer_id','mer_fname','mer_lname','mer_email','mer_phone','wallet','mer_staus')->whereIn('sales_rep_code',$all_code)->where('mer_fname','LIKE','%'.$this->search_keyword .'%')->where('mer_staus','=',$status)->orderBy('created_at','desc')->get();
                          # code...
                        }else{
                          $abc =  Merchant::query()->select('mer_id','mer_fname','mer_lname','mer_email','mer_phone','wallet','mer_staus')->where('mer_fname','LIKE','%'.$this->search_keyword .'%')->where('mer_staus','=',$status)->orderBy('created_at','desc')->get();
                        }


                      //search list with keyword and status 
                      }else if($this->search_keyword != '' && $status == "")
                      { 

                        if (Session::get('access_group_id') ==1) {
                          $abc = Merchant::query()->select('mer_id','mer_fname','mer_lname','mer_email','mer_phone','wallet','mer_staus')->where('mer_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->get();

                        }elseif (Session::get('access_group_id') ==4) {
                           $all_rep = AdminModel::select('adm_id')->where('sales_manager_id','=',Session::get('userid'))->get()->toArray();
                          $all_code = AdminModel::select('code')->whereIn('adm_id',$all_rep)->get()->toArray();
                          $abc =  Merchant::query()->select('mer_id','mer_fname','mer_lname','mer_email','mer_phone','wallet','mer_staus')->whereIn('sales_rep_code',$all_code)->where('mer_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->get();
                          # code...
                        }elseif (Session::get('access_group_id') ==6) {
                           $all_code = AdminModel::select('code')->where('adm_id','=',Session::get('userid'))->get()->toArray();
                         $abc =  Merchant::query()->select('mer_id','mer_fname','mer_lname','mer_email','mer_phone','wallet','mer_staus')->whereIn('sales_rep_code',$all_code)->where('mer_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->get();
                          # code...
                        }else{
                          $abc =  Merchant::query()->select('mer_id','mer_fname','mer_lname','mer_email','mer_phone','wallet','mer_staus')->where('mer_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->get();
                        }
                      // search list with keyword
                      }else if ($this->search_keyword == '' && $status != "" ) {

                        if (Session::get('access_group_id') ==1) {
                          $abc =  Merchant::query()->select('mer_id','mer_fname','mer_lname','mer_email','mer_phone','wallet','mer_staus')->where('mer_staus','=',$status)->orderBy('created_at','desc')->get();

                        }elseif (Session::get('access_group_id') ==4) {
                           $all_rep = AdminModel::select('adm_id')->where('sales_manager_id','=',Session::get('userid'))->get()->toArray();
                          $all_code = AdminModel::select('code')->whereIn('adm_id',$all_rep)->get()->toArray();
                          $abc =  Merchant::query()->select('mer_id','mer_fname','mer_lname','mer_email','mer_phone','wallet','mer_staus')->whereIn('sales_rep_code',$all_code)->where('mer_staus','=',$status)->orderBy('created_at','desc')->get();
                          # code...
                        }elseif (Session::get('access_group_id') ==6) {
                           $all_code = AdminModel::select('code')->where('adm_id','=',Session::get('userid'))->get()->toArray();
                         $abc =  Merchant::query()->select('mer_id','mer_fname','mer_lname','mer_email','mer_phone','wallet','mer_staus')->whereIn('sales_rep_code',$all_code)->where('mer_staus','=',$status)->orderBy('created_at','desc')->get();
                          # code...
                        }else{

                          $abc =  Merchant::query()->select('mer_id','mer_fname','mer_lname','mer_email','mer_phone','wallet','mer_staus')->where('mer_staus','=',$status)->orderBy('created_at','desc')->get();
                        }//search list with status 
                      }else{

                        if (Session::get('access_group_id') ==1) {
                           $abc =  Merchant::query()->select('mer_id','mer_fname','mer_lname','mer_email','mer_phone','wallet','mer_staus')->orderBy('created_at','desc')->get();

                        }elseif (Session::get('access_group_id') ==4) {
                           $all_rep = AdminModel::select('adm_id')->where('sales_manager_id','=',Session::get('userid'))->get()->toArray();
                          $all_code = AdminModel::select('code')->whereIn('adm_id',$all_rep)->get()->toArray();
                           $abc =  Merchant::query()->select('mer_id','mer_fname','mer_lname','mer_email','mer_phone','wallet','mer_staus')->whereIn('sales_rep_code',$all_code)->orderBy('created_at','desc')->get();
                          # code...
                        }elseif (Session::get('access_group_id') ==6) {
                           $all_code = AdminModel::select('code')->where('adm_id','=',Session::get('userid'))->get()->toArray();
                         $abc =  Merchant::query()->select('mer_id','mer_fname','mer_lname','mer_email','mer_phone','wallet','mer_staus')->whereIn('sales_rep_code',$all_code)->orderBy('created_at','desc')->get();
                          # code...
                        }else{
                           $abc =  Merchant::query()->select('mer_id','mer_fname','mer_lname','mer_email','mer_phone','wallet','mer_staus')->orderBy('created_at','desc')->get();
                        }
                      }
                      $output = array();
                      foreach ($abc as $key => $value) {
                      $internal_output = array();
                      $internal_output[] = $value->mer_id;
                      $internal_output[] = $value->mer_fname.' '.$value->mer_lname;
                      $internal_output[] = $value->mer_email;
                      $internal_output[] = $value->mer_phone;
                      $internal_output[] = '';
                      $internal_output[] = $value->wallet;
                      if(Session::get('admin_lang_file') == 'admin_ar_lang'){
                      if ($value->mer_staus == 1) {
                      $internal_output[] = 'نشيط';
                      }else{
                      $internal_output[] = 'غير نشط';
                      }
                      }else{
                      if ($value->mer_staus == 1) {
                      $internal_output[] = 'Active';
                      }else{
                      $internal_output[] = 'Inactive';
                      }
                      }
                      $output[]=  $internal_output;
                      }
                      $collect_abc = collect($output);
                      return $collect_abc;    
   }
}
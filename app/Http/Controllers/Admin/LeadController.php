<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB;
use Excel;
use Session;
use Lang;
use Response;
use Auth;
use Exception;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Models;
use App\AdminModel;
use App\AdminMenu;
use App\AdminUserGroup;
use App\Country;
use App\City;
use App\User;
Use App\Category;
Use App\Lead;

class LeadController extends Controller {
  public function __construct(){
    parent::__construct();
// set admin Panel language
    $this->setLanguageLocaleAdmin();
  }  

               public function Leadlist() 
                  {

                    if (Session::has('userid')) 
                      {
                        if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                          {
                            $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');

                          }
                          else 
                          {
                            $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                          }

                          $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail

                          // $users =  Lead::with('manager')->with('sub')->orderBy('created_at','desc')->with('get_cat')->paginate(10); //list of sub admin   

                          if (Session::get('access_group_id') == 1) {
                            $users =  Lead::with('manager')->with('sub')->orderBy('created_at','desc')->with('get_cat')->paginate(10);
                          }elseif (Session::get('access_group_id') == 4) {
                           $users =  Lead::where('sales_head','=',Session::get('userid'))->with('manager')->with('sub')->orderBy('created_at','desc')->with('get_cat')->paginate(10);
                          }elseif (Session::get('access_group_id') == 6) {
                           $users =  Lead::where('sales_executive','=',Session::get('userid'))->with('manager')->with('sub')->orderBy('created_at','desc')->with('get_cat')->paginate(10);
                          }else{
                             $users =  Lead::with('manager')->with('sub')->orderBy('created_at','desc')->with('get_cat')->paginate(10);
                          }   

                          foreach ($users as $mer_list) {
                                        
                            $isconverted=DB::table('nm_merchant')->where('mer_email',$mer_list->client_email)->count();
                              $mer_list['isconverted']=$isconverted;
                                          } 
                                                        
                          $groups =  AdminUserGroup::where('level','=',2)->get();
                          $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                          $group =  explode(",",$groups_1->group_permission);
                          $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                           foreach ($users as $key => $value) 
                        {
                            $value['updated'] = date("M d, Y", strtotime($value['created_at']));
                         }   
                          return view('admin.lead.lead',compact('users','users'))->with("routemenu",$session_message)->with('user',$user)->with('groups',$groups)->with('admin_menu',$admin_menu);;
                      }else 
                      {
                        return Redirect::to('siteadmin');
                      } 
                }                  



                  public function convertedvendor() 
                  {

                    if (Session::has('userid')) 
                      {
                        if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                          {
                            $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');

                          }
                          else 
                          {
                            $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                          }
                         
                          $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail

                          

                          // $users =  Lead::with('manager')->with('sub')->orderBy('created_at','desc')->with('get_cat')->paginate(10); //list of sub admin   
                            
                          if (Session::get('access_group_id') == 6 && $user->code!='') {
                              $ismechentconverted=DB::table('nm_merchant')->where('sales_rep_code',$user->code)->orderBy('mer_fname')->paginate(10);
                          }else{
                             $ismechentconverted =  array();
                          }   

                        

                                                        
                          $groups =  AdminUserGroup::where('level','=',2)->get();
                          $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                          $group =  explode(",",$groups_1->group_permission);
                          $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                          
                          return view('admin.lead.covertedlead',compact('convertedmerchants',$ismechentconverted))->with("routemenu",$session_message)->with('convertedmerchants',$ismechentconverted)->with('groups',$groups)->with('admin_menu',$admin_menu);;
                      }else 
                      {
                        return Redirect::to('siteadmin');
                      } 
                }                  


                public function changeStatus(Request $request)
                {
                    $id = $request->get('id');
                    $status = $request->get('status');                    
                    $dta= array();
                    $dta['status'] = $status;
                    Lead::where('id','=',$id)->update($dta); // change status of sales rep 
                    return 1 ;
                }
         
                public function addLead(Request $request) 
                {
                  if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                    {
                      $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
                    }
                    else 
                    {
                      $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                    }                   
                     $cat = Category::where('mc_status', '=', 1)->get();         
                   
                $groups =  AdminUserGroup::where('level','=',2)->get(); //get admin groups
                if (Session::get('access_group_id') == 4) {
                    $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
                    $sales_executive =  AdminModel::where('sales_manager_id', '=', $user->adm_id)->orderBy('created_at','desc')->get(); 
                }elseif (Session::get('access_group_id') == 6) {
                  $sales_executive =  AdminModel::where('adm_id', '=', Session::get('userid'))->get();

                    $user =  AdminModel::where('adm_id','=',$sales_executive[0]->sales_manager_id)->with('getCity')->with('getCountry')->first();
                    
                }else{
                    $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
                    $sales_executive =  AdminModel::where('sales_manager_id', '=', $user->adm_id)->orderBy('created_at','desc')->get(); 
                }
                $selected_city  = City::select('ci_con_id')->where('ci_status','=',1)->distinct()->get()->toArray();
                $selected_country = array();
                foreach ($selected_city as $key => $value) {
                  $selected_country[] = $value['ci_con_id'];
                }
                $countries = Country::whereIn('co_id',$selected_country)->where('co_status','=',1)->groupBy('co_name')->get();
                $select_city = array();
                $select_city =  City::where('ci_con_id','=',$countries[0]->co_id)->where('ci_status','=',1)->where('ci_status','=',1)->get();
                //get country
                if ($request->isMethod('post'))
                {
                  $validator = Validator::make($request->all(), [
                    'lead_name'    => 'required',
                    'client_email' => 'required',
                    'client_ph'    => 'required',                      
                    
                  ]);
                  if ($validator->passes()) 
                  {
                    $input = $request->all();                                  
                    unset($input['_token']);
                    $led = Lead::firstOrCreate($input); 
                    if($led)
                      {  $arr = array();
                        if($led->id >=0 && $led->id < 9)
                        {
                          $arr['lead_id']  = 'L00'.$led->id;
                        }
                        elseif($led->id >=10 && $led->id < 99)
                        {
                          $arr['lead_id']  = 'L0'.$led->id;
                        }
                        elseif($led->id >=100 && $led->id < 999)
                        {
                          $arr['lead_id']  = 'L'.$led->id;
                        }else
                        {
                          $arr['lead_id']  = 'L'.$led->id;
                        }
                        Lead::where('id',$led->id)->update($arr);             
                        Session::put('msg', 'Successfully added a new Sales Rep.');
                        return Redirect::to('/admin/lead');
                      }else{
                        return Redirect::to('/admin/lead/add')
                        ->with('message','Something went wrong please try again')
                        ->withErrors($validator)->withInput();
                      }
                    }
                  }
                  $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                  $group =  explode(",",$groups_1->group_permission);
                  $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                  return view('admin.lead.add_lead')->with("routemenu",$session_message)->with('groups',$groups)->with('countries',$countries)->with('select_city',$select_city)->with('admin_menu',$admin_menu)->with('cat' ,$cat)->with('sales_executive' ,$sales_executive)->with('user' ,$user);              
                }

                public function editLead(Request $request,$id)
                {
                  if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                    {
                      $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
                    }
                    else 
                    {
                      $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                    }
                     // $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first(); 
                      if (Session::get('access_group_id') == 4) {
                    $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
                    $sales_executive =  AdminModel::where('sales_manager_id', '=', $user->adm_id)->orderBy('created_at','desc')->get(); 
                }elseif (Session::get('access_group_id') == 6) {
                  $sales_executive =  AdminModel::where('adm_id', '=', Session::get('userid'))->get();

                    $user =  AdminModel::where('adm_id','=',$sales_executive[0]->sales_manager_id)->with('getCity')->with('getCountry')->first();
                }else{
                    $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
                    $sales_executive =  AdminModel::where('sales_manager_id', '=', $user->adm_id)->orderBy('created_at','desc')->get(); 
                }
                    $groups =  AdminUserGroup::where('level','=',2)->get();
                    $users = Lead::where('id','=',$id)->with('manager')->first();                    
                    $cat = Category::where('mc_status', '=', 1)->get();
                    // $sales_executive =  AdminModel::where('sales_manager_id', '=', $user->adm_id)->orderBy('created_at','desc')->paginate(10); 
                    if ($request->isMethod('post'))
                    {
                          $validator = Validator::make($request->all(), [
                            'lead_name'    => 'required',
                            'client_email' => 'required',
                            'client_ph'    => 'required',                        
                                      
                          ]);
                          if ($validator->passes()) 
                          {
                              $input = $request->all();
                              unset($input['_token']);                              
                              $sub_ad = Lead::where('id','=',$id)->update($input);//update lead                               
                              Session::put('msg', 'Successfully added a new Event.');
                              return Redirect::to('/admin/lead');
                            }
                            else
                            {
                              return Redirect::to('/admin/lead/edit/'.$id)
                              ->with('message','Something went wrong please try again')
                              ->withErrors($validator)->withInput();
                            }
                      }
                  $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                  $group =  explode(",",$groups_1->group_permission);
                  $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                  return view('admin.lead.edit_lead')->with("routemenu",$session_message)->with('groups',$groups)->with('users',$users)->with('admin_menu',$admin_menu)->with('cat',$cat)->with('sales_executive',$sales_executive)->with('user',$user);;
                }

                public function viewLead($id)
                {

                  if (Session::has('userid')) 
                  {

                    if(Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') 
                      {
                        $session_message = trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT');

                      }
                      else 
                      {
                        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT');
                      }

                      $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first(); 
                      $sub_admin =  Lead::where('id','=',$id)->with('manager')->with('get_cat')->with('sub')->first();//get lead detail 
                      if($sub_admin->status=='1')
                      {
                        if (Session::get('admin_lang_file') == 'admin_ar_lang') {
                           $sub_admin['status'] = 'نشيط';
                        } else {
                           $sub_admin['status'] = 'Active';
                        }
                        
                      } 
                      else 
                      {
                        if (Session::get('admin_lang_file') == 'admin_ar_lang') {
                            $sub_admin['status'] = 'غير نشط' ;
                        } else {
                            $sub_admin['status'] = 'InActive' ;
                        }
                    
                      }                                  
                      $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                      $group =  explode(",",$groups->group_permission);
                      $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                      return view('admin.lead.view_lead')->with("routemenu",$session_message)->with('user',$user)->with('sub_admin',$sub_admin)->with('admin_menu',$admin_menu);;
                   }
                    else 
                    {
                      return Redirect::to('siteadmin');
                    } 
                }

                public function deletelead($id)
                {
                      $sub_admin = Lead::where('id','=',$id)->first(); 
                    $flag = Lead::where('id','=',$id)->delete();//delete lead
                    return Redirect::to('/admin/lead');
                }   

                public function searchData(Request $request)
                {
                  $search = trim($request->get('search_keyword'));
                  if(isset($search))
                  {
                    $this->search_keyword = $search;
                  }
                  else
                  {
                    $this->search_keyword = '';
                  }   
                  $status  = trim($request->get('status'));        

                if( $status != "" &&  $this->search_keyword != ''){
                   if (Session::get('access_group_id') == 1) {
                    $users =  Lead::where('lead_name','LIKE','%'.$this->search_keyword .'%')->where('status','=',$status)->with('manager')->with('sub')->with('get_cat')->orderBy('created_at','desc')->paginate(10);
                   }elseif (Session::get('access_group_id') == 4) {
                     $users =  Lead::where('sales_head','=',Session::get('userid'))->where('lead_name','LIKE','%'.$this->search_keyword .'%')->where('status','=',$status)->with('manager')->with('sub')->with('get_cat')->orderBy('created_at','desc')->paginate(10);
                   }elseif (Session::get('access_group_id') == 6) {
                    $users =  Lead::where('sales_executive','=',Session::get('userid'))->where('lead_name','LIKE','%'.$this->search_keyword .'%')->where('status','=',$status)->with('manager')->with('sub')->with('get_cat')->orderBy('created_at','desc')->paginate(10);
                   }else{
                      $users =  Lead::where('lead_name','LIKE','%'.$this->search_keyword .'%')->where('status','=',$status)->with('manager')->with('sub')->with('get_cat')->orderBy('created_at','desc')->paginate(10);
                   }
                  //search list with keyword and status 
                }else if($this->search_keyword != '' && $status == "")
                { 
                   if (Session::get('access_group_id') == 1) {
                     $users =  Lead::where('lead_name','LIKE','%'.$this->search_keyword .'%')->with('get_cat')->with('manager')->with('sub')->orderBy('created_at','desc')->paginate(10);
                   }elseif (Session::get('access_group_id') == 4) {
                     $users =  Lead::where('sales_head','=',Session::get('userid'))->where('lead_name','LIKE','%'.$this->search_keyword .'%')->with('get_cat')->with('manager')->with('sub')->orderBy('created_at','desc')->paginate(10);
                   }elseif (Session::get('access_group_id') == 6) {
                      $users =  Lead::where('sales_executive','=',Session::get('userid'))->where('lead_name','LIKE','%'.$this->search_keyword .'%')->with('get_cat')->with('manager')->with('sub')->orderBy('created_at','desc')->paginate(10);
                   }else{
                     $users =  Lead::where('lead_name','LIKE','%'.$this->search_keyword .'%')->with('get_cat')->with('manager')->with('sub')->orderBy('created_at','desc')->paginate(10);
                   }
                 
                  // search list with keyword
                }else if ($this->search_keyword == '' && $status != "" ) {
                   if (Session::get('access_group_id') == 1) {
                     $users =  Lead::where('status','=',$status)->with('get_cat')->with('manager')->with('sub')->orderBy('created_at','desc')->paginate(10);
                   }elseif (Session::get('access_group_id') == 4) {
                      $users =  Lead::where('sales_head','=',Session::get('userid'))->where('status','=',$status)->with('get_cat')->with('manager')->with('sub')->orderBy('created_at','desc')->paginate(10);
                   }elseif (Session::get('access_group_id') == 6) {
                      $users =  Lead::where('sales_executive','=',Session::get('userid'))->where('status','=',$status)->with('get_cat')->with('manager')->with('sub')->orderBy('created_at','desc')->paginate(10);
                   }else{
                       $users =  Lead::where('status','=',$status)->with('get_cat')->with('manager')->with('sub')->orderBy('created_at','desc')->paginate(10);
                   }
                 //search list with status 
                }else{
                   if (Session::get('access_group_id') == 1) {
                    $users =  Lead::orderBy('created_at','desc')->with('get_cat')->with('manager')->with('sub')->paginate(10);
                   }elseif (Session::get('access_group_id') == 4) {
                    $users =  Lead::where('sales_head','=',Session::get('userid'))->orderBy('created_at','desc')->with('get_cat')->with('manager')->with('sub')->paginate(10);
                   }elseif (Session::get('access_group_id') == 6) {
                    $users =  Lead::where('sales_executive','=',Session::get('userid'))->orderBy('created_at','desc')->with('get_cat')->with('manager')->with('sub')->paginate(10);
                   }else{
                    $users =  Lead::orderBy('created_at','desc')->with('get_cat')->with('manager')->with('sub')->paginate(10);
                   }
                  
                }
                return compact('users'); 
              }    

}
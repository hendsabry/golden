<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB;
use Excel;
use Session;
use Lang;
use Response;
use Auth;
use Exception;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Models;
use App\AdminModel;
use App\AdminMenu;
use App\AdminUserGroup;
use App\Country;
use App\City;
use App\User;
Use App\Category;
Use App\Lead;
Use App\Merchant;



class SalesRepController extends Controller {
  public function __construct(){
    parent::__construct();
// set admin Panel language
    $this->setLanguageLocaleAdmin();
  }  

              public function SalesRep() 
              {
                    if (Session::has('userid')) {
                    if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                    {
                    $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');

                    }
                    else 
                    {
                    $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                    }

                    $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail
                    if (Session::get('access_group_id') == 1) {
                      $users =  AdminModel::where('sales_manager_id', '!=', 0)->orderBy('created_at','desc')->paginate(10); 
                    }else{
                      $users =  AdminModel::where('sales_manager_id', '=', $user->adm_id)->orderBy('created_at','desc')->paginate(10); 
                    }
                    //list of sub admin
                    foreach ($users as $key => $value) {
                      if($value->code != ''){
                         $value['code_achieve'] =  Merchant::where('sales_rep_code','=',$value->code)->count();
                       }else{
                        $value['code_achieve'] = 0;
                       }
                    }
                    $groups =  AdminUserGroup::where('level','=',2)->get();
                    $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                    $group =  explode(",",$groups_1->group_permission);
                    $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                    return view('admin.sales_manager.sales_rep',compact('users','users'))->with("routemenu",$session_message)->with('user',$user)->with('groups',$groups)->with('admin_menu',$admin_menu);;
                    }else {
                    return Redirect::to('siteadmin');
                    } 
                  }   

                public function addSalesrep(Request $request) 
                {
                  if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                  {
                  $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
                  }
                  else 
                  {
                  $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                  }
                  $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();               
                  $groups =  AdminUserGroup::where('level','=',2)->get();
                  //get admin groups
                  $selected_city  = City::select('ci_con_id')->where('ci_status','=',1)->distinct()->get()->toArray();
                  $selected_country = array();
                  foreach ($selected_city as $key => $value) {
                  $selected_country[] = $value['ci_con_id'];
                  }
                  $countries = Country::whereIn('co_id',$selected_country)->where('co_status','=',1)->groupBy('co_name')->get();
                  $select_city = array();
                  $select_city =  City::where('ci_con_id','=',$countries[0]->co_id)->where('ci_status','=',1)->where('ci_status','=',1)->get();

                  if ($request->isMethod('post'))
                  {
                  $validator = Validator::make($request->all(), [
                  'adm_fname' => 'required',
                  'adm_lname' => 'required',
                  'adm_email' => 'required',
                  'adm_phone' => 'required',
                  'dob'       => 'required',
                  ]);
                  if ($validator->passes()) {
                  $input = $request->all();
                  $checkSubAdmin =  AdminModel::where('adm_email','=',$input['adm_email'])->first();
                  if (!$checkSubAdmin) {
                  if (isset($input['image'])) //image upload functioning 
                  {
                  $uploaded_thumb_rules = array(
                  'image' => 'required|mimes:png,jpg,jpeg'
                  );

                  $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

                  if ($thumbnail_validator->passes()) {

                  $uploaded_partner_thumbnail = time() . '_' . $request->file('image')->getClientOriginalName();      

                  $imageUploadSuccess =  Image::make($input['image'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);

                  if ($imageUploadSuccess) {
                  $input['image'] ='/public/assets/adimage/' . $uploaded_partner_thumbnail;
                  } else {
                  return Redirect::to('/admin/sales_rep/add')->with('message', 'Thumbnail was not uploaded')->withInput();
                  }
                  } else {
                  return Redirect::to('/admin/sales_rep/add')->withErrors($thumbnail_validator)->withInput();
                  }
                  }
                  unset($input['_token']);
                  $input['sales_manager_id'] = $user->adm_id;
                  $input['dob'] = date("Y-m-d", strtotime($input['dob']));
                  $password = substr(md5(mt_rand()), 0, 7);
                  $input['adm_password']  = Hash::make($password);   
                  $input['access_group_id']  = 6; 
                  $input['admin_type']  = 2;        
                  $sub_ad = AdminModel::create($input);
                  $sub_ad = AdminModel::where('adm_id','=',$sub_ad->id)->first();
                  $dta['code'] = 'Gc-'.$sub_ad->sales_manager_id.'-'.$sub_ad->adm_id;
                  AdminModel::where('adm_id','=',$sub_ad->adm_id)->update($dta);

                  $email = $sub_ad->adm_email;                                  
                  $baseurl = url('/') ;
                  if (isset($sub_ad->adm_fname)) {
                  $username = $sub_ad->adm_fname;
                  }else{
                  $username = '';
                  }
                  if (isset($sub_ad->gender)) {
                  if($sub_ad->gender == '0'){
                  $respect = 'Mr';
                  }else if ($sub_ad->gender == 'female') {
                  $respect = 'Ms';
                  }else{
                  $respect = '';
                  }
                  }else{
                  $respect = '';
                  }
                  $this->confirmation_user_email = $email;
                  $this->confirmation_user_name = "Golden Cage";
                  $status = '';
                  if ($sub_ad->status == 1) 
                  {                       
                  $status = 'Activated';
                  }else{                      
                  $status = 'InActivated';
                  }                           
                  $subject = 'Your Account Detail is Successfully Added ! Golden Cage';
                  Mail::send('admin.mail.sales_rep',
                  array(
                  'user_name' => $username,
                  'user_email' => $email,
                  'site_url' => $baseurl,
                  'status' => $status,
                  'respect' => $respect,
                  'password' => $password,
                  ), function($message) use($subject)
                  {
                  $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
                  });                 
                  //sales rep entry create 
                  //Session::put('success', 'Successfully added a new Sales Rep.');
                  return Redirect::to('/admin/sales_rep')->with('success', 'Successfully added a new Sales Rep.');
                  }else{
                  Session::put('warning', 'This email is already registered.');
                  return Redirect::to('/admin/sales_rep/add');
                  }
                  }else{
                  return Redirect::to('/admin/sales_rep/add')
                  ->with('message','Something went wrong please try again')
                  ->withErrors($validator)->withInput();
                  }
                  }
                  $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                  $group =  explode(",",$groups_1->group_permission);
                  $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                  return view('admin.sales_manager.add_sales_rep')->with("routemenu",$session_message)->with('groups',$groups)->with('countries',$countries)->with('select_city',$select_city)->with('admin_menu',$admin_menu);;
              }

              public function editSalesrep(Request $request,$id)
              {
                  if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                  {
                  $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
                  }
                  else 
                  {
                  $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                  }
                  $groups =  AdminUserGroup::where('level','=',2)->get();
                  $sub_admin = AdminModel::where('adm_id','=',$id)->first(); //get admin groups
                  $selected_city  = City::select('ci_con_id')->where('ci_status','=',1)->distinct()->get()->toArray();
                  $selected_country = array();
                  foreach ($selected_city as $key => $value) {
                  $selected_country[] = $value['ci_con_id'];
                  }
                  $countries = Country::whereIn('co_id',$selected_country)->where('co_status','=',1)->groupBy('co_name')->get();
                  $cities  = City::where('ci_con_id','=',$sub_admin->adm_co_id)->where('ci_status','=',1)->orderBy('ci_name')->get();

                  if ($request->isMethod('post'))
                  {
                  $validator = Validator::make($request->all(), [
                  'adm_fname' => 'required',
                  'adm_lname' => 'required',
                  'adm_email' => 'required',
                  'adm_phone' => 'required',
                  'dob'       => 'required',                
                  ]);
                  if ($validator->passes()) 
                  {
                  $input = $request->all();
                  $sub_admin = AdminModel::where('adm_id','=',$id)->first(); 
                  if (isset($input['image'])) //image upload functioning 
                  {
                  $uploaded_thumb_rules = array(
                  'image' => 'required|mimes:png,jpg,jpeg'
                  );

                  $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

                  if ($thumbnail_validator->passes()) 
                  {
                  $uploaded_partner_thumbnail = time() . '_' . $request->file('image')->getClientOriginalName(); 
                  $imageUploadSuccess =  Image::make($input['image'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);
                  if ($imageUploadSuccess) 
                  {
                  $input['image'] ='/public/assets/adimage/' . $uploaded_partner_thumbnail;
                  if (isset($sub_admin->image) && $sub_admin->image != "" && file_exists('/var/www/html'.$sub_admin->image)) 
                  {
                  unlink('/var/www/html'.$sub_admin->image);
                  }  
                  } else 
                  {
                  return Redirect::to('/admin/sales_rep/edit/'.$id)->with('message', 'Thumbnail was not uploaded')->withInput();
                  }
                  } else 
                  {
                  return Redirect::to('/admin/sales_rep/edit/'.$id)->withErrors($thumbnail_validator)->withInput();
                  }
                  }

                  unset($input['_token']);
                  $input['dob'] = date("Y-m-d", strtotime($input['dob']));
                  $sub_ad = AdminModel::where('adm_id','=',$id)->update($input); 
                  
                  $sub_ad = AdminModel::where('adm_id','=',$id)->first();
                  $email = $sub_ad->adm_email;                                  
                  $baseurl = url('/') ;
                  if (isset($sub_ad->adm_fname)) {
                  $username = $sub_ad->adm_fname;
                  }else{
                  $username = '';
                  }
                  if (isset($sub_ad->gender)) {
                  if($sub_ad->gender == '0'){
                  $respect = 'Mr';
                  }else if ($sub_ad->gender == 'female') {
                  $respect = 'Ms';
                  }else{
                  $respect = '';
                  }
                  }else{
                  $respect = '';
                  }
                  $this->confirmation_user_email = $email;
                  $this->confirmation_user_name = "Golden Cage";
                  $status = '';
                  if ($sub_ad->status == 1) 
                  {                       
                  $status = 'Activated';
                  }else{                      
                  $status = 'InActivated';
                  }                           
                  $subject = 'Your Account Detail is Successfully Updated ! Golden Cage';
                  Mail::send('admin.mail.sales_rep_update_details',
                  array(
                  'user_name' => $username,
                  'user_email' => $email,
                  'site_url' => $baseurl,
                  'status' => $status,
                  'respect' => $respect,
                  ), function($message) use($subject)
                  {
                  $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
                  }); 

                  //sub admin entry create 
                  //Session::put('success', 'Successfully updated a sales rep.');
                  $msg_success='';
                  if (Session::get('admin_lang_file') == 'admin_ar_lang') {
                    $msg_success='تم بنجاح تحديث مندوب مبيعات';
                  } else {
                    $msg_success='Successfully updated a sales rep';
                  }
                  
                  return Redirect::to('/admin/sales_rep')->with('success', $msg_success);
                  }else{

                    $msg_fail='';
                  if (Session::get('admin_lang_file') == 'admin_ar_lang') {
                    $msg_fail='حدث خطأ ما. أعد المحاولة من فضلك';
                  } else {
                    $msg_fail='Something went wrong please try again';
                  }

                  return Redirect::to('/admin/sales_rep/edit/'.$id)
                  ->with('message',$msg_fail)
                  ->withErrors($validator)->withInput();

                  }
                  }
                  $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                  $group =  explode(",",$groups_1->group_permission);
                  $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                  return view('admin.sales_manager.edit_sales_rep')->with("routemenu",$session_message)->with('groups',$groups)->with('countries',$countries)->with('sub_admin',$sub_admin)->with('cities',$cities)->with('admin_menu',$admin_menu);;
               }

                  public function viewSalesRep($id)
                  {
                    if (Session::has('userid')) 
                    {

                    if(Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') 
                    {
                    $session_message = trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT');

                    }
                    else 
                    {
                    $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT');
                    }

                    $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first(); 


                    $sub_admin =  AdminModel::where('adm_id','=',$id)->first();//get user detail 


                    $getassignedvendorlist=Merchant::where('sales_rep_code',$sub_admin->code)->get();
                   


                    $offer_plan =  AdminModel::where('adm_id', '=', $id)->with('offer')->with('plan')->get();
                   $sales_manager =  AdminModel::where('adm_id','=',$sub_admin->sales_manager_id)->first();
                                 
                    $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                    $group =  explode(",",$groups->group_permission);
                    $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                    return view('admin.sales_manager.view_sales_rep')->with("routemenu",$session_message)->with('user',$user)->with('sub_admin',$sub_admin)->with('admin_menu',$admin_menu)->with('offer_plan',$offer_plan)->with('assignedvendorlist',$getassignedvendorlist)->with('sales_manager',$sales_manager);;
                    }
                    else 
                    {
                    return Redirect::to('siteadmin');
                    } 
                  }

                  public function deleteSalesrep($id)
                  {
                    $sub_admin = AdminModel::where('adm_id','=',$id)->first(); 
                    $flag = AdminModel::where('adm_id','=',$id)->delete();//delete subadmin
                    if (isset($sub_admin->image) && $sub_admin->image != "" && file_exists('/var/www/html'.$sub_admin->image)) 
                    {
                    unlink('/var/www/html'.$sub_admin->image);
                    }  
                    return Redirect::to('/admin/sales_rep');
                  }

                  public function changeStatus(Request $request)
                  {
                    $id = $request->get('id');
                    $status = $request->get('status');
                    $dta= array();
                    $dta['status'] = $status;
                    AdminModel::where('adm_id','=',$id)->update($dta); // change status of sales rep 
                    return 1 ;
                  }

                  public function searchData(Request $request)
                  {
                    if (Session::has('userid')) 
                    {
                    if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                    {
                    $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
                    }
                    else 
                    {
                    $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                    }

                    $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail
                    $search = trim($request->get('search_keyword'));
                    if(isset($search))
                    {
                    $this->search_keyword = $search;
                    }
                    else
                    {
                    $this->search_keyword = '';
                    }
                    $status  = trim($request->get('status'));
                    if( $status != "" && $this->search_keyword != '')
                    {
                      if (Session::get('access_group_id') == 1) {
                        $users =  AdminModel::where('sales_manager_id', '!=', 0)->where('status','=',$status)->where('adm_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->paginate(10);
                    }else{
                     $users =  AdminModel::where('sales_manager_id', '=', $user->adm_id)->where('status','=',$status)->where('adm_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->paginate(10);
                    }
                    
                    }
                    else if ($status != "" && $this->search_keyword == '') 
                    {
                       if (Session::get('access_group_id') == 1) {
                              $users =  AdminModel::where('sales_manager_id', '!=', 0)->where('status','=',$status)->orderBy('created_at','desc')->paginate(10);
                    }else{
                     $users =  AdminModel::where('sales_manager_id', '=', $user->adm_id)->where('status','=',$status)->orderBy('created_at','desc')->paginate(10);
                    }
                    }
                    else if ($status == "" && $this->search_keyword != '') 
                    {
                       if (Session::get('access_group_id') == 1) {
                        $users =  AdminModel::where('sales_manager_id', '!=', 0)->where('adm_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->paginate(10);
                    }else{
                      $users =  AdminModel::where('sales_manager_id', '=', $user->adm_id)->where('adm_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->paginate(10);
                    }
                    
                    }
                    else if ($status == "" && $this->search_keyword == '') 
                    {
                       if (Session::get('access_group_id') == 1) {
                        $users =  AdminModel::where('sales_manager_id', '!=', 0)->orderBy('created_at','desc')->paginate(10);
                    }else{
                     $users =  AdminModel::where('sales_manager_id', '=', $user->adm_id)->orderBy('created_at','desc')->paginate(10);
                    }
                    
                    }
                    else
                    {
                       if (Session::get('access_group_id') == 1) {
                      $users =  AdminModel::where('sales_manager_id', '!=', 0)->orderBy('created_at','desc')->paginate(10);
                    }else{
                     $users =  AdminModel::where('sales_manager_id', '=', $user->adm_id)->orderBy('created_at','desc')->paginate(10);
                    }
                    }
                    foreach ($users as $key => $value) {
                      if ($value->code != '') {
                       $value['code_achieve'] =  Merchant::where('sales_rep_code','=',$value->code)->count();
                      }else{
                        $value['code_achieve'] = 0;
                      }
                    }
                    return compact('users'); 
                    }
                    else 
                    {
                    return Redirect::to('siteadmin');
                    } 
                  }  
                  public function searchCSVData(Request $request)
                  {
                  return (new \App\Http\Controllers\Admin\Csv\SalesExport)->forYear($request)->download('salesrep.xlsx');
                  }

}
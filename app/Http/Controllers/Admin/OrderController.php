<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB;
use Excel;
use Session;
use Lang;
use Response;
use Auth;
use Exception;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Models;
use App\AdminModel;
use App\AdminMenu;
use App\AdminUserGroup;
use App\Country;
use App\City;
use App\User;
use App\Offer;


class OrderController extends Controller {
  public function __construct(){
    parent::__construct();
// set admin Panel language
    $this->setLanguageLocaleAdmin();
  }  

               public function Orderlist(Request $request) 
                  {
                    $order_id      = $request->orderid;
                     $from_date      = $request->from_date;
                    $to_date      = $request->to_date;

                  $getPagelimit = config('app.paginate');
                    if (Session::has('userid')) {
                      if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                        {
                          $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');

                        }
                        else 
                        {
                          $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                        }
  $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail 


           if ($from_date!='' && $to_date!=''){

              $start = date("Y-m-d",strtotime($from_date));
              $end = date("Y-m-d",strtotime($to_date));        

            $order = DB::table('nm_order')->whereBetween('created_at',[$start,$end])->orderBy('occasion_date','DESC')->paginate($getPagelimit)->appends(request()->query());
           }elseif (isset($order_id) && $order_id!='') {
              $order = DB::table('nm_order')->where('order_id',$order_id)->orderBy('id','DESC')->paginate($getPagelimit)->appends(request()->query());
           }
           else{
            $order = DB::table('nm_order')->orderBy('id','DESC')->paginate($getPagelimit)->appends(request()->query());
           }
                       
              $groups =  AdminUserGroup::where('level','=',2)->get();
                        $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                        $group =  explode(",",$groups_1->group_permission);
                        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                        return view('admin.order_management.order')->with('setOrder',$order)->with("routemenu",$session_message)->with('user',$user)->with('groups',$groups)->with('admin_menu',$admin_menu);;
                      }else {
                        return Redirect::to('siteadmin');
                      } 
                }   
                



public function adminorderdetail(Request $request) 
     {
         if (Session::has('userid')) {
                      if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                        {
                          $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');

                        }
                        else 
                        {
                          $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                        }         
         $order_id      = $request->id;


                 $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->groupBy('nm_order_product.shop_id')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.showcartproduct','=',0)->where('nm_order_product.order_id',$order_id)->selectRaw('*,sum(nm_order_product.total_price) as sum')->get();  
        //dd($productdetails);  
                    $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail 
                  $groups =  AdminUserGroup::where('level','=',2)->get();
                        $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                        $group =  explode(",",$groups_1->group_permission);
                        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();


        return view('admin.order_management.admin-order-detail',compact('productdetails','order_id'))->with("routemenu",$session_message)->with('user',$user)->with('groups',$groups)->with('admin_menu',$admin_menu);  
        } else {
           return Redirect::to('siteadmin');

        }             
     }

  


 public function serviceorder($cus_id,$product_type,$product_sub_type,$order_id,$category_id,$product_id,$merchant_id,$shop_id)
   {
        if (Session::has('userid')) {
                      if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                        {
                          $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');

                        }
                        else 
                        {
                          $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                        }  



                             $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail 
                  $groups =  AdminUserGroup::where('level','=',2)->get();
                        $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                        $group =  explode(",",$groups_1->group_permission);
                        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();

                        /////////////////// order details section starts//////



      
       //$userid            = $userid;
       $product_type      = $product_type;
       $product_sub_type  = $product_sub_type;
       $order_id          = $order_id;
       $category_id       = $category_id;
       $product_id        = $product_id;
       $merchant_id       = $merchant_id;
       $shop_id           = $shop_id; 
       $lang              = Session::get('lang_file');
       //////////////////////////// Electronic Invitation Order start here///////////////
       if($product_type=='occasion' && $product_sub_type=='invitations')
       { 
        $productdetails=DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getInvitations = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($getInvitations as $value)
        {
           $moredetail = DB::table('nm_order_invitation')->where('order_id',$order_id)->first();
           $value->bookingdetail = $moredetail; 
        }
      }
      else
      {
         $getInvitations = array(); 
         
      }
       }
      //////////////////////////// Electronic Invitation Order start end here///////////////
       //////////////////////////// Car Rental order start here///////////////
       if($product_type=='car_rental' && $product_sub_type=='car')
       { 
       $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();     
      $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getOrderCarRental = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($getOrderCarRental as $value)
        {
           $moredetail = DB::table('nm_order_product_rent')->where('product_type','car_rental')->where('order_id',$order_id)->where('product_id',$value->product_id)->get();
           $value->bookingdetail = $moredetail; 
        }
   
      }
      else
      {
         $getOrderCarRental = array(); 
         
      }
    }
      //////////////////////////// Car Rental order end here///////////////

      //////////////////////////// Travel Agency order start here///////////////
       if($product_type=='travel' && $product_sub_type=='travel')
       {  
       $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
      $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getOrderTravel = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($getOrderTravel as $value)
        {
           $moredetail = DB::table('nm_order_product_rent')->where('product_type','travel')->where('order_id',$order_id)->where('product_id',$value->product_id)->first();
           $value->bookingdetail = $moredetail; 
        }
   
      }
      else
      {
         $getOrderTravel = array(); 
         
      }
       }
      //////////////////////////// Car Rental Order end here///////////////  
       
      //////////////////////////// Beauty Center Order start here///////////////
       if($product_type=='beauty' && $product_sub_type=='beauty_centers')
       {   
       $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();   
      $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getOrderBeautyCenters = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($getOrderBeautyCenters as $value)
        {
           $moredetail = DB::table('nm_order_services_staff')->where('product_type','beauty')->where('order_id',$order_id)->where('service_id',$value->product_id)->first();
           $value->bookingdetail = $moredetail; 
        }
   
      }
      else
      {
         $getOrderBeautyCenters = array(); 
         
      }
       }
      //////////////////////////// Beauty Center Order end here///////////////

      //////////////////////////// Beauty Spa Order start here///////////////
       if($product_type=='beauty' && $product_sub_type=='spa')
       {  
       $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
      $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getOrderSpa = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($getOrderSpa as $value)
        {
           $moredetail = DB::table('nm_order_services_staff')->where('product_type','beauty')->where('order_id',$order_id)->where('service_id',$value->product_id)->first();
           $value->bookingdetail = $moredetail; 
        }
   
      }
      else
      {
         $getOrderSpa = array(); 
         
      }
       }
      //////////////////////////// Beauty Spa Order end here///////////////   
      
      //////////////////////////// Barber Order start here///////////////
       if($product_type=='beauty' && $product_sub_type=='men_saloon')
       {   
       $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();   
      $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getOrderBarber = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($getOrderBarber as $value)
        {
           $moredetail = DB::table('nm_order_services_staff')->where('product_type','beauty')->where('order_id',$order_id)->where('service_id',$value->product_id)->first();
           $value->bookingdetail = $moredetail; 
        }
   
      }
      else
      {
         $getOrderBarber = array(); 
         
      }
       }
      //////////////////////////// Barber Order end here///////////////

      //////////////////////////// Makeup Artist Order start here///////////////
       if($product_type=='beauty' && $product_sub_type=='makeup_artists')
       {   
       $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();   
      $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getOrderMakeupArtist = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get(); 
        foreach($getOrderMakeupArtist as $value)
        {
           $moredetail = DB::table('nm_order_services_staff')->where('product_type','beauty')->where('order_id',$order_id)->where('service_id',$value->product_id)->first();
           $value->bookingdetail = $moredetail; 
        }
      }
      else
      {
         $getOrderMakeupArtist = array();         
      }
       }
      //////////////////////////// Makeup Artist Order end here///////////////
      
      //////////////////////////// Makeup Product Order start here///////////////
       if($product_type=='beauty' && $product_sub_type=='makeup')
       {   
       $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();   
      $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getOrderMakeup = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($getOrderMakeup as $value)
        {
           $moredetail = DB::table('nm_order_services_staff')->where('product_type','beauty')->where('order_id',$order_id)->where('service_id',$value->product_id)->first();
           $value->bookingdetail = $moredetail; 
        }
   
      }
      else
      {
         $getOrderMakeup = array(); 
         
      }
       }
      //////////////////////////// Makeup Product Order end here///////////////

      //////////////////////////// Clinic Cosmetics And Laser Order start here///////////////
       if($product_type=='clinic' && $product_sub_type=='cosmetic')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getCosmetic = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($getCosmetic as $value)
        {
           $moredetail = DB::table('nm_order_services_staff')->where('product_type','clinic')->where('order_id',$order_id)->where('service_id',$value->product_id)->first();
           $value->bookingdetail = $moredetail; 
        }
   
      }
      else
      {
         $getCosmetic = array(); 
         
      }
       }
      //////////////////////////// Clinic Cosmetics And Laser Order start end here///////////////
       //////////////////////////// Dental And Dermatology Order start here///////////////
       if($product_type=='clinic' && $product_sub_type=='skin')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getSkin = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($getSkin as $value)
        {
           $moredetail = DB::table('nm_order_services_staff')->where('product_type','clinic')->where('order_id',$order_id)->where('service_id',$value->product_id)->first();
           $value->bookingdetail = $moredetail; 
        }
   
      }
      else
      {
         $getSkin = array(); 
         
      }
       }
      //////////////////////////// Dental And Dermatology Order start end here/////////////// 


       //////////////////////////// Jewellery & Gold Order start here///////////////
       if($product_type=='shopping' && $product_sub_type=='gold')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getJewellery = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
      }
      else
      {
         $getJewellery = array(); 
         
      }
       }
      //////////////////////////// Jewellery & Gold Order start end here/////////////// 

      //////////////////////////// Perfumes Order start here///////////////
       if($product_type=='shopping' && $product_sub_type=='perfume')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
          if($countCart>0)
          {
            $lang  = Session::get('lang_file');
            $getPerfume = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
          }
          else
          {
             $getPerfume = array(); 
             
          }
       }
      //////////////////////////// Perfumes Order start end here///////////////   

      //////////////////////////// Hotal Halls Order start here///////////////
       if($product_type=='hall' && $product_sub_type=='hall')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
          if($countCart>0)
          {
            $lang  = Session::get('lang_file');
            $getHotalHalls = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
            foreach($getHotalHalls as $value)
            {
              if(isset($lang) && $lang == 'ar_lang')
              {
                $moredetail = DB::table('nm_order_internal_food_dish')->where('order_id',$order_id)->where('product_id',$value->product_id)->select('cus_id','order_id','product_id','internal_food_menu_id','internal_food_dish_id','container_id','container_title','container_image','quantity','price','internal_dish_name_ar as internal_dish_name','dish_image')->get();
                $value->bookingdetail = $moredetail;
              }
              else
              {
                $moredetail = DB::table('nm_order_internal_food_dish')->where('order_id',$order_id)->where('product_id',$value->product_id)->select('cus_id','order_id','product_id','internal_food_menu_id','internal_food_dish_id','container_id','container_title','container_image','quantity','price','internal_dish_name','dish_image')->get();
                $value->bookingdetail = $moredetail;
              }           
            }   
          }
          else
          {
             $getHotalHalls = array(); 
             
          }
       }
      //////////////////////////// Hotal Halls Order start end here/////////////// 

       //////////////////////////// Buffets Order start here///////////////
       if($product_type=='food' && $product_sub_type=='buffet')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
          if($countCart>0)
          {
            $lang  = Session::get('lang_file');
            $getBuffets = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get(); 
            foreach($getBuffets as $value)
            {
                $moredetail = DB::table('nm_order_external_food_dish')->where('external_food_dish_id',$value->product_id)->where('order_id',$order_id)->get();
                $value->bookingdetail = $moredetail;
            }          
          }
          else
          {
             $getBuffets = array(); 
             
          }
          //dd($getBuffets);
       }
      //////////////////////////// Buffets Order start end here/////////////// 

       //////////////////////////// Dates start here///////////////
       if($product_type=='food' && $product_sub_type=='dates')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
          if($countCart>0)
          {
            $lang  = Session::get('lang_file');
            $getDates = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();           
          }
          else
          {
             $getDates = array(); 
             
          }
       }
      //////////////////////////// Dates Order start end here/////////////// 

      //////////////////////////// Desserts Order here///////////////
       if($product_type=='food' && $product_sub_type=='dessert')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
          if($countCart>0)
          {
            $lang  = Session::get('lang_file');
            $getDesserts = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();           
          }
          else
          {
             $getDesserts = array(); 
             
          }
       }
      //////////////////////////// Desserts Order start end here/////////////// 
      
      //////////////////////////// Tailors Order start here///////////////
       if($product_type=='shopping' && $product_sub_type=='tailor')
       {   
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();   
          $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
          if($countCart>0)
          {
            $lang  = Session::get('lang_file');
            $getOrderTailor = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();

            foreach($getOrderTailor as $value)
            {
               $moredetail = DB::table('nm_order_body_measurement')->where('order_id',$order_id)->where('product_id',$value->product_id)->first();               
               $value->bookingdetail = $moredetail; 
            }
       
          }
          else
          {
             $getOrderTailor = array(); 
             
          }
      }
      //////////////////////////// Tailors Order end here///////////////
      
      //////////////////////////// Dresses Order start here///////////////
       if($product_type=='shopping' && $product_sub_type=='dress')
       {   
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();   
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
          if($countCart>0)
          {
            $lang  = Session::get('lang_file');
            $getOrderDress = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
            foreach($getOrderDress as $value)
            {
               $moredetail = DB::table('nm_order_product_rent')->where('order_id',$order_id)->where('product_id',$value->product_id)->where('product_type','shopping')->first();
               $value->bookingdetail = $moredetail; 
            }
       
          }
          else
          {
             $getOrderDress = array(); 
             
          }
      }
      //////////////////////////// Dresses Order end here///////////////
      //////////////////////////// Abaya Order start here///////////////
       if($product_type=='shopping' && $product_sub_type=='abaya')
       {   
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();
        
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
          if($countCart>0)
          {
            $lang  = Session::get('lang_file');
            $getOrderAbaya = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
            foreach($getOrderAbaya as $value)
            {
               $moredetail = DB::table('nm_order_body_measurement')->where('order_id',$order_id)->where('product_id',$value->product_id)->first();
               $value->bookingdetail = $moredetail; 
            }
       
          }
          else
          {
             $getOrderAbaya = array();              
          }  
      }
      //////////////////////////// Abaya Order end here///////////////
      
      //////////////////////////// Sound Sysytems Acoustic Order start here///////////////
       if($product_type=='music' && $product_sub_type=='acoustic')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang = Session::get('lang_file');
        $SoundSysytemsAcoustic = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
        foreach($SoundSysytemsAcoustic as $value)
        {
           $moredetail = DB::table('nm_order_product_rent')->where('product_type','music')->where('order_id',$order_id)->where('product_id',$value->product_id)->first();
           $value->bookingdetail = $moredetail; 
        }
          
      }
      else
      {
         $SoundSysytemsAcoustic = array(); 
         
      }
       }
      //////////////////////////// Sound Sysytems Acoustic Order start end here/////////////// 
      
      //////////////////////////// Kosha Order start here///////////////
       if($product_type=='occasion' && $product_sub_type=='cosha')
       {   
       $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();   
      $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('showcartproduct',0)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getOrderKosha = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('showcartproduct',0)->where('shop_id',$shop_id)->get();  
      }
      else
      {
         $getOrderKosha = array(); 
         
      }
       }
      //////////////////////////// Kosha Order end here///////////////
       //////////////////////////// Photography Studio Order start here///////////////
       if($product_type=='occasion' && ($product_sub_type=='photography' || $product_sub_type=='video'))
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->whereIn('product_sub_type',['photography','video'])->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $photographyStudio = DB::table('nm_order_product')->where('product_type',$product_type)->whereIn('product_sub_type',['photography','video'])->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
          
      }
      else
      {
         $photographyStudio = array(); 
         
      }
       }
      //////////////////////////// Photography Studio Order start end here/////////////// 
       //////////////////////////// Reception & Hospitality Order start here///////////////
       if($product_type=='occasion' && $product_sub_type=='hospitality')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang = Session::get('lang_file');
        $ReceptionHospitality = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
          
      }
      else
      {
         $ReceptionHospitality = array(); 
         
      }
       }
      //////////////////////////// Reception & Hospitality Order start end here/////////////// 

        //////////////////////////// Roses Order start here///////////////
       if($product_type=='occasion' && $product_sub_type=='roses')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang = Session::get('lang_file');
        $orderRoses = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
          
      }
      else
      {
         $orderRoses = array(); 
         
      }
       }
      //////////////////////////// Roses Order start end here/////////////// 

      //////////////////////////// Special Event Order start here///////////////
       if($product_type=='occasion' && $product_sub_type=='events')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang = Session::get('lang_file');
        $orderSpecialEvent = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
          
      }
      else
      {
         $orderSpecialEvent = array(); 
         
      }
       }
      //////////////////////////// Special Event Order start end here/////////////// 
       //////////////////////////// Singer Order start here///////////////
       if($product_type=='singer' && $product_sub_type=='singer')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getSinger = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
      }
      else
      {
         $getSinger = array(); 
         
      }
       }
      //////////////////////////// Singer Order start end here///////////////
       //////////////////////////// Singer Order start here///////////////
       if($product_type=='band' && $product_sub_type=='band')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getBand = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
      }
      else
      {
         $getBand = array(); 
         
      }
       }
      //////////////////////////// Singer Order start end here///////////////

       //////////////////////////// Sound Sysytem form Order start here///////////////
       if($product_type=='recording' && $product_sub_type=='recording')
       {  
         $productdetails = DB::table('nm_order')->orderBy('nm_order.order_date','DESC')->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.order_id',$order_id)->where('nm_order_product.product_sub_type',$product_sub_type)
        ->first();    
         $countCart = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->count();
      if($countCart>0)
      {
        $lang  = Session::get('lang_file');
        $getRecording = DB::table('nm_order_product')->where('product_type',$product_type)->where('product_sub_type',$product_sub_type)->where('order_id',$order_id)->where('shop_id',$shop_id)->get();
      }
      else
      {
         $getRecording = array(); 
         
      }
       }
      //////////////////////////// Sound Sysytem form Order start end here///////////////


      


                        //////////// order deatils section ends//////////


        return view('admin.order_management.serviceorder',compact('productdetails','cus_id','product_type','product_sub_type','order_id','category_id','product_id','merchant_id','shop_id','productdetails','getOrderCarRental','getOrderTravel','getOrderBeautyCenters','getOrderSpa','getCosmetic','getSkin','getOrderBarber','getOrderMakeupArtist','getOrderMakeup','getJewellery','getPerfume','getHotalHalls','getBuffets','getDates','getDesserts','getOrderTailor','getOrderDress','getOrderAbaya','getOrderKosha','photographyStudio','ReceptionHospitality','SoundSysytemsAcoustic','orderRoses','orderSpecialEvent','getSinger','getBand','getRecording','getInvitations'))->with("routemenu",$session_message)->with('user',$user)->with('groups',$groups)->with('admin_menu',$admin_menu);  
        } else {
           return Redirect::to('siteadmin');

        }   



   }







                

}
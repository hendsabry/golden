<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Excel;
use Session;
use Lang;
use Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Models;
use App\AdminMenu;
use App\AdminModel;
use App\AdminUserGroup;
use App\subscription;
use App\subscriptionData;
use App\Category;

class SubscriptionController extends Controller {
    public function __construct(){
        parent::__construct();
        // set admin Panel language
        $this->setLanguageLocaleAdmin();
    } 

        public function subscription() {
        if (Session::has('userid')) {
        if(Lang::has(Session::get('admin_lang_file').'.BACK_SUBSCRIPTION_MANAGEMENT')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_SUBSCRIPTION_MANAGEMENT');

        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUBSCRIPTION_MANAGEMENT');
        }
        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail
        $users = subscription::with('getCategory')->orderBy('created_at','desc')->paginate(10);
        //get subscription data 
        foreach ($users as $key => $value) {
            $value['created_at_new'] = $value['created_at']->format('M d, Y');
        }
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        return view('admin.subscription.list',compact('users'))->with("routemenu",$session_message)->with('user',$user)->with('admin_menu',$admin_menu);;
        }else {
        return Redirect::to('siteadmin');
        } 
    }

    public function changeStatus(Request $request)
    {
        $id = $request->get('id');
        $status = $request->get('status');
        $dta= array();
        $dta['status'] = $status;
        subscription::where('id','=',$id)->update($dta); // change status of particulare subscription 
        return 1 ;
    }

    public function searchData(Request $request)
    {
        $services_id  = trim($request->get('services_id'));
        $status  = trim($request->get('status'));
        if ($services_id != ''  && $status  != '') {
        $users = subscription::where('services_id','=',$services_id)->where('status','=',$status)->with('getCategory')->orderBy('created_at','desc')->paginate(10);
        }else if ($services_id != ''  && $status  == '') {
        $users = subscription::where('services_id','=',$services_id)->with('getCategory')->orderBy('created_at','desc')->paginate(10);
        }else if ($services_id == '' && $status  != '') {
        $users = subscription::where('status','=',$status)->with('getCategory')->orderBy('created_at','desc')->paginate(10);
        }else{
        $users = subscription::with('getCategory')->orderBy('created_at','desc')->paginate(10);
        }

        foreach ($users as $key => $value) {
        $value['created_at_new'] = $value['created_at']->format('M d, Y');
        }
        return compact('users'); 
    }

        public function add(Request $request)
        {
        if (Session::has('userid')) {
        if(Lang::has(Session::get('admin_lang_file').'.BACK_SUBSCRIPTION_MANAGEMENT')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_SUBSCRIPTION_MANAGEMENT');
        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUBSCRIPTION_MANAGEMENT');
        }
        $category = Category::select('mc_id')->where('parent_id','=',0)->get()->toArray();
        $subcategory = Category::select('mc_id')->whereIn('parent_id',$category)->get()->toArray();
        $sub_cat = array();
        $main_cat = array();
        foreach ($subcategory as $key => $value) {
           $qty = Category::where('parent_id','=',$value['mc_id'])->count();
           if($qty){
            $cate = Category::select('mc_id')->where('parent_id','=',$value['mc_id'])->get()->toArray();
                $main_cat[] = array_column($cate, 'mc_id');
           }else{
                 $sub_cat[] = $value['mc_id'];
           }
        }
         $result = array();
        foreach ($main_cat as $key => $value) { 
        if (is_array($value)) { 
        $result = array_merge($result, array_flatten($value)); 
        } 
        else { 
        $result[$key] = $value; 
        } 
        } 
         $final_cat = array_merge($result,$sub_cat);
         $main_category = Category::whereIn('mc_id',$final_cat)->get(); 
        if ($request->isMethod('post'))
        {  
        $validator = Validator::make($request->all(), [
        'cat_id' => 'required',   
        'services_id' => 'required',
        'payment' => 'required',
        ]);
        if ($validator->passes()) {
        $input = $request->all();
        unset($input['_token']);
        $checkUser =  subscription::where('cat_id','=',$input['cat_id'])->count();
        if(!$checkUser)
        {
        subscription::create($input);
        //subscription entry create 
        Session::put('msg', 'Successfully added a new subscription.');
        return Redirect::to('/admin/subscription');
        }else{
            if(Session::get('admin_lang_file') == 'admin_ar_lang'){
                Session::put('warning', 'هذه الفئة مسجلة بالفعل.');
            return Redirect::to('/admin/subscription/add');
            }else{
              Session::put('warning', 'This Category is already registered.');
            return Redirect::to('/admin/subscription/add');  
            }
            
        }

        }else{

        return Redirect::to('/admin/subscription/add')->with('message','Something went wrong please try again')->withErrors($validator)->withInput();
        }
        }
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();

        return view('admin.subscription.add')->with("routemenu",$session_message)->with('admin_menu',$admin_menu)->with('main_category',$main_category);;
        }else{
        return Redirect::to('siteadmin');
        }
    }

    public function edit(Request $request,$id)
    {
        if(Session::has('userid')) 
        {
        if(Lang::has(Session::get('admin_lang_file').'.BACK_SUBSCRIPTION_MANAGEMENT')!= '') 
        {
          $session_message = trans(Session::get('admin_lang_file').'.BACK_SUBSCRIPTION_MANAGEMENT');
        }
        else 
        {
          $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUBSCRIPTION_MANAGEMENT');
        }
        $subscri      = subscription::where('id',$id)->first();
        $category     = Category::select('mc_id')->where('parent_id','=',0)->get()->toArray();
        $subcategory  = Category::select('mc_id')->whereIn('parent_id',$category)->get()->toArray();
        $sub_cat      = array();
        $main_cat     = array();
        foreach($subcategory as $key => $value) {
           $qty = Category::where('parent_id','=',$value['mc_id'])->count();
           if($qty){
            $cate = Category::select('mc_id')->where('parent_id','=',$value['mc_id'])->get()->toArray();
                $main_cat[] = array_column($cate, 'mc_id');
           }else{
                 $sub_cat[] = $value['mc_id'];
           }
        }
        $result = array();
        foreach ($main_cat as $key => $value) { 
        if (is_array($value)) { 
        $result = array_merge($result, array_flatten($value)); 
        } 
        else { 
        $result[$key] = $value; 
        } 
        } 
         $final_cat = array_merge($result,$sub_cat);
         $main_category = Category::whereIn('mc_id',$final_cat)->get(); 
        if ($request->isMethod('post'))
        {  
        $validator = Validator::make($request->all(), [
        'services_id' => 'required',
        //'payment' => 'required',
        ]);
        if ($validator->passes()) {
        $input = $request->all();
        unset($input['_token']);
        $checkUser = subscription::where('cat_id','=',$input['cat_id'])->count();
        if(!$checkUser)
        {
            if(isset($input['services_id']) && $input['services_id']==1)
           {
             $input['payment'] = $input['paymentpercentage'];
           }
           else
           {
             $input['payment'] = $input['paymentamount'];
           }
           unset($input['paymentpercentage']);
           unset($input['paymentamount']);

        subscription::where('id','=',$id)->update($input);
        //subscription entry update 
        Session::put('msg', 'Successfully added a new subscription.');
        return Redirect::to('/admin/subscription');
        }else{
        unset($input['cat_id']);
        if(isset($input['services_id']) && $input['services_id']==1)
           {
             $input['payment'] = $input['paymentpercentage'];
           }
           else
           {
             $input['payment'] = $input['paymentamount'];
           }
           unset($input['paymentpercentage']);
           unset($input['paymentamount']);
        subscription::where('id','=',$id)->update($input);
        //subscription entry update 
        Session::put('msg', 'Successfully added a new subscription.');
        return Redirect::to('/admin/subscription');
        }
        }else{
        return Redirect::to('/admin/subscription/edit/'.$id)->with('message','Something went wrong please try again')->withErrors($validator)->withInput();
        }
        }
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();

        return view('admin.subscription.edit')->with("routemenu",$session_message)->with('admin_menu',$admin_menu)->with('subscri',$subscri)->with('main_category',$main_category);
        }else{
        return Redirect::to('siteadmin');
        }
    }

        public function view($id){
        if (Session::has('userid')) {
        if(Lang::has(Session::get('admin_lang_file').'.BACK_SUBSCRIPTION_MANAGEMENT')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_SUBSCRIPTION_MANAGEMENT');
        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUBSCRIPTION_MANAGEMENT');
        }
        $subscri = subscription::where('id','=',$id)->with('getCategory')->first();
        //dd($subscri);
        $subscriptiondata = subscriptionData::where('sub_plan_id','=',$id)->with('getMarchant')->with('getUser')->with('getPlan')->paginate(10);
        $activesubscription = subscriptionData::where('sub_plan_id','=',$id)->where('sub_status','=',1)->count();
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();

        return view('admin.subscription.view',compact('subscriptiondata','activesubscription'))->with("routemenu",$session_message)->with('admin_menu',$admin_menu)->with('subscri',$subscri);
        }else{
        return Redirect::to('siteadmin');
        }
    }

    public function changeStatusData(Request $request)
    {
        $id = $request->get('id');
        $status = $request->get('status');
        $dta= array();
        $dta['sub_status'] = $status;
        subscriptionData::where('sub_id','=',$id)->update($dta); // change status of particulare subscription 
        return 1 ;
    }

    public function pagination(Request $request){
        $id = $request->get('sub_id');
        $subscriptiondata = subscriptionData::where('sub_plan_id','=',$id)->with('getMarchant')->with('getUser')->with('getPlan')->paginate(10);
        return compact('subscriptiondata'); 
    }
    
    public function searchCSV(Request $request)
    {
        return (new \App\Http\Controllers\Admin\Csv\SubscriptionExport)->forYear($request)->download('subscription_list.xlsx');
    }
}
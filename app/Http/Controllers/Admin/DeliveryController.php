<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Excel;
use Session;
use Lang;
use Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Models;
use App\AdminModel;
use App\AdminUserGroup;
use App\Country;
use App\City;
use App\AdminMenu;
use App\Delivery;

class DeliveryController extends Controller {

    public function __construct(){
    parent::__construct();
    // set admin Panel language
    $this->setLanguageLocaleAdmin();
    }       

    public function deliveryList() {

            if (Session::has('userid')) {
            if(Lang::has(Session::get('admin_lang_file').'.BACK_DELIVERY_LIST')!= '') 
            {
            $session_message = trans(Session::get('admin_lang_file').'.BACK_DELIVERY_LIST');

            }
            else 
            {
            $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_DELIVERY_LIST');
            }

            $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail

            $users =  Delivery::orderBy('created_at','desc')->paginate(10); //list of delivery members
            $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
            $group =  explode(",",$groups_1->group_permission);
            $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
            return view('admin.delivery.list',compact('users','users'))->with("routemenu",$session_message)->with('user',$user)->with('admin_menu',$admin_menu);;
            }else {
            return Redirect::to('siteadmin');
            } 
    } 

    public function addMember(Request $request){

            if (Session::has('userid')) {
            if(Lang::has(Session::get('admin_lang_file').'.BACK_DELIVERY_LIST')!= '') 
            {
            $session_message = trans(Session::get('admin_lang_file').'.BACK_DELIVERY_LIST');
            }
            else 
            {
            $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_DELIVERY_LIST');
            }

            //get admin groups
            $selected_city  = City::select('ci_con_id')->where('ci_status','=',1)->distinct()->get()->toArray();
            $selected_country = array();
            foreach ($selected_city as $key => $value) {
            $selected_country[] = $value['ci_con_id'];
            }
            $countries = Country::whereIn('co_id',$selected_country)->where('co_status','=',1)->groupBy('co_name')->get();
            $select_city = array();
            $select_city =  City::where('ci_con_id','=',$countries[0]->co_id)->where('ci_status','=',1)->where('ci_status','=',1)->get();
            //get country
            if ($request->isMethod('post'))
            {
            $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'phone_no' => 'required',
            'dob'       => 'required',
            ]);
            if ($validator->passes()) {
            $input = $request->all();
            $checkSubAdmin =  Delivery::where('email','=',$input['email'])->first();
            if (!$checkSubAdmin) {
            if (isset($input['image'])) //image upload functioning 
            {
            $uploaded_thumb_rules = array(
            'image' => 'required|mimes:png,jpg,jpeg'
            );

            $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

            if ($thumbnail_validator->passes()) {

            $uploaded_partner_thumbnail = time() . '_' . $request->file('image')->getClientOriginalName();      

            $imageUploadSuccess =  Image::make($input['image'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);

            if ($imageUploadSuccess) {
            $input['image'] ='/public/assets/adimage/' . $uploaded_partner_thumbnail;
            } else {
            return Redirect::to('/admin/delivery/add')->with('message', 'Thumbnail was not uploaded')->withInput();
            }
            } else {
            return Redirect::to('/admin/delivery/add')->withErrors($thumbnail_validator)->withInput();
            }
            }

            unset($input['_token']);
            $input['dob'] = date("Y-m-d", strtotime($input['dob']));
            $sub_ad = Delivery::create($input); 
            //delivery entry create 
            Session::put('msg', 'Successfully added a new delivery Member.');
            return Redirect::to('/admin/delivery');
            }else{
            Session::put('warning', 'This email is already registered.');
            return Redirect::to('/admin/delivery/add');
            }

            }else{

            return Redirect::to('/admin/delivery/add')
            ->with('message','Something went wrong please try again')
            ->withErrors($validator)->withInput();

            }
            }
            $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
            $group =  explode(",",$groups_1->group_permission);
            $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
            return view('admin.delivery.add')->with("routemenu",$session_message)->with('countries',$countries)->with('select_city',$select_city)->with('admin_menu',$admin_menu);;
            }else{
            return Redirect::to('siteadmin');
            }
    }


    public function editMember(Request $request,$id){

            if (Session::has('userid')) {
            if(Lang::has(Session::get('admin_lang_file').'.BACK_DELIVERY_LIST')!= '') 
            {
            $session_message = trans(Session::get('admin_lang_file').'.BACK_DELIVERY_LIST');
            }
            else 
            {
            $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_DELIVERY_LIST');
            }

            $member = Delivery::where('id','=',$id)->first(); 
            //get admin groups
            $selected_city  = City::select('ci_con_id')->where('ci_status','=',1)->distinct()->get()->toArray();
            $selected_country = array();
            foreach ($selected_city as $key => $value) {
            $selected_country[] = $value['ci_con_id'];
            }
            $countries = Country::whereIn('co_id',$selected_country)->where('co_status','=',1)->groupBy('co_name')->get();
            $cities  = City::where('ci_con_id','=',$member->co_id)->where('ci_status','=',1)->orderBy('ci_name')->get();
            //get country
            if ($request->isMethod('post'))
            {
            $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'dob'       => 'required',
            ]);
            if ($validator->passes()) {
            $input = $request->all();
            $member = Delivery::where('id','=',$id)->first(); 
            if (isset($input['image'])) //image upload functioning 
            {
            $uploaded_thumb_rules = array(
            'image' => 'required|mimes:png,jpg,jpeg'
            );

            $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

            if ($thumbnail_validator->passes()) {

            $uploaded_partner_thumbnail = time() . '_' . $request->file('image')->getClientOriginalName();      

            $imageUploadSuccess =  Image::make($input['image'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);

            if ($imageUploadSuccess) {
            $input['image'] ='/public/assets/adimage/' . $uploaded_partner_thumbnail;
            if (isset($member->image) && $member->image != "" && file_exists('/var/www/html'.$member->image)) {
            unlink('/var/www/html'.$member->image);
            }  
            } else {
            return Redirect::to('/admin/delivery/edit/'.$id)->with('message', 'Thumbnail was not uploaded')->withInput();
            }
            } else {
            return Redirect::to('/admin/delivery/'.$id)->withErrors($thumbnail_validator)->withInput();
            }
            }

            unset($input['_token']);
            $input['dob'] = date("Y-m-d", strtotime($input['dob']));
            $sub_ad = Delivery::where('id','=',$id)->update($input); 
            //sub admin entry create 
            Session::put('msg', 'Successfully edited Member.');
            return Redirect::to('/admin/delivery');
            }else{

            return Redirect::to('/admin/delivery/edit/'.$id)
            ->with('message','Something went wrong please try again')
            ->withErrors($validator)->withInput();

            }
            }
            $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
            $group =  explode(",",$groups_1->group_permission);
            $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
            return view('admin.delivery.edit')->with("routemenu",$session_message)->with('countries',$countries)->with('member',$member)->with('cities',$cities)->with('admin_menu',$admin_menu);;
            }else{
            return Redirect::to('siteadmin');
            }
    }


    public function viewMember($id) {

            if (Session::has('userid')) {

            if(Lang::has(Session::get('admin_lang_file').'.BACK_DELIVERY_LIST')!= '') 
            {
            $session_message = trans(Session::get('admin_lang_file').'.BACK_DELIVERY_LIST');

            }
            else 
            {
            $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_DELIVERY_LIST');
            }

            $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();

            $member =  Delivery::where('id','=',$id)->with('getCity')->with('getCountry')->first();//get user detail
            $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
            $group =  explode(",",$groups->group_permission);
            $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
            return view('admin.delivery.view')->with("routemenu",$session_message)->with('user',$user)->with('member',$member)->with('admin_menu',$admin_menu);;
            }else {
            return Redirect::to('siteadmin');
            } 
    }

    public function changeStatus(Request $request)
    {
            $id = $request->get('id');
            $status = $request->get('status');
            $dta= array();
            $dta['status'] = $status;
            Delivery::where('id','=',$id)->update($dta); // change status of particulare subadmin 
            return 1 ;
    }

    public function deleteMember($id){
        $member = Delivery::where('id','=',$id)->first(); 
        $flag = Delivery::where('id','=',$id)->delete();//delete subadmin
        if (isset($member->image) && $member->image != "" && file_exists('/var/www/html'.$member->image)) {
        unlink('/var/www/html'.$member->image);
        }  
        return Redirect::to('/admin/delivery');
    }

    public function searchMember(Request $request)
    {
            $search = trim($request->get('search_keyword'));
            if(isset($search))
            {
            $this->search_keyword = $search;
            }
            else
            {
            $this->search_keyword = '';
            }        

            $status  = trim($request->get('status'));

            if( $status != "" &&  $this->search_keyword != ''){
            $users =  Delivery::where('name','LIKE','%'.$this->search_keyword .'%')->where('status','=',$status)->orderBy('created_at','desc')->paginate(10);//search list with keyword and status 
            }else if($this->search_keyword != '' && $status == "")
            { 
            $users =  Delivery::where('name','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->paginate(10);
            // search list with keyword
            }else if ($this->search_keyword == '' && $status != "" ) {
            $users =  Delivery::where('status','=',$status)->orderBy('created_at','desc')->paginate(10);//search list with status 
            }else{
            $users =  Delivery::orderBy('created_at','desc')->paginate(10);
            }
            return compact('users'); 
    }

    public function searchCSVData(Request $request)
    {
            return (new \App\Http\Controllers\Admin\Csv\DeliveryExport)->forYear($request)->download('delivery.xlsx');
    }
}
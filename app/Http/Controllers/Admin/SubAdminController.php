<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Excel;
use Session;
use Lang;
use Response;
use Auth;
use File;
use Config;
use Exception;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Models;
use App\AdminModel;
use App\AdminUserGroup;
use App\Country;
use App\City;
use App\User;
use App\AdminMenu;

class SubAdminController extends Controller {

public $confirmation_user_email;
public $confirmation_user_name;

    public function __construct(){
    parent::__construct();
    // set admin Panel language
    $this->setLanguageLocaleAdmin();
    }       

    public function SubAdmin() {

            if (Session::has('userid')) {
            if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
            {
            $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');

            }
            else 
            {
            $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
            }

            $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail

            $users =  AdminModel::where('admin_type','=',2)->where('access_group_id','!=',6)->orderBy('created_at','desc')->with('get_role')->paginate(10); //list of sub admin
          
            foreach ($users as $key => $value) {
            if ($value['last_log_in'] != null && $value['last_log_in'] !='') {
            $value['last_log_in'] = date("M d, Y", strtotime($value['last_log_in']));
            }
            }
            $groups =  AdminUserGroup::where('level','=',2)->where('id','!=',6)->get();
            $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
            $group =  explode(",",$groups_1->group_permission);
            $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
            return view('admin.subadmin.subadmin',compact('users','users'))->with("routemenu",$session_message)->with('user',$user)->with('groups',$groups)->with('admin_menu',$admin_menu);;
            }else {
            return Redirect::to('siteadmin');
            } 
    }

    public function addSubAdmin(Request $request){

    if (Session::has('userid')) {
    if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
    {
    $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
    }
    else 
    {
    $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
    }

    $groups =  AdminUserGroup::where('level','=',2)->where('id','!=',6)->get();
    //get admin groups
    $selected_city  = City::select('ci_con_id')->where('ci_status','=',1)->orderBy('ci_name', 'asc')->distinct()->get()->toArray();
    $selected_country = array();
    foreach ($selected_city as $key => $value) {
    $selected_country[] = $value['ci_con_id'];
    }
    $countries = Country::whereIn('co_id',$selected_country)->where('co_status','=',1)->orderBy('co_name', 'asc')->groupBy('co_name')->get();
    $select_city = array();
    $select_city =  City::where('ci_con_id','=',$countries[0]->co_id)->where('ci_status','=',1)->where('ci_status','=',1)->orderBy('ci_name', 'asc')->get();
    //get country
    if ($request->isMethod('post'))
    {
    $validator = Validator::make($request->all(), [
    'adm_fname' => 'required',
    'adm_lname' => 'required',
    'adm_email' => 'required',
    'adm_phone' => 'required',
    'dob'       => 'required',
    'access_group_id' => 'required',
    ]);
    if ($validator->passes()) {
    $input = $request->all();
    $checkSubAdmin =  AdminModel::where('adm_email','=',$input['adm_email'])->first();
    if (!$checkSubAdmin) {
    if (isset($input['image'])) //image upload functioning 
    {
    $uploaded_thumb_rules = array(
    'image' => 'required|mimes:png,jpg,jpeg'
    );

    $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

    if ($thumbnail_validator->passes()) {

    $uploaded_partner_thumbnail = time() . '_' . $request->file('image')->getClientOriginalName();      

    $imageUploadSuccess =  Image::make($input['image'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);

    if ($imageUploadSuccess) {
    $input['image'] ='/public/assets/adimage/' . $uploaded_partner_thumbnail;
    } else {
    return Redirect::to('/admin/subadmin/add')->with('message', 'Thumbnail was not uploaded')->withInput();
    }
    } else {
    return Redirect::to('/admin/subadmin/add')->withErrors($thumbnail_validator)->withInput();
    }
    }

    unset($input['_token']);
    $password = substr(md5(mt_rand()), 0, 8);
    $input['adm_password']  = Hash::make($password);
    $input['dob'] = date("Y-m-d", strtotime($input['dob']));
    $input['admin_type'] = 2;
    $sub_ad = AdminModel::create($input); 
    $this->sendpassword($sub_ad, $password);
    //sub admin entry create 
    Session::put('msg', 'Successfully added a new Event.');
    return Redirect::to('/admin/sub_admin');
    }else{
    Session::put('warning', 'This email is already registered.');
    return Redirect::to('/admin/subadmin/add');
    }

    }else{

    return Redirect::to('/admin/subadmin/add')
    ->with('message','Something went wrong please try again')
    ->withErrors($validator)->withInput();

    }
    }
    $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
    $group =  explode(",",$groups_1->group_permission);
    $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
    return view('admin.subadmin.add_sub_admin')->with("routemenu",$session_message)->with('groups',$groups)->with('countries',$countries)->with('select_city',$select_city)->with('admin_menu',$admin_menu);;
    }else{
    return Redirect::to('siteadmin');
    }
    }

    public function getCity(Request $request){
            $ci_con_id = $request->get('ci_con_id');
            $cities  = City::where('ci_con_id','=',$ci_con_id)->where('ci_status','=',1)->orderBy('ci_name','asc')->get();
            $input = $request->all();
            if($input['language'] == 'ar')
            {
            $CIty = '<select class="form-control selectpicker" name="adm_ci_id" required="required" data-live-search="true" >';
            $CIty .= '<option value="">--Select City--</option>';
            foreach($cities as $city){
            $CIty .= '<option value="'.$city->ci_id.'">'.$city->ci_name_ar.'</option>';
            }
            $CIty .=   '</select>';

            }else{
            $CIty = '<select class="form-control selectpicker" name="adm_ci_id" required="required" data-live-search="true" >';
            $CIty .= '<option value="">--Select City--</option>';
            foreach($cities as $city){
            $CIty .= '<option value="'.$city->ci_id.'">'.$city->ci_name.'</option>';
            }
            $CIty .=   '</select>';
            }
            echo  $CIty;
    }

    public function sendpassword($sub_ad,$password){
            $email = $sub_ad->adm_email;
            $baseurl = url('/') ;
            if (isset($sub_ad->adm_fname) && isset($sub_ad->adm_lname)) {
            $username = $sub_ad->adm_fname.' '.$sub_ad->adm_lname;
            }else{
            $username = '';
            }
            if (isset($sub_ad->gender)) {
            if($sub_ad->gender == '0'){
            $respect = 'Mr';
            }else if ($sub_ad->gender == 'female') {
            $respect = 'Ms';
            }else{
            $respect = '';
            }
            }else{
            $respect = '';
            }

            $this->confirmation_user_email = $email;
            $this->confirmation_user_name = "Golden Cage";

            $subject = 'Your Account is Successfully Created ! Golden Cage';                           

            Mail::send('admin.mail.send_password',
            array(
            'user_name' => $username,
            'user_email' => $email,
            'site_url' => $baseurl,
            'password' => $password,
            'respect' => $respect,
            ), function($message) use($subject)
            {
            $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
            });
    }

    public function changeStatus(Request $request)
    {
            $id = $request->get('id');
            $status = $request->get('status');
            $dta= array();
            $dta['status'] = $status;
            AdminModel::where('adm_id','=',$id)->update($dta); // change status of particulare subadmin 
            return 1 ;
    }


    public function searchData(Request $request)
    {
            $search = trim($request->get('search_keyword'));
            if(isset($search))
            {
            $this->search_keyword = $search;
            }
            else
            {
            $this->search_keyword = '';
            }

            $access_group_id  = trim($request->get('access_group_id'));
            if (isset($access_group_id)) {
            $access_group_id = $access_group_id;
            }else{
            $access_group_id ='';
            }
            $status  = trim($request->get('status'));
            if (isset($status)) {
            $status = $status;
            }else{
            $status = '';
            }

            if( $status != '' &&  $access_group_id != '' && $this->search_keyword != ''){
            $users =  AdminModel::where('admin_type','=',2)->where('access_group_id','!=',6)->where('status','=',$status)->where('access_group_id','=',$access_group_id)->where('adm_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->with('get_role')->paginate(10);
            }else if ($status != '' &&  $access_group_id != '' && $this->search_keyword == '') {
            $users =  AdminModel::where('admin_type','=',2)->where('access_group_id','!=',6)->where('status','=',$status)->where('access_group_id','=',$access_group_id)->orderBy('created_at','desc')->with('get_role')->paginate(10);
            } else if ($status != '' &&  $access_group_id == '' && $this->search_keyword != '') {
            $users =  AdminModel::where('admin_type','=',2)->where('access_group_id','!=',6)->where('status','=',$status)->where('adm_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->with('get_role')->paginate(10);
            } else if ($status != '' &&  $access_group_id == '' && $this->search_keyword == '') {
            $users =  AdminModel::where('admin_type','=',2)->where('access_group_id','!=',6)->where('status','=',$status)->orderBy('created_at','desc')->with('get_role')->paginate(10);
            }else if ($status == '' &&  $access_group_id != '' && $this->search_keyword != '') {
            $users =  AdminModel::where('admin_type','=',2)->where('access_group_id','!=',6)->where('access_group_id','=',$access_group_id)->where('adm_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->with('get_role')->paginate(10);
            }else if ($status == '' &&  $access_group_id != '' && $this->search_keyword == '') {
            $users =  AdminModel::where('admin_type','=',2)->where('access_group_id','!=',6)->where('access_group_id','=',$access_group_id)->orderBy('created_at','desc')->with('get_role')->paginate(10);
            }else if ($status == '' &&  $access_group_id == '' && $this->search_keyword != '') {
            $users =  AdminModel::where('admin_type','=',2)->where('access_group_id','!=',6)->where('adm_fname','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->with('get_role')->paginate(10);
            }else{
            $users =  AdminModel::where('admin_type','=',2)->where('access_group_id','!=',6)->with('get_role')->orderBy('created_at','desc')->paginate(10);
            }
            foreach ($users as $key => $value) {
            if ($value['last_log_in'] != null) {
            $value['last_log_in'] = date("M d, Y", strtotime($value['last_log_in']));
            }
            }

            return compact('users'); 
    }

    public function searchCSVData(Request $request)
    {
        return (new \App\Http\Controllers\Admin\Csv\SubAdminExport)->forYear($request)->download('subadmin.xlsx');
    }

    public function deleteUser($id){
    $sub_admin = AdminModel::where('adm_id','=',$id)->first(); 
    $flag = AdminModel::where('adm_id','=',$id)->delete();//delete subadmin
    if (isset($sub_admin->image) && $sub_admin->image != "" && file_exists('/var/www/html'.$sub_admin->image)) {
    unlink('/var/www/html'.$sub_admin->image);
    }  
    return Redirect::to('/admin/sub_admin');
    }

    public function viewUser($id) {

            if (Session::has('userid')) {

            if(Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') 
            {
            $session_message = trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT');

            }
            else 
            {
            $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT');
            }

            $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();

            $sub_admin =  AdminModel::where('adm_id','=',$id)->where('admin_type','=',2)->with('get_role')->with('getCity')->with('getCountry')->first();//get user detail
            $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
            $group =  explode(",",$groups->group_permission);
            $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
            return view('admin.subadmin.sub_admin_view')->with("routemenu",$session_message)->with('user',$user)->with('sub_admin',$sub_admin)->with('admin_menu',$admin_menu);;
            }else {
            return Redirect::to('siteadmin');
            } 
    }

    public function editSubAdmin(Request $request,$id){
            if (Session::has('userid')) {
            if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
            {
            $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
            }
            else 
            {
            $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
            }

            $groups =  AdminUserGroup::where('level','=',2)->where('id','!=',6)->get();
            $sub_admin = AdminModel::where('adm_id','=',$id)->first(); 
            //get admin groups
            $selected_city  = City::select('ci_con_id')->where('ci_status','=',1)->orderBy('ci_name', 'asc')->distinct()->get()->toArray();
            $selected_country = array();
            foreach ($selected_city as $key => $value) {
            $selected_country[] = $value['ci_con_id'];
            }
            $countries = Country::whereIn('co_id',$selected_country)->where('co_status','=',1)->orderBy('co_name', 'asc')->groupBy('co_name')->get();
            $cities  = City::where('ci_con_id','=',$sub_admin->adm_co_id)->where('ci_status','=',1)->orderBy('ci_name','asc')->get();
            //get country
            if ($request->isMethod('post'))
            {
            $validator = Validator::make($request->all(), [
            'adm_fname' => 'required',
            'adm_lname' => 'required',
            'adm_email' => 'required',
            'adm_phone' => 'required',
            'dob'       => 'required',
            'access_group_id' => 'required',
            ]);
            if ($validator->passes()) {
            $input = $request->all();
            $sub_admin = AdminModel::where('adm_id','=',$id)->first(); 
            if (isset($input['image'])) //image upload functioning 
            {
            $uploaded_thumb_rules = array(
            'image' => 'required|mimes:png,jpg,jpeg'
            );

            $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

            if ($thumbnail_validator->passes()) {

            $uploaded_partner_thumbnail = time() . '_' . $request->file('image')->getClientOriginalName();      

            $imageUploadSuccess =  Image::make($input['image'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);

            if ($imageUploadSuccess) {
            $input['image'] ='/public/assets/adimage/' . $uploaded_partner_thumbnail;
            if (isset($sub_admin->image) && $sub_admin->image != "" && file_exists('/var/www/html'.$sub_admin->image)) {
            unlink('/var/www/html'.$sub_admin->image);
            }  
            } else {
            return Redirect::to('/admin/subadmin/edit/'.$id)->with('message', 'Thumbnail was not uploaded')->withInput();
            }
            } else {
            return Redirect::to('/admin/subadmin/edit/'.$id)->withErrors($thumbnail_validator)->withInput();
            }
            }

            unset($input['_token']);
            $input['dob'] = date("Y-m-d", strtotime($input['dob']));


            if($input['upassword']=='' && $input['cpassword']==''){
             unset($input['upassword']);
             unset($input['cpassword']);
        }else{
           $input['adm_password'] = bcrypt($request->input('upassword')); 
           unset($input['upassword']); 
            unset($input['cpassword']);
        } 
            unset($input['submit']);



            
            $sub_ad = AdminModel::where('adm_id','=',$id)->update($input); 
            //sub admin entry create 
            Session::put('msg', 'Successfully added a new Event.');
            return Redirect::to('/admin/sub_admin');
            }else{

            return Redirect::to('/admin/subadmin/edit/'.$id)
            ->with('message','Something went wrong please try again')
            ->withErrors($validator)->withInput();

            }
            }
            $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
            $group =  explode(",",$groups_1->group_permission);
            $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
            return view('admin.subadmin.edit_sub_admin')->with("routemenu",$session_message)->with('groups',$groups)->with('countries',$countries)->with('sub_admin',$sub_admin)->with('cities',$cities)->with('admin_menu',$admin_menu);;
            }else{
            return Redirect::to('siteadmin');
            }
    }
    public function fogetPassword(Request $request){
            $adm_id = base64_decode($request->input('id'));
            $adm_email = base64_decode($request->input('e_i'));
            $request->session()->forget('login_error');
            $request->session()->forget('login_success');
            if ($request->isMethod('post'))
            {
            $validator = Validator::make($request->all(), [
            'adm_email' => 'required',
            'adm_password_confirm' => 'required',
            ]);
            if ($validator->passes()) {
            $input = $request->all();
            if($input['adm_password_confirm'] == $input['adm_password']){
            $user =  AdminModel::where('adm_id','=',$input['adm_id'])->where('adm_email','=',$input['adm_email'])->first();
            if(isset($user)){
            if($user->forgotpassword_flag == 1){
            $data = array();
            $data['adm_password']  = Hash::make($input['adm_password']); 
            $data['forgotpassword_flag'] = 0;
            AdminModel::where('adm_id','=',$input['adm_id'])->where('adm_email','=',$input['adm_email'])->update($data); 
            if(Lang::has(Session::get('admin_lang_file').'.BACK_PASSWORD_SUCCESSFULLY_UPDATE')!= '') 
            { 
            $session_message = trans(Session::get('admin_lang_file').'.BACK_PASSWORD_SUCCESSFULLY_UPDATE');
            }  
            else 
            { 
            $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_PASSWORD_SUCCESSFULLY_UPDATE');
            }
            Session::put('login_success', $session_message);
            return Redirect::to('/siteadmin');
            }else{

            if(Lang::has(Session::get('admin_lang_file').'.BACK_PASSWORD_LINK_EXP')!= '') 
            { 
            $session_message = trans(Session::get('admin_lang_file').'.BACK_PASSWORD_LINK_EXP');
            }  
            else 
            { 
            $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_PASSWORD_LINK_EXP');
            }
            Session::put('login_error', $session_message);
            return view('admin.dashboard.reset_password')->with('adm_id',$input['adm_id'])->with('adm_email',$input['adm_email']);
            }

            }else{
            if(Lang::has(Session::get('admin_lang_file').'.BACK_USER_NOT_EXIT')!= '') 
            { 
            $session_message = trans(Session::get('admin_lang_file').'.BACK_USER_NOT_EXIT');
            }  
            else 
            { 
            $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER_NOT_EXIT');
            }
            Session::put('login_error', $session_message);
            return view('admin.dashboard.reset_password')->with('adm_id',$input['adm_id'])->with('adm_email',$input['adm_email']);
            }

            }else{
            if(Lang::has(Session::get('admin_lang_file').'.BACK_CONFIRM_PASSWORD_NOT_MATCH')!= '') 
            { 
            $session_message = trans(Session::get('admin_lang_file').'.BACK_CONFIRM_PASSWORD_NOT_MATCH');
            }  
            else 
            { 
            $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_CONFIRM_PASSWORD_NOT_MATCH');
            }
            Session::put('login_error', $session_message);
            return view('admin.dashboard.reset_password')->with('adm_id',$input['adm_id'])->with('adm_email',$input['adm_email']);
            }
            }
            }
            return view('admin.dashboard.reset_password')->with('adm_id',$adm_id)->with('adm_email',$adm_email);   
    }


}
<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Excel;
use Session;
use Lang;
use Response;
use Auth;
use Exception;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Models;
use App\AdminModel;
use App\AdminMenu;
use App\AdminUserGroup;

class AccessLevelManagementController  extends Controller {
    public function __construct(){
        parent::__construct();
        // set admin Panel language
        $this->setLanguageLocaleAdmin();
    }       

    public function index() {

        if (Session::has('userid')) {

        if(Lang::has(Session::get('admin_lang_file').'.BACK_ACCESS_LEVEL_MANAGEMENT')!= '') 
        {
            $session_message = trans(Session::get('admin_lang_file').'.BACK_ACCESS_LEVEL_MANAGEMENT');

        }
        else 
        {
            $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_ACCESS_LEVEL_MANAGEMENT');
        }

        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail

        $users = AdminUserGroup::where('level','!=',1)->orderBy('created_at','desc')->paginate(10);
        //get admin role 
        foreach ($users as $key => $value) {
            $users[$key]['total_user'] = AdminModel::where('access_group_id','=',$value->id)->count();
        }
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        return view('admin.access_level.access_level_list',compact('users','users'))->with("routemenu",$session_message)->with('user',$user)->with('admin_menu',$admin_menu);;
        }else {
            return Redirect::to('siteadmin');
        } 
    }
    public function changeStatus(Request $request)
    {
        $id = $request->get('id');
        $status = $request->get('status');
        $dta= array();
        $dta['status'] = $status;
        AdminUserGroup::where('id','=',$id)->update($dta); // change status of particulare user 
        return 1 ;
    }
    public function searchData(Request $request)
    {
        $search = trim($request->get('search_keyword'));
        if(isset($search))
        {
            $this->search_keyword = $search;
        }
        else
        {
            $this->search_keyword = '';
        }        

        $status  = trim($request->get('status'));

        if( $status != "" &&  $this->search_keyword != ''){
            $users =  AdminUserGroup::where('group_name','LIKE','%'.$this->search_keyword .'%')->where('status','=',$status)->where('level','=',2)->orderBy('created_at','desc')->paginate(10);//search list with keyword and status 
            foreach ($users as $key => $value) {
                $users[$key]['total_user'] = AdminModel::where('access_group_id','=',$value->id)->count();
            }
        }else if($this->search_keyword != '' && $status == "")
        { 
            $users =  AdminUserGroup::orwhere('group_name','LIKE','%'.$this->search_keyword .'%')->where('level','=',2)->orderBy('created_at','desc')->paginate(10);
            // search list with keyword
            foreach ($users as $key => $value) {
            $users[$key]['total_user'] = AdminModel::where('access_group_id','=',$value->id)->count();
            }
        }else if ($this->search_keyword == '' && $status != "" ) {
            $users =  AdminUserGroup::where('status','=',$status)->where('level','=',2)->orderBy('created_at','desc')->paginate(10);//search list with status 
            foreach ($users as $key => $value) {
            $users[$key]['total_user'] = AdminModel::where('access_group_id','=',$value->id)->count();
            }
        }else{
            $users =  AdminUserGroup::where('level','=',2)->orderBy('created_at','desc')->paginate(10);
            foreach ($users as $key => $value) {
            $users[$key]['total_user'] = AdminModel::where('access_group_id','=',$value->id)->count();
            }
        }
        return compact('users'); 
    }

    public function deleteRole($id){
        AdminUserGroup::where('id','=',$id)->delete();//delete role
        return Redirect::to('/admin/access_level_management');
    }

    public function addRole(Request $request){
        if (Session::has('userid')) {

        if(Lang::has(Session::get('admin_lang_file').'.BACK_ACCESS_LEVEL_MANAGEMENT')!= '') 
        {
            $session_message = trans(Session::get('admin_lang_file').'.BACK_ACCESS_LEVEL_MANAGEMENT');
        }
        else 
        {
            $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_ACCESS_LEVEL_MANAGEMENT');
        }
        $menus = AdminMenu::where('status','=',1)->get();
        //get menu 
        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
        if ($request->isMethod('post'))
        {  
            $validator = Validator::make($request->all(), [
            'group_name' => 'required',
            'group_permission' => 'required',
            ]);
            if ($validator->passes()) {
                $input = $request->all();
                unset($input['_token']);
                $input['level'] = 2;
                $input['group_permission'] = implode(',', $input['group_permission']);
                AdminUserGroup::create($input);
                // create new entry in admin user group                    
                Session::put('msg', 'Successfully added a new role.');
                return Redirect::to('/admin/access_level_management');
            }else{

                return Redirect::to('/admin/access_level/add')
                ->with('message','Something went wrong please try again')
                ->withErrors($validator)->withInput();

            }
        }
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        return view('admin.access_level.add_role')->with("routemenu",$session_message)->with('menus',$menus)->with('user',$user)->with('admin_menu',$admin_menu);;
        }else{
            return Redirect::to('siteadmin');
        }
    }  

    public function editRole(Request $request , $id){
        if (Session::has('userid')) {
            if(Lang::has(Session::get('admin_lang_file').'.BACK_ACCESS_LEVEL_MANAGEMENT')!= '') 
            {
            $session_message = trans(Session::get('admin_lang_file').'.BACK_ACCESS_LEVEL_MANAGEMENT');
            }
            else 
            {
            $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_ACCESS_LEVEL_MANAGEMENT');
            }
            $menus = AdminMenu::where('status','=',1)->get();
            //get menu 
            $usergroup = AdminUserGroup::where('id','=',$id)->first();
            $selected_menu = explode(',',  $usergroup->group_permission);
            // get user group
            $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
            if ($request->isMethod('post'))
            {  
            $validator = Validator::make($request->all(), [
            'group_name' => 'required',
            'group_permission' => 'required',
            ]);
            if ($validator->passes()) {
            $input = $request->all();
            unset($input['_token']);
            $input['level'] = 2;
            $input['group_permission'] = implode(',', $input['group_permission']);
            AdminUserGroup::where('id','=',$id)->update($input);
            // create new entry in admin user group                    
            Session::put('msg', 'Successfully added a new role.');
            return Redirect::to('/admin/access_level_management');
            }else{

            return Redirect::to('/admin/access_level/add')
            ->with('message','Something went wrong please try again')
            ->withErrors($validator)->withInput();

            }
            }
            $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
            $group =  explode(",",$groups->group_permission);
            $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
            return view('admin.access_level.edit_role')->with("routemenu",$session_message)->with('menus',$menus)->with('usergroup',$usergroup)->with('selected_menu',$selected_menu)->with('user',$user)->with('admin_menu',$admin_menu);;
        }else{
            return Redirect::to('siteadmin');
        }
    }

    public function viewRole($id) {

    if (Session::has('userid')) {

            if(Lang::has(Session::get('admin_lang_file').'.BACK_ACCESS_LEVEL_MANAGEMENT')!= '') 
            {
            $session_message = trans(Session::get('admin_lang_file').'.BACK_ACCESS_LEVEL_MANAGEMENT');

            }
            else 
            {
            $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_ACCESS_LEVEL_MANAGEMENT');
            }

            $usergroup =  AdminUserGroup::where('id','=',$id)->first();//get group detail
            $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();

            $selected_menu = explode(',',  $usergroup->group_permission);
            $menus = AdminMenu::whereIn('id',$selected_menu)->where('status','=',1)->get();
            //get selected menus 
            $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
            $group =  explode(",",$groups->group_permission);
            $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
            return view('admin.access_level.role_view')->with("routemenu",$session_message)->with('user',$user)->with('usergroup',$usergroup)->with('menus',$menus)->with('admin_menu',$admin_menu);;
        }else {
            return Redirect::to('siteadmin');
        } 
    }
}
<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB;
use Excel;
use Session;
use Lang;
use Response;
use Auth;
use Exception;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Models;
use App\AdminModel;
use App\Merchant;
use App\AdminMenu;
use App\AdminUserGroup;
use App\Country;
use App\City;
use App\User;
use App\WalletModel;


class WalletController extends Controller {
    public function __construct(){
      parent::__construct();
      // set admin Panel language
      $this->setLanguageLocaleAdmin();
    }  

              public function Walletlist() 
              {

                if (Session::has('userid')) {
                  if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                    {
                      $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
                    }
                    else 
                    {
                      $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                    }
                      $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail

                      $marchant = Merchant::orderBy('created_at','desc')->paginate(10); //list of sub admin 
                      foreach ($marchant as $key => $value) {
                      $value['created'] = date("M d, Y", strtotime($value['created_at']));
                      }                       
                      $users = User::orderBy('created_at','desc')->paginate(10);
                      foreach ($users as $key => $value) {
                      $value['created'] = date("M d, Y", strtotime($value['created_at']));
                      } 
                      //dd($users);
                      $groups =  AdminUserGroup::where('level','=',2)->get();
                      $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                      $group =  explode(",",$groups_1->group_permission);
                      $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
            return view('admin.wallet.wallet',compact('marchant','users'))->with("routemenu",$session_message)->with('user',$user)->with('groups',$groups)->with('admin_menu',$admin_menu);
          }else {
            return Redirect::to('siteadmin');
          } 
        }  
        public function vendorPaginate() 
        {        
          $marchant = Merchant::orderBy('created_at','desc')->paginate(10);
          return compact('marchant');
        } 
        public function usersPaginate() 
        {        
          $users = User::orderBy('created_at','desc')->paginate(10);
          return compact('users');
        }



        public function deduct($id){
          if (Session::has('userid')){
        if(Lang::has(Session::get('admin_lang_file').'.BACK_HALL_INSURANCE')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_HALL_INSURANCE');

        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_HALL_INSURANCE');
        }
        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
         $marchant = Merchant::where('mer_id','=',$id)->first();
         $wallets =  WalletModel::where('vendor_id','=',$id)->with('getVendor')->get();
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        return view('admin.wallet.view')->with("routemenu",$session_message)->with('user',$user)->with('admin_menu',$admin_menu)->with('marchant',$marchant)->with('wallets',$wallets);
        }else {
        return Redirect::to('siteadmin');
        } 
        }







        public function transaction($id){
          if (Session::has('userid')){
        if(Lang::has(Session::get('admin_lang_file').'.BACK_HALL_INSURANCE')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_HALL_INSURANCE');

        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_HALL_INSURANCE');
        }
        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
         $marchant = Merchant::where('mer_id','=',$id)->first();
         $wallets =  WalletModel::where('vendor_id','=',$id)->where('status','=',2)->with('getVendor')->orderBy('id','DESC')->get(); 
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        return view('admin.wallet.transaction')->with("routemenu",$session_message)->with('user',$user)->with('admin_menu',$admin_menu)->with('marchant',$marchant)->with('wallets',$wallets);
        }else {
        return Redirect::to('siteadmin');
        } 
        }

        public function deduct_wallet_money(Request $request){
          $input = $request->all();
          if ($input['amount'] > 0) {
           $marchant = Merchant::where('mer_id','=',$input['merchant_id'])->first();
            $dta['vendor_id'] = $input['merchant_id'];
            $dta['amount'] = $input['amount'];

            $dta['notes'] = $input['notes'];
            $dta['transction_id'] = $input['transaction_id'];
            $dta['transaction_type'] = 2;
            $dta['status'] = 2;
            WalletModel::create($dta);
            $wallet_money =  $marchant->wallet - $input['amount'];
            $dta_2['wallet'] = $wallet_money;
            Merchant::where('mer_id','=',$input['merchant_id'])->update($dta_2);
              return Redirect::to('/admin/payment/deduct/'.$input['merchant_id']);
          }else{
            return Redirect::to('/admin/payment/deduct/'.$input['merchant_id']);
          }
          
        }
          public function searchData(Request $request)
          {
            $created_at = trim($request->get('created_at'));          
          if($created_at != '')
          {
          $created_at = date("Y-m-d", strtotime($created_at));
          }else{
          $created_at = '';
          } 

          if( $created_at != ''){
          $marchant = Merchant::where('created_at','like',$created_at.'%')->orderBy('created_at','desc')->paginate(10); 
          }
          else{
          $marchant = Merchant::orderBy('created_at','desc')->paginate(10);        
          }
          foreach ($marchant as $key => $value) {
          $value['created'] = $value['created_at']->format('M d, Y');
          }
          return compact('marchant'); 
          }
      }
<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB;
use Excel;
use Session;
use Lang;
use Response;
use Auth;
use Exception;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Models;
use App\AdminModel;
use App\AdminMenu;
use App\AdminUserGroup;
use App\Country;
use App\City;
use App\User;
use App\Plan;


class PlanController extends Controller {
  public function __construct(){
    parent::__construct();
// set admin Panel language
    $this->setLanguageLocaleAdmin();
  }  

               public function Planlist() 
                  {

                    if (Session::has('userid')) {
                      if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                        {
                          $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');

                        }
                        else 
                        {
                          $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                        }



                        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail
                        if (Session::get('access_group_id') == 1) {
                          $users =  Plan::orderBy('created_at','desc')->with('getManager')->with('getSalesRep')->paginate(10);
                        }elseif (Session::get('access_group_id') ==4) {
                          $users =  Plan::where('manager_id','=',Session::get('userid'))->orderBy('created_at','desc')->with('getManager')->with('getSalesRep')->paginate(10);
                        }elseif (Session::get('access_group_id') == 6) {
                            $users =  Plan::where('vendor_list','=',Session::get('userid'))->orderBy('created_at','desc')->with('getManager')->with('getSalesRep')->paginate(10);
                        }else{
                          $users =  Plan::orderBy('created_at','desc')->with('getManager')->with('getSalesRep')->paginate(10);
                        }

                         //list of Plan  
                        foreach ($users as $key => $value) 
                        {
                            $value['updated'] = date("M d, Y", strtotime($value['created_at']));
                         }                   
                        $groups =  AdminUserGroup::where('level','=',2)->get();
                        $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                        $group =  explode(",",$groups_1->group_permission);
                        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                        return view('admin.plan.plan',compact('users','users'))->with("routemenu",$session_message)->with('user',$user)->with('groups',$groups)->with('admin_menu',$admin_menu);;
                      }else {
                        return Redirect::to('siteadmin');
                      } 
                }   
                

                public function changeStatus(Request $request)
                {
                    $id = $request->get('id');
                    $status = $request->get('status');                    
                    $dta= array();
                    $dta['status'] = $status;
                    Plan::where('id','=',$id)->update($dta); // change status of sales rep 
                    return 1 ;
                }
         
                public function addPlan(Request $request) 
                {
                  if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                    {
                      $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
                    }
                    else 
                    {
                      $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                    }
                    $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();                    
                    $vendor =  AdminModel::where('sales_manager_id', '=', $user->adm_id)->orderBy('created_at','desc')->paginate(10);  
                    $groups =  AdminUserGroup::where('level','=',2)->get();
                    //get admin groups
                    $selected_city  = City::select('ci_con_id')->where('ci_status','=',1)->distinct()->get()->toArray();
                    $selected_country = array();
                    foreach ($selected_city as $key => $value) {
                      $selected_country[] = $value['ci_con_id'];
                    }
                    $countries = Country::whereIn('co_id',$selected_country)->where('co_status','=',1)->groupBy('co_name')->get();
                    $select_city = array();
                    $select_city =  City::where('ci_con_id','=',$countries[0]->co_id)->where('ci_status','=',1)->where('ci_status','=',1)->get();
                    //get country
                    if ($request->isMethod('post'))
                    {
                      $validator = Validator::make($request->all(), [
                        'plan_name'    => 'required',
                        'plan_desc' => 'required',                        
                         ]);
                      if ($validator->passes()) {
                        $input = $request->all();                                  
                      unset($input['_token']);     
                      $input['manager_id'] = Session::get('userid');          
                      $Plan = Plan::Create($input); 
                       Session::put('msg', 'Successfully added a new plan.');
                       return Redirect::to('/admin/plan');
                      }else{
                        return Redirect::to('/admin/plan/add')
                        ->with('message','Something went wrong please try again')
                        ->withErrors($validator)->withInput();
                      }
                      }
                   
                      $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                      $group =  explode(",",$groups_1->group_permission);
                      $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                      return view('admin.plan.add_plan')->with("routemenu",$session_message)->with('groups',$groups)->with('countries',$countries)->with('select_city',$select_city)->with('admin_menu',$admin_menu)->with('vendor' ,$vendor)->with('user' ,$user);              
              }

                public function editPlan(Request $request,$id)
                {
                  if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                    {
                      $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
                    }
                    else 
                    {
                      $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                    }
                     $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first(); 
                    $groups =  AdminUserGroup::where('level','=',2)->get();
                    $users = Plan::where('id','=',$id)->first(); 
                    $vendor =  AdminModel::where('sales_manager_id', '=', $user->adm_id)->orderBy('created_at','desc')->paginate(10); 
                    if ($request->isMethod('post'))
                    {
                          $validator = Validator::make($request->all(), [
                           'plan_name'    => 'required',
                           'plan_desc' => 'required',              
                          ]);
                          if ($validator->passes()) 
                          {
                              $input = $request->all();
                              unset($input['_token']);                              
                              $sub_ad = Plan::where('id','=',$id)->update($input); 
                              //sub admin entry create 
                              Session::put('msg', 'Successfully added a new Event.');
                              return Redirect::to('/admin/plan');
                            }
                            else
                            {
                              return Redirect::to('/admin/plan/edit/'.$id)
                              ->with('message','Something went wrong please try again')
                              ->withErrors($validator)->withInput();
                            }
                      }
                  $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                  $group =  explode(",",$groups_1->group_permission);
                  $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                  return view('admin.plan.edit_plan')->with("routemenu",$session_message)->with('groups',$groups)->with('users',$users)->with('admin_menu',$admin_menu)->with('vendor',$vendor);;
                }

              public function viewPlan($id)
                {

                  if (Session::has('userid')) 
                  {

                    if(Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') 
                      {
                        $session_message = trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT');

                      }
                      else 
                      {
                        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT');
                      }

                      $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first(); 
                      $sub_admin =  Plan::where('id','=',$id)->with('getManager')->with('getSalesRep')->first();//get user detail 
                      if($sub_admin->status=='1')
                      {
                        if (Session::get('admin_lang_file') == 'admin_ar_lang') {
                           $sub_admin['status'] = 'نشيط';
                        } else {
                           $sub_admin['status'] = 'Active';
                        }
                      } 
                      else 
                      {
                        if (Session::get('admin_lang_file') == 'admin_ar_lang') {
                            $sub_admin['status'] = 'غير نشط' ;
                        } else {
                            $sub_admin['status'] = 'InActive' ;
                        }
                      }                                    
                      $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                      $group =  explode(",",$groups->group_permission);
                      $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                      return view('admin.plan.view_plan')->with("routemenu",$session_message)->with('user',$user)->with('sub_admin',$sub_admin)->with('admin_menu',$admin_menu);;
                   }
                    else 
                    {
                      return Redirect::to('siteadmin');
                    } 
                }

                public function deletePlan($id)
                {
                      $sub_admin = Plan::where('id','=',$id)->first(); 
                    $flag = Plan::where('id','=',$id)->delete();//delete lead
                    return Redirect::to('/admin/plan');
                }   

                public function searchData(Request $request)
                {
                  $search = trim($request->get('search_keyword'));
                  if(isset($search))
                  {
                    $this->search_keyword = $search;
                  }
                  else
                  {
                    $this->search_keyword = '';
                  }   
                  $status  = trim($request->get('status'));

                  if( $status != "" &&  $this->search_keyword != ''){
                    if (Session::get('access_group_id') ==1) {
                      $users =  Plan::where('Plan_name','LIKE','%'.$this->search_keyword .'%')->where('status','=',$status)->with('getManager')->with('getSalesRep')->orderBy('created_at','desc')->paginate(10);
                    }elseif (Session::get('access_group_id') ==4) {
                     $users =  Plan::where('manager_id','=',Session::get('userid'))->where('Plan_name','LIKE','%'.$this->search_keyword .'%')->where('status','=',$status)->with('getManager')->with('getSalesRep')->orderBy('created_at','desc')->paginate(10);
                    }elseif (Session::get('access_group_id') ==6) {
                     $users =  Plan::where('vendor_list','=',Session::get('userid'))->where('Plan_name','LIKE','%'.$this->search_keyword .'%')->where('status','=',$status)->with('getManager')->with('getSalesRep')->orderBy('created_at','desc')->paginate(10);
                    }else{
                      $users =  Plan::where('Plan_name','LIKE','%'.$this->search_keyword .'%')->where('status','=',$status)->with('getManager')->with('getSalesRep')->orderBy('created_at','desc')->paginate(10);
                    }
                  //search list with keyword and status 
                }else if($this->search_keyword != '' && $status == "")
                { 
                  if (Session::get('access_group_id') ==1) {
                        $users =  Plan::where('Plan_name','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->with('getManager')->with('getSalesRep')->paginate(10);
                    }elseif (Session::get('access_group_id') ==4) {
                      $users =  Plan::where('manager_id','=',Session::get('userid'))->where('Plan_name','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->with('getManager')->with('getSalesRep')->paginate(10);
                    }elseif (Session::get('access_group_id') ==6) {
                      $users =  Plan::where('vendor_list','=',Session::get('userid'))->where('Plan_name','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->with('getManager')->with('getSalesRep')->paginate(10);
                    }else{
                      $users =  Plan::where('Plan_name','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->with('getManager')->with('getSalesRep')->paginate(10);
                    }
                  // search list with keyword
                }else if ($this->search_keyword == '' && $status != "" ) {
                  if (Session::get('access_group_id') ==1) {
                       $users =  Plan::where('status','=',$status)->with('getManager')->with('getSalesRep')->orderBy('created_at','desc')->paginate(10);
                    }elseif (Session::get('access_group_id') ==4) {
                      $users =  Plan::where('manager_id','=',Session::get('userid'))->where('status','=',$status)->with('getManager')->with('getSalesRep')->orderBy('created_at','desc')->paginate(10);
                    }elseif (Session::get('access_group_id') ==6) {
                      $users =  Plan::where('vendor_list','=',Session::get('userid'))->where('status','=',$status)->with('getManager')->with('getSalesRep')->orderBy('created_at','desc')->paginate(10);
                    }else{
                       $users =  Plan::where('status','=',$status)->with('getManager')->with('getSalesRep')->orderBy('created_at','desc')->paginate(10);
                    }
                 //search list with status 
                }else{
                  if (Session::get('access_group_id') ==1) {
                      $users =  Plan::orderBy('created_at','desc')->with('getManager')->with('getSalesRep')->paginate(10);
                    }elseif (Session::get('access_group_id') ==4) {
                      $users =  Plan::where('manager_id','=',Session::get('userid'))->orderBy('created_at','desc')->with('getManager')->with('getSalesRep')->paginate(10);
                    }elseif (Session::get('access_group_id') ==6) {
                      $users =  Plan::where('vendor_list','=',Session::get('userid'))->orderBy('created_at','desc')->with('getManager')->with('getSalesRep')->paginate(10);
                    }else{
                      $users =  Plan::orderBy('created_at','desc')->with('getManager')->with('getSalesRep')->paginate(10);
                    }
                  
                }
                foreach ($users as $key => $value) 
                        {
                            $value['updated'] = date("M d, Y", strtotime($value['created_at']));
                         } 


                return compact('users'); 
              }    

}
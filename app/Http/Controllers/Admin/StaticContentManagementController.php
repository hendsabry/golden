<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Session;
use Lang;
use Response;
use Auth;
use File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Models;
use App\AdminModel;
use App\StaticContentManagement;
use App\Merchant;
use App\Message;
use App\MessageVendor;
use App\User;
use App\AdminMenu;
use App\AdminUserGroup;

  class StaticContentManagementController extends Controller
  {
        public $confirmation_user_email;
        public $confirmation_user_name;
          
        public function __construct(){
          parent::__construct();
          // set admin Panel language
          $this->setLanguageLocaleAdmin();
        }    


      public function staticContentManagement() {
          if (Session::has('userid')) {
              if(Lang::has(Session::get('admin_lang_file').'.BACK_STATIC_CONTENT_MANAGEMENT')!= '') 
              {
                  $session_message = trans(Session::get('admin_lang_file').'.BACK_STATIC_CONTENT_MANAGEMENT');
              }
              else 
              {
                  $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
              }
                  $contentmanagement = StaticContentManagement::orderBy('created_at','desc')->paginate(10);
                  foreach ($contentmanagement as $key => $value) {
                      $value['updated'] = $value['updated_at']->format('M d, Y');
                  }
                  $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                  $group =  explode(",",$groups->group_permission);
                  $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                  return view('admin.static_content.static_content_management',compact('contentmanagement','contentmanagement'))->with("routemenu",$session_message)->with('admin_menu',$admin_menu);;
          }else {
              return Redirect::to('siteadmin');
          } 
      }

      public function staticContentManagementPaginate() 
      {        
          $contentmanagement = StaticContentManagement::orderBy('created_at','desc')->paginate(10);
          return compact('contentmanagement');
      }

      public function addStaticContentManagement(Request $request)
      {
          if (Session::has('userid')) {
                     if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
          {
              $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
          }
          else 
          {
              $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
          }
          if ($request->isMethod('post'))
          { 
              $validator = Validator::make($request->all(), [
              'title' => 'required',
              'title_ar' => 'required',
              'content' => 'required',
              'content_ar' => 'required',
              'tags' => 'required',
              'tags_ar' => 'required',
              'meta_title' => 'required',
              'meta_title_ar' => 'required',

              ]);
              if ($validator->passes()) {
                  $input = $request->all();

                  if (isset($input['images'])) //image upload functioning 
                  {
                      $uploaded_thumb_rules = array(
                      'images' => 'required|mimes:png,jpg,jpeg'
                  );

                  $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

                  if ($thumbnail_validator->passes()) {

                      $uploaded_partner_thumbnail = time() . '_' . $request->file('images')->getClientOriginalName();      

                      $imageUploadSuccess =  Image::make($input['images'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);

                      if ($imageUploadSuccess) {
                          $input['images'] ='/public/assets/adimage/' . $uploaded_partner_thumbnail;
                      } else {
                          return Redirect::to('/admin/static_content_management/add')->with('message', 'Thumbnail was not uploaded')->withInput();
                      }
                  } else {
                      return Redirect::to('/admin/static_content_management/add')->withErrors($thumbnail_validator)->withInput();
                  }
                  }

                  unset($input['_token']);                         
                  $sub_ad = StaticContentManagement::create($input); 
                  Session::put('msg', 'Successfully added a new CMS Page.');
                  return Redirect::to('/admin/static_content_management');
              }else{

                  return Redirect::to('/admin/static_content_management/add')
                  ->with('message','Something went wrong please try again')
                  ->withErrors($validator)->withInput();

              }
          }
          $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
          $group =  explode(",",$groups->group_permission);
          $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
          return view('admin.static_content.add_content_management')->with("routemenu",$session_message)->with('admin_menu',$admin_menu);
                  }else{
                    return Redirect::to('siteadmin');
                  }
      }


      public function deleteCms($id)
      {
          $static_content =  StaticContentManagement::where('id','=',$id)->first();
          $flag = StaticContentManagement::where('id','=',$id)->delete();//delete page
          if (isset($static_content->images) && $static_content->images != "" && file_exists('/var/www/html'.$static_content->images)) {
              unlink('/var/www/html'.$static_content->images);
          } 
          return Redirect::to('/admin/static_content_management');
      }

      public function searchData(Request $request)
      {
          $search = trim($request->get('search_keyword'));
          if(isset($search))
          {
             $this->search_keyword = $search;
          }
          else
          {
             $this->search_keyword = '';
          }             
          $contentmanagement =  StaticContentManagement::where('title','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->paginate(10);//search list with keyword 
      foreach ($contentmanagement as $key => $value) {
                  $value['updated'] = $value['updated_at']->format('M d, Y');
              } 
          return compact('contentmanagement'); 
      }

      public function viewCms($id) 
      {
          if (Session::has('userid')) {

          if(Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') 
          {
              $session_message = trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT');

          }
          else 
          {
             $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT');
          }
          $contentmanagement = StaticContentManagement::where('id','=',$id)->first();
          $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();

          $sub_admin =  AdminModel::where('adm_id','=',$id)->where('admin_type','=',2)->with('get_role')->with('getCity')->with('getCountry')->first();//get user detail
          $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
          $group =  explode(",",$groups->group_permission);
          $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
          return view('admin.static_content.content_management_view')->with("routemenu",$session_message)->with('contentmanagement',$contentmanagement)->with('sub_admin',$sub_admin)->with('user',$user)->with('admin_menu',$admin_menu);;
          }else {
              return Redirect::to('siteadmin');
          } 
      }

      public function broardcastMessage() 
      {
          if (Session::has('userid')) {
              if(Lang::has(Session::get('admin_lang_file').'.BACK_STATIC_CONTENT_MANAGEMENT')!= '') 
              {
                  $session_message = trans(Session::get('admin_lang_file').'.BACK_STATIC_CONTENT_MANAGEMENT');

              }
              else 
              {
                  $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
              }
              $merchant = User::where('cus_status','=',1)->get();
              // get user list 
              $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
              $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
              $group =  explode(",",$groups->group_permission);
              $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
              return view('admin.static_content.cms_message')->with("routemenu",$session_message)->with('user',$user)->with('merchant',$merchant)->with('admin_menu',$admin_menu);;
          }else {
             return Redirect::to('siteadmin');
          } 
      }


      public function sendMail(Request $request)
      {
        if (Session::has('userid')) {
              if(Lang::has(Session::get('admin_lang_file').'.BACK_STATIC_CONTENT_MANAGEMENT')!= '') 
              {
                  $session_message = trans(Session::get('admin_lang_file').'.BACK_STATIC_CONTENT_MANAGEMENT');
              }
              else 
              {
                  $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
              }
              $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
          $input = $request->all();          
          $mesg = Message::Create($input); 
           $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
              $group =  explode(",",$groups->group_permission);
              $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
          return Redirect::to('/admin/static_content_management/broadcasst_message/list')->with('msg', 'Your Message has been saved successfully.')->with("routemenu",$session_message)->with('user',$user)->with('admin_menu',$admin_menu);
          }else {
             return Redirect::to('siteadmin');
          } 
      } 

      public function messageList() 
      {      
          if (Session::has('userid')) {
              if(Lang::has(Session::get('admin_lang_file').'.BACK_STATIC_CONTENT_MANAGEMENT')!= '') 
              {
                  $session_message = trans(Session::get('admin_lang_file').'.BACK_STATIC_CONTENT_MANAGEMENT');

              }
              else 
              {
                  $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
              }
                $msg = Message::orderBy('created_at','desc')->paginate(10);                  
                $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
              $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
              $group =  explode(",",$groups->group_permission);
              $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
              return view('admin.static_content.message_list',compact('msg','msg'))->with("routemenu",$session_message)->with('user',$user)->with('admin_menu',$admin_menu);;
          }else 
          {
              return Redirect::to('siteadmin');
          } 
      }  

      public function messageListPaginate() 
      {        
          $msg = Message::orderBy('created_at','desc')->paginate(10);
          return compact('msg');
      } 


      public function editStaticContentManagement(Request $request,$id)
      {
          if (Session::has('userid')) {
                   if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
          {
             $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
          }
          else 
          {
             $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
          }
          $static_content =  StaticContentManagement::where('id','=',$id)->first();
          if ($request->isMethod('post'))
          { 
              $validator = Validator::make($request->all(), [
              'title' => 'required',
              'title_ar' => 'required',
              'content' => 'required',
              'content_ar' => 'required',
              'tags' => 'required',
              'tags_ar' => 'required',
              'meta_title' => 'required',
              'meta_title_ar' => 'required',
              ]);
              if ($validator->passes()) {
                  $input = $request->all();
                  $static_content =  StaticContentManagement::where('id','=',$id)->first();
                  if (isset($input['images'])) //image upload functioning 
                  {
                     $uploaded_thumb_rules = array(
                     'images' => 'required|mimes:png,jpg,jpeg'
                  );

                  $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

                  if ($thumbnail_validator->passes()) {

                      $uploaded_partner_thumbnail = time() . '_' . $request->file('images')->getClientOriginalName();      

                      $imageUploadSuccess =  Image::make($input['images'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);

                      if ($imageUploadSuccess) {
                      $input['images'] ='/public/assets/adimage/' . $uploaded_partner_thumbnail;
                      if (isset($static_content->images) && $static_content->images != "" && file_exists('/var/www/html'.$static_content->images)) {
                      unlink('/var/www/html'.$static_content->images);
                      } 
                      } else {
                      return Redirect::to('/admin/static_content_management/edit/'.$id)->with('message', 'Thumbnail was not uploaded')->withInput();
                      }
                  } else {
                      return Redirect::to('/admin/static_content_management/add/'.$id)->withErrors($thumbnail_validator)->withInput();
                      }
                  }
                  unset($input['_token']);                         
                  $sub_ad = StaticContentManagement::where('id','=',$id)->update($input); 
                  Session::put('msg', 'Successfully added a Page.');
                  return Redirect::to('/admin/static_content_management');
              }else{

                  return Redirect::to('/admin/static_content_management/edit/'.$id)
                  ->with('message','Something went wrong please try again')
                  ->withErrors($validator)->withInput();
              }
          }
          $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
          $group =  explode(",",$groups->group_permission);
          $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
          return view('admin.static_content.edit_content_management')->with("routemenu",$session_message)->with('static_content',$static_content)->with('admin_menu',$admin_menu);
                  }else{
                    return Redirect::to('siteadmin');
                  }
      }

       public function viewMsg($id) 
      {
          if (Session::has('userid')) {

          if(Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') 
          {
              $session_message = trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT');

          }
          else 
          {
             $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT');
          }
          $contentmanagement = Message::where('id','=',$id)->first();
          $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();

          $sub_admin =  AdminModel::where('adm_id','=',$id)->where('admin_type','=',2)->with('get_role')->with('getCity')->with('getCountry')->first();//get user detail
          $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
          $group =  explode(",",$groups->group_permission);
          $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
          return view('admin.static_content.view_message')->with("routemenu",$session_message)->with('contentmanagement',$contentmanagement)->with('sub_admin',$sub_admin)->with('user',$user)->with('admin_menu',$admin_menu);;
          }else {
              return Redirect::to('siteadmin');
          } 
      }
}
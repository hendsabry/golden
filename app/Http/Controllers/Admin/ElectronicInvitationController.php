<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Excel;
use Session;
use Lang;
use Response;
use Auth;
use Exception;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use CodeItNow\BarcodeBundle\Utils\QrCode;
use App\Http\Models;
use App\AdminModel;
use App\OrderInvitation;
use App\InvitationList;
use App\Merchant;
use App\User;
use App\BusinessOccasionType;
use App\Products;
use App\AdminMenu;
use App\AdminUserGroup;
use App\OrderSingleProduct;


class ElectronicInvitationController extends Controller {
public $confirmation_user_email;
public $confirmation_user_name;
    public function __construct(){
        parent::__construct();
        // set admin Panel language
        $this->setLanguageLocaleAdmin();
    }       

    public function invitationList() {

        if (Session::has('userid')) {

        if(Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_RATING')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_REVIEW_RATING');

        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_RATING');
        }

        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail 

        $users =  OrderInvitation::with('getUser')->with('getPackage')->orderBy('id','DESC')->paginate(10);
        //get Invitation
        foreach ($users as $key => $value) {
        $value['created'] = date("M d, Y", strtotime($value['created_at']));
        $value['invite_count'] = InvitationList::where('order_id','=',$value->order_id)->where('packge_id','=',$value['packge_id'])->where('customer_id','=',$value['customer_id'])->count();
        }

        $customers = User::where('cus_status','=',1)->get();
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        
        return view('admin.electronic_invitation.invitation_list',compact('users'))->with("routemenu",$session_message)->with('user',$user)->with('customers',$customers)->with('admin_menu',$admin_menu);;
        }else {
        return Redirect::to('siteadmin');
        } 
    }


public function updateinvitations(Request $request)
{
 
    $nameEn = $request->name_en;
    $des_en = $request->des_en;
    $des_ar = $request->des_ar;
    $Address_en = $request->Address_en;
     
    $about_en = $request->about_en;
    $name_ar = $request->name_ar;
    $Address_ar = $request->Address_ar;
    $about_ar = $request->about_ar;

$youtubeurl = str_replace('youtu.be','www.youtube.com/embed',$request->youtubeurl);






 if($file=$request->file('logo')){
    
    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
    //$file->move('hallpics',$name); 
    $extension = $file->getClientOriginalExtension();
    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
    //$file->move('hallpics', $fileName);               
    $imageRealPath  =   $file->getRealPath();
    $thumbName      =   'thumb_'. $fileName;       
    $img = Image::make($imageRealPath); // use this if you want facade style code
    $thumb_width = 100;
    list($width,$height) = getimagesize($imageRealPath);
    $thumb_height = ($thumb_width/$width) * $height;
    $img->resize($thumb_width,$thumb_height);


    $img->save('makeupartist' . '/'. $thumbName);
    $file->move('makeupartist/', $fileName);
    $shop_Imgs = url('/').'/makeupartist/'.$thumbName; 

    
    

    } 









    if($files=$request->file('proimage')){
    foreach($files as $file){
    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
    //$file->move('hallpics',$name); 
    $extension = $file->getClientOriginalExtension();
    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
    //$file->move('hallpics', $fileName);               
    $imageRealPath  =   $file->getRealPath();
    $thumbName      =   'thumb_'. $fileName;       
    $img = Image::make($imageRealPath); // use this if you want facade style code
    $thumb_width = 100;
    list($width,$height) = getimagesize($imageRealPath);
    $thumb_height = ($thumb_width/$width) * $height;
    $img->resize($thumb_width,$thumb_height);


    $img->save('makeupartist' . '/'. $thumbName);
    $file->move('makeupartist/', $fileName);
    $shop_Img = url('/').'/makeupartist/'.$thumbName; 

    /*Insert data to db*/
    DB::table('nm_category_gallery')->insert( [
    'image'=>   $shop_Img,
    'category_id' =>16,
    'vendor_id' => 0,
    ]);
    }  

    } 

  if($file=$request->file('logo')){
DB::table('nm_category')->where('mc_id',16)->update(['mc_name'=>$nameEn,'mc_name_ar'=>$name_ar,'mc_discription'=>$des_en,'mc_discription_ar'=>$des_ar,'mc_video_url'=>$youtubeurl,'mc_img'=>$shop_Imgs,'mc_video_description'=>$about_en,'mc_video_description_ar'=>$about_ar,'address'=>$Address_en,'address_ar'=>$Address_ar]);

}
else
{
DB::table('nm_category')->where('mc_id',16)->update(['mc_name'=>$nameEn,'mc_name_ar'=>$name_ar,'mc_discription'=>$des_en,'mc_discription_ar'=>$des_ar,'mc_video_url'=>$youtubeurl,'mc_video_description'=>$about_en,'mc_video_description_ar'=>$about_ar,'address'=>$Address_en,'address_ar'=>$Address_ar]);
}

     return Redirect::back()->with('msg','Updated Successfully');
}

public function deleteinvitations(Request $request)
{

if($request->from=='deletehallpics')
{
$IDs = $request->id;

DB::table('nm_category_gallery')->where('id',$IDs)->delete();
echo 1;

    
}

if($request->from=='deletelogo')
{
$IDs = $request->id;

DB::table('nm_category')->where('mc_id',16)->update(['mc_img'=>'']);
echo 1;

    
}

}

public function manageinvitation() {

        if (Session::has('userid')) {
 if(Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_RATING')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_REVIEW_RATING');

        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_RATING');
        }
        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail 

        $users =  OrderInvitation::with('getUser')->with('getPackage')->paginate(10);
        //get Invitation
        foreach ($users as $key => $value) {
        $value['created'] = date("M d, Y", strtotime($value['created_at']));
        $value['invite_count'] = InvitationList::where('order_id','=',$value->order_id)->where('packge_id','=',$value['packge_id'])->where('customer_id','=',$value['customer_id'])->count();
        }

        $customers = User::where('cus_status','=',1)->get();
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();


        $getInfo = DB::table('nm_category')->where('mc_id',16)->first();


        $fetchfirstdata = DB::table('nm_category_gallery')->where('category_id',16)->get();
 

        return view('admin.electronic_invitation.manage_invitation',compact('users','getInfo','fetchfirstdata'))->with("routemenu",$session_message)->with('user',$user)->with('customers',$customers)->with('admin_menu',$admin_menu);;
        }else {
        return Redirect::to('siteadmin');
        } 
    }










    public function searchData(Request $request)
    {
        $created_date = trim($request->get('created_date'));
        if($created_date != '')
        {
        $created_date = date("Y-m-d", strtotime($created_date));
        }else{
        $created_date = '';
        }   
        $search = trim($request->get('search_keyword'));
        if(isset($search))
        {
        $this->search_keyword = $search;
        }
        else
        {
        $this->search_keyword = '';
        } 
        $status  = trim($request->get('status'));
        if($status != ''  &&  $this->search_keyword  != '' &&  $created_date != '' ){
          $user = User::select('cus_id')->where('cus_name','like',$this->search_keyword.'%')->where('cus_status','=',1)->get()->toArray();
        $users =  OrderInvitation::where('status','=',$status)->whereIn('customer_id',$user)->where('created_at','like',$created_date.'%')->orderBy('created_at','desc')->with('getUser')->with('getPackage')->paginate(10);
        }elseif ($status != ''  &&  $this->search_keyword  != '' &&  $created_date == '') {
          $user = User::select('cus_id')->where('cus_name','like',$this->search_keyword.'%')->where('cus_status','=',1)->get()->toArray();
        $users =  OrderInvitation::where('status','=',$status)->whereIn('customer_id',$user)->orderBy('created_at','desc')->with('getUser')->with('getPackage')->paginate(10);
        }elseif ($status != ''  &&  $this->search_keyword == '' &&  $created_date != '') {
        $users =  OrderInvitation::where('status','=',$status)->where('created_at','like',$created_date.'%')->orderBy('created_at','desc')->with('getUser')->with('getPackage')->paginate(10);
        }elseif ($status != ''  &&  $this->search_keyword == ''  &&  $created_date == '') {
        $users =  OrderInvitation::where('status','=',$status)->orderBy('created_at','desc')->with('getUser')->with('getPackage')->paginate(10);
        }elseif ($status  == '' &&  $this->search_keyword != ''  &&  $created_date != '') {
          $user = User::select('cus_id')->where('cus_name','like',$this->search_keyword.'%')->where('cus_status','=',1)->get()->toArray();
        $users =  OrderInvitation::whereIn('customer_id',$user)->where('created_at','like',$created_date.'%')->orderBy('created_at','desc')->with('getUser')->with('getPackage')->paginate(10);
        }elseif ($status == ''  &&  $this->search_keyword  != '' &&  $created_date == '' ) {
          $user = User::select('cus_id')->where('cus_name','like',$this->search_keyword.'%')->where('cus_status','=',1)->get()->toArray();
        $users =  OrderInvitation::whereIn('customer_id',$user)->orderBy('created_at','desc')->with('getUser')->with('getPackage')->paginate(10);
        }elseif ($status  == '' &&  $this->search_keyword == ''  &&  $created_date != '') {
        $users =  OrderInvitation::where('created_at','like',$created_date.'%')->orderBy('created_at','desc')->with('getUser')->with('getPackage')->paginate(10);
        }else{
        $users =  OrderInvitation::orderBy('created_at','desc')->with('getUser')->with('getPackage')->paginate(10);
        }

        foreach ($users as $key => $value) {
        $value['created'] = date('M d, Y', strtotime($value->created_at));
        $value['invite_count'] = InvitationList::where('order_id','=',$value->order_id)->where('packge_id','=',$value['packge_id'])->where('customer_id','=',$value['customer_id'])->count();
        }    
        return compact('users'); 
    }

    public function changeStatus(Request $request)
    {
        $id = $request->get('id');
        $status = $request->get('status');
        $dta= array();
        $dta['status'] = $status;
        OrderInvitation::where('id','=',$id)->update($dta); // change status of invitation
        return 1 ;
    }
    public function viewInvitation($id) {

        if (Session::has('userid')) {

        if(Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_RATING')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_REVIEW_RATING');

        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_RATING');
        }


        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();

        $invitation =  OrderInvitation::where('id','=',$id)->with('getUser')->with('getPackage')->first();
        $merchant =  Merchant::where('mer_staus','=',1)->orderBy('mer_fname', 'asc')->get();
        $invitation['invite_count'] = InvitationList::where('packge_id',$invitation['packge_id'])->where('customer_id',$invitation['customer_id'])->count();
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        return view('admin.electronic_invitation.view')->with("routemenu",$session_message)->with('user',$user)->with('invitation',$invitation)->with('merchant',$merchant)->with('admin_menu',$admin_menu);
        }else {
        return Redirect::to('siteadmin');
        } 
    }


    public function updateInvitation(Request $request,$id)
    {    
        $status           = $request->get('status');
        $vendor_id        = $request->get('vendor_id');
        $customer_id      = $request->get('customer_id');
        $dta              = array();
        $dta['status']    = $status;
        $lang   = Session::get('lang_file');
        $dta['vendor_id'] = $vendor_id;
        $flag             = OrderInvitation::where('id','=',$id)->update($dta);
        $getInfo = OrderInvitation::where('id','=',$id)->first();

        $orderId = $getInfo->order_id;
        DB::table('nm_invitation_list')->where('order_id',$orderId)->update(['assign_vendor'=>$vendor_id]);

        if($flag) 
        {
        $merchant     = Merchant::where('mer_id',$vendor_id)->first();
        $customers    = User::where('cus_id',$customer_id)->first();
        $cus_name     = $customers->cus_name;
        $cus_email    = $customers->email;
        $cus_phone    = $customers->cus_phone;
        $cus_address1 = $customers->cus_address1;
        if(isset($merchant))
        {
                $email = $merchant->mer_email;
                $baseurl = url('/') ;
                if(isset($merchant->mer_fname)) 
                {
                $username = $merchant->mer_fname;
                }
                else
                {
                $username = '';
                }
                $this->confirmation_user_email = $email;
                $this->confirmation_user_name = "Golden Cage";

                $subject = 'You have assign designcard ! Golden Cage';                           
                Mail::send('admin.mail.assign_to_vendor',
                array(
                'cus_name' => $cus_name,
                'cus_email' => $cus_email,
                'cus_phone' => $cus_phone,
                'cus_address1' => $cus_address1,
                'user_name' => $username,
                'user_email' => $email,
                'site_url' => $baseurl,
                'lang'     => $lang,
                'LANUAGE'  => $lang,
                ), function($message) use($subject)
                {
                $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
                });
        }

        }
        // change status of invitation
        if(\Config::get('app.locale') == 'ar'){
         Session::flash('message', "تم تحديث الفندق بنجاح");
         }
         else
         {
         Session::flash('message', "Electronic Invitation successfully updated");
         }
        return Redirect::to('/admin/designcard/view/'.$id);
    }

    public function invitList($id,$id2,$id3)
    {
      if(Session::has('userid')) 
      {

        if(Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_RATING')!= '') 
        {
           $session_message = trans(Session::get('admin_lang_file').'.BACK_REVIEW_RATING');
        }
        else 
        {
           $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_RATING');
        }
        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();




        $users =  InvitationList::where('order_id',$id3)->where('packge_id',$id)->where('customer_id',$id2)->orderBy('created_at','desc')->paginate(10);
         //echo '<pre>';print_r($users);
         $total =  InvitationList::where('order_id',$id3)->where('packge_id',$id)->where('customer_id',$id2)->where('send_msg',0)->get();
         $is_send = $total->count();
         //echo '<pre>';print_r($is_send);die;
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        return view('admin.electronic_invitation.invite_list',compact('users'))->with("routemenu",$session_message)->with('user',$user)->with('id',$id)->with('id2',$id2)->with('id3',$id3)->with('admin_menu',$admin_menu)->with('is_send',$is_send);
     }
     else 
     {
       return Redirect::to('siteadmin');
     } 
    }

    public function paginateInvite(Request $request){
        $input = $request->all();
        $users =  InvitationList::where('packge_id','=',$input['package_id'])->where('customer_id','=',$input['customer_id'])->orderBy('created_at','desc')->paginate(10);
        return compact('users'); 
    }

    public function addDesignerCard(Request $request)
    {
        if (Session::has('userid')) {
        if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
        }
        $event = BusinessOccasionType::where('top_category', '=', 1)->get();  

        if ($request->isMethod('post'))
        {  
        $validator = Validator::make($request->all(), [
        'pro_mc_id' => 'required',
        'event_type' => 'required',
        'event_sub_type' => 'required',  
        'pro_title' => 'required',
        'pro_title_ar' => 'required',
        ]);
        if ($validator->passes()) {
        $input = $request->all();
        //dd($input);

        if (isset($input['pro_Img'])) //image upload functioning 
        {
        $uploaded_thumb_rules = array(
        'pro_Img' => 'required|mimes:png,jpg,jpeg'
        );

        $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

        if ($thumbnail_validator->passes()) {

        $uploaded_partner_thumbnail = time() . '_' . $request->file('pro_Img')->getClientOriginalName();      

        $imageUploadSuccess =  Image::make($input['pro_Img'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);

        if ($imageUploadSuccess) {
        $input['pro_Img'] ='/public/assets/adimage/' . $uploaded_partner_thumbnail;
        } else {
        return Redirect::to('/admin/electronic_invitation/add')->with('message', 'Thumbnail was not uploaded')->withInput();
        }
        } else {
        return Redirect::to('/admin/electronic_invitation/add')->withErrors($thumbnail_validator)->withInput();
        }
        }

        unset($input['_token']);                         
        $sub_ad = Products::create($input); 
        Session::put('msg', 'Successfully added a new design card.');
        return Redirect::to('/admin/electronic_invitation/list');
        }else{

        return Redirect::to('/admin/electronic_invitation/add')
        ->with('message','Something went wrong please try again')
        ->withErrors($validator)->withInput();

        }
        }
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        return view('admin.electronic_invitation.add_new_designer_card')->with('routemenu',$session_message)->with('event',$event)->with('admin_menu',$admin_menu);
        }else{
        return Redirect::to('siteadmin');
        }
    }

    public function getEventsubType(Request $request){

        $input = $request->all();
        $subtypes  = BusinessOccasionType::where('status','=',1)->where('top_category','=',$input['val'])->orderBy('title')->get();
        $input = $request->all();
        if($input['language'] == 'ar')
        {
        $Event = '<select class="form-control selectpicker" name="event_id" required="required" data-live-search="true" id="sub_type_ar">';
        $Event .= '<option value="">--Select Event Sub Type--</option>';
        foreach($subtypes as $subtype){
        $Event .= '<option value="'.$subtype->id.'">'.$subtype->title_ar.'</option>';
        }
        $Event .=   '</select>';


        }else{
        $Event = '<select class="form-control selectpicker" name="event_id" required="required" data-live-search="true" id="sub_type_en">';
        $Event .= '<option value="">--Select Event Sub Type--</option>';
        foreach($subtypes as $subtype){
        $Event .= '<option value="'.$subtype->id.'">'.$subtype->title.'</option>';
        }
        $Event .=   '</select>';
        }
        echo  $Event;
    }


    public function designerList() {

        if (Session::has('userid')) {

        if(Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_RATING')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_REVIEW_RATING');

        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_RATING');
        }
        $elect_invite = Products::where('pro_mc_id', '=',16)->where('pro_id', '!=',454)->where('pro_id', '!=',455)->orderBy('created_at','desc')->paginate(10);
        foreach ($elect_invite as $key => $value) 
        {
        $value['created'] = date('d/m/Y', strtotime($value['created_at']));
        }
        // foreach ($elect_invite as $key => $value) 
        // {
        //     if (Session::get('admin_lang_file') == 'admin_ar_lang') {
        //         if($value->pro_status=='1')
        //         {
        //             $elect_invite[$key]['pro_status'] = "نشيط";
        //         }
        //         else
        //         {
        //             $elect_invite[$key]['pro_status'] = "غير نشط";
        //         }
        //     }else{
        //         if($value->pro_status=='1')
        //         {
        //             $elect_invite[$key]['pro_status'] = "Active";
        //         }
        //         else
        //         {
        //             $elect_invite[$key]['pro_status'] = "InActive";
        //         }
        //     }
       
        // } 
        foreach ($elect_invite as $key => $value) 
        {
            if (Session::get('admin_lang_file') == 'admin_ar_lang') {
                        if($value->event_type=='1')
                        {
                        $elect_invite[$key]['event_type'] = "اجتماع عمل";

                        }
                        else
                        {
                        $elect_invite[$key]['event_type'] = "زفاف ومناسبات أخرى";

                        }
            } else {
                        if($value->event_type=='1')
                        {
                        $elect_invite[$key]['event_type'] = "Business Meeting";

                        }
                        else
                        {
                        $elect_invite[$key]['event_type'] = "Wedding & Other Occasion";

                        }
            }
        } 
        //dd($elect_invite);

        //get verndors
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        return view('admin.electronic_invitation.designer_list',compact('elect_invite'))->with("routemenu",$session_message)->with('admin_menu',$admin_menu);;
        }else {
        return Redirect::to('siteadmin');
        } 
    }

    public function sms_emailList() {

        if (Session::has('userid')) {

        if(Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_RATING')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_REVIEW_RATING');

        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_RATING');
        }
        $elect_invite = Products::where('pro_mc_id', '=',16)->whereIn('pro_id',array(454,455))->orderBy('created_at','desc')->paginate(10);
        foreach ($elect_invite as $key => $value) 
        {
        $value['created'] = date('d/m/Y', strtotime($value['created_at']));
        }
        // foreach ($elect_invite as $key => $value) 
        // {
        //     if (Session::get('admin_lang_file') == 'admin_ar_lang') {
        //         if($value->pro_status=='1')
        //         {
        //             $elect_invite[$key]['pro_status'] = "نشيط";
        //         }
        //         else
        //         {
        //             $elect_invite[$key]['pro_status'] = "غير نشط";
        //         }
        //     }else{
        //         if($value->pro_status=='1')
        //         {
        //             $elect_invite[$key]['pro_status'] = "Active";
        //         }
        //         else
        //         {
        //             $elect_invite[$key]['pro_status'] = "InActive";
        //         }
        //     }
       
        // } 
        foreach ($elect_invite as $key => $value) 
        {
            if (Session::get('admin_lang_file') == 'admin_ar_lang') {
                        if($value->event_type=='1')
                        {
                        $elect_invite[$key]['event_type'] = "اجتماع عمل";

                        }
                        else
                        {
                        $elect_invite[$key]['event_type'] = "زفاف ومناسبات أخرى";

                        }
            } else {
                        if($value->event_type=='1')
                        {
                        $elect_invite[$key]['event_type'] = "Business Meeting";

                        }
                        else
                        {
                        $elect_invite[$key]['event_type'] = "Wedding & Other Occasion";

                        }
            }
        } 
        //dd($elect_invite);

        //get verndors
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        return view('admin.electronic_invitation.sms_email_list',compact('elect_invite'))->with("routemenu",$session_message)->with('admin_menu',$admin_menu);;
        }else {
        return Redirect::to('siteadmin');
        } 
    }

    public function changeStatusdesigncard(Request $request)
    {
        $id = $request->get('id');
        $status = $request->get('status');
        $dta= array();
        $dta['pro_status'] = $status;
        Products::where('pro_id','=',$id)->update($dta); // change status of invitation
        return 1 ;
    }



    public function editDesignerCard(Request $request,$id)
    {
        if (Session::has('userid')) {
        if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
        }
        $static_content =  Products::where('pro_id','=',$id)->first();
        //dd($static_content);
        $event = BusinessOccasionType::where('top_category', '=', $static_content->event_type)->get();  
        if ($request->isMethod('post'))
        { 
        $validator = Validator::make($request->all(), [
        'pro_mc_id' => 'required',
        'event_type' => 'required',
        'event_sub_type' => 'required',  
        'pro_title' => 'required',
        'pro_title_ar' => 'required',

        ]);
        if ($validator->passes()) {
        $input = $request->all();
        if (isset($input['pro_Img'])) //image upload functioning 
        {
        $uploaded_thumb_rules = array(
        'pro_Img' => 'required|mimes:png,jpg,jpeg'
        );

        $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

        if ($thumbnail_validator->passes()) {

        $uploaded_partner_thumbnail = time() . '_' . $request->file('pro_Img')->getClientOriginalName();      

        $imageUploadSuccess =  Image::make($input['pro_Img'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);

        if ($imageUploadSuccess) {
        $input['pro_Img'] ='/public/assets/adimage/' . $uploaded_partner_thumbnail;
        if (isset($static_content->pro_Img) && file_exists('/var/www/html'.$static_content->pro_Img) && $static_content->pro_Img != '') {
        unlink('/var/www/html'.$static_content->pro_Img);
        } 
        } else {
        return Redirect::to('/admin/electronic_invitation/edit/'.$id)->with('message', 'Thumbnail was not uploaded')->withInput();
        }
        } else {
        return Redirect::to('/admin/electronic_invitation/edit/'.$id)->withErrors($thumbnail_validator)->withInput();
        }
        }

        unset($input['_token']);                         
        $sub_ad = Products::where('pro_id','=',$id)->update($input); 
        Session::put('msg', 'Successfully added a Page.');
        return Redirect::to('/admin/electronic_invitation/list');
        }else{

        return Redirect::to('/admin/electronic_invitation/edit/'.$id)
        ->with('message','Something went wrong please try again')
        ->withErrors($validator)->withInput();
        }
        }
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        return view('admin.electronic_invitation.edit_new_designer_card')->with("routemenu",$session_message)->with('static_content',$static_content)->with('event',$event)->with('admin_menu',$admin_menu);
        }else{
        return Redirect::to('siteadmin');
        }
    }

     public function editSms_Mail(Request $request,$id)
    {
        if (Session::has('userid')) {
        if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
        }
        $static_content =  Products::where('pro_id','=',$id)->first();
        //dd($static_content);
        $event = BusinessOccasionType::where('top_category', '=', $static_content->event_type)->get();  
        if ($request->isMethod('post'))
        { 
        $validator = Validator::make($request->all(), [
        'pro_mc_id' => 'required',
        'event_type' => 'required',
        'event_sub_type' => 'required',  
        'pro_title' => 'required',
        'pro_title_ar' => 'required',

        ]);
        if ($validator->passes()) {
        $input = $request->all();
        if (isset($input['pro_Img'])) //image upload functioning 
        {
        $uploaded_thumb_rules = array(
        'pro_Img' => 'required|mimes:png,jpg,jpeg'
        );

        $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);

        if ($thumbnail_validator->passes()) {

        $uploaded_partner_thumbnail = time() . '_' . $request->file('pro_Img')->getClientOriginalName();      

        $imageUploadSuccess =  Image::make($input['pro_Img'])->save('./public/assets/adimage/'.$uploaded_partner_thumbnail);

        if ($imageUploadSuccess) {
        $input['pro_Img'] ='/public/assets/adimage/' . $uploaded_partner_thumbnail;
        if (isset($static_content->pro_Img) && file_exists('/var/www/html'.$static_content->pro_Img) && $static_content->pro_Img != '') {
        unlink('/var/www/html'.$static_content->pro_Img);
        } 
        } else {
        return Redirect::to('/admin/sms_mail/edit/'.$id)->with('message', 'Thumbnail was not uploaded')->withInput();
        }
        } else {
        return Redirect::to('/admin/sms_mail/edit/'.$id)->withErrors($thumbnail_validator)->withInput();
        }
        }

        unset($input['_token']);                         
        $sub_ad = Products::where('pro_id','=',$id)->update($input); 
        Session::put('msg', 'Successfully added a Page.');
        return Redirect::to('/admin/sms_email/list');
        }else{

        return Redirect::to('/admin/sms_mail/edit/'.$id)
        ->with('message','Something went wrong please try again')
        ->withErrors($validator)->withInput();
        }
        }
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        return view('admin.electronic_invitation.edit_sms_mail')->with("routemenu",$session_message)->with('static_content',$static_content)->with('event',$event)->with('admin_menu',$admin_menu);
        }else{
        return Redirect::to('siteadmin');
        }
    }

    public function delete($pro_id)
    {
        $elect_invite =  Products::where('pro_id','=',$pro_id)->first();
        //dd($elect_invite);

        $flag = Products::where('pro_id','=',$pro_id)->delete();//delete page
        if (isset($elect_invite->pro_Img) && file_exists('/var/www/html'.$elect_invite->pro_Img) && $elect_invite->pro_Img != '') 
        {
        unlink('/var/www/html'.$elect_invite->pro_Img);
        } 
        return Redirect::to('/admin/electronic_invitation/list');
    }

    public function designerCardPaginate() 
    {        
        $elect_invite = Products::where('pro_mc_id', '=',16)->orderBy('created_at','desc')->paginate(10);
        return compact('elect_invite');
    }

    public function view($pro_id) 
    {
        if (Session::has('userid')) {

        if(Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT');

        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT');
        }
        $elect_invite = Products::where('pro_id','=',$pro_id)->with('sub')->first();
        //dd($elect_invite);
        if($elect_invite->event_type=='1')
        {
        $elect_invite['event_type'] = 'Business meeting';
        } 
        else 
        {
        $elect_invite['event_type'] = 'wedding & Other Ocassion' ;
        }
        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get(); 
        return view('admin.electronic_invitation.view_designer_card')->with("routemenu",$session_message)->with('elect_invite',$elect_invite)->with('user',$user)->with('admin_menu',$admin_menu);;
        }else {
        return Redirect::to('siteadmin');
        } 
    }


      public function viewSms_Mail($pro_id) 
    {
        if (Session::has('userid')) {

        if(Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT');

        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT');
        }
        $elect_invite = Products::where('pro_id','=',$pro_id)->with('sub')->first();
        //dd($elect_invite);
        if($elect_invite->event_type=='1')
        {
        $elect_invite['event_type'] = 'Business meeting';
        } 
        else 
        {
        $elect_invite['event_type'] = 'wedding & Other Ocassion' ;
        }
        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get(); 
        return view('admin.electronic_invitation.view_sms_mail')->with("routemenu",$session_message)->with('elect_invite',$elect_invite)->with('user',$user)->with('admin_menu',$admin_menu);;
        }else {
        return Redirect::to('siteadmin');
        } 
    }


    public function searchDesigner(Request $request)
    {
        $created_date = trim($request->get('created_date'));
        if($created_date != '')
        {
        $created_date = date('Y-m-d', strtotime($created_date));
        }else{
        $created_date = '';
        }   
        $event_type  = trim($request->get('event_type'));
        if($event_type != "" &&  $created_date != '')
        {
        $elect_invite = Products::where('pro_mc_id', '=',16)->where('event_type','=',$event_type)->where('created_at','like',$created_date.'%')->orderBy('created_at','desc')->paginate(10);
        }
        elseif ($event_type == "" && $created_date != '') 
        {
        $elect_invite = Products::where('pro_mc_id', '=',16)->where('created_at','like',$created_date.'%')->orderBy('created_at','desc')->paginate(10);
        }
        elseif ($event_type != "" && $created_date == '') 
        {
        $elect_invite = Products::where('pro_mc_id', '=',16)->where('event_type','=',$event_type)->orderBy('created_at','desc')->paginate(10);

        }elseif ($event_type == "" &&  $created_date == '') 
        {
        $elect_invite = Products::where('pro_mc_id', '=',16)->orderBy('created_at','desc')->paginate(10);
        }else
        {
        $elect_invite = Products::where('pro_mc_id', '=',16)->orderBy('created_at','desc')->paginate(10);
        } 

        //return  $elect_invite ;
        foreach ($elect_invite as $key => $value) 
        {
        if($value->event_type=='1')
        {
        $elect_invite[$key]['event_type'] = "Business meeting";

        }
        else
        {
        $elect_invite[$key]['event_type'] = "wedding & Other Ocassion";

        }
        }   
        foreach ($elect_invite as $key => $value) 
        {
        if($value->pro_status=='1')
        {
        $elect_invite[$key]['pro_status'] = "Active";

        }
        else
        {
        $elect_invite[$key]['pro_status'] = "InActive";

        }
        } 
        //dd($elect_invite);
        foreach ($elect_invite as $key => $value) 
        {
        $value['created'] = date('d/m/Y', strtotime($value['created_at']));
        }             

        return compact('elect_invite'); 
    }

       public function sendInvitation($id,$id2,$id3)
       {

        if(Session::has('userid')) 
        {
        if(Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_RATING')!= '') 
        {
           $session_message = trans(Session::get('admin_lang_file').'.BACK_REVIEW_RATING');
        }
        else 
        {
           $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_RATING');
        }
        $users =  InvitationList::where('order_id',$id3)->where('packge_id',$id)->where('customer_id',$id2)->with('getUser')->with('getPackage')->with('getOrderInvitation')->orderBy('created_at','desc')->get();
        $order_invitation = OrderInvitation::where('order_id',$id3)->where('packge_id',$id)->where('customer_id',$id2)->with('getPackage')->first();
        //echo '<pre>';print_r($order_invitation);die;
             if($order_invitation->invitaion_mode == 2) 
             {
                    foreach($users as $key => $value)
                    {
                      if($value->send_msg == 0) 
                      {
                        $qrCode = new QrCode();

                    $barcode_value = $value->id.'_'.$value->order_id;

               $qrCode->setText($barcode_value)
                ->setSize(300)
                ->setPadding(10)
                ->setErrorCorrection('high')
                ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
                ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
                ->setLabel('Scan Qr Code')
                ->setLabelFontSize(16)
                ->setImageType(QrCode::IMAGE_TYPE_PNG)
                ;
                $barcode = 'data:'.$qrCode->getContentType().';base64,'.$qrCode->generate();
                $png_url = "barcode-".time().".png";
                $path = './public/assets/adimage/' . $png_url;
                $imageUploadSuccess =  Image::make(file_get_contents($barcode))->save($path);
                 $this->sendInvitationMail($value, $png_url);
                 $dta['send_msg'] = 1;
                $dta['barcode_value'] = $barcode_value;
                 InvitationList::where('id','=',$value->id)->update($dta);
                 sleep(5);
                }
             }
         }
         else if($order_invitation->invitaion_mode == 3) 
         {
            if($order_invitation->getPackage[0]->pro_id == 120) 
            {
                    foreach ($users as $key => $value) {
                if ($value->send_msg == 0) {
                  $qrCode = new QrCode();
                  $barcode_value = $value->id.'_'.$value->order_id;
                $qrCode
                ->setText($barcode_value)
                ->setSize(300)
                ->setPadding(10)
                ->setErrorCorrection('high')
                ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
                ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
                ->setLabel('Scan Qr Code')
                ->setLabelFontSize(16)
                ->setImageType(QrCode::IMAGE_TYPE_PNG)
                ;
                $barcode = 'data:'.$qrCode->getContentType().';base64,'.$qrCode->generate();
                $png_url = "barcode-".time().".png";
                $path = './public/assets/adimage/' . $png_url;
                $imageUploadSuccess =  Image::make(file_get_contents($barcode))->save($path);
                 $this->sendInvitationMail_1($value, $png_url);
                 $dta['send_msg'] = 1;
                 $dta['barcode_value'] = $barcode_value;
                 InvitationList::where('id','=',$value->id)->update($dta);
                 sleep(5);
                }
             }
            }
            else if($order_invitation->getPackage[0]->pro_id == 124) 
            {
               foreach($users as $key => $value) 
               {
                if($value->send_msg == 0) {
                  $qrCode = new QrCode();
                   $barcode_value = $value->id.'_'.$value->order_id;
                $qrCode
                ->setText($barcode_value)
                ->setSize(300)
                ->setPadding(10)
                ->setErrorCorrection('high')
                ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
                ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
                ->setLabel('Scan Qr Code')
                ->setLabelFontSize(16)
                ->setImageType(QrCode::IMAGE_TYPE_PNG)
                ;
                $barcode = 'data:'.$qrCode->getContentType().';base64,'.$qrCode->generate();
                $png_url = "barcode-".time().".png";
                $path = './public/assets/adimage/' . $png_url;
                $imageUploadSuccess =  Image::make(file_get_contents($barcode))->save($path);
                 $this->sendInvitationMail_2($value, $png_url);
                 $dta['send_msg'] = 1;
                 $dta['barcode_value'] = $barcode_value;
                 InvitationList::where('id','=',$value->id)->update($dta);
                 sleep(5);
                }
             }
            }
            else if ($order_invitation->getPackage[0]->pro_id == 126) 
            {
               foreach ($users as $key => $value) {
                if ($value->send_msg == 0) {
                  $qrCode = new QrCode();
                     $barcode_value = $value->id.'_'.$value->order_id;
                $qrCode
                ->setText($barcode_value)
                ->setSize(300)
                ->setPadding(10)
                ->setErrorCorrection('high')
                ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
                ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
                ->setLabel('Scan Qr Code')
                ->setLabelFontSize(16)
                ->setImageType(QrCode::IMAGE_TYPE_PNG)
                ;
                $barcode = 'data:'.$qrCode->getContentType().';base64,'.$qrCode->generate();
                $png_url = "barcode-".time().".png";
                $path = './public/assets/adimage/' . $png_url;
                $imageUploadSuccess =  Image::make(file_get_contents($barcode))->save($path);
                 $this->sendInvitationMail_3($value, $png_url);
                 $dta['send_msg'] = 1; $dta['barcode_value'] = $barcode_value;
                 InvitationList::where('id','=',$value->id)->update($dta);
                 sleep(5);
                }
             }
            }
            else if ($order_invitation->getPackage[0]->pro_id == 142) 
            {
                foreach ($users as $key => $value) {
                if ($value->send_msg == 0) {
                  $qrCode = new QrCode();
                     $barcode_value = $value->id.'_'.$value->order_id;
                $qrCode
                ->setText($barcode_value)
                ->setSize(300)
                ->setPadding(10)
                ->setErrorCorrection('high')
                ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
                ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
                ->setLabel('Scan Qr Code')
                ->setLabelFontSize(16)
                ->setImageType(QrCode::IMAGE_TYPE_PNG)
                ;
                $barcode = 'data:'.$qrCode->getContentType().';base64,'.$qrCode->generate();
                $png_url = "barcode-".time().".png";
                $path = './public/assets/adimage/' . $png_url;
                $imageUploadSuccess =  Image::make(file_get_contents($barcode))->save($path);
                 $this->sendInvitationMail_4($value, $png_url);
                 $dta['send_msg'] = 1; $dta['barcode_value'] = $barcode_value;
                 InvitationList::where('id','=',$value->id)->update($dta);
                 sleep(5);
                }
             }
            }
            else if ($order_invitation->getPackage[0]->pro_id == 155) 
            {
               foreach ($users as $key => $value) {
                if ($value->send_msg == 0) {
                  $qrCode = new QrCode();
                     $barcode_value = $value->id.'_'.$value->order_id;
                $qrCode
                ->setText($barcode_value)
                ->setSize(300)
                ->setPadding(10)
                ->setErrorCorrection('high')
                ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
                ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
                ->setLabel('Scan Qr Code')
                ->setLabelFontSize(16)
                ->setImageType(QrCode::IMAGE_TYPE_PNG)
                ;
                $barcode = 'data:'.$qrCode->getContentType().';base64,'.$qrCode->generate();
                $png_url = "barcode-".time().".png";
                $path = './public/assets/adimage/' . $png_url;
                $imageUploadSuccess =  Image::make(file_get_contents($barcode))->save($path);
                 $this->sendInvitationMail_5($value, $png_url);
                 $dta['send_msg'] = 1; $dta['barcode_value'] = $barcode_value;
                 InvitationList::where('id','=',$value->id)->update($dta);
                 sleep(5);
                }
             }
            }
         }
         else if($order_invitation->invitaion_mode == 1) 
         {
            //echo $order_invitation->invitaion_mode;die;
            foreach($users as $key_1 => $value) 
            {
                if ($value->send_msg == 0) {
                    if($value->getOrderInvitation[0]->invitation_msg != '') {
                    $msg = $value->getOrderInvitation[0]->invitation_msg;
                    }else{
                    $msg = '';
                    }

                    if($value->getOrderInvitation[0]->venue != '') {
                    $address = $value->getOrderInvitation[0]->venue;
                    }else{
                    $address = '';
                    } 

                    if ($value->getOrderInvitation[0]->date != '') {
                    $date_of_occasion =  date('d,M Y', strtotime($value->getOrderInvitation[0]->date));
                    }else{
                    $date_of_occasion = ''; 
                    }

                    if ($value->getOrderInvitation[0]->time != '') {
                    $time_of_occasion =  $value->getOrderInvitation[0]->time;
                    }else{
                    $time_of_occasion = ''; 
                    }

                    if ($value->getOrderInvitation[0]->occasion_name != '') {
                    $name_of_occasion =  $value->getOrderInvitation[0]->occasion_name;
                    }else{
                    $name_of_occasion = ''; 
                    }

                    if ($value->getOrderInvitation[0]->occasion_type != '') {
                    $type_of_occasions =  $value->getOrderInvitation[0]->occasion_type;
                           $type_of_occasion = '';
        $pro = DB::table('nm_product')->where('pro_id',$type_of_occasions)->count();
        if($pro >=1)
        {
        $pros = DB::table('nm_product')->where('pro_id',$type_of_occasions)->first(); 
        $type_of_occasion = $pros->pro_title; 
        }

                    }else{
                    $type_of_occasion = ''; 
                    }
                    if (isset($value->getUser)) {
                    $invite_by = $value->getUser->cus_name;
                    $invite_by_email = $value->getUser->email;
                    }else{
                    $invite_by = '';
                    $invite_by_email = '';
                    }
                    $barcode_value = $value->id.'_'.$value->order_id;
                    $text_msg = $name_of_occasion.' '.$msg.' on '.$date_of_occasion.' '.'At '.$time_of_occasion.' for '.$type_of_occasion.' at'.$address; 
                    $ulr = url('/elect-barcode/'); 
                    $text_msg .= ' '.$ulr.'/'.$barcode_value;
 

                    $key = "07b0934c-bcd9-4cee-9f7a-4050d200e053";    
                    $secret = "J4thDz1gWkK19HleIqXj1g=="; 
                    $phone_number = '+'.str_replace('-', '', $value->invite_phone);

                    //echo  $phone_number; die;
                    //$phone_number = '+919718381981';

                    $user = "application\\" . $key . ":" . $secret;    
                    $message = array("message"=>$text_msg);    
                    $data = json_encode($message);    
                    $ch = curl_init('https://messagingapi.sinch.com/v1/sms/' . $phone_number);    
                    curl_setopt($ch, CURLOPT_POST, true);    
                    curl_setopt($ch, CURLOPT_USERPWD,$user);    
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);    
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);    
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));    
                    $result = curl_exec($ch);    
                  //print_r($result); die;
                    if(curl_errno($ch)) {    
                    echo 'Curl error: ' . curl_error($ch);    
                    }else{
                        $dta['send_msg'] = 1;                         
                        $dta['barcode_value'] = $barcode_value; 
                        InvitationList::where('id','=',$value->id)->update($dta);
                    }   
                    curl_close($ch);    
                 sleep(5);
                }
             }
             
         }

        Session::put('msg', 'Successfully send an invitation.');
        return Redirect::to('/admin/designcard/invitation/list/'.$id.'/'.$id2.'/'.$id3);
        }else {
        return Redirect::to('siteadmin');
        }
    }

        public function sendInvitationMail($invite_user,$path){
        $email = $invite_user->invite_email;
        if($email=='')
        {
            $email='ajit@wisitech.com';
        }
        //echo $email;die;
        $baseurl = url('/') ;
        if(isset($invite_user->invite_name)) {
        $username = $invite_user->invite_name;
        }else{
        $username = '';
        }
        
        $this->confirmation_user_email = $email;
        $this->confirmation_user_name = "Golden Cage";

        if (Session::get('admin_lang_file') == 'admin_ar_lang') {
            $subject = 'بطاقة دعوة!';
        }else{
        $subject = 'Invitation Card!';
    }

        if($invite_user->getOrderInvitation[0]->invitation_msg != '') {
           $msg = $invite_user->getOrderInvitation[0]->invitation_msg;
        }else{
            $msg = '';
        }

        if($invite_user->getOrderInvitation[0]->venue != '') {
           $address = $invite_user->getOrderInvitation[0]->venue;
        }
        else
        {
            $address = '';
        } 

        if($invite_user->getOrderInvitation[0]->date != '' ) {
         $date_of_occasion =  date('d,M Y', strtotime($invite_user->getOrderInvitation[0]->date)).'/'. $invite_user->getOrderInvitation[0]->time;
        }
        else
        {
            $date_of_occasion = ''; 
        }

        if (isset($invite_user->getUser)) {
          $invite_by = $invite_user->getUser->cus_name;
          $invite_by_email = $invite_user->getUser->email;
        }else{
            $invite_by = '';
            $invite_by_email = '';
        }

        Mail::send('admin.mail.invitation_mail',
        array(
        'user_name' => $username,
        'user_email' => $email,
        'site_url' => $baseurl,
        'msg' => $msg,
        'address' => $address,
        'date_of_occasion' => $date_of_occasion,
        'path' => $path,
        'invite_by' => $invite_by,
        'invite_by_email' => $invite_by_email
        ), function($message) use($subject)
        {
        $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
        });
    }

    public function sendInvitationMail_1($invite_user,$path){

        $email = $invite_user->invite_email;
        if($email==''){
            $email='ajit@wisitech.com';
        }
        $baseurl = url('/') ;
        if (isset($invite_user->invite_name)) {
        $username = $invite_user->invite_name;
        }else{
        $username = '';
        }
        
        $this->confirmation_user_email = $email;
        $this->confirmation_user_name = "Golden Cage";

        if (Session::get('admin_lang_file') == 'admin_ar_lang') {
             $subject = 'بطاقة دعوة!';
        }else{
        $subject = 'Invitation Card!';

       }
        

         if($invite_user->getOrderInvitation[0]->invitation_msg != '') {
           $msg = $invite_user->getOrderInvitation[0]->invitation_msg;
        }else{
            $msg = '';
        } 

 // if($invite_user->getPackage[0]->pro_desc != '') {
 //           $msg .= $invite_user->getPackage[0]->pro_desc;
 //        }else{
 //            $msg = '';
 //        }



        if($invite_user->getOrderInvitation[0]->venue != '') {
           $address = $invite_user->getOrderInvitation[0]->venue;
        }else{
            $address = '';
        } 

        if ($invite_user->getOrderInvitation[0]->date != '') {
         $date_of_occasion = 'On '.date('l, M d, Y', strtotime($invite_user->getOrderInvitation[0]->date));
        }else{
            $date_of_occasion = ''; 
        }


       

        if ($invite_user->getOrderInvitation[0]->time != '') {
         $time_of_occasion =  $invite_user->getOrderInvitation[0]->time;
        }else{
            $time_of_occasion = ''; 
        }

        if ($invite_user->getOrderInvitation[0]->occasion_name != '') {
         $name_of_occasion =  $invite_user->getOrderInvitation[0]->occasion_name;
        }else{
            $name_of_occasion = ''; 
        }

        if ($invite_user->getOrderInvitation[0]->occasion_type != '') {
         $type_of_occasions =  $invite_user->getOrderInvitation[0]->occasion_type;
                $type_of_occasion = '';
        $pro = DB::table('nm_product')->where('pro_id',$type_of_occasions)->count();
        if($pro >=1)
        {
        $pros = DB::table('nm_product')->where('pro_id',$type_of_occasions)->first();

        if (Session::get('admin_lang_file') == 'admin_ar_lang') {
             $type_of_occasion = $pros->pro_title_ar;
         }else{ 
            $type_of_occasion = $pros->pro_title;
         } 
        }

        }else{
            $type_of_occasion = ''; 
        }

 

        if (isset($invite_user->getUser)) {
          $invite_by = $invite_user->getUser->cus_name;
          $invite_by_email = $invite_user->getUser->email;
        }else{
            $invite_by = '';
            $invite_by_email = '';
        }

        Mail::send('admin.mail.design_card.a',
        array(
        'user_name' => $username,
        'user_email' => $email,
        'site_url' => $baseurl,
        'msg' => $msg,
        'address' => $address,
        'date_of_occasion' => $date_of_occasion,
        'time_of_occasion' => $time_of_occasion,
        'name_of_occasion' => $name_of_occasion,
        'type_of_occasion' => $type_of_occasion,
        'path' => $path,
        'invite_by' => $invite_by,
        'invite_by_email' => $invite_by_email
        ), function($message) use($subject)
        {
        $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
        });
    }

    public function sendInvitationMail_2($invite_user,$path){$email = $invite_user->invite_email;
        if($email==''){
            $email='ajit@wisitech.com';
        }

        $baseurl = url('/') ;
        if (isset($invite_user->invite_name)) {
        $username = $invite_user->invite_name;
        }else{
        $username = '';
        }
        
        $this->confirmation_user_email = $email;
        $this->confirmation_user_name = "Golden Cage";

       if (Session::get('admin_lang_file') == 'admin_ar_lang') {
             $subject = 'بطاقة دعوة!';
        }else{
        $subject = 'Invitation Card!';

       }

        if($invite_user->getPackage[0]->pro_desc != '') {
                    if (Session::get('admin_lang_file') == 'admin_ar_lang') {

                    $msg = $invite_user->getPackage[0]->pro_desc_ar;
                    }
                    else
                    {
                    $msg = $invite_user->getPackage[0]->pro_desc;
                    }
        }else{
            $msg = '';
        }

        if($invite_user->getOrderInvitation[0]->venue != '') {
           $address = $invite_user->getOrderInvitation[0]->venue;
        }else{
            $address = '';
        } 

        if ($invite_user->getOrderInvitation[0]->date != '') {

            $date_of_occasion = 'On '.date('l, M d, Y', strtotime($invite_user->getOrderInvitation[0]->date));


        // $date_of_occasion =  date('d,M Y', strtotime($invite_user->getOrderInvitation[0]->date));
        }else{
            $date_of_occasion = ''; 
        }

        if ($invite_user->getOrderInvitation[0]->time != '') {
         $time_of_occasion =  $invite_user->getOrderInvitation[0]->time;
        }else{
            $time_of_occasion = ''; 
        }

        if ($invite_user->getOrderInvitation[0]->occasion_name != '') {
         $name_of_occasion =  $invite_user->getOrderInvitation[0]->occasion_name;
        }else{
            $name_of_occasion = ''; 
        }

        if ($invite_user->getOrderInvitation[0]->occasion_type != '') {
         $type_of_occasions =  $invite_user->getOrderInvitation[0]->occasion_type;
        $type_of_occasion = '';
        $pro = DB::table('nm_product')->where('pro_id',$type_of_occasions)->count();
        if($pro >=1)
        {
        $pros = DB::table('nm_product')->where('pro_id',$type_of_occasions)->first(); 
       
        if (Session::get('admin_lang_file') == 'admin_ar_lang') {
             $type_of_occasion = $pros->pro_title_ar;
         }else{ 
            $type_of_occasion = $pros->pro_title;
         } 


        }




        }else{
            $type_of_occasion = ''; 
        }



        if (isset($invite_user->getUser)) {
          $invite_by = $invite_user->getUser->cus_name;
          $invite_by_email = $invite_user->getUser->email;
        }else{
            $invite_by = '';
            $invite_by_email = '';
        }

        Mail::send('admin.mail.design_card.b',
        array(
        'user_name' => $username,
        'user_email' => $email,
        'site_url' => $baseurl,
        'msg' => $msg,
        'address' => $address,
        'date_of_occasion' => $date_of_occasion,
        'time_of_occasion' => $time_of_occasion,
        'name_of_occasion' => $name_of_occasion,
        'type_of_occasion' => $type_of_occasion,
        'path' => $path,
        'invite_by' => $invite_by,
        'invite_by_email' => $invite_by_email
        ), function($message) use($subject)
        {
        $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
        });
    }

    public function sendInvitationMail_3($invite_user,$path){
        $email = $invite_user->invite_email;
        if($email==''){
            $email='ajit@wisitech.com';
        }
        $baseurl = url('/') ;
        if (isset($invite_user->invite_name)) {
        $username = $invite_user->invite_name;
        }else{
        $username = '';
        }
        
        $this->confirmation_user_email = $email;
        $this->confirmation_user_name = "Golden Cage";

        if (Session::get('admin_lang_file') == 'admin_ar_lang') {
             $subject = 'بطاقة دعوة!';
        }else{
        $subject = 'Invitation Card!';

       }

       if($invite_user->getOrderInvitation[0]->invitation_msg != '') {
           $msg = $invite_user->getOrderInvitation[0]->invitation_msg;
 

        }else{
            $msg = '';
        }

        if($invite_user->getOrderInvitation[0]->venue != '') {
           $address = $invite_user->getOrderInvitation[0]->venue;
        }else{
            $address = '';
        } 

        if ($invite_user->getOrderInvitation[0]->date != '') {
         //$date_of_occasion =  date('d,M Y', strtotime($invite_user->getOrderInvitation[0]->date));
         $date_of_occasion = 'On '.date('l, M d, Y', strtotime($invite_user->getOrderInvitation[0]->date));
        }else{
            $date_of_occasion = ''; 
        }

        if ($invite_user->getOrderInvitation[0]->time != '') {
         $time_of_occasion =  $invite_user->getOrderInvitation[0]->time;
        }else{
            $time_of_occasion = ''; 
        }

        if ($invite_user->getOrderInvitation[0]->occasion_name != '') {
         $name_of_occasion =  $invite_user->getOrderInvitation[0]->occasion_name;
        }else{
            $name_of_occasion = ''; 
        }

        if ($invite_user->getOrderInvitation[0]->occasion_type != '') {
         $type_of_occasions =  $invite_user->getOrderInvitation[0]->occasion_type;
                $type_of_occasion = '';
        $pro = DB::table('nm_product')->where('pro_id',$type_of_occasions)->count();
        if($pro >=1)
        {
        $pros = DB::table('nm_product')->where('pro_id',$type_of_occasions)->first(); 
        //$type_of_occasion = $pros->pro_title; 
        if (Session::get('admin_lang_file') == 'admin_ar_lang') {
             $type_of_occasion = $pros->pro_title_ar;
              $msg = $pros->pro_desc_ar;
         }else{ 
            $type_of_occasion = $pros->pro_title;
         } 

        }

        }else{
            $type_of_occasion = ''; 
        }


  



        if (isset($invite_user->getUser)) {
          $invite_by = $invite_user->getUser->cus_name;
          $invite_by_email = $invite_user->getUser->email;
        }else{
            $invite_by = '';
            $invite_by_email = '';
        }

        Mail::send('admin.mail.design_card.c',
        array(
        'user_name' => $username,
        'user_email' => $email,
        'site_url' => $baseurl,
        'msg' => $msg,
        'address' => $address,
        'date_of_occasion' => $date_of_occasion,
        'time_of_occasion' => $time_of_occasion,
        'name_of_occasion' => $name_of_occasion,
        'type_of_occasion' => $type_of_occasion,
        'path' => $path,
        'invite_by' => $invite_by,
        'invite_by_email' => $invite_by_email
        ), function($message) use($subject)
        {
        $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
        });
    }

    public function sendInvitationMail_4($invite_user,$path){
        $email = $invite_user->invite_email;
        if($email==''){
            $email='ajit@wisitech.com';
        }
        $baseurl = url('/') ;
        if (isset($invite_user->invite_name)) {
        $username = $invite_user->invite_name;
        }else{
        $username = '';
        }
        
        $this->confirmation_user_email = $email;
        $this->confirmation_user_name = "Golden Cage";

         if (Session::get('admin_lang_file') == 'admin_ar_lang') {
             $subject = 'بطاقة دعوة!';
        }else{
        $subject = 'Invitation Card!';

       }

       if($invite_user->getPackage[0]->pro_desc != '') {
           $msg = $invite_user->getPackage[0]->pro_desc;
            if (Session::get('admin_lang_file') == 'admin_ar_lang') {

                    $msg = $invite_user->getPackage[0]->pro_desc_ar;
                    }


        }else{
            $msg = '';
        }

        if($invite_user->getOrderInvitation[0]->venue != '') {
           $address = $invite_user->getOrderInvitation[0]->venue;
        }else{
            $address = '';
        } 

           if($invite_user->getOrderInvitation[0]->invitation_msg != '') {
           $invitation_msg = $invite_user->getOrderInvitation[0]->invitation_msg;
        }else{
            $invitation_msg = '';
        } 


 

        if ($invite_user->getOrderInvitation[0]->date != '') {
         //$date_of_occasion =  date('d,M Y', strtotime($invite_user->getOrderInvitation[0]->date));
         $date_of_occasion = 'On '.date('l, M d, Y', strtotime($invite_user->getOrderInvitation[0]->date));
        }else{
            $date_of_occasion = ''; 
        }

        if ($invite_user->getOrderInvitation[0]->time != '') {
         $time_of_occasion =  $invite_user->getOrderInvitation[0]->time;
        }else{
            $time_of_occasion = ''; 
        }

        if ($invite_user->getOrderInvitation[0]->occasion_name != '') {
         $name_of_occasion =  $invite_user->getOrderInvitation[0]->occasion_name;
        }else{
            $name_of_occasion = ''; 
        }

        if ($invite_user->getOrderInvitation[0]->occasion_type != '') {
         $type_of_occasions =  $invite_user->getOrderInvitation[0]->occasion_type;
                $type_of_occasion = '';
        $pro = DB::table('nm_product')->where('pro_id',$type_of_occasions)->count();
        if($pro >=1)
        {
        $pros = DB::table('nm_product')->where('pro_id',$type_of_occasions)->first(); 
        //$type_of_occasion = $pros->pro_title; 
        if (Session::get('admin_lang_file') == 'admin_ar_lang') {
             $type_of_occasion = $pros->pro_title_ar;
         }else{ 
            $type_of_occasion = $pros->pro_title;
         } 
        }


        }else{
            $type_of_occasion = ''; 
        }



        if (isset($invite_user->getUser)) {
          $invite_by = $invite_user->getUser->cus_name;
          $invite_by_email = $invite_user->getUser->email;
        }else{
            $invite_by = '';
            $invite_by_email = '';
        }

        Mail::send('admin.mail.design_card.d',
        array(
        'user_name' => $username,
        'user_email' => $email,
        'site_url' => $baseurl,
        'msg' => $msg,
        'address' => $address,
        'invitation_msg' =>$invitation_msg,
        'date_of_occasion' => $date_of_occasion,
        'time_of_occasion' => $time_of_occasion,
        'name_of_occasion' => $name_of_occasion,
        'type_of_occasion' => $type_of_occasion,
        'path' => $path,
        'invite_by' => $invite_by,
        'invite_by_email' => $invite_by_email
        ), function($message) use($subject)
        {
        $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
        });
    }

    public function sendInvitationMail_5($invite_user,$path){

        $email = $invite_user->invite_email;

        if($email==''){
            $email='ajit@wisitech.com';
        }

        $baseurl = url('/') ;
        if (isset($invite_user->invite_name)) {
        $username = $invite_user->invite_name;
        }else{
        $username = '';
        }
        
        $this->confirmation_user_email = $email;
        $this->confirmation_user_name = "Golden Cage";

       if (Session::get('admin_lang_file') == 'admin_ar_lang') {
             $subject = 'بطاقة دعوة!';
        }else{
        $subject = 'Invitation Card!';

       }

        if($invite_user->getPackage[0]->pro_desc != '') {
           $msg = $invite_user->getPackage[0]->pro_desc;

            if (Session::get('admin_lang_file') == 'admin_ar_lang') {

                    $msg = $invite_user->getPackage[0]->pro_desc_ar;
                    }
        }else{
            $msg = '';
        }

        if($invite_user->getOrderInvitation[0]->venue != '') {
           $address = $invite_user->getOrderInvitation[0]->venue;
        }else{
            $address = '';
        } 

        if ($invite_user->getOrderInvitation[0]->date != '') {
        // $date_of_occasion =  date('d,M Y', strtotime($invite_user->getOrderInvitation[0]->date));
         $date_of_occasion = 'On '.date('l, M d, Y', strtotime($invite_user->getOrderInvitation[0]->date));
        }else{
            $date_of_occasion = ''; 
        }

        if ($invite_user->getOrderInvitation[0]->time != '') {
         $time_of_occasion =  $invite_user->getOrderInvitation[0]->time;
        }else{
            $time_of_occasion = ''; 
        }

        if ($invite_user->getOrderInvitation[0]->occasion_name != '') {
         $name_of_occasion =  $invite_user->getOrderInvitation[0]->occasion_name;
        }else{
            $name_of_occasion = ''; 
        }

        if ($invite_user->getOrderInvitation[0]->occasion_type != '') {
         $type_of_occasions =  $invite_user->getOrderInvitation[0]->occasion_type;
                $type_of_occasion = '';
        $pro = DB::table('nm_product')->where('pro_id',$type_of_occasions)->count();
        if($pro >=1)
        {
        $pros = DB::table('nm_product')->where('pro_id',$type_of_occasions)->first(); 
        if (Session::get('admin_lang_file') == 'admin_ar_lang') {
             $type_of_occasion = $pros->pro_title_ar;
         }else{ 
            $type_of_occasion = $pros->pro_title;
         } 
        }


        }else{
            $type_of_occasion = ''; 
        }



        if (isset($invite_user->getUser)) {
          $invite_by = $invite_user->getUser->cus_name;
          $invite_by_email = $invite_user->getUser->email;
        }else{
            $invite_by = '';
            $invite_by_email = '';
        }

        Mail::send('admin.mail.design_card.e',
        array(
        'user_name' => $username,
        'user_email' => $email,
        'site_url' => $baseurl,
        'msg' => $msg,
        'address' => $address,
        'date_of_occasion' => $date_of_occasion,
        'time_of_occasion' => $time_of_occasion,
        'name_of_occasion' => $name_of_occasion,
        'type_of_occasion' => $type_of_occasion,
        'path' => $path,
        'invite_by' => $invite_by,
        'invite_by_email' => $invite_by_email
        ), function($message) use($subject)
        {
        $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
        });
    }

}
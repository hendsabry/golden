<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Excel;
use Session;
use Lang;
use Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Models;
use App\AdminMenu;
use App\AdminModel;
use App\AdminUserGroup;
use App\Order;
use App\OrderSingleProduct;
use App\Merchant;
use App\MerchantPaymentHistory;

class PaymentController extends Controller {
    public function __construct(){
        parent::__construct();
        // set admin Panel language
        $this->setLanguageLocaleAdmin();
    } 

        public function payment_transaction() 
        {
            if(Session::has('userid')) 
            {
                if(Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_TRANSACTION')!= '') 
                {
                   $session_message = trans(Session::get('admin_lang_file').'.BACK_PAYMENT_TRANSACTION');
                }
                else 
                {
                   $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_TRANSACTION');
                }

        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail

        $users = Order::orderBy('order_id','desc')->with('getUser')->with('getMerchant')->paginate(10);
     
        $marchant = MerchantPaymentHistory::orderBy('created_at','desc')->with('getOrder.getUser')->with('getMarchant')->paginate(10);
     
        
        //get review data 
        foreach ($users as $key => $value) {
        $value['order_date'] = date('M d, Y', strtotime($value['order_date']));
        //dd($value['id']);
        $total_merchant = OrderSingleProduct::select('merchant_id')->where('order_id', '=', $value->order_id)->groupBy('merchant_id')->get();

        foreach ($total_merchant as $key => $merchant_value) {

          
            $associated_vendor = Merchant::select('mer_id','mer_fname','mer_lname','mer_email')->where('mer_id','=',$merchant_value->merchant_id)->first();

        }  
        if(!empty($associated_vendor)){           
            $value->merchant_name = $associated_vendor->mer_fname.' '.$associated_vendor->mer_lname;
        }else{
            $value->merchant_name = 'NA';
        }
        }

        foreach($marchant as $key => $value) 
        {
            $value['created_at_formate'] = $value['created_at']->format('M d, Y');
            $orderid    = $value->order_id;
            $orderInfo = DB::table('nm_order')->where('order_id',$orderid)->first();
            $value['amount'] = $value->amount + $orderInfo->order_taxAmt;
            $value['payer_name'] = $orderInfo->payer_name;
        }

        //dd($user);
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        return view('admin.payment_transaction.list',compact('users','marchant'))->with("routemenu",$session_message)->with('user',$user)->with('admin_menu',$admin_menu);;
        }else {
        return Redirect::to('siteadmin');
        } 
    }

    public function searchData(Request $request)
    {
        $created_date = trim($request->get('created_date'));
        if($created_date != '')
        {
        $created_date = date("Y-m-d", strtotime($created_date));
        }else{
        $created_date = '';
        }   
        $order_type  = trim($request->get('order_type'));
        $orders_type = array();
        if( $order_type != '' &&  $created_date != ''){
        $users = Order::where('order_type','=',$order_type)->where('created_date','like',$created_date.'%')->orderBy('order_id','desc')->with('getUser')->with('getMerchant')->paginate(10);
            $orders = Order::select('order_id')->where('order_type','=',$order_type)->paginate(10);
            foreach ($orders as $key => $value) {
                $orders_type[] = $value['order_id'];
            }
            $marchant = MerchantPaymentHistory::whereIn('order_id',$orders_type)->where('created_at','like',$created_date.'%')->orderBy('created_at','desc')->with('getOrder.getUser')->with('getMarchant')->paginate(10);
        }elseif ( $order_type != '' &&  $created_date == '') {
            $users = Order::where('order_type','=',$order_type)->orderBy('order_id','desc')->with('getUser')->with('getMerchant')->paginate(10);
            $orders = Order::select('order_id')->where('order_type','=',$order_type)->paginate(10);
            foreach ($orders as $key => $value) {
                $orders_type[] = $value['order_id'];
            }
            $marchant = MerchantPaymentHistory::whereIn('order_id',$orders_type)->orderBy('created_at','desc')->with('getOrder.getUser')->with('getMarchant')->paginate(10);
            }elseif ($order_type == '' &&  $created_date != '') {
             $users = Order::where('created_date','like',$created_date.'%')->orderBy('order_id','desc')->with('getUser')->with('getMerchant')->paginate(10);
             $marchant = MerchantPaymentHistory::where('created_at','like',$created_date.'%')->orderBy('created_at','desc')->with('getOrder.getUser')->with('getMarchant')->paginate(10);
        }else{
        $users = Order::orderBy('order_id','desc')->with('getUser')->with('getMerchant')->paginate(10);
        $marchant = MerchantPaymentHistory::orderBy('created_at','desc')->with('getOrder.getUser')->with('getMarchant')->paginate(10);
        }

        foreach ($users as $key => $value) {
        $value['order_date'] = date('M d, Y', strtotime($value['order_date']));
        $total_merchant = OrderSingleProduct::select('merchant_id')->where('order_id', '=', $value->order_id)->groupBy('merchant_id')->get();

        foreach ($total_merchant as $key => $merchant_value) {

          
            $associated_vendor = Merchant::select('mer_id','mer_fname','mer_lname','mer_email')->where('mer_id','=',$merchant_value->merchant_id)->first();

        }  
        if(!empty($associated_vendor)){           
            $value->merchant_name = $associated_vendor->mer_fname.' '.$associated_vendor->mer_lname;
        }else{
            $value->merchant_name = 'NA';
        }
        }
        foreach ($marchant as $key => $value) {
            $value['created_at_formate'] = $value['created_at']->format('M d, Y');
        }
        return compact('users','marchant'); 
    }

    public function filterCustomer(Request $request)
    {
        $created_date = trim($request->get('created_date'));
        if($created_date != '')
        {
        $created_date = date("Y-m-d", strtotime($created_date));
        }else{
        $created_date = '';
        }   
        $order_type  = trim($request->get('order_type'));
        $orders_type = array();
        if( $order_type != '' &&  $created_date != ''){
        $users = Order::where('order_type','=',$order_type)->where('created_date','like',$created_date.'%')->orderBy('order_id','desc')->with('getUser')->with('getMerchant')->paginate(10);
        }elseif ( $order_type != '' &&  $created_date == '') {
            $users = Order::where('order_type','=',$order_type)->orderBy('order_id','desc')->with('getUser')->with('getMerchant')->paginate(10);
            }elseif ($order_type == '' &&  $created_date != '') {
             $users = Order::where('created_date','like',$created_date.'%')->orderBy('created_date','desc')->with('getUser')->with('getMerchant')->paginate(10);
        }else{
        $users = Order::orderBy('order_id','desc')->with('getUser')->with('getMerchant')->paginate(10);
        }
        foreach ($users as $key => $value) {
        $value['order_date'] = date('M d, Y', strtotime($value['order_date']));
        $total_merchant = OrderSingleProduct::select('merchant_id')->where('order_id', '=', $value->order_id)->groupBy('merchant_id')->get();

        foreach ($total_merchant as $key => $merchant_value) {

          
            $associated_vendor = Merchant::select('mer_id','mer_fname','mer_lname','mer_email')->where('mer_id','=',$merchant_value->merchant_id)->first();

        }  
        if(!empty($associated_vendor)){           
            $value->merchant_name = $associated_vendor->mer_fname.' '.$associated_vendor->mer_lname;
        }else{
            $value->merchant_name = 'NA';
        }
        }
        return compact('users'); 
    }

        public function filterMerchant(Request $request)
        {
            $created_date = trim($request->get('created_date'));
            if($created_date != '')
            {
            $created_date = date("Y-m-d", strtotime($created_date));
            }else{
            $created_date = '';
            }   
            $order_type  = trim($request->get('order_type'));
            $orders_type = array();
            if( $order_type != '' &&  $created_date != ''){
                $orders = Order::select('order_id')->where('order_type','=',$order_type)->paginate(10);
                foreach ($orders as $key => $value) {
                    $orders_type[] = $value['order_id'];
                }
                $marchant = MerchantPaymentHistory::whereIn('order_id',$orders_type)->where('created_at','like',$created_date.'%')->orderBy('created_at','desc')->with('getOrder.getUser')->with('getMarchant')->paginate(10);
            }elseif ( $order_type != '' &&  $created_date == '') {
                $orders = Order::select('order_id')->where('order_type','=',$order_type)->paginate(10);
                foreach ($orders as $key => $value) {
                    $orders_type[] = $value['order_id'];
                }
                $marchant = MerchantPaymentHistory::whereIn('order_id',$orders_type)->orderBy('created_at','desc')->with('getOrder.getUser')->with('getMarchant')->paginate(10);
                }elseif ($order_type == '' &&  $created_date != '') {
                 $marchant = MerchantPaymentHistory::where('created_at','like',$created_date.'%')->orderBy('created_at','desc')->with('getOrder.getUser')->with('getMarchant')->paginate(10);
            }else{
            $marchant = MerchantPaymentHistory::orderBy('created_at','desc')->with('getOrder.getUser')->with('getMarchant')->paginate(10);
            }
            foreach ($marchant as $key => $value) {
                $value['created_at_formate'] = $value['created_at']->format('M d, Y');
            }
            return compact('marchant'); 
        }

        

        public function viewCredited($id){

        if (Session::has('userid')) {

        if(Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_TRANSACTION')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_PAYMENT_TRANSACTION');

        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_TRANSACTION');
        }
        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
        $credit = Order::where('order_id','=',$id)->with('getUser')->with('getMerchant')->first();

$orderid = $credit->order_id;

        $merchantName = ''; 
        $creditMer = OrderSingleProduct::where('order_id','=',$orderid)->get();
        $chkArr = array();
        foreach ($creditMer as $key => $value) {
          $merchantID = $value->merchant_id;
          if( $merchantID !=0 && !in_array($merchantID, $chkArr))
          {
                array_push($chkArr, $merchantID);
              $getMer =   DB::table('nm_merchant')->where('mer_id',$merchantID)->first();
                $merName = $getMer->mer_fname.' '.$getMer->mer_lname;
                $merchantName =  $merName.'/ '.$merchantName;
          }
         if( $merchantID ==0)
          {
             $merchantName = 'Admin';
          }
          
        } 
        $merchantName = rtrim($merchantName,'/ ');

        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        return view('admin.payment_transaction.credit_view')->with("routemenu",$session_message)->with('user',$user)->with('admin_menu',$admin_menu)->with('credit',$credit)->with('merchantName',$merchantName);
        }else {
        return Redirect::to('siteadmin');
        } 
    }

        public function viewDebited($id)
        {

        if(Session::has('userid')) {

        if(Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_TRANSACTION')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_PAYMENT_TRANSACTION');

        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_TRANSACTION');
        }
        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
        

        $debited = MerchantPaymentHistory::where('id',$id)->orderBy('created_at','desc')->with('getOrder.getUser')->with('getMarchant')->first();

$orderid = $debited->order_id;
 $chkArr = array();

        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();

           $merchantName = ''; 
        $creditMer = OrderSingleProduct::where('order_id','=',$orderid)->get();
        foreach ($creditMer as $key => $value) {
          $merchantID = $value->merchant_id;
         if( $merchantID !=0 && !in_array($merchantID, $chkArr))
          {
                array_push($chkArr, $merchantID);
              $getMer =   DB::table('nm_merchant')->where('mer_id',$merchantID)->first();
                $merName = $getMer->mer_fname.' '.$getMer->mer_lname;
                $merchantName =  $merName.'/ '.$merchantName;
          }
         if( $merchantID ==0)
          {
             $merchantName = 'Admin';
          }
          
        } 
$merchantName = rtrim($merchantName,'/ ');
     


        return view('admin.payment_transaction.debit_view')->with("routemenu",$session_message)->with('user',$user)->with('admin_menu',$admin_menu)->with('debited',$debited)->with('merchantName',$merchantName);
        }else {
        return Redirect::to('siteadmin');
        } 
    }

    public function searchCSVCreditData(Request $request)
    {
        return (new \App\Http\Controllers\Admin\Csv\CreditExport)->forYear($request)->download('credited_list.xlsx');
    }
     public function searchCSVDebitData(Request $request)
    {
        return (new \App\Http\Controllers\Admin\Csv\DebitExport)->forYear($request)->download('debited_list.xlsx');
    }
}
<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Excel;
use Session;
use Lang;
use Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Models;
use App\AdminMenu;
use App\AdminModel;
use App\AdminUserGroup;
use App\Order;
use App\MerchantPaymentHistory;
use App\Category;
use App\Products;

class InsuranceController extends Controller {
public $confirmation_user_email;
public $confirmation_user_name;
    public function __construct(){
        parent::__construct();
        // set admin Panel language
        $this->setLanguageLocaleAdmin();
    } 

        public function hallInsurance() 
        {
        if(Session::has('userid')) 
        {
        if(Lang::has(Session::get('admin_lang_file').'.BACK_HALL_INSURANCE')!= '') 
        {
           $session_message = trans(Session::get('admin_lang_file').'.BACK_HALL_INSURANCE');
        }
        else 
        {
           $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_HALL_INSURANCE');
        }
        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail
        $users = Order::where('order_type','=',1)->orderBy('created_date','desc')->with('getUser')->with('getMerchant')->with('getProduct.getCategory')->paginate(10);
        //get order data 
        foreach ($users as $key => $value) {
        $value['created_date'] = date('M d, Y', strtotime($value['created_date']));
        }
        $categoryManagement =  Category::select('mc_id')->where('parent_id','=', 3)->where('mc_status','=',1)->orderBy('created_at','desc')->get()->toArray();
        $category = array_map('current', $categoryManagement);
        $subcategoryManagement =  Category::select('mc_id')->whereIn('parent_id', $category)->where('mc_status','=',1)->orderBy('created_at','desc')->get()->toArray();
        $subcategory = array_map('current', $subcategoryManagement);
        $main_categoryManagement =  Category::select('mc_id')->whereIn('parent_id', $subcategory)->where('mc_status','=',1)->orderBy('created_at','desc')->get()->toArray();
        $subcategory_main = array_map('current', $main_categoryManagement);
        $vender_2 =  Category::select('mc_id')->whereIn('mc_id', $subcategory_main)->where('mc_status','=',1)->orderBy('created_at','desc')->get()->toArray();
        $vender_3 = array_map('current', $vender_2);
         $halls_exit = Products::select('pro_mc_id')->whereIn('pro_mc_id',$vender_3)->get()->toArray();
        $hall = array_map('current', $halls_exit);
        $branchs =  Category::select('mc_id','mc_name','mc_name_ar')->whereIn('mc_id', $hall)->where('mc_status','=',1)->orderBy('created_at','desc')->get();
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();

        $productdetails = DB::table('nm_order')
           ->leftjoin('nm_order_product','nm_order.order_id','=','nm_order_product.order_id')->where('nm_order_product.insurance_amount','!=','')->orderBy('order_date','DESC')->paginate(25)->appends(request()->query());
        
        return view('admin.hall_insurance.list',compact('users','productdetails'))->with("routemenu",$session_message)->with('user',$user)->with('admin_menu',$admin_menu)->with('branchs',$branchs);;
        }else {
        return Redirect::to('siteadmin');
        } 
    }
    
    public function search_insurance(Request $request)
    {
     $input = $request->all();
     echo '<pre>';print_r($input);die;   
    }
     
    public function getHall(Request $request){
        $input = $request->all();
        $pro_mc_id = $request->get('pro_mc_id');
        $halls = Products::select('pro_id','pro_title','pro_title_ar')->where('pro_mc_id',$pro_mc_id)->get();
        if($input['language'] == 'ar')
            {
                $hall = '<select v-model="hall" id="hall" class="form-control dtmagin">';
                $hall .= '<option value="" selected="selected">اختر هول</option>';
                foreach ($halls as $key => $value) {
                   $hall .= '<option value="'.$value->pro_id.'">'.$value->pro_title_ar.'</option>'; 
               }
               $hall .= '</select>';
            }else{
                 $hall = '<select v-model="hall" id="hall" class="form-control dtmagin">';
                $hall .= '<option value="" selected="selected">Select Hall</option>';
                foreach ($halls as $key => $value) {
                   $hall .= '<option value="'.$value->pro_id.'">'.$value->pro_title.'</option>'; 
               }
               $hall .= '</select>';
            }

            echo  $hall;
    }


    public function update_insurance($id,$amt,$oid,$ramt,$mid)
    {
        $userinfo    = DB::table('nm_customer')->where('cus_id',$id)->select('wallet')->first();
        $totalamount = $userinfo->wallet+$amt;
        $updateData  = DB::table('nm_customer')->where('cus_id',$id)->update(['wallet'=>$totalamount]);

        $merinfo    = DB::table('nm_merchant')->where('mer_id',$mid)->select('wallet')->first();
        $meramt=$ramt-$amt;
        $totalmeramount = $merinfo->wallet+$meramt;
        $updatemerData  = DB::table('nm_merchant')->where('mer_id',$mid)->update(['wallet'=>$totalmeramount]);

        DB::table('nm_order_product')->where('id',$oid)->update(['refund_status'=>1]);

        if($updateData)
        {  
           
            //language for display message//
            if(\Config::get('app.locale') == 'ar')
            {
              Session::flash('message', "رد مبلغ التأمين بنجاح");
            }
            else
            {
              Session::flash('message', "Refund successfully insurance amount");
            }
            return Redirect::to('admin/hall_insurance');
        }
        else
        {
         return Redirect::back(); 
        }
         
    }

     public function searchData(Request $request)
    {
        $hall  = trim($request->get('hall'));
        $branch  = trim($request->get('branch'));
        if ($hall != ''  && $branch  != '') {
        $users = Order::where('order_pro_id','=',$hall)->where('order_type','=',1)->orderBy('created_date','desc')->with('getUser')->with('getMerchant')->with('getProduct.getCategory')->paginate(10);
        }else if ($hall != ''  && $branch  == '') {
        $users = Order::where('order_pro_id','=',$hall)->where('order_type','=',1)->orderBy('created_date','desc')->with('getUser')->with('getMerchant')->with('getProduct.getCategory')->paginate(10);
        }else if ($hall == '' && $branch  != '') {
        $halls = Products::select('pro_id')->where('pro_mc_id',$branch)->get()->toArray();
        $halls = array_map('current', $halls);
        $users = Order::whereIn('order_pro_id',$halls)->where('order_type','=',1)->orderBy('created_date','desc')->with('getUser')->with('getMerchant')->with('getProduct.getCategory')->paginate(10);
        }else{
            $users = Order::where('order_type','=',1)->orderBy('created_date','desc')->with('getUser')->with('getMerchant')->with('getProduct.getCategory')->paginate(10);
        }

        foreach ($users as $key => $value) {
        $value['created_date'] = date('M d, Y', strtotime($value['created_date']));
        }
        return compact('users'); 
    }

        
        public function viewInsurance($id) {

        if (Session::has('userid')) {

        if(Lang::has(Session::get('admin_lang_file').'.BACK_HALL_INSURANCE')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_HALL_INSURANCE');

        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_HALL_INSURANCE');
        }


        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();

        $insurace = Order::where('order_id','=',$id)->where('order_type','=',1)->orderBy('created_date','desc')->with('getUser')->with('getMerchant')->with('getProduct.getCategory')->first();
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        return view('admin.hall_insurance.view')->with("routemenu",$session_message)->with('user',$user)->with('insurace',$insurace)->with('admin_menu',$admin_menu);
        }else {
        return Redirect::to('siteadmin');
        } 
    }

    public function refund(Request $request){
        if (Session::has('userid')) {

        if(Lang::has(Session::get('admin_lang_file').'.BACK_HALL_INSURANCE')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_HALL_INSURANCE');

        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_HALL_INSURANCE');
        }
        $input = $request->all();
        $insurace = Order::where('order_id','=',$input['order_id'])->where('order_type','=',1)->with('getUser')->with('getMerchant')->with('getProduct.getCategory')->first();
        if (isset($insurace->getUser[0])) {
           $email = $insurace->getUser[0]->email;
        $baseurl = url('/') ;
        if (isset($insurace->getUser[0]->cus_name)) {
        $username = $insurace->getUser[0]->cus_name;
        }else{
        $username = '';
        }
        if (isset($insurace->getUser[0]->gender)) {
        if($insurace->getUser[0]->gender == '0'){
        $respect = 'Mr';
        }else if ($insurace->getUser[0]->gender == 'female') {
        $respect = 'Ms';
        }else{
        $respect = '';
        }
        }else{
        $respect = '';
        }
        $this->confirmation_user_email = $email;
        $this->confirmation_user_name = "Golden Cage";

        $subject = 'Your Insurance Amount(SAR '. $input['amount'].') is Successfully refund ! Golden Cage';                           

        Mail::send('admin.mail.insurance_refund',
        array(
        'user_name' => $username,
        'user_email' => $email,
        'site_url' => $baseurl,
        'respect' => $respect,
        'refund_money' => $input['amount']
        ), function($message) use($subject)
        {
        $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
        });
        }

        if (isset($insurace->getMerchant[0])) {
                  $email = $insurace->getMerchant[0]->mer_email;
        $baseurl = url('/') ;
        if (isset($insurace->getMerchant[0]->mer_fname)) {
        $username = $insurace->getMerchant[0]->mer_fname;
        }else{
        $username = '';
        }
        $this->confirmation_user_email = $email;
        $this->confirmation_user_name = "Golden Cage";

        $subject = 'Customer('.$insurace->getUser[0]->cus_name.') Insurance Amount(SAR '. $input['amount'].') is Successfully refund ! Golden Cage';                           
        Mail::send('admin.mail.insurance_refund_mer_information',
        array(
        'user_name' => $username,
        'user_email' => $email,
        'site_url' => $baseurl,
        'customer' => $insurace->getUser[0]->cus_name,
        'refund_money' => $input['amount']
        ), function($message) use($subject)
        {
        $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
        });
        }

        unset($input['_token']);
        MerchantPaymentHistory::create($input); 
        Session::put('msg', 'Successfully refund amount');
        return Redirect::to('/admin/hall_insurance');
        }else {
        return Redirect::to('siteadmin');
        } 
    }
}
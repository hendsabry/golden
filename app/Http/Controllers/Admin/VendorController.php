<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB;
use Excel;
use Session;
use Lang;
use Response;
use Auth;
use Exception;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Models;
use App\AdminModel;
use App\Merchant;
use App\AdminMenu;
use App\AdminUserGroup;
use App\Country;
use App\City;
use App\User;
use App\VendorMessage;
use App\MessageMerchant;
use App\MerchantPaymentHistory;
use App\PaymentRequest;
Use App\Category;
Use App\Services;
use Helper;

class VendorController extends Controller {

public $confirmation_user_email;
public $confirmation_user_name;

public function __construct(){
parent::__construct();
// set admin Panel language
$this->setLanguageLocaleAdmin();
}  

  public function vendorlist() 
  {
      if(Session::has('userid')) 
      {
      if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
      {
      $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');

      }
      else 
      {
      $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
      }

      $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail
       if (Session::get('access_group_id') ==1) {
        $users =  Merchant::orderBy('created_at','desc')->paginate(10);
       }elseif (Session::get('access_group_id') ==4) {
        $all_rep = AdminModel::select('adm_id')->where('sales_manager_id','=',Session::get('userid'))->get()->toArray();
          $all_code = AdminModel::select('code')->whereIn('adm_id',$all_rep)->get()->toArray();
          $users =  Merchant::whereIn('sales_rep_code',$all_code)->where('sales_rep_code','!=',null)->orderBy('created_at','desc')->paginate(10);
       }elseif (Session::get('access_group_id') ==6) {
         $all_code = AdminModel::select('code')->where('adm_id','=',Session::get('userid'))->get()->toArray();
         $users =  Merchant::whereIn('sales_rep_code',$all_code)->orderBy('created_at','desc')->paginate(10);
       }else{
        $users =  Merchant::orderBy('created_at','desc')->paginate(10);
       }
       //list of merchant  
      //dd($users);                 
      $groups =  AdminUserGroup::where('level','=',2)->get();
      $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
      $group =  explode(",",$groups_1->group_permission);
      $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
      return view('admin.vendor_management.vendor',compact('users','users'))->with("routemenu",$session_message)->with('user',$user)->with('groups',$groups)->with('admin_menu',$admin_menu);;
      }else {
      return Redirect::to('siteadmin');
      } 
  }   


                  public function changeStatus(Request $request)
                  {
                      $id = $request->get('id');                   
                      $status = $request->get('status');                    
                      $dta= array();
                      $dta['mer_staus'] = $status;
                      
                      //Disable,enable manager also 
                      Merchant::where('vendor_parent_id','=',$id)->update($dta);
                      //Disable,enable shop also                      
                      Category::where('vendor_id','=',$id)->update(['mer_activity'=> $status]);                     
 
                      Merchant::where('mer_id','=',$id)->update($dta); // change status of vendors
 

                      $sub_ad = Merchant::where('mer_id','=',$id)->first();
                      $email = $sub_ad->mer_email;
                      $baseurl = url('/') ;
                      if (isset($sub_ad->mer_fname)) {
                      $username = $sub_ad->mer_fname;
                      }else{
                      $username = '';
                      }
                      if (isset($sub_ad->gender)) {
                      if($sub_ad->gender == '0'){
                      $respect = 'Mr';
                      }else if ($sub_ad->gender == 'female') {
                      $respect = 'Ms';
                      }else{
                      $respect = '';
                      }
                      }else{
                      $respect = '';
                      }
                      $this->confirmation_user_email = $email;
                      $this->confirmation_user_name = "Golden Cage";
                      $status = '';
                      if($sub_ad->mer_staus == 1) 
                      {
                        $subject = 'Your Account is Successfully Activated ! Golden Cage'; 
                        $status = 'Activated';
                        Mail::send('admin.mail.active_status_vendor',
                        array(
                        'user_name' => $username,
                        'user_email' => $email,
                        'site_url' => $baseurl,
                        'status' => $status,
                        'respect' => $respect,
                        ), function($message) use($subject)
                        {
                          $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
                        });
                      }
                      else
                      {
                        $subject = 'Your Account is InActivated ! Golden Cage'; 
                        $status = 'InActivated';
                        Mail::send('admin.mail.in_active_status_vendor',
                        array(
                        'user_name' => $username,
                        'user_email' => $email,
                        'site_url' => $baseurl,
                        'status' => $status,
                        'respect' => $respect,
                        ), function($message) use($subject)
                        {
                          $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
                        });
                      }                           

                      
                     return 1 ;
                  }

                  public function addVendor(Request $request) 
                  {
                      if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                      {
                      $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
                      }
                      else 
                      {
                      $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                      }
                      $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();  
                      $groups =  AdminUserGroup::where('level','=',2)->get();
                      //get admin groups
                      $selected_city  = City::select('ci_con_id')->where('ci_status','=',1)->distinct()->get()->toArray();
                      $selected_country = array();
                      foreach ($selected_city as $key => $value) {
                      $selected_country[] = $value['ci_con_id'];
                      }
                      $countries = Country::whereIn('co_id',$selected_country)->where('co_status','=',1)->groupBy('co_name')->get();
                      $select_city = array();
                      $select_city =  City::where('ci_con_id','=',$countries[0]->co_id)->where('ci_status','=',1)->where('ci_status','=',1)->get();
                      //get country
                      if ($request->isMethod('post'))
                      {
                      $validator = Validator::make($request->all(), [
                      'mer_fname'    => 'required',
                      'mer_lname'    => 'required',
                      'mer_email'    => 'required',                        
                      'mer_phone'    => 'required',
                      'gender'       => 'required',
                      'dob'          => 'required',
                      ]);
                      if ($validator->passes()) 
                      {
                      $input = $request->all();                                  
                      unset($input['_token']);
                      $input['dob'] = date("Y-m-d", strtotime($input['dob']));
                      $password = substr(md5(mt_rand()), 0, 7);
                      $input['mer_password']  = Hash::make($password);
                      $led = Merchant::Create($input);
                      $sub_ad = Merchant::where('mer_id','=',$led->mer_id)->first();
                      $email = $sub_ad->mer_email;
                      $baseurl = url('/') ;
                      if (isset($sub_ad->mer_fname)) {
                      $username = $sub_ad->mer_fname;
                      }else{
                      $username = '';
                      }
                      if (isset($sub_ad->gender)) {
                      if($sub_ad->gender == '0'){
                      $respect = 'Mr';
                      }else if ($sub_ad->gender == 'female') {
                      $respect = 'Ms';
                      }else{
                      $respect = '';
                      }
                      }else{
                      $respect = '';
                      }
                      $this->confirmation_user_email = $email;
                      $this->confirmation_user_name = "Golden Cage";   
                      $subject = 'Your Account is Successfully Created ! Golden Cage';                       
                      Mail::send('admin.mail.send_password_vendor',
                      array(
                      'user_name' => $username,
                      'user_email' => $email,
                      'site_url' => $baseurl,
                      'respect' => $respect,
                      'password' => $password,
                      ), function($message) use($subject)
                      {
                      $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
                      });
                      Session::put('msg', 'Successfully added a new Vendor.');
                      return Redirect::to('/admin/vendor_management');
                      }else
                      {
                      return Redirect::to('/admin/vendor_management/add')
                      ->with('message','Something went wrong please try again')
                      ->withErrors($validator)->withInput();
                      }
                      }                    
                      $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                      $group =  explode(",",$groups_1->group_permission);
                      $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                      return view('admin.vendor_management.add_vendor')->with("routemenu",$session_message)->with('groups',$groups)->with('countries',$countries)->with('select_city',$select_city)->with('admin_menu',$admin_menu)->with('user' ,$user);              
                  }

                  public function editVendor(Request $request,$id)
                  {
                      if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') 
                      {
                      $session_message = trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN');
                      }
                      else 
                      {
                      $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                      }
                      $selected_city  = City::select('ci_con_id')->where('ci_status','=',1)->orderBy('ci_name', 'asc')->distinct()->get()->toArray();
                      $selected_country = array();
                      foreach ($selected_city as $key => $value) {
                      $selected_country[] = $value['ci_con_id'];
                      }
                      $countries = Country::whereIn('co_id',$selected_country)->where('co_status','=',1)->orderBy('co_name', 'asc')->groupBy('co_name')->get();
                      $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first(); 
                      $groups =  AdminUserGroup::where('level','=',2)->get();
                      $users = Merchant::where('mer_id','=',$id)->first(); 
                      // dd($users);                   

                      if ($request->isMethod('post'))
                      {
                      $validator = Validator::make($request->all(), [
                      'mer_fname'    => 'required',
                      'mer_lname'    => 'required',
                      'mer_email'    => 'required',                        
                      'mer_phone'    => 'required',
                      'gender'       => 'required',
                      'dob'          => 'required',
                                    
                      ]);
                      if ($validator->passes()) 
                      {
                      $input = $request->all();
                      unset($input['_token']);
                      $input['dob'] = date("Y-m-d", strtotime($input['dob']));
                      $sub_ad = Merchant::where('mer_id','=',$id)->update($input); 
                      //sub admin entry create 
                      Session::put('msg', 'Successfully added a new Event.');
                      return Redirect::to('/admin/vendor_management');
                      }
                      else
                      {
                      return Redirect::to('/admin/vendor_management/edit/'.$id)
                      ->with('message','Something went wrong please try again')
                      ->withErrors($validator)->withInput();
                      }
                      }
                      $groups_1 =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                      $group =  explode(",",$groups_1->group_permission);
                      $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                      return view('admin.vendor_management.edit_vendor')->with("routemenu",$session_message)->with('groups',$groups)->with('users',$users)->with('admin_menu',$admin_menu)->with('countries',$countries);
                  }

                  public function vendorViewList($id)
                  {
                    //$catArr = array(4,5,6,8,9,10,12,14,15,16,17,127,19,20,21,22,23,25,26,27,29,30,32,34,36,94,95,87,37);
                    if(Session::has('userid')) 
                    {
                      if(Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') 
                      {
                        $session_message = trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT');
                      }
                      else 
                      {
                        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT');
                      }
                      $groups     =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                      $group      =  explode(",",$groups->group_permission);
                      $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                      //$getServicelist = DB::table('nm_category')->whereIn('mc_id',$catArr)->orderBy('mc_id','DESC')->get();
                      $getServicelist = DB::table('nm_services')->where('stor_merchant_id',$id)->orderBy('id','DESC')->get();
                      foreach($getServicelist as $value) 
                      {
                        $serviceid = $value->service_id;
                        $getServiceName = DB::table('nm_category')->where('mc_id',$serviceid)->select('mc_name')->first();
                        $value->service_name = $getServiceName;

                      }
                      $getPagelimit = config('app.paginate');
                       $getTotalAmount = DB::table('nm_wallet_transaction')->where('vendor_id',$id)->where('status',2)->orderBy('id','ASC')->paginate($getPagelimit);

                      return view('admin.vendor_management.view_list_vendor')->with("routemenu",$session_message)->with('getServicelist',$getServicelist)->with('admin_menu',$admin_menu)->with('id',$id)->with('walletamount',$getTotalAmount);
                    }
                    else 
                    {
                      return Redirect::to('siteadmin');
                    } 
                  }
                
                  public function updateserviceofmerchant(Request $request)
                  {
                    $input       = $request->all();
                    //echo '<pre>';print_r($input);die;
                    $dataservice = $input['active_and_deactive'];
                    $merchantid  = $input['merchantid'];
                    DB::table('nm_services')->where('stor_merchant_id',$merchantid)->update(['status'=>0]);                    
                    DB::table('nm_category')->where('vendor_id',$merchantid)->update(['mc_status'=>0]);
                    foreach($dataservice as $key=>$value) 
                    {
                      
                      $getserviceid=DB::table('nm_services')->where('id',$key)->first();

                      
                       DB::table('nm_category')->where('vendor_id',$merchantid)->where('parent_id',$getserviceid->service_id)->update(['mc_status'=>$value]);
                      DB::table('nm_services')->where('id',$key)->update(['status'=>$value]);
                    }
                    // language for display message //
                    if(\Config::get('app.locale') == 'ar')
                    {
                       Session::flash('message', "الخدمات تغيرت بنجاح");
                    }
                    else
                    {
                       Session::flash('message', "Services changed successfully");
                    }
                    // language for display message // 
                    return Redirect::back();  
                  }                 
                  public function viewVendor($id)
                  {
                    if(Session::has('userid')) 
                    {
                      if(Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') 
                      {
                        $session_message = trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT');
                      }
                      else 
                      {
                        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT');
                      }

                     $user =  AdminModel::where('adm_id',Session::get('userid'))->with('getCity')->with('getCountry')->first(); 
                     $sub_admin = Merchant::where('mer_id','=',$id)->first();//get user detail 
                     $sales_rep = AdminModel::where('code','=',$sub_admin->sales_rep_code)->first();
                     $manager   = '';
                     if(isset($sales_rep)) 
                     {
                      $manager = AdminModel::where('adm_id','=',$sales_rep->sales_manager_id)->first();
                     }

                      if($sub_admin->mer_staus=='1')
                      {
                        if (Session::get('admin_lang_file') == 'admin_ar_lang') {
                           $sub_admin['mer_staus'] = 'نشيط';
                        } else {
                           $sub_admin['mer_staus'] = 'Active';
                        }
                      } 
                      else 
                      {
                        if (Session::get('admin_lang_file') == 'admin_ar_lang') {
                            $sub_admin['mer_staus'] = 'غير نشط' ;
                        } else {
                            $sub_admin['mer_staus'] = 'InActive' ;
                        }
                      }

                      if($sub_admin->gender=='0')
                      {
                        if (Session::get('admin_lang_file') == 'admin_ar_lang') {
                           $sub_admin['gender'] = 'الذكر';
                        } else {
                           $sub_admin['gender'] = 'Male';
                        }
                      } 
                      else 
                      {
                        if (Session::get('admin_lang_file') == 'admin_ar_lang') {
                           $sub_admin['gender'] = 'إناثا';
                        } else {
                           $sub_admin['gender'] = 'Female';
                        }
                      }
                      $users = PaymentRequest::where('vendor_id',$id)->orderBy('created_at','desc')->paginate(10); 
                      foreach ($users as $key => $value) {
                       $value['created'] = date("M d, Y", strtotime($value['created_at']));
                      } 

                      $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                      $group =  explode(",",$groups->group_permission);
                      $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                      $members = Services::select('service_id')->where('stor_merchant_id','=',$id)->get()->toArray();
                       $myfield_arr = array_column($members, 'service_id');
                      $services = Category::whereIn('mc_id',$myfield_arr)->get();
                      
                      $getTotalAmount = DB::table('nm_merchant_payment_history')->where('merchant_id',$id)->where('status','!=',1)->orderBy('id','DESC')->paginate(10);


                    $getaccDetailcount = DB::table('nm_merchant_bank_details')->where('merchant_id',$id)->count();
                     
                    
                         $getaccDetail = DB::table('nm_merchant_bank_details')->where('merchant_id',$id)->first();

                 


                      return view('admin.vendor_management.view_vendor')->with("routemenu",$session_message)->with('user',$user)->with('sub_admin',$sub_admin)->with('admin_menu',$admin_menu)->with('users',$users)->with('manager',$manager)->with('sales_rep',$sales_rep)->with('services',$services)->with('getTotalAmount',$getTotalAmount)->with('getaccDetail',$getaccDetail);
                      }
                      else 
                      {
                      return Redirect::to('siteadmin');
                      } 
                  }

                  public function update_payment_of_merchant($id,$merid)
                  {
                    if(Session::has('userid')) 
                    {
                      if(Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') 
                      {
                        $session_message = trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT');
                      }
                      else 
                      {
                        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT');
                      }

                      $merinfo    = DB::table('nm_merchant')->where('mer_id',$merid)->select('wallet')->first();
                      $getpaidam=DB::table('nm_merchant_payment_history')->where('id',$id)->first();
                      if(isset($getpaidam->payable_amount) && $getpaidam->payable_amount>1){$meramt=$getpaidam->payable_amount; }else{ $meramt=$getpaidam->amount;}
        
        $totalmeramount = $merinfo->wallet+$meramt;
        $updatemerData  = DB::table('nm_merchant')->where('mer_id',$merid)->update(['wallet'=>$totalmeramount]);

                      $updateData = DB::table('nm_merchant_payment_history')->where('id',$id)->update(['is_paid'=>1,'status'=>1]);
                      if($updateData)
                      {  
                          //language for display message//
                          if(\Config::get('app.locale') == 'ar')
                          {
                            Session::flash('message', "دفع المبلغ بنجاح");
                          }
                          else
                          {
                            Session::flash('message', "Pay successfully amount");
                          }
                          return Redirect::to('admin/vendor_management/view/'.$merid);
                      }
                      else
                      {
                       return Redirect::back(); 
                      }      
                    }
                    else 
                    {
                      return Redirect::to('siteadmin');
                    } 
                  }

                  public function deleteVendor($id)
                  {
                    $sub_admin = Merchant::where('mer_id','=',$id)->first(); 
                    $flag = Merchant::where('mer_id','=',$id)->delete();//delete lead
                    return Redirect::to('/admin/vendor_management');
                  }   

                  public function searchData(Request $request)
                  {
                    $search = trim($request->search_keyword);
                    $merid = array();
                    if(trim($request->search_shop!=''))
                    {
                      $search_shop = $request->search_shop;
                      $getAlls =  DB::table('nm_category')->where('mc_name','Like','%'.$search_shop.'%')->where('mc_status',1)->get();
                      $getAllsC =  DB::table('nm_category')->where('mc_name','Like','%'.$search_shop.'%')->where('mc_status',1)->count();
                      if($getAllsC  < 1)
                      {
                      $getAlls =  DB::table('nm_category')->where('mc_name_ar','Like','%'.$search_shop.'%')->where('mc_status',1)->get();
                      }
                        foreach ($getAlls as $key => $value) {
                        $VID = $value->vendor_id;
                        array_push($merid, $VID);
                        }
                        $users =  Merchant::whereIn('mer_id',$merid)->orderBy('created_at','desc')->paginate(10);
                        return compact('users'); 
                      }
                      



                     // $search = trim($request->get('search_keyword'));
                      if(isset($search))
                      {
                      $this->search_keyword = $search;
                      }
                      else
                      {
                      $this->search_keyword = '';
                      }   
                      $status  = trim($request->get('status'));

                      if( $status != "" &&  $this->search_keyword != ''){
                        if (Session::get('access_group_id') ==1) {
                          $users =  Merchant::where('mer_fname','LIKE','%'.$this->search_keyword .'%')->orWhere('mer_email','LIKE','%'.$this->search_keyword .'%')->where('mer_staus','=',$status)->orderBy('created_at','desc')->paginate(10);

                        }elseif (Session::get('access_group_id') ==4) {
                           $all_rep = AdminModel::select('adm_id')->where('sales_manager_id','=',Session::get('userid'))->get()->toArray();
                          $all_code = AdminModel::select('code')->whereIn('adm_id',$all_rep)->get()->toArray();
                          $users =  Merchant::whereIn('sales_rep_code',$all_code)->where('mer_fname','LIKE','%'.$this->search_keyword .'%')->where('mer_staus','=',$status)->orderBy('created_at','desc')->paginate(10);
                          # code...
                        }elseif (Session::get('access_group_id') ==6) {
                           $all_code = AdminModel::select('code')->where('adm_id','=',Session::get('userid'))->get()->toArray();
                         $users =  Merchant::whereIn('sales_rep_code',$all_code)->where('mer_fname','LIKE','%'.$this->search_keyword .'%')->orWhere('mer_email','LIKE','%'.$this->search_keyword .'%')->where('mer_staus','=',$status)->orderBy('created_at','desc')->paginate(10);
                          # code...
                        }else{
                          $users =  Merchant::where('mer_fname','LIKE','%'.$this->search_keyword .'%')->orWhere('mer_email','LIKE','%'.$this->search_keyword .'%')->where('mer_staus','=',$status)->orderBy('created_at','desc')->paginate(10);
                        }


                      //search list with keyword and status  $request->search_keyword
                      }
                      else if($this->search_keyword != '' && $status == "")
                      {                   

                        if (Session::get('access_group_id') ==1) {
                          $users =  Merchant::where('mer_fname','LIKE','%'.$request->search_keyword.'%')->orWhere('mer_email','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->paginate(10);

                        }elseif (Session::get('access_group_id') ==4) {
                           $all_rep = AdminModel::select('adm_id')->where('sales_manager_id','=',Session::get('userid'))->get()->toArray();
                          $all_code = AdminModel::select('code')->whereIn('adm_id',$all_rep)->get()->toArray();
                          $users =  Merchant::whereIn('sales_rep_code',$all_code)->where('mer_fname','LIKE','%'.$this->search_keyword .'%')->orWhere('mer_email','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->paginate(10);
                          # code...
                        }elseif (Session::get('access_group_id') ==6) {
                           $all_code = AdminModel::select('code')->where('adm_id','=',Session::get('userid'))->get()->toArray();
                         $users =  Merchant::whereIn('sales_rep_code',$all_code)->where('mer_fname','LIKE','%'.$this->search_keyword .'%')->orWhere('mer_email','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->paginate(10);
                          # code...
                        }else{
                          $users =  Merchant::where('mer_fname','LIKE','%'.$this->search_keyword .'%')->orWhere('mer_email','LIKE','%'.$this->search_keyword .'%')->orderBy('created_at','desc')->paginate(10);
                        }
                      // search list with keyword
                      }else if ($this->search_keyword == '' && $status != "" ) {

                        if (Session::get('access_group_id') ==1) {
                          $users =  Merchant::where('mer_staus','=',$status)->orderBy('created_at','desc')->paginate(10);

                        }elseif (Session::get('access_group_id') ==4) {
                           $all_rep = AdminModel::select('adm_id')->where('sales_manager_id','=',Session::get('userid'))->get()->toArray();
                          $all_code = AdminModel::select('code')->whereIn('adm_id',$all_rep)->get()->toArray();
                          $users =  Merchant::whereIn('sales_rep_code',$all_code)->where('mer_staus','=',$status)->orderBy('created_at','desc')->paginate(10);
                          # code...
                        }elseif (Session::get('access_group_id') ==6) {
                           $all_code = AdminModel::select('code')->where('adm_id','=',Session::get('userid'))->get()->toArray();
                         $users =  Merchant::whereIn('sales_rep_code',$all_code)->where('mer_staus','=',$status)->orderBy('created_at','desc')->paginate(10);
                          # code...
                        }else{

                          $users =  Merchant::where('mer_staus','=',$status)->orderBy('created_at','desc')->paginate(10);
                        }//search list with status 
                      }else{

                        if (Session::get('access_group_id') ==1) {
                           $users =  Merchant::orderBy('created_at','desc')->paginate(10);

                        }elseif (Session::get('access_group_id') ==4) {
                           $all_rep = AdminModel::select('adm_id')->where('sales_manager_id','=',Session::get('userid'))->get()->toArray();
                          $all_code = AdminModel::select('code')->whereIn('adm_id',$all_rep)->get()->toArray();
                           $users =  Merchant::whereIn('sales_rep_code',$all_code)->orderBy('created_at','desc')->paginate(10);
                          # code...
                        }elseif (Session::get('access_group_id') ==6) {
                           $all_code = AdminModel::select('code')->where('adm_id','=',Session::get('userid'))->get()->toArray();
                          $users =  Merchant::whereIn('sales_rep_code',$all_code)->orderBy('created_at','desc')->paginate(10);
                          # code...
                        }else{
                           $users =  Merchant::orderBy('created_at','desc')->paginate(10);
                        }
                      }
                      return compact('users'); 
                  }    

                  public function broardcastMessage() 
                  {
                      $catArr = array(4,5,6,8,9,10,12,14,15,16,17,127,19,20,21,22,23,25,26,27,29,30,32,34,36,94,95,87,37);
                      if (Session::has('userid')) {
                      if(Lang::has(Session::get('admin_lang_file').'.BACK_STATIC_CONTENT_MANAGEMENT')!= '') 
                      {
                      $session_message = trans(Session::get('admin_lang_file').'.BACK_STATIC_CONTENT_MANAGEMENT');

                      }
                      else 
                      {
                      $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                      }
                      $merchant = Merchant::where('mer_staus','=',1)->get();
                      // get user list 
                      $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
                      $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                      $group =  explode(",",$groups->group_permission);
                      $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                      $getServicelist = DB::table('nm_category')->whereIn('mc_id',$catArr)->orderBy('mc_id','DESC')->get();
                      return view('admin.vendor_management.add_message')->with("routemenu",$session_message)->with('user',$user)->with('merchant',$merchant)->with('admin_menu',$admin_menu)->with('getServicelist',$getServicelist);
                      }else {
                      return Redirect::to('siteadmin');
                      } 
                  }

                  public function getmerchantwithajaxfunc()
                  {
                    $catid = $_REQUEST['cat_id'];
                    $setallcat = DB::table('nm_services')->where('service_id',$catid)->get();
                    //echo '<pre>';print_r($setallcat);die;
                    $htmdat = '';
                    $htmdat .= '<select id="boot-multiselect-demo" name="selectedVendor[]" multiple="multiple" required="required" class="width-auto t-box form-control"><option value="">None selected</option>';
                    foreach($setallcat as $value) 
                    {
                      $merchantid = $value->stor_merchant_id;
                      $vendor_name = Helper::getServiceProviderName($merchantid);
                      if(isset($vendor_name) && $vendor_name!='')
                      {            
                       $htmdat .= '<option value="'.$value->stor_merchant_id.'">'.$vendor_name.'</option>';
                      }
                    }
                    $htmdat .= '</select>';
                    echo $htmdat; 
                  }
                  
                  public function sendMail(Request $request)
                  {
                      $input = $request->all();
                      $dta = array();
                      $dta['message'] = $input['message'];
                      $dta['updated_at'] = date("Y-m-d H:i:s");
                      $mesg = VendorMessage::firstOrCreate($dta);
                      if(isset($mesg))
                      {
                        foreach($input['selectedVendor'] as $key => $slv)
                        {   
                          $dta_vendor               = array();
                          $dta_vendor['service_id'] = $input['services'];
                          $dta_vendor['message_id'] = $mesg['id']; 
                          $dta_vendor['created_at'] = now();  
                          $dta_vendor['updated_at'] = now();            
                          $dta_vendor['vendor_id']  = $slv;
                          $msg_vendor               = MessageMerchant::insert($dta_vendor);
                        }
                      }

                      $users = Merchant::whereIn('mer_id',$input['selectedVendor'])->get();
                      foreach ($users as  $user) {
                      $this->sendbroadcastmsg($user, $input['message']);
                      sleep(5);
                      }

                      return Redirect::to('/admin/vendor_management/broadcasst_message/list')->with('msg', 'Your Message has been sent successfully.')->withInput();
                  } 

                  public function sendbroadcastmsg($sub_ad,$msg)
                  {
                      $email = $sub_ad->mer_email;
                      $baseurl = url('/') ;
                      $username = $sub_ad->mer_fname;
                      if($sub_ad->gender == '0'){
                      $respect = 'Mr';
                      }else if ($sub_ad->gender == 'female') {
                      $respect = 'Ms';
                      }else{
                      $respect = '';
                      }
                      $this->confirmation_user_email = $email;
                      $this->confirmation_user_name = "Golden Cage";
                      $subject = 'Golden Cage share a message with You !';
                      Mail::send('admin.mail.broadcast_mail_merchant',
                      array(
                      'user_name' => $username,
                      'user_email' => $email,
                      'site_url' => $baseurl,
                      'msg' => $msg,
                      'respect' => $respect,
                      ), function($message) use($subject)
                      {
                      $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
                      });
                  }
                  public function messageList() 
                  {      
                    if(Session::has('userid')) 
                    {
                      if(Lang::has(Session::get('admin_lang_file').'.BACK_STATIC_CONTENT_MANAGEMENT')!= '') 
                      {
                        $session_message = trans(Session::get('admin_lang_file').'.BACK_STATIC_CONTENT_MANAGEMENT');
                      }
                      else 
                      {
                        $session_message = trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN');
                      }
                      $msg = VendorMessage::orderBy('created_at','desc')->paginate(10);
                       foreach ($msg as $key => $value) {
                      $value['created'] = date("M d, Y", strtotime($value['created_at']));
                      }                   
                      $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
                      $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                      $group =  explode(",",$groups->group_permission);
                      $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                      return view('admin.vendor_management.message_list',compact('msg','msg'))->with("routemenu",$session_message)->with('user',$user)->with('admin_menu',$admin_menu);;
                    }
                    else 
                    {
                      return Redirect::to('siteadmin');
                    } 
                  }  

                  public function viewMsg($id) 
                  {
                      if (Session::has('userid')) {

                      if(Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') 
                      {
                      $session_message = trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT');

                      }
                      else 
                      {
                      $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT');
                      }
                      $contentmanagement = VendorMessage::where('id','=',$id)->first();
                      $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();

                      $sub_admin =  AdminModel::where('adm_id','=',$id)->where('admin_type','=',2)->with('get_role')->with('getCity')->with('getCountry')->first();//get user detail
                      $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                      $group =  explode(",",$groups->group_permission);
                      $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                      return view('admin.vendor_management.view_message')->with("routemenu",$session_message)->with('contentmanagement',$contentmanagement)->with('sub_admin',$sub_admin)->with('user',$user)->with('admin_menu',$admin_menu);;
                      }else {
                      return Redirect::to('siteadmin');
                      } 
                  }

                  public function viewVendorPayment(Request $request,$id)
                  {

                      if (Session::has('userid')) 
                      {

                      if(Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') 
                      {
                      $session_message = trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT');

                      }
                      else 
                      {
                      $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT');
                      }

                      $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first(); 
                      $sub_admin =  Merchant::where('mer_id','=',$id)->first();//get user detail 
                      // dd($sub_admin);
                      if($sub_admin->mer_staus=='1')
                      {
                      $sub_admin['mer_staus'] = 'Active';
                      } 
                      else 
                      {
                      $sub_admin['mer_staus'] = 'In-Active' ;
                      }
                      if($sub_admin->gender=='0')
                      {
                      $sub_admin['gender'] = 'Male';
                      } 
                      else 
                      {
                      $sub_admin['gender'] = 'Female' ;
                      }
                      $pay = PaymentRequest::where('vendor_id', '=',$id)->first();
                      $sub_ad = Merchant::where('mer_id','=',$id)->first();
                      $email = $sub_ad->mer_email;
                      $baseurl = url('/') ;
                      if (isset($sub_ad->mer_fname)) {
                      $username = $sub_ad->mer_fname;
                      }else{
                      $username = '';
                      }
                      if (isset($sub_ad->gender)) {
                      if($sub_ad->gender == '0'){
                      $respect = 'Mr';
                      }else if ($sub_ad->gender == 'female') {
                      $respect = 'Ms';
                      }else{
                      $respect = '';
                      }
                      }else{
                      $respect = '';
                      }
                      $this->confirmation_user_email = $email;
                      $this->confirmation_user_name = "Golden Cage";
                      $subject = 'Payment Request ! Golden Cage';                          

                      Mail::send('admin.mail.vendor_payment_request',
                      array(
                      'user_name' => $username,
                      'user_email' => $email,
                      'site_url' => $baseurl,                      
                      'respect' => $respect,
                      ), function($message) use($subject)
                      {
                      $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
                      });

                      //dd($pay);
                      if ($request->isMethod('post'))
                      {
                      $validator = Validator::make($request->all(), [

                      ]);
                      if ($validator->passes()) 
                      {
                      $input = $request->all();                                  
                      unset($input['_token']);                     
                      $input['merchant_id'] = $sub_admin->mer_id;
                      $input['date'] = date("Y-m-d", strtotime($input['date']));                    
                      $led = MerchantPaymentHistory::Create($input);
                      Session::put('msg', 'Successfully added a new Vendor.');
                      return Redirect::to('/admin/vendor_management');
                      }else
                      {
                      return Redirect::to('/admin/vendor_management/add')
                      ->with('message','Something went wrong please try again')
                      ->withErrors($validator)->withInput();
                      }
                      }  

                      $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                      $group =  explode(",",$groups->group_permission);
                      $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                      return view('admin.vendor_management.view_payment_vendor')->with("routemenu",$session_message)->with('user',$user)->with('sub_admin',$sub_admin)->with('admin_menu',$admin_menu)->with('pay',$pay);;
                      }
                      else 
                      {
                      return Redirect::to('siteadmin');
                      } 
                  }

                  public function viewCredited($id)
                  {
                  if (Session::has('userid')) 
                  {
                  if(Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_TRANSACTION')!= '') 
                  {
                  $session_message = trans(Session::get('admin_lang_file').'.BACK_PAYMENT_TRANSACTION');
                  }
                  else 
                  {
                  $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_TRANSACTION');
                  }
                  $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();
                  $credit = MerchantPaymentHistory::where('order_id','=',$id)->with('getMarchant')->orderBy('created_at','desc')->first();
                  //dd($credit);
                  $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
                  $group =  explode(",",$groups->group_permission);
                  $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
                  return view('admin.vendor_management.view_credit')->with("routemenu",$session_message)->with('user',$user)->with('admin_menu',$admin_menu)->with('credit',$credit);
                  }else {
                  return Redirect::to('siteadmin');
                  } 
                  }

                public function searchCSVData(Request $request)
                {
                return (new \App\Http\Controllers\Admin\Csv\VendorExport)->forYear($request)->download('users.xlsx');
                }

         }
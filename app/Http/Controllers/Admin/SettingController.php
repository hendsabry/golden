<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Excel;
use Session;
use Lang;
use Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Models;
use App\AdminMenu;
use App\AdminModel;
use App\AdminUserGroup;
use App\Setting;

class SettingController extends Controller {
    public function __construct(){
        parent::__construct();
        // set admin Panel language
        $this->setLanguageLocaleAdmin();
    }

    public function settings()
    {
           if(Session::has('userid'))
       {
            if(Lang::has(Session::get('admin_lang_file').'.BACK_SETTINGS')!= '') 
            { 
                $session_message = trans(Session::get('admin_lang_file').'.BACK_SETTINGS');
            }  
            else 
            { 
                $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SETTINGS');
            }   
       $adminheader = view('siteadmin.includes.admin_header')->with("routemenu",$session_message);  
       $adminfooter = view('siteadmin.includes.admin_footer');
       $admin_setting_details = Setting::get();

       $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();

        $country_admin_menu = DB::table('nm_country')->where('co_status','=',1)->get();

       return view('admin.settings')->with('adminheader', $adminheader)->with('adminfooter', $adminfooter)->with('admin_setting_details' , $admin_setting_details)->with('admin_menu',$admin_menu)->with('country_admin_menu',$country_admin_menu);
       }
       else
           {
           return Redirect::to('siteadmin');
           }
    }

    public function admin_submit()
    {

      $data = Input::except(array('_token')) ;
        $rule = array(
            'url1' => 'required',
            'url2' => 'required',
            'url3' => 'required',
            'url4' => 'required',
            'url5' => 'required',
            'url6' => 'required',
            'email' => 'required|email',
            'phone_no' => 'required',
            'address' => 'required',                 
           );
          $validator = Validator::make($data,$rule);            
           if ($validator->fails())
           {
       return Redirect::to('admin/setting')->withErrors($validator->messages())->withInput();
           }
           else
           {
        $entry  =  array(
        'url1' => Input::get('url1'),
        'url2' => Input::get('url2'),
        'url3' => Input::get('url3'),
        'url4' => Input::get('url4'),
        'url5' => Input::get('url5'),
        'url6' => Input::get('url6'),
        'email' => Input::get('email'),
        'phone_no' => Input::get('phone_no'),
        'address' => Input::get('address'),                  
            );
         

        $country_return = Setting::where('id','=',1)->update($entry);

        $cid=Input::get('contryid');
        $taxpercentage=Input::get('taxpercentage');
        $k=0;
        foreach ($cid as $key => $value) {
          # code...
          if($taxpercentage[$k]>0){
          $country_return = DB::table('nm_country')->where('co_id','=',$cid[$k])->update(['vat'=>$taxpercentage[$k]]);
          $k++;
          }
        }


        if(Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_UPDATED_SUCCESSFULLY')!= '') 
            { 
                $session_message = trans(Session::get('admin_lang_file').'.BACK_RECORD_UPDATED_SUCCESSFULLY');
            }  
            else 
            { 
                $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_RECORD_UPDATED_SUCCESSFULLY');
            }   
        return Redirect::to('admin/setting')->with('success', $session_message);
       }
    }



        
}
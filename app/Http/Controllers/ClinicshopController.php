<?php 
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//

use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;
use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\DoctorPrescription;
use App\CartServiceStaff;
use App\Formbusinesstype;
use App\Shoplisthall;
use App\Foodcategorylist;
use App\Clinicshop;
//------------wisitech code end -----------//
class ClinicshopController extends Controller
{  
    public function __construct(){
        parent::__construct();
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
     
    public function clinicshopandservices(Request $request,$halltype,$typeofhallid,$shopid,$branchid)
    {
		$halltype         = $halltype;
		$subcategory_id   = $halltype;
		$shop_id          = $shopid;
        $branchid         = $branchid;
        $city_id          = Session::get('searchdata.cityid');
		if(Session::get('searchdata.basecategoryid')!='')
        {
		  $basecategory_id=Session::get('searchdata.basecategoryid');
		}
        else 
        {
		  $basecategory_id=Session::get('searchdata.maincategoryid');
		}
		 $category_id=$typeofhallid;
         $subsecondcategoryid=$halltype;
		 $typeofhallid=$typeofhallid;
		 $budget=Session::get('searchdata.budget');
		$lang=Session::get('lang_file');
		
		//$request->session()->push('searchdata.typeofhallid', $category_id);
		
		 $foodsessioninfo = array('maincategory_id' => $halltype, "category_id" => $category_id, "shop_id" => $shopid); 
				Session::put('fooddata', $foodsessioninfo);
		
		
		$Formbusinesstype= new Formbusinesstype();
		  $bussinesstype=$Formbusinesstype->Getbussinesstype($basecategory_id,$lang);	
			 $value = $request->session()->get('searchdata');
		       $getsubcategories = '';
			   
			   $foodShoplisthall= new Foodcategorylist();
			   $foodShopdate= new Clinicshop();
			 
			 $foodshopunderbugetincity=$foodShoplisthall->Getfoodshoplistbycity($category_id,$city_id,$budget,$lang);
			// print_r($foodshopunderbugetincity);
             $otherbarnch = Category::select('mc_id','parent_id','mc_name','mc_name_ar')->where('city_id','=',$city_id)->where('parent_id',$shop_id)->get();
			 $fooddateshopdetails=$foodShopdate->Getfoodshopdate($branchid,$lang);
			  $fooddateshopgallery=$foodShopdate->Getgalleryimages($branchid);
			  //$fooddateshopreview=$foodShopdate->GetshopReviews($branchid);
              $fooddateshopreview =   DB::table('nm_review')->where('shop_id',$branchid)->where('review_type','shop')->where('status',1)->orderBy('comment_id', 'desc')->paginate(2);			 
			   $fooddateshopproducts=$foodShopdate->Getfoodshopdateproduct($branchid,$lang);
			  $beautyshopleftproduct=$foodShopdate->Getfoodshopdatefirstproduct($branchid,$lang);
              $prod_id=$beautyshopleftproduct->pro_id;

 $tblfield='service_id';
                    
             if($prod_id!=''){    
          if ($lang == 'ar_lang') {
            $professionalbio = DB::table('nm_product_attribute')->where('product_id', '=', $prod_id)->select('id','attribute_title_ar as attribute_title')->orderBy('id')->get();

               }else{
            $professionalbio = DB::table('nm_product_attribute')->where('product_id', '=', $prod_id)->select('id','attribute_title')->orderBy('id')->get();
          }
          }else{
            $professionalbio=array();
          }
        
         
                    if ($branchid != '' && $lang == 'ar_lang') {
                        $servicecategoryAndservices = DB::table('nm_services_attribute')->where('status',1)->where('services_id', '=', $category_id)->select('id','attribute_title_ar as attribute_title','services_id','vendor_id','image')->orderBy('id','DESC')->get();
                   }else{
                         $servicecategoryAndservices = DB::table('nm_services_attribute')->where('status',1)->where('services_id', '=', $category_id)->select('id','attribute_title','services_id','vendor_id','image')->orderBy('id','DESC')->get();
                }
              
                $getPagelimit = config('app.paginate');
              
                foreach ($servicecategoryAndservices as $sericesdata) {
                       if($sericesdata->id==29){ $atid=29;}elseif($sericesdata->id==30){ $atid=30;}else{ $atid=$sericesdata->id;}
                       //if($sericesdata->id==30){ $atid=1;}else{ $atid=2;}
                     if ($lang == 'ar_lang') {
                        $productbeautyshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $branchid)->where('attribute_id', '=', $atid)->select('pro_id', 'pro_title_ar as pro_title', 'pro_price', 'pro_Img','pro_disprice')->orderBy('pro_title')->paginate('9');
                   }else{
                         $productbeautyshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $branchid)->where('attribute_id', '=', $atid)->select('pro_id', 'pro_title', 'pro_Img','pro_price','pro_disprice')->orderBy('pro_title')->paginate('9');
                          }
                        $sericesdata->serviceslist=$productbeautyshopinfo;
                }
                //echo "<pre>";
//print_r($servicecategoryAndservices);
//die;
                
                /////////////////// end package ///////////
        return view('newWebsite.clinic', compact('getsubcategories','bussinesstype','foodshopunderbugetincity','halltype','fooddateshopdetails','category_id','subcategory_id','subsecondcategoryid','fooddateshopgallery','fooddateshopreview','fooddateshopproducts','beautyshopleftproduct','shop_id','servicecategoryAndservices','branchid','professionalbio','otherbarnch','typeofhallid'));
 
    }

    function getcartproduct(Request $request){
    			$pro_id = $request->product_id;
                $branchid=$request->branchid;
                $date=$request->chk;
                $tblfld=$request->tblfld;
    			$lang   = Session::get('lang_file');
    			$Subdatefoodfunc = new Clinicshop();
        		$getsubproduct=$Subdatefoodfunc->getajaxdatefoodproduct($pro_id,$lang,$branchid,$tblfld,$date);
                
                
        	
			return response()->json($getsubproduct);

    }

    function addcartproduct(Request $request)
    {
       //echo "<pre>";
    	//print_r($request->all());
    	//die;
    	$branch_id           = $request->branch_id;
    	$shop_id             = $request->shop_id;
    	$vendor_id           = $request->vendor_id;
        $product_id          = $request->product_id;
        $product_price       = $request->product_price;        
        $cart_sub_type       = $request->cart_sub_type;
        $category_id         = $request->category_id;
        $subsecondcategoryid = $request->subsecondcategoryid;
        $bookingdate         = $request->bookingdate;
        $bookingtime         = $request->bookingtime;
        $patienttype         = $request->patienttype;
        $fileno              = $request->fileno;
    	$userid              = Session::get('customerdata.user_id');
        $productinfo         = new Clinicshop();
        $lang                = Session::get('lang_file');
        $cartproductdata     = $productinfo->Getproductforcart($product_id,$lang);
        $pro_title           = $cartproductdata->pro_title;
        $pro_Img             = $cartproductdata->pro_Img;
        $pro_title_ar        = $cartproductdata->pro_title_ar;
        $product             = Products::select('pro_id','pro_title','pro_title_ar','pro_desc','pro_desc_ar','pro_Img','pro_price','service_hour','pro_mc_id')->where('pro_id',$product_id)->first(); //GET Product Detail 
        $file_no = DoctorPrescription::where('clinic_id',$branch_id)->where('doc_id',$product_id)->where('file_number',$fileno)->first();
           //dd($file_no);
                 if(!empty($fileno)) 
                 {
                     $cart = Cart::where('user_id',$userid)->first();
                     if(!empty($cart)) 
                     {
                         $cart_pro = CartProduct::where('cart_type','clinic')->where('cart_id',$cart->id)->where('product_id',$product_id)->delete();
                         $cart_option = CartServiceStaff::where('cart_id',$cart->id)->where('service_id',$product_id)->delete();
                     } 
                     else 
                     {
                        $cart_data = array();
                        $cart_data['user_id'] = $userid;
                        $cart = Cart::create($cart_data); //cart entery
                     }
                     if(!empty($cart)) 
                     {
                        $cart_product_data                    = array();
                        $cart_product_data['merchant_id']     = $vendor_id;
                        $cart_product_data['shop_id']         = $branch_id;
                        $cart_product_data['category_id']     = $shop_id;
                        $cart_product_data['cart_type']       = 'clinic';
                        $cart_product_data['cart_sub_type']   = $cart_sub_type;
                        $cart_product_data['cart_id']         = $cart->id;
                        $cart_product_data['product_id']      = $product_id;
                        $cart_product_data['total_price']     = $product_price;                       
                        $cart_product_data['shop_vendor_id']  = $branch_id;
                        $cart_product_data['pro_title']       = $product->pro_title;
                        $cart_product_data['pro_title_ar']    = $product->pro_title_ar;
                        $cart_product_data['pro_desc']        = $product->pro_desc;
                        $cart_product_data['pro_desc_ar']     = $product->pro_desc_ar;
                        $cart_product_data['pro_Img']         = $product->pro_Img;
                        $cart_product_data['review_type']     = 'shop';
                        $cart_product_data['quantity']        = '1';
                        $cart_product_data['pro_price']       = $product->pro_price;
                        $cart_product_data['status']          = 1;
                        $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                        $cart_service_staff                         = array();
                        $cart_service_staff['cart_id']              = $cart->id;
                        $cart_service_staff['cart_type']            = 'clinic';
                        if($fileno) 
                        {
                            $cart_service_staff['file_no']          = $fileno;
                        }
                        $cart_service_staff['shop_id']              = $branch_id;
                        $cart_service_staff['service_id']           = $product_id;
                       // $cart_service_staff['staff_id']             = $product_id;
                        $cart_service_staff['staff_id']             = $subsecondcategoryid;
                        $cart_service_staff['booking_date']         = $bookingdate;
                        $cart_service_staff['start_time']           = $bookingtime;
                        $cart_service_staff['staff_member_name']    = $product->pro_title;
                        $cart_service_staff['staff_member_name_ar'] = $product->pro_desc_ar;
                        CartServiceStaff::create($cart_service_staff); //Cart Service Entery entery                               
                    } 
                 }
                 else
                 {
                    $cart = Cart::where('user_id',$userid)->first();
                    if(!empty($cart)) 
                    {
                        $cart_pro = CartProduct::where('cart_type','clinic')->where('cart_id',$cart->id)->where('product_id',$product_id)->delete();
                        $cart_option = CartServiceStaff::where('cart_id',$cart->id)->where('service_id',$product_id)->delete();
                    } 
                    else 
                    {
                        $cart_data = array();
                        $cart_data['user_id'] = $userid;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                    if(!empty($cart)) 
                    {                                
                        $cart_product_data                          = array();
                        $cart_product_data['merchant_id']           = $vendor_id;
                        $cart_product_data['shop_id']               = $branch_id;
                        $cart_product_data['category_id']           = $shop_id;
                        $cart_product_data['cart_type']             = 'clinic';
                        $cart_product_data['cart_sub_type']         = $cart_sub_type;
                        $cart_product_data['cart_id']               = $cart->id;
                        $cart_product_data['product_id']            = $product_id;
                        $cart_product_data['total_price']           = $product_price;                              
                        $cart_product_data['shop_vendor_id']        = $branch_id;
                        $cart_product_data['pro_title']             = $product->pro_title;
                        $cart_product_data['pro_title_ar']          = $product->pro_title_ar;
                        $cart_product_data['pro_desc']              = $product->pro_desc;
                        $cart_product_data['pro_desc_ar']           = $product->pro_desc_ar;
                        $cart_product_data['pro_Img']               = $product->pro_Img;
                        $cart_product_data['review_type']           = 'shop';
                        $cart_product_data['pro_price']             = $product->pro_price;
                        $cart_product_data['status']                = 1;
                        $cart_product_data['quantity']              = '1';
                        CartProduct::create($cart_product_data);//cart product entry  
                        $cart_service_staff                         = array();
                        $cart_service_staff['cart_id']              = $cart->id;
                        $cart_service_staff['cart_type']            = 'clinic';                                    
                        $cart_service_staff['shop_id']              = $branch_id;
                        $cart_service_staff['service_id']           = $product_id;
                       // $cart_service_staff['staff_id']             = $product_id;
                        $cart_service_staff['staff_id']             = $subsecondcategoryid;
                        $cart_service_staff['booking_date']         = $bookingdate;
                        $cart_service_staff['start_time']           = $bookingtime;
                        $cart_service_staff['staff_member_name']    = $product->pro_title;
                        $cart_service_staff['staff_member_name_ar'] = $product->pro_desc_ar;
                        CartServiceStaff::create($cart_service_staff);//Cart Service Entery entery
                    }                       
                }                 
                if(\Config::get('app.locale') == 'ar')
                {
                Session::flash('status', "تمت إضافة السلة بنجاح");
                }
                else
                {
                Session::flash('status', "Added Cart Successfully");
                }
            //language for display message //                
            return Redirect::back();
            return redirect('clinicshop/'.$category_id.'/'.$subsecondcategoryid.'/'.$shop_id.'/'.$branch_id);
        }

        function chkavailbility(Request $request)
        {
          
        $bdate = $request->bdate;
        $btime = $request->btime;
        $pid = $request->pid;       
        $count =   DB::table('nm_order_services_staff')->where('booking_date',$bdate)->where('start_time',$btime)->where('staff_id',$pid)->count();
      echo $count;
         
        }

}

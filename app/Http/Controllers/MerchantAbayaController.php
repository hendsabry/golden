<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Models;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon;
use File;
use App\Reviewrating;
use App\Categorylist;
use Intervention\Image\ImageManagerStatic as Image; 
use Auth;
use Lang;
use App\ProductsMerchant;
use App\City;
use App\Merchant;
use App\NmServicesAttribute;
use App\HallOffer;
use App\NmSize;
use App\NmProductGallery;
use App\NmFabrics;
use App\NmproductOptionValue;
class MerchantAbayaController extends Controller
{
      public function __construct(){
            parent::__construct();
            // set admin Panel language
            $this->middleware(function ($request, $next) {
                        $this->setLanguageLocaleMerchant();
                        Session::get('mer_lang_code');
                        return $next($request);
           });
     }


    public function abayaShopInfo(Request $request)
     { 
        

       if(Session::has('merchantid')) 
      {
          $mer_id   = Session::get('merchantid');
          $parent_id = request()->id;
          $sid = request()->sid;
          $fetchdata = Categorylist::where('mc_id',$sid)->where('vendor_id',$mer_id)->first();
           $city = City::where('ci_con_id',10)->get();
           $manager = Merchant::where('vendor_parent_id',$mer_id)->where('mer_staus',1)->get();
           
          $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
          return view('sitemerchant.abaya.abaya-shop-info', compact('merchantheader','merchantfooter','parent_id','sid','city','manager','fetchdata','shopName'));       
      } 
      else 
          {
              return Redirect::to('sitemerchant');
          }
        
      }

      //Store shop Info
            public function storeabayaShopInfo(Request $request)

            {
                $merid  = Session::get('merchantid');
                $sid = $request->sid;
                $id = $request->parent_id;

               if($sid){
                  $savebranch =Categorylist::where('mc_id',$sid)->first();
                } else { 
                 $savebranch = new Categorylist;
               }
          if($request->file('branchimage')){ 
                  $file = $request->branchimage;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                  $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 150;
                  list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('uploadimage/abaya' . '/'. $thumbName);
                  $file->move('uploadimage/abaya/', $Image);
                  $savebranch->mc_img = url('').'/uploadimage/abaya/'.$thumbName;
                }
                //Addressimage
                if($request->file('address_image')){ 
                  $file = $request->address_image;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                  $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('uploadimage/abaya' . '/'. $thumbName);
                  $file->move('uploadimage/abaya/', $Image);
                  $savebranch->address_image = url('').'/uploadimage/abaya/'.$thumbName;
                }
              //Termaand condition
               if(!empty($request->mc_tnc)){ 
                  $file = $request->mc_tnc;
                  $orgname =  $file->getClientOriginalName();
                  $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                  $extension = $file->getClientOriginalExtension();
                  $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                  $file->move('./public/assets/storeimage/', $filename_c);                      
                  $Cname =   url('').'/public/assets/storeimage/'.$filename_c;
                  $savebranch->terms_conditions =$Cname;
                  
               }
               else {
                    $orgname = $request->tmcvalue; 
               }
                //Terms Condition in Arabic
               if(!empty($request->mc_tnc_ar)){ 
                  $file = $request->mc_tnc_ar;
                  $orgname_ar =  $file->getClientOriginalName();
                  $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                  $extension = $file->getClientOriginalExtension();
                  $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                  $file->move('./public/assets/storeimage/', $filename_c);                      
                  $Cname_ar =   url('').'/public/assets/storeimage/'.$filename_c;
                  $savebranch->terms_conditions_ar =$Cname_ar;
                  
               }
               else {
                    $orgname_ar = $request->tmcvalue_ar; 
               }
 
                  $savebranch ->mc_status = $request->mc_status;
                  $savebranch ->vendor_id = $merid;
                  $savebranch ->google_map_address = $request->google_map_address;

                  $savebranch ->latitude = $request->latitude;
                  $savebranch ->longitude = $request->longitude;
                  
                  $savebranch ->address = $request->address;
                  $savebranch ->address_ar = $request->address_ar;
                  $savebranch ->city_id = $request->city_id;
                  $savebranch ->parent_id = $id;
                  $savebranch ->mc_name = $request->mc_name;
                  $savebranch ->mc_name_ar = $request->mc_name_ar;
                  $savebranch ->mc_discription = $request->about;
                  $savebranch ->mc_discription_ar = $request->about_ar;
                  $savebranch ->city_id = $request->city_id;
                  $savebranch ->terms_condition_name = $orgname;
                   $savebranch ->terms_condition_name_ar = $orgname_ar;
                   
                if($savebranch->save()){
                  $id = $request->parent_id;

            if($sid==''){
             $sid = $savebranch->mc_id;
            }
            if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "تم حفظ معلومات المتجر بنجاح");
             }
             else
             {
             Session::flash('message', "Shop information successfully saved");
             }

                 return redirect()->route('abaya-shop-info',['id' => $id,'sid' =>$sid]);
                }   
            }

      //End shop info

	 
	 
   
   
    public function abayaPictures(Request $request)
     {
       
          if (Session::has('merchantid')) 
                        {
                            $merid  = Session::get('merchantid');
                            $parentid = $request->id;
                            $itemid = $request->sid;
                            $this->setLanguageLocaleMerchant();
                            $mer_id              = Session::get('merchantid');             
                            $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                            $merchantfooter     = view('sitemerchant.includes.merchant_footer');           
                            
                            $getDbC = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->count();
                            
                            $getVideos = DB::table('nm_category')->where('mc_id',$itemid)->where('vendor_id',$merid)->first();
                            if($getDbC >=1)
                            {
                          
                            $getDb = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->get();

                            }
                            else
                            {
                            $getDb = '';    
                            }

                          return view('sitemerchant.abaya.abaya-picture', compact('merchantheader','merchantfooter','getCatlists','GetAlreadyServices','getDb','getDbC','getVideos','parentid','itemid'));       
                        } else {
                            return Redirect::to('sitemerchant');
                    }
               }

       //Store abaya PICTURE
          //Store picture and  video url
      public function storePictureVideourl(Request $request)
      {
          
         if (Session::has('merchantid')) 
                {

                    $validatedData = $request->validate([
                          //'image' => 'required',
                         //'youtubevideo' => 'required',
                        // 'image.*' => 'image|mimes:jpg,jpeg,png,gif'
 
                    ]); 
 

                    $merid  = Session::get('merchantid');
                    $this->setLanguageLocaleMerchant();
                    $mer_id              = Session::get('merchantid');  
                    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              
                    //Insert images in folder
                          $catid = $request->parent_id;
                          $sid = $request->itemid;

                    if($files=$request->file('image')){
                    foreach($files as $file){
                    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
                    //$file->move('hallpics',$name); 
                    $extension = $file->getClientOriginalExtension();
                    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
                    //$file->move('hallpics', $fileName);               
                    $imageRealPath  =   $file->getRealPath();
                    $thumbName      =   'thumb_'. $fileName;       
                    $img = Image::make($imageRealPath); // use this if you want facade style code
                     $thumb_width = 100;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);

                  
                    $img->save('uploadimage/abaya/gallery' . '/'. $thumbName);
                    $file->move('uploadimage/abaya/gallery/', $fileName);
                    $shop_Img = url('/').'/uploadimage/abaya/gallery/'.$thumbName; 
                       
                    /*Insert data to db*/
                    DB::table('nm_category_gallery')->insert( [
                    'image'=>   $shop_Img,
                    'category_id' =>$sid,
                    'vendor_id' => $mer_id,
                    ]);
                   }  

                 } 
                   
                    $youtubevideoa = str_replace('youtu.be','www.youtube.com/embed',$request->youtubevideo);
                    $mc_video_description = $request->about_video;
                    $mc_video_description_ar = $request->about_video_ar;

                    


                    /*Update data to db*/
                    DB::table('nm_category')->where('mc_id',$sid)->where('vendor_id',$mer_id)->update( ['mc_video_url'=>$youtubevideoa,'mc_video_description'=> $mc_video_description,'mc_video_description_ar'=> $mc_video_description_ar]);


                    if(trim($mc_video_description) != '' || trim($mc_video_description_ar) != '' || trim($youtubevideoa) != '' || $request->file('image') !='')
                    {
                   
                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تمت إضافة الخدمة");
                    }
                    else
                    {
                    Session::flash('message', "Record saved successfully");
                    }
                    // language for display message //   

                    }
 

                  return Redirect::back();      
                } else {
                        return Redirect::to('sitemerchant');
                }
      }  

       // END STORE abaya PICTURE        
	 
   







    public function abayaCategory(Request $request)
     {
       
          if (Session::has('merchantid')) 
             {
                $parent_id = request()->id;
                $sid = request()->sid;
                $status = $request->status;
                $search = $request->search;
                $getPagelimit = config('app.paginate');
                $mer_id   = Session::get('merchantid');

 $menulist =  NmServicesAttribute::where('vendor_id',$mer_id)->where('services_id',$sid );


                  if($status!='')
                {
                  $menulist = $menulist->where('status',$status);
                }

               if($search !='')
                {      
                     $mc_name='attribute_title';
                     $mer_selected_lang_code = Session::get('mer_lang_code'); 
                   if($mer_selected_lang_code !='en')
                   {

                    
                    $menulist = $menulist->where('attribute_title_ar','LIKE','%' .$search.'%');
                   }
                                      
                  $menulist =$menulist->where('attribute_title','LIKE','%' .$search.'%');
                } 

               
 $menulist =  $menulist->orderBy('id', 'desc')->paginate($getPagelimit)->appends(request()->query());
              
                $this->setLanguageLocaleMerchant();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
            return view('sitemerchant.abaya.abaya-category', compact('merchantheader','merchantfooter','menulist','status','search','sid','parent_id','itemid'));       
            } else {
                return Redirect::to('sitemerchant');
            }  
          }


        

    public function abayaAddCategory(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $sid = $request->sid;
                $autoid = $request->autoid;
                 $datafirst = NmServicesAttribute::where('vendor_id',$mer_id)->where('id',$autoid)->first();
                 $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.abaya.abaya-add-catogory', compact('merchantheader','merchantfooter','id','getCities','getSinger','sid','autoid','datafirst'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 
        //Store buffet Category
   //Store Category
   public function storeabayaCategory(Request $request)

        {
             $mer_id   = Session::get('merchantid');
             $parent_id = $request->parent_id;
             $sid = $request->sid;
             $autoid = $request->autoid;
             if($autoid!=''){
             $categorysave = NmServicesAttribute::where('id',$autoid)->first();
             } else {  
             $categorysave = new NmServicesAttribute; 
             }
             $categorysave->attribute_title  = $request->category_name;
             $categorysave->attribute_title_ar   = $request->category_name_ar;
             $categorysave->vendor_id   = $mer_id;
             $categorysave->services_id   = $sid;
             $categorysave->parent   = $parent_id;
             $categorysave->status  = 1;
             if($categorysave->save()){ 
              // language for display message //
             if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "تم حفظ الفئة بنجاح");
             }
             else
             {
             Session::flash('message', "Category successfully saved");
             }
     // language for display message //
             
            return redirect()->route('abaya-category',['id' =>$parent_id,'sid' =>$sid]);
             //return redirect()->route('menulist',['hid' => $hid,'bid' =>$bid]);
             }


        }

   //End Category  




    public function abayaItemAddCategory(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.abaya.abaya-item-add-catogory', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 	 
	 
	 
    public function abayaItem(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $itemid = $request->sid;
                $catname = $request->catid;
                $status = $request->status;
                $search = $request->search;
                $maincatid =  $request->maincatid;
                $getPagelimit = config('app.paginate');
                  $productdata        = ProductsMerchant::where('pro_mc_id',$itemid)->where('pro_mr_id',$mer_id);
                
                if($status != '')
                {
                $productdata = $productdata->where('pro_status',$status); 
                }

                if($catname != '')
                {
                $productdata = $productdata->where('attribute_id',$catname); 
                }

                 if($maincatid != '')
                {
                $productdata = $productdata->where('parent_attribute_id',$maincatid); 
                }


              

               if($search !='')
                {
                  $mc_name='pro_title';
                  $mer_selected_lang_code = Session::get('mer_lang_code'); 
                  if($mer_selected_lang_code !='en')
                  {
                  $mc_name= 'pro_title'.$mer_selected_lang_code;
                  }                                         
                  $productdata = $productdata->where($mc_name,'LIKE',"%{$search}%");                     
                } 
                 


                $productdata = $productdata->orderBy('pro_id','desc')->paginate($getPagelimit)->appends(request()->query());

                foreach ($productdata as $prodidforproduct_option_value) {
                  # code...
                  $issizeavailbe = DB::table('nm_product_option_value')->where('product_id',$prodidforproduct_option_value->pro_id)->count();
                  if($issizeavailbe<1){
                    // DB::table('nm_product')->where('pro_id', $prodidforproduct_option_value->pro_id)->where('parent_attribute_id', '2')->update(['pro_status'=>0]);
                  }
                }
                 $attrcat            = NmServicesAttribute::where('vendor_id',$mer_id)->where('services_id',$itemid)->get();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
 
                return view('sitemerchant.abaya.abaya-item', compact('merchantheader','merchantfooter','id','productdata','status','search','attrcat','catname'));       
            } else {
                 return Redirect::to('sitemerchant');
            }

     }	
    public function abayaAddItem(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $sid = $request->sid;
                $autoid = $request->autoid;
                 $fetchfirstdata = ProductsMerchant::where('pro_id',$autoid)->where('pro_mr_id',$mer_id)->first();
                $category = NmServicesAttribute::where('vendor_id',$mer_id)->where('services_id',$sid)->where('status',1)->get();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');                 
                $getAllOptions = DB::table('nm_product_option_value')->where('vandor_id', $mer_id)->where('product_id', $autoid)->where('product_option_id','19')->get();
                  $Size = NmSize::where('type',2)->where('status',1)->get();
                $productfabric = NmFabrics::where('vendor_id',$mer_id)->where('status',1)->where('service_type',2)->get();

                $CheckSize = array();
                foreach($getAllOptions as $vals)
                {
                 $OT = $vals->option_title;
                  array_push($CheckSize, $OT);
                }
 $Oval = NmproductOptionValue::select('short_name')->where('product_option_id', 37)->where('product_id',$autoid)->where('vandor_id',$mer_id)->get()->toArray();

 
                $productGallery = NmProductGallery::where('vendor_id',$mer_id)->where('product_id',$autoid)->get();
 
                return view('sitemerchant.abaya.abaya-add-item', compact('merchantheader','merchantfooter','id','sid','category','fetchfirstdata','autoid','CheckSize','Size','productGallery','productfabric','Oval'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     	 }	  
	 
	 
   //Store abayaitem 
      public function storeabayaItem(Request $request)

            {
                 
            
                $merid  = Session::get('merchantid');
                $sid = $request->sid;

                $id = $request->parentid;
                $autoid = $request->autoid;

                $size = $request->size;
               $quantity = $request->mulquantity;

               if($autoid){
                  $savebranch =ProductsMerchant::where('pro_id',$autoid)->first();
                } else { 
                 $savebranch = new ProductsMerchant;
               }

                


 

               if($request->file('stor_img')){ 
                  $file = $request->stor_img;  
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                 $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 272;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('uploadimage/abaya' . '/'. $thumbName);
                  $file->move('uploadimage/abaya/', $Image);
                  $savebranch->pro_Img = url('').'/uploadimage/abaya/'.$thumbName;
                }



                 $parencat = $request->category;

                   if($parencat=='1') {
                    
                     $savebranch ->attribute_id = '42';
                     $savebranch ->pro_qty = '100000';
                   } else {
                     $savebranch ->attribute_id = '41';

                     //$savebranch ->pro_qty = $request->quantity;

                     
                   }

                  $savebranch ->pro_mr_id = $merid;
                  $savebranch ->parent_attribute_id = $request->category;
                 
                  $savebranch ->pro_price = $request->price;
                  $savebranch ->pro_discount_percentage = $request->discount;
                  
                   $discount = $request->discount;
                   $price = $request->price;
                  
                    if($discount!='' && $discount!='0') {
                     $discountprice =   $price-($price * ($discount/100));
                    } else {
                       $discountprice = 0;

                     }

                  $savebranch ->pro_mc_id = $sid;
                  $savebranch ->pro_title = $request->name;
                  $savebranch ->pro_title_ar = $request->name_ar;
                  $savebranch ->pro_no_of_purchase = $request->noofday;
                  
                  $savebranch ->deliver_day = $request->noofday;
                 $savebranch ->pro_disprice = $discountprice;

                 $savebranch ->pro_weight = $request->pro_weight;
       

                  $savebranch ->pro_desc = $request->description;
                  $savebranch ->pro_desc_ar = $request->description_ar;
                  $savebranch->pro_status  = 1;
                  $savebranch->save();
                  $getProID = $savebranch->pro_id;



 

                  $merid  = Session::get('merchantid');
                  $fabrics =  $request->fabric; 

                  NmproductOptionValue::where('product_option_id', 37)->where('product_id',$getProID)->where('vandor_id',$merid)->delete();
                  if(count($fabrics)>=1)
                  {


                    
                    foreach ($fabrics as $key=>$val) {  
                          $saveattr = new NmproductOptionValue;                    
                          $getinf = NmFabrics::find($val);
                          $saveattr->option_title = $getinf->fabric_name;
                          $saveattr->option_title_ar = $getinf->fabric_name_ar;
                          $saveattr->price =$getinf->price; 
                          $saveattr->image =$getinf->image; 
                          $saveattr->vandor_id =$getinf->vendor_id; 
                          $saveattr->product_id =$getProID;
                          $saveattr->product_option_id = 37;
                          $saveattr->short_name = $val;                          
                          $saveattr->save();
                    }
                  }

 

// FOR PRODUCT MULTIPLE IMAGES UPLOAD //
                     $files=$request->file('image');
                    $newFiles = array();
 

                $merid  = Session::get('merchantid');
                if($files=$request->file('image')){
                    foreach($files as $key=>$val){
                    array_push($newFiles, $key);
                    }

                    $privius=$request->privius;
                    $newFilesMatch = array();
                    if(isset($privius) && $privius!='')
                    {
                    foreach($privius as $key=>$val){
                    array_push($newFilesMatch, $key);
                    }  
                    }



                  $CheckInfo = array_intersect($newFiles, $newFilesMatch);

                  if(isset($privius) && $privius!='')
                  {

                  foreach($privius as $key=>$val){

                  if(in_array($key, $CheckInfo))
                  {
                  DB::table('nm_product_gallery')->where('id',$val)->where('vendor_id',$merid)->delete(); 
                  }

                  }
                  }
 

                 
                   
                    foreach($files as $file){
                    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
                     $extension = $file->getClientOriginalExtension();
                    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
                     $imageRealPath  =   $file->getRealPath();
                    $thumbName      =   'thumb_'. $fileName;       
                    $img = Image::make($imageRealPath); // use this if you want facade style code
                     $thumb_width = 272;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);                  
                    $img->save('uploadimage/abaya' . '/'. $thumbName);
                    $file->move('uploadimage/abaya/', $fileName);
                    $shop_Img = url('/').'/uploadimage/abaya/'.$thumbName; 
                     /*Insert data to db*/
                    DB::table('nm_product_gallery')->insert( [
                    'image'=>   $shop_Img,
                    'product_id' =>$getProID,
                    'status' => 1,
                    'vendor_id' => $merid,
                    ]);
                   }  
                } 


 
// FOR PRODUCT MULTIPLE IMAGES UPLOAD END //


                  $size = $request->size;

                   DB::table('nm_product_option_value')->where('product_option_id', '!=', 37)->where('vandor_id',$merid)->where('product_id',$getProID)->delete();
                  if(count($size) >=1)
                  {
                 


                    for($i=0;$i<count($size); $i++)
                    {
                        $Size = $size[$i];
                        $Quantity = $quantity[$i];
                       
                        if($Quantity !='')
                        {
                       DB::table('nm_product_option_value')->insert(['vandor_id' => $merid,'product_id' => $getProID, 'product_option_id' =>'19', 'option_title' => $Size, 'option_title_ar'=> $Size,'value'=>$Quantity]);
                        }
                    
                   }

                  }
                  
                 
            
            if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "تم حفظ العنصر بنجاح");
             }
             else
             {
             Session::flash('message', "Item successfully saved");
             }

                 return redirect()->route('abaya-item',['id' => $id,'sid' =>$sid]);
                }   
         

    //End abayaitem 

	
    public function abayaAddService(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.abaya.abaya-add-service', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 

  public function getabayaorderdetail(Request $request)
  {
     if(Session::has('merchantid')) 
     {
       $mer_id         = Session::get('merchantid');
       $id             = $request->id;
       $sid            = $request->sid;
       $opid           = $request->opid;
       $oid            = $request->oid;
       $cusid          = $request->cusid;
       $prodid         = $request->prodid;
       $merchantheader = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
       $merchantfooter = view('sitemerchant.includes.merchant_footer'); 
             
        $productdata = DB::table('nm_order_product')->where('product_type','shopping')->where('order_id',$oid)->where('product_sub_type','abaya')->where('cus_id',$cusid)->where('order_id',$oid)->orderBy('created_at','DESC')->get();
         foreach($productdata as $value)
         {
             $moredetail = DB::table('nm_order_body_measurement')->where('customer_id',$cusid)->where('order_id',$oid)->where('product_id',$value->product_id)->first();               
             $value->bookingdetail = $moredetail; 


$getShippingC = DB::table('nm_shipping')->where('ship_order_id',$oid)->where('cart_sub_type','abaya')->count();
          $value->shippingMethod = 'N/A';
          $value->shippingPrice = 'N/A';
          if($getShippingC >=1)
          { 
          $getShipping = DB::table('nm_shipping')->where('ship_order_id',$oid)->where('cart_sub_type','abaya')->first();
          $shipping_type = $getShipping->shipping_type;
          if($shipping_type == 1){ $shipping_type = 'Aramex';}else { $shipping_type = 'Pick Logistics'; }
          $shipping_price =  $getShipping->shipping_price;
          $value->shippingMethod = $shipping_type;
          $value->shippingPrice = $shipping_price;
          }


         }
        return view('sitemerchant.abaya.abayaorderdetail', compact('merchantheader','merchantfooter','id','sid','opid','oid','productdata','prodid','cusid'));       
      } 
      else 
      {
        return Redirect::to('sitemerchant');
      }
  }
    
	  public function abayaOrder(Request $request)
    {
        if(Session::has('merchantid')) 
        {
                 $subproducttype = 'abaya';
                 $mer_id         = Session::get('merchantid');
                 $id             = $request->id;
                 $hid            = $request->hid;
                 $sid            = $request->sid;
                 $searchkeyword  = $request->searchkeyword;
                 $date_to        = $request->date_to;
                 $End_Date       = $request->from_to;
                 $order_days     = $request->order_days;
                 $status         = $request->status;                
                 $serachfirstfrm = $request->serachfirstfrm;
                 $getorderedproducts = DB::table('nm_order_product')->where('product_type','shopping')->where('product_sub_type','abaya');
                  if($searchkeyword!='')
                  {
                     $getorderedproducts = $getorderedproducts->where('order_id',$searchkeyword);
                  }
                  if($status!='')
                  {
                     $getorderedproducts = $getorderedproducts->where('status',$status);
                  }
                  if($date_to!='' && $End_Date!='')
                  {
                    $getorderedproducts = $getorderedproducts->whereDate('created_at', '>=', $date_to)->whereDate('created_at', '<=', $End_Date);
                  } 
                  $getorderedproducts = $getorderedproducts->where('merchant_id',$mer_id)->groupBy('order_id')->orderBy('created_at','DESC')->selectRaw('*,sum(nm_order_product.total_price) as sum')->get();

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                return view('sitemerchant.abaya.abaya-order', compact('merchantheader','merchantfooter','id','hid','sid','mer_id','getorderedproducts'));       
            } 
            else 
            {
              return Redirect::to('sitemerchant');
            }
     }	 
	  
    


     public function abayaReview(Request $request)
     {
        if(Session::has('merchantid')) 
        {
                $mer_id          = Session::get('merchantid');
                $id              = $request->id;
                $getPagelimit    = config('app.paginate');
                $sid             = $request->sid;
                $merchantheader  = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter  = view('sitemerchant.includes.merchant_footer'); 
                $reviewrating    = Reviewrating::where('vandor_id',$mer_id)->where('shop_id',$sid)->orderBy('created_at','desc')->where('status',1)->paginate($getPagelimit)->appends(request()->query());
 
                return view('sitemerchant.abaya.abaya-reviews-and-comments', compact('merchantheader','merchantfooter','id','reviewrating'));       
         } 
         else 
         {
            return Redirect::to('sitemerchant');
         }
     }   	
	  public function abayaOffer(Request $request)
     {
       
       if (Session::has('merchantid')) 
             {   $getPagelimit = config('app.paginate');
                $id = $request->id;
                $sid = $request->sid;
                $autoid = $request->autoid;
                $status = $request->status;
                $search = $request->search;

             $mer_id   = Session::get('merchantid');
            $hallofferlist =  HallOffer::where('vendor_id',$mer_id)->where('pro_id',$sid);
            if($status!='')
            {
            $hallofferlist = $hallofferlist->where('status',$status);
            }
            if($search !='')
            {
            $mer_selected_lang_code = Session::get('mer_lang_code'); 
            if($mer_selected_lang_code !='en')
            {
            $hallofferlist = $hallofferlist->where('title_ar','LIKE','%' .$search.'%');
            }
            else
            {
            $hallofferlist = $hallofferlist->where('title','LIKE','%' .$search.'%');
 
            }
            }   
            $hallofferlist =  $hallofferlist->orderBy('id','DESC')->paginate($getPagelimit);

                $this->setLanguageLocaleMerchant();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
            return view('sitemerchant.abaya.abaya-offer', compact('merchantheader','merchantfooter','hallofferlist','status','autoid','sid','id','search'));       
            } else {
                return Redirect::to('sitemerchant');
            }
      }   	
	  public function abayaAddOffer(Request $request)
     {
        
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $getPagelimit = config('app.paginate');
                $sid = $request->sid;
                $autoid = $request->autoid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
               $reviewrating = Reviewrating::where('vandor_id',$mer_id)->paginate($getPagelimit)->appends(request()->query());
                $categorysave = HallOffer::where('id',$autoid)->where('vendor_id',$mer_id)->first(); 
 
                return view('sitemerchant.abaya.abaya-add-offer', compact('merchantheader','merchantfooter','id','sid','autoid','reviewrating','categorysave'));       
            } else {
                 return Redirect::to('sitemerchant');
            }

       
     }   	
   
   //STORE OFFER
       public function storeabayaOffer(Request $request)
       {
            if (Session::has('merchantid')) 
      {      
        $mer_id   = Session::get('merchantid');
        $id = $request->parentid;
        $sid = $request->sid;
       
        $autoid = $request->autoid;
        if($autoid =='')
        {
        $categorysave = new HallOffer; 
        }
        else
        {
        $categorysave = HallOffer::where('id',$autoid)->where('vendor_id',$mer_id)->first();
        }
        $categorysave->vendor_id    = $mer_id; 
        $categorysave->title        =$request->title;
        $categorysave->title_ar    = $request->title_ar;
        $categorysave->start_date     = $request->Date_from;
        $categorysave->end_date    = $request->date_to;
        $categorysave->discount   = $request->discount;
        $categorysave->coupon   = $request->coupon;
        $categorysave->pro_id   = $sid;
        $categorysave->status    = 1;
        if (\Config::get('app.locale') == 'ar'){
        Session::flash('message', "تمت إضافة العرض بنجاح  ");
        }
        else
        {
        Session::flash('message', "Offer successfully saved");
        }
        $categorysave->save(); 
        return redirect()->route('abaya-offer',['id' => $id,'sid' => $sid]);
       } else {
      return Redirect::to('sitemerchant');
      } 
       }
     //END OFFER    


///////////// update product status ////////////
       function getproductstatus(Request $request){
        $orderproductid=$request->orderproductid;
        $orderstatus=$request->orderstatus;

        $postedorderid=$request->postedorderid;


        //   if(isset($postedorderid) && $postedorderid>0){
  
        //     DB::table('nm_order_product')->where('order_id', $postedorderid)->update(['status' => $orderstatus]);
 
        // $array_output = array('status'    => 'approved', 'orderstatus'=>$orderstatus);
        //   }else{

        $getDb = DB::table('nm_order_product')->where('id', $orderproductid)->first();
        $order_id = $getDb->order_id;
        $merchant_id = $getDb->merchant_id;
        $product_sub_type = $getDb->product_sub_type;
        
        if($product_sub_type=='video' || $product_sub_type=='photography')
        {
         DB::table('nm_order_product')->where('product_sub_type', 'video')->where('order_id', $order_id)->where('merchant_id', $merchant_id)->update(['status' => $orderstatus]);
         DB::table('nm_order_product')->where('product_sub_type', 'photography')->where('order_id', $order_id)->where('merchant_id', $merchant_id)->update(['status' => $orderstatus]);

        }
        else
        {
           DB::table('nm_order_product')->where('product_sub_type', $product_sub_type)->where('order_id', $order_id)->where('merchant_id', $merchant_id)->update(['status' => $orderstatus]);

        } 
     

        $array_output = array('status'    => 'approved', 'orderstatus'=>$orderstatus);
        //}

        echo json_encode($array_output);
         exit;
       }
////////////////end produc status///////////////////


   
 } // end 

 
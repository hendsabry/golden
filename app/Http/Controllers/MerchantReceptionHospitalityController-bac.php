<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Models;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon;
use File;
use App\Reviewrating;
use App\Categorylist;
use App\City;
use Intervention\Image\ImageManagerStatic as Image; 
use Auth;
use Lang;
use App\Country;

 

class MerchantReceptionHospitalityController extends Controller
{
      public function __construct(){
            parent::__construct();
            // set admin Panel language
            $this->middleware(function ($request, $next) {
                        $this->setLanguageLocaleMerchant();
                        Session::get('mer_lang_code');
                        return $next($request);
           });
     }
    
 
//Add Shop info
    
    public function receptionAddShopinfo(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $city = City::where('ci_status',1)->get();
                $id = $request->id;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
           return view('sitemerchant.reception-add-shopinfo', compact('merchantheader','merchantfooter','id','city'));       
            } else {
                return Redirect::to('sitemerchant');
            }
     }
   
   
   


 //Shop Ajax

    public function receptionShopStatus(Request $request)
    {
     if ($request->ajax()) {
             $id = $request->id;
             $activestatus = $request->activestatus;
             if($activestatus==1) {
                $status = 0;
             } else {
               $status = 1;
             }
             $updatestatus = Categorylist::where('mc_id',$id)->first();
             $updatestatus->mc_status = $status;
              if($updatestatus->save()) {
                 echo 1;die;
              }  

         }
   }  
//Shop list
  public function receptionShopList(Request $request)

  {
            if (Session::has('merchantid')) 
             {

                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $fetchdata = '';
                $search = $request->search;
                $status = $request->status;
                 $getPagelimit = config('app.paginate');
                 if($search !='')
                {
                     $mc_name='mc_name';
                     $mer_selected_lang_code = Session::get('mer_lang_code'); 
                   if($mer_selected_lang_code !='en')
                   {
                             $mc_name= 'mc_name_ar';
                            
                   }

                  $alldata =   Categorylist::where('vendor_id',$mer_id)->where('nm_category.'.$mc_name,'like', '%' .$search. '%')->where('parent_id',$id)->orderBy('mc_id','DESC')->paginate($getPagelimit)->appends(request()->query()); 
                }
                 else if($status !='')
                {
                $alldata = Categorylist::where('vendor_id',$mer_id)->where('mc_status',$status)->where('parent_id',$id)->orderBy('mc_id','DESC')->paginate($getPagelimit)->appends(request()->query()); ;

                } else {
                       $alldata = Categorylist::where('vendor_id',$mer_id)->where('parent_id',$id)->orderBy('mc_id','DESC')->paginate($getPagelimit)->appends(request()->query()); ;

                }


              
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
           return view('sitemerchant.reception-shop-list', compact('merchantheader','merchantfooter','id','alldata'));       
            } else {
                return Redirect::to('sitemerchant');
            }


  }
     




    //Reception Sho Info
     public function receptionHospitality(Request $request)

     {
           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                 $parent_id = request()->id;
                 $mcid = request()->shopid;
                 $city = City::where('ci_status',1)->get();
                 
                $fetchdata = '';
                 $fetchdatacount = Categorylist::where('vendor_id',$mer_id)->where('parent_id',$parent_id)->where('mc_id',$mcid)->count();
                 if($fetchdatacount > 0) {
                 $fetchdata = Categorylist::where('vendor_id',$mer_id)->where('parent_id',$parent_id)->where('mc_id',$mcid)->first();
                 }

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
           return view('sitemerchant.reception-shop-info', compact('merchantheader','merchantfooter','parent_id','fetchdata','city'));       
            } else {
                return Redirect::to('sitemerchant');
            }


     }

     public function storeReceptionHospitality(Request $request)
     {
           if (Session::has('merchantid')) 
             {
                $id = request()->id;
                $mer_id   = Session::get('merchantid');
                $storeShopinfo =  new Categorylist;
                $storeShopinfo->mc_name = $request->mc_name;
                $storeShopinfo->mc_name_ar = $request->mc_name_ar;
                $storeShopinfo->google_map_address = $request->google_map_address;
                $storeShopinfo->vendor_id = $mer_id;
                $storeShopinfo->city_id = $request->city_id;

                $storeShopinfo->parent_id = $id;
                $storeShopinfo->mc_status = 1;


                //Insert images in folder
                   if($file=$request->file('mc_img'))
                    {          
                        $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());          
                        $extension = $file->getClientOriginalExtension();
                        $fileName = rand(11111,99999).'_'.time().'.'.$extension;                 
                        $imageRealPath  =   $file->getRealPath();
                        $thumbName      =   'thumb_'. $fileName;       
                        $img = Image::make($imageRealPath);
                        $img->resize(300,300);
                        $img->save('receptionspic' . '/small/'. $thumbName);
                        $file->move('receptionspic/', $fileName);            
                        $shopimage_Img = url('/').'/receptionspic/'.$fileName; 
                        $storeShopinfo->mc_img = $shopimage_Img;       
                     }

                     //Address image
                      //Insert images in folder
                   if($file=$request->file('address_image'))
                    {          
                        $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());          
                        $extension = $file->getClientOriginalExtension();
                        $fileName = rand(11111,99999).'_'.time().'.'.$extension;                 
                        $imageRealPath  =   $file->getRealPath();
                        $thumbName      =   'thumb_'. $fileName;       
                        $img = Image::make($imageRealPath);
                        $img->resize(300,300);
                        $img->save('receptionspic' . '/small/'. $thumbName);
                        $file->move('receptionspic/', $fileName);            
                        $address_Img = url('/').'/receptionspic/'.$fileName; 
                        $storeShopinfo->address_image = $address_Img;          
                     }

              $storeShopinfo->save();
                 
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                // language for display message //
                if (\Config::get('app.locale') == 'ar'){
                Session::flash('message', "تمت إضافة المتجر بنجاح");
                }
                else
                {
                Session::flash('message', "Shop successfully added");
                }
          // language for display message //  
            return redirect()->route('reception-shop-list',['id' => $id,]); 
         //  return view('sitemerchant.reception-shop-info', compact('merchantheader','merchantfooter','id'));       
            } else {
                return Redirect::to('sitemerchant');
            }
 
     }

     
     public function updateShopInfo(Request $request)

     {
             if (Session::has('merchantid')) 
             {
                
                 $parent_id = request()->id;
                 $mcid = request()->shopid;
                $mer_id   = Session::get('merchantid');
                $storeShopinfo = Categorylist::where('vendor_id',$mer_id)->where('parent_id',$parent_id)->where('mc_id',$mcid)->first();
                $storeShopinfo->mc_name = $request->mc_name;
                $storeShopinfo->mc_name_ar = $request->mc_name_ar;
                $storeShopinfo->google_map_address = $request->google_map_address;
                $storeShopinfo->vendor_id = $mer_id;
                $storeShopinfo->parent_id = $parent_id;
                $storeShopinfo->mc_id = $mcid;
                $storeShopinfo->city_id = $request->city_id;
                $storeShopinfo->mc_status = 1;


                //Insert images in folder
                   if($file=$request->file('mc_img'))
                    {          
                        $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());          
                        $extension = $file->getClientOriginalExtension();
                        $fileName = rand(11111,99999).'_'.time().'.'.$extension;                 
                        $imageRealPath  =   $file->getRealPath();
                        $thumbName      =   'thumb_'. $fileName;       
                        $img = Image::make($imageRealPath);
                        $img->resize(300,300);
                        $img->save('receptionspic' . '/small/'. $thumbName);
                        $file->move('receptionspic/', $fileName);            
                        $shopimage_Img = url('/').'/receptionspic/'.$fileName; 
                        $storeShopinfo->mc_img = $shopimage_Img;       
                     } else {
                         $storeShopinfo->mc_img = $request->updateimage;
                     }

                     //Address image
                      //Insert images in folder
                   if($file=$request->file('address_image'))
                    {          
                        $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());          
                        $extension = $file->getClientOriginalExtension();
                        $fileName = rand(11111,99999).'_'.time().'.'.$extension;                 
                        $imageRealPath  =   $file->getRealPath();
                        $thumbName      =   'thumb_'. $fileName;       
                        $img = Image::make($imageRealPath);
                        $img->resize(300,300);
                        $img->save('receptionspic' . '/small/'. $thumbName);
                        $file->move('receptionspic/', $fileName);            
                        $address_Img = url('/').'/receptionspic/'.$fileName; 
                        $storeShopinfo->address_image = $address_Img;          
                     } else {

                         $storeShopinfo->address_image = $request->update_add_img;
                     }

              $storeShopinfo->save();
                 
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                // language for display message //
                if (\Config::get('app.locale') == 'ar'){
                Session::flash('message', "متجر تم تحديثه بنجاح");
                }
                else
                {
                Session::flash('message', "Shop successfully Updated");
                }
          // language for display message //  
            return redirect()->route('reception-shop-list',['id' => $parent_id]); 
         //  return view('sitemerchant.reception-shop-info', compact('merchantheader','merchantfooter','id'));       
            } else {
                return Redirect::to('sitemerchant');
            }  

     }






           //Shop picture and Video Url
           public function receptionShopPicture(Request $request)

             { 

               if (Session::has('merchantid')) 
                        {
                            $merid  = Session::get('merchantid');
                              $catid = $request->id;
                             $shopid = $request->shopid;
                            $this->setLanguageLocaleMerchant();
                            $mer_id              = Session::get('merchantid');             
                            $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                            $merchantfooter     = view('sitemerchant.includes.merchant_footer');                
                             
                            $proid = $request->id;
                            $getDbC = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $shopid)->count();
                            
                            $getVideos = DB::table('nm_category')->where('mc_id',$shopid)->where('vendor_id',$merid)->first();

                            if($getDbC >=1)
                            {
                            $getDb = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $shopid)->get(); 
                            $getDb = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $shopid)->get();


                            }
                            else
                            {
                            $getDb = '';    
                            }


                          return view('sitemerchant.reception-shop-picture', compact('merchantheader','merchantfooter','getCatlists','GetAlreadyServices','getDb','getDbC','getVideos','shopid','catid'));       
                        } else {
                            return Redirect::to('sitemerchant');
                    }
             }


      //Store Hall Picture

        public function add_Reception_picture(Request $request)
    {

    if (Session::has('merchantid')) 
                {

                    $validatedData = $request->validate([
                          'image' => 'required',
                         'youtubevideo' => 'required',
                         'image.*' => 'image|mimes:jpg,jpeg,png,gif'
 
                    ]); 
 

                    $merid  = Session::get('merchantid');
                    $this->setLanguageLocaleMerchant();
                    $mer_id              = Session::get('merchantid');             
                    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              
                    //Insert images in folder
                 
                            $catid = $request->cat_id;
                            $shopid = $request->pro_id;
                           
                  
                    if($files=$request->file('image')){
                    foreach($files as $file){
                    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
                    //$file->move('hallpics',$name); 
                    $extension = $file->getClientOriginalExtension();
                    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
                    //$file->move('hallpics', $fileName);               
                    $imageRealPath  =   $file->getRealPath();
                    $thumbName      =   'thumb_'. $fileName;       
                    $img = Image::make($imageRealPath); // use this if you want facade style code
                    $thumb_width = 100;
                    list($width,$height) = getimagesize($imageRealPath);
                    $thumb_height = ($thumb_width/$width) * $height;
                    $img->resize($thumb_width,$thumb_height);                     
                    $img->save('receptionspic' . '/'. $thumbName);
                    $file->move('receptionspic/', $fileName);
                    $shop_Img = url('/').'/receptionspic/'.$fileName; 
                       
                    /*Insert data to db*/
                    DB::table('nm_category_gallery')->insert( [
                    'image'=>   $shop_Img,
                    'category_id' =>$shopid,
                    'vendor_id' => $merid,
                    ]);

                    $youtubevideoa = $request->youtubevideo;
                    if($youtubevideoa !=''){
                    /*Update data to db*/
                    DB::table('nm_category')->where('mc_id',$shopid)->where('vendor_id',$merid)->update( ['mc_video_url'=>  $youtubevideoa]);
                    } 

                    } 
                    
                    }
                      
                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تمت إضافة الخدمة");
                    }
                    else
                    {
                    Session::flash('message', "Picture successfully added");
                    }
                    // language for display message //   
                    return Redirect::back();      
                } else {
                        return Redirect::to('sitemerchant');
                }
              }

                public function deleteShopImage(Request $request)

                {
                        $id = $request->id;
                        $mer_id   = Session::get('merchantid'); 

                $GetPics = DB::table('nm_category_gallery')->where('id',$id)->where('vendor_id',$mer_id)->delete();
                    echo 1;die;  

                }



     //Attributes Listing

  public function receptionAttributes(Request $request)

     {
           if (Session::has('merchantid')) 
             {    $getPagelimit = config('app.paginate'); 
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $cid = $request->shopid;
               


                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $status = $request->status;
                $search = $request->search;
           $getAttr = DB::table('nm_services_attribute')->where('vendor_id', '=', $mer_id)->where('services_id', '=', $cid);

                if($search !='')
                {
                 $getAttr = $getAttr->where('attribute_title', 'like', '%'.$search.'%');         
                }
                if($status !='')  
                {  
                $getAttr = $getAttr->where('status', '=', $status);        
                }  

                $getAttr = $getAttr->orderBy('id','DESC')->paginate($getPagelimit);


              
           return view('sitemerchant.reception-list-attributes', compact('merchantheader','merchantfooter','id','getAttr'));       
            } else {
                return Redirect::to('sitemerchant');
            }


     }



     //Worker Listing

  public function receptionListWorkers(Request $request)

     {
           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                 $shopid = $request->shopid;
                 $countdata = DB::table('nm_service_staff_nationality')->count();
                 if($countdata) {
                $countryid = DB::table('nm_service_staff_nationality')->select('nation_id')->get()->toArray();
                foreach($countryid as $val ) {

                      $natid[]  = $val->nation_id;
                }
            } else
            {    $natid = array();
                  $countryid = '';
            }

                $countrylist  = Country::all();
                 $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
           return view('sitemerchant.reception-list-workers', compact('merchantheader','merchantfooter','id','countrylist','shopid','natid'));       
            } else {
                return Redirect::to('sitemerchant');
            }


     }
  //Store functionality
     public function storenationality(Request $request) 

     {       

           $this->validate($request,[
             'workers_nationality' => 'required'
             
            ]);


      if (Session::has('merchantid')) 
             {
             
             $countryname = $request->workers_nationality;

             $shopid = $request->shopid;
             
             $mer_id   = Session::get('merchantid');
              
              DB::table('nm_service_staff_nationality')->where('shop_id',$shopid)->where('vendor_id',$mer_id)->delete();
              foreach($countryname as $val) 
              {
                 // insert attributes into table
                DB::table('nm_service_staff_nationality')->insert( [
                'vendor_id' => $mer_id,
                'shop_id' =>$shopid,
                'nation_id' =>$val             
                ]);
              }
            
          // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "الجنسية بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Nationality successfully added");
                    }
                    // language for display message //   
                    return Redirect::back();      
                } else {
                        return Redirect::to('sitemerchant');
                }

     }


     //Information Listing

  public function receptionListInformations(Request $request)

     {
           if (Session::has('merchantid')) 
             {  $getPagelimit = config('app.paginate');
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $shopid = $request->shopid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
 
                $status = $request->status;
                $search = $request->search;
                $getAttr = DB::table('nm_product')->where('pro_mr_id', '=', $mer_id)->where('pro_mc_id', '=', $shopid)->where('pro_type', '=', 'reception_hospitality')->where('packege', '=', 'no');

                if($search !='')
                {
                 $getAttr = $getAttr->where('pro_title', 'like', '%'.$search.'%');         
                }
                if($status !='')  
                {  
                $getAttr = $getAttr->where('pro_status', '=', $status);        
                }  

                $getAttr = $getAttr->orderBy('pro_id','DESC')->paginate($getPagelimit);
 


           return view('sitemerchant.reception-list-informations', compact('merchantheader','merchantfooter','id','getAttr','shopid'));       
            } else {
                return Redirect::to('sitemerchant');
            }


     }


public function receptionAddcategory(Request $request)
{

           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $cid = $request->shopid;
                $itemid = $request->itemid;
        
            $getAttr = DB::table('nm_services_attribute')->where('vendor_id', '=', $mer_id)->where('services_id', '=', $cid)->where('id', '=', $itemid)->first();

 

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                return view('sitemerchant.reception-addcategory', compact('merchantheader','merchantfooter','id','getAttr'));       
            } else {
                return Redirect::to('sitemerchant');
            }


}




public function recptionUpdatecategory(Request $request)
{

           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');

                $id = $request->id;
                $cid = $request->cid;
                $itemid = $request->itemid;

                $categoryname = $request->category_name;
                $categoryname_ar = $request->category_name_ar;
                $categorydescription = $request->category_description;
                $categorydescription_ar = $request->category_description_ar;
                $status = 1;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');

                if($itemid =='')
                {
            // insert attributes into table
                DB::table('nm_services_attribute')->insert( [
                'attribute_title' => $categoryname,
                'attribute_title_ar' => $categoryname_ar,
                'attribute_description'=> $categorydescription,
                'attribute_description_ar'=>$categorydescription_ar, 
                'services_id' =>$cid,
                'parent' =>$id,
                'vendor_id' =>$mer_id, 
                'status' =>$status,            
                ]);
            // insert attributes into table


                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تم حفظ الفئة بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Category successfully saved");
                    }
                    // language for display message // 

                } 
                else
                {


                DB::table('nm_services_attribute')->where('id',$itemid)->where('services_id',$cid)->where('vendor_id',$mer_id)->update( ['attribute_title'=>  $categoryname,'attribute_title_ar'=>  $categoryname_ar,'attribute_description'=>  $categorydescription,'attribute_description_ar'=> $categorydescription_ar]);  


                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تم تحديث الفئة بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Category successfully updated");
                    }
                    // language for display message // 

                }




 
                return redirect()->route('reception-list-attributes', ['id' => $id,'cid' =>$cid]);

 
              //  return view('sitemerchant.reception-list-attributes', compact('merchantheader','merchantfooter','id'));       
            } else {
                return Redirect::to('sitemerchant');
            }


}


public function receptionAdditems()
{

           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              $alldata = array();
              $cid = request()->cid;
              $id = request()->id;
              $shopid = request()->shopid;             
               $getAttrcount = DB::table('nm_product')->where('pro_id',$cid)->where('pro_mr_id',$mer_id)->count();
               $getAttr = DB::table('nm_product')->where('pro_id',$cid)->where('pro_mr_id',$mer_id)->first();

              

            


            if(isset(request()->cid))
              {
                return view('sitemerchant.reception-additems', compact('merchantheader','merchantfooter','alldata','getAttr','shopid'));

              }else
              {
                $getAttr=0;
                return view('sitemerchant.reception-additems', compact('merchantheader','merchantfooter','alldata','getAttrcount','shopid'));
                
              }

            } else {
                return Redirect::to('sitemerchant');
            }
}
 
      //Package Listing

  public function receptionListPackages(Request $request)

     {if (Session::has('merchantid')) 
             {  $getPagelimit = config('app.paginate');
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $shopid = $request->shopid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
 
                $status = $request->status;
                $search = $request->search;
                $getAttr = DB::table('nm_product')->where('pro_mr_id', '=', $mer_id)->where('pro_mc_id', '=', $shopid)->where('pro_type', '=', 'reception_hospitality')->where('packege', '=', 'yes');

                if($search !='')
                {
                 $getAttr = $getAttr->where('pro_title', 'like', '%'.$search.'%');         
                }
                if($status !='')  
                {  
                $getAttr = $getAttr->where('pro_status', '=', $status);        
                }  

                $getAttr = $getAttr->orderBy('pro_id','DESC')->paginate($getPagelimit);
           return view('sitemerchant.reception-list-packages', compact('merchantheader','merchantfooter','id','getAttr','shopid'));       
            } else {
                return Redirect::to('sitemerchant');
            }


     }


     public function AddUpdatePackege(Request $request)
{

           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');

                $getAttr = DB::table('nm_product')->where('pro_mr_id', '=', $mer_id)->where('pro_mc_id', '=', $request->shopid)->where('pro_type', '=', 'reception_hospitality')->where('packege', '=', 'yes');

                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $cid = $request->cid;
                $pro_mc_id = $request->shopid;
                $no_of_staff = $request->no_of_staff;
                $pro_title = $request->packeg_name;
                $pro_title_ar = $request->packeg_name_ar;
                $workers_nationality =$request->workers_nationality;
                $pro_price = $request->price;             
                $pro_desc = $request->packegdescription;
                $pro_desc_ar = $request->packegdescription_ar;
                $pro_qty  = 999;
                $pro_status = 1;
                $pro_type = 'reception_hospitality';
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');
                $pro_Img = ''; 


                $itemid
     
                if($cid =='')
                {
            // insert attributes into table
                $packeg_id=DB::table('nm_product')->insertGetId( [
                'pro_title' => $pro_title,
                'pro_title_ar' => $pro_title_ar,
                'pro_desc'=> $pro_desc,
                'pro_desc_ar'=>$pro_desc_ar, 
                'pro_Img' =>$pro_Img,
                'pro_qty' =>$pro_qty, 
                'pro_mc_id' =>$pro_mc_id, 
                'pro_price' =>$pro_price, 
                'pro_mr_id' =>$mer_id, 
                'pro_status' =>$pro_status, 
                'pro_type' =>$pro_type,
                'packege' =>'yes',
                'no_of_staff' =>$no_of_staff,
                'staff_nationality'=>$workers_nationality
                 

                ]);


                if( $request->cid)
                {
                    $packeg_id=$request->cid;

                }
                else
                {
                    $packeg_id=$packeg_id;

                }
                    $count=DB::table('nm_product_to_packeg')->where('packege_id',$packeg_id)->count();
                    if($count>0)
                    {
                            DB::table('nm_product_to_packeg')->where('packege_id',$packeg_id)->delete();
                    }

                foreach ($getAttr as $key => $value) {

                        $item='item'. $value

                        if($request->$item)
                        {
                            $itemid = $request->$item


                              foreach ($getAttr as $key => $value) {



                                
                              }




                        }

                       
                    # code...
                }

            // insert attributes into table


                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تم حفظ العنصر بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Item successfully saved");
                    }
                    // language for display message // 

                } 
                else 
                {
                    if($pro_Img=='')  
                    {
                    DB::table('nm_product')->where('pro_id',$cid)->where('pro_type','reception_hospitality')->where('pro_mr_id',$mer_id)->update( ['pro_title'=>  $pro_title,'pro_title_ar'=>  $pro_title_ar,'pro_desc'=>  $pro_desc,'pro_desc_ar'=> $pro_desc_ar,'pro_mc_id'=> $pro_mc_id,'pro_price'=> $pro_price,'pro_status'=> $pro_status,'pro_type'=> $pro_type]);
                    }
                    else
                    {
                      DB::table('nm_product')->where('pro_id',$cid)->where('pro_type','reception_hospitality')->where('pro_mr_id',$mer_id)->update( ['pro_title'=>  $pro_title,'pro_title_ar'=>  $pro_title_ar,'pro_desc'=>  $pro_desc,'pro_desc_ar'=> $pro_desc_ar,'pro_Img'=> $pro_Img,'pro_mc_id'=> $pro_mc_id,'pro_price'=> $pro_price,'pro_status'=> $pro_status,'pro_type'=> $pro_type]);
                    }
 
                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تم تحديث العنصر بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Item successfully updated");
                    }
                    // language for display message // 

                }




 
                return redirect()->route('reception-list-packages', ['id' => $id,'shopid'=>$pro_mc_id]);

 
              //  return view('sitemerchant.reception-list-attributes', compact('merchantheader','merchantfooter','id'));       
            } else {
                return Redirect::to('sitemerchant');
            }

}


     public function addPackages(Request $request)

     {if (Session::has('merchantid')) 
             {  $getPagelimit = config('app.paginate');
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $cid = $request->cid;
                $shopid = $request->shopid;

                //$itemid = $request->itemid;
                $shopid = $request->shopid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
            $packeg=array();
            if($request->cid)
                {
               $packeg = DB::table('nm_product')->where('pro_mr_id', '=', $mer_id)->where('pro_mc_id', '=', $shopid)->where('pro_type', '=', 'reception_hospitality')->where('packege', '=', 'yes')->where('pro_id', '=', $cid)->first();
                }
           

                 $getAttr = DB::table('nm_services_attribute')->select('*')->where('vendor_id', '=', $mer_id)->where('services_id', '=', $shopid)->get();

                  $countdata = DB::table('nm_service_staff_nationality')->count();
                 if($countdata) {
                $countryid = DB::table('nm_service_staff_nationality')->select('nation_id')->get()->toArray();
                $natid=array();
                foreach($countryid as $val ) {
                      $natid[]  = $val->nation_id;
                }
                } else
                {    $natid = array();
                      $countryid = '';
                }
                 $countrylist  = Country::whereIn('co_id',$natid)->get();                

           return view('sitemerchant.reception_hospitality.reception-add-packages', compact('merchantheader','merchantfooter','id','getAttr','shopid','countrylist','items','packeg'));       
            } else {
                return Redirect::to('sitemerchant');
            }


     }


  public function addWorkersNationlity(Request $request)

     {
          if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              $alldata = array();
           return view('sitemerchant.reception-addworker-nationlity', compact('merchantheader','merchantfooter','alldata'));       
            } else {
                return Redirect::to('sitemerchant');
            }


     }






 //Order Listing

  public function receptionListOrders(Request $request)
     { 

if (Session::has('merchantid')) 
    {
    $merid  = Session::get('merchantid');
    $this->setLanguageLocaleMerchant();
    $mer_id              = Session::get('merchantid');             
    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
    $getCatlists = DB::table('nm_category')->where('parent_id','0')->get();
    $GetAlreadyServices = DB::table('nm_services')->where('stor_merchant_id',$merid)->get();


    $getDbC = DB::table('nm_product_option_value')->where('vandor_id', $merid)->where('product_option_id',2)->count(); 
    if($getDbC >=1)
    {
    $getDb = DB::table('nm_product_option_value')->where('vandor_id', $merid)->where('product_option_id',2)->get(); 
    }
    else
    {
    $getDb = '';  
    }


    return view('sitemerchant.reception-list-orders', compact('merchantheader','merchantfooter','getCatlists','GetAlreadyServices','getDb','getDbC'));       
    } else {
    return Redirect::to('sitemerchant');
    }








     }

     //Package Listing

  public function receptionListCommentsReview(Request $request)

     {            

if (Session::has('merchantid')) 
    { $getPagelimit = config('app.paginate');
    $merid  = Session::get('merchantid');
    $this->setLanguageLocaleMerchant();
    $mer_id              = Session::get('merchantid');             
    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
    $getId = request()->id;
    $shopId = request()->shopid;
    $reviewrating = Reviewrating::where('vandor_id',$mer_id)->where('product_id',$shopId)->paginate($getPagelimit);
    return view('sitemerchant.reception-listcommentsreview', compact('merchantheader','merchantfooter','reviewrating'));       
    } else {
    return Redirect::to('sitemerchant');
    }
 
     }






 //Store Value in shopinfo

      public function storeShopinfo(Request $request)
      {
           $serviceid = request()->id;
           


      }  




     //Add Attributes

       public function addAttributes(Request $request)

      {
           if (Session::has('merchantid')) 
             {
                $id = $request->id;
                $mer_id   = Session::get('merchantid');
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
           return view('sitemerchant.add-attribute', compact('merchantheader','merchantfooter'));       
            } else {
                return Redirect::to('sitemerchant');
            }


     }


     

 }



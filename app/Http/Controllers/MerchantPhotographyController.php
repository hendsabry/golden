<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Models;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon;
use File;
use App\Reviewrating;
use App\Categorylist;
use Intervention\Image\ImageManagerStatic as Image; 
use Auth;
use Lang;
use App\ProductsMerchant;
use App\City;
use App\HallOffer;
use App\NmServicesAttribute;
use App\NmProductGallery;
class MerchantPhotographyController extends Controller
{
      public function __construct(){
            parent::__construct();
            // set admin Panel language
            $this->middleware(function ($request, $next) {
                        $this->setLanguageLocaleMerchant();
                        Session::get('mer_lang_code');
                        return $next($request);
           });
     }


    public function PhotographyShopInfo(Request $request)
     { 
        if (Session::has('merchantid')) 
             {
               
               $mer_id   = Session::get('merchantid');
                $id = $request->id;
                 $itemid = $request->sid;
                 $fetchdata = array();
                $city = City::where('ci_con_id',10)->get();
                $getshopid = '';
                $fetchdatacount = Categorylist::where('mc_id',$itemid)->where('vendor_id',$mer_id)->count();
                if($fetchdatacount >0) {
               $fetchdata = Categorylist::where('mc_id',$itemid)->where('vendor_id',$mer_id)->first();
               //$getshopid = $fetchdata->mc_id;
              }

               
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getPagelimit = config('app.paginate');
                 
                return view('sitemerchant.photography.photography-shop-info', compact('merchantheader','merchantfooter','id','itemid','fetchdata','city')); 

                  

            } else {
                 return Redirect::to('sitemerchant');
            }
      }

      
      //End shop info
   
    public function PhotographyPictures(Request $request)
     {
        if (Session::has('merchantid')) 
                        {
                           $merid  = Session::get('merchantid');
                            $parentid = $request->id;
                            $itemid = $request->sid;
                            $this->setLanguageLocaleMerchant();
                            $mer_id              = Session::get('merchantid');             
                            $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                            $merchantfooter     = view('sitemerchant.includes.merchant_footer');           
                            
                            $getDbC = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->count();
                            
                            $getVideos = DB::table('nm_category')->where('mc_id',$itemid)->where('vendor_id',$merid)->first();
                            if($getDbC >=1)
                            {
                          
                            $getDb = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->get();

                            }
                            else
                            {
                            $getDb = '';    
                            }

                          return view('sitemerchant.photography.photography-picture', compact('merchantheader','merchantfooter','getCatlists','GetAlreadyServices','getDb','getDbC','getVideos','parentid','itemid'));  
        
                        } else {
                            return Redirect::to('sitemerchant');
                    }

              }
				//End Picture & Video 


public function PhotographyPackage(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $itemid = $request->sid;
                $catname = $request->catid;
                $status = $request->status;
                $search = $request->search;
                $pfor = $request->pfor;
                $getPagelimit = config('app.paginate');
                  $productdata        = ProductsMerchant::where('pro_mc_id',$itemid)->where('pro_mr_id',$mer_id);
            
                if($status != '')
                {
                $productdata = $productdata->where('pro_status',$status);
                }
                if($search != '')
                {
                    
                $productdata = $productdata->where('pro_title','like', '%'.$search.'%');
                }
                if($pfor != '')
                {
                    if($pfor ==0)
                    {
                      $productdata = $productdata->where('attribute_id','=', 142);   
                    }
                    else
                    {
                     $productdata = $productdata->where('attribute_id','!=', 142);   
                    }
               
                
                }

 


                $productdata = $productdata->orderBy('pro_id','desc')->paginate($getPagelimit)->appends(request()->query());
                 $attrcat            = NmServicesAttribute::where('vendor_id',$mer_id)->where('services_id',$itemid)->get();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
 
                return view('sitemerchant.photography.photography-package', compact('merchantheader','merchantfooter','id','productdata','status','search','attrcat','catname'));                
  

            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
		//End Photography Package
    

    public function PhotographyAddPackage(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $itemid = $request->itemid;
                $autoid = $request->autoid;
                $fetchfirstdata = ProductsMerchant::where('pro_id',$autoid)->where('pro_mr_id',$mer_id)->first();

                 $productcat =  NmServicesAttribute::where('vendor_id',$mer_id)->where('services_id',$itemid )->get();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 


                $CameraNumber =   DB::table('nm_product_attribute')->where('vendor_id', $mer_id)->where('product_id', $autoid)->where('attribute_title', 'Number of Cameras')->first();

                $PictureNumber =  DB::table('nm_product_attribute')->where('vendor_id', $mer_id)->where('product_id', $autoid)->where('attribute_title', 'Number of Pictures')->first();

               $VideoDuration = DB::table('nm_product_attribute')->where('vendor_id', $mer_id)->where('product_id', $autoid)->where('attribute_title', 'Duration of the video')->first();
 
                $productGallery = NmProductGallery::where('vendor_id',$mer_id)->where('product_id',$autoid)->get();

                
                 return view('sitemerchant.photography.photography-add-package', compact('merchantheader','merchantfooter','id','itemid','productcat','fetchfirstdata','autoid','CameraNumber','PictureNumber','VideoDuration','productGallery')); 
             
      
            } else {
                 return Redirect::to('sitemerchant');
            }
     }		  	
	
 
    
	  public function PhotographyOrder(Request $request)
     {
        if(Session::has('merchantid')) 
        {
                $mer_id         = Session::get('merchantid');
                $id             = $request->id;
                $hid            = $request->hid;
                $searchkeyword  = $request->searchkeyword;
                $date_to        = $request->date_to;
                $End_Date       = $request->from_to;
                $order_days     = $request->order_days;
                $status         = $request->status;                
                $serachfirstfrm = $request->serachfirstfrm;
                $getorderedproducts = DB::table('nm_order_product')->where('product_type','occasion')->whereIn('product_sub_type',['photography','video']);
                if($searchkeyword!='')
                {
                  $getorderedproducts = $getorderedproducts->where('order_id',$searchkeyword);
                }
                if($status!='')
                {
                  $getorderedproducts = $getorderedproducts->where('status',$status);
                }
                if($date_to!='' && $End_Date!='')
                {
                  $getorderedproducts = $getorderedproducts->whereDate('created_at', '>=', $date_to)->whereDate('created_at', '<=', $End_Date);
                }
                $getorderedproducts = $getorderedproducts->where('merchant_id',$mer_id)->groupBy('order_id')->orderBy('created_at','DESC')->selectRaw('*,sum(nm_order_product.total_price) as sum')->get();

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.photography.photography-order', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid','sid','mer_id','getorderedproducts'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 
	  
    
       public function getorderdetail(Request $request)
       {
         if(Session::has('merchantid')) 
         {
             $mer_id   = Session::get('merchantid');
             $id       = $request->id;
             $opid     = $request->opid;
             $oid      = $request->oid;
             $cusid    = $request->cusid;
             $hid      = $request->hid;
             $merchantheader = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
             $merchantfooter = view('sitemerchant.includes.merchant_footer');  

             $getPhotographyStudio = DB::table('nm_order_product')->where('product_type','occasion')->where('order_id',$oid)->whereIn('product_sub_type',['photography','video'])->where('cus_id',$cusid)->where('order_id',$oid)->orderBy('created_at','DESC')->get();    
             return view('sitemerchant.photography.photography-orderdetail', compact('merchantheader','merchantfooter','id','hid','sid','getPhotographyStudio'));       
            } 
            else 
            {
                 return Redirect::to('sitemerchant');
            }
  }


     public function PhotographyReview(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $getPagelimit = config('app.paginate');
                $sid = $request->sid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
               $reviewrating = Reviewrating::where('vandor_id',$mer_id)->where('shop_id',$sid)->orderBy('created_at','DESC')->paginate($getPagelimit)->appends(request()->query());
 
                return view('sitemerchant.photography.photography-reviews-and-comments', compact('merchantheader','merchantfooter','id','reviewrating'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }   	
	  public function PhotographyOffer(Request $request)
     {
        if (Session::has('merchantid')) 
             {
              $getPagelimit = config('app.paginate');
                  $id = $request->id;
                  $sid = $request->sid;
                  $autoid = $request->autoid;
                  $status = $request->status;
                  $search = $request->search;

             $mer_id   = Session::get('merchantid');
            $hallofferlist =  HallOffer::where('vendor_id',$mer_id)->where('pro_id',$sid);
            if($status!='')
            {
            $hallofferlist = $hallofferlist->where('status',$status);
            }
            if($search !='')
            {
            $mer_selected_lang_code = Session::get('mer_lang_code'); 
            if($mer_selected_lang_code !='en')
            {
            $hallofferlist = $hallofferlist->where('title_ar','LIKE','%' .$search.'%');
            }
            else
            {
            $hallofferlist = $hallofferlist->where('title','LIKE','%' .$search.'%');
 
            }
            }   
            $hallofferlist =  $hallofferlist->orderBy('id','DESC')->paginate($getPagelimit);

                $this->setLanguageLocaleMerchant();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
            return view('sitemerchant.photography.photography-offer',compact('merchantheader','merchantfooter','hallofferlist','status','autoid','sid','id','search')); 
                      
            } else {
                 return Redirect::to('sitemerchant');
            }
     }   	
	  public function PhotographyAddOffer(Request $request)
     {
        if (Session::has('merchantid')) 
             {
              $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $getPagelimit = config('app.paginate');
                $sid = $request->sid;
                $autoid = $request->autoid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
               $reviewrating = Reviewrating::where('vandor_id',$mer_id)->paginate($getPagelimit)->appends(request()->query());
                $categorysave = HallOffer::where('id',$autoid)->where('vendor_id',$mer_id)->first(); 
 
                return view('sitemerchant.photography.photography-add-offer', compact('merchantheader','merchantfooter','id','sid','autoid','reviewrating','categorysave'));  
 
                     
            } else {
                 return Redirect::to('sitemerchant');
            }
     }   	
   
   

    


 //Store shop Info
            public function storeMensShopInfo(Request $request)

            {
                $merid  = Session::get('merchantid');
                $parent_id = $request->parent_id;
                $itemid = $request->itemid;
               if($itemid){
                  $savebranch =Categorylist::where('mc_id',$itemid)->first();
                } else { 
                 $savebranch = new Categorylist;
               }
           if($request->file('branchimage')){ 
                  $file = $request->branchimage;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                 $img = Image::make($imageRealPath); // use this if you want facade style code
                   $thumb_width = 220;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('uploadimage/photography' . '/'. $thumbName);
                  $file->move('uploadimage/photography/', $Image);
                  $savebranch->mc_img = url('').'/uploadimage/photography/'.$thumbName;
                }
                //Addressimage
                if($request->file('address_image')){ 
                  $file = $request->address_image;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                  $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('uploadimage/photography' . '/'. $thumbName);
                  $file->move('uploadimage/photography/', $Image);
                  $savebranch->address_image = url('').'/uploadimage/photography/'.$thumbName;
                }
              //Termaand condition
               if(!empty($request->mc_tnc)){ 
                  $file = $request->mc_tnc;
                  $orgname =  $file->getClientOriginalName();
                  $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                  $extension = $file->getClientOriginalExtension();
                  $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                  $file->move('./public/assets/storeimage/', $filename_c);                      
                  $Cname =   url('').'/public/assets/storeimage/'.$filename_c;
                  $savebranch->terms_conditions =$Cname;
                  
               }
               else {
                    $orgname = $request->tmcvalue; 
                }

//Termaand condition
               if(!empty($request->mc_tnc_ar)){ 
                  $file = $request->mc_tnc_ar;
                    $orgname_ar =  $file->getClientOriginalName(); 
                  $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                  $extension = $file->getClientOriginalExtension();
                  $filename_ca = rand(11111,99999).'_'.time().'.'.$extension;                       
                  $file->move('./public/assets/storeimage/', $filename_ca);                      
                  $Cname_ar =   url('').'/public/assets/storeimage/'.$filename_ca;
                  $savebranch->terms_conditions_ar =$Cname_ar;
                  $savebranch ->terms_condition_name_ar = $orgname_ar;
               }
               else {
                    $orgname_ar = $request->tmcvalue_ar; 
                }

                  $savebranch ->longitude =  $request->longitude;
                  $savebranch ->latitude =  $request->latitude;
                  $savebranch ->mc_name = $request->mc_name;
                  $savebranch ->mc_name_ar = $request->mc_name_ar;
                  $savebranch ->vendor_id = $merid;
                  $savebranch ->google_map_address = $request->google_map_address;
                  $savebranch ->address = $request->address;
                  $savebranch ->address_ar = $request->address_ar;
                  $savebranch ->city_id = $request->city_id;
                  $savebranch ->parent_id = $parent_id;
                  $savebranch ->mc_discription = $request->about;
                  $savebranch ->mc_discription_ar = $request->about_ar;
                  $savebranch ->city_id = $request->city_id;
                  $savebranch ->terms_condition_name = $orgname;
                  $savebranch ->opening_time = $request->opening_time;
                  $savebranch ->closing_time = $request->closing_time;
                 // $savebranch ->home_visit_charge = $request->home_visit_charge;
                  /*$serviceabailable =  $request->service_available;
                  if(count($serviceabailable) ==2)
                  {
                    $serviceavil = 3;
                    $savebranch->service_availability = $serviceavil;
                  }
                  else
                  {
                     $serviceavil =  $serviceabailable[0];
                     $savebranch->service_availability = $serviceavil;
                  }*/
                 // echo $serviceabailable;die;
                  $savebranch ->mc_status = $request->mc_status;
                if($savebranch->save()){
                  $id = $request->parent_id;

            if($itemid==''){
            $itemid = $savebranch->mc_id;
            }
            if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "تم حفظ معلومات المتجر بنجاح");
             }
             else
             {
             Session::flash('message', "Shop information successfully saved");
             }

                 return redirect()->route('photography-shop-info',['id' => $id,'itmemid' =>$itemid]);
                }   
            }

      //End shop info

   //Store picture and  video url
      public function storePictureVideourl(Request $request)
      {
          
         if (Session::has('merchantid')) 
                {

                    $validatedData = $request->validate([
                          //'image' => 'required',
                         //'youtubevideo' => 'required',
                        // 'image.*' => 'image|mimes:jpg,jpeg,png,gif'
 
                    ]); 
 

                    $merid  = Session::get('merchantid');
                    $this->setLanguageLocaleMerchant();
                    $mer_id              = Session::get('merchantid');  
                    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              
                    //Insert images in folder
                          $catid = $request->parent_id;
                          $sid = $request->itemid;

                    if($files=$request->file('image')){
                    foreach($files as $file){
                    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
                    //$file->move('hallpics',$name); 
                    $extension = $file->getClientOriginalExtension();
                    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
                    //$file->move('hallpics', $fileName);               
                    $imageRealPath  =   $file->getRealPath();
                    $thumbName      =   'thumb_'. $fileName;       
                    $img = Image::make($imageRealPath); // use this if you want facade style code
                     $thumb_width = 100;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);

                  
                    $img->save('uploadimage/photography/gallery' . '/'. $thumbName);
                    $file->move('uploadimage/photography/gallery/', $fileName);
                    $shop_Img = url('/').'/uploadimage/photography/gallery/'.$thumbName; 
                       
                    /*Insert data to db*/
                    DB::table('nm_category_gallery')->insert( [
                    'image'=>   $shop_Img,
                    'category_id' =>$sid,
                    'vendor_id' => $mer_id,
                    ]);
                   }  

                 } 
                   
                    $youtubevideoa = str_replace('youtu.be','www.youtube.com/embed',$request->youtubevideo);
                    $mc_video_description = $request->about_video;
                    $mc_video_description_ar = $request->about_video_ar;


                    /*Update data to db*/
  DB::table('nm_category')->where('mc_id',$sid)->where('vendor_id',$mer_id)->update( ['mc_video_url'=>$youtubevideoa,'mc_video_description'=> $mc_video_description,'mc_video_description_ar'=> $mc_video_description_ar]);





                      // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تمت إضافة الخدمة");
                    }
                    else
                    {
                    Session::flash('message', "Record saved successfully");
                    }
                    // language for display message //   
                    return Redirect::back();      
                } else {
                        return Redirect::to('sitemerchant');
                }
      }

 

 //Store storephotographypackage
            public function storephotographypackage(Request $request)
              {
        
            if (Session::has('merchantid')) 
             {
               $merid  = Session::get('merchantid');           
                $id = $request->id;
                $sid = $request->sid;
                $autoid = $request->autoid;
                
              if($autoid){
                  $saveproduct =ProductsMerchant::where('pro_id',$autoid)->first();
                } else { 
                 $saveproduct = new ProductsMerchant;
               }

           if($request->file('proimage')){ 
                $file = $request->proimage;  
                $merid  = Session::get('merchantid');
                $extension = $file->getClientOriginalExtension();
                $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                $imageRealPath  =   $file->getRealPath();
                $thumbName      =   'thumb_'. $Image;       
                $img = Image::make($imageRealPath); // use this if you want facade style code
                $thumb_width = 473;
                list($width,$height) = getimagesize($imageRealPath);
                $thumb_height = ($thumb_width/$width) * $height;
                $img->resize($thumb_width,$thumb_height);
                $img->save('uploadimage/photography/package' . '/'. $thumbName);
                $file->move('uploadimage/photography/package/', $Image);
                $saveproduct->pro_Img = url('').'/uploadimage/photography/package/'.$thumbName;
                }

             
                  $saveproduct ->pro_title = $request->title;
                  $saveproduct ->pro_title_ar = $request->title_ar;
                  $saveproduct ->pro_mr_id = $merid;
                  $saveproduct ->attribute_id = $request->package_for;
                  $saveproduct ->pro_mc_id = $sid;
                  $saveproduct ->pro_desc = $request->description;
                  $saveproduct ->pro_desc_ar = $request->description_ar;
                  $saveproduct ->pro_price = $request->price;
                  $saveproduct ->pro_discount_percentage = $request->discount;


                  $price = $request->price;
                  $discount = $request->discount;

                   if($discount!='' && $discount!='0') {
                     $discountprice =   $price-($price * ($discount/100));
                    } else {
                       $discountprice = 0;

                    }
                   
                   $saveproduct ->pro_disprice =$discountprice;

                  $saveproduct ->pro_qty = $request->quantity;
                  if($autoid==''){
                  $saveproduct->pro_status  = 1;
                  }
                 $saveproduct->save();
                  if($autoid){
                  $product_id = $autoid;
                  }
                  else
                  {
                  $product_id = $saveproduct->pro_id;
                  }
                  $no_of_camra = $request->no_of_camra;
                  $no_of_picture = $request->no_of_picture;
                  $duration = $request->duration;

                  DB::table('nm_product_attribute')->where('product_id', $product_id)->delete();
                  if($duration=='')
                  {
                  
                  DB::table('nm_product_attribute')->insert(
                  ['product_id' => $product_id, 'vendor_id' => $merid, 'attribute_title_ar' => 'عدد الصور ', 'attribute_title' => 'Number of Pictures', 'value' => $no_of_picture]
                  ); 
                  }
                  else
                  {
                    DB::table('nm_product_attribute')->insert(
                  ['product_id' => $product_id, 'vendor_id' => $merid, 'attribute_title_ar' => 'مدة الفيديو ', 'attribute_title' => 'Duration of the video', 'value' => $duration]
                  );
                  }
                  
                   DB::table('nm_product_attribute')->insert(
                  ['product_id' => $product_id, 'vendor_id' => $merid, 'attribute_title_ar' => 'عدد الكاميرات  ', 'attribute_title' => 'Number of Cameras', 'value' => $no_of_camra]
                  );
 
                  if (\Config::get('app.locale') == 'ar'){
                   Session::flash('message', "تم حفظ الحزمة بنجاح ");
                   }
                   else
                   {
                   Session::flash('message', "Package successfully saved");
                   }

             
// FOR PRODUCT MULTIPLE IMAGES UPLOAD //
    $files=$request->file('image');
    $newFiles = array();
    $merid  = Session::get('merchantid');
    if($files=$request->file('image')){
    foreach($files as $key=>$val){
    array_push($newFiles, $key);
    }

    $privius=$request->privius;
    $newFilesMatch = array();
    if(isset($privius) && $privius!='')
    {
    foreach($privius as $key=>$val){
    array_push($newFilesMatch, $key);
    }  
    }

    $CheckInfo = array_intersect($newFiles, $newFilesMatch);
    if(isset($privius) && $privius!='')
    {
    foreach($privius as $key=>$val){
    if(in_array($key, $CheckInfo))
    {
    DB::table('nm_product_gallery')->where('id',$val)->where('vendor_id',$merid)->delete(); 
    }
    }
    } 
    foreach($files as $file){
    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
    $extension = $file->getClientOriginalExtension();
    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
    $imageRealPath  =   $file->getRealPath();
    $thumbName      =   'thumb_'. $fileName;       
    $img = Image::make($imageRealPath); // use this if you want facade style code
    $thumb_width = 473;
    list($width,$height) = getimagesize($imageRealPath);
    $thumb_height = ($thumb_width/$width) * $height;
    $img->resize($thumb_width,$thumb_height);                  
    $img->save('uploadimage/photography/package' . '/'. $thumbName);
    $file->move('uploadimage/photography/package/', $fileName);
    $shop_Img = url('/').'/uploadimage/photography/package/'.$thumbName; 
    /*Insert data to db*/
    DB::table('nm_product_gallery')->insert( [
    'image'=>   $shop_Img,
    'product_id' =>$product_id,
    'status' => 1,
    'vendor_id' => $merid,
    ]);
    }  
        } 


 
// FOR PRODUCT MULTIPLE IMAGES UPLOAD END //


            return redirect()->route('photography-package',['id' => $id,'sid' => $sid]);
                    
            } else {
                 return Redirect::to('sitemerchant');
            }
     }

      //End shop info

             //STORE OFFER
       public function storeMakeOffer(Request $request)
       {
            if (Session::has('merchantid')) 
        {      
        $mer_id   = Session::get('merchantid');
        $id = $request->parentid;
        $sid = $request->sid;
        $autoid = $request->autoid;
        if($autoid =='')
        {
        $categorysave = new HallOffer; 
        }
        else
        {
        $categorysave = HallOffer::where('id',$autoid)->where('vendor_id',$mer_id)->first();
        }
        $categorysave->vendor_id    = $mer_id; 
        $categorysave->title        =$request->title;
        $categorysave->title_ar    = $request->title_ar;
        $categorysave->start_date     = $request->Date_from;
        $categorysave->end_date    = $request->date_to;
        $categorysave->discount   = $request->discount;
        $categorysave->coupon   = $request->coupon;
        $categorysave->pro_id   = $sid;
        $categorysave->status    = 1;
        if (\Config::get('app.locale') == 'ar'){
        Session::flash('message', "تمت إضافة العرض بنجاح  ");
        }
        else
        {
        Session::flash('message', "Offer successfully saved");
        }
        $categorysave->save(); 
        return redirect()->route('photography-offer',['id' => $id,'sid' => $sid]);
       } else {
      return Redirect::to('sitemerchant');
      } 
       }
     //END OFFER  

   
 } // end 



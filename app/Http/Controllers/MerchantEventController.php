<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Models;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon;
use File;
use App\Reviewrating;
use App\Categorylist;
use Intervention\Image\ImageManagerStatic as Image; 
use Auth;
use Lang;
use App\ProductsMerchant;
use App\City;
use App\HallOffer; 
use App\NmProductGallery;

class MerchantEventController extends Controller
{
      public function __construct(){
            parent::__construct();
            // set admin Panel language
            $this->middleware(function ($request, $next) {
                        $this->setLanguageLocaleMerchant();
                        Session::get('mer_lang_code');
                        return $next($request);
           });
     }


    public function eventShopInfo(Request $request)
     { 
        if (Session::has('merchantid')) 
             {
              $mer_id   = Session::get('merchantid');
                 $parent_id = request()->id;
                 $mcid = request()->shopid;
                 $city = City::where('ci_status',1)->get();
                 
                $fetchdata = '';
                 $fetchdatacount = Categorylist::where('vendor_id',$mer_id)->where('parent_id',$parent_id)->where('mc_id',$mcid)->count();
                 if($fetchdatacount > 0) {
                 $fetchdata = Categorylist::where('vendor_id',$mer_id)->where('parent_id',$parent_id)->where('mc_id',$mcid)->first();
                 }

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
           return view('sitemerchant.event.event-shop-info', compact('merchantheader','merchantfooter','parent_id','fetchdata','city'));  
                   
            } else {
                 return Redirect::to('sitemerchant');
            }
      }
	  

      //Store shop Info
            public function storeeventShopInfo(Request $request)

            {
                $merid  = Session::get('merchantid');
                $sid = $request->sid;
                $itemid = $request->itemid;
               if($itemid){
                  $savebranch =Categorylist::where('mc_id',$itemid)->first();
                } else { 
                 $savebranch = new Categorylist;
               }
          if($request->file('branchimage')){ 
                  $file = $request->branchimage;  

                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                 $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 220;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('buffetimage' . '/'. $thumbName);
                  $file->move('buffetimage/', $Image);
                  $savebranch->mc_img = url('').'/buffetimage/'.$thumbName;
                }
                //Addressimage
                if($request->file('address_image')){ 
                  $file = $request->address_image;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                  $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('buffetimage' . '/'. $thumbName);
                  $file->move('buffetimage/', $Image);
                  $savebranch->address_image = url('').'/buffetimage/'.$thumbName;
                }
              //Termaand condition
               if(!empty($request->mc_tnc)){ 
                  $file = $request->mc_tnc;
                  $orgname =  $file->getClientOriginalName();
                  $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                  $extension = $file->getClientOriginalExtension();
                  $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                  $file->move('./public/assets/storeimage/', $filename_c);                      
                  $Cname =   url('').'/public/assets/storeimage/'.$filename_c;
                  $savebranch->terms_conditions =$Cname;
                  
               }
               else {
                    $orgname = $request->tmcvalue; 
               }
                  $savebranch ->vendor_id = $merid;
                  $savebranch ->google_map_address = $request->google_map_address;
                  $savebranch ->insurance_amount = $request->Insuranceamount;
                  $savebranch ->address = $request->address;
                  $savebranch ->address_ar = $request->address_ar;
                  $savebranch ->city_id = $request->city_id;
                  $savebranch ->parent_id = $sid;
                  $savebranch ->mc_name = $request->mc_name;
                  $savebranch ->mc_name_ar = $request->mc_name_ar;
                  $savebranch ->mc_discription = $request->about;
                  $savebranch ->mc_discription_ar = $request->about_ar;
                  $savebranch ->city_id = $request->city_id;
                  $savebranch ->terms_condition_name = $orgname;
                  $savebranch->mc_status  = 1;
                  $savebranch->branch_manager_id  =$request->manager;

                if($savebranch->save()){
                  $id = $request->parent_id;

if($itemid==''){
 $itemid = $savebranch->mc_id;
}
            if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "فرع حفظ بنجاح");
             }
             else
             {
             Session::flash('message', "Branch successfully saved");
             }

                 return redirect()->route('buffet-info',['id' => $id,'sid' =>$sid,'itmemid' =>$itemid]);
                }   
            }

      //End shop info

	 
	 
   
   
    public function eventPictures(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                 $merid  = Session::get('merchantid');
                            $catid = $request->id;
                             $shopid = $request->shopid; 


                            // CheckPoint for security by Himanshu //
                            $checkPoints = DB::table('nm_category')->where('mc_id',$shopid)->where('parent_id',$catid)->where('vendor_id',$merid)->count();
                            if($checkPoints < 1)
                            {
                            return Redirect::to('sitemerchant_dashboard');
                            }
                            // CheckPoint for security by Himanshu //


                            $this->setLanguageLocaleMerchant();
                            $mer_id              = Session::get('merchantid');             
                            $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                            $merchantfooter     = view('sitemerchant.includes.merchant_footer');           
                           
                            $proid = $request->id;
                            $getDbC = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $shopid)->count();
                            
                            $getVideos = DB::table('nm_category')->where('mc_id',$shopid)->where('vendor_id',$merid)->first();
                            if($getDbC >=1)
                            {
                            $getDb = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $shopid)->get(); 
                            $getDb = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $shopid)->get();


                            }
                            else
                            {
                            $getDb = '';    
                            }

                          return view('sitemerchant.event.event-picture', compact('merchantheader','merchantfooter','getCatlists','GetAlreadyServices','getDb','getDbC','getVideos','shopid','catid'));   
 
               
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 
    
	 
	 
    public function eventCategory(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.event.event-category', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 
	 
    public function eventAddCategory(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.event.event-add-catogory', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 

    public function eventItemAddCategory(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.event.event-item-add-catogory', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 	 
	 
	 
    public function eventItem(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.event.event-item', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 
	 
    public function eventAddItem(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.event.event-add-item', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }		 	 	  
	 
	 
    public function eventItemInformation(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $getPagelimit = config('app.paginate');
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $shopid = $request->shopid;
                $category = $request->category;



                            // CheckPoint for security by Himanshu //
                            $checkPoints = DB::table('nm_category')->where('mc_id',$shopid)->where('parent_id',$id)->where('vendor_id',$mer_id)->count();
                            if($checkPoints < 1)
                            {
                            return Redirect::to('sitemerchant_dashboard');
                            }
                            // CheckPoint for security by Himanshu //

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
 
                 $status = $request->status;
                $search = $request->search;
                $getAttr = DB::table('nm_product')->where('pro_mr_id', '=', $mer_id)->where('pro_mc_id', '=', $shopid)->where('packege', '=', 'no');
                  if($category!='')
                {
                   $getAttr = $getAttr->where('attribute_id',$category);

                }
                if($search !='')
                {
                 $getAttr = $getAttr->where('pro_title', 'like', '%'.$search.'%');         
                }
                if($status !='')  
                {  
                $getAttr = $getAttr->where('pro_status', '=', $status);        
                }  

                $getAttr = $getAttr->orderBy('pro_id','DESC')->paginate($getPagelimit);
 


           return view('sitemerchant.event.event-item-information', compact('merchantheader','merchantfooter','id','getAttr','shopid','category','search','status'));
 
          
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 
    public function eventService(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.event.event-service', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }		 	
	 
	 public function eventAddService(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.event.event-add-service', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }		 	  
	 
	 public function eventAddItemInformation(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                 $mer_id   = Session::get('merchantid');

                    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                    $alldata = array();
                    $cid = request()->cid;
                    $id = request()->id;
                    $shopid = request()->shopid;   




                    // CheckPoint for security by Himanshu //
                    $checkPoints = DB::table('nm_category')->where('mc_id',$shopid)->where('parent_id',$id)->where('vendor_id',$mer_id)->count();
                    if($checkPoints < 1)
                    {
                    return Redirect::to('sitemerchant_dashboard');
                    }
                    // CheckPoint for security by Himanshu //





                    $getAttrcount = DB::table('nm_product')->where('pro_id',$cid)->where('pro_mr_id',$mer_id)->count();
                    $getAttr = DB::table('nm_product')->where('pro_id',$cid)->where('pro_mr_id',$mer_id)->first();




$productGallery = NmProductGallery::where('vendor_id',$mer_id)->where('product_id',$cid)->get();

                    if(isset(request()->cid))
                    {
                    return view('sitemerchant.event.event-item-add-information', compact('merchantheader','merchantfooter','alldata','getAttr','shopid','productGallery'));

                    }else
                    {
                    $myArray = array('attribute_id'=>'');
                    $getAttr =(object) $myArray;
                    return view('sitemerchant.event.event-item-add-information', compact('merchantheader','merchantfooter','alldata','getAttrcount','shopid','getAttr','productGallery'));

                    }
      
            } else {
                 return Redirect::to('sitemerchant');
            }
     }		  
	 	 
    
	


    public function eventPackage(Request $request)
     {
        if (Session::has('merchantid')) 
             {
              $getPagelimit = config('app.paginate'); 
              $mer_id   = Session::get('merchantid');
              $id = $request->id;
              $cid = $request->shopid;
              // CheckPoint for security by Himanshu //
              $checkPoints = DB::table('nm_category')->where('mc_id',$cid)->where('parent_id',$id)->where('vendor_id',$mer_id)->count();
              if($checkPoints < 1)
              {
              return Redirect::to('sitemerchant_dashboard');
              }
              // CheckPoint for security by Himanshu //
 
              $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
              $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              $status = $request->status;
              $search = $request->search;
              $getAttr = DB::table('nm_services_attribute')->where('vendor_id', '=', $mer_id)->where('services_id', '=', $cid);

              if($search !='')
              {
              $getAttr = $getAttr->where('attribute_title', 'like', '%'.$search.'%');         
              }
              if($status !='')  
              {  
              $getAttr = $getAttr->where('status', '=', $status);        
              }  

              $getAttr = $getAttr->orderBy('id','DESC')->paginate($getPagelimit);


              
           return view('sitemerchant.event.event-package', compact('merchantheader','merchantfooter','id','getAttr')); 
 
                
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	




public function recptionUpdatecategory(Request $request)
{
           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');

                $id = $request->id;
                $cid = $request->cid;
                $itemid = $request->itemid;

                $categoryname = $request->category_name;
                $categoryname_ar = $request->category_name_ar;
                $categorydescription = $request->category_description;
                $categorydescription_ar = $request->category_description_ar;
                $status = 1;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');

                if($request->file('catimage')){ 
                  $file = $request->catimage;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                  $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                   $img->save('uploadimage/categoryimage' . '/'. $thumbName);
                   $file->move('uploadimage/categoryimage/', $Image);
                    $fileimage = url('').'/uploadimage/categoryimage/'.$thumbName;
                } else
                { 
                   $fileimage =  $request->updatecatimage;
                 
                 }

                if($itemid =='')
                {
            // insert attributes into table
                DB::table('nm_services_attribute')->insert( [
                'attribute_title' => $categoryname,
                'attribute_title_ar' => $categoryname_ar,
                'services_id' =>$cid,
                'parent' =>$id,
                'vendor_id' =>$mer_id, 
                'status' =>$status,
                'image' =>$fileimage,            
                ]);
            // insert attributes into table


                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تم حفظ الفئة بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Category successfully saved");
                    }
                    // language for display message // 

                } 
                else
                {


                DB::table('nm_services_attribute')->where('id',$itemid)->where('services_id',$cid)->where('vendor_id',$mer_id)->update( ['attribute_title'=>  $categoryname,'attribute_title_ar'=>  $categoryname_ar, 'image' => $fileimage]);  


                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تم تحديث الفئة بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Category successfully updated");
                    }
                    // language for display message // 

                }




 
                return redirect()->route('event-package', ['id' => $id,'cid' =>$cid]);

 
              //  return view('sitemerchant.reception-list-attributes', compact('merchantheader','merchantfooter','id'));       
            } else {
                return Redirect::to('sitemerchant');
            }


}


    public function eventAddPackage(Request $request)
     {
        if (Session::has('merchantid')) 
             {
               $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $cid = $request->shopid;
                $itemid = $request->itemid;
        


                            // CheckPoint for security by Himanshu //
                            $checkPoints = DB::table('nm_category')->where('mc_id',$cid)->where('parent_id',$id)->where('vendor_id',$mer_id)->count();
                            if($checkPoints < 1)
                            {
                            return Redirect::to('sitemerchant_dashboard');
                            }
                            // CheckPoint for security by Himanshu //




            $getAttr = DB::table('nm_services_attribute')->where('vendor_id', '=', $mer_id)->where('services_id', '=', $cid)->where('id', '=', $itemid)->first();

 

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                return view('sitemerchant.event.event-add-package', compact('merchantheader','merchantfooter','id','getAttr'));
             } else {
                 return Redirect::to('sitemerchant');
            }
     }		  	
	
    public function eventWorker(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.event.event-worker', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 
    public function eventAddWorker(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.event.event-add-worker', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 
	  public function eventOrder(Request $request)
     {
        if(Session::has('merchantid')) 
        {
                $mer_id         = Session::get('merchantid');
                $id             = $request->id;
                $hid            = $request->hid;
                $searchkeyword  = $request->searchkeyword;
                $date_to        = $request->date_to;
                $End_Date       = $request->from_to;
                $order_days     = $request->order_days;
                $status         = $request->status;                
                $serachfirstfrm = $request->serachfirstfrm;
                $getorderedproducts = DB::table('nm_order_product')->where('product_type','occasion')->where('product_sub_type','events');
                if($searchkeyword!='')
                {
                  $getorderedproducts = $getorderedproducts->where('order_id',$searchkeyword);
                }
                if($status!='')
                {
                  $getorderedproducts = $getorderedproducts->where('status',$status);
                }
                if($date_to!='' && $End_Date!='')
                {
                  $getorderedproducts = $getorderedproducts->whereDate('created_at', '>=', $date_to)->whereDate('created_at', '<=', $End_Date);
                }
                $getorderedproducts = $getorderedproducts->where('merchant_id',$mer_id)->groupBy('order_id')->orderBy('created_at','DESC')->selectRaw('*,sum(nm_order_product.total_price) as sum')->get();



                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.event.event-order', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid','sid','mer_id','getorderedproducts'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 
	  
    

      public function getorderdetail(Request $request)
      {
       if(Session::has('merchantid')) 
       {
          $mer_id         = Session::get('merchantid');
          $id             = $request->id;
          $shopid         = $request->shopid;
          $opid           = $request->opid;
          $prodid         = $request->prodid;
          $cusid          = $request->cusid;
          $oid            = $request->oid;
          $merchantheader = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter = view('sitemerchant.includes.merchant_footer');
          $productdata = DB::table('nm_order_product')->where('product_type','occasion')->where('order_id',$oid)->where('product_sub_type','events')->where('cus_id',$cusid)->where('order_id',$oid)->orderBy('created_at','DESC')->get();

   foreach($productdata as $value)
         {            

          $getShippingC = DB::table('nm_shipping')->where('ship_order_id',$oid)->where('cart_sub_type','events')->count();
          $value->shippingMethod = 'N/A';
          $value->shippingPrice = '0';
          if($getShippingC >=1)
          { 
          $getShipping = DB::table('nm_shipping')->where('ship_order_id',$oid)->where('cart_sub_type','events')->first();
          $shipping_type = $getShipping->shipping_type;
          if($shipping_type == 1){ $shipping_type = 'Aramex';}else { $shipping_type = 'Pick Logistics'; }
          $shipping_price =  $getShipping->shipping_price;
          $value->shippingMethod = $shipping_type;
          $value->shippingPrice = $shipping_price;
          }
         }
 



         return view('sitemerchant.event.event-orderdetail', compact('merchantheader','merchantfooter','id','hid','sid','productdata'));       
        } else {
             return Redirect::to('sitemerchant');
        }
  }



     public function eventReview(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $getPagelimit = config('app.paginate');
                $shopid = $request->shopid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
               $reviewrating = Reviewrating::where('vandor_id',$mer_id)->where('shop_id',$shopid)->orderBy('created_at','desc')->paginate($getPagelimit)->appends(request()->query());
 
                return view('sitemerchant.event.event-reviews-and-comments', compact('merchantheader','merchantfooter','id','reviewrating'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }   	
	  public function eventOffer(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $getPagelimit = config('app.paginate');
                $id = $request->id;
                $sid = $request->shopid;
                $autoid = $request->autoid;
                $status = $request->status;
                $search = $request->search;

             $mer_id   = Session::get('merchantid');
            $hallofferlist =  HallOffer::where('vendor_id',$mer_id)->where('pro_id',$sid);
            if($status!='')
            {
            $hallofferlist = $hallofferlist->where('status',$status);
            }
            if($search !='')
            {
            $mer_selected_lang_code = Session::get('mer_lang_code'); 
            if($mer_selected_lang_code !='en')
            {
            $hallofferlist = $hallofferlist->where('title_ar','LIKE','%' .$search.'%');
            }
            else
            {
            $hallofferlist = $hallofferlist->where('title','LIKE','%' .$search.'%');
 
            }
            }   
            $hallofferlist =  $hallofferlist->orderBy('id','DESC')->paginate($getPagelimit);

                $this->setLanguageLocaleMerchant();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
            return view('sitemerchant.event.event-offer', compact('merchantheader','merchantfooter','hallofferlist','status','autoid','sid','id','search'));   
       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }   	
	  public function eventAddOffer(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                 $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $getPagelimit = config('app.paginate');
                $sid = $request->shopid;
                $autoid = $request->autoid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
           
                $categorysave = HallOffer::where('id',$autoid)->where('vendor_id',$mer_id)->first(); 
 
                return view('sitemerchant.event.event-add-offer', compact('merchantheader','merchantfooter','id','sid','autoid','categorysave')); 
 
                       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }   	
   
   

     public function savebeautyservice(Request $request)
        {    
          

              $merid  = Session::get('merchantid');
                if($request->mc_img){ 
                $file             = $request->mc_img; 
                $extension = $file->getClientOriginalExtension();
                $Adressimgname = rand(11111,99999).'_'.time().'.'.$extension;
                //$file->move('hallpics', $fileName);               
                $imageRealPath  =   $file->getRealPath();
                $thumbName      =   'thumb_'. $Adressimgname;       
                $img = Image::make($imageRealPath); // use this if you want facade style code
 
                $thumb_width = 100;
                list($width,$height) = getimagesize($imageRealPath);
                $thumb_height = ($thumb_width/$width) * $height;
                $img->resize($thumb_width,$thumb_height);
 
                 $img->save('beautyupload' . '/'. $thumbName);
                $file->move('beautyupload/', $Adressimgname);
                $Adressimgnames = url('').'/beautyupload/'.$thumbName;
                }
                else
                {
                $Adressimgnames = $request->mcimg; 
                }
                 if($request->hid!='')
                 {   
                    $pid = $request->hid;  
                    $saveservice = Categorylist::where('mc_id',$pid)->where('vendor_id',$merid)->first();
                 }
                 else
                 {
                   $saveservice = new Categorylist; 
                 }
                
                $saveservice ->parent_id = $request->parent_id;
                $saveservice ->mc_name = $request->mc_name;
                $saveservice ->mc_name_ar = $request->mc_name_ar;
                $saveservice ->mc_discription = $request->description;
                $saveservice ->mc_discription_ar = $request->description_ar;
                $saveservice ->vendor_id = $merid;
                $saveservice ->mc_img = $Adressimgnames; 
                $saveservice->mc_status = 1;
                if($saveservice->save()){
                 $id = $request->parent_id;
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تمت إضافة الفندق بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Shop successfully saved");
                    }
              
                }
             return redirect()->route('spa-shop',['id' => $id]);
            
        }



     public function updateShopInfo(Request $request)

     {
            
          

                if (Session::has('merchantid')) 
                {

                $parent_id = request()->id;
                $mcid = $request->shopid;
                $mer_id   = Session::get('merchantid');
                if($mcid=='')
                {
                $storeShopinfo =  new Categorylist; 
                }
                else
                {
                $storeShopinfo = Categorylist::where('vendor_id',$mer_id)->where('parent_id',$parent_id)->where('mc_id',$mcid)->first(); 

                }

                $storeShopinfo->mc_name = $request->mc_name;
                $storeShopinfo->mc_name_ar = $request->mc_name_ar;
                $storeShopinfo->google_map_address = $request->google_map_address;
                $storeShopinfo->vendor_id = $mer_id;
                $storeShopinfo->parent_id = $parent_id;

                $storeShopinfo->city_id = $request->city_id;
               $storeShopinfo ->mc_status = $request->mc_status;
                $storeShopinfo->address = $request->address;
                $storeShopinfo->address_ar = $request->address_ar;

                $storeShopinfo->mc_discription = $request->about;
                $storeShopinfo->mc_discription_ar = $request->about_ar;

                if(!empty($request->mc_tnc)){ 

                $file = $request->mc_tnc;
                $TNCname =   $file->getClientOriginalName();  

                $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                $extension = $file->getClientOriginalExtension();
                $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                $file->move('./receptionspic/', $filename_c);                      
                $Tname =   url('').'/receptionspic/'.$filename_c;

                $storeShopinfo->terms_condition_name = $TNCname;

                }
                else{
                $Tname = $request->updatemc_tnc;

                } 


                if(!empty($request->mc_tnc_ar)){ 

                $file = $request->mc_tnc_ar;
                $TNCname_ar =   $file->getClientOriginalName();  

                $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                $extension = $file->getClientOriginalExtension();
                $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                $file->move('./receptionspic/', $filename_c);                      
                $Tname_ar =   url('').'/receptionspic/'.$filename_c;

                $storeShopinfo->terms_condition_name_ar = $TNCname_ar;

                }
                else{
                $Tname_ar = $request->updatemc_tnc_ar;

                } 

                $storeShopinfo ->longitude =  $request->longitude;
                  $storeShopinfo ->latitude =  $request->latitude;
                $storeShopinfo->terms_conditions_ar = $Tname_ar;
                $storeShopinfo->terms_conditions = $Tname;

                //Insert images in folder
                if($file=$request->file('mc_img'))
                {          
                $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());          
                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111,99999).'_'.time().'.'.$extension;                 
                $imageRealPath  =   $file->getRealPath();
                $thumbName      =   'thumb_'. $fileName;       
                $img = Image::make($imageRealPath);


                $thumb_width = 150;
                list($width,$height) = getimagesize($imageRealPath);
                $thumb_height = ($thumb_width/$width) * $height;
                $img->resize($thumb_width,$thumb_height);

                $img->save('receptionspic' . '/'. $thumbName);
                $file->move('receptionspic/', $fileName);            
                $shopimage_Img = url('/').'/receptionspic/'.$thumbName; 
                $storeShopinfo->mc_img = $shopimage_Img;       
                } else {
                $storeShopinfo->mc_img = $request->updateimage;
                }

                //Address image
                //Insert images in folder
                if($file=$request->file('address_image'))
                {          
                $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());          
                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111,99999).'_'.time().'.'.$extension;                 
                $imageRealPath  =   $file->getRealPath();
                $thumbName      =   'thumb_'. $fileName;       
                $img = Image::make($imageRealPath);



                $thumb_width = 150;
                list($width,$height) = getimagesize($imageRealPath);
                $thumb_height = ($thumb_width/$width) * $height;
                $img->resize($thumb_width,$thumb_height);



                $img->save('receptionspic' . '/'. $thumbName);
                $file->move('receptionspic/', $fileName);            
                $address_Img = url('/').'/receptionspic/'.$thumbName; 
                $storeShopinfo->address_image = $address_Img;          
                } else {

                $storeShopinfo->address_image = $request->update_add_img;
                }

                $storeShopinfo->save();

                if($mcid=='')
                {
                $shopid = $storeShopinfo->mc_id;
                }
                else
                {
                $shopid = $mcid; 
                }


                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                // language for display message //
                if (\Config::get('app.locale') == 'ar'){
                Session::flash('message', "متجر تم تحديثه بنجاح");
                }
                else
                {
                Session::flash('message', "Shop successfully Updated");
                }
return redirect()->route('event-shop-info',['id' => $parent_id,'shopid' => $shopid]); 
        
            } else {
                return Redirect::to('sitemerchant');
            }  

     }

    //Store Hall Picture

        public function add_event_picture(Request $request)
    {

    if (Session::has('merchantid')) 
                {

                   
 

                    $merid  = Session::get('merchantid');
                    $this->setLanguageLocaleMerchant();
                    $mer_id              = Session::get('merchantid');             
                    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              
                    //Insert images in folder
                 
                            $catid = $request->cat_id;
                            $shopid = $request->pro_id;
                           
                         if($files=$request->file('image')){
                    foreach($files as $file){
                    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
                    //$file->move('hallpics',$name); 
                    $extension = $file->getClientOriginalExtension();
                    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
                    //$file->move('hallpics', $fileName);               
                    $imageRealPath  =   $file->getRealPath();
                    $thumbName      =   'thumb_'. $fileName;       
                    $img = Image::make($imageRealPath); // use this if you want facade style code
                  

                    $thumb_width = 100;
                    list($width,$height) = getimagesize($imageRealPath);
                    $thumb_height = ($thumb_width/$width) * $height;
                    $img->resize($thumb_width,$thumb_height);

                    $img->save('receptionspic' . '/'. $thumbName);
                    $file->move('receptionspic/', $fileName);
                    $shop_Img = url('/').'/receptionspic/'.$thumbName; 
                    
                        /*Insert data to db*/
                    DB::table('nm_category_gallery')->insert( [
                    'image'=>   $shop_Img,
                    'category_id' =>$shopid,
                    'vendor_id' => $merid,
                    ]);
                        } 

                       }

                    $youtubevideoa = str_replace('youtu.be','www.youtube.com/embed',$request->youtubevideo);


                      $mc_video_description = $request->about_video;
                     $mc_video_description_ar = $request->about_video_ar;
                    
                    /*Update data to db*/
                    DB::table('nm_category')->where('mc_id',$shopid)->where('vendor_id',$merid)->update( ['mc_video_url'=>  $youtubevideoa,'mc_video_description'=>  $mc_video_description,'mc_video_description_ar'=>  $mc_video_description_ar]);
                   

                    
                    
                   
                      
                    // language for display message //
                    if($mc_video_description !='' || $mc_video_description_ar !='' || $request->file('image') !='' )
                    {
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تمت إضافة الخدمة");
                    }
                    else
                    {
                    Session::flash('message', "Record successfully saved");
                    }
                  }
                    // language for display message //   
                    return Redirect::back(); 

                } else {
                        return Redirect::to('sitemerchant');
                }
              }

                public function deleteShopImage(Request $request)

                {
                        $id = $request->id;
                        $mer_id   = Session::get('merchantid'); 

                $GetPics = DB::table('nm_category_gallery')->where('id',$id)->where('vendor_id',$mer_id)->delete();
                    echo 1;die;  

                }


public function eventUpdateitem(Request $request)
{

           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = request()->id;
                $cid = $request->cid;
                $pro_mc_id = request()->shopid; 
                $attribute_id = $request->attribute;
                $pro_title = $request->itemname;
                $pro_title_ar = $request->itemname_ar;
                $pro_price = $request->price;
               $pro_weight = $request->pro_weight;
                $about = $request->about;
                $about_ar = $request->about_ar;
                $discount = $request->discount;

                 if($discount!='' && $discount!='0') {
                     $discountprice =   $pro_price-($pro_price * ($discount/100));
                    } else {
                       $discountprice = 0;

                     }

                //$pro_desc = $request->itemdescription;
                //$pro_desc_ar = $request->itemdescription_ar;
                $pro_qty  = $request->quantity;
                $pro_status = 1;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');
                $pro_Img = '';


  if($file=$request->file('uploadimage'))
      {          
          $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());          
          $extension = $file->getClientOriginalExtension();
          $fileName = rand(11111,99999).'_'.time().'.'.$extension;                 
          $imageRealPath  =   $file->getRealPath();
          $thumbName      =   'thumb_'. $fileName;       
          $img = Image::make($imageRealPath);
          

                    $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                    $thumb_height = ($thumb_width/$width) * $height;
                    $img->resize($thumb_width,$thumb_height);

          $img->save('receptionspic' . '/'. $thumbName);
          $file->move('receptionspic/', $fileName);            
          $pro_Img = url('/').'/receptionspic/'.$thumbName;        
       }

     
                if($cid =='')
                {
            // insert attributes into table
                DB::table('nm_product')->insert( [
                'pro_title' => $pro_title,
                'pro_title_ar' => $pro_title_ar,
               //'pro_desc'=> $pro_desc,
                //'pro_desc_ar'=>$pro_desc_ar, 
                'pro_Img' =>$pro_Img,
                'pro_qty' =>$pro_qty, 
                'pro_mc_id' =>$pro_mc_id, 
                'attribute_id' =>$attribute_id, 
                'pro_price' =>$pro_price, 
                'pro_mr_id' =>$mer_id, 
                'pro_status' =>$pro_status, 
                'pro_desc' =>$about,
                'pro_desc_ar' =>$about_ar,
                'pro_discount_percentage' =>$discount,
                'pro_disprice' =>$discountprice,
                'pro_weight' =>$pro_weight,



                      
                ]);
            // insert attributes into table

                  $cid = DB::getPdo()->lastInsertId();
                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تم حفظ العنصر بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Item successfully saved");
                    }
                    // language for display message // 

                } 
                else 
                {
                    if($pro_Img=='')  
                    {
                    DB::table('nm_product')->where('pro_id',$cid)->where('pro_mr_id',$mer_id)->update( ['pro_title'=>  $pro_title,'pro_title_ar'=>  $pro_title_ar,'pro_mc_id'=> $pro_mc_id,'pro_price'=> $pro_price,'pro_status'=> $pro_status,'pro_desc' =>$about,'pro_desc_ar' =>$about_ar,'pro_qty' =>$pro_qty,'attribute_id' =>$attribute_id,'pro_discount_percentage' =>$discount,'pro_disprice' =>$discountprice,'pro_weight' =>$pro_weight]);
                    }
                    else
                    {
                      DB::table('nm_product')->where('pro_id',$cid)->where('pro_mr_id',$mer_id)->update( ['pro_title'=>  $pro_title,'pro_title_ar'=>  $pro_title_ar,'pro_Img'=> $pro_Img,'pro_mc_id'=> $pro_mc_id,'pro_price'=> $pro_price,'pro_status'=> $pro_status,'pro_desc' =>$about,'pro_desc_ar' =>$about_ar,'pro_qty' =>$pro_qty,'attribute_id' =>$attribute_id,'pro_discount_percentage' =>$discount,'pro_disprice' =>$discountprice,'pro_weight' =>$pro_weight]);
                    }

 
 
                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تم تحديث العنصر بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Item successfully updated");
                    }
                    // language for display message // 

                }
 
// FOR PRODUCT MULTIPLE IMAGES UPLOAD //
    $files=$request->file('image');
    $newFiles = array();
    $merid  = Session::get('merchantid');
    if($files=$request->file('image')){
    foreach($files as $key=>$val){
    array_push($newFiles, $key);
    }

    $privius=$request->privius;
    $newFilesMatch = array();
    if(isset($privius) && $privius!='')
    {
    foreach($privius as $key=>$val){
    array_push($newFilesMatch, $key);
    }  
    }

    $CheckInfo = array_intersect($newFiles, $newFilesMatch);
    if(isset($privius) && $privius!='')
    {
    foreach($privius as $key=>$val){
    if(in_array($key, $CheckInfo))
    {
    DB::table('nm_product_gallery')->where('id',$val)->where('vendor_id',$merid)->delete(); 
    }
    }
    } 
    foreach($files as $file){
    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
    $extension = $file->getClientOriginalExtension();
    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
    $imageRealPath  =   $file->getRealPath();
    $thumbName      =   'thumb_'. $fileName;       
    $img = Image::make($imageRealPath); // use this if you want facade style code
    $thumb_width = 150;
    list($width,$height) = getimagesize($imageRealPath);
    $thumb_height = ($thumb_width/$width) * $height;
    $img->resize($thumb_width,$thumb_height);                  
    $img->save('receptionspic' . '/'. $thumbName);
    $file->move('receptionspic/', $fileName);
    $shop_Img = url('/').'/receptionspic/'.$thumbName; 
    /*Insert data to db*/
    DB::table('nm_product_gallery')->insert( [
    'image'=>   $shop_Img,
    'product_id' =>$cid,
    'status' => 1,
    'vendor_id' => $merid,
    ]);
    }  
        } 


 
// FOR PRODUCT MULTIPLE IMAGES UPLOAD END //

                return redirect()->route('event-item-information', ['id' => $id,'shopid'=>$pro_mc_id]);

 
              //  return view('sitemerchant.reception-list-attributes', compact('merchantheader','merchantfooter','id'));       
            } else {
                return Redirect::to('sitemerchant');
            }

}


 //STORE OFFER
       public function storeMakeOffer(Request $request)
       {
            if (Session::has('merchantid')) 
        {      
        $mer_id   = Session::get('merchantid');
        $id = $request->parentid;
        $sid = $request->shopid;
        $autoid = $request->autoid;
        if($autoid =='')
        {
        $categorysave = new HallOffer; 
        }
        else
        {
        $categorysave = HallOffer::where('id',$autoid)->where('vendor_id',$mer_id)->first();
        }
        $categorysave->vendor_id    = $mer_id; 
        $categorysave->title        =$request->title;
        $categorysave->title_ar    = $request->title_ar;
        $categorysave->start_date     = $request->Date_from;
        $categorysave->end_date    = $request->date_to;
        $categorysave->discount   = $request->discount;
        $categorysave->coupon   = $request->coupon;
        $categorysave->pro_id   = $sid;
        $categorysave->status    = 1;
        if (\Config::get('app.locale') == 'ar'){
        Session::flash('message', "تمت إضافة العرض بنجاح  ");
        }
        else
        {
        Session::flash('message', "Offer successfully saved");
        }
        $categorysave->save(); 
        return redirect()->route('event-offer',['id' => $id,'itemid' => $sid]);
       } else {
      return Redirect::to('sitemerchant');
      } 
       }
     //END OFFER
   
 } // end 



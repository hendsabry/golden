<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Models;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon;
use File;
use App\Reviewrating;
use App\Categorylist;
use Intervention\Image\ImageManagerStatic as Image; 
use Auth;
use Lang;
use App\ProductsMerchant;
use App\ServiceStaff;
use App\Merchant;
use App\ProductAttribute;
use App\HallOffer;



class MerchantSkinTeethController extends Controller
{
      public function __construct(){
            parent::__construct();
            // set admin Panel language
            $this->middleware(function ($request, $next) {
                        $this->setLanguageLocaleMerchant();
                        Session::get('mer_lang_code');
                        return $next($request);
           });
     }
    
 
//Add Shop info

// Start For Popular band info
   
    public function SkinTeethShop(Request $request)
     {
             //$q->where('name', 'like', "%{$breed}%");
        if (Session::has('merchantid')) 
        {   
                $getPagelimit = config('app.paginate');
                $id = request()->id;  
                $status = $request->status;
                $search = $request->input('search');
                $mer_id   = Session::get('merchantid');
                //$alldata = Categorylist::where('parent_id',$id)->where('vendor_id',$mer_id)->orderBy('mc_id','DESC');

        if(Session::get('LoginType') =='4')
           {
            //= CHECK THE MARCHANT ACCESS ==//
              $merchantManagerid = Session::get('merchant_managerid');
              $fetchB = Categorylist::where('parent_id',$id)->where('vendor_id',$mer_id)->get();
              $ChekArr = array();
              foreach($fetchB as $chekB)
              {
              $mcid  = $chekB->mc_id;
              $fetchBC = Categorylist::where('parent_id',$mcid)->where('vendor_id',$mer_id)->where('branch_manager_id',$merchantManagerid)->count();
              if($fetchBC >=1)
              {
              $BId = Categorylist::where('parent_id',$mcid)->where('vendor_id',$mer_id)->where('branch_manager_id',$merchantManagerid)->first();
              $CP= $BId->parent_id;
              array_push($ChekArr, $CP);
              }

              } 
              $alldata = Categorylist::where('parent_id',$id)->where('vendor_id',$mer_id)->whereIn('mc_id', $ChekArr)->orderBy('mc_id','DESC');
            //= CHECK THE MARCHANT ACCESS ==//
           }
           else
           {
            $alldata = Categorylist::where('parent_id',$id)->where('vendor_id',$mer_id)->orderBy('mc_id','DESC');
           }




                if($search !='')
                {
                $mc_name='mc_name';
                $mer_selected_lang_code = Session::get('mer_lang_code'); 
                if($mer_selected_lang_code !='en')
                {
                $mc_name= 'mc_name_'.$mer_selected_lang_code;
                }
                $alldata = $alldata->where('nm_category.'.$mc_name,'like', '%' .$search. '%');
                }


                if($status !='')
                {
                $alldata = $alldata->where('mc_status','=',$status);
                }

                $alldata=$alldata->paginate($getPagelimit)->appends(request()->query());
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');
                return view('sitemerchant.skin-teeth.skin-teeth-shop', compact('merchantheader','merchantfooter','alldata','id','search'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 
/* Laser Cosmetic Shop */

    public function SkinTeethAddShop(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $sid = request()->sid;
                $fetchfirstdata = Categorylist::where('mc_id',$sid)->where('vendor_id',$mer_id)->first();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');   
                return view('sitemerchant.skin-teeth.skin-teeth-add-shop', compact('merchantheader','merchantfooter','id','sid','fetchfirstdata'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
   
 //Skin teeth shop store
   public function storeClinicshop(Request $request)

        {
                 //echo $request->ssb_name;die
                $this->validate($request,[
               /*'city' => 'required|max:200',
                'hotelname' => 'required|max:150',
                hotel_image' => 'required'*/
            ]);
               
                   $sid = $request->sid;
                   $id = $request->parentid;


               if($sid){
                  $saveservice =Categorylist::where('mc_id',$sid)->first();
                } else { 
                 $saveservice = new Categorylist;
               }
                 $mer_id   = Session::get('merchantid');
                 if($request->file('mc_img')){ 
                      $file = $request->mc_img;  
                      $merid  = Session::get('merchantid');
                      $extension = $file->getClientOriginalExtension();
                      $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                      $imageRealPath  =   $file->getRealPath();
                      $thumbName      =   'thumb_'. $Image;       
                      $img = Image::make($imageRealPath); // use this if you want facade style code
                      $thumb_width = 150;
                      list($width,$height) = getimagesize($imageRealPath);
                      $thumb_height = ($thumb_width/$width) * $height;
                      $img->resize($thumb_width,$thumb_height);

                      $img->save('uploadimage/skinteeth' . '/'. $thumbName);
                      $file->move('uploadimage/skinteeth/', $Image);
                      $saveservice->mc_img = url('').'/uploadimage/skinteeth/'.$thumbName;
                        }
                        $saveservice ->mc_name = $request->mc_name;
                        $saveservice ->mc_name_ar = $request->mc_name_ar;
                        $saveservice ->vendor_id = $mer_id;
                        $saveservice ->parent_id = $request->parentid;
                        $saveservice->mc_status = 1;
                        if($saveservice->save()){
                          $id = $request->parentid;
                         if (\Config::get('app.locale') == 'ar'){
                            Session::flash('message', "عيادة حفظها بنجاح");
                         }
                         else
                         {
                             Session::flash('message', "Clinic successfully saved");
                         }

                          }
                       return redirect()->route('skin-teeth-shop',['id' => $id]); 


                     }
    
 //End Skin teeth shop store




/* Laser Cosmetic Add Shop */

      public function SkinTeethShopinfo(Request $request)
       {
        	if (Session::has('merchantid')) 
        		 {
        			$mer_id   = Session::get('merchantid');
        			$id = $request->id;
        			$sid = request()->sid;
              $autoid = request()->autoid;

            			$merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
            			$merchantfooter     = view('sitemerchant.includes.merchant_footer');   

            		 $fetchdata = Categorylist::where('mc_id', $autoid)->where('vendor_id',$mer_id)->first();
                 $storename = Categorylist::where('mc_id', $sid)->where('vendor_id',$mer_id)->first(); 
                 $manager = Merchant::where('vendor_parent_id',$mer_id)->where('mer_staus',1)->get();
        			return view('sitemerchant.skin-teeth.skin-teeth-shop-info', compact('merchantheader','merchantfooter','id','sid','fetchdata','storename','autoid','manager'));       
        		} else {
        			 return Redirect::to('sitemerchant');
        		}
       }


     //Skinbranch add
        
     public function StoreSkinTeethShopinfo(Request $request)
        {
                  
                   $merid  = Session::get('merchantid');
                   $sid = $request->sid;
                   $itemid = $request->autoid;
                   if($itemid){
                      $savebranch =Categorylist::where('mc_id',$itemid)->first();
                    } else { 
                     $savebranch = new Categorylist;
                   }
               if($request->file('branchimage')){ 
                      $file = $request->branchimage;  
                      $merid  = Session::get('merchantid');
                      $extension = $file->getClientOriginalExtension();
                      $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                      $imageRealPath  =   $file->getRealPath();
                      $thumbName      =   'thumb_'. $Image;       
                     $img = Image::make($imageRealPath); // use this if you want facade style code
                      $thumb_width = 252;
                        list($width,$height) = getimagesize($imageRealPath);
                       $thumb_height = ($thumb_width/$width) * $height;
                       $img->resize($thumb_width,$thumb_height);
                      $img->save('buffetimage' . '/'. $thumbName);
                      $file->move('buffetimage/', $Image);
                      $savebranch->mc_img = url('').'/buffetimage/'.$thumbName;
                    }
                    //Addressimage
                    if($request->file('address_image')){ 
                      $file = $request->address_image;  
                      $merid  = Session::get('merchantid');
                      $extension = $file->getClientOriginalExtension();
                      $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                      $imageRealPath  =   $file->getRealPath();
                      $thumbName      =   'thumb_'. $Image;       
                      $img = Image::make($imageRealPath); // use this if you want facade style code
                      $thumb_width = 150;
                        list($width,$height) = getimagesize($imageRealPath);
                       $thumb_height = ($thumb_width/$width) * $height;
                       $img->resize($thumb_width,$thumb_height);
                      $img->save('buffetimage' . '/'. $thumbName);
                      $file->move('buffetimage/', $Image);
                      $savebranch->address_image = url('').'/buffetimage/'.$thumbName;
                    }
                  //Termaand condition
                   if(!empty($request->mc_tnc)){ 
                      $file = $request->mc_tnc;
                      $orgname =  $file->getClientOriginalName();
                      $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                      $extension = $file->getClientOriginalExtension();
                      $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                      $file->move('./public/assets/storeimage/', $filename_c);                      
                      $Cname =   url('').'/public/assets/storeimage/'.$filename_c;
                      $savebranch->terms_conditions =$Cname;
                      
                   }
                   else {
                        $orgname = $request->tmcvalue; 
                     }



                   //Arabic documents
                   //Termaand condition
                   if(!empty($request->mc_tnc_ar)){ 
                      $file = $request->mc_tnc_ar;
                      $orgnamear =  $file->getClientOriginalName();
                      $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                      $extension = $file->getClientOriginalExtension();
                      $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                      $file->move('./public/assets/storeimage/', $filename_c);                      
                      $Cnamear =   url('').'/public/assets/storeimage/'.$filename_c;
                      $savebranch->terms_conditions_ar =$Cnamear;
                      
                   }
                   else {
                        $orgnamear = $request->tmcvalue_ar; 
                   }

                        $savebranch ->longitude = $request->longitude;
                  $savebranch ->latitude = $request->latitude;
                      $savebranch ->vendor_id = $merid;
                      $savebranch ->google_map_address = $request->google_map_address;
                      $savebranch ->address = $request->address;
                      $savebranch ->address_ar = $request->address_ar;
                      $savebranch ->city_id = $request->city_id;
                      $savebranch ->parent_id = $sid;
                      $savebranch ->mc_name = $request->mc_name;
                      $savebranch ->mc_name_ar = $request->mc_name_ar;
                      $savebranch ->mc_discription = $request->about;
                      $savebranch ->mc_discription_ar = $request->about_ar;
                      $savebranch ->city_id = $request->city_id;
                      $savebranch ->terms_condition_name = $orgname;
                      $savebranch ->terms_condition_name_ar = $orgnamear;
                      $savebranch->mc_status  = 1;
                      $savebranch->branch_manager_id  =$request->manager;
                      $savebranch->opening_time  = $request->opening_time;
                      $savebranch->closing_time  = $request->closing_time;
                    if($savebranch->save()){
                      $id = $request->parent_id;

                  if($itemid==''){
                   $itemid = $savebranch->mc_id;
                  }
                if (\Config::get('app.locale') == 'ar'){
                   Session::flash('message', "فرع حفظ بنجاح");
                   }
                   else
                   {
                   Session::flash('message', "Branch successfully saved");
                   }

                       return redirect()->route('skin-teeth-shop-info',['id' => $id,'sid' =>$sid,'autoid' =>$itemid]);
                      }   
                }
     //End skinbranch
 
   public function SkinTeethPicture(Request $request)
   {
  	

                          if (Session::has('merchantid')) 
                          {
                              $merid  = Session::get('merchantid');
                              $catid = $request->id;
                              $sid = $request->sid;
                              $itemid = $request->autoid;

                              $this->setLanguageLocaleMerchant();
                              $mer_id              = Session::get('merchantid');             
                              $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                              $merchantfooter     = view('sitemerchant.includes.merchant_footer');           
                             
                              $proid = $request->id;
                              $getDbC = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->count();
                              
                              $getVideos = DB::table('nm_category')->where('mc_id',$itemid)->where('vendor_id',$merid)->first();
                              if($getDbC >=1)
                              {
                              $getDb = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->get(); 
                              $getDb = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->get();

                              }
                              else
                              {
                              $getDb = '';    
                              }

                            return view('sitemerchant.skin-teeth.skin-teeth-picture', compact('merchantheader','merchantfooter','getCatlists','GetAlreadyServices','getDb','getDbC','getVideos','sid','catid','itemid'));       
                          } else {
                              return Redirect::to('sitemerchant');
                      }
            }
     
   //Store picture
           //Store picture
       public function StoreClinicteethShopPicture(Request $request)

               { 

                  if (Session::has('merchantid')) 
                  {

                      $validatedData = $request->validate([
                            //'image' => 'required',
                           //'youtubevideo' => 'required',
                           'image.*' => 'image|mimes:jpg,jpeg,png,gif'
   
                      ]); 
   

                      $merid  = Session::get('merchantid');
                      $this->setLanguageLocaleMerchant();
                      $mer_id              = Session::get('merchantid');             
                      $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                      $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                
                      //Insert images in folder
                   
                              $catid = $request->cat_id;
                              $sid = $request->sid;
                              $itemid = $request->itemid;


                    
                      if($files=$request->file('image')){
                      foreach($files as $file){
                      $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
                      //$file->move('hallpics',$name); 
                      $extension = $file->getClientOriginalExtension();
                      $fileName = rand(11111,99999).'_'.time().'.'.$extension;
                      //$file->move('hallpics', $fileName);               
                      $imageRealPath  =   $file->getRealPath();
                      $thumbName      =   'thumb_'. $fileName;       
                      $img = Image::make($imageRealPath); // use this if you want facade style code
                       $thumb_width = 100;
                      list($width,$height) = getimagesize($imageRealPath);
                     $thumb_height = ($thumb_width/$width) * $height;
                     $img->resize($thumb_width,$thumb_height);

                    
                      $img->save('buffetimage' . '/'. $thumbName);
                      $file->move('buffetimage/', $fileName);
                      $shop_Img = url('/').'/buffetimage/'.$thumbName; 
                         
                      /*Insert data to db*/
                      DB::table('nm_category_gallery')->insert( [
                      'image'=>   $shop_Img,
                      'category_id' =>$itemid,
                      'vendor_id' => $merid,
                      ]);
                     }  

                   } 
                     
                      $youtubevideoa = str_replace('youtu.be','www.youtube.com/embed',$request->youtubevideo);
                      $mc_video_description = $request->about_video;
                      $mc_video_description_ar = $request->about_video_ar;

                         
                      /*Update data to db*/
                      DB::table('nm_category')->where('mc_id',$itemid)->where('vendor_id',$merid)->update( ['mc_video_url'=>$youtubevideoa,'mc_video_description'=> $mc_video_description,'mc_video_description_ar'=> $mc_video_description_ar]);
                     

                    
                      
                    
                      // language for display message //

                       if($mc_video_description != '' || $mc_video_description_ar != '' || $youtubevideoa != '' || $request->file('image') !='')
                      {
                      if (\Config::get('app.locale') == 'ar'){
                      Session::flash('message', "حفظ السجل بنجاح");
                      }
                      else
                      {
                      Session::flash('message', "Record successfully saved");
                      }
                    }
                      // language for display message //   
                      return Redirect::back();      
                  } else {
                          return Redirect::to('sitemerchant');
                  }

               }

   //End picture         

 
 





     public function SkinTeethReview(Request $request)
         {
            if (Session::has('merchantid')) 
                 {
                    $mer_id         = Session::get('merchantid');
                    $id             = $request->id;
                    $getPagelimit   = config('app.paginate');
                    $sid            = $request->sid;
                    $autoid         = $request->autoid;
                    $merchantheader = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                    $merchantfooter = view('sitemerchant.includes.merchant_footer'); 
                    $reviewrating   = Reviewrating::where('vandor_id',$mer_id)->where('shop_id',$autoid)->where('status',1)->orderBy('created_at','desc')->paginate($getPagelimit)->appends(request()->query());
     
                    return view('sitemerchant.skin-teeth.skin-teeth-reviews-and-comments', compact('merchantheader','merchantfooter','id','reviewrating'));       
                } else {
                     return Redirect::to('sitemerchant');
                }
         } 
	 
            public function SkinTeethDoctors(Request $request)
            {

              if(Session::has('merchantid')) 
              {
              $getPagelimit = config('app.paginate');

              $mer_id   = Session::get('merchantid');
              $id = request()->id;
              $sid = request()->sid;

              $autoid = request()->autoid;


              $status = $request->status;
              $search = $request->search;
              $fetchdata = ProductsMerchant::where('pro_mc_id',$autoid)->where('pro_mr_id',$mer_id)->orderBy('pro_id','DESC');


              if($search !='')
                  {      
                  $mc_name='pro_title';
                  $mer_selected_lang_code = Session::get('mer_lang_code'); 
                  if($mer_selected_lang_code !='en')
                  {


                  $fetchdata = $fetchdata->where('pro_title_ar','LIKE','%' .$search.'%');
                  }

                  $fetchdata =$fetchdata->where('pro_title','LIKE','%' .$search.'%');
                  } 

              if($status !='')
              {
              $fetchdata = $fetchdata->where('pro_status','=',$status);

              } 

              $fetchdata=$fetchdata->paginate($getPagelimit)->appends(request()->query());

              $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
              $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              return view('sitemerchant.skin-teeth.skin-teeth-doctors', compact('merchantheader','merchantfooter','parent_id','fetchdata','id','sid','autoid','search'));       
              } 
              else 
              {
              return Redirect::to('sitemerchant');
              }

            } 	
	 

          public function SkinTeethAddDoctor(Request $request)
           {
          	if (Session::has('merchantid')) 
          		 {
          			$mer_id   = Session::get('merchantid');
          			 $id = $request->id;
          			 $hid = request()->sid;
                 $autoid  = request()->autoid;
                 $incid = request()->incid;


                 $fetchdata =ProductsMerchant::where('pro_id',$incid)->first();
                 $alldata =ProductAttribute::where('product_id',$incid)->get();



                $getDbC = DB::table('nm_product_option_value')->where('vandor_id', $mer_id)->where('product_option_id',1)->where('product_id',$hid)->count(); 
                    if($getDbC >=1)
                    {
                    $getDb = DB::table('nm_product_option_value')->where('vandor_id', $mer_id)->where('product_option_id',1)->where('product_id',$hid)->get(); 
                    }
                    else
                    {
                    $getDb = '';  
                    }

          			$merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          			$merchantfooter     = view('sitemerchant.includes.merchant_footer');   

          			$getlisting = Categorylist::where('mc_id', $hid)->where('vendor_id',$mer_id)->first(); 
          			return view('sitemerchant.skin-teeth.skin-teeth-add-doctors', compact('merchantheader','merchantfooter','id','sid','autoid','incid','getlisting','getDb','getDbC','fetchdata','alldata'));       
          		} else {
          			 return Redirect::to('sitemerchant');
          		}
           }

      //Store Doctor
      public function StoreDoctor(Request $request)
            {
                  $parent_id = $request->parent_id;
                  $sid = $request->sid;
                  $mer_id   = Session::get('merchantid');
                  $autoid = $request->autoid;
                  $incid =$request->incid; 
                 
                   //STORE DISH NAME IN PRODUCT TABLE
                  if($incid!='') {

                      $doctorsave =ProductsMerchant::where('pro_id',$incid)->first();
                     } else {
                     $doctorsave = new ProductsMerchant;
                    }
                 if($request->file('branchimage')){ 
                          $file = $request->branchimage;  
                          $extension = $file->getClientOriginalExtension();
                          $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                          $imageRealPath  =   $file->getRealPath();
                          $thumbName      =   'thumb_'. $Image;       
                          $img = Image::make($imageRealPath); // use this if you want facade style code
                          $thumb_width = 220;
                         list($width,$height) = getimagesize($imageRealPath);
                            $thumb_height = ($thumb_width/$width) * $height;
                            $img->resize($thumb_width,$thumb_height);
                            $img->save('buffetimage' . '/'. $thumbName);
                            $file->move('buffetimage/', $Image);
                            $doctorsave->pro_Img = url('').'/buffetimage/'.$thumbName;
                     } 

                         //Termaand condition
                   if(!empty($request->mc_tnc)){ 
                      $file = $request->mc_tnc;
                      $orgname =  $file->getClientOriginalName();
                      $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                      $extension = $file->getClientOriginalExtension();
                      $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                      $file->move('./public/assets/storeimage/', $filename_c);                      
                      $Cname =   url('').'/public/assets/storeimage/'.$filename_c;
                      $doctorsave->terms_conditions =$Cname;
                      
                   }
                   else {
                        $orgname = $request->tmcvalue; 
                     }



                   //Arabic documents
                   //Termaand condition
                   if(!empty($request->mc_tnc_ar)){ 
                      $file = $request->mc_tnc_ar;
                      $orgnamear =  $file->getClientOriginalName();
                      $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                      $extension = $file->getClientOriginalExtension();
                      $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                      $file->move('./public/assets/storeimage/', $filename_c);                      
                      $Cnamear =   url('').'/public/assets/storeimage/'.$filename_c;
                      $doctorsave->terms_conditions_ar =$Cnamear;
                      
                   }
                   else {
                        $orgnamear = $request->tmcvalue_ar; 
                   }

                         $doctorsave->pro_mc_id     = $autoid;
                         $doctorsave->attribute_id  = $request->category;
                         $doctorsave->pro_title  = $request->mc_name;
                         $doctorsave->pro_title_ar  = $request->mc_name_ar;
                         $doctorsave->pro_price  = $request->consulation_fees;

                        
                         
                         $doctorsave->pro_desc  = $request->about;
                         $doctorsave->pro_desc_ar  = $request->about_ar;
                         $doctorsave->pro_mr_id  = $mer_id;
                         $doctorsave->pro_status  = '1';
                         $doctorsave ->terms_condition_name = $orgname;
                         $doctorsave ->terms_condition_name_ar = $orgnamear;
                         $doctorsave ->specialistion = $request->specialist;
                         $doctorsave ->specialistion_ar = $request->specialist_ar;
                         $doctorsave->opening_time  = $request->opening_time;
                         $doctorsave->closing_time  = $request->closing_time;
                         $doctorsave->save();
                         if($incid=='') { 
                         $proid = DB::getPdo()->lastInsertId();
                          } else {
                           $proid = $incid; 
                          }
                        if($proid) {
                           //$dishpricesave = new nm_product_attribute;
                             $productid         =      $proid;
                             $biography    =        $request->servicename;
                             $biographyar    =      $request->servicename_ar;

                             DB::table('nm_product_attribute')->where('product_id',$proid)->delete();  

                                $titlle = $request->servicename;
                               $titlle_ar = $request->servicename_ar;
                               $getBio = count($biography); 
                              for($i=0; $i<$getBio;$i++)
                              {
                             
                               $attributetitle = $titlle[$i];
                                $attributetitle_ar = $titlle_ar[$i];

                              DB::table('nm_product_attribute')->insert([
                              'product_id' =>$productid,
                              'attribute_title' =>$attributetitle,
                              'attribute_title_ar' =>$attributetitle_ar,
                              ]);
                              }
     
                                   if (\Config::get('app.locale') == 'ar'){
                                     Session::flash('message', "معلومات الطبيب حفظها بنجاح");
                                     }
                                     else
                                     {
                                     Session::flash('message', "Doctor information successfully saved");
                                     }

                                      return redirect()->route('skin-teeth-doctors',['id'=>$parent_id,'sid'=>$sid,'autoid'=>$autoid]);
                                       
                                     }   
                                   }

      //End Doctor
	 	    
 
              public function SkinTeethOreder(Request $request)
              {
              	if(Session::has('merchantid')) 
              	{
              		 $subproducttype = 'skin';
                   $mer_id         = Session::get('merchantid');
                   $id             = $request->id;
                   $sid            = $request->sid;
                   $autoid         = $request->autoid;
                   $searchkeyword  = $request->searchkeyword;
                   $date_to        = $request->date_to;
                   $End_Date       = $request->from_to;
                   $order_days     = $request->order_days;
                   $status         = $request->status;                
                   $serachfirstfrm = $request->serachfirstfrm;
                   $getorderedproducts = DB::table('nm_order_product')->where('product_type','clinic')->where('product_sub_type','skin');
                  if($searchkeyword!='')
                  {
                    $getorderedproducts = $getorderedproducts->where('order_id',$searchkeyword);
                  }

                  if($status!='')
                  {
                    $getorderedproducts = $getorderedproducts->where('status',$status);
                  } 

                  if($date_to!='' && $End_Date!='')
                  {
                    $getorderedproducts = $getorderedproducts->whereDate('created_at', '>=', $date_to)->whereDate('created_at', '<=', $End_Date);
                  } 

                  $getorderedproducts = $getorderedproducts->where('merchant_id',$mer_id)->where('category_id',$autoid)->groupBy('order_id')->orderBy('created_at','DESC')->selectRaw('*,sum(nm_order_product.total_price) as sum')->get();

              			$merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
              			$merchantfooter     = view('sitemerchant.includes.merchant_footer');   

              			$getlisting = Categorylist::where('mc_id', $sid)->where('vendor_id',$mer_id)->first(); 
              			return view('sitemerchant.skin-teeth.skin-teeth-order', compact('merchantheader','merchantfooter','id','sid','getlisting','getorderedproducts','mer_id','searchkeyword','autoid'));       
              		} else {
              			 return Redirect::to('sitemerchant');
              		}
               }

            //ORDER DETAILS

         public function getSkinorderdetail(Request $request){
         if(Session::has('merchantid')) 
         {
                 $mer_id           = Session::get('merchantid');
                 $id               = $request->id;
                 $sid              = $request->sid;
                 $autoid           = $request->autoid;
                 $proid            = $request->proid;
                 $cusid            = $request->cusid;
                 $ordid            = $request->ordid;
                 $productid        = $request->productid;                 
                 $productdata = DB::table('nm_order_product')->where('product_type','clinic')->where('category_id',$autoid)->where('order_id',$ordid)->where('product_sub_type','skin')->where('cus_id',$cusid)->where('order_id',$ordid)->orderBy('created_at','DESC')->get();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
             
                return view('sitemerchant.skin-teeth.skinorderdetail', compact('merchantheader','merchantfooter','id','sid','autoid','proid','cusid','ordid','productid','productdata'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
      }
     

        public function SkinTeethBranch(Request $request)
         {
            
             if(Session::has('merchantid')) 
          {
              $mer_id   = Session::get('merchantid');
              $parent_id = request()->id;
              $sid = request()->sid;
              $status = $request->status;
              $search = $request->search;
              $cityname = $request->cityname;
              $getPagelimit = config('app.paginate');

                //$alldata = Categorylist::where('parent_id',$sid)->where('vendor_id',$mer_id);


           if(Session::get('LoginType') =='4')
           {
            //= CHECK THE MARCHANT ACCESS ==//
             $merchantManagerid = Session::get('merchant_managerid');
            $alldata = Categorylist::where('parent_id',$sid)->where('branch_manager_id',$merchantManagerid)->where('vendor_id',$mer_id);
             //= CHECK THE MARCHANT ACCESS ==//
            }
            else
            {
             $alldata = Categorylist::where('parent_id',$sid)->where('vendor_id',$mer_id);  
            }
 


                    if($cityname != '')
                    {
                    $alldata = $alldata->where('city_id',$cityname); 
                    }
                    if($status != '')
                    {
                    $alldata = $alldata->where('mc_status',$status);
                    }
                    if($search !='')
                    {
                        $mer_selected_lang_code = Session::get('mer_lang_code'); 
                        if($mer_selected_lang_code !='en')
                        {
                        $alldata = $alldata->where('mc_name_ar','LIKE','%' .$search.'%');
                        }
                        else
                        {
                        $alldata = $alldata->where('mc_name','LIKE','%' .$search.'%');
             
                        }
                  }   

                    $alldata = $alldata->orderBy('mc_id','desc')->paginate($getPagelimit)->appends(request()->query());




              $servicedata =Categorylist::where('mc_id',$sid)->where('vendor_id',$mer_id)->get();
              $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
              $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              return view('sitemerchant.skin-teeth.skin-teeth-shop-branch', compact('merchantheader','merchantfooter','parent_id','sid','alldata'));       
          } 
          else 
          {
              return Redirect::to('sitemerchant');
          }

           
         }



  /// Start For Makeup Offer
    public function skinTeethOffer(Request $request)
     {
           if (Session::has('merchantid')) 
             {
                
               if (Session::has('merchantid')) 
             {   $getPagelimit = config('app.paginate');
                  $id = $request->id;
                  $sid = $request->sid;
                  $autoid = $request->autoid;
                  $status = $request->status;
                  $search = $request->search;

             $mer_id   = Session::get('merchantid');
            $hallofferlist =  HallOffer::where('vendor_id',$mer_id)->where('pro_id',$autoid);
            if($status!='')
            {
            $hallofferlist = $hallofferlist->where('status',$status);
            }
            if($search !='')
            {
            $mer_selected_lang_code = Session::get('mer_lang_code'); 
            if($mer_selected_lang_code !='en')
            {
            $hallofferlist = $hallofferlist->where('title_ar','LIKE','%' .$search.'%');
            }
            else
            {
            $hallofferlist = $hallofferlist->where('title','LIKE','%' .$search.'%');
 
            }
            }   
            $hallofferlist =  $hallofferlist->orderBy('id','DESC')->paginate($getPagelimit);

                $this->setLanguageLocaleMerchant();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
            return view('sitemerchant.skin-teeth.skin-offer',compact('merchantheader','merchantfooter','hallofferlist','status','autoid','sid','id','search'));       
            } else {
                return Redirect::to('sitemerchant');
            }
        }
     }
// End For Makeup Offer    


// Start For Makeup Add Offer
    public function skinTeethAddOffer(Request $request)
     {
           
           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $getPagelimit = config('app.paginate');
                $sid = $request->sid;
                $autoid = $request->autoid;
                $incid = $request->incid;
                
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
               $reviewrating = Reviewrating::where('vandor_id',$mer_id)->paginate($getPagelimit)->appends(request()->query());
                $categorysave = HallOffer::where('id',$incid)->where('vendor_id',$mer_id)->first(); 
 
                return view('sitemerchant.skin-teeth.skin-teeth-add-offer', compact('merchantheader','merchantfooter','id','sid','autoid','incid','reviewrating','categorysave'));       
            } else {
                 return Redirect::to('sitemerchant');
            }

        }
// End For Makeup Add Offer 
    //STORE OFFER
       public function storeSkingTeethOffer(Request $request)
       {
            if (Session::has('merchantid')) 
        {      
        $mer_id   = Session::get('merchantid');
        $id = $request->parentid;
        $sid = $request->sid;
        $autoid = $request->autoid;
        $incid = $request->incid;

        if($incid =='')
        {
        $categorysave = new HallOffer; 
        }
        else
        {
        $categorysave = HallOffer::where('id',$incid)->where('vendor_id',$mer_id)->first();
        }
        $categorysave->vendor_id    = $mer_id; 
        $categorysave->title        =$request->title;
        $categorysave->title_ar    = $request->title_ar;
        $categorysave->start_date     = $request->Date_from;
        $categorysave->end_date    = $request->date_to;
        $categorysave->discount   = $request->discount;
        $categorysave->coupon   = $request->coupon;
        $categorysave->pro_id   = $autoid;
        $categorysave->status    = 1;
        if (\Config::get('app.locale') == 'ar'){
        Session::flash('message', "تمت إضافة العرض بنجاح  ");
        }
        else
        {
        Session::flash('message', "Offer successfully saved");
        }
        $categorysave->save(); 
        return redirect()->route('skin-teeth-offer',['id' => $id,'sid' => $sid,'autoid'=>$autoid]);
       } else {
      return Redirect::to('sitemerchant');
      } 
       }
     //END OFFER  

   
 } // end 



<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Models;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon;
use File;
use App\Reviewrating;
use App\Categorylist;
use Intervention\Image\ImageManagerStatic as Image; 
use Auth;
use Lang;

 

class MerchantSingerController extends Controller
{
      public function __construct(){
            parent::__construct();
            // set admin Panel language
            $this->middleware(function ($request, $next) {
                        $this->setLanguageLocaleMerchant();
                        Session::get('mer_lang_code');
                        return $next($request);
           });
     }
    
 
//Add Shop info

// Start For Singer
    public function manageSinger(Request $request)
    {
        if(Session::has('merchantid')) 
        {
            $mer_id         = Session::get('merchantid');
            $id             = $request->id;
            $hid            = $request->hid;
            $merchantheader = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
            $merchantfooter = view('sitemerchant.includes.merchant_footer'); 

            $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

            $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'singer')->where('category_id', $id)->first();
 
            return view('sitemerchant.singer.singer-info', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
        } 
        else 
        {
           return Redirect::to('sitemerchant');
        }
     }

public function addsingerinfo(Request $request)
{

 if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
             

                $singer_name = $request->singer_name;
                $singer_name_ar = $request->singer_name_ar;                
             
                $about_singer = $request->about_singer;
                $about_singer_ar = $request->about_singer_ar;
                $tnc = $request->tnc;
                $singer_image = $request->singer_image;
                 $mc_status = $request->mc_status;
              


                if($file=$request->file('singer_image')){  

                $extension = $file->getClientOriginalExtension();
                $Adressimgname = rand(11111,99999).'_'.time().'.'.$extension;               
                $imageRealPath  =   $file->getRealPath();
                $thumbName      =   'thumb_'. $Adressimgname;       
                $img = Image::make($imageRealPath); // use this if you want facade style code
                  
                $thumb_width = 150;
                list($width,$height) = getimagesize($imageRealPath);
                $thumb_height = ($thumb_width/$width) * $height;
                $img->resize($thumb_width,$thumb_height);
 
                $img->save('singer' . '/'. $thumbName);
                $file->move('singer/', $Adressimgname);
                $thumbNameurl = url('').'/singer/'.$thumbName;
                }
                else
                {
               $thumbNameurl =  $request->singer_image_url;
                }


                if($tnc=$request->file('tnc')){  
                    $tncname =   $tnc->getClientOriginalName();  
                $extension = $tnc->getClientOriginalExtension();
                $tncs = rand(11111,99999).'_'.time().'.'.$extension;              
                $tnc->move('singer/certificate/', $tncs);
                $tncsurl = url('').'/singer/certificate/'.$tncs;
                }
                else
                {
               $tncsurl =  $request->tnc_url;
               $tncname = $request->tnc_name;
                }



                if($tnc=$request->file('tnc_ar')){  
                    $tncname_ar =   $tnc->getClientOriginalName();  
                $extension = $tnc->getClientOriginalExtension();
                $tncs_ar = rand(11111,99999).'_'.time().'.'.$extension;              
                $tnc->move('singer/certificate/', $tncs_ar);
                $tncsurl_ar = url('').'/singer/certificate/'.$tncs_ar;
                }
                else
                {
               $tncsurl_ar =  $request->tnc_url_ar;
               $tncname_ar = $request->tnc_name_ar;
                }
 

                $getC = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_id', $id)->count();
                if($getC < 1){
              $Q =  DB::table('nm_music')->insert([
                ['category_id' => $id, 'vendor_id' => $mer_id,'name_ar' => $singer_name_ar,'name' => $singer_name,'about' => $about_singer,'about_ar'=>$about_singer_ar,'category_type'=>'singer','status'=>$mc_status,'image'=>$thumbNameurl,'terms_conditions'=>$tncsurl,'terms_conditions_ar'=>$tncsurl_ar,'terms_condition_name' => $tncname,'terms_condition_name_ar' => $tncname_ar],
                ]); 
                   $hid = DB::getPdo()->lastInsertId();
                } 
                else
                {
                
                DB::table('nm_music')->where('category_type','singer')->where('vendor_id',$mer_id)->where('category_id',$id)->update( ['name_ar'=>  $singer_name_ar,'name' => $singer_name,'about' => $about_singer,'status'=>$mc_status,'about_ar'=>$about_singer_ar,'image'=>$thumbNameurl,'terms_conditions'=>$tncsurl,'terms_condition_name' => $tncname,'terms_conditions_ar'=>$tncsurl_ar,'terms_condition_name_ar' => $tncname_ar]); 
 
                }
                


                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();
 


 

                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تمت إضافة الخدمة");
                    }
                    else
                    {
                    Session::flash('message', "Record successfully saved");
                    }
                    // language for display message // 

            return redirect()->route('manage-singer',['id' => $id,'hid' => $hid]); 
                      
            } else {
                 return Redirect::to('sitemerchant');
            }

}

	 


public function addsingerpicture(Request $request)
{
  if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;

                if(!$request->file('hallpics') && trim($request->youtubevideo)=='' &&   trim($request->about)==''){
                $validatedData = $request->validate([
                'youtubevideo' => 'required', 
                ]);
                }
               
                if($files=$request->file('hallpics')){
                foreach($files as $file){
                $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());            
                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111,99999).'_'.time().'.'.$extension;                        
                $imageRealPath  =   $file->getRealPath();
                $thumbName      =   'thumb_'. $fileName;       
                $img = Image::make($imageRealPath); // use this if you want facade style code
               

                    $thumb_width = 100;
                    list($width,$height) = getimagesize($imageRealPath);
                    $thumb_height = ($thumb_width/$width) * $height;
                    $img->resize($thumb_width,$thumb_height);



                $img->save('singer' . '/images/'. $thumbName);
                $file->move('singer/images/', $fileName);
                $thumbNameUrl = url('').'/singer/images/'.$thumbName;
                DB::table('nm_music_gallery')->insert([
                ['vendor_id' => $mer_id,'image' => $thumbNameUrl,'music_id' => $hid],
                ]); 
                }
                }


                $about = $request->about;
                $about_ar = $request->about_ar;
                 $youtubevideo = str_replace('youtu.be','www.youtube.com/embed',$request->youtubevideo);

                DB::table('nm_music')->where('id',$hid)->where('vendor_id',$mer_id)->update( ['video_url'=>$youtubevideo,'video_description'=> $about,'video_description_ar'=> $about_ar]);

                 
                

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();


                 

                     // language for display message //
                if (\Config::get('app.locale') == 'ar'){
                Session::flash('message', "تمت إضافة الخدمة");
                }
                else
                {
                Session::flash('message', "Record saved successfully");
                }
                // language for display message // 
                   
               

                return Redirect::back();
                      
            } else {
                 return Redirect::to('sitemerchant');
            }

}
 


    public function singerPictureVideo(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 

  $getDbC = DB::table('nm_music_gallery')->where('vendor_id', $mer_id)->where('music_id', $hid)->get();

$getVideos = DB::table('nm_music')->where('vendor_id', $mer_id)->where('id', $hid)->first();
           return view('sitemerchant.singer.singer-photo-video', compact('merchantheader','merchantfooter','id','getDbC','getVideos'));       
            } else {
                return Redirect::to('sitemerchant');
            }
     }	 
   
 	// for displaying comment and reviews
	public function singerCommentsReview(Request $request)
	{
		 $getPagelimit = config('app.paginate');
		if (Session::has('merchantid')) 
		{
		$merid  = Session::get('merchantid'); 
    $hid = $request->hid;
		$this->setLanguageLocaleMerchant();
		$mer_id              = Session::get('merchantid');             
		$merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
		$merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
		$reviewrating = Reviewrating::where('vandor_id',$mer_id)->where('product_id',$hid)->where('review_type','shop')->paginate($getPagelimit)->appends(request()->query());
		return view('sitemerchant.singer.singer-comments-review', compact('merchantheader','merchantfooter','reviewrating'));       
		} else {
		return Redirect::to('sitemerchant');
		}


	}
 	// for displaying Quoted Requested List 
 public function singerQuotedRequestedList(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                  $mer_id   = Session::get('merchantid');
                  $id = $request->id;
                  $hid = $request->hid;
                  $getPagelimit = config('app.paginate');
                  $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                  $merchantfooter     = view('sitemerchant.includes.merchant_footer');
                  $getusers = DB::table('nm_services_inquiry')
            ->join('nm_customer', 'nm_services_inquiry.user_id', '=', 'nm_customer.cus_id')
            ->select('nm_customer.cus_name','nm_customer.email','nm_customer.cus_phone','nm_services_inquiry.created_at','nm_services_inquiry.language_type','nm_services_inquiry.id','nm_services_inquiry.status')
            ->where('nm_services_inquiry.merchant_id', $mer_id)
             ->where('nm_services_inquiry.quote_for','singer')->orderBy('nm_services_inquiry.id','DESC');
            
            if(isset($_REQUEST['acceptance']) && $_REQUEST['acceptance']!='')
            {
            $srchs = $_REQUEST['acceptance'];
            $getusers =   $getusers ->where('nm_services_inquiry.status', '=', $srchs);
            }


            if(isset($_REQUEST['search']) && $_REQUEST['search']!='')
            {
                $srch = $_REQUEST['search'];
            $getusers =   $getusers ->where('nm_customer.cus_name','like', '%'.$srch.'%');
            }

           $getusers = $getusers->paginate($getPagelimit)->appends(request()->query());
 

           return view('sitemerchant.singer.singer-quoted-requested-list', compact('merchantheader','merchantfooter','id','getusers'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }  
   
     public function singerOrderList(Request $request)
     {
        if(Session::has('merchantid')) 
        {
                $mer_id         = Session::get('merchantid');
                $id             = $request->id;
                $hid            = $request->hid;
                $searchkeyword  = $request->searchkeyword;
                $date_to        = $request->date_to;
                $End_Date       = $request->from_to;
                $order_days     = $request->order_days;
                $status         = $request->status;                
                $serachfirstfrm = $request->serachfirstfrm;

                  $getorderedproducts = DB::table('nm_order_product')->where('product_type','singer')->where('product_sub_type','singer');
                  if($searchkeyword!=''){
                  $getorderedproducts = $getorderedproducts->where('order_id',$searchkeyword);
                  }

                  if($status!=''){
                  $getorderedproducts = $getorderedproducts->where('status',$status);
                  } 

                  if($date_to!='' && $End_Date!=''){
                  $getorderedproducts = $getorderedproducts->whereDate('created_at', '>=', $date_to)->whereDate('created_at', '<=', $End_Date);
                  } 

                  $getorderedproducts = $getorderedproducts->where('merchant_id',$mer_id)->groupBy('order_id')->orderBy('created_at','DESC')->selectRaw('*,sum(nm_order_product.total_price) as sum')->get();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                
                return view('sitemerchant.singer.singer-order-list', compact('merchantheader','merchantfooter','id','hid','getorderedproducts','mer_id'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }

    public function getorderdetailsinger(Request $request)
    {
     if(Session::has('merchantid')) 
     {
         $mer_id         = Session::get('merchantid');
         $id             = $request->id;
         $hid            = $request->hid;
         $aid            = $request->aid;
         $cusid          = $request->cusid;
         $orderid        = $request->orderid;
         $merchantheader = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
         $merchantfooter = view('sitemerchant.includes.merchant_footer');
         
         $productdata = DB::table('nm_order_product')->where('product_type','singer')->where('order_id',$orderid)->where('product_sub_type','singer')->where('cus_id',$cusid)->orderBy('created_at','DESC')->get();

         foreach ($productdata as $value) {
           $enquiry_id = $value->enquiry_id;
                if($enquiry_id !='')
                {
                $getloc = DB::table('nm_services_inquiry')->where('id',$enquiry_id)->first();
                $value->location= $getloc->location;
                }
                else
                {
                $value->location="N/A"; 
                }

           }  



        return view('sitemerchant.singer.order-detailsinger', compact('merchantheader','merchantfooter','id','hid','aid','cusid','orderid','productdata'));       
    } 
    else 
    {
         return Redirect::to('sitemerchant');
    }
  }
     
  	// for displaying Quoted Requested View 
     public function singerQuotedRequestedView(Request $request)
     {
        if(Session::has('merchantid')) 
        {
           $mer_id         = Session::get('merchantid');
           $lang           = Session::get('lang_file');
           $id             = $request->id;
           $hid            = $request->hid;
           $itmid          = $request->itmid;
           $autoid         = $request->autoid;
           if(isset($lang) && $lang == 'ar_lang')
           {
             $getCities = DB::table('nm_city')->where('ci_status',1)->select('ci_id','ci_lati','ci_name_ar as ci_name','ci_lati','ci_status')->orderBy('ci_name','ASC')->get();
           }
           else
           {
             $getCities = DB::table('nm_city')->where('ci_status',1)->select('ci_id','ci_lati','ci_name','ci_lati','ci_status')->orderBy('ci_name','ASC')->get();
           }
           $merchantheader = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
           $merchantfooter = view('sitemerchant.includes.merchant_footer');
           $getquote       = DB::table('nm_vendor_comment')->where('id',$autoid)->first();
           $getusers       = DB::table('nm_services_inquiry')
            ->join('nm_customer', 'nm_services_inquiry.user_id', '=', 'nm_customer.cus_id')
            ->select('nm_customer.cus_name','nm_customer.email','nm_customer.cus_phone','nm_services_inquiry.created_at','nm_services_inquiry.location','nm_services_inquiry.city_id','nm_services_inquiry.hall','nm_services_inquiry.occasion_type','nm_services_inquiry.duration','nm_services_inquiry.user_comment','nm_services_inquiry.time','nm_services_inquiry.date','nm_services_inquiry.status','nm_services_inquiry.language_type','nm_services_inquiry.user_id','nm_services_inquiry.id')
            ->where('nm_services_inquiry.id',  $itmid)
            ->where('nm_services_inquiry.merchant_id',$mer_id)
            ->first();

            return view('sitemerchant.singer.singer-quoted-requested-view', compact('merchantheader','merchantfooter','id','hid','itmid','getusers','getCities','getquote','autoid'));       
            } else {
                return Redirect::to('sitemerchant');
            }
     }    
   
   
   




  public function storeCommentSinger(Request $request)
  {
             //dd($request->all());die;
             $enqueryid    = $request->enqueryid;
             $comment      = $request->comments;
             $catid        = $request->catid;
             $hid          = $request->hid;
             $itmid        = $request->itmid;
             $user_id      = $request->userid;
             $price        = $request->price;
             $mer_id       = Session::get('merchantid');
             $autoid       = $request->autoid;
             $languagetype = $request->languagetype;            
             $email        = $request->email;
               
             $getcustomercurrency = DB::table('nm_services_inquiry')->where('id',$itmid)->first();
             //$customercurrency=$getcustomercurrency->rev_currency;
             $customercurrency = 'SAR';

             if($autoid!='')
             {
               DB::table('nm_services_inquiry')->where('id',$itmid)->update(['status'=>'2']);
               $Q = DB::table('nm_vendor_comment')->where('id',$autoid)->update([
                'cat_id'=>$catid, 
                'vendor_id'=>$mer_id,
                'user_id'=>$user_id,
                'enq_id'=>$itmid,
                'comment'=>$comment,
                'price'=>$price,
                'type'=>'singer',
                'status'=>'2'
                ]);
                $autoid=$autoid;
                $subject = 'Singer Pricing Quote is Successfully Approval ! Golden Cage'; 
                $status = 'Approved';
                $email    = $email;
                $username = $request->customername;
                $comment  = $request->comments;
                $price    = $customercurrency.' '.$request->price;
                                
                Mail::send('emails.vendor-singer-reply',
                array(
                'username' => $username,
                'languagetype' => $languagetype,
                'comment' => $comment,
                'price' => $price,
                'status' => $status
                ), function($message) use($email,$subject)
                { 
                  $message->to($email,'Golden Cage')->subject($subject);
                }); 

                /*Mail::send('emails.vendor-singer-reply', ['username' => $username,'languagetype' => $languagetype,'comment'=>$comment,'price'=>$price], function ($m) use ($username,$comment,$price,$email,$languagetype) {
                 $m->from('ajit@wisitech.com', 'Goldencages');
                 $m->to($email, $username)->subject('Singer Pricing Quote');
         
                  });
                     */

            


             } else {

             DB::table('nm_services_inquiry')->where('id',$itmid)->update([
                'status' => '2'
                ]);

             $Q =  DB::table('nm_vendor_comment')->insert([
                'cat_id' => $catid, 
                'vendor_id' => $mer_id,
                'user_id' => $user_id,
                'enq_id' => $itmid,
                'comment' => $comment,
                'price' => $price,
                'type' => 'singer',
                'status' => '2'
                ]);
                $autoid=DB::getPdo()->lastInsertId();;

                 $email = $email;
                 $username = $request->customername;
                 $comment = $request->comments;
                 $price = $customercurrency.' '.$request->price;

                 

                $subject = 'Singer Pricing Quote is Successfully Approval ! Golden Cage'; 
                $status = 'Approved';

                Mail::send('emails.vendor-singer-reply',
                array(
                'username' => $username,
                'languagetype' => $languagetype,
                'comment' => $comment,
                'price' => $price,
                'status' => $status
                ), function($message) use($email,$subject)
                {
                  $message->to($email,'Golden Cage')->subject($subject);
                }); 
                 
         }
            //Mail Content


         //Mail Implementations Code

            if($autoid =='') {
           
             $autoid = DB::getPdo()->lastInsertId();

             
        }



                  // language for display message //
                if (\Config::get('app.locale') == 'ar'){
                Session::flash('message', "ارسل التعليق بنجاح");
                }
                else
                {
                Session::flash('message', "Comment successfully sent");
                }
                // language for display message //
               return redirect()->route('singer-quoted-requested-view',['id' => $catid,'hid' => $hid,'itmid' =>$itmid,'autoid'=>$autoid]); 
          

      }

            







// End For Singer  
   


 //Shop Ajax

    public function receptionShopStatus(Request $request)
    {
     if ($request->ajax()) {
             $id = $request->id;
             $activestatus = $request->activestatus;
             if($activestatus==1) {
                $status = 0;
             } else {
               $status = 1;
             }
             $updatestatus = Categorylist::where('mc_id',$id)->first();
             $updatestatus->mc_status = $status;
              if($updatestatus->save()) {
                 echo 1;die;
              }  

         }
   }  
//Shop list
  public function receptionShopList(Request $request)

  {
            if (Session::has('merchantid')) 
             {

                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $fetchdata = '';
                $search = $request->search;
                $status = $request->status;
                 $getPagelimit = config('app.paginate');
                 if($search !='')
                {
                     $mc_name='mc_name';
                     $mer_selected_lang_code = Session::get('mer_lang_code'); 
                   if($mer_selected_lang_code !='en')
                   {
                             $mc_name= 'mc_name_ar';
                            
                   }

                  $alldata =   Categorylist::where('vendor_id',$mer_id)->where('nm_category.'.$mc_name,'like', '%' .$search. '%')->where('parent_id',$id)->orderBy('mc_id','DESC')->paginate($getPagelimit)->appends(request()->query()); 
                }
                 else if($status !='')
                {
                $alldata = Categorylist::where('vendor_id',$mer_id)->where('mc_status',$status)->where('parent_id',$id)->orderBy('mc_id','DESC')->paginate($getPagelimit)->appends(request()->query()); ;

                } else {
                       $alldata = Categorylist::where('vendor_id',$mer_id)->where('parent_id',$id)->orderBy('mc_id','DESC')->paginate($getPagelimit)->appends(request()->query()); ;

                }


              
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
           return view('sitemerchant.reception-shop-list', compact('merchantheader','merchantfooter','id','alldata'));       
            } else {
                return Redirect::to('sitemerchant');
            }


  }
     




    //Reception Sho Info
     public function receptionHospitality(Request $request)

     {
           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                 $parent_id = request()->id;
                 $mcid = request()->shopid;
                 
                $fetchdata = '';
                 $fetchdatacount = Categorylist::where('vendor_id',$mer_id)->where('parent_id',$parent_id)->where('mc_id',$mcid)->count();
                 if($fetchdatacount > 0) {
                 $fetchdata = Categorylist::where('vendor_id',$mer_id)->where('parent_id',$parent_id)->where('mc_id',$mcid)->first();
                 }

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
           return view('sitemerchant.reception-shop-info', compact('merchantheader','merchantfooter','parent_id','fetchdata'));       
            } else {
                return Redirect::to('sitemerchant');
            }


     }

     public function storeReceptionHospitality(Request $request)
     {
           if (Session::has('merchantid')) 
             {
                $id = request()->id;
                $mer_id   = Session::get('merchantid');
                $storeShopinfo =  new Categorylist;
                $storeShopinfo->mc_name = $request->mc_name;
                $storeShopinfo->mc_name_ar = $request->mc_name_ar;
                $storeShopinfo->google_map_address = $request->google_map_address;
                $storeShopinfo->vendor_id = $mer_id;
                $storeShopinfo->parent_id = $id;
                $storeShopinfo->mc_status = 1;


                //Insert images in folder
                   if($file=$request->file('mc_img'))
                    {          
                        $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());          
                        $extension = $file->getClientOriginalExtension();
                        $fileName = rand(11111,99999).'_'.time().'.'.$extension;                 
                        $imageRealPath  =   $file->getRealPath();
                        $thumbName      =   'thumb_'. $fileName;       
                        $img = Image::make($imageRealPath);
                         

                    $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                    $thumb_height = ($thumb_width/$width) * $height;
                    $img->resize($thumb_width,$thumb_height);

                        $img->save('receptionspic' . '/'. $thumbName);
                        $file->move('receptionspic/', $fileName);            
                        $shopimage_Img = url('/').'/receptionspic/'.$thumbName; 
                        $storeShopinfo->mc_img = $shopimage_Img;       
                     }

                     //Address image
                      //Insert images in folder
                   if($file=$request->file('address_image'))
                    {          
                        $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());          
                        $extension = $file->getClientOriginalExtension();
                        $fileName = rand(11111,99999).'_'.time().'.'.$extension;                 
                        $imageRealPath  =   $file->getRealPath();
                        $thumbName      =   'thumb_'. $fileName;       
                        $img = Image::make($imageRealPath);
                        


                  $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                    $thumb_height = ($thumb_width/$width) * $height;
                    $img->resize($thumb_width,$thumb_height);


                        $img->save('receptionspic' . '/'. $thumbName);
                        $file->move('receptionspic/', $fileName);            
                        $address_Img = url('/').'/receptionspic/'.$fileName; 
                        $storeShopinfo->address_image = $address_Img;          
                     }

              $storeShopinfo->save();
                 
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                // language for display message //
                if (\Config::get('app.locale') == 'ar'){
                Session::flash('message', "تمت إضافة المتجر بنجاح");
                }
                else
                {
                Session::flash('message', "Shop successfully added");
                }
          // language for display message //  
            return redirect()->route('reception-shop-list',['id' => $id,]); 
         //  return view('sitemerchant.reception-shop-info', compact('merchantheader','merchantfooter','id'));       
            } else {
                return Redirect::to('sitemerchant');
            }
 
     }

     
     public function updateShopInfo(Request $request)

     {
             if (Session::has('merchantid')) 
             {
                
                 $parent_id = request()->id;
                 $mcid = request()->shopid;
                $mer_id   = Session::get('merchantid');
                $storeShopinfo = Categorylist::where('vendor_id',$mer_id)->where('parent_id',$parent_id)->where('mc_id',$mcid)->first();
                $storeShopinfo->mc_name = $request->mc_name;
                $storeShopinfo->mc_name_ar = $request->mc_name_ar;
                $storeShopinfo->google_map_address = $request->google_map_address;
                $storeShopinfo->vendor_id = $mer_id;
                $storeShopinfo->parent_id = $parent_id;
                $storeShopinfo->mc_id = $mcid;
                $storeShopinfo->mc_status = 1;


                //Insert images in folder
                   if($file=$request->file('mc_img'))
                    {          
                        $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());          
                        $extension = $file->getClientOriginalExtension();
                        $fileName = rand(11111,99999).'_'.time().'.'.$extension;                 
                        $imageRealPath  =   $file->getRealPath();
                        $thumbName      =   'thumb_'. $fileName;       
                        $img = Image::make($imageRealPath);
                        $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                    $thumb_height = ($thumb_width/$width) * $height;
                    $img->resize($thumb_width,$thumb_height);
                        $img->save('receptionspic' . '/'. $thumbName);
                        $file->move('receptionspic/', $fileName);            
                        $shopimage_Img = url('/').'/receptionspic/'.$fileName; 
                        $storeShopinfo->mc_img = $shopimage_Img;       
                     } else {
                         $storeShopinfo->mc_img = $request->updateimage;
                     }

                     //Address image
                      //Insert images in folder
                   if($file=$request->file('address_image'))
                    {          
                        $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());          
                        $extension = $file->getClientOriginalExtension();
                        $fileName = rand(11111,99999).'_'.time().'.'.$extension;                 
                        $imageRealPath  =   $file->getRealPath();
                        $thumbName      =   'thumb_'. $fileName;       
                        $img = Image::make($imageRealPath);
                      $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                    $thumb_height = ($thumb_width/$width) * $height;
                    $img->resize($thumb_width,$thumb_height);
                        $img->save('receptionspic' . '/'. $thumbName);
                        $file->move('receptionspic/', $fileName);            
                        $address_Img = url('/').'/receptionspic/'.$fileName; 
                        $storeShopinfo->address_image = $address_Img;          
                     } else {

                         $storeShopinfo->address_image = $request->update_add_img;
                     }

              $storeShopinfo->save();
                 
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                // language for display message //
                if (\Config::get('app.locale') == 'ar'){
                Session::flash('message', "متجر تم تحديثه بنجاح");
                }
                else
                {
                Session::flash('message', "Shop successfully Updated");
                }
          // language for display message //  
            return redirect()->route('reception-shop-list',['id' => $parent_id]); 
         //  return view('sitemerchant.reception-shop-info', compact('merchantheader','merchantfooter','id'));       
            } else {
                return Redirect::to('sitemerchant');
            }  

     }






   //Shop picture and Video Url
   public function receptionShopPicture(Request $request)

     {
           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
           return view('sitemerchant.reception-shop-picture', compact('merchantheader','merchantfooter','id'));       
            } else {
                return Redirect::to('sitemerchant');
            }


     }


     //Attributes Listing

  public function receptionAttributes(Request $request)

     {
           if (Session::has('merchantid')) 
             {    $getPagelimit = config('app.paginate'); 
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $status = $request->status;
                $search = $request->search;
           $getAttr = DB::table('nm_services_attribute')->where('vendor_id', '=', $mer_id)->where('services_id', '=', $id);

                if($search !='')
                {
                 $getAttr = $getAttr->where('attribute_title', 'like', '%'.$search.'%');         
                }
                if($status !='')  
                {  
                $getAttr = $getAttr->where('status', '=', $status);        
                }  

                $getAttr = $getAttr->orderBy('id','DESC')->paginate($getPagelimit);


              
           return view('sitemerchant.reception-list-attributes', compact('merchantheader','merchantfooter','id','getAttr'));       
            } else {
                return Redirect::to('sitemerchant');
            }


     }



     //Worker Listing

  public function receptionListWorkers(Request $request)

     {
           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
           return view('sitemerchant.reception-list-workers', compact('merchantheader','merchantfooter','id'));       
            } else {
                return Redirect::to('sitemerchant');
            }


     }


     //Information Listing

  public function receptionListInformations(Request $request)

     {
           if (Session::has('merchantid')) 
             {  $getPagelimit = config('app.paginate');
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
 
                $status = $request->status;
                $search = $request->search;
                $getAttr = DB::table('nm_product')->where('pro_mr_id', '=', $mer_id)->where('pro_type', '=', 'reception_hospitality');

                if($search !='')
                {
                 $getAttr = $getAttr->where('pro_title', 'like', '%'.$search.'%');         
                }
                if($status !='')  
                {  
                $getAttr = $getAttr->where('pro_status', '=', $status);        
                }  

                $getAttr = $getAttr->orderBy('pro_id','DESC')->paginate($getPagelimit);
 


           return view('sitemerchant.reception-list-informations', compact('merchantheader','merchantfooter','id','getAttr'));       
            } else {
                return Redirect::to('sitemerchant');
            }


     }


public function receptionAddcategory(Request $request)
{

           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $cid = $request->cid;
        
            $getAttr = DB::table('nm_services_attribute')->where('vendor_id', '=', $mer_id)->where('services_id', '=', $id)->where('id', '=', $cid)->first();

 

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                return view('sitemerchant.reception-addcategory', compact('merchantheader','merchantfooter','id','getAttr'));       
            } else {
                return Redirect::to('sitemerchant');
            }


}

public function recptionUpdateitem(Request $request)
{

           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $cid = $request->cid;

                $pro_mc_id = $request->attribute;
                $pro_title = $request->itemname;
                $pro_title_ar = $request->itemname_ar;
                $pro_price = $request->price;

                $pro_desc = $request->itemdescription;
                $pro_desc_ar = $request->itemdescription_ar;
                $pro_qty  = 999;
                $pro_status = 1;
                $pro_type = 'reception_hospitality';
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');
                $pro_Img = '';


  if($file=$request->file('uploadimage'))
      {          
          $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());          
          $extension = $file->getClientOriginalExtension();
          $fileName = rand(11111,99999).'_'.time().'.'.$extension;                 
          $imageRealPath  =   $file->getRealPath();
          $thumbName      =   'thumb_'. $fileName;       
          $img = Image::make($imageRealPath);
          

          $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                    $thumb_height = ($thumb_width/$width) * $height;
                    $img->resize($thumb_width,$thumb_height);


          $img->save('receptionspic' . '/'. $thumbName);
          $file->move('receptionspic/', $fileName);            
          $pro_Img = url('/').'/receptionspic/'.$thumbName;        
       }

     
                if($cid =='')
                {
            // insert attributes into table
                DB::table('nm_product')->insert( [
                'pro_title' => $pro_title,
                'pro_title_ar' => $pro_title_ar,
                'pro_desc'=> $pro_desc,
                'pro_desc_ar'=>$pro_desc_ar, 
                'pro_Img' =>$pro_Img,
                'pro_qty' =>$pro_qty, 
                'pro_mc_id' =>$pro_mc_id, 
                'pro_price' =>$pro_price, 
                'pro_mr_id' =>$mer_id, 
                'pro_status' =>$pro_status, 
                'pro_type' =>$pro_type,      
                ]);
            // insert attributes into table


                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تم حفظ العنصر بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Item successfully saved");
                    }
                    // language for display message // 

                } 
                else 
                {
                    if($pro_Img=='')  
                    {
                    DB::table('nm_product')->where('pro_id',$cid)->where('pro_type','reception_hospitality')->where('pro_mr_id',$mer_id)->update( ['pro_title'=>  $pro_title,'pro_title_ar'=>  $pro_title_ar,'pro_desc'=>  $pro_desc,'pro_desc_ar'=> $pro_desc_ar,'pro_mc_id'=> $pro_mc_id,'pro_price'=> $pro_price,'pro_status'=> $pro_status,'pro_type'=> $pro_type]);
                    }
                    else
                    {
                      DB::table('nm_product')->where('pro_id',$cid)->where('pro_type','reception_hospitality')->where('pro_mr_id',$mer_id)->update( ['pro_title'=>  $pro_title,'pro_title_ar'=>  $pro_title_ar,'pro_desc'=>  $pro_desc,'pro_desc_ar'=> $pro_desc_ar,'pro_Img'=> $pro_Img,'pro_mc_id'=> $pro_mc_id,'pro_price'=> $pro_price,'pro_status'=> $pro_status,'pro_type'=> $pro_type]);
                    }
 
                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تم تحديث العنصر بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Item successfully updated");
                    }
                    // language for display message // 

                }




 
                return redirect()->route('reception-list-informations', ['id' => $id]);

 
              //  return view('sitemerchant.reception-list-attributes', compact('merchantheader','merchantfooter','id'));       
            } else {
                return Redirect::to('sitemerchant');
            }

}


public function recptionUpdatecategory(Request $request)
{

           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $cid = $request->cid;
                $categoryname = $request->category_name;
                $categoryname_ar = $request->category_name_ar;
                $categorydescription = $request->category_description;
                $categorydescription_ar = $request->category_description_ar;
                $status = 1;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');

                if($cid =='')
                {
            // insert attributes into table
                DB::table('nm_services_attribute')->insert( [
                'attribute_title' => $categoryname,
                'attribute_title_ar' => $categoryname_ar,
                'attribute_description'=> $categorydescription,
                'attribute_description_ar'=>$categorydescription_ar, 
                'services_id' =>$id,
                'vendor_id' =>$mer_id, 
                'status' =>$status,            
                ]);
            // insert attributes into table


                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تم حفظ الفئة بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Category successfully saved");
                    }
                    // language for display message // 

                } 
                else
                {


                DB::table('nm_services_attribute')->where('id',$cid)->where('services_id',$id)->where('vendor_id',$mer_id)->update( ['attribute_title'=>  $categoryname,'attribute_title_ar'=>  $categoryname_ar,'attribute_description'=>  $categorydescription,'attribute_description_ar'=> $categorydescription_ar]);  


                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تم تحديث الفئة بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Category successfully updated");
                    }
                    // language for display message // 

                }




 
                return redirect()->route('reception-list-attributes', ['id' => $id]);

 
              //  return view('sitemerchant.reception-list-attributes', compact('merchantheader','merchantfooter','id'));       
            } else {
                return Redirect::to('sitemerchant');
            }


}


public function receptionAdditems()
{

           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              $alldata = array();
              $cid = request()->cid;
              $id = request()->id;
              $getAttr = DB::table('nm_product')->where('pro_id',$cid)->where('pro_mr_id',$mer_id)->first();

              

           return view('sitemerchant.reception-additems', compact('merchantheader','merchantfooter','alldata','getAttr'));       
            } else {
                return Redirect::to('sitemerchant');
            }
}
 
      //Package Listing

  public function receptionListPackages(Request $request)

     {
           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              $alldata = array();
           return view('sitemerchant.reception-list-packages', compact('merchantheader','merchantfooter','id','alldata'));       
            } else {
                return Redirect::to('sitemerchant');
            }


     }


  public function addWorkersNationlity(Request $request)

     {
          if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              $alldata = array();
           return view('sitemerchant.reception-addworker-nationlity', compact('merchantheader','merchantfooter','alldata'));       
            } else {
                return Redirect::to('sitemerchant');
            }


     }






 //Order Listing

  public function receptionListOrders(Request $request)
     { 

if (Session::has('merchantid')) 
    {
    $merid  = Session::get('merchantid');
    $this->setLanguageLocaleMerchant();
    $mer_id              = Session::get('merchantid');             
    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
    $getCatlists = DB::table('nm_category')->where('parent_id','0')->get();
    $GetAlreadyServices = DB::table('nm_services')->where('stor_merchant_id',$merid)->get();


    $getDbC = DB::table('nm_product_option_value')->where('vandor_id', $merid)->where('product_option_id',2)->count(); 
    if($getDbC >=1)
    {
    $getDb = DB::table('nm_product_option_value')->where('vandor_id', $merid)->where('product_option_id',2)->get(); 
    }
    else
    {
    $getDb = '';  
    }


    return view('sitemerchant.reception-list-orders', compact('merchantheader','merchantfooter','getCatlists','GetAlreadyServices','getDb','getDbC'));       
    } else {
    return Redirect::to('sitemerchant');
    }








     }

     //Package Listing

  public function receptionListCommentsReview(Request $request)

     {            

if (Session::has('merchantid')) 
    { $getPagelimit = config('app.paginate');
    $merid  = Session::get('merchantid');
    $this->setLanguageLocaleMerchant();
    $mer_id              = Session::get('merchantid');             
    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
    $getId = request()->id;
    $reviewrating = Reviewrating::where('vandor_id',$mer_id)->where('product_id',$getId)->paginate($getPagelimit);
    return view('sitemerchant.reception-listcommentsreview', compact('merchantheader','merchantfooter','reviewrating'));       
    } else {
    return Redirect::to('sitemerchant');
    }
 
     }






 //Store Value in shopinfo

      public function storeShopinfo(Request $request)
      {
           $serviceid = request()->id;
           


      }  




     //Add Attributes

       public function addAttributes(Request $request)

      {
           if (Session::has('merchantid')) 
             {
                $id = $request->id;
                $mer_id   = Session::get('merchantid');
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
           return view('sitemerchant.add-attribute', compact('merchantheader','merchantfooter'));       
            } else {
                return Redirect::to('sitemerchant');
            }


     }


     

 }



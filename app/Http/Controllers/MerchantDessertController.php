<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Models;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon;
use File;
use App\Reviewrating;
use App\Categorylist;
use App\City;
use App\Merchant;
use App\NmServicesAttribute;

use Intervention\Image\ImageManagerStatic as Image; 
use Auth;
use Lang;
use App\NmproductOptionValue;
use App\ProductsMerchant;
use App\NmProductToOptionPrice;
use App\ProductOption;
use App\HallOffer;
use App\NmProductGallery;



class MerchantdessertController extends Controller
{
      public function __construct(){
            parent::__construct();
            // set admin Panel language
            $this->middleware(function ($request, $next) {
                        $this->setLanguageLocaleMerchant();
                        Session::get('mer_lang_code');
                        return $next($request);
           });
     }
    
 
//Add Shop info

// Start For Dessert Shop
 public function dessertShop(Request $request)
    {
      if (Session::has('merchantid')) 
      {
      $mer_id   = Session::get('merchantid');
      $id = isset($request->id) ? $request->id :'-1';
      if(!isset($request->id))
      {
      return Redirect::to('sitemerchant_dashboard');
      }
   
      $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
      $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
      $getCities = City::where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();
     
 

 if(Session::get('LoginType') =='4')
           {
            //= CHECK THE MARCHANT ACCESS ==//
              $merchantManagerid = Session::get('merchant_managerid');
              $fetchB = Categorylist::where('parent_id',$id)->where('vendor_id',$mer_id)->get();
              $ChekArr = array();
              foreach($fetchB as $chekB)
              {
              $mcid  = $chekB->mc_id;
              $fetchBC = Categorylist::where('parent_id',$mcid)->where('vendor_id',$mer_id)->where('branch_manager_id',$merchantManagerid)->count();
              if($fetchBC >=1)
              {
              $BId = Categorylist::where('parent_id',$mcid)->where('vendor_id',$mer_id)->where('branch_manager_id',$merchantManagerid)->first();
              $CP= $BId->parent_id;
              array_push($ChekArr, $CP);
              }

              } 
              $alldata = Categorylist::where('parent_id',$id)->where('vendor_id',$mer_id)->whereIn('mc_id', $ChekArr)->orderBy('mc_id','DESC');
            //= CHECK THE MARCHANT ACCESS ==//
           }
           else
           {
            $alldata = Categorylist::where('parent_id',$id)->where('vendor_id',$mer_id)->orderBy('mc_id','DESC');
           }





      $status = $request->status;
      $search =  $request->search;

      if($status!='')
      {
      $alldata =  $alldata->where('mc_status',$status);
      }
      if($search!='')
      {

      $alldata =  $alldata->where('mc_name','like','%'.$search.'%');
      }
      $alldata =  $alldata->orderBy('mc_id','DESC')->get();

      return view('sitemerchant.dessert.dessert-shop', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid','alldata'));       
      } else {
      return Redirect::to('sitemerchant');
      }
    }
// End For Dessert Shop

  // Start For Dessert Add Shop 
    public function dessertAddShop(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $services_id = $request->services_id;
                $shopid = $request->shopid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $city = City::where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();
                $alldata = Categorylist::where('mc_id',$shopid)->where('vendor_id',$mer_id )->first();
 
                return view('sitemerchant.dessert.dessert-add-shop', compact('merchantheader','merchantfooter','services_id','getCities','alldata','shopid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }


      public function storeDessertAddShop(Request $request)
        {              
             //echo $request->ssb_name;die
             $this->validate($request,[
             /*'city' => 'required|max:200',
             'hotelname' => 'required|max:150',
             'hotel_image' => 'required'*/
         ]);
              $services_id= $request->service_id;
              $sid = $request->shop_id;
               if($sid){
                  $saveservice =Categorylist::where('mc_id',$sid)->first();
                } else { 
                 $saveservice = new Categorylist;
               }
                $mer_id   = Session::get('merchantid');
               if($request->file('mc_img'))
               { 
              $file = $request->mc_img;  
              $merid  = Session::get('merchantid');
              $extension = $file->getClientOriginalExtension();
              $Image = rand(11111,99999).'_'.time().'.'.$extension;               
              $imageRealPath  =   $file->getRealPath();
              $thumbName      =   'thumb_'. $Image;       
              $img = Image::make($imageRealPath); // use this if you want facade style code
              $thumb_width = 150;
              list($width,$height) = getimagesize($imageRealPath);
              $thumb_height = ($thumb_width/$width) * $height;
              $img->resize($thumb_width,$thumb_height);
              $img->save('uploadimage/dessertimage' .'/'. $thumbName);
              $file->move('uploadimage/dessertimage/', $Image);
              $saveservice->mc_img = url('').'/uploadimage/dessertimage/'.$thumbName;
                }

              $saveservice ->mc_name = $request->mc_name;
              $saveservice ->mc_name_ar = $request->mc_name_ar;
              $saveservice ->mc_discription = $request->description;
              $saveservice ->mc_discription_ar = $request->description_ar;
              $saveservice ->vendor_id = $mer_id;
              $saveservice ->parent_id = $services_id;
              $saveservice->mc_status = 1;

                if($saveservice->save()){
                  $id = $request->parent_id;
                 if (\Config::get('app.locale') == 'ar'){

                     Session::flash('message', "وتم حفظ المطعم بنجاح");
                 }
                 else
                 {
                     Session::flash('message', "Shop successfully saved");
                 }

              }
               return redirect()->route('dessert-shop',['service_id' => $services_id]); 
        }
// End For Dessert Add Shop 

  // Start For Dessert Manager
    public function dessertManager(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.dessert.dessert-manager', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
// End For Dessert Manager

  // Start For Dessert Add Manager
    public function dessertAddManager(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.dessert.dessert-add-manager', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
// Start For Dessert Add Manager

    // Start For Dessert Branch List
    public function dessertListBranch(Request $request)
     {
           if(Session::has('merchantid')) 
      {
          $mer_id   = Session::get('merchantid');
          $services_id = request()->services_id;
          $shopid = request()->shopid;
           $status = request()->status;
           if (Session::has('merchantid')) 
              {
                  $mer_id   = Session::get('merchantid');
                  $search = $request->input('search');
                  $city = request()->city;
                  if(Session::get('LoginType') =='4')
                  {
                  //= CHECK THE MARCHANT ACCESS ==//
                  $merchantManagerid = Session::get('merchant_managerid');
                  $alldata = Categorylist::where('parent_id',$shopid)->where('branch_manager_id',$merchantManagerid)->where('vendor_id',$mer_id);
                  //= CHECK THE MARCHANT ACCESS ==//
                  }
                  else
                  {
                  $alldata = Categorylist::where('parent_id',$shopid)->where('vendor_id',$mer_id);  
                  }
 
                 if($search !='')
                 {
                      $mc_name='mc_name';
                      $mer_selected_lang_code = Session::get('mer_lang_code'); 
                   if($mer_selected_lang_code !='en')
                   {
                             $mc_name= 'mc_name_'.$mer_selected_lang_code;
                   }
                                         
                     $alldata = $alldata->where('nm_category.'.$mc_name,'LIKE',"%{$search}%");
                }

                  if($city !='')
                  {
                                                     
                     $alldata = $alldata->where('nm_category.city_id','=',"$city");
                  }
                 if($status !='')
                  {
                                                     
                     $alldata = $alldata->where('nm_category.mc_status','=',"$status");
                  }  
               
                $alldata = $alldata->orderBy('mc_id','DESC')->get();

          $servicedata =Categorylist::where('mc_id',$shopid)->where('vendor_id',$mer_id)->get();
          $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
          return view('sitemerchant.dessert.dessert-list-branch', compact('merchantheader','merchantfooter','services_id','shopid','alldata'));       
      } 
      else 
      {
          return Redirect::to('sitemerchant');
      }
   }
     }


       //Buffet store branch
 public function storeBranch(Request $request)
    {
            $merid  = Session::get('merchantid');
          //Store Resturant Image
              $sid = $request->shopid;
              $itemid = $request->itemid;
              $services_id=$request->services_id;

               if($itemid){
                  $savebranch =Categorylist::where('mc_id',$itemid)->first();
                } else { 
                 $savebranch = new Categorylist;
               }
          if($request->file('branchimage')){ 
                  $file = $request->branchimage;  

                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                 $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 220;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('buffetimage' . '/'. $thumbName);
                  $file->move('buffetimage/', $Image);
                  $savebranch->mc_img = url('').'/buffetimage/'.$thumbName;
                }
                //Addressimage
                if($request->file('address_image')){ 
                  $file = $request->address_image;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                  $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('buffetimage' . '/'. $thumbName);
                  $file->move('buffetimage/', $Image);
                  $savebranch->address_image = url('').'/buffetimage/'.$thumbName;
                }
              //Termaand condition
               if(!empty($request->mc_tnc)){ 
                  $file = $request->mc_tnc;
                  $orgname =  $file->getClientOriginalName();
                  $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                  $extension = $file->getClientOriginalExtension();
                  $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                  $file->move('./public/assets/storeimage/', $filename_c);                      
                  $Cname =   url('').'/public/assets/storeimage/'.$filename_c;
                  $savebranch->terms_conditions =$Cname;
                  
               }
               else {
                    $orgname = $request->tmcvalue; 
               }
              

              //Termaand condition arabic
               if(!empty($request->mc_tnc_ar)){ 
                  $file = $request->mc_tnc_ar;
                  $orgnamear =  $file->getClientOriginalName();
                  $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                  $extension = $file->getClientOriginalExtension();
                  $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                  $file->move('./public/assets/storeimage/', $filename_c);                      
                  $Cnamear =   url('').'/public/assets/storeimage/'.$filename_c;
                  $savebranch->terms_conditions_ar =$Cnamear;
                  
               }
               else {
                    $orgnamear = $request->tmcvaluear; 
               }    






                  $savebranch ->vendor_id = $merid;
                  $savebranch ->google_map_address = $request->google_map_address;
                 $savebranch ->longitude = $request->longitude;
                  $savebranch ->latitude = $request->latitude;   
                  $savebranch ->address = $request->address;
                  $savebranch ->address_ar = $request->address_ar;
                  $savebranch ->city_id = $request->city_id;
                  $savebranch ->parent_id = $sid;
                  $savebranch ->mc_name = $request->mc_name;
                  $savebranch ->mc_name_ar = $request->mc_name_ar;
                  $savebranch ->mc_discription = $request->about;
                  $savebranch ->mc_discription_ar = $request->about_ar;
                  $savebranch ->city_id = $request->city_id;
                  $savebranch ->terms_condition_name = $orgname;
                  $savebranch ->terms_condition_name_ar = $orgnamear;
                  $savebranch->mc_status  = 1;
                  $savebranch->branch_manager_id  =$request->manager;

                if($savebranch->save()){
                  $id = $request->parent_id;
                  if($itemid=='')
                  {
                  $itemid =  $savebranch->mc_id;
                  }
                

            if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "فرع حفظ بنجاح");
             }
             else
             {
             Session::flash('message', "Branch successfully saved");
             }

                 return redirect()->route('dessert-info',['services_id' => $services_id,'shopid' =>$sid,'itemid' =>$itemid]);
                }   
            }





// Start For Dessert Branch List
 
    // Start For Dessert Info
    public function dessertInfo(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $services_id = $request->services_id;
                $shopid = $request->shopid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 

                     $itemid = request()->itemid;
                     $fetchdata = Categorylist::where('mc_id',$itemid)->where('vendor_id',$mer_id)->first();

                      $shopdata = Categorylist::where('mc_id',$shopid)->where('vendor_id',$mer_id)->first();
                     $city = City::where('ci_con_id',10)->get();
                     $manager = Merchant::where('vendor_parent_id',$mer_id)->where('mer_staus',1)->get();
                     $city = City::where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

          
         
          return view('sitemerchant.dessert.dessert-info', compact('merchantheader','merchantfooter','shopid','services_id','city','manager','shopdata','fetchdata'));
 
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
// Start For Dessert Info
 
   // Start For Dessert Picture & Video
    public function dessertPictureVideo(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $shopid = $request->shopid;
                $services_id = $request->services_id;
                $branch_id = $request->itemid;
               
                            $getDbC = DB::table('nm_category_gallery')->where('vendor_id', $mer_id)->where('category_id', $branch_id)->count();
                            
                            $getVideos = DB::table('nm_category')->where('mc_id',$branch_id)->where('vendor_id',$mer_id)->first();
                            if($getDbC >=1)
                            {
                                $getDb = DB::table('nm_category_gallery')->where('vendor_id', $mer_id)->where('category_id', $branch_id)->get(); 

                            }
                            else
                            {
                                $getDb = '';    
                            }
                    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                return view('sitemerchant.dessert.dessert-picture-video', compact('merchantheader','merchantfooter','services_id','getCities','services_id','branch_id','shopid','getDbC','getDb','getVideos' ));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
// End For Dessert Picture & Video


     public function StoreDessertPictureVideo(Request $request)

             { 
                if (Session::has('merchantid')) 
                {

                    $validatedData = $request->validate([ 
                          'image.*' => 'image|mimes:jpg,jpeg,png,gif'
 
                    ]); 

                    $shopid = $request->shopid;
                    $services_id = $request->services_id;
                    $branch_id = $request->branch_id;
 

                    $merid  = Session::get('merchantid');
                    $this->setLanguageLocaleMerchant();
                    $mer_id              = Session::get('merchantid');             
                    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              
                    //Insert images in folder
                 
                            $catid = $request->cat_id;
                            $sid = $request->sid;
                           
                  
                    if($files=$request->file('image')){
                    foreach($files as $file){
                    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
                    //$file->move('hallpics',$name); 
                    $extension = $file->getClientOriginalExtension();
                    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
                    //$file->move('hallpics', $fileName);               
                    $imageRealPath  =   $file->getRealPath();
                    $thumbName      =   'thumb_'. $fileName;       
                    $img = Image::make($imageRealPath); // use this if you want facade style code
                     $thumb_width = 100;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);

                  
                    $img->save('uploadimage/dessertimage' . '/'. $thumbName);
                    $file->move('uploadimage/dessertimage/', $fileName);
                    $shop_Img = url('/').'/uploadimage/dessertimage/'.$thumbName; 
                       
                    /*Insert data to db*/
                    DB::table('nm_category_gallery')->insert( [
                    'image'=>   $shop_Img,
                    'category_id' =>$branch_id,
                    'vendor_id' => $merid,
                    ]);

                   
                    } 
                  }
 

                  $mc_video_description = $request->about_video;
                  $mc_video_description_ar = $request->about_video_ar;
                  $youtubevideoa = str_replace('youtu.be','www.youtube.com/embed',$request->youtubevideo);
                  DB::table('nm_category')->where('mc_id',$branch_id)->where('vendor_id',$mer_id)->update( ['mc_video_url'=>  $youtubevideoa,'mc_video_description'=>  $mc_video_description,'mc_video_description_ar'=>  $mc_video_description_ar]);

                 
                    // language for display message //
                     if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تم حفظ السجل بنجاح ");
                    }
                    else
                    {
                    Session::flash('message', "Record successfully saved");
                    }
                    // language for display message //   
                    return Redirect::back();      
                } else {
                        return Redirect::to('sitemerchant');
                }

             }


// Start For Dessert List Container
    public function dessertListContainer(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $shopid = $request->shopid;
                $services_id = $request->services_id;
                $branch_id = $request->branch_id;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();
                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $branch_id)->first(); 
                return view('sitemerchant.dessert.dessert-list-container', compact('merchantheader','merchantfooter','shopid','getCities','getSinger','services_id','branch_id'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
// End For Dessert List Container

// Start For Dessert Add Container
    public function dessertAddContainer(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
               $shopid = $request->shopid;
                $services_id = $request->services_id;
                $branch_id = $request->branch_id;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $branch_id)->first();
 
                return view('sitemerchant.dessert.dessert-add-container', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
// End For Dessert Add Container



// Start For Dessert Offer
    public function dessertListOffer(Request $request)
     {
        if (Session::has('merchantid')) 
             {   $getPagelimit = config('app.paginate');
                $shopid = $request->shopid;
                 $services_id = $request->services_id;
                 $shopid = $request->shopid;
                 $branch_id = $request->itemid;
               // die();
                $mer_id   = Session::get('merchantid');
                $this->setLanguageLocaleMerchant();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');
                $hallofferlist = HallOffer::where('vendor_id',$mer_id)->where('pro_id','=',$branch_id);
                $ourlang=\Config::get('app.locale') ;
                $search=isset($request->search)? $request->search:'';
                $status=isset($request->status)? $request->status:'';
                $language = $ourlang !='en' ? '_'.$ourlang :'';
                if(isset($request->search) && $request->search != '')
                {
                   $hallofferlist=$hallofferlist->where('title'.$language,'LIKE','%' .$request->search . '%');
                }
                if(isset($request->status) && $request->status != '')
                {
                   $hallofferlist=$hallofferlist->where('status',$request->status);
                }



                $hallofferlist= $hallofferlist->paginate($getPagelimit)->appends(request()->query());
           return view('sitemerchant.dessert.dessert-list-offer', compact('merchantheader','merchantfooter','branch_id','hallofferlist','shopid','services_id','search','status'));       
            } else {
                return Redirect::to('sitemerchant');
            }  
	   
	   
	   
     }
// End For Dessert Offer

// Start For Dessert Add Offer
    public function dessertAddOffer(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $shopid = $request->shopid;
                $services_id = $request->services_id;
                $branch_id = $request->itemid;
                $offer_id = $request->offer_id;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = City::where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();
                $dessertoffer = HallOffer::where('vendor_id',$mer_id)->where('id',$offer_id)->first();
 
                return view('sitemerchant.dessert.dessert-add-offer', compact('merchantheader','merchantfooter','branch_id','getCities','dessertoffer','shopid','services_id','offer_id'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }




// End For Dessert Add Offer


      public function storeDessertOffer(Request $request)
       {
             $mer_id   = Session::get('merchantid');
             $services_id = $request->services_id;
             $shopid = $request->shopid;
             $branch_id = $request->branch_id;
             $bid = $request->bid;

             if(isset($request->offer_id) && $request->offer_id !='')
             {
                  $offersave=  HallOffer::where('id','=',$request->offer_id)->first();
             }
             else
             {
                  $offersave = new HallOffer; 

             }
             $offersave->vendor_id  = $mer_id; 
             $offersave->title   =$request->title;
             $offersave->title_ar    = $request->title_ar;
             $offersave->start_date     = $request->Date_from;
             $offersave->end_date    = $request->date_to;
             $offersave->discount   = $request->discount;
              $offersave->coupon   = $request->coupon;
             $offersave->pro_id  = $branch_id;                
             $offersave->status    = 1;

              if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "تمت إضافة العرض بنجاح  ");
             }
             else
             {
             Session::flash('message', "Offer successfully saved");
             }
             if($offersave->save()){  
             
               return redirect()->route('dessert-list-offer',['services_id' => $services_id,'shopid' =>$shopid,'branch_id'=>$branch_id]);
             
             }     
       }

// Start For Dessert Order
    public function dessertOrder(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->services_id;
                $hid = $request->shopid;
                $itemid = $request->itemid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 



                $sid=$request->sid;
                 $searchkeyword=$request->searchkeyword;
                 $date_to=$request->date_to;
                 $End_Date=$request->from_to;
                 $order_days=$request->order_days;
                 $status=$request->status;                
                 $serachfirstfrm=$request->serachfirstfrm;

                  $getorderedproducts = DB::table('nm_order_product')->where('product_type','food')->where('product_sub_type','dessert')->where('category_id',$itemid);
                  if($searchkeyword!=''){
                  $getorderedproducts = $getorderedproducts->where('order_id',$searchkeyword);
                  }

                  if($status!=''){
                  $getorderedproducts = $getorderedproducts->where('status',$status);
                  } 

                  if($date_to!='' && $End_Date!=''){
                  $getorderedproducts = $getorderedproducts->whereDate('created_at', '>=', $date_to)->whereDate('created_at', '<=', $End_Date);
                  } 

                  $getorderedproducts = $getorderedproducts->where('merchant_id',$mer_id)->groupBy('order_id')->orderBy('created_at','DESC')->selectRaw('*,sum(nm_order_product.total_price) as sum')->get();




                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.dessert.dessert-order', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid','sid','getorderedproducts','mer_id'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }

      public function getorderdetail(Request $request){
      if (Session::has('merchantid')) 
             {

              $mer_id   = Session::get('merchantid');
                 $id = $request->services_id;
                 $hid = $request->shopid;
                 $opid = $request->opid;
                 $oid=$request->oid;
                 $cusid=$request->cusid;
                 $sid=$request->sid;
                 $itemid=$request->itemid;

              $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');
             
               $getbody_measurement = DB::table('nm_order_services_attribute')->where('product_type','food')->where('cus_id',$cusid)->where('order_id',$oid)->where('category_id',$itemid)->get();
             foreach ($getbody_measurement as $dateotherattribute) {
                 
                $getbuy_dates = DB::table('nm_order_option_value')->where('product_type','food')->where('cus_id',$cusid)->where('order_id',$oid)->where('product_id',$dateotherattribute->product_id)->get();
                $dateotherattribute->order_option_value=$getbuy_dates;

 
   $getShippingC = DB::table('nm_shipping')->where('ship_order_id',$oid)->where('shop_id',$itemid)->where('cart_sub_type','dessert')->count();
          $dateotherattribute->shippingMethod = 'N/A';
          $dateotherattribute->shippingPrice = '0';
          if($getShippingC >=1)
          { 
          $getShipping = DB::table('nm_shipping')->where('ship_order_id',$oid)->where('shop_id',$itemid)->where('cart_sub_type','dessert')->first();
          $shipping_type = $getShipping->shipping_type;
          if($shipping_type == 1){ $shipping_type = 'Aramex';}else { $shipping_type = 'Pick Logistics'; }
          $shipping_price =  $getShipping->shipping_price;
          $dateotherattribute->shippingMethod = $shipping_type;
          $dateotherattribute->shippingPrice = $shipping_price;
          }




              }
              
                return view('sitemerchant.dessert.dessert-orderdetail', compact('merchantheader','merchantfooter','id','hid','sid','getbody_measurement'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
  }
// End For Dessert Order


// Start For Dessert Reviews & Comments
    public function dessertReviewsComments(Request $request)
     {
        if (Session::has('merchantid')) 
             {  $getPagelimit = config('app.paginate');
               $merid  = Session::get('merchantid');
        $this->setLanguageLocaleMerchant();
        $shopid = request()->itemid;
        $mer_id              = Session::get('merchantid');             
        $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
        $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
        $reviewrating = Reviewrating::where('shop_id',$shopid)->orderBy('created_at','desc')->paginate($getPagelimit)->appends(request()->query());
       // return view('sitemerchant.hall_comments_review', compact('merchantheader','merchantfooter','reviewrating')); 
 
                return view('sitemerchant.dessert.dessert-reviews-and-comments', compact('merchantheader','merchantfooter','reviewrating'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 
//End For Dessert Reviews & Comments


//Start For Dessert Type
    public function dessertType(Request $request)
    {
      if(Session::has('merchantid')) 
      {
          $mer_id          = Session::get('merchantid');
          $shopid          = $request->shopid;
          $services_id     = $request->services_id;
          $branch_id       = $request->itemid;
          $getPagelimit    = config('app.paginate');
          $merchantheader  = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter  = view('sitemerchant.includes.merchant_footer'); 
          $getCities       = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();
          $status          = $request->status;
          $search          = $request->search;
          $getdesart       = ProductsMerchant::where('pro_mc_id',$branch_id)->where('pro_mr_id',$mer_id);
          if(isset($status) && ($status==0 || $status==1))
          {
            $getdesart = $getdesart->where('pro_status',$status);
          }
          if(isset($status) && ($status==44 || $status==45))
          {
            $getdesart = $getdesart->where('attribute_id',$status);
          }
          if($search!='')
          {
            $getdesart = $getdesart->where('pro_title', 'like', '%'.$search.'%');
          }
          $getdesart = $getdesart->orderBy('pro_id','DESC')->paginate($getPagelimit);                
          return view('sitemerchant.dessert.dessert-type', compact('merchantheader','merchantfooter','shopid','getCities','getdesart','services_id','branch_id'));       
      } 
      else 
      {
        return Redirect::to('sitemerchant');
      }
    }
// End For Dessert Type


// Start For Add Dessert Type
    public function dessertAddType(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $shopid = $request->shopid;
                $services_id = $request->services_id;
                $branch_id = $request->itemid;
                $product_id = $request->autoid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();
                $attributes = NmServicesAttribute::where('services_id',$branch_id)->get();

                $option = ProductOption::where('services_id','72')->get();
                $option_id='';
           
                foreach ($option  as $key => $value) {

                  if($value->option_title=='Shop by')
                  {
                   $option_id=$value->id;
                  }

                 } 
 
              $optionvalue = NmproductOptionValue::where('product_option_id',$option_id)->get();
                 
            $getproductOption = DB::table('nm_product_option_value')->where('vandor_id',$mer_id)->where('product_id',$product_id)->get();

                 $getdesart = ProductsMerchant::where('pro_id',$product_id)->where('pro_mr_id',$mer_id)->first();

                 //print_r($getdesart );
 $productGallery = NmProductGallery::where('vendor_id',$mer_id)->where('product_id',$product_id)->get();
                return view('sitemerchant.dessert.dessert-add-type', compact('merchantheader','merchantfooter','shopid','getCities','getdesart','services_id','branch_id','attributes','option_id','optionvalue','product_id','getproductOption','productGallery'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }


     public function dessertStoreType(Request $request)
     {
      //echo '<pre>';print_r($_REQUEST);die;
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $shopid = $request->shopid;
                $services_id = $request->services_id;
                $branch_id = $request->branch_id;
                $attribute_id =  $request->attribute_id;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();
                $attributes = NmServicesAttribute::where('services_id',$branch_id)->get();
                $option = ProductOption::where('services_id',$branch_id)->get();
                $attributessingal = NmServicesAttribute::where('id',$attribute_id)->first();
                 //echo '<pre>';print_r($attributessingal->attribute_title);die;
                $optionvalue='';
                $autoid=$request->proid;
 
                if($autoid!='') {
                  $dishsave =ProductsMerchant::where('pro_id',$autoid)->first();
                 } else {
                 $dishsave = new ProductsMerchant;
                }
              
             if($request->file('pro_img')){   
                      $file = $request->pro_img;  
                      $extension = $file->getClientOriginalExtension();
                      $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                      $imageRealPath  =   $file->getRealPath();
                      $thumbName      =   'thumb_'. $Image;       
                      $img = Image::make($imageRealPath); // use this if you want facade style code
                      $thumb_width = 150;
                     list($width,$height) = getimagesize($imageRealPath);
                        $thumb_height = ($thumb_width/$width) * $height;
                        $img->resize($thumb_width,$thumb_height);
                        $img->save('dessartimage' . '/'. $thumbName);
                        $file->move('dessartimage/', $Image);
                        $dishsave->pro_Img = url('').'/dessartimage/'.$thumbName;
                 } 
                     $dishsave->pro_mc_id     = $branch_id;
                     $dishsave->attribute_id  = $attribute_id;                     
                     $dishsave->pro_title    = $request->title;
                     $dishsave->pro_title_ar  = $request->title_ar;
                      $dishsave->pro_desc  = $request->pro_description;
                       $dishsave->pro_desc_ar  = $request->pro_description_ar;  

                       $dishsave->pro_weight  = $request->pro_weight;   
                     
                  if($request->attribute_id=='44'){ $dishsave->option_id='21'; }
                  if($request->attribute_id=='45'){ $dishsave->option_id='23'; }
                      $dishsave->pro_mr_id  = $mer_id;
                     $dishsave->pro_qty  = $request->aval_qty !=''? $request->aval_qty : 0;                     
                     $dishsave->pro_status  = 1;
                     $dishsave->save();

                     if($autoid=='') { 
                       $proid = DB::getPdo()->lastInsertId();
                      } else {
                       $proid = $autoid; 
                      }
                    
                      $type_perkg = $request->type_perkg;
                      $type_perkg_discount = $request->type_perkg_discount;

                      $type_perpiece = $request->type_perpiece;
                      $type_perpiece_discount = $request->type_perpiece_discount;
                      $dish_per_qty = $request->dish_per_qty;
                      $dish_per_qty_discount = $request->dish_per_qty_discount;
                      $aval_qty = $request->aval_qty;
                   
                     //Per Kg
                    
                      if($type_perkg_discount!='' && $type_perkg_discount!='0') {
                                         $discountprice =   $type_perkg-($type_perkg * ($type_perkg_discount/100));
                                        } else {
                                           $discountprice = 0;

                                        }

                        if($dish_per_qty_discount!='' && $dish_per_qty_discount!='0') {
                                         $dishdiscountprice =   $dish_per_qty-($dish_per_qty * ($dish_per_qty_discount/100));
                                        } else {
                                           $dishdiscountprice = 0;

                                        }                

                       //Per quantity


                      DB::table('nm_product_option_value')->where('product_id',$proid)->where('vandor_id', $mer_id)->delete();
                      if($request->attribute_id=='45')
                      {
                     DB::table('nm_product_option_value')->insert([
                      'product_id' =>$proid,
                      'vandor_id' =>$mer_id,
                      'product_option_id' =>'22',
                      'option_title'=>'Per Qty',
                      'option_title_ar'=>'لكل الكمية',
                      'value' =>$request->dish_per_qty, 
                      'discount' =>$request->dish_per_qty_discount,
                      'discount_price' =>$dishdiscountprice, 
                      ]);
                      }
                      else
                      {
                      DB::table('nm_product_option_value')->insert([
                      'product_id' =>$proid,
                      'vandor_id' =>$mer_id,
                      //'product_option_id' =>'22',
                      'product_option_id' =>'21',
                      'option_title'=>'Per Kilo',
                      'option_title_ar'=>'لكل كيلو',
                      'value' =>$request->type_perkg,
                      'discount' =>$request->type_perkg_discount,
                      'discount_price' =>$discountprice,
                      ]);
                      DB::table('nm_product_option_value')->insert([
                      'product_id' =>$proid,
                      'vandor_id' =>$mer_id,
                      'option_title'=>'Per Piece',
                      'option_title_ar'=>'لكل قطعة',
                      'product_option_id' =>'23',
                      'value' =>$request->type_perpiece,
                      'discount' =>$request->type_perpiece_discount,
                      ]);
                      }


// FOR PRODUCT MULTIPLE IMAGES UPLOAD //
    $files=$request->file('image');
    $newFiles = array();
    $merid  = Session::get('merchantid');
    if($files=$request->file('image')){
    foreach($files as $key=>$val){
    array_push($newFiles, $key);
    }

    $privius=$request->privius;
    $newFilesMatch = array();
    if(isset($privius) && $privius!='')
    {
    foreach($privius as $key=>$val){
    array_push($newFilesMatch, $key);
    }  
    }

    $CheckInfo = array_intersect($newFiles, $newFilesMatch);
    if(isset($privius) && $privius!='')
    {
    foreach($privius as $key=>$val){
    if(in_array($key, $CheckInfo))
    {
    DB::table('nm_product_gallery')->where('id',$val)->where('vendor_id',$merid)->delete(); 
    }
    }
    } 
    foreach($files as $file){
    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
    $extension = $file->getClientOriginalExtension();
    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
    $imageRealPath  =   $file->getRealPath();
    $thumbName      =   'thumb_'. $fileName;       
    $img = Image::make($imageRealPath); // use this if you want facade style code
    $thumb_width = 150;
    list($width,$height) = getimagesize($imageRealPath);
    $thumb_height = ($thumb_width/$width) * $height;
    $img->resize($thumb_width,$thumb_height);                  
    $img->save('dessartimage' . '/'. $thumbName);
    $file->move('dessartimage/', $fileName);
    $shop_Img = url('/').'/dessartimage/'.$thumbName; 
    /*Insert data to db*/
    DB::table('nm_product_gallery')->insert( [
    'image'=>   $shop_Img,
    'product_id' =>$proid,
    'status' => 1,
    'vendor_id' => $merid,
    ]);
    }  
        } 


 
// FOR PRODUCT MULTIPLE IMAGES UPLOAD END //

   
/*
                      if($attributessingal->attribute_title !='Dish')
                      {
                          foreach ($option  as $key => $value)
                           {
                                    if($value->option_title=='Shop by')
                                    {
                                    $option_id=$value->id;
                                    }
                            } 

                                    if( $option_id != '')
                                    {
                                    $optionvalue = NmproductOptionValue::where('product_option_id',$option_id)->get();
                                    }

                                    if($optionvalue) {
                                    foreach ($optionvalue as $key => $value) {
                                    $discountqty ='discountqty'.$value->id;
                                    $discount ='discount'.$value->id;
                                    $discountqtydata = $request->$discountqty;
                                    $discountdata = $request->$discount;
                                    DB::table('nm_product_to_option_discount')->where('pro_id',$autoid)->where('product_option_value_id', $value->id)->delete();
                                    $discountqtydata = $discountqtydata !='' ? $discountqtydata  :0;
                                    $discountdata = $discountdata !='' ? $discountdata  :0;

                                    DB::table('nm_product_to_option_discount')->insert([
                                    'pro_id' =>$proid,
                                    'vendor_id' =>$mer_id,
                                    'product_option_id' =>$value->product_option_id,
                                    'product_option_value_id' =>$value->id,
                                    'attribute_id' =>$request->attribute_id,
                                    'discount' =>$discountdata,
                                    'quantity' =>$discountqtydata,
                                    ]);
                                    }
                                    }
                                      
                            }

                            else
                            {
                                  DB::table('nm_product_to_option_discount')->insert([
                                  'pro_id' =>$proid,
                                  'vendor_id' =>$mer_id,
                                  'product_option_id' =>0,
                                  'product_option_value_id' =>0,
                                  'attribute_id' =>$request->attribute_id,
                                  'discount' =>$request->discountperqty,
                                  'quantity' =>$request->perqty,
                                  ]);
                            }

*/
                               if (\Config::get('app.locale') == 'ar'){
                                 Session::flash('message', "النوع حفظ بنجاح");
                                 }
                                 else
                                 {
                                 Session::flash('message', "Record successfully saved");
                                 }

      return redirect()->route('dessert-type',['services_id'=>$services_id,'shopid'=>$shopid,'itemid'=>$branch_id]);                               
          } else {
                 return Redirect::to('sitemerchant');
            }
     }
// End For Add Dessert Type


// Start For Add Dessert Add Branch
    public function dessertAddBranch(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.dessert.dessert-add-branch', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
// End For Add Dessert Add Branch



 } 


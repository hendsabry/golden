<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code start----------//

use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;
use App\ServiceAttribute;
use App\FoodContainerPrice;

use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\Formbusinesstype;
use App\Internalfooditems;
use App\ProductCategory;
use App\CartExternalFoodDish;
use App\Foodshopdate;
//------------wisitech code end -----------//
class FoodshopbuffetmenuController extends Controller
{  
    public function __construct(){
        parent::__construct();
		
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
     
    public function menuitemlist(Request $request,$foodtype,$shopid,$branchid,$productid)
    {
 		//print_r($_REQUEST);
		//echo $halltype;


		 if(!Session::has('customerdata.token')) 
	    		{
          	return Redirect::to('login-signup');
			}  
	$mainbranchid=$branchid;
     $shop_id=$shopid;
		if(Session::get('searchdata.basecategoryid')!=''){
		$basecategory_id=Session::get('searchdata.basecategoryid');
		}else {
		$basecategory_id=Session::get('searchdata.maincategoryid');
		}
		//$category_id=$foodtype;
		//print_r($productsessionid=Session::get('searchdata'));
    $city_id=Session::get('searchdata.cityid');
		$productsessionid=Session::get('searchdata.prodid');
		 $productid=$productid;
		$lang=Session::get('lang_file');
		$basecategoryid=Session::get('searchdata.basecategoryid');
		$cid=Session::get('searchdata.catgeoryid');
		$categoryid=$cid[0];
		$scid=Session::get('searchdata.typeofhallid');
		$subcategoryid=$scid[0];
		$shid=Session::get('searchdata.shopid');
		$shopid=$shid[0];
		$bid=Session::get('searchdata.branchid');
		$branchid=$bid[0];
		
		$adoptedservices=Session::get('searchdata.hallchoosenservices');
		

        $rids=$foodtype.'/'.$shop_id.'/'.$mainbranchid;
		
		    if ($lang == 'ar_lang') {
					 

		    		 /*
                    Language Arabic
                    */
                    //Get Menu Detail
                    $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'image', 'status')->where('services_id', '=', $productid)->where('status', '=', 1)->orderBy('attribute_title', 'ASC')->get();


                    $activecontainer = DB::table('nm_product_option_value')->select('id')->where('status', '=', 1)->where('product_id', '=', $productid)->get();
                      $act_container=array();
                         foreach($activecontainer as $key => $value) {

                          array_push($act_container, $value->id);

                         }

                    if (isset($menu)) {
                        foreach($menu as $key => $value) {
                            //get food detail according menu
                            $food = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_qty', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                            $value->food_list = $food;
                            foreach($food as $val) {
                                
                                //Get service attribute option value with container list
                                $container = DB::table('nm_product_to_option_price')->select('id', 'product_option_value_id', 'price', 'quantity','discount_price')->where('product_id', '=', $val->pro_id)->whereIn('product_option_value_id',$act_container)->orderBy('id', 'ASC')->get();
                                                    

                               // $val->container = $pro_Option;
                                $val->container_list = $container;
                                foreach($container as $cont_value) {
                                    //Get Container Price    
                                    $container_price = DB::table('nm_product_option_value')->select('id', 'option_title_ar as option_title', 'short_name')->where('id', '=', $cont_value->product_option_value_id)->get();
                                    $cont_value->container_price = $container_price;
                                }

                            }

                        }

                       
                    }





			}else{
							
					//Get menu detail
                    $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->where('services_id', '=', $productid)->where('status', '=', 1)->orderBy('attribute_title', 'ASC')->get(); 

                      $activecontainer = DB::table('nm_product_option_value')->select('id')->where('status', '=', 1)->where('product_id', '=', $productid)->get();
                      $act_container=array();
                         foreach($activecontainer as $key => $value) {

                          array_push($act_container, $value->id);

                         }


                    if (isset($menu)) {
                        foreach($menu as $key => $value) {
                            //get food detail according menu
                            $food = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_qty', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                            $value->food_list = $food;




                            foreach($food as $val) {
                                
                                //Get service attribute option value with container list
                                $container = DB::table('nm_product_to_option_price')->select('id', 'product_option_value_id', 'price', 'quantity','discount_price')->where('product_id', '=', $val->pro_id)->whereIn('product_option_value_id',$act_container)->orderBy('id', 'ASC')->get();
                                                    

                               // $val->container = $pro_Option;
                                $val->container_list = $container;
                                foreach($container as $cont_value) {
                                    //Get Container Price    
                                    $container_price = DB::table('nm_product_option_value')->select('id', 'option_title', 'short_name')->where('id', '=', $cont_value->product_option_value_id)->get();
                                    $cont_value->container_price = $container_price;
                                }

                            }

                        }

                       
                   }
					 
					 
					 
					 
			}
			//echo "<pre>";
			//print_r($menu);
			//die;
			$foodShopdate= new Foodshopdate();

            $fooddateshopdetails=$foodShopdate->Getfoodshopdate($productid,$lang);
              $fooddateshopgallery=$foodShopdate->Getgalleryimages($productid);
              $fooddateshopreview=$foodShopdate->GetshopReviews($productid);



              ////////////// other branches////////////
                    $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $productid)->first(); 
                if (!empty($category)) {
                 if ($productid != '' && $lang == 'ar_lang') {
                  $hallsubcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $mainbranchid)->where('city_id','=',$city_id)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get(); 
                    foreach ($hallsubcategory as $getchildelement) {
                          $cid=$getchildelement->mc_id;
                            $ischildthere[] = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $cid)->count();
                            $hallsubcategory->ischild=$ischildthere;
                         }

                             } else {
                    $hallsubcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $mainbranchid)->where('city_id','=',$city_id)->select('mc_id', 'mc_name', 'mc_img')->get();

                        foreach ($hallsubcategory as $getchildelement) {
                          $cid=$getchildelement->mc_id;
                            $ischildthere[] = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $cid)->count();
                            $hallsubcategory->ischild=$ischildthere;
                         }

                  }
               }
               $otherbarnch=$hallsubcategory;

		$mainmenuwithItemAndContainer=$menu; 
		$ProductCategoryselected= new ProductCategory();
		$branchinfo=$ProductCategoryselected->Getbasicfromcategory($productid,$lang);
		
				$barnchcontainerlist=$ProductCategoryselected->foodproductbranchcontainer($productid,$lang);
				//print_r($barnchcontainerlist);
        return view('newWebsite.buffet', compact('mainmenuitemtype','mainmenuwithItemAndContainer','category_id','containerlistitem','mkbasecatgory','productinfo','newproductprice','productservices','branchinfo','barnchcontainerlist','productid','fooddateshopdetails','fooddateshopgallery','fooddateshopreview','otherbarnch','rids','mainbranchid'));
 
    }


function addcartproduct(Request $request){

//dd($request);
	
	$cart_type=$request->cart_type;
	$userid=Session::get('customerdata.user_id');
	$did=array();
	$addeddishitemid=$request->addeddishid;
		$addedmainmenuids=$request->addedmainmenuid;
		$addedcontainerid=$request->newdishitemid;
		$shop_id=$request->product_id;
    $did=$request->menudishid;
    $menudishqtyst=$request->menudishqty;

	if(count($addedcontainerid)>0){

    $i=0;

		 foreach($addedcontainerid as  $key => $value){
		 			
				if($value!=''){
                $menuid=$request->newdishitemid[$i];
                $containerid=$request->newcontainerid[$i];
                $menudishqty=$request->menudishqty[$i];
            $k[]=array('product_id'=>$menuid ,'menu_id'=>$menuid,'dish'=>array('dish_id'=>$menuid,'container_id'=>$containerid,'qty'=>$menudishqty));

					/*$dishid=$did[$i];
				   $itemqty=$request->menudishqtyst[$i];
				   $container_id=$value;					
				 	 
                     $newcontainerids=explode(',',$container_id);
                     $newitemqty=explode(',',$itemqty);                      
                     $k=array();
                     foreach ($newcontainerids as $key => $value) {
                        if($newitemqty[$key]!=''){
						 $k[]=array('product_id'=>$did[$key] ,'menu_id'=>$menuid,'dish'=>array('dish_id'=>$did[$key],'container_id'=>$value,'qty'=>$menudishqtyst[$key]));
						
                        }
                      }*/
				    }
				$i= $i+1;
		    } 
		}
        
        //echo $container_id;
		$cartmenuitem['item']=$k;
    //echo "<pre>";
    //print_r($k);
	   //die;
            if (isset($cartmenuitem['item'])) {
            foreach($cartmenuitem['item'] as $value) {
            $product_id=$value['product_id'];
             /////////////// add product ///////////////////
            $product = Products::where('pro_id', '=', $product_id)->first();
                        		
                if (!empty($product)) {
                    $cart = Cart::where('user_id', '=', $userid)->first();
                    if (!empty($cart)) {
                       $cart_pro = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('product_id', $product_id)->first();

                        if (!empty($cart_pro)) {
							
                            //$cart_food_dish = CartExternalFoodDish::where('cart_id', $cart->id)->where('external_food_dish_id', $product_id)->where('category_id', $cart_pro->category_id)->delete();
                            //$cart_attribute = CartServiceAttribute::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('category_id', $cart_pro->category_id)->delete();
                           //$cart_pro = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('category_id', $shop_id)->delete();

                        }
                        

                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $userid;
                        $cart = Cart::create($cart_data); //cart entery
                    }

                      $isalreadyadded= DB::table('nm_cart_external_food_dish')->where('external_food_dish_id', '=', $product_id)->where('cart_id', '=', $cart->id)->where('container_id', '=', $value['dish']['container_id'])->first();  

                      if (!empty($isalreadyadded)) {

                          $oldqty=$isalreadyadded->quantity;
                          $dishunitprice=$isalreadyadded->price/$oldqty;

                          $newqty=$oldqty+$value['dish']['qty'];
                          $newtotal_price=$dishunitprice*$newqty;
                            $updatenewqty=array();
                            $updatenewqty['quantity'] = $newqty;
                            $updatenewqty['price'] = $newtotal_price;

                          $update_product_total = CartExternalFoodDish::where('id', '=', $isalreadyadded->id)->update($updatenewqty);

                          $update_original_product = CartProduct::where('cart_id', '=', $cart->id)->where('product_id', '=', $value['dish']['dish_id'])->where('container_id', '=', $value['dish']['container_id'])->first();

                            $new_updated_product=array();
                            $new_updated_product['total_price'] =  $newtotal_price;
                             $new_updated_product['quantity'] =  $newqty;
                              $update_product = CartProduct::where('id', '=', $update_original_product->id)->update($new_updated_product);


                      }else{

                    
                    $cart_product_data = array();
                    $cart_product_data['cart_type'] = $cart_type;
                    $cart_product_data['cart_sub_type'] = 'buffet';
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['category_id'] = $shop_id;
                    $cart_product_data['shop_id'] = $shop_id;
                    $cart_product_data['merchant_id'] = $product->pro_mr_id;
                    $cart_product_data['product_id'] = $product_id;
                    $cart_product_data['quantity'] = $value['dish']['qty'];
                    $cart_product_data['container_id'] = $value['dish']['container_id'];
                    $cart_product_data['status'] = 1;
                    $cart_product_data['review_type'] = 'shop';
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry

                        	/////////////////// end add product////////////////
 

                            $cart_attribute_data = array();
                            $cart_attribute_data['cart_type'] = $cart_type;
                            $cart_attribute_data['cart_id'] = $cart->id;
                            $cart_attribute_data['category_id'] = $product->pro_mc_id;
                            $cart_attribute_data['attribute_id'] = $value['menu_id'];
                            $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry*/
                           
                            		$container_price = DB::table('nm_product_to_option_price')->where('product_id', '=', $value['dish']['dish_id'])->where('product_option_value_id', '=', $value['dish']['container_id'])->first();                            		
                              
                                $cart_external_food = array();
                                $cart_external_food['cart_id'] = $cart->id;
                                $cart_external_food['category_id'] = $shop_id;
                                $cart_external_food['product_id'] = $value['product_id'];

                                $cart_external_food['external_food_menu_id'] = $product->attribute_id;
                                $cart_external_food['external_food_dish_id'] = $value['dish']['dish_id'];
                                $cart_external_food['container_id'] = $value['dish']['container_id'];
                                $cart_external_food['quantity'] = $value['dish']['qty'];
                                $cart_external_food['status'] = 1;
                                if(isset($container_price->discount) && $container_price->discount>0){
                                  $unitprice=$container_price->discount_price;
                                }else{
                                  $unitprice=$container_price->price;
                                }
                                $cart_external_food['price'] = $unitprice*$value['dish']['qty'];

                              

                                $cart_value = CartExternalFoodDish::create($cart_external_food); //external food entery 
                                 $cart_product = CartProduct::where('category_id', '=', $cart_value->category_id)->first();
                                
                                 $total_price['total_price'] =  $unitprice * $value['dish']['qty'];
                                $update_product = CartProduct::where('category_id', '=', $shop_id)->where('product_id', '=', $value['dish']['dish_id'])->where('container_id', '=', $value['dish']['container_id'])->update($total_price);
                            
                           }

                        }
 
                    }

   

return redirect('mycart/')->with('status', 'item added sucessfully!');


                   

               } 
            

}
 
}

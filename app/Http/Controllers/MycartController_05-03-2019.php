<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//
use App\BusinessOccasionType;
use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;

use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\Formbusinesstype;
use App\Internalfooditems;
use App\ProductCategory;
use App\CartExternalFoodDish;

use App\CartProductRent;
use App\CartServiceStaff;
use App\CartHospitality;
use App\CartInvitation;
use App\CartBodyMeasurement;
 
            
         
//------------wisitech code end -----------//
class MycartController extends Controller
{  
    public function __construct(){
        parent::__construct();
		
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
     
    public function savecartitem(Request $request)
    {    
        if(!Session::has('customerdata.token')) 
        {
         return Redirect::to('login-signup');
        }  
    
        if(Session::get('searchdata.basecategoryid')!=''){
        $basecategory_id=Session::get('searchdata.basecategoryid');
        }else {
        $basecategory_id=Session::get('searchdata.maincategoryid');
        }
        $cart_fooditem=array();
        $cart_fooddateitem=array();
        $cart_fooddessertitem=array();
        $internal_foods=array();
        $userid=Session::get('customerdata.user_id');
        $productsessionid=Session::get('searchdata.prodid');
        $productid=$productsessionid[0];
        $lang=Session::get('lang_file');
        $basecategoryid=Session::get('searchdata.basecategoryid');
        $cid=Session::get('searchdata.catgeoryid');
        $categoryid=$cid[0];
        $scid=Session::get('searchdata.typeofhallid');
        $subcategoryid=$scid[0];
        $shid=Session::get('searchdata.shopid');
        $shopid=$shid[0];
        $bid=Session::get('searchdata.branchid');
        $branchid=$bid[0];
        
        $adoptedservices=Session::get('searchdata.hallchoosenservices');
          $basecategory_ids =Session::get('searchdata.mainselectedvalue');  

        if($basecategory_ids ==2)
        {

         $occasiontype=Session::get('searchdata.weddingoccasiontype');
        }
        else
        {
              $occasiontype=Session::get('searchdata.bussinesslist');
        }
   
 

        $ProductCategoryselected= new ProductCategory();
        $eventtype=$ProductCategoryselected->eventtypeoption($occasiontype,$lang);
        if(count($eventtype) >=1)
        {
        $otype=$eventtype[0]->title;
        }
        else
        {
       $otype='';      
        }
          
            Session::put('otype',$otype);
           /////////////// get cart data from db ///////////////
                
                
            ////////////////// Hall product info ////////////
               
                
                  $cart = Cart::where('user_id', '=', $userid)->first();

                  $cart_proc = CartProduct::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->count();

 
                
                if ($cart_proc >=1) {
                     $cart_pro = CartProduct::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->get();
 $productservices=array();
 foreach( $cart_pro as  $cart_pro)
 {


 
                $hall = Products::select('pro_id','pro_mc_id')->where('pro_id', '=', $cart_pro->product_id)->first();

                 $productid=$cart_pro->product_id;
                $mkbasecatgory='';
                ////////////////// product info ////////////
             $ProductCategoryselected= new ProductCategory();
           $productinfo[]=$ProductCategoryselected->productinfo($productid,$lang);
           

 
    
 

           if($productinfo[0][0]->pro_discount_percentage==''){
             $newproductprice=$productinfo[0][0]->pro_price;
           }else{
                $newproductprice=$productinfo[0][0]->pro_disprice;
           }

                $branch_city = Category::select('mc_id','city_id')->where('mc_id', '=', $hall->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();
                if (!empty($cart) && $cart_pro->cart_type = 'hall') {
                    
                    $cart_option_value = CartOptionValue::where('cart_id', '=', $cart->id)->where('product_id', '=', $cart_pro->product_id)->where('cart_type', '=', 'hall')->with('getProductOptionValue')->get();                   
                        $productservices=$cart_option_value;
                      //$cart_pro->cart_paidservices = $cart_option_value;
                               // productinfo  echo "<pre>";
                                       //print_r($productservices);
                                      // die;
                           //array_push($productinfo[0][0]->cart_paidservices,$cart_option_value);

                    $internal_food = CartInternalFoodDish::where('cart_id', '=', $cart->id)->where('product_id', '=', $cart_pro->product_id)->with('getServiceAttribute')->with('getFood')->with('getContainer')->get();

                    $hall = CartProduct::where('cart_id', '=', $cart->id)->where('product_id', '=', $cart_pro->product_id)->where('cart_type', '=', 'hall')->with('getHall')->get();
                    $cart_service = array();
                    $internal_foods = array();
                    $hall_data = array();
                   
                    if (isset($cart_option_value)) { //get service
                        foreach($cart_option_value as $key => $value) {
                            if ($lang=='en_lang') {
                                $cart_service[$key]['option_title'] = $value->getProductOption['option_title'];
                            } else {
                                $cart_service[$key]['option_title'] = $value->getProductOption['option_title_ar'];
                            }

                            $cart_service[$key]['price'] = $value->getProductOption['price'];
                            $cart_service[$key]['no_person'] = $value->getProductOption['no_person'];
                            $cart_service[$key]['status'] = $value->getProductOption['status'];
                        }
                        
                    }
    
                    if (isset($internal_food)) {
                        //print_r($internal_food);
                        foreach($internal_food as $key => $value) { //get internal food

                        $container_price = InternalFoodContainerPrice::where('dish_id', '=', $value->internal_food_dish_id)->where('container_id','=',$value->container_id)->first();    
                        
                            if ($lang=='en_lang') {
                            if(isset($value->getServiceAttribute[0]->attribute_title) && $value->getServiceAttribute[0]->attribute_title!=''){
                            $internal_foods[$key]['menu_name'] = $value->getServiceAttribute[0]->attribute_title;   
                            }
                            else
                            {
                            $internal_foods[$key]['menu_name'] ='';
                            }
                       
                                $internal_foods[$key]['dish_name'] = $value->getFood[0]->dish_name;
                                $internal_foods[$key]['container_title'] = $value->getContainer[0]->title;
                            } else {
                                if(isset($value->getServiceAttribute[0]->attribute_title_ar) && $value->getServiceAttribute[0]->attribute_title_ar!=''){
                            $internal_foods[$key]['menu_name'] = $value->getServiceAttribute[0]->attribute_title_ar;   
                            }
                            else
                            {
                            $internal_foods[$key]['menu_name'] ='';
                            }
                                $internal_foods[$key]['dish_name'] = $value->getFood[0]->dish_name_ar;
                                $internal_foods[$key]['container_title'] = $value->getContainer[0]->title;
                            }
                            $internal_foods[$key]['dish_id'] = $value->getFood[0]->id;
                            $internal_foods[$key]['quantity'] = $internal_food[$key]->quantity;
                            $internal_foods[$key]['container_price'] = $container_price->container_price;
                            $internal_foods[$key]['dish_image'] = $value->getFood[0]->dish_image;
                            $internal_foods[$key]['status'] = $value->getFood[0]->status;

                            $internal_foods[$key]['no_people'] = $value->getContainer[0]->no_people;

                            
                           if(!empty($container_price)) {
                            if($container_price->discount_price==0 || $container_price->discount_price==null){
                                $cprice=$container_price->container_price;
                            }else{
                                $cprice=$container_price->discount_price;
                            }
                         
                                 $internal_foods[$key]['final_price'] = $cprice * $internal_food[$key]->quantity;

                           } 
                            
                        }
                    }
                
                    if (isset($hall)) {
                        foreach($hall as $key => $value) { //get hall
                            if ($lang=='en_lang') {
                                $hall_data[$key]['pro_title'] = $value->getHall[0]->pro_title;
                                $hall_data[$key]['pro_desc'] = $value->getHall[0]->pro_desc;
                                $hall_data[$key]['city'] = $branch_city->getCity->ci_name;
                            } else {
                                $hall_data[$key]['pro_title'] = $value->getHall[0]->pro_title_ar;
                                $hall_data[$key]['pro_desc'] = $value->getHall[0]->pro_desc_ar;
                                $hall_data[$key]['city'] = $branch_city->getCity->ci_name_ar;
                            }
                            $hall_data[$key]['price'] = $value->getHall[0]->pro_price;
                            $hall_data[$key]['pro_qty'] = $value->getHall[0]->pro_qty;
                            $hall_data[$key]['pro_Img'] = $value->getHall[0]->pro_Img;
                            $hall_data[$key]['pro_status'] = $value->getHall[0]->pro_status;
                        }
                    }
                     }
                  }  
                } else{
				$productinfo = array();
				$productservices=array();
				}

         // dd($productinfo);
                /////////////////////// end Hall cart data from db ////////////  
             
                  
                   
             ////////////////////////// food cart info start here//////////


                        $cart_food = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('cart_id', '=', $cart->id)
                        ->where('cart_sub_type', '=', 'buffet')->count();
                    //echo $cart_food;
                    if($cart_food>0){
                           //$cart_fooditem = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('cart_id', '=', $cart->id)->where('cart_sub_type', '=', 'buffet')->get();
                           $cart_fooditem = CartProduct::where('cart_type','food')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status')->where('cart_sub_type','buffet')->get();
                          // echo "<pre>";
                           //dd($cart_fooditem);



                           $cinfo=array();
                    if(count($cart_fooditem) > 0)
                    {
                        //dd($cart_fooditem);
                       foreach($cart_fooditem as $cartproduct) 
                       {
                          $product_id=$cartproduct->getProduct[0]->pro_id;
                            //$cart_food_dish = CartExternalFoodDish::where('cart_id', $cart->id)->where('external_food_dish_id', $product_id)->where('container_id', $cartproduct->container_id)->get();container_id
 

                            $cart_food_dish = CartExternalFoodDish::where('cart_id', $cart->id)->where('external_food_dish_id', $cartproduct->product_id)->where('container_id', $cartproduct->container_id)->get();


  if(isset($_REQUEST['test']) && $_REQUEST['test']==1)
{
  dd($cartproduct); die;
}
                             $cartproduct->cart_external_food_dish = $cart_food_dish;
                            
                             foreach($cart_food_dish as $getbuffetcontainer) 
                             {                                    
                                   $shopinfo = DB::table('nm_category')->select('mc_id','mc_name','mc_name_ar')->where('mc_id', '=', $getbuffetcontainer->category_id)->get();
                                    $cartproduct->branch_info = $shopinfo;
                                    $container_price = DB::table('nm_product_option_value')->select('id', 'option_title', 'short_name')->where('id', '=', $getbuffetcontainer->container_id)->get();
                                $cinfo[]=$container_price;
                                  
                             }  $cartproduct->dish_container_info = $cinfo;
                             
                       }
                        //print_r($cart_fooditem);
                      }
                      else
                      {
                        $cart_fooditem = array();
                      }
                   // echo "<pre>";

                    }
            /////////////////// food cart info ends here ////////////////

            //////////////////////////// dates cart start here///////////////
                      $cartType = array('dates','date');
                         $cart_datefood = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->whereIn('cart_sub_type', $cartType)->count();

                            if($cart_datefood>0){
                          
                           $cart_fooddateitem = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status')->whereIn('cart_sub_type',  $cartType)->get();
                         
                       foreach ($cart_fooddateitem as $cartdateproduct) {
                          $product_id=$cartdateproduct->getProduct[0]->pro_id;
                           $cart_food_datesdish = CartServiceAttribute::where('cart_id', $cart->id)->where('product_id', $product_id)->get();
                             $cartdateproduct->cart_date_food_dish = $cart_food_datesdish;

                             
                             $dateshopby_price = DB::table('nm_cart_option_value')->where('cart_id', $cart->id)->where('product_id', $product_id)->get();
                                    $cartdateproduct->cart_option_value = $dateshopby_price;

                             foreach ($cart_food_datesdish as $getdatescontainer) {
                                    
                                    $dateshopinfo = DB::table('nm_category')->select('mc_id', 'mc_name', 'mc_name_ar')->where('mc_id', '=', $getdatescontainer->category_id)->get();
                                    $cartdateproduct->shop_info = $dateshopinfo;

                                    


                             }
                       }
 
                    }else{
                        $cart_fooddateitem=array();
                    }
       
                    //////////////////////// dates cart end here///////////

 //////////////////////////// dessert cart start here///////////////
                    


                        $cart_dessertfood = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('cart_sub_type', '=', 'dessert')->count();

                            if($cart_dessertfood>0){
                          
                           $cart_fooddessertitem = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status')->where('cart_sub_type', '=', 'dessert')->where('cart_sub_type', '=', 'dessert')->get();
                          
                       foreach ($cart_fooddessertitem as $cartdessertproduct) {
                          $product_id=$cartdessertproduct->getProduct[0]->pro_id;
                           $cart_food_dessertsdish = CartServiceAttribute::where('cart_id', $cart->id)->where('product_id', $product_id)->get();
                             $cartdessertproduct->cart_dessert_food_dish = $cart_food_dessertsdish;

                                $c=DB::table('nm_cart_option_value')->where('cart_id', $cart->id)->where('product_id', $product_id)->count();
                             if($c>0){
                                $dessertshopby_price = DB::table('nm_cart_option_value')->where('cart_id', $cart->id)->where('product_id', $product_id)->get();
                            }else{
                                $dessertshopby_price=array();
                            }


                             
                                    $cartdessertproduct->cart_option_value = $dessertshopby_price;

                             foreach ($cart_food_dessertsdish as $getdessertcontainer) {
                                    
                                    $dessertshopinfo = DB::table('nm_category')->select('mc_id', 'mc_name', 'mc_name_ar')->where('mc_id', '=', $getdessertcontainer->category_id)->get();
                                    $cartdessertproduct->shop_info = $dessertshopinfo;

                                    


                             }
                       }
 
                    }else{
                        $cart_fooddessertitem=array();
                    }
    //////////////////////////// dessert cart end here///////////////
               
    //////////////////////////// Tailor cart start here///////////////      
    $countCart = CartProduct::where('cart_type','shopping')->where('cart_sub_type','tailor')->where('cart_id',$cart->id)->count();
    if($countCart>0)
    {
        $lang            = Session::get('lang_file');
        if(isset($lang) && $lang == 'ar_lang')
        {
          $carttailortitem = CartProduct::where('cart_type','shopping')->where('cart_sub_type','tailor')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }
        else
        {
          $carttailortitem = CartProduct::where('cart_type','shopping')->where('cart_sub_type','tailor')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        } 
        
        foreach($carttailortitem as $carttailorproduct) 
        {
          $product_id = $carttailorproduct->getProduct[0]->pro_id;                      
          $mment = DB::table('nm_cart_body_measurement')->where('cart_id',$cart->id)->where('product_id',$product_id)->get();   
          $carttailorproduct->body_measurement = $mment;            
        }
    }
    else
    {
       $carttailortitem = array(); 
    }
    //////////////////////////// Tailor cart end here///////////////

    //////////////////////////// Abaya cart start here///////////////      
    $countCart = CartProduct::where('cart_type','shopping')->where('cart_sub_type','abaya')->where('cart_id',$cart->id)->count();
    if($countCart>0)
    {
        $lang            = Session::get('lang_file');
        if(isset($lang) && $lang == 'ar_lang')
        {
          $cartabayatitem = CartProduct::where('cart_sub_type','abaya')->where('cart_type','shopping')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }
        else
        {
          $cartabayatitem = CartProduct::where('cart_sub_type','abaya')->where('cart_type','shopping')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        } 
        
        foreach($cartabayatitem as $cartabayaproduct) 
        {
          $product_id = $cartabayaproduct->getProduct[0]->pro_id;                      
          $mment = DB::table('nm_cart_body_measurement')->where('cart_id',$cart->id)->where('product_id',$product_id)->get();   
          $cartabayaproduct->body_measurement = $mment;            
        }
    }
    else
    {
       $cartabayatitem = array(); 
    }
    //////////////////////////// Abaya cart end here///////////////  

    //////////////////////////// Car Rental cart start here///////////////      
    $countCart = CartProduct::where('cart_type','car_rental')->where('cart_sub_type','car')->where('cart_id',$cart->id)->count();
    if($countCart>0)
    {
        $lang            = Session::get('lang_file');
        if(isset($lang) && $lang == 'ar_lang')
        {
          $cartcarrentaltitem = CartProduct::where('cart_type','car_rental')->where('cart_sub_type','car')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }
        else
        {
          $cartcarrentaltitem = CartProduct::where('cart_type','car_rental')->where('cart_sub_type','car')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        } 

            foreach($cartcarrentaltitem as $bookingandretaldate) 
        {
          $product_id = $bookingandretaldate->getProduct[0]->pro_id;                      
          //$mment = DB::table('nm_cart_product_attribute')->where('cart_id',$cart->id)->where('product_id',$product_id)->orderby('id','ASC')->get();
          $mment = DB::table('nm_cart_product_rent')->where('cart_id',$cart->id)->where('product_id',$product_id)->orderby('id','ASC')->get();    
          $bookingandretaldate->bookedandrentaldate = $mment;            
        }

        
    }
    else
    {
       $cartcarrentaltitem = array(); 
       
    }
    //////////////////////////// Car Rental cart end here///////////////         
     
    //////////////////////////// Travel Agency cart start here///////////////      
    $countCart = CartProduct::where('cart_type','travel')->where('cart_id',$cart->id)->count();
    if($countCart>0)
    {
        $lang  = Session::get('lang_file');
        if(isset($lang) && $lang == 'ar_lang')
        {
          $carttraveltitem = CartProduct::where('cart_type','travel')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }
        else
        {
          $carttraveltitem = CartProduct::where('cart_type','travel')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }
        if(isset($carttraveltitem[0]->product_id) && $carttraveltitem[0]->product_id!='')
        {
           $pid = $carttraveltitem[0]->product_id;
           $product_rent = DB::table('nm_cart_product_rent')->where('cart_id',$cart->id)->where('product_id',$pid)->first();
           $carttraveltitem->product_rent = $product_rent;  
        } 
           
    }
    else
    {
       $carttraveltitem = array(); 
    }
    //////////////////////////// Travel Agency cart end here///////////////   

    //////////////////////////// Music cart start here///////////////      
    $countCart = CartProduct::where('cart_type','music')->where('cart_id',$cart->id)->count();
    if($countCart>0)
    {
        $lang  = Session::get('lang_file');
        if(isset($lang) && $lang == 'ar_lang')
        {
          $cartmusictitem = CartProduct::where('cart_type','music')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }
        else
        {
          $cartmusictitem = CartProduct::where('cart_type','music')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }
/*echo count($cartmusictitem);
        foreach ($cartmusictitem as $musicrent) {

            # code...
                $pid = $musicrent->product_id;
           $product_rent = DB::table('nm_cart_product_rent')->where('cart_id',$cart->id)->where('product_id',$pid)->first();
           
                $cartmusictitem->product_rent = $product_rent; 
           
        }*/


        if(isset($cartmusictitem[0]->product_id) && $cartmusictitem[0]->product_id!='')
        {
           $pid = $cartmusictitem[0]->product_id;
           $product_rent = DB::table('nm_cart_product_rent')->where('cart_id',$cart->id)->where('product_id',$pid)->first();
           $cartmusictitem->product_rent = $product_rent;  
        } 
        //echo "<pre>";
        //print_r($cartmusictitem);
        //die;
           
    }
    else
    {
       $cartmusictitem = array(); 
    }
    //////////////////////////// Music cart end here/////////////// 

    //////////////////////////// Gold & Jewelry cart start here///////////////      
    $countCart = CartProduct::where('cart_type','shopping')->where('cart_sub_type','gold')->where('cart_id',$cart->id)->count();
    if($countCart>0)
    {
        $lang  = Session::get('lang_file');
        if(isset($lang) && $lang == 'ar_lang')
        {
          $cartgoldtitem = CartProduct::where('cart_type','shopping')->where('cart_sub_type','gold')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }
        else
        {
          $cartgoldtitem = CartProduct::where('cart_type','shopping')->where('cart_sub_type','gold')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
           
        }


        if(isset($cartgoldtitem[0]->product_id) && $cartgoldtitem[0]->product_id!='')
        {
           $pid = $cartgoldtitem[0]->product_id;
           $product_attr = DB::table('nm_cart_services_attribute')->where('cart_id',$cart->id)->where('product_id',$pid)->first();
           $cartgoldtitem->product_attr = $product_attr;  
        } 
           
    }
    else
    {
       $cartgoldtitem = array(); 
    }
    //////////////////////////// Gold & Jewelry cart end here/////////////// 
  
    //////////////////////////// Perfume cart start here///////////////      
    $countCart = CartProduct::where('cart_type','shopping')->where('cart_sub_type','perfume')->where('cart_id',$cart->id)->count();
    if($countCart>0)
    {
        $lang  = Session::get('lang_file');
        if(isset($lang) && $lang == 'ar_lang')
        {
          $cartperfumetitem = CartProduct::where('cart_type','shopping')->where('cart_sub_type','perfume')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }
        else
        {
          $cartperfumetitem = CartProduct::where('cart_type','shopping')->where('cart_sub_type','perfume')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }
        if(isset($cartperfumetitem[0]->product_id) && $cartperfumetitem[0]->product_id!='')
        {
           $pid = $cartperfumetitem[0]->product_id;
           $product_attr = DB::table('nm_cart_services_attribute')->where('cart_id',$cart->id)->where('product_id',$pid)->first();
           $cartperfumetitem->product_attr = $product_attr;  
        } 
           
    }
    else
    {
       $cartperfumetitem = array(); 
    }
    //////////////////////////// Perfume cart end here///////////////

    //////////////////////////// Dresses cart start here///////////////      
    $countCart = CartProduct::where('cart_type','shopping')->where('cart_sub_type','dress')->where('cart_id',$cart->id)->count();
    if($countCart>0)
    {
        $lang  = Session::get('lang_file');
        if(isset($lang) && $lang == 'ar_lang')
        {
          $cartdressitem = CartProduct::where('cart_type','shopping')->where('cart_sub_type','dress')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }
        else
        {
          $cartdressitem = CartProduct::where('cart_type','shopping')->where('cart_sub_type','dress')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }
        if(isset($cartdressitem[0]->product_id) && $cartdressitem[0]->product_id!='')
        {
           $pid = $cartdressitem[0]->product_id;
           $product_attr = DB::table('nm_cart_services_attribute')->where('cart_id',$cart->id)->where('product_id',$pid)->first();
           $cartdressitem->product_attr = $product_attr;  
        } 
           
    }
    else
    {
       $cartdressitem = array(); 
    }
    //////////////////////////// Dresses cart end here///////////////

    ////////////////////////////  makeup product start here///////////////      
    $makeupcostmeticcountCart = CartProduct::where('cart_type','beauty')->where('cart_sub_type','makeup')->where('cart_id',$cart->id)->count();
    if($makeupcostmeticcountCart>0)
    {
        $lang  = Session::get('lang_file');
        if(isset($lang) && $lang == 'ar_lang')
        {
          $cartmakeupcosmeticitem = CartProduct::where('cart_type','beauty')->where('cart_sub_type','makeup')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }
        else
        {
          $cartmakeupcosmeticitem = CartProduct::where('cart_type','beauty')->where('cart_sub_type','makeup')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }
           
    }
    else
    {
       $cartmakeupcosmeticitem = array(); 
    }
    //////////////////////////// Makeup Product cart end here///////////////



    ////////////////////////////  beauty center start here///////////////      
    $beautycentercountCart = CartProduct::where('cart_type','beauty')->where('cart_sub_type','beauty_centers')->where('cart_id',$cart->id)->count();
    if($beautycentercountCart>0)
    {
        $lang  = Session::get('lang_file');
        if(isset($lang) && $lang == 'ar_lang')
        {
          $cartbeautycenteritem = CartProduct::where('cart_type','beauty')->where('cart_sub_type','beauty_centers')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }
        else
        {
          $cartbeautycenteritem = CartProduct::where('cart_type','beauty')->where('cart_sub_type','beauty_centers')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }

        if(isset($cartbeautycenteritem[0]->product_id) && $cartbeautycenteritem[0]->product_id!='')
        {
            foreach ($cartbeautycenteritem as $getstaffandattribute) {
               
          
           $pid = $getstaffandattribute->product_id;
           $bid = $cartbeautycenteritem[0]->branch_id;
           $staffdata = DB::table('nm_cart_services_staff')->where('cart_id',$cart->id)->where('service_id',$pid)->first();
           if(count($staffdata)>0){
                $getstaffandattribute->booked_staffandotherinformation = $staffdata; 
           } 
             }
        } 
           
    }
    else
    {
       $cartbeautycenteritem = array(); 
    }
    //////////////////////////// Makeup Product cart end here///////////////



////////////////////////////  SPA center start here///////////////      
    $spacountCart = CartProduct::where('cart_type','beauty')->where('cart_sub_type','spa')->where('cart_id',$cart->id)->count();
    if($spacountCart>0)
    {
        $lang  = Session::get('lang_file');
        if(isset($lang) && $lang == 'ar_lang')
        {
          $cartspaitem = CartProduct::where('cart_type','beauty')->where('cart_sub_type','spa')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }
        else
        {
          $cartspaitem = CartProduct::where('cart_type','beauty')->where('cart_sub_type','spa')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }

        if(isset($cartspaitem[0]->product_id) && $cartspaitem[0]->product_id!='')
        {
            foreach ($cartspaitem as $getspastaffandattribute) {
               
          
           $pid = $getspastaffandattribute->product_id;
           $bid = $cartspaitem[0]->branch_id;
           $staffspadata = DB::table('nm_cart_services_staff')->where('cart_id',$cart->id)->where('service_id',$pid)->first();
           if(count($staffspadata)>0){
                $getspastaffandattribute->booked_staffandotherinformation = $staffspadata; 
           } 
             }
        } 
           
    }
    else
    {
       $cartspaitem = array(); 
    }
    //////////////////////////// SPA Product cart end here///////////////




////////////////////////////  Barbar center start here///////////////      
    $men_salooncountCart = CartProduct::where('cart_type','beauty')->where('cart_sub_type','men_saloon')->where('cart_id',$cart->id)->count();
    if($men_salooncountCart>0)
    {
        $lang  = Session::get('lang_file');
        if(isset($lang) && $lang == 'ar_lang')
        {
          $cartmen_saloonitem = CartProduct::where('cart_type','beauty')->where('cart_sub_type','men_saloon')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }
        else
        {
          $cartmen_saloonitem = CartProduct::where('cart_type','beauty')->where('cart_sub_type','men_saloon')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }

        if(isset($cartmen_saloonitem[0]->product_id) && $cartmen_saloonitem[0]->product_id!='')
        {
            foreach ($cartmen_saloonitem as $getsaloonstaffandattribute) {
               
          
           $pid = $getsaloonstaffandattribute->product_id;
           $bid = $cartmen_saloonitem[0]->branch_id;
           $staffsaloondata = DB::table('nm_cart_services_staff')->where('cart_id',$cart->id)->where('service_id',$pid)->first();
           if(count($staffsaloondata)>0){
                $getsaloonstaffandattribute->booked_staffandotherinformation = $staffsaloondata; 
           } 
             }
        } 
           
    }
    else
    {
       $cartmen_saloonitem = array(); 
    }
//////////////////////////// SPA Product cart end here///////////////


////////////////////////////  Makeup Artist start here///////////////      
    $makeup_artistscountCart = CartProduct::where('cart_type','beauty')->where('cart_sub_type','makeup_artists')->where('cart_id',$cart->id)->count();
    if($makeup_artistscountCart>0)
    {
        $lang  = Session::get('lang_file');
        if(isset($lang) && $lang == 'ar_lang')
        {
          $cartmakeup_artistitem = CartProduct::where('cart_type','beauty')->where('cart_sub_type','makeup_artists')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }
        else
        {
          $cartmakeup_artistitem = CartProduct::where('cart_type','beauty')->where('cart_sub_type','makeup_artists')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }

        if(isset($cartmakeup_artistitem[0]->product_id) && $cartmakeup_artistitem[0]->product_id!='')
        {
            foreach ($cartmakeup_artistitem as $getmakeup_artiststaffandattribute) {
               
          
           $pid = $getmakeup_artiststaffandattribute->product_id;
           $bid = $cartmakeup_artistitem[0]->branch_id;
           $staffmakeup_artistdata = DB::table('nm_cart_services_staff')->where('cart_id',$cart->id)->where('service_id',$pid)->first();
           if(count($staffmakeup_artistdata)>0){
                $getmakeup_artiststaffandattribute->booked_staffandotherinformation = $staffmakeup_artistdata; 
           } 
             }
        } 
           
    }
    else
    {
       $cartmakeup_artistitem = array(); 
    }
////////////////////////////  Makeup Artist cart end here///////////////




////////////////////////////  Makeup clinic cosmetic start here///////////////      
    $clinic_cosmeticcountCart = CartProduct::where('cart_type','clinic')->where('cart_sub_type','cosmetic')->where('cart_id',$cart->id)->count();
    if($clinic_cosmeticcountCart>0)
    {
        $lang  = Session::get('lang_file');
        if(isset($lang) && $lang == 'ar_lang')
        {
          $cartclinic_cosmeticitem = CartProduct::where('cart_type','clinic')->where('cart_sub_type','cosmetic')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }
        else
        {
          $cartclinic_cosmeticitem = CartProduct::where('cart_type','clinic')->where('cart_sub_type','cosmetic')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
        }

        foreach($cartclinic_cosmeticitem as $getclinic_cosmeticstaffandattribute) 
        {        
          $pid = $getclinic_cosmeticstaffandattribute->product_id;
          $bid = $getclinic_cosmeticstaffandattribute->branch_id;
          $staffclinic_cosmeticdata = DB::table('nm_cart_services_staff')->where('cart_id',$cart->id)->where('service_id',$pid)->first();
          if(count($staffclinic_cosmeticdata)>0)
          {
            $getclinic_cosmeticstaffandattribute->booked_staffandotherinformation = $staffclinic_cosmeticdata; 
          }
        }
        //dd($getclinic_cosmeticstaffandattribute->booked_staffandotherinformation);
    }
    else
    {
       $cartclinic_cosmeticitem = array(); 
    }
    
////////////////////////////  Makeup clinic cosmetic cart end here///////////////


////////////////////////////  Makeup clinic skin start here///////////////      
$clinic_skincountCart = CartProduct::where('cart_type','clinic')->where('cart_sub_type','skin')->where('cart_id',$cart->id)->count();
if($clinic_skincountCart>0)
{
    $lang  = Session::get('lang_file');
    if(isset($lang) && $lang == 'ar_lang')
    {
      $cartclinic_skinitem = CartProduct::where('cart_type','clinic')->where('cart_sub_type','skin')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
    }
    else
    {
      $cartclinic_skinitem = CartProduct::where('cart_type','clinic')->where('cart_sub_type','skin')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
    }

    if(isset($cartclinic_skinitem[0]->product_id) && $cartclinic_skinitem[0]->product_id!='')
    {
        foreach ($cartclinic_skinitem as $getclinic_skinstaffandattribute) {
           
      
       $pid = $cartclinic_skinitem[0]->product_id;
       $bid = $cartclinic_skinitem[0]->branch_id;
       $staffclinic_skindata = DB::table('nm_cart_services_staff')->where('cart_id',$cart->id)->where('service_id',$pid)->first();
       if(count($staffclinic_skindata)>0){
            $getclinic_skinstaffandattribute->booked_staffandotherinformation = $staffclinic_skindata; 
       } 
         }
    } 
       
}
else
{
   $cartclinic_skinitem = array(); 
}  
////////////////////////////  Makeup clinic skin cart end here///////////////

//////////////////////////// Electronics cart start here///////////////      
$countCart = CartProduct::where('cart_type','occasion')->where('cart_sub_type','invitations')->where('cart_id',$cart->id)->count();
if($countCart>0)
{
    $lang  = Session::get('lang_file');
    if(isset($lang) && $lang == 'ar_lang')
    {
      $cartinvitationsitem = CartProduct::where('cart_type','occasion')->where('cart_sub_type','invitations')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
    }
    else
    {
      $cartinvitationsitem = CartProduct::where('cart_type','occasion')->where('cart_sub_type','invitations')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
    }
    if(isset($cartinvitationsitem[0]->product_id) && $cartinvitationsitem[0]->product_id!='')
    {
       $pid = $cartinvitationsitem[0]->product_id;
       $product_attr = DB::table('nm_cart_invitation')->where('cart_id',$cart->id)->where('packge_id',$pid)->first();
       $cartinvitationsitem->product_attr = $product_attr;  
    } 
       
}
else
{
   $cartinvitationsitem = array(); 
}
/////////////////////////// Electronics cart end here///////////////

//////////////////////////// Kosha cart start here///////////////      
$countCart = CartProduct::where('cart_type','occasion')->where('cart_sub_type','cosha')->where('cart_id',$cart->id)->count();
if($countCart>0)
{
    $lang  = Session::get('lang_file');
    $attr=array('Design Your Kosha','Ready made Kosha');

    $cartcoshaitem = DB::table('nm_cart_services_attribute')->where('cart_id',$cart->id)->where('cart_type','occasion')->whereIn('attribute_title',$attr)->get();
    if(isset($lang) && $lang == 'ar_lang')
    {
      //$cartcoshaitem = CartProduct::where('cart_type','occasion')->where('cart_sub_type','cosha')->where('cart_id',$cart->id)->where('quantity','>',0)->where('status',1)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
    }
    else
    {
         
      //$cartcoshaitem = CartProduct::where('cart_type','occasion')->where('cart_sub_type','cosha')->where('cart_id',$cart->id)->where('quantity','>',0)->where('status',1)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
    }
    /*if(isset($cartcoshaitem[0]->product_id) && $cartcoshaitem[0]->product_id!='')
    {
       $pid = $cartcoshaitem[0]->product_id;
       $product_attr = DB::table('nm_cart_services_attribute')->where('cart_id',$cart->id)->where('product_id',$pid)->first();
       $cartcoshaitem->product_attr = $product_attr;  
    } */
       
}
else
{
   $cartcoshaitem = array(); 
}
//////////////////////////// Kosha cart end here///////////////

//////////////////////////// Photography start here///////////////      
$countCart = CartProduct::where('cart_type','occasion')->where('cart_sub_type','photography')->where('cart_id',$cart->id)->count();
if($countCart>0)
{
    $lang  = Session::get('lang_file');
    if(isset($lang) && $lang == 'ar_lang')
    {
      $cartphotoitem = CartProduct::where('cart_type','occasion')->where('cart_sub_type','photography')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
    }
    else
    {
      $cartphotoitem = CartProduct::where('cart_type','occasion')->where('cart_sub_type','photography')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
    }
    if(isset($cartphotoitem[0]->product_id) && $cartphotoitem[0]->product_id!='')
    {
       $pid = $cartphotoitem[0]->product_id;
       $product_attr = DB::table('nm_cart_services_attribute')->where('cart_id',$cart->id)->where('product_id',$pid)->first();
       $cartphotoitem->product_attr = $product_attr;  
    } 
       
}
else
{
   $cartphotoitem = array(); 
}
//////////////////////////// Photography cart end here///////////////

/////////////////////////// Video Graphy start here///////////////      
$countCart = CartProduct::where('cart_type','occasion')->where('cart_sub_type','video')->where('cart_id',$cart->id)->count();
if($countCart>0)
{
    $lang  = Session::get('lang_file');
    if(isset($lang) && $lang == 'ar_lang')
    {
      $cartvideographyitem = CartProduct::where('cart_type','occasion')->where('cart_sub_type','video')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
    }
    else
    {
      $cartvideographyitem = CartProduct::where('cart_type','occasion')->where('cart_sub_type','video')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
    }
    if(isset($cartvideographyitem[0]->product_id) && $cartvideographyitem[0]->product_id!='')
    {
       $pid = $cartvideographyitem[0]->product_id;
       $product_attr = DB::table('nm_cart_services_attribute')->where('cart_id',$cart->id)->where('product_id',$pid)->first();
       $cartvideographyitem->product_attr = $product_attr;  
    } 
       
}
else
{
   $cartvideographyitem = array(); 
}
//////////////////////////// Video Graphy cart end here///////////////

/////////////////////////// Roses start here///////////////      
$countCart = CartProduct::where('cart_type','occasion')->where('cart_sub_type','roses')->where('cart_id',$cart->id)->count();
if($countCart>0)
{
    $lang  = Session::get('lang_file');
    if(isset($lang) && $lang == 'ar_lang')
    {
      $cartrosesitem = CartProduct::where('cart_type','occasion')->where('cart_sub_type','roses')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
    }
    else
    {
      $cartrosesitem = CartProduct::where('cart_type','occasion')->where('cart_sub_type','roses')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
    }
    if(isset($cartrosesitem[0]->product_id) && $cartrosesitem[0]->product_id!='')
    {
       $pid = $cartrosesitem[0]->product_id;
       $product_attr = DB::table('nm_cart_services_attribute')->where('cart_id',$cart->id)->where('product_id',$pid)->first();
       $cartrosesitem->product_attr = $product_attr;  
    } 
       
}
else
{
   $cartrosesitem = array(); 
}
//////////////////////////// Roses cart end here///////////////
  
/////////////////////////// Special Events start here///////////////      
$countCart = CartProduct::where('cart_type','occasion')->where('cart_sub_type','events')->where('cart_id',$cart->id)->count();
if($countCart>0)
{
    $lang  = Session::get('lang_file');
    if(isset($lang) && $lang == 'ar_lang')
    {
      $carteventsitem = CartProduct::where('cart_type','occasion')->where('cart_sub_type','events')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
    }
    else
    {
      $carteventsitem = CartProduct::where('cart_type','occasion')->where('cart_sub_type','events')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
    }
    if(isset($carteventsitem[0]->product_id) && $carteventsitem[0]->product_id!='')
    {
       $pid = $carteventsitem[0]->product_id;
       $product_attr = DB::table('nm_cart_services_attribute')->where('cart_id',$cart->id)->where('product_id',$pid)->first();
       $carteventsitem->product_attr = $product_attr;  
    } 
       
}
else
{
   $carteventsitem = array(); 
}
//////////////////////////// Reception & Hospitality cart end here/////////////// 

/////////////////////////// Special Events start here///////////////      
$countCart = CartProduct::where('cart_type','occasion')->where('cart_sub_type','hospitality')->where('cart_id',$cart->id)->count();
if($countCart>0)
{
    $lang  = Session::get('lang_file');
    if(isset($lang) && $lang == 'ar_lang')
    {
      $carthospitalityitem = CartProduct::where('cart_type','occasion')->where('cart_sub_type','hospitality')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
    }
    else
    {
      $carthospitalityitem = CartProduct::where('cart_type','occasion')->where('cart_sub_type','hospitality')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->get();
    }
    if(isset($carthospitalityitem[0]->product_id) && $carthospitalityitem[0]->product_id!='')
    {
        $attarr=array('Design Your Package','Package');
       $pid = $carthospitalityitem[0]->product_id;
       $product_attr = DB::table('nm_cart_services_attribute')->where('cart_id',$cart->id)->whereIn('attribute_title',$attarr)->get();
       $carthospitalityitem->product_attr = $product_attr;  
    } 
       
}
else
{
   $carthospitalityitem = array(); 
}
//////////////////////////// Reception & Hospitality cart end here/////////////// 
//cart_pro
 return view('mycart', compact('mainmenuitemtype','cart_pro','mainmenuwithItemAndContainer','category_id','containerlistitem','mkbasecatgory','productinfo','newproductprice','productservices','otype','cart_service','internal_foods','hall_data','cart_fooditem','cart_fooddateitem','cart_fooddessertitem','carttailortitem','cartabayatitem','cartcarrentaltitem','carttraveltitem','cartmusictitem','cartgoldtitem','cartperfumetitem','cartmakeupcosmeticitem','cartbeautycenteritem','cartspaitem','cartmen_saloonitem','cartmakeup_artistitem','cartdressitem','cartclinic_cosmeticitem','cartclinic_skinitem','cartinvitationsitem','cartcoshaitem','cartphotoitem','cartvideographyitem','cartrosesitem','carteventsitem','carthospitalityitem'));
   }

   public function deleteCartproduct(Request $request)
   {

            $Pid = $request->pid;
             $ptype = $request->ptype;
            $User = Session::get('customerdata'); 
            $UserID =  $User['user_id'];
            $cart = Cart::where('user_id', '=', $UserID)->first();


            if($ptype == 'buffetdelfood')
            {
            $cart_pro = CartExternalFoodDish::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->first();
            }
            elseif($ptype == 'hall')
            {
            $cart_pro = CartProduct::where('product_id', '=', $Pid)->where('cart_id', '=', $cart->id)->first();
            }
            elseif($ptype == 'hall_attr')
            {
            $cart_pro = CartOptionValue::where('product_option_value_id', '=', $Pid)->where('cart_id', '=', $cart->id)->first();

            }
            elseif($ptype == 'hall_internal')
            {
            $cart_pro = CartInternalFoodDish::where('internal_food_dish_id', '=', $Pid)->where('cart_id', '=', $cart->id)->first();
            }
            elseif($ptype == 'food')
            {
            $cart_pro = CartOptionValue::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->first();
            }
            elseif($ptype == 'dessertfood')
            {
            $cart_pro = CartOptionValue::where('product_id', '=', $Pid)->where('cart_id', '=', $cart->id)->first();
            }
            elseif($ptype == 'datesfood')
            {
            $cart_pro = CartOptionValue::where('product_id', '=', $Pid)->where('cart_id', '=', $cart->id)->first();
            }elseif($ptype == 'cosha'){
            $cart_pro = CartServiceAttribute::where('cart_product_id', '=', $Pid)->where('cart_id', '=', $cart->id)->first();    
            }
            elseif($ptype == 'hospitality'){
            $cart_pro = CartServiceAttribute::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->first();    
            }
            else
            {
            $cart_pro = CartProduct::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->first();
            }



            if (!empty($cart) && !empty($cart_pro)) {
             if($ptype == 'buffetdelfood')
            {
              CartProduct::where('cart_id', '=', $cart->id)->where('product_id', '=', $cart_pro->product_id)->where('container_id', '=', $cart_pro->container_id)->delete();
              CartExternalFoodDish::where('cart_id', '=', $cart->id)->where('id', '=', $Pid)->delete();
            }elseif($ptype == 'cosha'){

                CartProduct::where('cart_id', '=', $cart->id)->where('product_id', '=', $cart_pro->product_id)->where('id', '=', $Pid)->delete();
                $attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('cart_product_id', $Pid)->delete();                
            }
            elseif($ptype == 'hospitality'){

                if($cart_pro->attribute_title=='Design Your Package'){
                    $cart_prod = CartProduct::where('cart_sub_type', '=', 'hospitality')->where('buy_rent', '=', 'design')->where('cart_id', '=', $cart->id)->first();
                    $newtotal=$cart_prod->total_price-$cart_pro->total_price;
                    CartProduct::where('cart_id', '=', $cart->id)->where('id', '=', $cart_prod->id)->update(['total_price'=> $newtotal]);
                }
                 if($cart_pro->attribute_title=='Package'){
                    $cart_prod = CartProduct::where('cart_sub_type', '=', 'hospitality')->where('buy_rent', '=', 'Package')->where('cart_id', '=', $cart->id)->first();
                    $newtotal=$cart_prod->total_price-$cart_pro->total_price;
                    CartProduct::where('cart_id', '=', $cart->id)->where('id', '=', $cart_prod->id)->update(['total_price'=> $newtotal]);
                }

                 CartServiceAttribute::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->delete();  
                $istilldesignproduct=CartServiceAttribute::where('attribute_title', '=', 'Design Your Package')->where('cart_id', '=', $cart->id)->count();
                if($istilldesignproduct<1){
                   CartProduct::where('cart_id', '=', $cart->id)->where('buy_rent', '=', 'design')->where('cart_sub_type', '=', 'hospitality')->delete();
                    CartHospitality::where('cart_id', '=', $cart->id)->where('order_type', '=', 'Design Your Package')->delete();  
                }   

                $istillPackageproduct=CartServiceAttribute::where('attribute_title', '=', 'Package')->where('cart_id', '=', $cart->id)->count();
                if($istillPackageproduct<1){
                   CartProduct::where('cart_id', '=', $cart->id)->where('buy_rent', '=', 'Package')->where('cart_sub_type', '=', 'hospitality')->delete(); 
                    CartHospitality::where('cart_id', '=', $cart->id)->where('order_type', '=', 'Package')->delete(); 
                }  


            }
            elseif($ptype == 'hall')
            {
            CartProduct::where('cart_id', '=', $cart->id)->where('product_id', '=', $Pid)->delete();
            CartInternalFoodDish::where('cart_id', '=', $cart->id)->where('product_id', '=', $Pid)->delete();
            CartOptionValue::where('cart_id', '=', $cart->id)->where('product_id', '=', $Pid)->delete();
            CartOption::where('cart_id', '=', $cart->id)->where('product_id', '=', $Pid)->delete();

            }
             elseif($ptype == 'hall_attr')
            {
            $cart_pro = CartOptionValue::where('product_option_value_id', '=', $Pid)->where('cart_id', '=', $cart->id)->delete();

            }
            elseif($ptype == 'hall_internal')
            {
            $cart_pro = CartInternalFoodDish::where('internal_food_dish_id', '=', $Pid)->where('cart_id', '=', $cart->id)->delete();
            }
            elseif($ptype == 'food')
            {
            $cart_pro = CartOptionValue::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->delete();
            $cart_last_item = CartOptionValue::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->count();
                if($cart_last_item<1){
                    CartProduct::where('cart_id', '=', $cart->id)->where('cart_type', '=', $ptype)->delete();
                 }
                   $attribute = CartServiceAttribute::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('product_id', $Pid)->delete();
            }
            elseif($ptype == 'datesfood')
            {
            $cart_pro = CartOptionValue::where('product_id', '=', $Pid)->where('cart_id', '=', $cart->id)->delete();
            $cart_last_item = CartOptionValue::where('product_id', '=', $Pid)->where('cart_id', '=', $cart->id)->count();
            
                if($cart_last_item<1){
                    CartProduct::where('cart_id', '=', $cart->id)->where('cart_type', '=', 'food')->where('product_id', $Pid)->delete();
                 }
                   $attribute = CartServiceAttribute::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('product_id', $Pid)->delete();
            }

            elseif($ptype == 'dessertfood')
            {
            $cart_pro = CartOptionValue::where('product_id', '=', $Pid)->where('cart_id', '=', $cart->id)->delete();
            $cart_last_item = CartOptionValue::where('product_id', '=', $Pid)->where('cart_id', '=', $cart->id)->count();
            
                if($cart_last_item<1){
                    CartProduct::where('cart_id', '=', $cart->id)->where('cart_type', '=', 'food')->where('product_id', $Pid)->delete();
                 }
                   $attribute = CartServiceAttribute::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('product_id', $Pid)->delete();
            }
            else
            {
            CartProduct::where('cart_id', '=', $cart->id)->where('id', '=', $Pid)->delete();
            CartOption::where('cart_id', '=', $cart->id)->where('id', '=', $Pid)->delete();
            CartOptionValue::where('cart_id', '=', $cart->id)->where('id', '=', $Pid)->delete();
            CartServiceAttribute::where('cart_id', '=', $cart->id)->where('id', '=', $Pid)->delete();

            CartProductRent::where('cart_id', '=', $cart->id)->where('id', '=', $Pid)->delete();
            CartServiceStaff::where('cart_id', '=', $cart->id)->where('id', '=', $Pid)->delete();
            CartHospitality::where('cart_id', '=', $cart->id)->where('id', '=', $Pid)->delete();
            CartInvitation::where('cart_id', '=', $cart->id)->where('id', '=', $Pid)->delete();
            CartExternalFoodDish::where('cart_id', '=', $cart->id)->where('id', '=', $Pid)->delete();
            CartBodyMeasurement::where('cart_id', '=', $cart->id)->where('id', '=', $Pid)->delete(); 
            }
             



           
              
                }

              
echo "1";

 
   }

function checkcartproductqty(Request $request){
            $Pid = $request->pid;
            $type = $request->type;
            $qty = $request->qty;
            $productcartid = $request->productcartid;
            $size=$request->size;
                if($type=='dress'){
                     $isrentproduct=Db::table('nm_cart_product')->where('id',$Pid)->first();
                            $isproduct = DB::table('nm_product_option_value')->where('product_id',$productcartid)->where('option_title',$size)->first();
                            if($isrentproduct->buy_rent=='rent'){ $nqty=$isproduct->value_ar; }else{ $nqty=$isproduct->value;}
                        if($nqty<$qty){
                            $notavailableqty=0;
                        }else{
                           $notavailableqty=1; 
                        }
                }elseif($type=='buffetfood'){
                    $notavailableqty=1;
              }elseif($type=='tailor'){
                    $notavailableqty=1;
              }elseif($type=='abaya'){
                    $notavailableqty=1;
              }else{
                $isproduct = DB::table('nm_product')->where('pro_id',$productcartid)->first();
                if($isproduct->pro_qty<$qty){
                    $notavailableqty=0;
                }else{
                   $notavailableqty=1; 
                }

                }
                
                if($type=='abaya')
                {
                echo 1;
                }
                else
                {
                 echo $notavailableqty;        
                }
               
}




public function updateCartproductqty(Request $request)
   {

            $Pid = $request->pid;
            $type = $request->type;
            $qty = $request->qty;


            $User = Session::get('customerdata'); 
            $UserID =  $User['user_id'];
            $cart = Cart::where('user_id', '=', $UserID)->first(); 


   if($type == 'kosha')
{ 
         

        $getPro = CartProduct::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->first();
        $getqtyfromattr=CartServiceAttribute::where('cart_id', '=', $cart->id)->where('product_id','=', $getPro->product_id)->where('cart_type','occasion')->where('cart_product_id',$Pid)->first();
        $quantity =  $getqtyfromattr->quantity; 
        $total_price =  $getqtyfromattr->total_price;      
        if($quantity == 0){  $quantity =  1; } 
        if($total_price == 0){  $total_price =  1; } 

        $singleItemPrice = $total_price/$quantity;

        $proQty = $request->qty;

        $Tp = $singleItemPrice * $proQty;


/////////////// new code/////////////

 //Main KOSHA design case only
        if($getPro->buy_rent=='design'){
                $cart_pro =   CartServiceAttribute::where('cart_id', '=', $cart->id)->where('product_id','=', $getPro->product_id)->update(['quantity'=> $proQty, 'total_price'=> $Tp]);

                CartProduct::where('quantity', '!=', 0)->where('cart_id', '=', $cart->id)->where('id', '=', $Pid)->where('product_id','=', $getPro->product_id)->update(['total_price'=> $Tp,'quantity'=> $proQty]);

                    $total_price = 0;


                    $getProinfo = CartProduct::where('cart_id', '=', $cart->id)->where('id', '=', $Pid)->where('product_id','=', $getPro->product_id)->first();
                    $Cid = $getPro->category_id;
 
                    $Infos = CartProduct::where('quantity', '!=', 0)->where('buy_rent','design')->where('cart_sub_type', '=', 'cosha')->where('cart_id', '=', $cart->id)->where('category_id',$Cid)->get();
                    foreach($Infos as $vals)
                    {
                    $total_price= $total_price+ $vals->total_price;
                    }

                    CartProduct::where('quantity', 0)->where('category_id',$Cid)->where('buy_rent','design')->where('cart_id', '=', $cart->id)->where('cart_sub_type','=', 'cosha')->update(['total_price'=> $total_price]);

                }
                else
                {
                $cart_pro =   CartServiceAttribute::where('cart_id', '=', $cart->id)->where('cart_product_id', '=', $Pid)->where('product_id','=', $getPro->product_id)->update(['quantity'=> $proQty,'total_price'=> $Tp]); 
                CartProduct::where('cart_id', '=', $cart->id)->where('product_id','=', $getPro->product_id)->update(['total_price'=> $Tp]);

                }
        /////////////// end here ////////////








     
                /*$cart_pro =   CartServiceAttribute::where('cart_id', '=', $cart->id)->where('product_id','=', $getPro->product_id)->update(['quantity'=> $proQty,'total_price'=> $Tp]);

                CartProduct::where('quantity', '!=', 0)->where('cart_id', '=', $cart->id)->where('product_id','=', $getPro->product_id)->update(['total_price'=> $Tp,'quantity'=> $proQty]);

                    $total_price = 0;


                    $getProinfo = CartProduct::where('cart_id', '=', $cart->id)->where('product_id','=', $getPro->product_id)->first();
                    $Cid = $getProinfo->category_id;
 
                    $Infos = CartProduct::where('quantity', '!=', 0)->where('cart_sub_type', '=', 'cosha')->where('cart_id', '=', $cart->id)->where('category_id',$Cid)->get();
                    foreach($Infos as $vals)
                    {
                    $total_price= $total_price+ $vals->total_price;
                    }

                    CartProduct::where('quantity', 0)->where('buy_rent','design')->where('cart_id', '=', $cart->id)->where('cart_sub_type','=', 'cosha')->update(['total_price'=> $total_price]);*/

 

}

elseif($type == 'hospitality')
{ 
        $IFDish = CartServiceAttribute::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->first();

        $getnoofstaff = CartHospitality::where('order_type', '=', 'Design Your Package')->where('cart_id', '=', $cart->id)->first();

        $IFDishQty = $IFDish->quantity;
        $IFDishPri = $IFDish->total_price;
        $OneTime = $IFDishPri/$IFDishQty;
        $price = $qty * $OneTime;
        $newtotal=0+($getnoofstaff->no_of_staff*$IFDish->worker_price);
        $tt=0;
        CartServiceAttribute::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->update(['quantity'=>$qty,'total_price'=>$price]);
        $getprototal = CartServiceAttribute::where('attribute_title', '=', 'Design Your Package')->where('cart_id', '=', $cart->id)->get();
         if(!empty($getprototal)) {
        foreach ($getprototal as  $value) {
            # code...
            $tt=$tt+$value->total_price;
            }
            $newtotal=$newtotal+$tt;
            CartProduct::where('cart_id', '=', $cart->id)->where('cart_sub_type','=', 'hospitality')->where('buy_rent','=', 'design')->update(['total_price'=> $newtotal]);
        }
       
}

elseif($type == 'buffetfood')
{ 
        $IFDish = CartExternalFoodDish::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->first();
        $IFDishQty = $IFDish->quantity;
        $IFDishPri = $IFDish->price;
        $OneTime = $IFDishPri/$IFDishQty;
        $price = $qty * $OneTime;
        CartExternalFoodDish::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->update(['quantity'=>$qty,'price'=>$price]);
        CartProduct::where('product_id', '=', $IFDish->product_id)->where('container_id', '=', $IFDish->container_id)->where('cart_id', '=', $cart->id)->update(['quantity'=>$qty,'total_price'=>$price]);
}

else if($type == 'internalfood')
{ 
        $IFDish = CartInternalFoodDish::where('internal_food_dish_id', '=', $Pid)->where('cart_id', '=', $cart->id)->first();
        $IFDishQty = $IFDish->quantity;
        $IFDishPri = $IFDish->price;
        $OneTime = $IFDishPri/$IFDishQty;
        $price = $qty * $OneTime;
        CartInternalFoodDish::where('internal_food_dish_id', '=', $Pid)->where('cart_id', '=', $cart->id)->update(['quantity'=>$qty,'price'=>$price]);

}else if($type == 'rose'){
         $cart_pro = CartProduct::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->first();
         if (!empty($cart) && !empty($cart_pro)) {
                $getPro = CartProduct::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->first();

             $isproduct = DB::table('nm_product')->where('pro_id',$getPro->product_id)->first();
             $proQty = $request->qty;
                if($isproduct->packege=='no'){

                    $extratotal = DB::table('nm_cart_option_value')->where('cart_id', '=', $cart->id)->where('product_id',$getPro->product_id)->sum('price');
                        
                            if($isproduct->pro_discount_percentage>0){
                                $pr=$isproduct->pro_disprice;

                            }else{
                                $pr=$isproduct->pro_price;
                            }
                         
                    $total_price =  ($proQty*$pr) + $extratotal;
                 
                    CartProduct::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->update(['quantity'=>$proQty,'total_price'=>$total_price]);

                }else{
                    $getPro = CartProduct::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->first();
                            $quantity =  $getPro->quantity; 
                            $total_price =  $getPro->total_price;      
                            if($quantity == 0){  $quantity =  1; } 
                            if($total_price == 0){  $total_price =  1; } 

                            $singleItemPrice = $total_price/$quantity;

                            $proQty = $request->qty;

                            $Tp = $singleItemPrice * $proQty;

                            
                            CartProduct::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->update(['quantity'=>$proQty,'total_price'=>$Tp]);





                }

 } }
else
{



        $cart_pro = CartProduct::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->first();

        if (!empty($cart) && !empty($cart_pro)) {

        $getPro = CartProduct::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->first();

        $quantity =  $getPro->quantity; 
        $total_price =  $getPro->total_price;      
        if($quantity == 0){  $quantity =  1; } 
        if($total_price == 0){  $total_price =  1; } 

        $singleItemPrice = $total_price/$quantity;

        $proQty = $request->qty;

        $Tp = $singleItemPrice * $proQty;

        if($type == 'food'){
        CartOptionValue::where('product_id', '=', $getPro->product_id)->where('cart_id', '=', $cart->id)->update(['quantity'=>$qty]);
        }
        CartProduct::where('id', '=', $Pid)->where('cart_id', '=', $cart->id)->update(['quantity'=>$proQty,'total_price'=>$Tp]);

        }  
}
 
              
echo "1";

 
   }

 

}

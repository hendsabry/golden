<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//

use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;
use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\Formbusinesstype;
//------------wisitech code end -----------//
class ShopController extends Controller
{  
    public function __construct(){
        parent::__construct();
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
     
    public function searchresults(Request $request)
    {
 		//dd($_REQUEST);
		$frmdata=$_REQUEST;
		Session::put('searchdata', $frmdata);
	  	//print_r(Session::get('searchdata'));
        $category_id=$_REQUEST['mainselectedvalue'];
		/*if($_REQUEST['basecategoryid']!=''){
        $category_id=$_REQUEST['basecategoryid'];
        }elseif($_REQUEST['maincategoryid']!='') {
        $category_id=$_REQUEST['maincategoryid'];
        }else{
         $category_id=$_REQUEST['mainselectedvalue'];   
        }*/
		$lang = Session::get('lang_file');
		$Formbusinesstype= new Formbusinesstype();
		  $bussinesstype=$Formbusinesstype->Getbussinesstype($category_id,$lang);	
			 $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $category_id)->first(); //    GET MAIN CATEGORY
             $hallsubcategory=array();
                if (!empty($category)) {

                    if ($category_id != '' && $lang == 'ar_lang') {

                        $hallsubcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $category_id)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get(); //GET SUB CATEGORY LIST IN ARABIC
                        
                    } else {

                        //GET SUB CATEGORY LIST IN ENGLISH
                        $hallsubcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $category_id)->select('mc_id', 'mc_name', 'mc_img')->get();
                      

                    }

                   // $response = Response::json($response_array);

                }
			
			 $value = $request->session()->get('searchdata');
			// print_r($bussinesstype);
          
             $getsubcategories = $hallsubcategory;
			 
        return view('search-results', compact('getsubcategories','bussinesstype'));
 
    }
   
}

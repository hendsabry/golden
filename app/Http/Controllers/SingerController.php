<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//

use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;
use App\SaveCart;
use App\Cart;
use App\User;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\Formbusinesstype;
use App\CartProductRent;
//------------wisitech code end -----------//

class SingerController extends Controller
{
	 public function __construct(){
        parent::__construct();
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
	
	 public function revivingconcerts($id)
	 {	
	 	$category_id = $id;
        if(Session::has('customerdata.user_id')) 
        {
    	 	if(Session::get('searchdata.basecategoryid')!='')
    	 	{
                 $basecategory_id=Session::get('searchdata.basecategoryid');
            }
            else 
            {
                 $basecategory_id=Session::get('searchdata.maincategoryid');
            }
    	 	$lang = Session::get('lang_file');
    		$Formbusinesstype= new Formbusinesstype();
    		  $bussinesstype=$Formbusinesstype->Getbussinesstype($basecategory_id,$lang);	
    		  $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $category_id)->first(); 
                    if(!empty($category)) 
                    {
                        if($category_id != '' && $lang == 'ar_lang') 
                        {
                            $revivingconcerts = Category::where('mc_status', '=', 1)->where('parent_id', '=', $category_id)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get(); //GET SUB CATEGORY LIST IN ARABIC
                        } 
                        else 
                        {                       
                            $revivingconcerts = Category::where('mc_status', '=', 1)->where('parent_id', '=', $category_id)->select('mc_id', 'mc_name', 'mc_img')->get();
                        }
                    }

    	 	$getsubcategories = $revivingconcerts;
            return view('revivingconcerts.revivingconcerts',compact('getsubcategories','bussinesstype','category_id')); 
          } 
          else
          {
          return Redirect::to('login-signup');
          }      
     }
	
	 public function singerslist($id,$sid)
	 {	        
	    $category_id = $id;
	    $subcat_id   = $sid;
        if(Session::has('customerdata.user_id')) 
        {
    	 	if(Session::get('searchdata.basecategoryid')!='')
    	 	{
                 $basecategory_id = Session::get('searchdata.basecategoryid');
            }
            else 
            {
                 $basecategory_id = Session::get('searchdata.maincategoryid');
            }
            $city_id = Session::get('searchdata.cityid');
            $lang = Session::get('lang_file');
    		$Formbusinesstype = new Formbusinesstype();
    		$bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);
            if(!empty($subcat_id)) 
            {
                if($category_id != '' && $lang == 'ar_lang') 
                {
                    $getallsinger = DB::table('nm_music')->where('category_id',$subcat_id)->where('status', '=', 1)->select('id', 'name_ar as name', 'image')->paginate(9)->appends(request()->query()); 
                } 
                else 
                {                       
                    $getallsinger = DB::table('nm_music')->where('category_id',$subcat_id)->where('status', '=', 1)->select('id', 'name', 'image')->paginate(9)->appends(request()->query());
                }
            }

            return view('revivingconcerts.singers-list',compact('getallsinger','bussinesstype','category_id','subcat_id'));  
        } 
        else
        {
          return Redirect::to('login-signup');
        }      
     }

     public function popularbandslist($id,$sid)
     {          
        $category_id = $id;
        $subcat_id   = $sid;
        if(Session::has('customerdata.user_id')) 
        {
            if(Session::get('searchdata.basecategoryid')!='')
            {
                 $basecategory_id = Session::get('searchdata.basecategoryid');
            }
            else 
            {
                 $basecategory_id = Session::get('searchdata.maincategoryid');
            }
            $lang = Session::get('lang_file');
            $city_id = Session::get('searchdata.cityid');
            $Formbusinesstype = new Formbusinesstype();
            $bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);
            if(!empty($subcat_id)) 
            {
                if($category_id != '' && $lang == 'ar_lang') 
                {
                    $getallsinger = DB::table('nm_music')->where('category_id',$subcat_id)->where('status', '=', 1)->select('id', 'name_ar as name', 'image')->paginate(9)->appends(request()->query()); 
                } 
                else 
                {                       
                    $getallsinger = DB::table('nm_music')->where('category_id',$subcat_id)->where('status', '=', 1)->select('id', 'name', 'image')->paginate(9)->appends(request()->query());
                }
            }
            //dd($getallsinger);
            return view('revivingconcerts.popular-bands-list',compact('getallsinger','bussinesstype','category_id','subcat_id'));
        } 
        else
        {
          return Redirect::to('login-signup');
        }        
     }

     public function acousticlist($id,$sid)
     {          
        $category_id = $id;
        $subcat_id   = $sid;
        if(Session::has('customerdata.user_id')) 
        {
            if(Session::get('searchdata.basecategoryid')!='')
            {
                 $basecategory_id = Session::get('searchdata.basecategoryid');
            }
            else 
            {
                 $basecategory_id = Session::get('searchdata.maincategoryid');
            }
            $lang = Session::get('lang_file');
            $Formbusinesstype = new Formbusinesstype();
            $city_id = Session::get('searchdata.cityid');
            $bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);
            if(!empty($subcat_id)) 
            {
                if($category_id != '' && $lang == 'ar_lang') 
                {
                    $getallsinger = DB::table('nm_music')->where('category_id',$subcat_id)->where('city_id',$city_id)->where('status',1)->select('id', 'name_ar as name', 'image')->groupBy('vendor_id')->paginate(9)->appends(request()->query());
                } 
                else 
                {                       
                    $getallsinger = DB::table('nm_music')->where('category_id',$subcat_id)->where('city_id',$city_id)->where('status',1)->select('id', 'name', 'image')->groupBy('vendor_id')->paginate(9)->appends(request()->query());
                }
            }
            return view('revivingconcerts.acoustic-list',compact('getallsinger','bussinesstype','category_id','subcat_id'));
        } 
        else
        {
          return Redirect::to('login-signup');
        }        
     }
     
     public function singerdetail($id,$sid,$lid)
	 {	
	     $id  = $id;
         $sid = $sid;
         $lid = $lid;
         //if(Session::has('customerdata.user_id'))
         //{
             $lang          = Session::get('lang_file');            
             $city_id       = Session::get('searchdata.cityid');
             $occasiontype  = Session::get('searchdata.weddingoccasiontype');
    	     $singerDetails = DB::table('nm_music')->where('category_id',$sid)->where('id',$lid)->first();
             $allreview     = DB::table('nm_review')->where('product_id',$lid)->where('review_type','shop')->where('status',1)->paginate(2);
             $setOccasion   = DB::table('nm_business_occasion_type')->where('status',1)->get();
             return view('newWebsite.singers',compact('singerDetails','allreview','sid','lid','setOccasion','city_id','occasiontype'));
       // }
        //else
        //{
         // return Redirect::to('login-signup');
        //}
     }

    public function popularbandsdetail($id,$sid,$lid)
     {  
         $id  = $id;$sid = $sid;$lid = $lid; 
         if(Session::has('customerdata.user_id')) 
         {  
             $lang          = Session::get('lang_file');            
             $city_id       = Session::get('searchdata.cityid');
             $occasiontype  = Session::get('searchdata.weddingoccasiontype');   
             $singerDetails = DB::table('nm_music')->where('category_id',$sid)->where('id',$lid)->first();
             $allreview     = DB::table('nm_review')->where('product_id',$lid)->where('review_type','shop')->where('status',1)->get();
             $setOccasion   = DB::table('nm_business_occasion_type')->where('status',1)->get();
             return view('revivingconcerts.popular-bands',compact('singerDetails','allreview','sid','lid','setOccasion','city_id','occasiontype')); 
        } 
        else
        {
          return Redirect::to('login-signup');
        }      
     }

     public function acousticrecording($id,$sid,$lid)
     {  
         $id  = $id;$sid = $sid;$lid = $lid; 
         if(Session::has('customerdata.user_id')) 
         {      
             $lang          = Session::get('lang_file');            
             $city_id       = Session::get('searchdata.cityid');
             $occasiontype  = Session::get('searchdata.weddingoccasiontype');  
             $singerDetails  = DB::table('nm_music')->where('category_id',$sid)->where('id',$lid)->first();
             $allreview      = DB::table('nm_review')->where('shop_id',$lid)->where('review_type','music')->where('status',1)->paginate(2);
             $setOccasion    = DB::table('nm_business_occasion_type')->where('status',1)->get();
             return view('newWebsite.records',compact('singerDetails','allreview','id','sid','lid','setOccasion','city_id','occasiontype'));
         } 
        else
        {
          return Redirect::to('login-signup');
        }        
     }
 
     public function acousticequipment($id,$sid,$lid)
     {  
         $id  = $id;
         $sid = $sid;
         $lid = $lid;
         if(Session::has('customerdata.user_id')) 
         {
         $city_id  = Session::get('searchdata.cityid'); 
         if(Session::get('searchdata.basecategoryid')!='')
         {
           $basecategory_id=Session::get('searchdata.basecategoryid');
         }
         else
         {
           $basecategory_id=Session::get('searchdata.maincategoryid');
         }
         $budget = Session::get('searchdata.budget');
         $lang   = Session::get('lang_file');
         $Formbusinesstype= new Formbusinesstype();
         $bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);
         //$value = $request->session()->get('searchdata');

         $singerDetails = DB::table('nm_music')->where('category_id',$sid)->where('id',$lid)->first();
        // dd($singerDetails);
         $vendor_id     = $singerDetails->vendor_id;
         $catDetailsC    = DB::table('nm_category')->where('parent_id',$sid)->where('vendor_id',$vendor_id)->where('mc_status',1)->count();
 
if($catDetailsC < 1)
{
   return Redirect::back()->with('msg','No product added');
 
}


  $catDetails    = DB::table('nm_category')->where('parent_id',$sid)->where('vendor_id',$vendor_id)->where('mc_status',1)->first();


         $shop_id       = $catDetails->mc_id;
         $allreview     = DB::table('nm_review')->where('shop_id',$shop_id)->where('status',1)->paginate(2);

         if($lang == 'ar_lang') 
         {
            $getAllProduct  = Products::select('pro_id','pro_title_ar as pro_title','pro_title_ar','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc_ar as pro_desc','notes_ar as notes','pro_price','Insuranceamount','pro_disprice','attribute_id','pro_qty')->where('pro_mc_id',$shop_id)->where('pro_mr_id',$vendor_id)->where('pro_status',1)->orderBy('pro_id','DESC')->paginate(9)->appends(request()->query());
         }
         else
         {
            $getAllProduct  = Products::select('pro_id','pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc','notes','pro_price','Insuranceamount','pro_disprice','attribute_id','pro_qty')->where('pro_mc_id',$shop_id)->where('pro_mr_id',$vendor_id)->where('pro_status',1)->orderBy('pro_id','DESC')->paginate(9)->appends(request()->query());
         }   

         return view('newWebsite.sound-system',compact('catDetails','allreview','id','sid','lid','getAllProduct','singerDetails'));
        } 
        else
        {
          return Redirect::to('login-signup');
        }        
     }
     
     public function insertsinger(Request $request)
     {
        $data = array();
        $lang = Session::get('lang_file');
        $userid  = Session::get('customerdata.user_id');
        $userifo = User::find($userid);
        if(isset($lang) && $lang=='en_lang')
        {
          $language_type = 1;  
        }
        else
        {
          $language_type = 2;  
        }
        $validatedData = $request->validate([
                  'hall'=>'required|max:100',
                  'occasion'=>'required',
                  'city'=>'required',
                  'location'=>'required|max:200',
                  'duration' =>'required',
                  'date' =>'required']); 
        
        $data           = $request->all();
        $vendor_id      = $data['vendor_id'];
        $getCity        = DB::table('nm_city')->where('ci_id',$data['city'])->first();        
        
        $getOccasion    = DB::table('nm_business_occasion_type')->where('id',$data['occasion'])->first();  

            $lang   = Session::get('lang_file');
            if($lang == 'ar_lang') 
            {
                $title          = $getOccasion->title_ar;
                $city_name      = $getCity->ci_name_ar;
            }
            else
            {
                $title          = $getOccasion->title;
                $city_name      = $getCity->ci_name;
            }

        $Current_Currency  = Session::get('currency'); 
  if($Current_Currency =='') 
  { 
    $Current_Currency = 'SAR'; 
  } 


        $getVendorInfo  = DB::table('nm_merchant')->where('mer_id',$vendor_id)->first(); 
        $vendor_name    = $getVendorInfo->mer_fname.' '.$getVendorInfo->mer_lname;
        $vendor_email   = $getVendorInfo->mer_email;
        $datetime = explode(' ',$data['date']);
         $full_date = $datetime[0];
         $full_time = $datetime[1].' '.$datetime[2];
        $user_name = $userifo->cus_name;
        $user_email = $userifo->email;
        $entry = array(
                'relate_with'     => $vendor_id,
                'user_id'         => $data['user_id'],
                'city_id'         => $data['city'],
                'hall'            => $data['hall'],
                'occasion_type'   => $title,
                'location'        => $data['location'],
                'duration'        => $data['duration'],
                'date'            => $full_date,
                'time'            => $full_time,
                'user_comment'    => $data['comments'],
                'singer_name'     => $data['singer_name'],
                'quote_for'       => $data['quote_for'],
                'merchant_id'     => $vendor_id,
                'music_id'        => $data['music_id'],
                'rev_currency'    => $Current_Currency,
                'occasion_type_id'    => $data['occasion'],
                'language_type'   => $language_type);
        $insertenquiry = DB::table('nm_services_inquiry')->insert($entry);
        if($insertenquiry)
        { 
            $send_mail_data = array(
                'name'          => $user_name,
                'hall'          => $data['hall'],
                'occasion_type' => $title,
                'city'          => $city_name,
                'location'      => $data['location'],
                'duration'      => $data['duration'],
                'date'          => $full_date,
                'time'          => $full_time,
                'comments'      => $data['comments'],
                'email'         => $vendor_email,
                'Current_Currency' => $Current_Currency,
                'lang'          => $lang,
                'LANUAGE'       => $lang,
                'vendor_name'   => $vendor_name);


            Mail::send('emails.revivingconcertsmail',$send_mail_data,function($message)
            {
                if (Lang::has(Session::get('lang_file').'.ENQUIRY_SUCCESSFULLY')!= '')
                { 
                    $session_message =  trans(Session::get('lang_file').'.ENQUIRY_SUCCESSFULLY');
                }
                $userid  = Session::get('customerdata.user_id');
                $userifo = User::find($userid);
                $user_email = $userifo->email;
                $user_name = $userifo->cus_name;
                $message->to($user_email,$user_name)->subject($session_message);
            });

            Mail::send('emails.revivingconcertsvendormail',$send_mail_data,function($message)
            {
                if (Lang::has(Session::get('lang_file').'.ENQUIRY_SUCCESSFULLY')!= '')
                { 
                    $session_message =  trans(Session::get('lang_file').'.ENQUIRY_SUCCESSFULLY');
                } 
                $vendor_id = Input::get('vendor_id');
                $getVendorInfo  = DB::table('nm_merchant')->where('mer_id',$vendor_id)->first(); 
                $vendor_name    = $getVendorInfo->mer_fname.' '.$getVendorInfo->mer_lname;
                $vendor_email   = $getVendorInfo->mer_email;
                $message->to($vendor_email,$vendor_name)->subject($session_message);
            }); 

            // language for display message //
            if(\Config::get('app.locale') == 'ar')
            {
               Session::flash('message', "تمت إضافة المراجعة بنجاح");
            }
            else
            {
               Session::flash('message', "Enquiry added successfully.");
            }
            // language for display message // 
            return Redirect::back();
        }
    }
    
    public function insertacoustic(Request $request)
    {
        $data = array();
        
        $vendor_name = '';
        $lang = Session::get('lang_file');
        $userid  = Session::get('customerdata.user_id');
        $userifo = User::find($userid);
        if(isset($lang) && $lang=='en_lang')
        {
          $language_type = 1;  
        }
        else
        {
          $language_type = 2;  

        }


        $validatedData = $request->validate([            
                  'hall'=>'required|max:100',
                  'occasion'=>'required',
                  'date'=>'required',                  
                  'music_type' =>'required',
                 // 'singer_name' =>'required',
                  'song_name' =>'required',
                  'city'=>'required']);
       // dd($request->all());


        $data           = $request->all();        
        $vendor_id      = $data['vendor_id'];
        $getCity        = DB::table('nm_city')->where('ci_id',$data['city'])->first();        
        
        $getOccasion    = DB::table('nm_business_occasion_type')->where('id',$data['occasion'])->first();        
     

            $lang   = Session::get('lang_file');
            if($lang == 'ar_lang') 
            {
            $title          = $getOccasion->title_ar;
            $city_name      = $getCity->ci_name_ar;
            }
            else
            {
            $title          = $getOccasion->title;
            $city_name      = $getCity->ci_name;
            }



        $getVendorInfo  = DB::table('nm_merchant')->where('mer_id',$vendor_id)->first(); 
        $vendor_name    = $getVendorInfo->mer_fname.' '.$getVendorInfo->mer_lname;
        $vendor_email   = $getVendorInfo->mer_email;
        $datetime = explode(' ',$data['date']);

        //dd($datetime);

        $full_date = $datetime[0];
        $full_time = $datetime[1].' '.$datetime[2];
        $user_name = $userifo->cus_name;
        $user_email = $userifo->email;
        $entry = array(
                'relate_with'       => $vendor_id,
                'user_id'           => $data['user_id'],
                'city_id'           => $data['city'],
                'the_groooms_name'  => $data['groom_name'],
                'hall'              => $data['hall'],
                'occasion_type'     => $title,
                'occasion_type_id'  => $data['occasion'],
                'date'              => $full_date,
                'time'              => $full_time,             
                'music_type'        => $data['music_type'],
             //   'singer_name'       => $data['singer_name'],
                'song_name'         => $data['song_name'],
                'quote_for'         => $data['quote_for'],
                'merchant_id'       => $vendor_id,
                'music_id'          => $data['music_id'],
                'language_type'     => $language_type);
       // dd($entry);
        $insertenquiry = DB::table('nm_services_inquiry')->insert($entry);
        if($insertenquiry)
        { 
            $send_mail_data = array(
                'groom_name'        => $data['groom_name'],
                'name'              => $user_name,
                'hall'              => $data['hall'],
                'occasion_type'     => $title,
                'date'              => $full_date,
                'time'              => $full_time,
                'recording_section' => '',
                'music_type'        => $data['music_type'],
                //'singer_name'       => $data['singer_name'],
                'song_name'         => $data['song_name'],
                'email'             => $vendor_email,
                'lang'              => $lang,
                'LANUAGE'           => $lang,
                'vendor_name'       => $vendor_name);
           
            Mail::send('emails.acousticmail', $send_mail_data, function($message)
            {
                if (Lang::has(Session::get('lang_file').'.ENQUIRY_SUCCESSFULLY')!= '')
                { 
                    $session_message =  trans(Session::get('lang_file').'.ENQUIRY_SUCCESSFULLY');
                } 
                $userid  = Session::get('customerdata.user_id');
                $userifo = User::find($userid);
                $user_email = $userifo->email;
                $user_name = $userifo->cus_name;
                $message->to($user_email,$user_name)->subject($session_message);
            });

            Mail::send('emails.acousticvendormail', $send_mail_data, function($message)
            {
                if (Lang::has(Session::get('lang_file').'.ENQUIRY_SUCCESSFULLY')!= '')
                { 
                    $session_message =  trans(Session::get('lang_file').'.ENQUIRY_SUCCESSFULLY');
                }     
                $vendor_id = Input::get('vendor_id');
                $getVendorInfo  = DB::table('nm_merchant')->where('mer_id',$vendor_id)->first(); 
                $vendor_name    = $getVendorInfo->mer_fname.' '.$getVendorInfo->mer_lname;
                $vendor_email   = $getVendorInfo->mer_email;
                $message->to($vendor_email,$vendor_name)->subject($session_message);
            });

            // language for display message //
            if(\Config::get('app.locale') == 'ar')
            {
               Session::flash('message', "تمت إضافة المراجعة بنجاح");
            }
            else
            {
               Session::flash('message', "Enquiry added successfully.");
            }
            // language for display message // 
            return Redirect::back();
        }
    }

    function getproductdetail(Request $request)
    {
        $id     = $request->product_id;
        $lang   = Session::get('lang_file');
        //echo $id;die;
        if ($id != '' && $lang == 'ar_lang') 
        {
            $productdateshopinfo = DB::table('nm_product')->where('pro_status', 1)->where('pro_id', $id)->select('pro_id', 'pro_title_ar as pro_title','pro_desc_ar as pro_desc', 'pro_price','pro_disprice','Insuranceamount', 'pro_Img','pro_disprice','notes_ar as notes','pro_qty')->orderBy('pro_title')->get();
                        
            $productdateshopproductpricecount = DB::table('nm_product_option_value')->where('product_id',$id)->select('id', 'option_title_ar as option_title', 'product_option_id','value as product_option_value_id','discount','price')->count();
            if($productdateshopproductpricecount > 0)
            {
               $productdateshopproductprice = DB::table('nm_product_option_value')->where('product_id',$id)->select('id', 'option_title_ar as option_title', 'product_option_id','value as product_option_value_id','discount','price','value')->orderBy('id','ASC')->get();
               $productdateshopinfo->product_price = $productdateshopproductprice;
            }else{ 
                $productdateshopproductprice = array();
            }
            
        }
        else
        {
            $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_id',$id)->select('pro_id', 'pro_title', 'pro_Img','pro_mc_id','attribute_id','pro_price','pro_disprice','Insuranceamount','pro_disprice','option_id','pro_desc','notes','pro_qty')->orderBy('pro_title')->get();
                         
            $productdateshopproductpricecount = DB::table('nm_product_option_value')->where('product_id',$id)->select('id', 'option_title', 'product_option_id','value as product_option_value_id','discount','price')->count();
            if($productdateshopproductpricecount > 0)
            {
                $productdateshopproductprice = DB::table('nm_product_option_value')->where('product_id',$id)->select('id', 'option_title', 'product_option_id','value as product_option_value_id','discount','price','discount_price','value')->orderBy('id','ASC')->get();
               $productdateshopinfo->product_price = $productdateshopproductprice;
            }else{
                $productdateshopproductprice = array();
            }
            
                         
        }
        $productdatesshopfullinfo = array('productdateshopinfo'=>$productdateshopinfo,'productdateshopprice'=>$productdateshopproductprice);
        return response()->json($productdatesshopfullinfo);
    }

    function checksoldout(Request $request)
   {
        $product_id     = $request->product_id;
        $qty            = $request->qty;
        $rel            = $request->rel;
        
        if($rel=='rent')
        {
            $productinfo  = DB::table('nm_product_option_value')->where('product_id',$product_id)->where('option_title','Quantity in stock')->select('value')->first();
          if($productinfo->value<1)
            {
              echo 'ok';
            }
            else
            {
                echo "error";
            }            
        }
        else
        {
           $productinfo = DB::table('nm_product')->where('pro_id',$product_id)->select('pro_qty')->first();
            if($productinfo->pro_qty<1)
            {
              echo 'ok';
            }
            else
            {
                echo "error";
            }
        }
    }

   function checkQuantity(Request $request)
   {
        $product_id     = $request->product_id;
        $qty            = $request->qty;
        $rel            = $request->rel;
        
        if($rel=='rent')
        {
            $productinfo  = DB::table('nm_product_option_value')->where('product_id',$product_id)->where('option_title','Quantity in stock')->select('value')->first();
            if($qty <= @$productinfo->value)
            {
              echo 'ok';
            }
            else
            {
                echo "error";
            }            
        }
        else
        {
           $productinfo = DB::table('nm_product')->where('pro_id',$product_id)->select('pro_qty')->first();
            if($qty <= @$productinfo->pro_qty)
            {
              echo 'ok';
            }
            else
            {
                echo "error";
            }
        }
    }
   
    function addcartsingerproduct(Request $request)
    {
        
        //echo '<pre>';print_r($_REQUEST);die;

        $rent_datetime   = $request->occasions_date;

        if($rent_datetime!='')
        {
        $rent_array = explode(' ',$rent_datetime);
        $rentdate = $rent_array[0].' '.$rent_array[1].' '.$rent_array[2];
        $renttime = $rent_array[3].' '.$rent_array[4];  
        }
        else
        {
           $rentdate = '';
        $renttime = '';   
        }


        $return_datetime  = $request->return_date;
        if($rent_datetime!='')
        {

        $return_array = explode(' ',$return_datetime);
        $returndate = $return_array[0].' '.$return_array[1].' '.$return_array[2];
        $returntime = $return_array[3].' '.$return_array[4];  
        }
        else
        {
          $returndate = '';
        $returntime = ''; 
        }      

        $insurance_amount = $request->insurance_amount;
        $productoptionval = $request->productoptionval;
        $itemqty          = $request->itemqty;
        $product_id       = $request->product_id;
        $buyamt           = $request->pr58;
        $rentamt          = $request->pr59;
        $shopby           = $request->shopby;
        $category_id      = $request->category_id;
        $subcategory_id   = $request->subcategory_id;
        $vendor_id        = $request->vendor_id;
        $attribute_id     = $request->attribute_id; 
        $cart_type        = $request->cart_type;
        $cart_sub_type    = 'acoustic';
        $rentfinalprice   = $request->finalprice;
        $user_id          = Session::get('customerdata.user_id');
        $product          = DB::table('nm_product')->where('pro_id',$product_id)->first();
    
        $product_options  = DB::table('nm_product_option_value')->where('product_id',$product_id)->where('id',$productoptionval)->distinct()->first();
        $cart = Cart::where('user_id',$user_id)->first();
        if(!empty($cart)) 
        {
            $cart_pro = CartProduct::where('cart_type','music')->where('cart_id',$cart->id)->where('product_id',$product_id)->delete();
            $cart_pro_opt = CartOption::where('cart_type','music')->where('cart_id',$cart->id)->where('product_id',$product_id)->delete();
            $cart_pro_val = CartOptionValue::where('cart_type','music')->where('cart_id',$cart->id)->where('product_id',$product_id)->delete();
            $cart_pro_rent = CartProductRent::where('cart_type','music')->where('cart_id',$cart->id)->where('product_id',$product_id)->delete();
        } 
        else 
        {
            $cart_data = array();
            $cart_data['user_id'] = $user_id;
            $cart = Cart::create($cart_data); //cart entery
        }

        $cart_product_data                    = array();
        $cart_product_data['cart_type']       = $cart_type;
        $cart_product_data['cart_sub_type']   = $cart_sub_type;
        $cart_product_data['cart_id']         = $cart->id;
        $cart_product_data['product_id']      = $product_id;
        $cart_product_data['status']          = 1;                    
        $cart_product_data['category_id']     = $product->pro_mc_id;
        if($rent_datetime || $return_datetime != '') 
        {  
            $protype='rent';
        }else{
            $protype='buy';
        }

        $cart_product_data['buy_rent']     = $protype;
        $cart_product_data['pro_title']     = $product->pro_title;
        $cart_product_data['pro_title_ar']     = $product->pro_title_ar;
        $cart_product_data['pro_desc']     = $product->pro_desc;
        $cart_product_data['pro_desc_ar']     = $product->pro_desc_ar;
        $cart_product_data['pro_Img']     = $product->pro_Img;


        $cart_product_data['quantity']        = $itemqty;
        $cart_product_data['merchant_id']     = $product->pro_mr_id;
        $cart_product_data['shop_id']         = $product->pro_mc_id;
        $cart_product_data['review_type']     = 'shop';
        if($rentfinalprice!=''){
            $cart_product_data['pro_price']     = $rentfinalprice/$itemqty;
            $cart_product_data['total_price'] = $rentfinalprice;

        }else{
            $cart_product_data['pro_price']     = $buyamt;
           $cart_product_data['total_price'] = ($itemqty*$buyamt);
           
        }
        $cart_product = CartProduct::create($cart_product_data); //cart product entry 

        if(isset($productoptionval)) 
        {
                $cart_option_data = array();
                $cart_option_data['cart_type']         = $cart_type;
                $cart_option_data['product_option_id'] = @$product_options->product_option_id;
                $cart_option_data['cart_id']           = $cart->id;
                $cart_option_data['quantity']          = $itemqty;
                $cart_option_data['product_id']        = $product_id;
                $cart_option_data['cart_product_id']        = $cart_product->id;
                CartOption::create($cart_option_data); //cart option entry
            
                $cart_option_value                      = array();                            
                $cart_option_value['cart_type']         = $cart_type;
                $cart_option_value['cart_id']           = $cart->id;
                $cart_option_value['product_id']        = $product_id;
                $cart_option_value['quantity']          = $itemqty;
                $cart_option_value['product_option_id'] = @$product_options->product_option_id;
                $cart_option_value['product_option_value_id'] = @$product_options->id;                            
                $cart_option_value['value'] = @$product_options->price;
                $cart_option_value['status'] = 1;
                $cart_option_value['insurance_amount'] = $insurance_amount;
                $cart_option_value['cart_product_id']        = $cart_product->id;
                CartOptionValue::create($cart_option_value); //cart option value entry
        }
        
        if($rent_datetime || $return_datetime != '') 
        {                   
                $cart_product_rent                     = array();
                $cart_product_rent['cart_id']          = $cart->id;
                $cart_product_rent['cart_type']        = $cart_type;
                $cart_product_rent['product_id']       = $product_id;
                $cart_product_rent['rental_date']      = $rentdate;
                $cart_product_rent['rental_time']      = $renttime;
                $cart_product_rent['return_date']      = $returndate;
                $cart_product_rent['return_time']      = $returntime;
                $cart_product_rent['service_id']       = $product->pro_mc_id;
                $cart_product_rent['quantity']         = $itemqty;
                $cart_product_rent['insurance_amount'] = $insurance_amount;
                $cart_product_rent['cart_product_id'] = $cart_product->id;
                $cart_product_rent['cart_product_id']        = $cart_product->id;
                CartProductRent::create($cart_product_rent); //internal food entery
        }
        if(\Config::get('app.locale') == 'ar')
        {
          Session::flash('status', "وأضاف إلى السلة بنجاح");
        }
        else
        {
          Session::flash('status', "Added to Cart Successfully");
        }
        // language for display message //                
        return Redirect::back();
     return redirect('acousticequipment/'.$category_id.'/'.$subcategory_id.'/'.$vendor_id);
    }

}

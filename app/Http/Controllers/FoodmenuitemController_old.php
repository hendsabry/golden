<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//

use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;

use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\Formbusinesstype;
use App\Internalfooditems;
//------------wisitech code end -----------//
class FoodmenuitemController extends Controller
{  
    public function __construct(){
        parent::__construct();
		
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
     
    public function menuitemlist(Request $request,$foodtype)
    {
 		//print_r($_REQUEST);
		//echo $halltype;
		 if(!Session::has('customerdata.token')) 
	    		{
          	return Redirect::to('login-signup');
			}  
	
		if(Session::get('searchdata.basecategoryid')!=''){
		$basecategory_id=Session::get('searchdata.basecategoryid');
		}else {
		$basecategory_id=Session::get('searchdata.maincategoryid');
		}
		//$category_id=$foodtype;
		$productsessionid=Session::get('searchdata.prodid');
		 $productid=$productsessionid[0];
		$lang=Session::get('lang_file');
		
		
		
		    if ($lang == 'ar_lang') {
					  // Get Menu details with Dish and container. 
                    $menu = InternalFoodMenu::select('id', 'pro_id', 'vendor_id', 'menu_name_ar as menu_name', 'menu_image', 'status')->where('pro_id', '=', $productid)->orderBy('menu_name', 'ASC')->get();

                    if (isset($menu)) {
                       foreach($menu as $key => $value) {
                            $food = InternalFoodDish::select('id', 'relate_with', 'vendor_id', 'internal_food_menu_id', 'dish_name_ar as dish_name', 'dish_image','price','status')->where('status', '=', 1)->where('relate_with', '=', $productid)->where('internal_food_menu_id', '=', $value->id)->orderBy('dish_name', 'ASC')->paginate(9);
                            
                            $value->food_list = $food;
                            foreach($food as $val) {
                                //Get Container
                                $container = InternalFoodContainer::select('id', 'img', 'title_ar as title', 'no_people', 'short_code', 'status')->orderBy('short_code', 'ASC')->where('status', '=', 1)->get();
                                $val->container_list = $container;
                                foreach($container as $cont_value) {
                                    //Get Container Price
                                $container_price = InternalFoodContainerPrice::select('id', 'dish_id', 'container_id', 'container_price')->where('dish_id', '=', $val->id)->where('container_id', '=', $cont_value->id)->get(); 
                                $cont_value->container_price = $container_price;

                                }
                               
                            }

                        }

                      
                    } 
			}else{
							
					 
					 // Get Menu details with Dish and container.
                    $foodmenu = InternalFoodMenu::select('id', 'pro_id', 'vendor_id', 'menu_name', 'menu_image', 'status')->where('pro_id', '=', $productid)->orderBy('menu_name', 'ASC')->get();
				
                    if (isset($foodmenu)) {
                        foreach($foodmenu as $key => $value) {
                            $food = InternalFoodDish::select('id', 'relate_with', 'vendor_id', 'internal_food_menu_id', 'dish_name', 'dish_image','price','status')->where('status', '=', 1)->where('relate_with', '=', $productid)->where('internal_food_menu_id', '=', $value->id)->orderBy('dish_name', 'ASC')->paginate(9);
                            	
                            $value->food_list = $food;
                            foreach($food as $val) {
                                //Get Container
                                $container = InternalFoodContainer::select('id', 'img', 'title', 'no_people', 'short_code', 'status')->orderBy('short_code', 'ASC')->where('status', '=', 1)->get();
                                $val->container_list = $container;
								
								
                                foreach($container as $cont_value) {
								  //Get Container Price
                                $container_price = InternalFoodContainerPrice::select('id', 'dish_id', 'container_id', 'container_price')->where('dish_id', '=', $val->id)->where('container_id', '=', $cont_value->id)->get(); 
                                $cont_value->container_price = $container_price;

                                }
								
                            }

                        }
                       
                    }
					 
					 
					 
					 
			}
		
		$Internalfooditemstype= new Internalfooditems();
		  $mainmenuitemtype=$Internalfooditemstype->Getnm_internal_food_menutype($productid,$lang);	
		  $containerlistitem=$Internalfooditemstype->GetcontainerListByProduct($productid,$lang);	
	
		  $mainmenuwithItemAndContainer=$foodmenu;
		
        return view('menuitemlist', compact('mainmenuitemtype','mainmenuwithItemAndContainer','category_id','containerlistitem'));
 
    }
   
}

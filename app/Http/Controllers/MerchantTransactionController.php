<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Lang;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Products;
use App\Auction;
use App\Customer;
use App\Transactions;
use App\Merchantdeals;
use App\Merchantadminlogin;
use App\Merchantproducts;
use App\Merchantsettings;
use App\MerchantTransactions;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Fund; // for checkMerchantOverOrderReport function supports
use paypal_class; //Paypal payment of commission

class MerchantTransactionController extends Controller
{
    
    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

   public function __construct(){
        parent::__construct();
       
        // set admin Panel language
        $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleMerchant();
                    return $next($request);
    });
       
    }

    
    public function show_merchant_transactions()
    {
        if (Session::has('merchantid')) {
			if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $adminheader    = view('sitemerchant.includes.merchant_header')->with("routemenu",$session_message);
            $adminfooter    = view('sitemerchant.includes.merchant_footer');
            $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
           
            $producttransactioncnt = MerchantTransactions::get_producttransaction();
            $dealtransactioncnt    = MerchantTransactions::get_dealstransaction();
            $auctiontransactioncnt = MerchantTransactions::get_auctiontransaction();
         
            $producttoday  = MerchantTransactions::get_producttoday_order();
            $produst7days  = MerchantTransactions::get_product7days_order();
            $product30days = MerchantTransactions::get_product30days_order();
            
            $dealstoday  = MerchantTransactions::get_dealstoday_order();
            $deals7days  = MerchantTransactions::get_deals7days_order();
            $deals30days = MerchantTransactions::get_deals30days_order();
            
            $auctiontoday     = MerchantTransactions::get_auctiontoday_order();
            $auction7days     = MerchantTransactions::get_auction7days_order();
            $auction30days    = MerchantTransactions::get_auction30days_order();
            $merid            = Session::get('merchantid');
            $getproductidlist = Merchantproducts::getproductidlist($merid);
            $productlist      = $getproductidlist[0]->proid;
            
            $getauctionidlistrs = Merchant::getauctionidlist($merid);
            $getauctionidlist   = $getauctionidlistrs[0]->proid;
            
            $productchartdetails = MerchantTransactions::get_chart_product_details($productlist);
            $dealchartdetails    = MerchantTransactions::get_chart_deals_details($productlist);
            $auctionchartdetails = MerchantTransactions::get_chart_auction_details($getauctionidlist);
            
            
            return view('sitemerchant.merchant_transactiondashboard')->with('merchantheader', $adminheader)->with('merchantfooter', $adminfooter)->with('merchantleftmenus', $adminleftmenus)->with('producttoday', $producttoday)->with('produst7days', $produst7days)->with('product30days', $product30days)->with('dealstoday', $dealstoday)->with('deals7days', $deals7days)->with('deals30days', $deals30days)->with('auctiontoday', $auctiontoday)->with('auction7days', $auction7days)->with('auction30days', $auction30days)->with('producttransactioncnt', $producttransactioncnt)->with('dealtransactioncnt', $dealtransactioncnt)->with('auctiontransactioncnt', $auctiontransactioncnt)->with('productchartdetails', $productchartdetails)->with('dealchartdetails', $dealchartdetails)->with('auctionchartdetails', $auctionchartdetails);
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
  
    public function product_all_orders()
    {
        if(Session::get('merchantid')) {
            
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            
            $merid            = Session::get('merchantid');
            
            $orderdetails     = MerchantTransactions::getproduct_all_orders($merid);
            
            $alltrans_reports = MerchantTransactions::alltrans_reports($from_date, $to_date, $merid);
			if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            
            return view('sitemerchant.product_allorders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("allorders", $orderdetails)->with("alltrans_reports", $alltrans_reports)->with('from_date',$from_date)
            ->with('to_date',$to_date);
        }
        
        else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function merchant_payu_product_all_orders()
    {
        if(Session::get('merchantid')) {
            
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            
            $merid            = Session::get('merchantid');
            
            $orderdetails     = MerchantTransactions::get_payu_product_all_orders($merid);
            
            $alltrans_reports = MerchantTransactions::payu_alltrans_reports($from_date, $to_date, $merid);
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            
            return view('sitemerchant.payu_product_allorders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("allorders", $orderdetails)->with("alltrans_reports", $alltrans_reports)->with('from_date',$from_date)
            ->with('to_date',$to_date);
        }
        
        else {
            return Redirect::to('sitemerchant');
        }
        
    }
    
    
    public function product_success_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
			if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
           
             $orderdetails          = MerchantTransactions::getproduct_success_orders($merid);

             $allsucessprod_reports = MerchantTransactions::allsucessprod_reports($from_date, $to_date, $merid);

            return view('sitemerchant.product_success_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("successorders", $orderdetails)->with("allsucessprod_reports", $allsucessprod_reports)->with('from_date',$from_date)
            ->with('to_date',$to_date);
        }
        
        else {
            return Redirect::to('sitemerchant');
        }
    }
    public function payu_product_success_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
           
             $orderdetails          = MerchantTransactions::get_payu_product_success_orders($merid);

             $allsucessprod_reports = MerchantTransactions::payu_allsucessprod_reports($from_date, $to_date, $merid);

            return view('sitemerchant.payu_product_success_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("successorders", $orderdetails)->with("allsucessprod_reports", $allsucessprod_reports)->with('from_date',$from_date)
            ->with('to_date',$to_date);
        }
        
        else {
            return Redirect::to('sitemerchant');
        }
    }
    public function product_completed_orders()
    {
        if (Session::get('merchantid')) {
            
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
           /* $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;
                
                $orderdetails             = MerchantTransactions::getproduct_completed_orders($productlist);
                $allcompletedprod_reports = MerchantTransactions::allcompletedprod_reports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();      
                $orderdetails = array();
                $allcompletedprod_reports = array();                  
            } else {
                $orderdetails = array();
            }*/
            $orderdetails             = MerchantTransactions::getproduct_completed_orders($merid);
            $allcompletedprod_reports = MerchantTransactions::allcompletedprod_reports($from_date, $to_date, $merid);
            return view('sitemerchant.product_completed_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("completedorders", $orderdetails)->with("allcompletedprod_reports", $allcompletedprod_reports);
        } else {
            return Redirect::to('sitemerchant');
        }
    }
    
    
    public function product_failed_orders()
    {
        if (Session::get('merchantid')) {
            
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            /*$getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;
                
                $orderdetails          = MerchantTransactions::getproduct_failed_orders($productlist);
                $allfailedprod_reports = MerchantTransactions::allfailedprod_reports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();      
                $orderdetails = array();
                $allfailedprod_reports = array();              
            } else {
                $orderdetails = array();
            }*/
            $orderdetails          = MerchantTransactions::getproduct_failed_orders($merid);
            $allfailedprod_reports = MerchantTransactions::allfailedprod_reports($from_date, $to_date, $merid);
            return view('sitemerchant.product_failed_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("failedorders", $orderdetails)->with("allholdprod_reports", $allfailedprod_reports)->with('from_date',$from_date)
            ->with('to_date',$to_date);
        }
        
    }
    public function payu_product_failed_orders()
    {
        if (Session::get('merchantid')) {
            
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            
            $orderdetails          = MerchantTransactions::get_payu_product_failed_orders($merid);
            $allfailedprod_reports = MerchantTransactions::payu_allfailedprod_reports($from_date, $to_date, $merid);
            return view('sitemerchant.payu_product_failed_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("failedorders", $orderdetails)->with("allholdprod_reports", $allfailedprod_reports)->with('from_date',$from_date)
            ->with('to_date',$to_date);
        }
        
    }
    
    public function product_hold_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            /*$getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist  = $getproductidlist[0]->proid;
                $orderdetails = MerchantTransactions::getproduct_hold_orders($productlist);
                
                $allholdprod_reports = MerchantTransactions::allholdprod_reports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();      
                $orderdetails = array();
                $allholdprod_reports = array();                        
            } else {
                $orderdetails = array();
            }*/
            $orderdetails        = MerchantTransactions::getproduct_hold_orders($merid);
            $allholdprod_reports = MerchantTransactions::allholdprod_reports($from_date, $to_date, $merid);
            return view('sitemerchant.product_hold_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("holdorders", $orderdetails)->with("allholdprod_reports", $allholdprod_reports)->with('from_date',$from_date)
            ->with('to_date',$to_date);
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function payu_product_hold_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            
            $orderdetails        = MerchantTransactions::get_payu_product_hold_orders($merid);
            $allholdprod_reports = MerchantTransactions::payu_allholdprod_reports($from_date, $to_date, $merid);
            return view('sitemerchant.payu_product_hold_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("holdorders", $orderdetails)->with("allholdprod_reports", $allholdprod_reports)->with('from_date',$from_date)
            ->with('to_date',$to_date);
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    
    
    public function cod_all_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            /*$getproductidlist  = Merchantproducts::getproductidlist($merid);
            if($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;
                
            }elseif(empty($getproductidlist)) {
                $productlist = array();      
                $orderdetails = array();
                $allprod_codreports = array();                   
            }else {
                $orderdetails = array();
            }*/
            $orderdetails = MerchantTransactions::getcod_all_orders($merid);
            $allprod_codreports = MerchantTransactions::allprod_codreports($from_date, $to_date, $merid);
            return view('sitemerchant.productcod_all_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("allorders", $orderdetails)->with("allprod_codreports", $allprod_codreports)
            ->with('from_date',$from_date)->with('to_date',$to_date);
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
       
    public function cod_completed_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
           /* if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;
                
                
             } elseif(empty($getproductidlist)) {
                $productlist = array();      
                $orderdetails = array();
                $allpro_codcompleted_reports = array();     
            } else {
                $orderdetails = array();
            }*/
            $orderdetails = MerchantTransactions::getcod_success_orders($merid);
            $allpro_codcompleted_reports = MerchantTransactions::allpro_codsuccess_reports($from_date, $to_date, $merid);
            return view('sitemerchant.productcod_completed_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("completedorders", $orderdetails)->with("allpro_codcompleted_reports", $allpro_codcompleted_reports)
            ->with('from_date',$from_date)->with('to_date',$to_date);
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
	public function mer_cod_replacement_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
             $merid             = Session::get('merchantid');
            $payment_type=0;
            $order_type=1;
            $calceled_order  = MerchantTransactions::get_replace_order($merid,$payment_type,$order_type,$from_date,$to_date);
              $replace_order_all  = MerchantTransactions::get_replace_order_alltype($from_date,$to_date,$merid); 
              
            
            $er_msg = '';
            if(Session::has("er_msg"))
                $er_msg = Session::get("er_msg");
            return view('sitemerchant.productcod_replace_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("calceled_order", $calceled_order)->with('from_date',$from_date)->with('to_date',$to_date)->with('er_msg',$er_msg)->with('replace_order_all',$replace_order_all);
           ;
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }

    public function get_approve_replacecontent($delStatus_id)
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $data  = MerchantTransactions::get_approve_content($delStatus_id);
            //print_r($data);
            echo '<b>Replace Note:</b><br>'.strip_tags($data[0]->replace_notes);
            echo '<br><b>Replace Applied On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->replace_date));
            if($data[0]->replace_status==1)
            {
                 if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE')!= '') 
                    {
                        $note_label = trans(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE');
                    }
                    else 
                    {
                        $note_label =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVE_DISAPPROVED_NOTE');
                    }
            echo '
                <form action="'.url('').'/approve_replace_request" method="post">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input name="cod_order_id" type="hidden" value="'.$data[0]->cod_order_id.'"/>
                    <input name="order_cust_id" type="hidden" value="'.$data[0]->order_cust_id.'"/>
                    <input type="hidden" name="delStatus_id" value="'.$delStatus_id.'"/>
                    <div class="col-sm-12">
                        <label>'.$note_label.'</label>
                        <textarea class="form-control" required name="approve_or_disapprove_note"></textarea>
                    </div><br>
                    <div class="col-sm-12 ">
                        <input type="submit" class="btn btn-success" name="approve" value="Approve replace request">
                        <input type="submit" class="btn btn-danger" name="disapprove" value="Dis-Approve replace request">
                    </div>
                </form>';
            }
            else{
                $status ='Order Delivered';
                if($data[0]->replace_status==2)
                        $status ='Replaced';
                 if($data[0]->replace_status==4)
                        $status ='Disapproved';

                echo '<br><b>Replace Status:</b><br>'.$status;
                echo '<br><b>Replace Processed On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->replace_approved_date));
            }
             
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }

    public function approve_replace_request()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $cod_order_id=$_POST["cod_order_id"];
            $delStatus_id=$_POST["delStatus_id"];
            $cust_id=$_POST["order_cust_id"];
            $mer_id=Session::get('merchantid');
            $admin_id="1";
            $approve_or_disapprove_note=$_POST["approve_or_disapprove_note"];
            $return_Status =" "; 

            //check product qunatity to replace
            $prod_info =DB::table('nm_product as prod')->join("nm_ordercod as cod","cod.cod_pro_id",'=',"prod.pro_id")->join("nm_order_delivery_status AS od","od.prod_id",'=',"prod.pro_id")->select('prod.pro_id','prod.pro_no_of_purchase','prod.pro_qty','cod.cod_qty')->where("od.delStatus_id","=",$delStatus_id)->where("cod.cod_id","=",$cod_order_id)->first();
           // echo $_POST["disapprove"];
            //print_r($prod_info);exit();
            if(count($prod_info)>0){
                if(isset($_POST["approve"]))
                {
                    $available_qty = $prod_info->pro_qty - $prod_info->pro_no_of_purchase;
                    if($available_qty>$prod_info->cod_qty){
                        $new_purchased = $prod_info->pro_no_of_purchase+$prod_info->cod_qty;
                         MerchantTransactions::approve_replace($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');  
                         $return_Status ="Approved";   
                          //Update Product quantity
                        Products::edit_product(array('pro_no_of_purchase' => $new_purchased  ),$prod_info->pro_id); 
                        
                        if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY')!= '') 
                        {
                            $er_msg = trans(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY');
                        }
                        else 
                        {
                            $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVED_SUCCESSFULLY');
                        }
                        Session::flash("er_msg",$er_msg);
                    }else{
                        $er_msg = "quantity not available";
                        Session::flash("er_msg",$er_msg);
                       return redirect("mer_cod_replacement_orders")->with('er_msg',$er_msg);
                    }
                              
                }
                if(isset($_POST["disapprove"]))
                {
                     MerchantTransactions::disapprove_replace($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');
                     $return_Status ="Disapproved"; 
                     if(Lang::has(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY')!= '') 
                    {
                        $er_msg = trans(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY');
                    }
                    else 
                    {
                        $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DISAPPROVED_SUCCESSFULLY');
                    }
                     
                    Session::flash("er_msg",$er_msg);
                    
                }


             /* mail starts */
                 if($delStatus_id !=''){
                    $deliveryDetails =  DB::table("nm_order_delivery_status AS od")->leftjoin("delivery_status_chat As dc","dc.delStatus_id","=","od.delStatus_id")->where("dc.delStatus_id","=",$delStatus_id)->where("dc.send_by","=",'2')->first(); 
                   // print_r($deliveryDetails) ;exit();
                        $prod_title = '';
                        $prod_title=DB::table('nm_product')->select('pro_title')->where("pro_id","=",$deliveryDetails->prod_id)->get();
                        $prod_title=$prod_title[0]->pro_title;


                        include('SMTP/sendmail.php');
                        //Customer mail
                        $custom_name = $customer_mail =''; 
                        $custom_data = DB::table('nm_customer')->select('cus_name','cus_email')->where('cus_id','=',$deliveryDetails->order_cust_id)->first();
                        if(count($custom_data)>0){
                        $custom_name = $custom_data->cus_name;
                        $customer_mail = $custom_data->cus_email;
                        }
                        if($customer_mail!='')
                        {

                            Mail::send('emails.replaceOrderApproveOrDisapprove_mail', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->replace_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'date'              =>  $deliveryDetails->replace_date,
                                'approve_status'    =>  $return_Status,
                                'approved_date'     =>  $deliveryDetails->replace_approved_date
                                ), function($message) use ($customer_mail)
                            {
                                $message->to($customer_mail)->subject('Order replace Approve/Disapprove Notification');
                            });     
                        }    
                        //merchant mail
                        $get_mer_email  = Home::get_mer_email($deliveryDetails->mer_id);
                        $mer_email      = $get_mer_email[0]->mer_email;
                        if($mer_email!=''){
                            Mail::send('emails.replaceOrderApproveOrDisapprove_mail_to_merchant', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->replace_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'date'              =>  $deliveryDetails->replace_date,
                                'approve_status'    =>  $return_Status,
                                'approved_date'     =>  $deliveryDetails->replace_approved_date
                                ), function($message) use ($mer_email)
                            {
                                $message->to($mer_email)->subject('Order replace Approve/Disapprove Notification');
                            }); 
                        }
                        //Admin mail
                        $adminData = DB::table('nm_admin')->where('adm_id', '=', '1')->first();
                        if(count($adminData)>0){
                            $adminEmail = $adminData->adm_email;
                            if($adminEmail!='')
                            {
                                Mail::send('emails.replaceOrderApproveOrDisapprove_mail_to_admin', array(
                                    'delStatus_id'      =>  $delStatus_id,
                                    'transaction_id'    =>  $deliveryDetails->transaction_id,
                                    'prod_id'           =>  $deliveryDetails->prod_id,
                                    'prod_title'        =>  $prod_title,
                                    'customer_name'     =>  $custom_name,
                                    'return_notes'      =>  strip_tags($deliveryDetails->replace_notes),
                                    'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                    'date'              =>  $deliveryDetails->replace_date,
                                    'approve_status'    =>  $return_Status,
                                    'approved_date'     =>  $deliveryDetails->replace_approved_date
                                ), function($message) use ($adminEmail)
                                {
                                    $message->to($adminEmail)->subject('Order replace Approve/Disapprove Notification');
                                }); 
                            }
                        }

                    }
                 /* mail ends */ 
     
            }else{
                $er_msg = "Product Not available";
                Session::flash("er_msg",$er_msg);
                return redirect("mer_cod_replacement_orders");
            }     

            return redirect("mer_cod_replacement_orders");
            
        } else {

            return Redirect::to('sitemerchant');
        }
        
    }


    /* replacement ends */
    /* paypal replacement starts */
    public function mer_replacement_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
             $merid             = Session::get('merchantid');
            $payment_type=1;
            $order_type=1;
            $calceled_order  = MerchantTransactions::get_replace_order_paypal($merid,$payment_type,$order_type,$from_date,$to_date);
            $replace_order_all  = MerchantTransactions::replace_order_allpaypal($from_date,$to_date,$merid); 

          

            $er_msg = '';
            if(Session::has("er_msg"))
                $er_msg = Session::get("er_msg");
            return view('sitemerchant.product_replace_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("calceled_order", $calceled_order)->with('from_date',$from_date)->with('to_date',$to_date)->with('er_msg',$er_msg)->with('replace_order_all',$replace_order_all);
           ;
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
public function mer_payu_replacement_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
             $merid             = Session::get('merchantid');
            $payment_type=1;
            $order_type=1;
            $calceled_order  = MerchantTransactions::get_replace_order_payu($merid,$payment_type,$order_type,$from_date,$to_date);
            $replace_order_all  = MerchantTransactions::replace_order_allpayu($from_date,$to_date,$merid); 

          

            $er_msg = '';
            if(Session::has("er_msg"))
                $er_msg = Session::get("er_msg");
            return view('sitemerchant.payu_product_replace_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("calceled_order", $calceled_order)->with('from_date',$from_date)->with('to_date',$to_date)->with('er_msg',$er_msg)->with('replace_order_all',$replace_order_all);
           ;
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function get_approve_replacecontent_paypal($delStatus_id)
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $data  = MerchantTransactions::get_approve_content($delStatus_id);
            //print_r($data);
            echo '<b>Replace Note:</b><br>'.strip_tags($data[0]->replace_notes);
            echo '<br><b>Replace Applied On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->replace_date));
            if($data[0]->replace_status==1)
            {
                 if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE')!= '') 
                    {
                        $note_label = trans(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE');
                    }
                    else 
                    {
                        $note_label =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVE_DISAPPROVED_NOTE');
                    }
            echo '
                <form action="'.url('').'/approve_replace_request_paypal" method="post">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input name="cod_order_id" type="hidden" value="'.$data[0]->order_id.'"/>
                    <input name="order_cust_id" type="hidden" value="'.$data[0]->order_cust_id.'"/>
                    <input type="hidden" name="delStatus_id" value="'.$delStatus_id.'"/>
                    <div class="col-sm-12">
                        <label>'.$note_label.'</label>
                        <textarea class="form-control" required name="approve_or_disapprove_note"></textarea>
                    </div><br>
                    <div class="col-sm-12 ">
                        <input type="submit" class="btn btn-success" name="approve" value="Approve replace request">
                        <input type="submit" class="btn btn-danger" name="disapprove" value="Dis-Approve replace request">
                    </div>
                </form>';
            }
            else{
                $status ='Order Delivered';
                if($data[0]->replace_status==2)
                        $status ='Replaced';
                 if($data[0]->replace_status==4)
                        $status ='Disapproved';

                echo '<br><b>Replace Status:</b><br>'.$status;
                echo '<br><b>Replace Processed On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->replace_approved_date));
            }
             
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
public function get_approve_replacecontent_payumoney($delStatus_id)
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $data  = MerchantTransactions::get_approve_content($delStatus_id);
            //print_r($data);
            echo '<b>Replace Note:</b><br>'.strip_tags($data[0]->replace_notes);
            echo '<br><b>Replace Applied On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->replace_date));
            if($data[0]->replace_status==1)
            {
                 if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE')!= '') 
                    {
                        $note_label = trans(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE');
                    }
                    else 
                    {
                        $note_label =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVE_DISAPPROVED_NOTE');
                    }
            echo '
                <form action="'.url('').'/approve_replace_request_payu" method="post">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input name="cod_order_id" type="hidden" value="'.$data[0]->order_id.'"/>
                    <input name="order_cust_id" type="hidden" value="'.$data[0]->order_cust_id.'"/>
                    <input type="hidden" name="delStatus_id" value="'.$delStatus_id.'"/>
                    <div class="col-sm-12">
                        <label>'.$note_label.'</label>
                        <textarea class="form-control" required name="approve_or_disapprove_note"></textarea>
                    </div><br>
                    <div class="col-sm-12 ">
                        <input type="submit" class="btn btn-success" name="approve" value="Approve replace request">
                        <input type="submit" class="btn btn-danger" name="disapprove" value="Dis-Approve replace request">
                    </div>
                </form>';
            }
            else{
                $status ='Order Delivered';
                if($data[0]->replace_status==2)
                        $status ='Replaced';
                 if($data[0]->replace_status==4)
                        $status ='Disapproved';

                echo '<br><b>Replace Status:</b><br>'.$status;
                echo '<br><b>Replace Processed On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->replace_approved_date));
            }
             
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function approve_replace_request_paypal()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $cod_order_id=$_POST["cod_order_id"];
            $delStatus_id=$_POST["delStatus_id"];
            $cust_id=$_POST["order_cust_id"];
            $mer_id=Session::get('merchantid');
            $admin_id="1";
            $approve_or_disapprove_note=$_POST["approve_or_disapprove_note"];
            $return_Status =" "; 

            //check product qunatity to replace
            $prod_info =DB::table('nm_product as prod')->join("nm_order as ord","ord.order_pro_id",'=',"prod.pro_id")->join("nm_order_delivery_status AS od","od.prod_id",'=',"prod.pro_id")->select('prod.pro_id','prod.pro_no_of_purchase','prod.pro_qty','ord.order_qty as cod_qty')->where("od.delStatus_id","=",$delStatus_id)->where("ord.order_id","=",$cod_order_id)->first();
           // echo $_POST["disapprove"];
            //print_r($prod_info);exit();
            if(count($prod_info)>0){
                if(isset($_POST["approve"]))
                {
                    $available_qty = $prod_info->pro_qty - $prod_info->pro_no_of_purchase;
                    if($available_qty>$prod_info->cod_qty){
                        $new_purchased = $prod_info->pro_no_of_purchase+$prod_info->cod_qty;
                         MerchantTransactions::approve_replace_paypal($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');  
                         $return_Status ="Approved";   
                          //Update Product quantity
                        Products::edit_product(array('pro_no_of_purchase' => $new_purchased ),$prod_info->pro_id); 
                        if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY')!= '') 
                        {
                            $er_msg = trans(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY');
                        }
                        else 
                        {
                            $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVED_SUCCESSFULLY');
                        }
                        
                        Session::flash("er_msg",$er_msg);
                    }else{
                        $er_msg = "quantity not available";
                        Session::flash("er_msg",$er_msg);
                       return redirect("mer_replacement_orders")->with('er_msg',$er_msg);
                    }
                              
                }
                if(isset($_POST["disapprove"]))
                {
                     MerchantTransactions::disapprove_replace_paypal($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');
                     $return_Status ="Disapproved"; 
                     if(Lang::has(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY')!= '') 
                    {
                        $er_msg = trans(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY');
                    }
                    else 
                    {
                        $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DISAPPROVED_SUCCESSFULLY');
                    }

                    Session::flash("er_msg",$er_msg);
                    
                }


             /* mail starts */
                 if($delStatus_id !=''){
                    $deliveryDetails =  DB::table("nm_order_delivery_status AS od")->leftjoin("delivery_status_chat As dc","dc.delStatus_id","=","od.delStatus_id")->where("dc.delStatus_id","=",$delStatus_id)->where("dc.send_by","=",'2')->first(); 
                   // print_r($deliveryDetails) ;exit();
                        $prod_title = '';
                        $prod_title=DB::table('nm_product')->select('pro_title')->where("pro_id","=",$deliveryDetails->prod_id)->get();
                        $prod_title=$prod_title[0]->pro_title;


                        include('SMTP/sendmail.php');
                        //Customer mail
                        $custom_name = $customer_mail =''; 
                        $custom_data = DB::table('nm_customer')->select('cus_name','cus_email')->where('cus_id','=',$deliveryDetails->order_cust_id)->first();
                        if(count($custom_data)>0){
                        $custom_name = $custom_data->cus_name;
                        $customer_mail = $custom_data->cus_email;
                        }
                        if($customer_mail!='')
                        {

                            Mail::send('emails.replaceOrderApproveOrDisapprove_mail', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->replace_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'date'              =>  $deliveryDetails->replace_date,
                                'approve_status'    =>  $return_Status,
                                'approved_date'     =>  $deliveryDetails->replace_approved_date
                                ), function($message) use ($customer_mail)
                            {
                                $message->to($customer_mail)->subject('Order replace Approve/Disapprove Notification');
                            });     
                        }    
                        //merchant mail
                        $get_mer_email  = Home::get_mer_email($deliveryDetails->mer_id);
                        $mer_email      = $get_mer_email[0]->mer_email;
                        if($mer_email!=''){
                            Mail::send('emails.replaceOrderApproveOrDisapprove_mail_to_merchant', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->replace_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'date'              =>  $deliveryDetails->replace_date,
                                'approve_status'    =>  $return_Status,
                                'approved_date'     =>  $deliveryDetails->replace_approved_date
                                ), function($message) use ($mer_email)
                            {
                                $message->to($mer_email)->subject('Order replace Approve/Disapprove Notification');
                            }); 
                        }
                        //Admin mail
                        $adminData = DB::table('nm_admin')->where('adm_id', '=', '1')->first();
                        if(count($adminData)>0){
                            $adminEmail = $adminData->adm_email;
                            if($adminEmail!='')
                            {
                                Mail::send('emails.replaceOrderApproveOrDisapprove_mail_to_admin', array(
                                    'delStatus_id'      =>  $delStatus_id,
                                    'transaction_id'    =>  $deliveryDetails->transaction_id,
                                    'prod_id'           =>  $deliveryDetails->prod_id,
                                    'prod_title'        =>  $prod_title,
                                    'customer_name'     =>  $custom_name,
                                    'return_notes'      =>  strip_tags($deliveryDetails->replace_notes),
                                    'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                    'date'              =>  $deliveryDetails->replace_date,
                                    'approve_status'    =>  $return_Status,
                                    'approved_date'     =>  $deliveryDetails->replace_approved_date
                                ), function($message) use ($adminEmail)
                                {
                                    $message->to($adminEmail)->subject('Order replace Approve/Disapprove Notification');
                                }); 
                            }
                        }

                    }
                 /* mail ends */ 
     
            }else{
                $er_msg = "Product Not available";
                Session::flash("er_msg",$er_msg);
                return redirect("mer_replacement_orders");
            }     

            return redirect("mer_replacement_orders");
            
        } else {

            return Redirect::to('sitemerchant');
        }
        
    }
public function approve_replace_request_payu()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $cod_order_id=$_POST["cod_order_id"];
            $delStatus_id=$_POST["delStatus_id"];
            $cust_id=$_POST["order_cust_id"];
            $mer_id=Session::get('merchantid');
            $admin_id="1";
            $approve_or_disapprove_note=$_POST["approve_or_disapprove_note"];
            $return_Status =" "; 

            //check product qunatity to replace
            $prod_info =DB::table('nm_product as prod')->join("nm_order_payu as ord","ord.order_pro_id",'=',"prod.pro_id")->join("nm_order_delivery_status AS od","od.prod_id",'=',"prod.pro_id")->select('prod.pro_id','prod.pro_no_of_purchase','prod.pro_qty','ord.order_qty as cod_qty')->where("od.delStatus_id","=",$delStatus_id)->where("ord.order_id","=",$cod_order_id)->first();
           // echo $_POST["disapprove"];
            //print_r($prod_info);exit();
            if(count($prod_info)>0){
                if(isset($_POST["approve"]))
                {
                    $available_qty = $prod_info->pro_qty - $prod_info->pro_no_of_purchase;
                    if($available_qty>$prod_info->cod_qty){
                        $new_purchased = $prod_info->pro_no_of_purchase+$prod_info->cod_qty;
                         MerchantTransactions::approve_replace_payu($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');  
                         $return_Status ="Approved";   
                          //Update Product quantity
                        Products::edit_product(array('pro_no_of_purchase' => $new_purchased ),$prod_info->pro_id); 
                        if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY')!= '') 
                        {
                            $er_msg = trans(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY');
                        }
                        else 
                        {
                            $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVED_SUCCESSFULLY');
                        }
                        
                        Session::flash("er_msg",$er_msg);
                    }else{
                        $er_msg = "quantity not available";
                        Session::flash("er_msg",$er_msg);
                       return redirect("mer_replacement_orders")->with('er_msg',$er_msg);
                    }
                              
                }
                if(isset($_POST["disapprove"]))
                {
                     MerchantTransactions::disapprove_replace_payu($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');
                     $return_Status ="Disapproved"; 
                     if(Lang::has(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY')!= '') 
                    {
                        $er_msg = trans(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY');
                    }
                    else 
                    {
                        $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DISAPPROVED_SUCCESSFULLY');
                    }

                    Session::flash("er_msg",$er_msg);
                    
                }


             /* mail starts */
                 if($delStatus_id !=''){
                    $deliveryDetails =  DB::table("nm_order_delivery_status AS od")->leftjoin("delivery_status_chat As dc","dc.delStatus_id","=","od.delStatus_id")->where("dc.delStatus_id","=",$delStatus_id)->where("dc.send_by","=",'2')->first(); 
                   // print_r($deliveryDetails) ;exit();
                        $prod_title = '';
                        $prod_title=DB::table('nm_product')->select('pro_title')->where("pro_id","=",$deliveryDetails->prod_id)->get();
                        $prod_title=$prod_title[0]->pro_title;


                        include('SMTP/sendmail.php');
                        //Customer mail
                        $custom_name = $customer_mail =''; 
                        $custom_data = DB::table('nm_customer')->select('cus_name','cus_email')->where('cus_id','=',$deliveryDetails->order_cust_id)->first();
                        if(count($custom_data)>0){
                        $custom_name = $custom_data->cus_name;
                        $customer_mail = $custom_data->cus_email;
                        }
                        if($customer_mail!='')
                        {

                            Mail::send('emails.replaceOrderApproveOrDisapprove_mail', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->replace_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'date'              =>  $deliveryDetails->replace_date,
                                'approve_status'    =>  $return_Status,
                                'approved_date'     =>  $deliveryDetails->replace_approved_date
                                ), function($message) use ($customer_mail)
                            {
                                $message->to($customer_mail)->subject('Order replace Approve/Disapprove Notification');
                            });     
                        }    
                        //merchant mail
                        $get_mer_email  = Home::get_mer_email($deliveryDetails->mer_id);
                        $mer_email      = $get_mer_email[0]->mer_email;
                        if($mer_email!=''){
                            Mail::send('emails.replaceOrderApproveOrDisapprove_mail_to_merchant', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->replace_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'date'              =>  $deliveryDetails->replace_date,
                                'approve_status'    =>  $return_Status,
                                'approved_date'     =>  $deliveryDetails->replace_approved_date
                                ), function($message) use ($mer_email)
                            {
                                $message->to($mer_email)->subject('Order replace Approve/Disapprove Notification');
                            }); 
                        }
                        //Admin mail
                        $adminData = DB::table('nm_admin')->where('adm_id', '=', '1')->first();
                        if(count($adminData)>0){
                            $adminEmail = $adminData->adm_email;
                            if($adminEmail!='')
                            {
                                Mail::send('emails.replaceOrderApproveOrDisapprove_mail_to_admin', array(
                                    'delStatus_id'      =>  $delStatus_id,
                                    'transaction_id'    =>  $deliveryDetails->transaction_id,
                                    'prod_id'           =>  $deliveryDetails->prod_id,
                                    'prod_title'        =>  $prod_title,
                                    'customer_name'     =>  $custom_name,
                                    'return_notes'      =>  strip_tags($deliveryDetails->replace_notes),
                                    'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                    'date'              =>  $deliveryDetails->replace_date,
                                    'approve_status'    =>  $return_Status,
                                    'approved_date'     =>  $deliveryDetails->replace_approved_date
                                ), function($message) use ($adminEmail)
                                {
                                    $message->to($adminEmail)->subject('Order replace Approve/Disapprove Notification');
                                }); 
                            }
                        }

                    }
                 /* mail ends */ 
     
            }else{
                $er_msg = "Product Not available";
                Session::flash("er_msg",$er_msg);
                return redirect("mer_replacement_orders");
            }     

            return redirect("mer_replacement_orders");
            
        } else {

            return Redirect::to('sitemerchant');
        }
        
    }


    /* paypal replacement ends */
    /* paypal deal replacement starts */
    public function mer_deal_replacement_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
             $merid             = Session::get('merchantid');
            $payment_type=1;
            $order_type=2;
            $calceled_order  = MerchantTransactions::get_replace_order_paypal($merid,$payment_type,$order_type,$from_date,$to_date);
            $replace_order_all  = MerchantTransactions::replace_dealorder_allpaypal($from_date,$to_date,$merid); 
            $er_msg = '';
            if(Session::has("er_msg"))
                $er_msg = Session::get("er_msg");
            return view('sitemerchant.deal_replace_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("calceled_order", $calceled_order)->with('from_date',$from_date)->with('to_date',$to_date)->with('er_msg',$er_msg)->with('replace_order_all',$replace_order_all);
           ;
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function mer_payu_deal_replacement_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
             $merid             = Session::get('merchantid');
            $payment_type=1;
            $order_type=2;
            $calceled_order  = MerchantTransactions::get_payu_replace_order_paypal($merid,$payment_type,$order_type,$from_date,$to_date);
            $replace_order_all  = MerchantTransactions::replace_dealorder_allpayu($from_date,$to_date,$merid); 
            $er_msg = '';
            if(Session::has("er_msg"))
                $er_msg = Session::get("er_msg");
            return view('sitemerchant.payu_deal_replace_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("calceled_order", $calceled_order)->with('from_date',$from_date)->with('to_date',$to_date)->with('er_msg',$er_msg)->with('replace_order_all',$replace_order_all);
           ;
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }

    public function deal_get_approve_replacecontent_paypal($delStatus_id)
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $data  = MerchantTransactions::get_approve_content($delStatus_id);
            //print_r($data);
            echo '<b>Replace Note:</b><br>'.strip_tags($data[0]->replace_notes);
            echo '<br><b>Replace Applied On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->replace_date));
            if($data[0]->replace_status==1)
            {
                 if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE')!= '') 
                    {
                        $note_label = trans(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE');
                    }
                    else 
                    {
                        $note_label =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVE_DISAPPROVED_NOTE');
                    }
                
            echo '
                <form action="'.url('').'/deal_approve_replace_request_paypal" method="post">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input name="cod_order_id" type="hidden" value="'.$data[0]->order_id.'"/>
                    <input name="order_cust_id" type="hidden" value="'.$data[0]->order_cust_id.'"/>
                    <input type="hidden" name="delStatus_id" value="'.$delStatus_id.'"/>
                    <div class="col-sm-12">
                        <label>'.$note_label.'</label>
                        <textarea class="form-control" required name="approve_or_disapprove_note"></textarea>
                    </div><br>
                    <div class="col-sm-12 ">
                        <input type="submit" class="btn btn-success" name="approve" value="Approve replace request">
                        <input type="submit" class="btn btn-danger" name="disapprove" value="Dis-Approve replace request">
                    </div>
                </form>';
            }
            else{
                $status ='Order Delivered';
                if($data[0]->replace_status==2)
                        $status ='Replaced';
                 if($data[0]->replace_status==4)
                        $status ='Disapproved';

                echo '<br><b>Replace Status:</b><br>'.$status;
                echo '<br><b>Replace Processed On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->replace_approved_date));
            }
             
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
public function deal_get_approve_replacecontent_payu($delStatus_id)
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $data  = MerchantTransactions::get_approve_content($delStatus_id);
            //print_r($data);
            echo '<b>Replace Note:</b><br>'.strip_tags($data[0]->replace_notes);
            echo '<br><b>Replace Applied On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->replace_date));
            if($data[0]->replace_status==1)
            {
                 if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE')!= '') 
                    {
                        $note_label = trans(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE');
                    }
                    else 
                    {
                        $note_label =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVE_DISAPPROVED_NOTE');
                    }
                
            echo '
                <form action="'.url('').'/deal_approve_replace_request_payu" method="post">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input name="cod_order_id" type="hidden" value="'.$data[0]->order_id.'"/>
                    <input name="order_cust_id" type="hidden" value="'.$data[0]->order_cust_id.'"/>
                    <input type="hidden" name="delStatus_id" value="'.$delStatus_id.'"/>
                    <div class="col-sm-12">
                        <label>'.$note_label.'</label>
                        <textarea class="form-control" required name="approve_or_disapprove_note"></textarea>
                    </div><br>
                    <div class="col-sm-12 ">
                        <input type="submit" class="btn btn-success" name="approve" value="Approve replace request">
                        <input type="submit" class="btn btn-danger" name="disapprove" value="Dis-Approve replace request">
                    </div>
                </form>';
            }
            else{
                $status ='Order Delivered';
                if($data[0]->replace_status==2)
                        $status ='Replaced';
                 if($data[0]->replace_status==4)
                        $status ='Disapproved';

                echo '<br><b>Replace Status:</b><br>'.$status;
                echo '<br><b>Replace Processed On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->replace_approved_date));
            }
             
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function deal_approve_replace_request_paypal()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $cod_order_id=$_POST["cod_order_id"];
            $delStatus_id=$_POST["delStatus_id"];
            $cust_id=$_POST["order_cust_id"];
            $mer_id=Session::get('merchantid');
            $admin_id="1";
            $approve_or_disapprove_note=$_POST["approve_or_disapprove_note"];
            $return_Status =" "; 
          
            //check product qunatity to replace
             $prod_info =DB::table('nm_deals as prod')->join("nm_order as ord","ord.order_pro_id",'=',"prod.deal_id")->join("nm_order_delivery_status AS od","od.prod_id",'=',"prod.deal_id")->select('prod.deal_id as pro_id','prod.deal_no_of_purchase as pro_no_of_purchase','prod.deal_max_limit as pro_qty','ord.order_qty as cod_qty')->where("od.delStatus_id","=",$delStatus_id)->where("ord.order_id","=",$cod_order_id)->first();
           // echo $_POST["disapprove"];
          // print_r($prod_info);exit();
            if(count($prod_info)>0){
                if(isset($_POST["approve"]))
                {
                    $available_qty = $prod_info->pro_qty - $prod_info->pro_no_of_purchase;
                    if($available_qty>$prod_info->cod_qty){
                        $new_purchased = $prod_info->pro_no_of_purchase+$prod_info->cod_qty;
                         MerchantTransactions::approve_replace_paypal($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');  
                         $return_Status ="Approved";   
                        //Update Product quantity
                        DB::table('nm_deals')->where('deal_id', '=', $prod_info->pro_id)->update(array('deal_no_of_purchase' => $new_purchased  ));

                        
                        if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY')!= '') 
                        {
                            $er_msg = trans(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY');
                        }
                        else 
                        {
                            $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVED_SUCCESSFULLY');
                        }
                        Session::flash("er_msg",$er_msg);
                    }else{
                        $er_msg = "quantity not available";
                        Session::flash("er_msg",$er_msg);
                       return redirect("mer_deal_replacement_orders")->with('er_msg',$er_msg);
                    }
                              
                }
                if(isset($_POST["disapprove"]))
                {
                     MerchantTransactions::disapprove_replace_paypal($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');
                     $return_Status ="Disapproved"; 

                    if(Lang::has(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY')!= '') 
                    {
                        $er_msg = trans(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY');
                    }
                    else 
                    {
                        $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DISAPPROVED_SUCCESSFULLY');
                    } 
                    
                    Session::flash("er_msg",$er_msg);
                    
                }


             /* mail starts */
                 if($delStatus_id !=''){
                    $deliveryDetails =  DB::table("nm_order_delivery_status AS od")->leftjoin("delivery_status_chat As dc","dc.delStatus_id","=","od.delStatus_id")->where("dc.delStatus_id","=",$delStatus_id)->where("dc.send_by","=",'2')->first(); 
                   // print_r($deliveryDetails) ;exit();
                        $prod_title = '';
                        $prod_title=DB::table('nm_deals')->select('deal_title as pro_title')->where("deal_id","=",$deliveryDetails->prod_id)->get();
                        $prod_title=$prod_title[0]->pro_title;


                        include('SMTP/sendmail.php');
                        //Customer mail
                        $custom_name = $customer_mail =''; 
                        $custom_data = DB::table('nm_customer')->select('cus_name','cus_email')->where('cus_id','=',$deliveryDetails->order_cust_id)->first();
                        if(count($custom_data)>0){
                        $custom_name = $custom_data->cus_name;
                        $customer_mail = $custom_data->cus_email;
                        }
                        if($customer_mail!='')
                        {

                            Mail::send('emails.replaceOrderApproveOrDisapprove_mail', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->replace_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'date'              =>  $deliveryDetails->replace_date,
                                'approve_status'    =>  $return_Status,
                                'approved_date'     =>  $deliveryDetails->replace_approved_date
                                ), function($message) use ($customer_mail)
                            {
                                $message->to($customer_mail)->subject('Order replace Approve/Disapprove Notification');
                            });     
                        }    
                        //merchant mail
                        $get_mer_email  = Home::get_mer_email($deliveryDetails->mer_id);
                        $mer_email      = $get_mer_email[0]->mer_email;
                        if($mer_email!=''){
                            Mail::send('emails.replaceOrderApproveOrDisapprove_mail_to_merchant', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->replace_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'date'              =>  $deliveryDetails->replace_date,
                                'approve_status'    =>  $return_Status,
                                'approved_date'     =>  $deliveryDetails->replace_approved_date
                                ), function($message) use ($mer_email)
                            {
                                $message->to($mer_email)->subject('Order replace Approve/Disapprove Notification');
                            }); 
                        }
                        //Admin mail
                        $adminData = DB::table('nm_admin')->where('adm_id', '=', '1')->first();
                        if(count($adminData)>0){
                            $adminEmail = $adminData->adm_email;
                            if($adminEmail!='')
                            {
                                Mail::send('emails.replaceOrderApproveOrDisapprove_mail_to_admin', array(
                                    'delStatus_id'      =>  $delStatus_id,
                                    'transaction_id'    =>  $deliveryDetails->transaction_id,
                                    'prod_id'           =>  $deliveryDetails->prod_id,
                                    'prod_title'        =>  $prod_title,
                                    'customer_name'     =>  $custom_name,
                                    'return_notes'      =>  strip_tags($deliveryDetails->replace_notes),
                                    'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                    'date'              =>  $deliveryDetails->replace_date,
                                    'approve_status'    =>  $return_Status,
                                    'approved_date'     =>  $deliveryDetails->replace_approved_date
                                ), function($message) use ($adminEmail)
                                {
                                    $message->to($adminEmail)->subject('Order replace Approve/Disapprove Notification');
                                }); 
                            }
                        }

                    }
                 /* mail ends */ 
     
            }else{
                $er_msg = "Product Not available";
                Session::flash("er_msg",$er_msg);
                return redirect("mer_deal_replacement_orders");
            }     

            return redirect("mer_deal_replacement_orders");
            
        } else {

            return Redirect::to('sitemerchant');
        }
        
    }
 public function deal_approve_replace_request_payu()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $cod_order_id=$_POST["cod_order_id"];
            $delStatus_id=$_POST["delStatus_id"];
            $cust_id=$_POST["order_cust_id"];
            $mer_id=Session::get('merchantid');
            $admin_id="1";
            $approve_or_disapprove_note=$_POST["approve_or_disapprove_note"];
            $return_Status =" "; 
          
            //check product qunatity to replace
             $prod_info =DB::table('nm_deals as prod')->join("nm_order_payu as ord","ord.order_pro_id",'=',"prod.deal_id")->join("nm_order_delivery_status AS od","od.prod_id",'=',"prod.deal_id")->select('prod.deal_id as pro_id','prod.deal_no_of_purchase as pro_no_of_purchase','prod.deal_max_limit as pro_qty','ord.order_qty as cod_qty')->where("od.delStatus_id","=",$delStatus_id)->where("ord.order_id","=",$cod_order_id)->first();
           // echo $_POST["disapprove"];
          // print_r($prod_info);exit();
            if(count($prod_info)>0){
                if(isset($_POST["approve"]))
                {
                    $available_qty = $prod_info->pro_qty - $prod_info->pro_no_of_purchase;
                    if($available_qty>$prod_info->cod_qty){
                        $new_purchased = $prod_info->pro_no_of_purchase+$prod_info->cod_qty;
                         MerchantTransactions::approve_replace_payu($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');  
                         $return_Status ="Approved";   
                        //Update Product quantity
                        DB::table('nm_deals')->where('deal_id', '=', $prod_info->pro_id)->update(array('deal_no_of_purchase' => $new_purchased  ));

                        
                        if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY')!= '') 
                        {
                            $er_msg = trans(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY');
                        }
                        else 
                        {
                            $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVED_SUCCESSFULLY');
                        }
                        Session::flash("er_msg",$er_msg);
                    }else{
                        $er_msg = "quantity not available";
                        Session::flash("er_msg",$er_msg);
                       return redirect("mer_deal_replacement_orders")->with('er_msg',$er_msg);
                    }
                              
                }
                if(isset($_POST["disapprove"]))
                {
                     MerchantTransactions::disapprove_replace_payu($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');
                     $return_Status ="Disapproved"; 

                    if(Lang::has(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY')!= '') 
                    {
                        $er_msg = trans(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY');
                    }
                    else 
                    {
                        $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DISAPPROVED_SUCCESSFULLY');
                    } 
                    
                    Session::flash("er_msg",$er_msg);
                    
                }


             /* mail starts */
                 if($delStatus_id !=''){
                    $deliveryDetails =  DB::table("nm_order_delivery_status AS od")->leftjoin("delivery_status_chat As dc","dc.delStatus_id","=","od.delStatus_id")->where("dc.delStatus_id","=",$delStatus_id)->where("dc.send_by","=",'2')->first(); 
                   // print_r($deliveryDetails) ;exit();
                        $prod_title = '';
                        $prod_title=DB::table('nm_deals')->select('deal_title as pro_title')->where("deal_id","=",$deliveryDetails->prod_id)->get();
                        $prod_title=$prod_title[0]->pro_title;


                        include('SMTP/sendmail.php');
                        //Customer mail
                        $custom_name = $customer_mail =''; 
                        $custom_data = DB::table('nm_customer')->select('cus_name','cus_email')->where('cus_id','=',$deliveryDetails->order_cust_id)->first();
                        if(count($custom_data)>0){
                        $custom_name = $custom_data->cus_name;
                        $customer_mail = $custom_data->cus_email;
                        }
                        if($customer_mail!='')
                        {

                            Mail::send('emails.replaceOrderApproveOrDisapprove_mail', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->replace_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'date'              =>  $deliveryDetails->replace_date,
                                'approve_status'    =>  $return_Status,
                                'approved_date'     =>  $deliveryDetails->replace_approved_date
                                ), function($message) use ($customer_mail)
                            {
                                $message->to($customer_mail)->subject('Order replace Approve/Disapprove Notification');
                            });     
                        }    
                        //merchant mail
                        $get_mer_email  = Home::get_mer_email($deliveryDetails->mer_id);
                        $mer_email      = $get_mer_email[0]->mer_email;
                        if($mer_email!=''){
                            Mail::send('emails.replaceOrderApproveOrDisapprove_mail_to_merchant', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->replace_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'date'              =>  $deliveryDetails->replace_date,
                                'approve_status'    =>  $return_Status,
                                'approved_date'     =>  $deliveryDetails->replace_approved_date
                                ), function($message) use ($mer_email)
                            {
                                $message->to($mer_email)->subject('Order replace Approve/Disapprove Notification');
                            }); 
                        }
                        //Admin mail
                        $adminData = DB::table('nm_admin')->where('adm_id', '=', '1')->first();
                        if(count($adminData)>0){
                            $adminEmail = $adminData->adm_email;
                            if($adminEmail!='')
                            {
                                Mail::send('emails.replaceOrderApproveOrDisapprove_mail_to_admin', array(
                                    'delStatus_id'      =>  $delStatus_id,
                                    'transaction_id'    =>  $deliveryDetails->transaction_id,
                                    'prod_id'           =>  $deliveryDetails->prod_id,
                                    'prod_title'        =>  $prod_title,
                                    'customer_name'     =>  $custom_name,
                                    'return_notes'      =>  strip_tags($deliveryDetails->replace_notes),
                                    'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                    'date'              =>  $deliveryDetails->replace_date,
                                    'approve_status'    =>  $return_Status,
                                    'approved_date'     =>  $deliveryDetails->replace_approved_date
                                ), function($message) use ($adminEmail)
                                {
                                    $message->to($adminEmail)->subject('Order replace Approve/Disapprove Notification');
                                }); 
                            }
                        }

                    }
                 /* mail ends */ 
     
            }else{
                $er_msg = "Product Not available";
                Session::flash("er_msg",$er_msg);
                return redirect("mer_deal_replacement_orders");
            }     

            return redirect("mer_deal_replacement_orders");
            
        } else {

            return Redirect::to('sitemerchant');
        }
        
    }

    /* paypal deal replacement ends */


    /* REturn order starts */
	public function mer_cod_return_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
             $merid             = Session::get('merchantid');
            $payment_type=0;
            $order_type=1;
            $calceled_order  = MerchantTransactions::get_return_order($merid,$payment_type,$order_type,$from_date,$to_date);
            $return_order_all  = MerchantTransactions::get_return_order_alltype($from_date,$to_date,$merid); 



            $er_msg ='';
            if(Session::has('er_msg') )
                 $er_msg = Session::get('er_msg');
            return view('sitemerchant.productcod_return_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("calceled_order", $calceled_order)->with('from_date',$from_date)->with('to_date',$to_date)->with("er_msg",$er_msg)->with("return_order_all", $return_order_all);
           ;
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }

    public function get_approve_Returncontent($delStatus_id)
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $data  = MerchantTransactions::get_approve_content($delStatus_id);

            echo '<b>Return Note:</b><br>'.strip_tags($data[0]->return_notes);
            echo '<br><b>Return Applied On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->return_date));
            if($data[0]->return_status==1)
            {
                 if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE')!= '') 
                {
                    $note_label = trans(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE');
                }
                else 
                {
                    $note_label =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVE_DISAPPROVED_NOTE');
                }
            echo '
                <form action="'.url('').'/approve_return_request" method="post">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input name="cod_order_id" type="hidden" value="'.$data[0]->cod_order_id.'"/>
                    <input name="order_cust_id" type="hidden" value="'.$data[0]->order_cust_id.'"/>
                    <input type="hidden" name="delStatus_id" value="'.$delStatus_id.'"/>
                    <div class="col-sm-12">
                        <label>'.$note_label.'</label>
                        <textarea class="form-control" required name="approve_or_disapprove_note"></textarea>
                    </div><br>
                    <div class="col-sm-12 ">
                        <input type="submit" class="btn btn-success" name="approve" value="Approve return request">
                        <input type="submit" class="btn btn-danger" name="disapprove" value="Dis-Approve return request">
                    </div>
                </form>';
            }
            else{
                $status ='Order Delivered';
                if($data[0]->return_status==2)
                        $status ='Returned';
                if($data[0]->return_status==4)
                        $status ='Disapproved';

                echo '<br><b>Return Status:</b><br>'.$status;    
                echo '<br><b>Return Processed On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->return_approved_date));
            }
             
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }

    public function approve_return_request()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $cod_order_id=$_POST["cod_order_id"];
            $delStatus_id=$_POST["delStatus_id"];
            $cust_id=$_POST["order_cust_id"];
            $mer_id=Session::get('merchantid');
            $admin_id="1";
            $return_Status ="";   
            $approve_or_disapprove_note=$_POST["approve_or_disapprove_note"];
            if(isset($_POST["approve"]))
            {
                 MerchantTransactions::approve_return($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');
                 //add return amount to wallet
                 MerchantTransactions::pay_to_wallet_cod($cod_order_id,$cust_id);
                  $return_Status ="Approved"; 
                  if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY')!= '') 
                {
                    $er_msg = trans(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY');
                }
                else 
                {
                    $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVED_SUCCESSFULLY');
                }  
                  Session::flash('er_msg',$er_msg);  
            }
            if(isset($_POST["disapprove"]))
            {
                 MerchantTransactions::disapprove_return($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');
                $return_Status ="Disapproved";
                if(Lang::has(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY')!= '') 
                {
                    $er_msg = trans(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY');
                }
                else 
                {
                    $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DISAPPROVED_SUCCESSFULLY');
                }
                Session::flash('er_msg',$er_msg);     
            }

             if(Lang::has(Session::get('mer_lang_file').'.MER_RETURN_APPROVE_DISAPPROVED_SUBJECT')!= '') 
                {
                    $return_mail_subj = trans(Session::get('mer_lang_file').'.MER_RETURN_APPROVE_DISAPPROVED_SUBJECT');
                }
                else 
                {
                    $return_mail_subj =  trans($this->MER_OUR_LANGUAGE.'.MER_RETURN_APPROVE_DISAPPROVED_SUBJECT');
                }

            /* mail starts */
                 if($delStatus_id !=''){
                    $deliveryDetails =  DB::table("nm_order_delivery_status AS od")->leftjoin("delivery_status_chat As dc","dc.delStatus_id","=","od.delStatus_id")->where("dc.delStatus_id","=",$delStatus_id)->where("dc.send_by","=",'2')->first(); 
                   // print_r($deliveryDetails) ;exit();
                        $prod_title = '';
                        $prod_title=DB::table('nm_product')->select('pro_title')->where("pro_id","=",$deliveryDetails->prod_id)->get();
                        $prod_title=$prod_title[0]->pro_title;


                        include('SMTP/sendmail.php');
                        //Customer mail
                        $custom_name = $customer_mail =''; 
                        $custom_data = DB::table('nm_customer')->select('cus_name','cus_email')->where('cus_id','=',$deliveryDetails->order_cust_id)->first();
                        if(count($custom_data)>0){
                        $custom_name = $custom_data->cus_name;
                        $customer_mail = $custom_data->cus_email;
                        }
                         // set frontend language 
                                $this->setLanguageLocaleFront();

                        if($customer_mail!='')
                        {

                            Mail::send('emails.returnOrderApproveOrDisapprove_mail', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->return_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'date'              =>  $deliveryDetails->return_date,
                                'approve_status'    =>  $return_Status,
                                'approved_date'     =>  $deliveryDetails->return_approved_date
                                ), function($message) use ($customer_mail,$return_mail_subj)
                            {
                                $message->to($customer_mail)->subject($return_mail_subj);
                            });     
                        }    
                        //merchant mail
                        $get_mer_email  = Home::get_mer_email($deliveryDetails->mer_id);
                        $mer_email      = $get_mer_email[0]->mer_email;
                        if($mer_email!=''){
                            Mail::send('emails.returnOrderApproveOrDisapprove_mail_to_merchant', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->return_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'approve_status'    =>  $return_Status,
                                'date'              =>  $deliveryDetails->return_date,
                                'approved_date'     =>  $deliveryDetails->return_approved_date
                                ), function($message) use ($mer_email,$return_mail_subj)
                            {
                                $message->to($mer_email)->subject($return_mail_subj);
                            }); 
                        }
                        //Admin mail
                        $adminData = DB::table('nm_admin')->where('adm_id', '=', '1')->first();
                        if(count($adminData)>0){
                            $adminEmail = $adminData->adm_email;
                            if($adminEmail!='')
                            {
                                Mail::send('emails.returnOrderApproveOrDisapprove_mail_to_admin', array(
                                    'delStatus_id'      =>  $delStatus_id,
                                    'transaction_id'    =>  $deliveryDetails->transaction_id,
                                    'prod_id'           =>  $deliveryDetails->prod_id,
                                    'prod_title'        =>  $prod_title,
                                    'customer_name'     =>  $custom_name,
                                    'return_notes'      =>  strip_tags($deliveryDetails->return_notes),
                                    'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                    'approve_status'    =>  $return_Status,
                                    'date'              =>  $deliveryDetails->return_date,
                                    'approved_date'     =>  $deliveryDetails->return_approved_date
                                ), function($message) use ($adminEmail,$return_mail_subj)
                                {
                                    $message->to($adminEmail)->subject($return_mail_subj);
                                }); 
                            }
                        }

                    }
                 /* mail ends */ 


           return Redirect::to('mer_cod_return_orders');
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }

    /* REturn order ends */

	public function mer_cod_cancel_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $payment_type=0;
            $order_type=1;
            $calceled_order_all  = MerchantTransactions::get_calceled_order($merid,$payment_type,$order_type,$from_date,$to_date);
            $calceled_order  = MerchantTransactions::get_calceled_order_alltype($from_date,$to_date,$merid);

             


            /* echo '<pre>';print_r($calceled_order);die; */
            $er_msg = '';
            return view('sitemerchant.productcod_canceled_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("calceled_order", $calceled_order)->with('from_date',$from_date)->with('to_date',$to_date)->with('er_msg',$er_msg)->with('calceled_order_all',$calceled_order_all);
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function mer_cancel_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
			$merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
			$payment_type=1;
			$order_type=1;
            $calceled_order  = MerchantTransactions::get_calceled_order_paypal($merid,$payment_type,$order_type,$from_date,$to_date);

            $calceled_order_all  = MerchantTransactions::calceled_order_allpaypal($from_date,$to_date,$merid); 
			/* echo '<pre>';print_r($calceled_order);die; */
            $er_msg = '';
            return view('sitemerchant.productcod_canceled_orders_paypal')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("calceled_order", $calceled_order)->with('from_date',$from_date)->with('to_date',$to_date)->with('er_msg',$er_msg)->with('calceled_order_all',$calceled_order_all);
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function mer_payu_cancel_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $payment_type=1;
            $order_type=1;
            $calceled_order  = MerchantTransactions::get_calceled_order_payu($merid,$payment_type,$order_type,$from_date,$to_date);

            $calceled_order_all  = MerchantTransactions::calceled_order_allpayu($from_date,$to_date,$merid); 
            /* echo '<pre>';print_r($calceled_order);die; */
            $er_msg = '';
            return view('sitemerchant.product_payu_canceled_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("calceled_order", $calceled_order)->with('from_date',$from_date)->with('to_date',$to_date)->with('er_msg',$er_msg)->with('calceled_order_all',$calceled_order_all);
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }

    
	public function get_approve_content($delStatus_id)
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $data  = MerchantTransactions::get_approve_content($delStatus_id);
           //echo '<pre>';print_r($data);die;
            echo '<b>Cancel Note:</b><br>'.strip_tags($data[0]->cancel_notes);
            echo '<br><b>Cancel Applied On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->cancel_date));
            if($data[0]->cancel_status==1)
            {
                 if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE')!= '') 
                {
                    $note_label = trans(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE');
                }
                else 
                {
                    $note_label =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVE_DISAPPROVED_NOTE');
                }
            echo '
                <form action="'.url('').'/approve_cancel_request" method="post">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input name="cod_order_id" type="hidden" value="'.$data[0]->cod_order_id.'"/>
                    <input name="order_cust_id" type="hidden" value="'.$data[0]->order_cust_id.'"/>
                    <input name="date" type="hidden" value="'.$data[0]->cancel_date.'"/>
                    <input name="prod_id" type="hidden" value="'.$data[0]->prod_id.'"/>
                    <input name="transaction_id" type="hidden" value="'.$data[0]->transaction_id.'"/>
                    <input type="hidden" name="delStatus_id" value="'.$delStatus_id.'"/>
                    <input type="hidden" name="get_subtotal" value="'.$data[0]->transaction_id.'"/>
                    <div class="col-sm-12">
                        <label>'.$note_label.'</label>
                        <textarea class="form-control" required name="approve_or_disapprove_note"></textarea>
                    </div>
                    <div class="col-sm-12 btn-group">
                        <input type="submit" class="btn btn-success" name="approve" value="Approve cancel request">
                        <input type="submit" class="btn btn-danger" name="disapprove" value="Dis-Approve cancel request">
                    </div>
                </form>';
            }
            else{
                $status ='Order Delivered';
                if($data[0]->cancel_status==2)
                        $status ='Cancelled';
                if($data[0]->cancel_status==4)
                        $status ='Disapproved';
                echo '<br><b>Cancel Status:</b><br>'.$status;
                echo '<br><b>Cancel Processed On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->cancel_approved_date));
            }
            //echo '<pre>';print_r($data);die; 
            //echo $cod_id; 
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function get_approve_content_paypal($delStatus_id)
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
			$data  = MerchantTransactions::get_approve_content($delStatus_id);
           //echo '<pre>';print_r($data);die;
			echo '<b>Cancel Note:</b><br>'.strip_tags($data[0]->cancel_notes);
            echo '<br><b>Cancel Applied On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->cancel_date));
			if($data[0]->cancel_status==1)
			{
                 if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE')!= '') 
                {
                    $note_label = trans(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE');
                }
                else 
                {
                    $note_label =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVE_DISAPPROVED_NOTE');
                }
			echo '
                <form action="'.url('').'/approve_cancel_request_paypal" method="post">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input name="order_id" type="hidden" value="'.$data[0]->order_id.'"/>
                    <input name="order_cust_id" type="hidden" value="'.$data[0]->order_cust_id.'"/>
                    <input name="date" type="hidden" value="'.$data[0]->cancel_date.'"/>
                    <input name="prod_id" type="hidden" value="'.$data[0]->prod_id.'"/>
                    <input name="transaction_id" type="hidden" value="'.$data[0]->transaction_id.'"/>
                    <input type="hidden" name="delStatus_id" value="'.$delStatus_id.'"/>
                    <input type="hidden" name="get_subtotal" value="'.$data[0]->transaction_id.'"/>
                    <div class="col-sm-12">
                        <label>'.$note_label.'</label>
                        <textarea class="form-control" required name="approve_or_disapprove_note"></textarea>
                    </div>
                    <div class="col-sm-12 btn-group">
                        <input type="submit" class="btn btn-success" name="approve" value="Approve cancel request">
                        <input type="submit" class="btn btn-danger" name="disapprove" value="Dis-Approve cancel request">
                    </div>
                </form>';
			}
			else{
                $status ='Order Delivered';
                if($data[0]->cancel_status==2)
                        $status ='Cancelled';
                if($data[0]->cancel_status==4)
                        $status ='Disapproved';
                echo '<br><b>Cancel Status:</b><br>'.$status;
				echo '<br><b>Cancel Processed On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->cancel_approved_date));
			}
			//echo '<pre>';print_r($data);die; 
			//echo $cod_id; 
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function get_payu_approve_content($delStatus_id)
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $data  = MerchantTransactions::get_approve_content($delStatus_id);
           //echo '<pre>';print_r($data);die;
            echo '<b>Cancel Note:</b><br>'.strip_tags($data[0]->cancel_notes);
            echo '<br><b>Cancel Applied On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->cancel_date));
            if($data[0]->cancel_status==1)
            {
                 if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE')!= '') 
                {
                    $note_label = trans(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE');
                }
                else 
                {
                    $note_label =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVE_DISAPPROVED_NOTE');
                }
            echo '
                <form action="'.url('').'/approve_cancel_request_payumoney" method="post">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input name="order_id" type="hidden" value="'.$data[0]->order_id.'"/>
                    <input name="order_cust_id" type="hidden" value="'.$data[0]->order_cust_id.'"/>
                    <input name="date" type="hidden" value="'.$data[0]->cancel_date.'"/>
                    <input name="prod_id" type="hidden" value="'.$data[0]->prod_id.'"/>
                    <input name="transaction_id" type="hidden" value="'.$data[0]->transaction_id.'"/>
                    <input type="hidden" name="delStatus_id" value="'.$delStatus_id.'"/>
                    <input type="hidden" name="get_subtotal" value="'.$data[0]->transaction_id.'"/>
                    <div class="col-sm-12">
                        <label>'.$note_label.'</label>
                        <textarea class="form-control" required name="approve_or_disapprove_note"></textarea>
                    </div>
                    <div class="col-sm-12 btn-group">
                        <input type="submit" class="btn btn-success" name="approve" value="Approve cancel request">
                        <input type="submit" class="btn btn-danger" name="disapprove" value="Dis-Approve cancel request">
                    </div>
                </form>';
            }
            else{
                $status ='Order Delivered';
                if($data[0]->cancel_status==2)
                        $status ='Cancelled';
                if($data[0]->cancel_status==4)
                        $status ='Disapproved';
                echo '<br><b>Cancel Status:</b><br>'.$status;
                echo '<br><b>Cancel Processed On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->cancel_approved_date));
            }
            //echo '<pre>';print_r($data);die; 
            //echo $cod_id; 
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
	public function approve_cancel_request()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            /*print_r($_POST);die;*/
            $cod_order_id=$_POST["cod_order_id"];
            $order_detail=DB::table('nm_ordercod')->select('cod_amt','cod_taxAmt')->where("cod_id","=",$cod_order_id)->get();
            $delStatus_id=$_POST["delStatus_id"];

            $cust_id=$_POST["order_cust_id"];
            $customer_det=DB::table('nm_customer')->select('cus_name','cus_email')->where("cus_id","=",$cust_id)->get();
            $customer_name=$customer_det[0]->cus_name;
            $customer_email=$customer_det[0]->cus_email;

            $date=$_POST["date"];
            $prod_id=$_POST["prod_id"];
            $prod_title=DB::table('nm_product')->select('pro_title')->where("pro_id","=",$prod_id)->get();
            $prod_title=$prod_title[0]->pro_title;
            $transaction_id=$_POST["transaction_id"];
            $get_subtotal=$order_detail[0]->cod_amt+$order_detail[0]->cod_taxAmt;
            
            $mer_id=Session::get('merchantid');
            $merchant_det=DB::table('nm_merchant')->select('mer_email')->where("mer_id","=",$mer_id)->get();
            $merchant_email=$merchant_det[0]->mer_email;

            $admin_id="1";
            $admin_det=DB::table('nm_admin')->select('adm_email')->where("adm_id","=",$admin_id)->get();
            $admin_email=$admin_det[0]->adm_email;

            $array = array('email_id'=>"sathyaseelan@pofitec.com");
            $approve_or_disapprove_note=$_POST["approve_or_disapprove_note"];
            $this->setLanguageLocaleFront();
            if(isset($_POST["approve"]))
            {
                 MerchantTransactions::approve_cancel($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id);
                 /* Sending mail to following merchanta customer and admin */

                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail', array(   
                    'status' => 'Success',
                    'approve_status' => 'Approved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$customer_email)
                    {
                        $message->to($customer_email)->subject("Cancel Request Approved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_merchant', array(    
                    'status' => 'Success',
                    'approve_status' => 'Approved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$merchant_email)
                    {
                        $message->to($merchant_email)->subject("Cancel Request Approved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_admin', array(    
                    'status' => 'Success',
                    'approve_status' => 'Approved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$admin_email)
                    {
                        $message->to($admin_email)->subject("Cancel Request Approved");
                    });
            }
            if(isset($_POST["disapprove"]))
            {
                 MerchantTransactions::disapprove_cancel($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id);
                 /* Sending mail to following merchanta customer and admin */

                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail', array(   
                    'status' => 'Success',
                    'approve_status' => 'DisApproved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$customer_email)
                    {
                        $message->to($customer_email)->subject("Cancel Request DisApproved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_merchant', array(    
                    'status' => 'Success',
                    'approve_status' => 'DisApproved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$merchant_email)
                    {
                        $message->to($merchant_email)->subject("Cancel Request DisApproved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_admin', array(    
                    'status' => 'Success',
                    'approve_status' => 'DisApproved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$admin_email)
                    {
                        $message->to($admin_email)->subject("Cancel Request DisApproved");
                    });
            }
           return Redirect::to('mer_cod_cancel_orders');
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function approve_cancel_request_paypal()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            /*print_r($_POST);die;*/
			$cod_order_id=$_POST["order_id"];
            $order_detail=DB::table('nm_order')->select('order_amt as cod_amt','order_taxAmt as cod_taxAmt')->where("order_id","=",$cod_order_id)->get();
            $delStatus_id=$_POST["delStatus_id"];

            $cust_id=$_POST["order_cust_id"];
            $customer_det=DB::table('nm_customer')->select('cus_name','cus_email')->where("cus_id","=",$cust_id)->get();
            $customer_name=$customer_det[0]->cus_name;
            $customer_email=$customer_det[0]->cus_email;

            $date=$_POST["date"];
            $prod_id=$_POST["prod_id"];
            $prod_title=DB::table('nm_product')->select('pro_title')->where("pro_id","=",$prod_id)->get();
            $prod_title=$prod_title[0]->pro_title;
            $transaction_id=$_POST["transaction_id"];
            $get_subtotal=$order_detail[0]->cod_amt+$order_detail[0]->cod_taxAmt;
            
            $mer_id=Session::get('merchantid');
            $merchant_det=DB::table('nm_merchant')->select('mer_email')->where("mer_id","=",$mer_id)->get();
            $merchant_email=$merchant_det[0]->mer_email;

            $admin_id="1";
            $admin_det=DB::table('nm_admin')->select('adm_email')->where("adm_id","=",$admin_id)->get();
            $admin_email=$admin_det[0]->adm_email;

            $array = array('email_id'=>"sathyaseelan@pofitec.com");
			$approve_or_disapprove_note=$_POST["approve_or_disapprove_note"];
            $this->setLanguageLocaleFront();
            if(isset($_POST["approve"]))
            {
                 MerchantTransactions::approve_cancel_paypal($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id);
                 /* Sending mail to following merchanta customer and admin */
                 MerchantTransactions::pay_to_wallet($order_id,$cust_id);
                

                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail', array(   
                    'status' => 'Success',
                    'approve_status' => 'Approved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$customer_email)
                    {
                        $message->to($customer_email)->subject("Cancel Request Approved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_merchant', array(    
                    'status' => 'Success',
                    'approve_status' => 'Approved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$merchant_email)
                    {
                        $message->to($merchant_email)->subject("Cancel Request Approved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_admin', array(    
                    'status' => 'Success',
                    'approve_status' => 'Approved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$admin_email)
                    {
                        $message->to($admin_email)->subject("Cancel Request Approved");
                    });
            }
            if(isset($_POST["disapprove"]))
            {
			     MerchantTransactions::disapprove_cancel($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id);
                 /* Sending mail to following merchanta customer and admin */

                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail', array(   
                    'status' => 'Success',
                    'approve_status' => 'DisApproved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$customer_email)
                    {
                        $message->to($customer_email)->subject("Cancel Request DisApproved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_merchant', array(    
                    'status' => 'Success',
                    'approve_status' => 'DisApproved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$merchant_email)
                    {
                        $message->to($merchant_email)->subject("Cancel Request DisApproved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_admin', array(    
                    'status' => 'Success',
                    'approve_status' => 'DisApproved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$admin_email)
                    {
                        $message->to($admin_email)->subject("Cancel Request DisApproved");
                    });
            }
           return Redirect::to('mer_cancel_orders');
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function approve_cancel_request_payumoney()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            /*print_r($_POST);die;*/
            $cod_order_id=$_POST["order_id"];
            $order_detail=DB::table('nm_order_payu')->select('order_amt as cod_amt','order_taxAmt as cod_taxAmt')->where("order_id","=",$cod_order_id)->get();
            $delStatus_id=$_POST["delStatus_id"];

            $cust_id=$_POST["order_cust_id"];
            $customer_det=DB::table('nm_customer')->select('cus_name','cus_email')->where("cus_id","=",$cust_id)->get();
            $customer_name=$customer_det[0]->cus_name;
            $customer_email=$customer_det[0]->cus_email;

            $date=$_POST["date"];
            $prod_id=$_POST["prod_id"];
            $prod_title=DB::table('nm_product')->select('pro_title')->where("pro_id","=",$prod_id)->get();
            $prod_title=$prod_title[0]->pro_title;
            $transaction_id=$_POST["transaction_id"];
            $get_subtotal=$order_detail[0]->cod_amt+$order_detail[0]->cod_taxAmt;
            
            $mer_id=Session::get('merchantid');
            $merchant_det=DB::table('nm_merchant')->select('mer_email')->where("mer_id","=",$mer_id)->get();
            $merchant_email=$merchant_det[0]->mer_email;

            $admin_id="1";
            $admin_det=DB::table('nm_admin')->select('adm_email')->where("adm_id","=",$admin_id)->get();
            $admin_email=$admin_det[0]->adm_email;

            $array = array('email_id'=>"sathyaseelan@pofitec.com");
            $approve_or_disapprove_note=$_POST["approve_or_disapprove_note"];
            $this->setLanguageLocaleFront();
            if(isset($_POST["approve"]))
            {
                 MerchantTransactions::approve_cancel_payu($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id);
                 /* Sending mail to following merchanta customer and admin */
                 MerchantTransactions::pay_to_wallet_payu($order_id,$cust_id);
                

                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail', array(   
                    'status' => 'Success',
                    'approve_status' => 'Approved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$customer_email)
                    {
                        $message->to($customer_email)->subject("Cancel Request Approved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_merchant', array(    
                    'status' => 'Success',
                    'approve_status' => 'Approved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$merchant_email)
                    {
                        $message->to($merchant_email)->subject("Cancel Request Approved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_admin', array(    
                    'status' => 'Success',
                    'approve_status' => 'Approved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$admin_email)
                    {
                        $message->to($admin_email)->subject("Cancel Request Approved");
                    });
            }
            if(isset($_POST["disapprove"]))
            {
                 MerchantTransactions::disapprove_cancel($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id);
                 /* Sending mail to following merchanta customer and admin */

                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail', array(   
                    'status' => 'Success',
                    'approve_status' => 'DisApproved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$customer_email)
                    {
                        $message->to($customer_email)->subject("Cancel Request DisApproved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_merchant', array(    
                    'status' => 'Success',
                    'approve_status' => 'DisApproved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$merchant_email)
                    {
                        $message->to($merchant_email)->subject("Cancel Request DisApproved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_admin', array(    
                    'status' => 'Success',
                    'approve_status' => 'DisApproved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$admin_email)
                    {
                        $message->to($admin_email)->subject("Cancel Request DisApproved");
                    });
            }
           return Redirect::to('mer_cancel_orders');
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }

    /* deal paypal starts */  
    public function mer_deal_cancel_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $payment_type=1;
            $order_type=2;
            $calceled_order  = MerchantTransactions::get_calceled_order_paypal_deals($merid,$payment_type,$order_type,$from_date,$to_date);
            $calceled_order_alldeal  = MerchantTransactions::calceled_dealorder_allpaypal($from_date,$to_date,$merid); 
            /* echo '<pre>';print_r($calceled_order);die; */
            $er_msg = '';
            return view('sitemerchant.deal_canceled_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("calceled_order", $calceled_order)->with('from_date',$from_date)->with('to_date',$to_date)->with('er_msg',$er_msg)->with('calceled_order_alldeal',$calceled_order_alldeal);
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }  
public function mer_payu_deal_cancel_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $payment_type=1;
            $order_type=2;
            $calceled_order  = MerchantTransactions::get_payu_calceled_order_deals($merid,$payment_type,$order_type,$from_date,$to_date);
            $calceled_order_alldeal  = MerchantTransactions::calceled_dealorder_all_payu($from_date,$to_date,$merid); 
            /* echo '<pre>';print_r($calceled_order);die; */
            $er_msg = '';
            return view('sitemerchant.payu_deal_canceled_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("calceled_order", $calceled_order)->with('from_date',$from_date)->with('to_date',$to_date)->with('er_msg',$er_msg)->with('calceled_order_alldeal',$calceled_order_alldeal);
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }  
     public function deal_get_approve_content_paypal($delStatus_id)
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $data  = MerchantTransactions::get_approve_content($delStatus_id);
           //echo '<pre>';print_r($data);die;
              if(Lang::has(Session::get('mer_lang_file').'.MER_CANCEL_NOTE')!= '') 
                {
                    $cancelnote_label   = trans(Session::get('mer_lang_file').'.MER_CANCEL_NOTE');
                    $note_label         = trans(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE');
                    $appplied_label         = trans(Session::get('mer_lang_file').'.MER_CANCEL_APPLIED_ON'); 
                    $status_label           = trans(Session::get('mer_lang_file').'.MER_CANCEL_STATUS'); 
                    $processed_onLabel      = trans(Session::get('mer_lang_file').'.MER_CANCEL_PROCESSED_ON');
                }
                else 
                {
                    $cancelnote_label =  trans($this->MER_OUR_LANGUAGE.'.MER_CANCEL_NOTE');
                    $note_label       =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVE_DISAPPROVED_NOTE');
                    $appplied_label   =  trans($this->MER_OUR_LANGUAGE.'.MER_CANCEL_APPLIED_ON');
                    $status_label       =  trans($this->MER_OUR_LANGUAGE.'.MER_CANCEL_STATUS');
                    $processed_onLabel  =  trans($this->MER_OUR_LANGUAGE.'.MER_CANCEL_PROCESSED_ON');
                }
            echo '<b>'.$cancelnote_label.':</b><br>'.strip_tags($data[0]->cancel_notes);
            echo '<br><b>'.$appplied_label.':</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->cancel_date));
            if($data[0]->cancel_status==1)
            {
               
            echo '
                <form action="'.url('').'/deal_approve_cancel_request_paypal" method="post">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input name="order_id" type="hidden" value="'.$data[0]->order_id.'"/>
                    <input name="order_cust_id" type="hidden" value="'.$data[0]->order_cust_id.'"/>
                    <input name="date" type="hidden" value="'.$data[0]->cancel_date.'"/>
                    <input name="prod_id" type="hidden" value="'.$data[0]->prod_id.'"/>
                    <input name="transaction_id" type="hidden" value="'.$data[0]->transaction_id.'"/>
                    <input type="hidden" name="delStatus_id" value="'.$delStatus_id.'"/>
                    <input type="hidden" name="get_subtotal" value="'.$data[0]->transaction_id.'"/>
                    <div class="col-sm-12">
                        <label>'.$note_label.'</label>
                        <textarea class="form-control" required name="approve_or_disapprove_note"></textarea>
                    </div><br>
                    <div class="col-sm-12 ">
                        <input type="submit" class="btn btn-success" name="approve" value="Approve cancel request">
                        <input type="submit" class="btn btn-danger" name="disapprove" value="Dis-Approve cancel request">
                    </div>
                </form>';
            }
            else{
                $status ='Order Delivered';
                if($data[0]->cancel_status==2)
                        $status ='Cancelled';
                if($data[0]->cancel_status==4)
                        $status ='Disapproved';
                echo '<br><b>'.$status_label.':</b><br>'.$status;
                echo '<br><b>'.$processed_onLabel.':</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->cancel_approved_date));
            }
            //echo '<pre>';print_r($data);die; 
            //echo $cod_id; 
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
 public function deal_get_approve_content_payu($delStatus_id)
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $data  = MerchantTransactions::get_approve_content($delStatus_id);
           //echo '<pre>';print_r($data);die;
              if(Lang::has(Session::get('mer_lang_file').'.MER_CANCEL_NOTE')!= '') 
                {
                    $cancelnote_label   = trans(Session::get('mer_lang_file').'.MER_CANCEL_NOTE');
                    $note_label         = trans(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE');
                    $appplied_label         = trans(Session::get('mer_lang_file').'.MER_CANCEL_APPLIED_ON'); 
                    $status_label           = trans(Session::get('mer_lang_file').'.MER_CANCEL_STATUS'); 
                    $processed_onLabel      = trans(Session::get('mer_lang_file').'.MER_CANCEL_PROCESSED_ON');
                }
                else 
                {
                    $cancelnote_label =  trans($this->MER_OUR_LANGUAGE.'.MER_CANCEL_NOTE');
                    $note_label       =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVE_DISAPPROVED_NOTE');
                    $appplied_label   =  trans($this->MER_OUR_LANGUAGE.'.MER_CANCEL_APPLIED_ON');
                    $status_label       =  trans($this->MER_OUR_LANGUAGE.'.MER_CANCEL_STATUS');
                    $processed_onLabel  =  trans($this->MER_OUR_LANGUAGE.'.MER_CANCEL_PROCESSED_ON');
                }
            echo '<b>'.$cancelnote_label.':</b><br>'.strip_tags($data[0]->cancel_notes);
            echo '<br><b>'.$appplied_label.':</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->cancel_date));
            if($data[0]->cancel_status==1)
            {
               
            echo '
                <form action="'.url('').'/deal_approve_cancel_request_payu" method="post">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input name="order_id" type="hidden" value="'.$data[0]->order_id.'"/>
                    <input name="order_cust_id" type="hidden" value="'.$data[0]->order_cust_id.'"/>
                    <input name="date" type="hidden" value="'.$data[0]->cancel_date.'"/>
                    <input name="prod_id" type="hidden" value="'.$data[0]->prod_id.'"/>
                    <input name="transaction_id" type="hidden" value="'.$data[0]->transaction_id.'"/>
                    <input type="hidden" name="delStatus_id" value="'.$delStatus_id.'"/>
                    <input type="hidden" name="get_subtotal" value="'.$data[0]->transaction_id.'"/>
                    <div class="col-sm-12">
                        <label>'.$note_label.'</label>
                        <textarea class="form-control" required name="approve_or_disapprove_note"></textarea>
                    </div><br>
                    <div class="col-sm-12 ">
                        <input type="submit" class="btn btn-success" name="approve" value="Approve cancel request">
                        <input type="submit" class="btn btn-danger" name="disapprove" value="Dis-Approve cancel request">
                    </div>
                </form>';
            }
            else{
                $status ='Order Delivered';
                if($data[0]->cancel_status==2)
                        $status ='Cancelled';
                if($data[0]->cancel_status==4)
                        $status ='Disapproved';
                echo '<br><b>'.$status_label.':</b><br>'.$status;
                echo '<br><b>'.$processed_onLabel.':</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->cancel_approved_date));
            }
            //echo '<pre>';print_r($data);die; 
            //echo $cod_id; 
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function deal_approve_cancel_request_payu()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            /*print_r($_POST);die;*/
            $cod_order_id = $order_id = $_POST["order_id"];
            $order_detail=DB::table('nm_order_payu')->select('order_amt as cod_amt','order_taxAmt as cod_taxAmt')->where("order_id","=",$order_id)->get();
            $delStatus_id=$_POST["delStatus_id"];

            $cust_id=$_POST["order_cust_id"];
            $customer_det=DB::table('nm_customer')->select('cus_name','cus_email')->where("cus_id","=",$cust_id)->get();
            $customer_name=$customer_det[0]->cus_name;
            $customer_email=$customer_det[0]->cus_email;

            $date=$_POST["date"];
            $prod_id=$_POST["prod_id"];
            $prod_title=DB::table('nm_deals')->select('deal_title as pro_title')->where("deal_id","=",$prod_id)->get();
            $prod_title=$prod_title[0]->pro_title;
            $transaction_id=$_POST["transaction_id"];
            $get_subtotal=$order_detail[0]->cod_amt+$order_detail[0]->cod_taxAmt;
            
            $mer_id=Session::get('merchantid');
            $merchant_det=DB::table('nm_merchant')->select('mer_email')->where("mer_id","=",$mer_id)->get();
            $merchant_email=$merchant_det[0]->mer_email;

            $admin_id="1";
            $admin_det=DB::table('nm_admin')->select('adm_email')->where("adm_id","=",$admin_id)->get();
            $admin_email=$admin_det[0]->adm_email;

            
            $approve_or_disapprove_note=$_POST["approve_or_disapprove_note"];
            $this->setLanguageLocaleFront();
            if(isset($_POST["approve"]))
            {
                 MerchantTransactions::approve_cancel_payu($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id);
                 /* Sending mail to following merchanta customer and admin */
                 MerchantTransactions::pay_to_wallet_payu($order_id,$cust_id);
                

                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail', array(   
                    'status' => 'Success',
                    'approve_status' => 'Approved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($customer_email)
                    {
                        $message->to($customer_email)->subject("Cancel Request Approved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_merchant', array(    
                    'status' => 'Success',
                    'approve_status' => 'Approved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($merchant_email)
                    {
                        $message->to($merchant_email)->subject("Cancel Request Approved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_admin', array(    
                    'status' => 'Success',
                    'approve_status' => 'Approved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($admin_email)
                    {
                        $message->to($admin_email)->subject("Cancel Request Approved");
                    });
            }
            if(isset($_POST["disapprove"]))
            {
                 MerchantTransactions::disapprove_cancel_payu($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id);
                 /* Sending mail to following merchanta customer and admin */

                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail', array(   
                    'status' => 'Success',
                    'approve_status' => 'DisApproved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($customer_email)
                    {
                        $message->to($customer_email)->subject("Cancel Request DisApproved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_merchant', array(    
                    'status' => 'Success',
                    'approve_status' => 'DisApproved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($merchant_email)
                    {
                        $message->to($merchant_email)->subject("Cancel Request DisApproved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_admin', array(    
                    'status' => 'Success',
                    'approve_status' => 'DisApproved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($admin_email)
                    {
                        $message->to($admin_email)->subject("Cancel Request DisApproved");
                    });
            }
           return Redirect::to('mer_deal_cancel_orders');
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    /* deal paypal ends */  

    /* deal cod  start*/  
    public function mer_dealcod_cancel_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $payment_type=0;
            $order_type=2;
            $calceled_order  = MerchantTransactions::get_calceled_order($merid,$payment_type,$order_type,$from_date,$to_date);
            $calceled_order_all  = MerchantTransactions::calceled_dealorder_allcod($from_date,$to_date,$merid); 
            /* echo '<pre>';print_r($calceled_order);die; */
            $er_msg = '';
            return view('sitemerchant.dealcod_canceled_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("calceled_order", $calceled_order)->with('from_date',$from_date)->with('to_date',$to_date)->with('er_msg',$er_msg)->with('calceled_order_all',$calceled_order_all);
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function deal_get_approve_content($delStatus_id)
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $data  = MerchantTransactions::get_approve_content($delStatus_id);
           //echo '<pre>';print_r($data);die;
            echo '<b>Cancel Note:</b><br>'.strip_tags($data[0]->cancel_notes);
            echo '<br><b>Cancel Applied On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->cancel_date));
            if($data[0]->cancel_status==1)
            {
                 if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE')!= '') 
                {
                    $note_label = trans(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE');
                }
                else 
                {
                    $note_label =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVE_DISAPPROVED_NOTE');
                }
            echo '
                <form action="'.url('').'/deal_approve_cancel_request" method="post">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input name="cod_order_id" type="hidden" value="'.$data[0]->cod_order_id.'"/>
                    <input name="order_cust_id" type="hidden" value="'.$data[0]->order_cust_id.'"/>
                    <input name="date" type="hidden" value="'.$data[0]->cancel_date.'"/>
                    <input name="prod_id" type="hidden" value="'.$data[0]->prod_id.'"/>
                    <input name="transaction_id" type="hidden" value="'.$data[0]->transaction_id.'"/>
                    <input type="hidden" name="delStatus_id" value="'.$delStatus_id.'"/>
                    <input type="hidden" name="get_subtotal" value="'.$data[0]->transaction_id.'"/>
                    <div class="col-sm-12">
                        <label>'.$note_label.'</label>
                        <textarea class="form-control" required name="approve_or_disapprove_note"></textarea>
                    </div><br>
                    <div class="col-sm-12 ">
                        <input type="submit" class="btn btn-success" name="approve" value="Approve cancel request">
                        <input type="submit" class="btn btn-danger" name="disapprove" value="Dis-Approve cancel request">
                    </div>
                </form>';
            }
            else{
                $status ='Order Delivered';
                if($data[0]->cancel_status==2)
                        $status ='Cancelled';
                if($data[0]->cancel_status==4)
                        $status ='Disapproved';
                echo '<br><b>Cancel Status:</b><br>'.$status;
                echo '<br><b>Cancel Processed On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->cancel_approved_date));
            }
            //echo '<pre>';print_r($data);die; 
            //echo $cod_id; 
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function deal_approve_cancel_request()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            /*print_r($_POST);die;*/
            $cod_order_id=$_POST["cod_order_id"];
            $order_detail=DB::table('nm_ordercod')->select('cod_amt','cod_taxAmt')->where("cod_id","=",$cod_order_id)->get();
            $delStatus_id=$_POST["delStatus_id"];

            $cust_id=$_POST["order_cust_id"];
            $customer_det=DB::table('nm_customer')->select('cus_name','cus_email')->where("cus_id","=",$cust_id)->get();
            $customer_name=$customer_det[0]->cus_name;
            $customer_email=$customer_det[0]->cus_email;

            $date=$_POST["date"];
            $prod_id=$_POST["prod_id"];
            $prod_title=DB::table('nm_deals')->select('deal_title as pro_title')->where("deal_id","=",$prod_id)->get();
            $prod_title=$prod_title[0]->pro_title;
            $transaction_id=$_POST["transaction_id"];
            $get_subtotal=$order_detail[0]->cod_amt+$order_detail[0]->cod_taxAmt;
            
            $mer_id=Session::get('merchantid');
            $merchant_det=DB::table('nm_merchant')->select('mer_email')->where("mer_id","=",$mer_id)->get();
            $merchant_email=$merchant_det[0]->mer_email;

            $admin_id="1";
            $admin_det=DB::table('nm_admin')->select('adm_email')->where("adm_id","=",$admin_id)->get();
            $admin_email=$admin_det[0]->adm_email;

            $array = array('email_id'=>"sathyaseelan@pofitec.com");
            $approve_or_disapprove_note=$_POST["approve_or_disapprove_note"];
            $this->setLanguageLocaleFront();
            if(isset($_POST["approve"]))
            {
                 MerchantTransactions::approve_cancel($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id);
                 /* Sending mail to following merchanta customer and admin */

                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail', array(   
                    'status' => 'Success',
                    'approve_status' => 'Approved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$customer_email)
                    {
                        $message->to($customer_email)->subject("Cancel Request Approved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_merchant', array(    
                    'status' => 'Success',
                    'approve_status' => 'Approved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$merchant_email)
                    {
                        $message->to($merchant_email)->subject("Cancel Request Approved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_admin', array(    
                    'status' => 'Success',
                    'approve_status' => 'Approved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$admin_email)
                    {
                        $message->to($admin_email)->subject("Cancel Request Approved");
                    });
            }
            if(isset($_POST["disapprove"]))
            {
                 MerchantTransactions::disapprove_cancel($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id);
                 /* Sending mail to following merchanta customer and admin */

                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail', array(   
                    'status' => 'Success',
                    'approve_status' => 'DisApproved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$customer_email)
                    {
                        $message->to($customer_email)->subject("Cancel Request DisApproved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_merchant', array(    
                    'status' => 'Success',
                    'approve_status' => 'DisApproved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$merchant_email)
                    {
                        $message->to($merchant_email)->subject("Cancel Request DisApproved");
                    });
                    Mail::send('emails.cancelOrderApproveOrDisapprove_mail_to_admin', array(    
                    'status' => 'Success',
                    'approve_status' => 'DisApproved',
                    'transaction_id' => $transaction_id,
                    'prod_title' => $prod_title,
                    'date' => $date,
                    'customer_name' => $customer_name,
                    'cancel_notes' => strip_tags($approve_or_disapprove_note),
                    'Sub_total' =>  $get_subtotal), 
                    function($message) use($array,$admin_email)
                    {
                        $message->to($admin_email)->subject("Cancel Request DisApproved");
                    });
            }
           return Redirect::to('mer_dealcod_cancel_orders');
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }

    public function mer_dealcod_return_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
             $merid             = Session::get('merchantid');
            $payment_type=0;
            $order_type=2;
            $calceled_order  = MerchantTransactions::get_return_order($merid,$payment_type,$order_type,$from_date,$to_date);

            $returned_order  = MerchantTransactions::returned_dealorder_allcod($from_date,$to_date,$merid);





            $er_msg ='';
            if(Session::has('er_msg') )
                 $er_msg = Session::get('er_msg');
            return view('sitemerchant.dealcod_return_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("calceled_order", $calceled_order)->with('from_date',$from_date)->with('to_date',$to_date)->with("er_msg",$er_msg)->with("returned_order",$returned_order);
           ;
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }

    public function deal_get_approve_Returncontent($delStatus_id)
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $data  = MerchantTransactions::get_approve_content($delStatus_id);

            echo '<b>Return Note:</b><br>'.strip_tags($data[0]->return_notes);
            echo '<br><b>Return Applied On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->return_date));
            if($data[0]->return_status==1)
            {
                 if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE')!= '') 
                {
                    $note_label = trans(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE');
                }
                else 
                {
                    $note_label =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVE_DISAPPROVED_NOTE');
                }
            echo '
                <form action="'.url('').'/deal_approve_return_request" method="post">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input name="cod_order_id" type="hidden" value="'.$data[0]->cod_order_id.'"/>
                    <input name="order_cust_id" type="hidden" value="'.$data[0]->order_cust_id.'"/>
                    <input type="hidden" name="delStatus_id" value="'.$delStatus_id.'"/>
                    <div class="col-sm-12">
                        <label>'.$note_label.'</label>
                        <textarea class="form-control" required name="approve_or_disapprove_note"></textarea>
                    </div><br>
                    <div class="col-sm-12 ">
                        <input type="submit" class="btn btn-success" name="approve" value="Approve return request">
                        <input type="submit" class="btn btn-danger" name="disapprove" value="Dis-Approve return request">
                    </div>
                </form>';
            }
            else{
                $status ='Order Delivered';
                if($data[0]->return_status==2)
                        $status ='Returned';
                if($data[0]->return_status==4)
                        $status ='Disapproved';

                echo '<br><b>Return Status:</b><br>'.$status;    
                echo '<br><b>Return Processed On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->return_approved_date));
            }
             
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }

    public function deal_approve_return_request()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $cod_order_id=$_POST["cod_order_id"];
            $delStatus_id=$_POST["delStatus_id"];
            $cust_id=$_POST["order_cust_id"];
            $mer_id=Session::get('merchantid');
            $admin_id="1";
            $return_Status ="";   
            $approve_or_disapprove_note=$_POST["approve_or_disapprove_note"];
            if(isset($_POST["approve"]))
            {
                 MerchantTransactions::approve_return($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');
                 //add return amount to wallet
                 MerchantTransactions::pay_to_wallet_cod($cod_order_id,$cust_id);
                  $return_Status ="Approved"; 
                  if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY')!= '') 
                {
                    $er_msg = trans(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY');
                }
                else 
                {
                    $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVED_SUCCESSFULLY');
                }  
                  Session::flash('er_msg',$er_msg);  
            }
            if(isset($_POST["disapprove"]))
            {
                 MerchantTransactions::disapprove_return($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');
                $return_Status ="Disapproved";
                if(Lang::has(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY')!= '') 
                {
                    $er_msg = trans(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY');
                }
                else 
                {
                    $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DISAPPROVED_SUCCESSFULLY');
                }
                Session::flash('er_msg',$er_msg);     
            }
             if(Lang::has(Session::get('mer_lang_file').'.MER_RETURN_APPROVE_DISAPPROVED_SUBJECT')!= '') 
                {
                    $return_mail_subj = trans(Session::get('mer_lang_file').'.MER_RETURN_APPROVE_DISAPPROVED_SUBJECT');
                }
                else 
                {
                    $return_mail_subj =  trans($this->MER_OUR_LANGUAGE.'.MER_RETURN_APPROVE_DISAPPROVED_SUBJECT');
                }

            /* mail starts */
                 if($delStatus_id !=''){
                    $deliveryDetails =  DB::table("nm_order_delivery_status AS od")->leftjoin("delivery_status_chat As dc","dc.delStatus_id","=","od.delStatus_id")->where("dc.delStatus_id","=",$delStatus_id)->where("dc.send_by","=",'2')->first(); 
                   // print_r($deliveryDetails) ;exit();
                        $prod_title = '';
                        $prod_title=DB::table('nm_product')->select('pro_title')->where("pro_id","=",$deliveryDetails->prod_id)->get();
                        $prod_title=$prod_title[0]->pro_title;


                        include('SMTP/sendmail.php');
                        //Customer mail
                        $custom_name = $customer_mail =''; 
                        $custom_data = DB::table('nm_customer')->select('cus_name','cus_email')->where('cus_id','=',$deliveryDetails->order_cust_id)->first();
                        if(count($custom_data)>0){
                        $custom_name = $custom_data->cus_name;
                        $customer_mail = $custom_data->cus_email;
                        }
                        $this->setLanguageLocaleFront();
                        if($customer_mail!='')
                        {

                            Mail::send('emails.returnOrderApproveOrDisapprove_mail', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->return_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'date'              =>  $deliveryDetails->return_date,
                                'approve_status'    =>  $return_Status,
                                'approved_date'     =>  $deliveryDetails->return_approved_date
                                ), function($message) use ($customer_mail,$return_mail_subj)
                            {
                                $message->to($customer_mail)->subject($return_mail_subj);
                            });     
                        }    
                        //merchant mail
                        $get_mer_email  = Home::get_mer_email($deliveryDetails->mer_id);
                        $mer_email      = $get_mer_email[0]->mer_email;
                        if($mer_email!=''){
                            Mail::send('emails.returnOrderApproveOrDisapprove_mail_to_merchant', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->return_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'approve_status'    =>  $return_Status,
                                'date'              =>  $deliveryDetails->return_date,
                                'approved_date'     =>  $deliveryDetails->return_approved_date
                                ), function($message) use ($mer_email,$return_mail_subj)
                            {
                                $message->to($mer_email)->subject($return_mail_subj);
                            }); 
                        }
                        //Admin mail
                        $adminData = DB::table('nm_admin')->where('adm_id', '=', '1')->first();
                        if(count($adminData)>0){
                            $adminEmail = $adminData->adm_email;
                            if($adminEmail!='')
                            {
                                Mail::send('emails.returnOrderApproveOrDisapprove_mail_to_admin', array(
                                    'delStatus_id'      =>  $delStatus_id,
                                    'transaction_id'    =>  $deliveryDetails->transaction_id,
                                    'prod_id'           =>  $deliveryDetails->prod_id,
                                    'prod_title'        =>  $prod_title,
                                    'customer_name'     =>  $custom_name,
                                    'return_notes'      =>  strip_tags($deliveryDetails->return_notes),
                                    'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                    'approve_status'    =>  $return_Status,
                                    'date'              =>  $deliveryDetails->return_date,
                                    'approved_date'     =>  $deliveryDetails->return_approved_date
                                ), function($message) use ($adminEmail,$return_mail_subj)
                                {
                                    $message->to($adminEmail)->subject($return_mail_subj);
                                }); 
                            }
                        }

                    }
                 /* mail ends */ 


           return Redirect::to('mer_dealcod_return_orders');
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }


    public function mer_dealcod_replacement_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
             $merid             = Session::get('merchantid');
            $payment_type=0;
            $order_type=2;
            $calceled_order  = MerchantTransactions::get_replace_order($merid,$payment_type,$order_type,$from_date,$to_date);
           $replace_order  = MerchantTransactions::replace_dealorder_allcod($from_date,$to_date,$merid); 
            $er_msg = '';
            if(Session::has("er_msg"))
                $er_msg = Session::get("er_msg");
            return view('sitemerchant.dealcod_replace_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("calceled_order", $calceled_order)->with('from_date',$from_date)->with('to_date',$to_date)->with('er_msg',$er_msg)->with("replace_order",$replace_order);
           ;
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }

    public function deal_get_approve_replacecontent($delStatus_id)
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $data  = MerchantTransactions::get_approve_content($delStatus_id);
            //print_r($data);
            echo '<b>Replace Note:</b><br>'.strip_tags($data[0]->replace_notes);
            echo '<br><b>Replace Applied On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->replace_date));
            if($data[0]->replace_status==1)
            {
                 if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE')!= '') 
                    {
                        $note_label = trans(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE');
                    }
                    else 
                    {
                        $note_label =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVE_DISAPPROVED_NOTE');
                    }
            echo '
                <form action="'.url('').'/deal_approve_replace_request" method="post">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input name="cod_order_id" type="hidden" value="'.$data[0]->cod_order_id.'"/>
                    <input name="order_cust_id" type="hidden" value="'.$data[0]->order_cust_id.'"/>
                    <input type="hidden" name="delStatus_id" value="'.$delStatus_id.'"/>
                    <div class="col-sm-12">
                        <label>'.$note_label.'</label>
                        <textarea class="form-control" required name="approve_or_disapprove_note"></textarea>
                    </div><br>
                    <div class="col-sm-12 ">
                        <input type="submit" class="btn btn-success" name="approve" value="Approve replace request">
                        <input type="submit" class="btn btn-danger" name="disapprove" value="Dis-Approve replace request">
                    </div>
                </form>';
            }
            else{
                $status ='Order Delivered';
                if($data[0]->replace_status==2)
                        $status ='Replaced';
                 if($data[0]->replace_status==4)
                        $status ='Disapproved';

                echo '<br><b>Replace Status:</b><br>'.$status;
                echo '<br><b>Replace Processed On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->replace_approved_date));
            }
             
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }

    public function deal_approve_replace_request()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $cod_order_id=$_POST["cod_order_id"];
            $delStatus_id=$_POST["delStatus_id"];
            $cust_id=$_POST["order_cust_id"];
            $mer_id=Session::get('merchantid');
            $admin_id="1";
            $approve_or_disapprove_note=$_POST["approve_or_disapprove_note"];
            $return_Status =" "; 

            //check product qunatity to replace
            $prod_info =DB::table('nm_deals as prod')->join("nm_order as ord","ord.order_pro_id",'=',"prod.deal_id")->join("nm_order_delivery_status AS od","od.prod_id",'=',"prod.deal_id")->select('prod.deal_id as pro_id','prod.deal_no_of_purchase as pro_no_of_purchase','prod.deal_max_limit as pro_qty','ord.order_qty as cod_qty')->where("od.delStatus_id","=",$delStatus_id)->where("ord.order_id","=",$cod_order_id)->first();
           // echo $_POST["disapprove"];
            //print_r($prod_info);exit();
            if(count($prod_info)>0){
                if(isset($_POST["approve"]))
                {
                    $available_qty = $prod_info->pro_qty - $prod_info->pro_no_of_purchase;
                    if($available_qty>$prod_info->cod_qty){
                        $new_purchased = $prod_info->pro_no_of_purchase+$prod_info->cod_qty;
                         MerchantTransactions::approve_replace($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');  
                         $return_Status ="Approved";   
                          //Update Product quantity
                        Products::edit_product(array('pro_no_of_purchase' => $new_purchased  ),$prod_info->pro_id); 
                        
                        if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY')!= '') 
                        {
                            $er_msg = trans(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY');
                        }
                        else 
                        {
                            $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVED_SUCCESSFULLY');
                        }
                        Session::flash("er_msg",$er_msg);
                    }else{
                        $er_msg = "quantity not available";
                        Session::flash("er_msg",$er_msg);
                       return redirect("mer_cod_replacement_orders")->with('er_msg',$er_msg);
                    }
                              
                }
                if(isset($_POST["disapprove"]))
                {
                     MerchantTransactions::disapprove_replace($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');
                     $return_Status ="Disapproved"; 
                     if(Lang::has(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY')!= '') 
                    {
                        $er_msg = trans(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY');
                    }
                    else 
                    {
                        $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DISAPPROVED_SUCCESSFULLY');
                    }
                     
                    Session::flash("er_msg",$er_msg);
                    
                }


             /* mail starts */
                 if($delStatus_id !=''){
                    $deliveryDetails =  DB::table("nm_order_delivery_status AS od")->leftjoin("delivery_status_chat As dc","dc.delStatus_id","=","od.delStatus_id")->where("dc.delStatus_id","=",$delStatus_id)->where("dc.send_by","=",'2')->first(); 
                   // print_r($deliveryDetails) ;exit();
                        $prod_title = '';
                        $prod_title=DB::table('nm_product')->select('pro_title')->where("pro_id","=",$deliveryDetails->prod_id)->get();
                        $prod_title=$prod_title[0]->pro_title;


                        include('SMTP/sendmail.php');
                        //Customer mail
                        $custom_name = $customer_mail =''; 
                        $custom_data = DB::table('nm_customer')->select('cus_name','cus_email')->where('cus_id','=',$deliveryDetails->order_cust_id)->first();
                        if(count($custom_data)>0){
                        $custom_name = $custom_data->cus_name;
                        $customer_mail = $custom_data->cus_email;
                        }
                        if($customer_mail!='')
                        {
                            $this->setLanguageLocaleFront();

                            Mail::send('emails.replaceOrderApproveOrDisapprove_mail', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->replace_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'date'              =>  $deliveryDetails->replace_date,
                                'approve_status'    =>  $return_Status,
                                'approved_date'     =>  $deliveryDetails->replace_approved_date
                                ), function($message) use ($customer_mail)
                            {
                                $message->to($customer_mail)->subject('Order replace Approve/Disapprove Notification');
                            });     
                        }    
                        //merchant mail
                        $get_mer_email  = Home::get_mer_email($deliveryDetails->mer_id);
                        $mer_email      = $get_mer_email[0]->mer_email;
                        if($mer_email!=''){
                            Mail::send('emails.replaceOrderApproveOrDisapprove_mail_to_merchant', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->replace_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'date'              =>  $deliveryDetails->replace_date,
                                'approve_status'    =>  $return_Status,
                                'approved_date'     =>  $deliveryDetails->replace_approved_date
                                ), function($message) use ($mer_email)
                            {
                                $message->to($mer_email)->subject('Order replace Approve/Disapprove Notification');
                            }); 
                        }
                        //Admin mail
                        $adminData = DB::table('nm_admin')->where('adm_id', '=', '1')->first();
                        if(count($adminData)>0){
                            $adminEmail = $adminData->adm_email;
                            if($adminEmail!='')
                            {
                                Mail::send('emails.replaceOrderApproveOrDisapprove_mail_to_admin', array(
                                    'delStatus_id'      =>  $delStatus_id,
                                    'transaction_id'    =>  $deliveryDetails->transaction_id,
                                    'prod_id'           =>  $deliveryDetails->prod_id,
                                    'prod_title'        =>  $prod_title,
                                    'customer_name'     =>  $custom_name,
                                    'return_notes'      =>  strip_tags($deliveryDetails->replace_notes),
                                    'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                    'date'              =>  $deliveryDetails->replace_date,
                                    'approve_status'    =>  $return_Status,
                                    'approved_date'     =>  $deliveryDetails->replace_approved_date
                                ), function($message) use ($adminEmail)
                                {
                                    $message->to($adminEmail)->subject('Order replace Approve/Disapprove Notification');
                                }); 
                            }
                        }

                    }
                 /* mail ends */ 
     
            }else{
                $er_msg = "Product Not available";
                Session::flash("er_msg",$er_msg);
                return redirect("mer_dealcod_replacement_orders");
            }     

            return redirect("mer_dealcod_replacement_orders");
            
        } else {

            return Redirect::to('sitemerchant');
        }
        
    }


    /* deal cod  end */  

    /* paypal REturn order starts */
    public function mer_return_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
             $merid             = Session::get('merchantid');
            $payment_type=1;
            $order_type=1;
            $calceled_order  = MerchantTransactions::get_return_order_paypal($merid,$payment_type,$order_type,$from_date,$to_date);
            $returned_order_all  = MerchantTransactions::returned_order_allpaypal($from_date,$to_date,$merid);
            $er_msg = '';
            if(Session::has('er_msg') )
                 $er_msg = Session::get('er_msg');
            return view('sitemerchant.product_return_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("calceled_order", $calceled_order)->with('from_date',$from_date)->with('to_date',$to_date)->with('er_msg',$er_msg)->with('returned_order_all',$returned_order_all);
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function mer_payu_return_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
             $merid             = Session::get('merchantid');
            $payment_type=1;
            $order_type=1;
            $calceled_order  = MerchantTransactions::get_return_order_payu($merid,$payment_type,$order_type,$from_date,$to_date);
            $returned_order_all  = MerchantTransactions::returned_order_allpayu($from_date,$to_date,$merid);
            $er_msg = '';
            if(Session::has('er_msg') )
                 $er_msg = Session::get('er_msg');
            return view('sitemerchant.payu_product_return_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("calceled_order", $calceled_order)->with('from_date',$from_date)->with('to_date',$to_date)->with('er_msg',$er_msg)->with('returned_order_all',$returned_order_all);
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }

    public function get_approve_Returncontent_paypal($delStatus_id)
    {

       
        if (Session::get('merchantid')) {
          

            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $data  = MerchantTransactions::get_approve_content($delStatus_id);

            if(Lang::has(Session::get('admin_lang_file').'.BACK_APPROVE_DISAPPROVED_NOTE')!= '') 
                    {
                        $note_label = trans(Session::get('admin_lang_file').'.BACK_APPROVE_DISAPPROVED_NOTE');
                    }
                    else 
                    {
                        $note_label =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_APPROVE_DISAPPROVED_NOTE');
                    }
 

 
            echo '<b>Return Note:</b><br>'.strip_tags($data[0]->return_notes);
            echo '<br><b>Return Applied On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->return_date));

            if($data[0]->return_status==1)
            {

            echo '
                <form action="'.url('').'/approve_return_request_paypal" method="post">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input name="cod_order_id" type="hidden" value="'.$data[0]->order_id.'"/>
                    <input name="order_cust_id" type="hidden" value="'.$data[0]->order_cust_id.'"/>
                    <input type="hidden" name="delStatus_id" value="'.$delStatus_id.'"/>
                    <div class="col-sm-12">
                        <label>'.$note_label.'</label>
                        <textarea class="form-control" required name="approve_or_disapprove_note"></textarea>
                    </div><br>
                    <div class="col-sm-12 ">
                        <input type="submit" class="btn btn-success" name="approve" value="Approve return request">
                        <input type="submit" class="btn btn-danger" name="disapprove" value="Dis-Approve return request">
                    </div>
                </form>';

               

            }
            else{
                $status ='Order Delivered';
                if($data[0]->replace_status==2)
                        $status ='Returned';
                if($data[0]->replace_status==4)
                        $status ='Disapproved';

                echo '<br><b>Return Status:</b><br>'.$status;    
                echo '<br><b>Return Processed On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->return_approved_date));
            }
             
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
 public function get_approve_Returncontent_payu($delStatus_id)
    {

       
        if (Session::get('merchantid')) {
          

            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $data  = MerchantTransactions::get_approve_content($delStatus_id);

            if(Lang::has(Session::get('admin_lang_file').'.BACK_APPROVE_DISAPPROVED_NOTE')!= '') 
                    {
                        $note_label = trans(Session::get('admin_lang_file').'.BACK_APPROVE_DISAPPROVED_NOTE');
                    }
                    else 
                    {
                        $note_label =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_APPROVE_DISAPPROVED_NOTE');
                    }
 

 
            echo '<b>Return Note:</b><br>'.strip_tags($data[0]->return_notes);
            echo '<br><b>Return Applied On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->return_date));

            if($data[0]->return_status==1)
            {

            echo '
                <form action="'.url('').'/approve_return_request_payu" method="post">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input name="cod_order_id" type="hidden" value="'.$data[0]->order_id.'"/>
                    <input name="order_cust_id" type="hidden" value="'.$data[0]->order_cust_id.'"/>
                    <input type="hidden" name="delStatus_id" value="'.$delStatus_id.'"/>
                    <div class="col-sm-12">
                        <label>'.$note_label.'</label>
                        <textarea class="form-control" required name="approve_or_disapprove_note"></textarea>
                    </div><br>
                    <div class="col-sm-12 ">
                        <input type="submit" class="btn btn-success" name="approve" value="Approve return request">
                        <input type="submit" class="btn btn-danger" name="disapprove" value="Dis-Approve return request">
                    </div>
                </form>';

               

            }
            else{
                $status ='Order Delivered';
                if($data[0]->replace_status==2)
                        $status ='Returned';
                if($data[0]->replace_status==4)
                        $status ='Disapproved';

                echo '<br><b>Return Status:</b><br>'.$status;    
                echo '<br><b>Return Processed On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->return_approved_date));
            }
             
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function approve_return_request_paypal()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $cod_order_id=$_POST["cod_order_id"];
            $delStatus_id=$_POST["delStatus_id"];
            $cust_id=$_POST["order_cust_id"];
            $mer_id=Session::get('merchantid');
            $admin_id="1";
            $return_Status ="";   
            $approve_or_disapprove_note=$_POST["approve_or_disapprove_note"];
            if(isset($_POST["approve"]))
            {
                 MerchantTransactions::approve_return_paypal($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');

                 //add return amount to wallet
                 MerchantTransactions::pay_to_wallet($cod_order_id,$cust_id);

                  $return_Status ="Approved"; 
                  if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY')!= '') 
                {
                    $er_msg = trans(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY');
                }
                else 
                {
                    $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVED_SUCCESSFULLY');
                } 
                  Session::flash('er_msg',$er_msg);   
            }
            if(isset($_POST["disapprove"]))
            {
                 MerchantTransactions::disapprove_return_paypal($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');
                $return_Status ="Disapproved"; 
                if(Lang::has(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY')!= '') 
                {
                    $er_msg = trans(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY');
                }
                else 
                {
                    $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DISAPPROVED_SUCCESSFULLY');
                }
                Session::flash('er_msg',$er_msg);  
            }

            /* mail starts */
                 if(Lang::has(Session::get('mer_lang_file').'.MER_RETURN_APPROVE_DISAPPROVED_SUBJECT')!= '') 
                {
                    $return_mail_subj = trans(Session::get('mer_lang_file').'.MER_RETURN_APPROVE_DISAPPROVED_SUBJECT');
                }
                else 
                {
                    $return_mail_subj =  trans($this->MER_OUR_LANGUAGE.'.MER_RETURN_APPROVE_DISAPPROVED_SUBJECT');
                }
                 if($delStatus_id !=''){
                    $deliveryDetails =  DB::table("nm_order_delivery_status AS od")->leftjoin("delivery_status_chat As dc","dc.delStatus_id","=","od.delStatus_id")->where("dc.delStatus_id","=",$delStatus_id)->where("dc.send_by","=",'2')->first(); 
                   // print_r($deliveryDetails) ;exit();
                        $prod_title = '';
                        $prod_title=DB::table('nm_product')->select('pro_title')->where("pro_id","=",$deliveryDetails->prod_id)->get();
                        $prod_title=$prod_title[0]->pro_title;


                        include('SMTP/sendmail.php');
                        //Customer mail
                        $custom_name = $customer_mail =''; 
                        $custom_data = DB::table('nm_customer')->select('cus_name','cus_email')->where('cus_id','=',$deliveryDetails->order_cust_id)->first();
                        if(count($custom_data)>0){
                        $custom_name = $custom_data->cus_name;
                        $customer_mail = $custom_data->cus_email;
                        }
                        if($customer_mail!='')
                        {
$this->setLanguageLocaleFront();
                            Mail::send('emails.returnOrderApproveOrDisapprove_mail', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->return_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'date'              =>  $deliveryDetails->return_date,
                                'approve_status'    =>  $return_Status,
                                'approved_date'     =>  $deliveryDetails->return_approved_date
                                ), function($message) use ($customer_mail,$return_mail_subj)
                            {
                                $message->to($customer_mail)->subject($return_mail_subj);
                            });     
                        }    
                        //merchant mail
                        $get_mer_email  = Home::get_mer_email($deliveryDetails->mer_id);
                        $mer_email      = $get_mer_email[0]->mer_email;
                        if($mer_email!=''){
                            Mail::send('emails.returnOrderApproveOrDisapprove_mail_to_merchant', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->return_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'approve_status'    =>  $return_Status,
                                'date'              =>  $deliveryDetails->return_date,
                                'approved_date'     =>  $deliveryDetails->return_approved_date
                                ), function($message) use ($mer_email,$return_mail_subj)
                            {
                                $message->to($mer_email)->subject($return_mail_subj);
                            }); 
                        }
                        //Admin mail
                        $adminData = DB::table('nm_admin')->where('adm_id', '=', '1')->first();
                        if(count($adminData)>0){
                            $adminEmail = $adminData->adm_email;
                            if($adminEmail!='')
                            {
                                Mail::send('emails.returnOrderApproveOrDisapprove_mail_to_admin', array(
                                    'delStatus_id'      =>  $delStatus_id,
                                    'transaction_id'    =>  $deliveryDetails->transaction_id,
                                    'prod_id'           =>  $deliveryDetails->prod_id,
                                    'prod_title'        =>  $prod_title,
                                    'customer_name'     =>  $custom_name,
                                    'return_notes'      =>  strip_tags($deliveryDetails->return_notes),
                                    'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                    'approve_status'    =>  $return_Status,
                                    'date'              =>  $deliveryDetails->return_date,
                                    'approved_date'     =>  $deliveryDetails->return_approved_date
                                ), function($message) use ($adminEmail,$return_mail_subj)
                                {
                                    $message->to($adminEmail)->subject($return_mail_subj);
                                }); 
                            }
                        }

                    }
                 /* mail ends */ 


           return Redirect::to('mer_return_orders');
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
public function approve_return_request_payu()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $cod_order_id=$_POST["cod_order_id"];
            $delStatus_id=$_POST["delStatus_id"];
            $cust_id=$_POST["order_cust_id"];
            $mer_id=Session::get('merchantid');
            $admin_id="1";
            $return_Status ="";   
            $approve_or_disapprove_note=$_POST["approve_or_disapprove_note"];
            if(isset($_POST["approve"]))
            {
                 MerchantTransactions::approve_return_payu($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');

                 //add return amount to wallet
                 MerchantTransactions::pay_to_wallet_payu($cod_order_id,$cust_id);

                  $return_Status ="Approved"; 
                  if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY')!= '') 
                {
                    $er_msg = trans(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY');
                }
                else 
                {
                    $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVED_SUCCESSFULLY');
                } 
                  Session::flash('er_msg',$er_msg);   
            }
            if(isset($_POST["disapprove"]))
            {
                 MerchantTransactions::disapprove_return_payu($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');
                $return_Status ="Disapproved"; 
                if(Lang::has(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY')!= '') 
                {
                    $er_msg = trans(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY');
                }
                else 
                {
                    $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DISAPPROVED_SUCCESSFULLY');
                }
                Session::flash('er_msg',$er_msg);  
            }

            /* mail starts */
                 if(Lang::has(Session::get('mer_lang_file').'.MER_RETURN_APPROVE_DISAPPROVED_SUBJECT')!= '') 
                {
                    $return_mail_subj = trans(Session::get('mer_lang_file').'.MER_RETURN_APPROVE_DISAPPROVED_SUBJECT');
                }
                else 
                {
                    $return_mail_subj =  trans($this->MER_OUR_LANGUAGE.'.MER_RETURN_APPROVE_DISAPPROVED_SUBJECT');
                }
                 if($delStatus_id !=''){
                    $deliveryDetails =  DB::table("nm_order_delivery_status AS od")->leftjoin("delivery_status_chat As dc","dc.delStatus_id","=","od.delStatus_id")->where("dc.delStatus_id","=",$delStatus_id)->where("dc.send_by","=",'2')->first(); 
                   // print_r($deliveryDetails) ;exit();
                        $prod_title = '';
                        $prod_title=DB::table('nm_product')->select('pro_title')->where("pro_id","=",$deliveryDetails->prod_id)->get();
                        $prod_title=$prod_title[0]->pro_title;


                        include('SMTP/sendmail.php');
                        //Customer mail
                        $custom_name = $customer_mail =''; 
                        $custom_data = DB::table('nm_customer')->select('cus_name','cus_email')->where('cus_id','=',$deliveryDetails->order_cust_id)->first();
                        if(count($custom_data)>0){
                        $custom_name = $custom_data->cus_name;
                        $customer_mail = $custom_data->cus_email;
                        }
                        if($customer_mail!='')
                        {
$this->setLanguageLocaleFront();
                            Mail::send('emails.returnOrderApproveOrDisapprove_mail', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->return_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'date'              =>  $deliveryDetails->return_date,
                                'approve_status'    =>  $return_Status,
                                'approved_date'     =>  $deliveryDetails->return_approved_date
                                ), function($message) use ($customer_mail,$return_mail_subj)
                            {
                                $message->to($customer_mail)->subject($return_mail_subj);
                            });     
                        }    
                        //merchant mail
                        $get_mer_email  = Home::get_mer_email($deliveryDetails->mer_id);
                        $mer_email      = $get_mer_email[0]->mer_email;
                        if($mer_email!=''){
                            Mail::send('emails.returnOrderApproveOrDisapprove_mail_to_merchant', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->return_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'approve_status'    =>  $return_Status,
                                'date'              =>  $deliveryDetails->return_date,
                                'approved_date'     =>  $deliveryDetails->return_approved_date
                                ), function($message) use ($mer_email,$return_mail_subj)
                            {
                                $message->to($mer_email)->subject($return_mail_subj);
                            }); 
                        }
                        //Admin mail
                        $adminData = DB::table('nm_admin')->where('adm_id', '=', '1')->first();
                        if(count($adminData)>0){
                            $adminEmail = $adminData->adm_email;
                            if($adminEmail!='')
                            {
                                Mail::send('emails.returnOrderApproveOrDisapprove_mail_to_admin', array(
                                    'delStatus_id'      =>  $delStatus_id,
                                    'transaction_id'    =>  $deliveryDetails->transaction_id,
                                    'prod_id'           =>  $deliveryDetails->prod_id,
                                    'prod_title'        =>  $prod_title,
                                    'customer_name'     =>  $custom_name,
                                    'return_notes'      =>  strip_tags($deliveryDetails->return_notes),
                                    'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                    'approve_status'    =>  $return_Status,
                                    'date'              =>  $deliveryDetails->return_date,
                                    'approved_date'     =>  $deliveryDetails->return_approved_date
                                ), function($message) use ($adminEmail,$return_mail_subj)
                                {
                                    $message->to($adminEmail)->subject($return_mail_subj);
                                }); 
                            }
                        }

                    }
                 /* mail ends */ 


           return Redirect::to('mer_return_orders');
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    /* REturn order ends */


    /* paypal deal REturn order starts */
    public function mer_deal_return_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
             $merid             = Session::get('merchantid');
            $payment_type=1;
            $order_type=2;
            $calceled_order  = MerchantTransactions::get_return_order_paypal($merid,$payment_type,$order_type,$from_date,$to_date);
            $return_order_all  = Transactions::returned_dealorder_allpaypal($from_date,$to_date); 
            //print_r($calceled_order);exit();
            $er_msg = '';
            if(Session::has('er_msg') )
                 $er_msg = Session::get('er_msg');
            return view('sitemerchant.deal_return_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("calceled_order", $calceled_order)->with('from_date',$from_date)->with('to_date',$to_date)->with('er_msg',$er_msg)->with('return_order_all',$return_order_all);
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
     public function mer_payu_deal_return_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
             $merid             = Session::get('merchantid');
            $payment_type=1;
            $order_type=2;
            $calceled_order  = MerchantTransactions::get_return_order_payu($merid,$payment_type,$order_type,$from_date,$to_date);
            $return_order_all  = Transactions::returned_dealorder_allpayumoney($from_date,$to_date); 
            //print_r($calceled_order);exit();
            $er_msg = '';
            if(Session::has('er_msg') )
                 $er_msg = Session::get('er_msg');
            return view('sitemerchant.payu_deal_return_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("calceled_order", $calceled_order)->with('from_date',$from_date)->with('to_date',$to_date)->with('er_msg',$er_msg)->with('return_order_all',$return_order_all);
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }

    public function deal_get_approve_Returncontent_paypal($delStatus_id)
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $data  = MerchantTransactions::get_approve_content($delStatus_id);
           // print_r($data);
            echo '<b>Return Note:</b><br>'.strip_tags($data[0]->return_notes);
            echo '<br><b>Return Applied On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->return_date));
            if($data[0]->return_status==1)
            {
                 if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE')!= '') 
                    {
                        $note_label = trans(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE');
                    }
                    else 
                    {
                        $note_label =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVE_DISAPPROVED_NOTE');
                    }
            echo '
                <form action="'.url('').'/deal_approve_return_request_paypal" method="post">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input name="cod_order_id" type="hidden" value="'.$data[0]->order_id.'"/>
                    <input name="order_cust_id" type="hidden" value="'.$data[0]->order_cust_id.'"/>
                    <input type="hidden" name="delStatus_id" value="'.$delStatus_id.'"/>
                    <div class="col-sm-12">
                        <label>'.$note_label.'</label>
                        <textarea class="form-control" required name="approve_or_disapprove_note"></textarea>
                    </div><br>
                    <div class="col-sm-12 ">
                        <input type="submit" class="btn btn-success" name="approve" value="Approve return request">
                        <input type="submit" class="btn btn-danger" name="disapprove" value="Dis-Approve return request">
                    </div>
                </form>';
            }
            else{
                $status ='Order Delivered';
                if($data[0]->return_status==2)
                        $status ='Returned';
                if($data[0]->return_status==4)
                        $status ='Disapproved';

                echo '<br><b>Return Status:</b><br>'.$status;    
                echo '<br><b>Return Processed On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->return_approved_date));
            }
             
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function deal_get_approve_Returncontent_payu($delStatus_id)
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $data  = MerchantTransactions::get_approve_content($delStatus_id);
           // print_r($data);
            echo '<b>Return Note:</b><br>'.strip_tags($data[0]->return_notes);
            echo '<br><b>Return Applied On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->return_date));
            if($data[0]->return_status==1)
            {
                 if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE')!= '') 
                    {
                        $note_label = trans(Session::get('mer_lang_file').'.MER_APPROVE_DISAPPROVED_NOTE');
                    }
                    else 
                    {
                        $note_label =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVE_DISAPPROVED_NOTE');
                    }
            echo '
                <form action="'.url('').'/deal_approve_return_request_payu" method="post">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input name="cod_order_id" type="hidden" value="'.$data[0]->order_id.'"/>
                    <input name="order_cust_id" type="hidden" value="'.$data[0]->order_cust_id.'"/>
                    <input type="hidden" name="delStatus_id" value="'.$delStatus_id.'"/>
                    <div class="col-sm-12">
                        <label>'.$note_label.'</label>
                        <textarea class="form-control" required name="approve_or_disapprove_note"></textarea>
                    </div><br>
                    <div class="col-sm-12 ">
                        <input type="submit" class="btn btn-success" name="approve" value="Approve return request">
                        <input type="submit" class="btn btn-danger" name="disapprove" value="Dis-Approve return request">
                    </div>
                </form>';
            }
            else{
                $status ='Order Delivered';
                if($data[0]->return_status==2)
                        $status ='Returned';
                if($data[0]->return_status==4)
                        $status ='Disapproved';

                echo '<br><b>Return Status:</b><br>'.$status;    
                echo '<br><b>Return Processed On:</b><br>'.date("Y-m-d h:i A",strtotime($data[0]->return_approved_date));
            }
             
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }

    public function  deal_approve_return_request_paypal()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $cod_order_id=$_POST["cod_order_id"];
            $delStatus_id=$_POST["delStatus_id"];
            $cust_id=$_POST["order_cust_id"];
            $mer_id=Session::get('merchantid');
            $admin_id="1";
            $return_Status ="";   
            $approve_or_disapprove_note=$_POST["approve_or_disapprove_note"];
            if(isset($_POST["approve"]))
            {
                 MerchantTransactions::approve_return_paypal($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');

                 //add return amount to wallet
                 MerchantTransactions::pay_to_wallet($cod_order_id,$cust_id);

                  $return_Status ="Approved";
                  if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY')!= '') 
                {
                    $er_msg = trans(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY');
                }
                else 
                {
                    $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVED_SUCCESSFULLY');
                }  
                  Session::flash('er_msg',$er_msg);   
            }
            if(isset($_POST["disapprove"]))
            {
                 MerchantTransactions::disapprove_return_paypal($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');
                $return_Status ="Disapproved";
                if(Lang::has(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY')!= '') 
                {
                    $er_msg = trans(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY');
                }
                else 
                {
                    $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DISAPPROVED_SUCCESSFULLY');
                } 
                Session::flash('er_msg',$er_msg);  
            }
             if(Lang::has(Session::get('mer_lang_file').'.MER_RETURN_APPROVE_DISAPPROVED_SUBJECT')!= '') 
                {
                    $return_mail_subj = trans(Session::get('mer_lang_file').'.MER_RETURN_APPROVE_DISAPPROVED_SUBJECT');
                }
                else 
                {
                    $return_mail_subj =  trans($this->MER_OUR_LANGUAGE.'.MER_RETURN_APPROVE_DISAPPROVED_SUBJECT');
                }

            /* mail starts */
                 if($delStatus_id !=''){
                    $deliveryDetails =  DB::table("nm_order_delivery_status AS od")->leftjoin("delivery_status_chat As dc","dc.delStatus_id","=","od.delStatus_id")->where("dc.delStatus_id","=",$delStatus_id)->where("dc.send_by","=",'2')->first(); 
                   // print_r($deliveryDetails) ;exit();
                        $prod_title = '';
                        $prod_title=DB::table('nm_deals')->select('deal_title')->where("deal_id","=",$deliveryDetails->prod_id)->get();
                        $prod_title=$prod_title[0]->deal_title;


                        include('SMTP/sendmail.php');
                        //Customer mail
                        $custom_name = $customer_mail =''; 
                        $custom_data = DB::table('nm_customer')->select('cus_name','cus_email')->where('cus_id','=',$deliveryDetails->order_cust_id)->first();
                        if(count($custom_data)>0){
                        $custom_name = $custom_data->cus_name;
                        $customer_mail = $custom_data->cus_email;
                        }
                        $this->setLanguageLocaleFront();
                        if($customer_mail!='')
                        {

                            Mail::send('emails.returnOrderApproveOrDisapprove_mail', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->return_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'date'              =>  $deliveryDetails->return_date,
                                'approve_status'    =>  $return_Status,
                                'approved_date'     =>  $deliveryDetails->return_approved_date
                                ), function($message) use ($customer_mail,$return_mail_subj)
                            {
                                $message->to($customer_mail)->subject($return_mail_subj);
                            });     
                        }    
                        //merchant mail
                        $get_mer_email  = Home::get_mer_email($deliveryDetails->mer_id);
                        $mer_email      = $get_mer_email[0]->mer_email;
                        if($mer_email!=''){
                            Mail::send('emails.returnOrderApproveOrDisapprove_mail_to_merchant', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->return_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'approve_status'    =>  $return_Status,
                                'date'              =>  $deliveryDetails->return_date,
                                'approved_date'     =>  $deliveryDetails->return_approved_date
                                ), function($message) use ($mer_email,$return_mail_subj)
                            {
                                $message->to($mer_email)->subject($return_mail_subj);
                            }); 
                        }
                        //Admin mail
                        $adminData = DB::table('nm_admin')->where('adm_id', '=', '1')->first();
                        if(count($adminData)>0){
                            $adminEmail = $adminData->adm_email;
                            if($adminEmail!='')
                            {
                                Mail::send('emails.returnOrderApproveOrDisapprove_mail_to_admin', array(
                                    'delStatus_id'      =>  $delStatus_id,
                                    'transaction_id'    =>  $deliveryDetails->transaction_id,
                                    'prod_id'           =>  $deliveryDetails->prod_id,
                                    'prod_title'        =>  $prod_title,
                                    'customer_name'     =>  $custom_name,
                                    'return_notes'      =>  strip_tags($deliveryDetails->return_notes),
                                    'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                    'approve_status'    =>  $return_Status,
                                    'date'              =>  $deliveryDetails->return_date,
                                    'approved_date'     =>  $deliveryDetails->return_approved_date
                                ), function($message) use ($adminEmail,$return_mail_subj)
                                {
                                    $message->to($adminEmail)->subject($return_mail_subj);
                                }); 
                            }
                        }

                    }
                 /* mail ends */ 


           return Redirect::to('mer_return_orders');
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
 public function  deal_approve_return_request_payu()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $cod_order_id=$_POST["cod_order_id"];
            $delStatus_id=$_POST["delStatus_id"];
            $cust_id=$_POST["order_cust_id"];
            $mer_id=Session::get('merchantid');
            $admin_id="1";
            $return_Status ="";   
            $approve_or_disapprove_note=$_POST["approve_or_disapprove_note"];
            if(isset($_POST["approve"]))
            {
                 MerchantTransactions::approve_return_paypal($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');

                 //add return amount to wallet
                 MerchantTransactions::pay_to_wallet_payu($cod_order_id,$cust_id);

                  $return_Status ="Approved";
                  if(Lang::has(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY')!= '') 
                {
                    $er_msg = trans(Session::get('mer_lang_file').'.MER_APPROVED_SUCCESSFULLY');
                }
                else 
                {
                    $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_APPROVED_SUCCESSFULLY');
                }  
                  Session::flash('er_msg',$er_msg);   
            }
            if(isset($_POST["disapprove"]))
            {
                 MerchantTransactions::disapprove_return_paypal($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id,'2');
                $return_Status ="Disapproved";
                if(Lang::has(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY')!= '') 
                {
                    $er_msg = trans(Session::get('mer_lang_file').'.MER_DISAPPROVED_SUCCESSFULLY');
                }
                else 
                {
                    $er_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DISAPPROVED_SUCCESSFULLY');
                } 
                Session::flash('er_msg',$er_msg);  
            }
             if(Lang::has(Session::get('mer_lang_file').'.MER_RETURN_APPROVE_DISAPPROVED_SUBJECT')!= '') 
                {
                    $return_mail_subj = trans(Session::get('mer_lang_file').'.MER_RETURN_APPROVE_DISAPPROVED_SUBJECT');
                }
                else 
                {
                    $return_mail_subj =  trans($this->MER_OUR_LANGUAGE.'.MER_RETURN_APPROVE_DISAPPROVED_SUBJECT');
                }

            /* mail starts */
                 if($delStatus_id !=''){
                    $deliveryDetails =  DB::table("nm_order_delivery_status AS od")->leftjoin("delivery_status_chat As dc","dc.delStatus_id","=","od.delStatus_id")->where("dc.delStatus_id","=",$delStatus_id)->where("dc.send_by","=",'2')->first(); 
                   // print_r($deliveryDetails) ;exit();
                        $prod_title = '';
                        $prod_title=DB::table('nm_deals')->select('deal_title')->where("deal_id","=",$deliveryDetails->prod_id)->get();
                        $prod_title=$prod_title[0]->deal_title;


                        include('SMTP/sendmail.php');
                        //Customer mail
                        $custom_name = $customer_mail =''; 
                        $custom_data = DB::table('nm_customer')->select('cus_name','cus_email')->where('cus_id','=',$deliveryDetails->order_cust_id)->first();
                        if(count($custom_data)>0){
                        $custom_name = $custom_data->cus_name;
                        $customer_mail = $custom_data->cus_email;
                        }
                        $this->setLanguageLocaleFront();
                        if($customer_mail!='')
                        {

                            Mail::send('emails.returnOrderApproveOrDisapprove_mail', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->return_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'date'              =>  $deliveryDetails->return_date,
                                'approve_status'    =>  $return_Status,
                                'approved_date'     =>  $deliveryDetails->return_approved_date
                                ), function($message) use ($customer_mail,$return_mail_subj)
                            {
                                $message->to($customer_mail)->subject($return_mail_subj);
                            });     
                        }    
                        //merchant mail
                        $get_mer_email  = Home::get_mer_email($deliveryDetails->mer_id);
                        $mer_email      = $get_mer_email[0]->mer_email;
                        if($mer_email!=''){
                            Mail::send('emails.returnOrderApproveOrDisapprove_mail_to_merchant', array(
                                'delStatus_id'      =>  $delStatus_id,
                                'transaction_id'    =>  $deliveryDetails->transaction_id,
                                'prod_id'           =>  $deliveryDetails->prod_id,
                                'prod_title'        =>  $prod_title,
                                'customer_name'     =>  $custom_name,
                                'return_notes'      =>  strip_tags($deliveryDetails->return_notes),
                                'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                'approve_status'    =>  $return_Status,
                                'date'              =>  $deliveryDetails->return_date,
                                'approved_date'     =>  $deliveryDetails->return_approved_date
                                ), function($message) use ($mer_email,$return_mail_subj)
                            {
                                $message->to($mer_email)->subject($return_mail_subj);
                            }); 
                        }
                        //Admin mail
                        $adminData = DB::table('nm_admin')->where('adm_id', '=', '1')->first();
                        if(count($adminData)>0){
                            $adminEmail = $adminData->adm_email;
                            if($adminEmail!='')
                            {
                                Mail::send('emails.returnOrderApproveOrDisapprove_mail_to_admin', array(
                                    'delStatus_id'      =>  $delStatus_id,
                                    'transaction_id'    =>  $deliveryDetails->transaction_id,
                                    'prod_id'           =>  $deliveryDetails->prod_id,
                                    'prod_title'        =>  $prod_title,
                                    'customer_name'     =>  $custom_name,
                                    'return_notes'      =>  strip_tags($deliveryDetails->return_notes),
                                    'approve_disapprove_notes'      =>  strip_tags($deliveryDetails->note),
                                    'approve_status'    =>  $return_Status,
                                    'date'              =>  $deliveryDetails->return_date,
                                    'approved_date'     =>  $deliveryDetails->return_approved_date
                                ), function($message) use ($adminEmail,$return_mail_subj)
                                {
                                    $message->to($adminEmail)->subject($return_mail_subj);
                                }); 
                            }
                        }

                    }
                 /* mail ends */ 


           return Redirect::to('mer_return_orders');
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    /* REturn order ends */
  
    
    public function cod_failed_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
			if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            /*if ($getproductidlist) {
                $productlist           = $getproductidlist[0]->proid;
                
            } elseif(empty($getproductidlist)) {
                $productlist = array();      
                $orderdetails = array();
                $allprod_failedreports = array();       
            } else {
                $orderdetails = array();
            }*/
            $orderdetails          = MerchantTransactions::getcod_failed_orders($merid);
            $allprod_failedreports = MerchantTransactions::allprod_failedreports($from_date, $to_date, $merid);
            return view('sitemerchant.productcod_failed_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("failedorders", $orderdetails)->with("allprod_failedreports", $allprod_failedreports)
            ->with('from_date',$from_date)->with('to_date',$to_date);
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    
    
    public function cod_hold_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            /*if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;
                
                
            } elseif(empty($getproductidlist)) {
                $productlist = array();      
                $orderdetails = array();
                $allprod_holdreports     = array();                 
            } else {
                $orderdetails = array();
            }*/
            $orderdetails = MerchantTransactions::getcod_hold_orders($merid);
            $allprod_holdreports = MerchantTransactions::allprod_holdreports($from_date, $to_date, $merid);
            return view('sitemerchant.productcod_hold_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("holdorders", $orderdetails)->with("allprod_holdreports", $allprod_holdreports)
            ->with('from_date',$from_date)->with('to_date',$to_date);
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
  
    public function merchant_deals_all_orders($merid)
    {
        
        if (Session::get('merchantid')) {
            
			if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
                      
            $getproductidlist = MerchantTransactions::getdeals_all_orders($merid);
            if ($getproductidlist) {
                $productlist  = $getproductidlist[0]->proid;
                $orderdetails = MerchantTransactions::get_transaction_details($productlist);
             } elseif(empty($getproductidlist)) {
                $productlist = array();      
                $orderdetails = array();
                         
            } else {
                $orderdetails = array();

            }
            return view('sitemerchant.deals_allorders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("allorders", $orderdetails);
            
            
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    
    
    public function deals_all_orders()
    {
        if (Session::get('merchantid')) {
            
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
           
            
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
          
            
				$getproductidlist = Merchantdeals::get_deals_all_details($merid);
           
                
                $dealrepp     = MerchantTransactions::getmerchant_dealreports($from_date,$to_date,$merid);

            return view('sitemerchant.deals_allorders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("allorders", $getproductidlist)->with("dealrepp", $dealrepp)->with('from_date',$from_date)
            ->with('to_date',$to_date);
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
     public function payu_deals_all_orders()
    {
        if (Session::get('merchantid')) {
            
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
           
            
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
          
            
                $getproductidlist = Merchantdeals::get_payu_deals_all_details($merid);
           
                
                $dealrepp     = MerchantTransactions::getmerchant_payu_dealreports($from_date,$to_date,$merid);

            return view('sitemerchant.payumoney_deals_allorders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("allorders", $getproductidlist)->with("dealrepp", $dealrepp)->with('from_date',$from_date)
            ->with('to_date',$to_date);
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
  
    
    public function deals_success_orders()
    {
        if (Session::get('merchantid')) {
           
            
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');


            $getproductidlist  = Merchantproducts::getproductidlist($merid);

           
            if ($getproductidlist) {

                 $productlist = $getproductidlist[0]->proid;

                
                
              

            } elseif(empty($getproductidlist)) {
                $productlist = array();      
                $orderdetails = array();
                $getsuccess_dealreports = array();
            } else {
                $orderdetails = array();
            }
              $orderdetails           = MerchantTransactions::getdeals_success_orders($merid);
               
                $getsuccess_dealreports = MerchantTransactions::getsuccess_dealreports($from_date, $to_date,  $merid);
          

            
            return view('sitemerchant.deals_success_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("successorders", $orderdetails)->with("getsuccess_dealreports", $getsuccess_dealreports)->with('from_date',$from_date)
            ->with('to_date',$to_date);
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function payu_deals_success_orders()
    {
        if (Session::get('merchantid')) {
           
            
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');


            $getproductidlist  = Merchantproducts::getproductidlist($merid);

           
            if ($getproductidlist) {

                 $productlist = $getproductidlist[0]->proid;

                
                
              

            } elseif(empty($getproductidlist)) {
                $productlist = array();      
                $orderdetails = array();
                $getsuccess_dealreports = array();
            } else {
                $orderdetails = array();
            }
              $orderdetails           = MerchantTransactions::get_payu_deals_success_orders($merid);
               
                $getsuccess_dealreports = MerchantTransactions::get_payu_success_dealreports($from_date, $to_date,  $merid);
          

            
            return view('sitemerchant.payu_deals_success_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("successorders", $orderdetails)->with("getsuccess_dealreports", $getsuccess_dealreports)->with('from_date',$from_date)
            ->with('to_date',$to_date);
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    
    public function deals_completed_orders()
    {
        if (Session::get('merchantid')) {
       
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
			if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;
                $orderdetails        = MerchantTransactions::getdeals_completed_orders($productlist);
                $getcomp_dealreports = MerchantTransactions::getcomp_dealreports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();      
                $orderdetails = array();
                $getcomp_dealreports = array();      
            } else {
                $orderdetails = array();
            }
            return view('sitemerchant.deals_completed_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("completedorders", $orderdetails)->with("getcomp_dealreports", $getcomp_dealreports);
        } else {
            return Redirect::to('sitemerchant');
        }
    }
 
    
    public function deals_failed_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
			if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;
                
                $orderdetails      = Transactions::getdeals_failed_orders($productlist);
                $allfailed_reports = MerchantTransactions::allfailed_reports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();      
                $orderdetails = array();
                $allfailed_reports = array();                   
            } else {
                $orderdetails = array();
            }

             $orderdetails      = Transactions::getdeals_failed_orders($productlist);
             $allfailed_reports = MerchantTransactions::allfailed_reports($from_date, $to_date, $merid);
            return view('sitemerchant.deals_failed_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("failedorders", $orderdetails)->with("allfailed_reports", $allfailed_reports)->with('from_date',$from_date)
            ->with('to_date',$to_date);
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function mer_payu_deals_failed_orders()
    {
        if (Session::get('merchantid')) {
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;
                
                $orderdetails      = Transactions::get_payu_deals_failed_orders($productlist);
                $allfailed_reports = MerchantTransactions::payu_deal_allfailed_reports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();      
                $orderdetails = array();
                $allfailed_reports = array();                   
            } else {
                $orderdetails = array();
            }

             $orderdetails      = Transactions::get_payu_deals_failed_orders($productlist);
             $allfailed_reports = MerchantTransactions::payu_deal_allfailed_reports($from_date, $to_date, $merid);
            return view('sitemerchant.payu_deals_failed_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("failedorders", $orderdetails)->with("allfailed_reports", $allfailed_reports)->with('from_date',$from_date)
            ->with('to_date',$to_date);
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
   
    public function deals_hold_orders()
    {
        if (Session::get('merchantid')) {
            
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu",$session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
           /** $getproductidlist  = Merchantproducts::getproductidlist($merid);
            if ($getproductidlist) {
                $productlist = $getproductidlist[0]->proid;
                
                $orderdetails    = MerchantTransactions::getdeals_hold_orders($productlist);
                $allhold_reports = MerchantTransactions::allhold_reports($from_date, $to_date, $productlist);
            } elseif(empty($getproductidlist)) {
                $productlist = array();      
                $orderdetails = array();
                $allhold_reports = array();                 
            } else {
                $orderdetails = array();
            } **/
             
             $orderdetails    = MerchantTransactions::getdeals_hold_orders($merid);
            
             $allhold_reports = MerchantTransactions::allhold_reports($from_date, $to_date, $merid);


            return view('sitemerchant.deals_hold_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("holdorders", $orderdetails)->with("allhold_reports", $allhold_reports)->with('from_date',$from_date)
            ->with('to_date',$to_date);
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    public function payu_deals_hold_orders()
    {
        if (Session::get('merchantid')) {
            
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
            {
                $session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
            }
            else 
            {
                $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
            }
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu",$session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
           
             
             $orderdetails    = MerchantTransactions::get_payu_deals_hold_orders($merid);
            
             $allhold_reports = MerchantTransactions::payu_allhold_reports($from_date, $to_date, $merid);


            return view('sitemerchant.payu_deals_hold_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("holdorders", $orderdetails)->with("allhold_reports", $allhold_reports)->with('from_date',$from_date)
            ->with('to_date',$to_date);
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
    
    public function dealscod_all_orders()
    {
        if (Session::has('merchantid')) {
           
            $from_date         = Input::get('from_date');
            $to_date           = Input::get('to_date');
			if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            
            $orderdetails      = MerchantTransactions::getdeal_cod_all_orders($merid);
			
			
            $allcod_reports    = MerchantTransactions::all_cod_reports($from_date, $to_date, $merid);
            
            return view('sitemerchant.dealscod_allorders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("allorders", $orderdetails)->with("allcod_reports", $allcod_reports)
            ->with('from_date',$from_date)->with('to_date',$to_date);
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
   
    
    public function dealscod_completed_orders(){

        if (Session::get('merchantid')) {
            
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $orderdetails          = MerchantTransactions::getdealscod_success_orders($merid); // getting datas using merhant id
            $allcodsuccess_reports = MerchantTransactions::allcodsuccess_reports($from_date, $to_date, $merid);
            return view('sitemerchant.dealscod_completed_orders')
            ->with('merchantheader', $merchantheader)
            ->with('merchantfooter', $merchantfooter)
            ->with('merchantleftmenus', $merchantleftmenus)
            ->with("completedorders", $orderdetails)
            ->with("allcodsuccess_reports", $allcodsuccess_reports)
            ->with('from_date',$from_date)
            ->with('to_date',$to_date);
        } else {
            return Redirect::to('sitemerchant');
        }
    }
       
    public function dealscod_failed_orders()
    {
        if (Session::get('merchantid')) {
            
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            $orderdetails         = MerchantTransactions::getdealscod_failed_orders($merid);
            $allcodfailed_reports = MerchantTransactions::allcodfailed_reports($from_date, $to_date, $merid);
            
            return view('sitemerchant.dealscod_failed_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("failedorders", $orderdetails)
            ->with("allcodfailed_reports", $allcodfailed_reports)
            ->with('from_date',$from_date)
            ->with('to_date',$to_date);
        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
 
    
    public function dealscod_hold_orders()
    {
        
        if (Session::get('merchantid')) {
            
            $from_date = Input::get('from_date');
            $to_date   = Input::get('to_date');
			if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $merid             = Session::get('merchantid');
            //$getproductidlist  = Merchantproducts::getproductidlist($merid);
            $orderdetails       = MerchantTransactions::getdeals_cod_hold_orders($merid);
            $allcodhold_reports = MerchantTransactions::allcodhold_reports($from_date, $to_date, $merid);
            
            return view('sitemerchant.dealscod_hold_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("holdorders", $orderdetails)->with("allcodhold_reports", $allcodhold_reports)
            ->with('from_date',$from_date)->with('to_date',$to_date);


        } else {
            return Redirect::to('sitemerchant');
        }
        
    }
  
    public function update_order_cod()
    {

		
        $orderid = $_GET['order_id'];
        $status  = $_GET['id'];
		$proid  = $_GET['proid'];
		$pay_status = 1;
		$now  = date('Y-m-d h:i:sa'); 
		
        if($status == 1){
            $updaters = MerchantTransactions::update_cod_status($status, $orderid,$proid);
        }elseif($status == 2){
            $updaters = MerchantTransactions::update_cod_status($status, $orderid,$proid);
        }elseif($status == 3){
            $updaters = MerchantTransactions::update_cod_status($status, $orderid,$proid);
        }elseif($status == 4 ){
             $updaters = MerchantTransactions::update_cod_status_and_delivery($status, $orderid,$proid,$pay_status);
        }elseif($status == 6 ){
             $updaters = MerchantTransactions::update_cod_status_and_cancel($status, $orderid,$proid);
        }elseif($status == 8 ){
             $updaters = MerchantTransactions::update_cod_status_and_returned($status, $orderid,$proid);
        }elseif($status == 10 ){
             $updaters = MerchantTransactions::update_cod_status_and_replaced($status, $orderid,$proid);
        }

        if ($updaters) {
        echo "success";
		 $getdetails = MerchantTransactions::getdetails_customer($orderid); 
		 if($getdetails){ 
			$send_title = "Order Delivery Status"; 
			if($getdetails[0]->delivery_status == 2){
					
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_PACKED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.ADMIN_DELIVERY_STATUS_PRODUCT_PACKED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_PACKED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_PACKED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_PACKED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_PACKED');
                }
					 
			} else
					
			if($getdetails[0]->delivery_status == 3){
				
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DISPATCHED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DISPATCHED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_DISPATCHED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_DISPATCHED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_DISPATCHED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_DISPATCHED');
                }
			
			} else
			if($getdetails[0]->delivery_status == 4){
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DELIVERED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DELIVERED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_DELIVERED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_DELIVERED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_DELIVERED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_DELIVERED');
                }
				
			} else 
			if($getdetails[0]->delivery_status == 6){
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_CANCELLED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_CANCELLED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_CANCELLED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_CANCELLED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_CANCELLED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_CANCELLED');
                }
			} else 
			if($getdetails[0]->delivery_status == 10){
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_REPLACED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_REPLACED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_REPLACED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_REPLACED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_REPLACED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_REPLACED');
                } 
			} else 
			if($getdetails[0]->delivery_status == 8){
					if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_RETURNED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_RETURNED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_RETURNED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_RETURNED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_RETURNED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_RETURNED');
                }  
			}	
				
                $this->setLanguageLocaleFront();
				
			$CusEmail = $getdetails[0]->cus_email;
			if($CusEmail!=''){
			 Mail::send('emails.adminOrderstatuschange', array(
                                    'transaction_id'    =>  $orderid,
									'prod_title' 		=>  $getdetails[0]->pro_title,
									'customer_name' 	=>  $getdetails[0]->cus_name,
									'approve_status' 	=>  $send_status,
									'status_title' 		=>  $send_title,
									'status_message' 	=>  $send_msg,
									'date' 	=>  $now,
                                    
                                ), function($message) use ($CusEmail)
                                {
                                    $message->to($CusEmail)->subject('Order Delivery Status By Merchant');
                                });   
				} 
			}			
							
        }
        
    }
	
	 public function update_order_cod_mer_deal()
    {
		
        $orderid = $_GET['order_id'];
        $status  = $_GET['id'];
		 $proid  = $_GET['proid'];
		 $pay_status = 1;
		 $now  = date('Y-m-d h:i:sa'); 
        
         
		
        if($status == 1){
            $updaters = MerchantTransactions::update_cod_status_admin($status, $orderid,$proid);
        }elseif($status == 2){
            $updaters = MerchantTransactions::update_cod_status_admin($status, $orderid,$proid);
        }elseif($status == 3){
            $updaters = MerchantTransactions::update_cod_status_admin($status, $orderid,$proid);
        }elseif($status == 4 ){
             $updaters = MerchantTransactions::update_cod_status_and_delivery($status, $orderid,$proid,$pay_status);
        }elseif($status == 6 ){
             $updaters = MerchantTransactions::update_cod_status_and_cancel($status, $orderid,$proid);
        }elseif($status == 8 ){
             $updaters = MerchantTransactions::update_cod_status_and_returned($status, $orderid,$proid);
        }elseif($status == 10 ){
             $updaters = MerchantTransactions::update_cod_status_and_replaced($status, $orderid,$proid);
        }
		
        if ($updaters) {
        echo "success";
		 $getdetails = MerchantTransactions::getdetails_customer_deal($orderid); 
		 if($getdetails){ 
			$send_title = "Order Delivery Status"; 
			if($getdetails[0]->delivery_status == 2){
					
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_PACKED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_PACKED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_PACKED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_PACKED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_PACKED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_PACKED');
                }
					 
			} else
					
			if($getdetails[0]->delivery_status == 3){
				
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DISPATCHED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DISPATCHED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_DISPATCHED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_DISPATCHED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_DISPATCHED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_DISPATCHED');
                }
			
			} else
			if($getdetails[0]->delivery_status == 4){
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DELIVERED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DELIVERED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_DELIVERED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_DELIVERED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_DELIVERED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_DELIVERED');
                }
				
			} else 
			if($getdetails[0]->delivery_status == 6){
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_CANCELLED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_CANCELLED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_CANCELLED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_CANCELLED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_CANCELLED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_CANCELLED');
                }
			} else 
			if($getdetails[0]->delivery_status == 10){
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_REPLACED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_REPLACED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_REPLACED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_REPLACED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_REPLACED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_REPLACED');
                } 
			} else 
			if($getdetails[0]->delivery_status == 8){
					if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_RETURNED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_RETURNED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_RETURNED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_RETURNED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_RETURNED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_RETURNED');
                }  
			}	
				$this->setLanguageLocaleFront();
				
			$CusEmail = $getdetails[0]->cus_email;
			if($CusEmail!=''){
			 Mail::send('emails.adminOrderstatuschange', array(
                                    'transaction_id'    =>  $orderid,
									'prod_title' 		=>  $getdetails[0]->deal_title,
									'customer_name' 	=>  $getdetails[0]->cus_name,
									'approve_status' 	=>  $send_status,
									'status_title' 		=>  $send_title,
									'status_message' 	=>  $send_msg,
									'date' 	=>  $now,
                                    
                                ), function($message) use ($CusEmail)
                                {
                                    $message->to($CusEmail)->subject('Order Delivery Status By Merchant');
                                });   
				} 
			}			
							
        }
								
        
    }
	
	
	public function update_order_paypal()
    {
        $orderid = $_GET['order_id'];
        $status  = $_GET['id'];
		$proid  = $_GET['proid'];
		$now  = date('Y-m-d h:i:sa'); 
		$pay_status = 1; 
        if($status == 1){
            $updaters = MerchantTransactions::update_paypal_status($status, $orderid,$proid);
        }elseif($status == 2){
            $updaters = MerchantTransactions::update_paypal_status($status, $orderid,$proid);
        }elseif($status == 3){
            $updaters = MerchantTransactions::update_paypal_status($status, $orderid,$proid);
        }elseif($status == 4){
             $updaters = MerchantTransactions::update_paypal_status($status, $orderid,$proid);
        }elseif($status == 6){
             $updaters = MerchantTransactions::update_paypal_status_cancel($status, $orderid,$proid);
        }elseif($status == 8){
             $updaters = MerchantTransactions::update_paypal_status_returned($status, $orderid,$proid);
        }elseif($status == 10){
             $updaters = MerchantTransactions::update_paypal_status_replaced($status, $orderid,$proid);
        }
       
		if ($updaters) {
        echo "success";
		 $getdetails = MerchantTransactions::getdetails_customer_paypal($orderid); 
		 if($getdetails){ 
			$send_title = "Order Delivery Status"; 
			if($getdetails[0]->delivery_status == 2){
					
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_PACKED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_PACKED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_PACKED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_PACKED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_PACKED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_PACKED');
                }
					 
			} else
					
			if($getdetails[0]->delivery_status == 3){
				
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DISPATCHED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DISPATCHED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_DISPATCHED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_DISPATCHED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_DISPATCHED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_DISPATCHED');
                }
			
			} else
			if($getdetails[0]->delivery_status == 4){
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DELIVERED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.ADMIN_DELIVERY_STATUS_PRODUCT_DELIVERED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_DELIVERED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_DELIVERED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_DELIVERED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_DELIVERED');
                }
				
			} else 
			if($getdetails[0]->delivery_status == 6){
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_CANCELLED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_CANCELLED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_CANCELLED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_CANCELLED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_CANCELLED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_CANCELLED');
                }
			} else 
			if($getdetails[0]->delivery_status == 10){
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_REPLACED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_REPLACED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_REPLACED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_REPLACED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_REPLACED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_REPLACED');
                } 
			} else 
			if($getdetails[0]->delivery_status == 8){
					if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_RETURNED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_RETURNED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_RETURNED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_RETURNED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_RETURNED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_RETURNED');
                }  
			}	
				$this->setLanguageLocaleFront();
				
			$CusEmail = $getdetails[0]->cus_email;
			if($CusEmail!=''){
			 Mail::send('emails.adminOrderstatuschange', array(
                                    'transaction_id'    =>  $orderid,
									'prod_title' 		=>  $getdetails[0]->pro_title,
									'customer_name' 	=>  $getdetails[0]->cus_name,
									'approve_status' 	=>  $send_status,
									'status_title' 		=>  $send_title,
									'status_message' 	=>  $send_msg,
									'date' 	=>  $now,
                                    
                                ), function($message) use ($CusEmail)
                                {
                                    $message->to($CusEmail)->subject('Order Delivery Status By Merchant');
                                });   
				} 
			}			
							
        }
        
    }
	public function update_order_payumoney()
    {
        $orderid = $_GET['order_id'];
        $status  = $_GET['id'];
        $proid  = $_GET['proid'];
        $now  = date('Y-m-d h:i:sa'); 
        $pay_status = 1; 
        if($status == 1){
            $updaters = MerchantTransactions::update_payu_status($status, $orderid,$proid);
        }elseif($status == 2){
            $updaters = MerchantTransactions::update_payu_status($status, $orderid,$proid);
        }elseif($status == 3){
            $updaters = MerchantTransactions::update_payu_status($status, $orderid,$proid);
        }elseif($status == 4){
             $updaters = MerchantTransactions::update_payu_status($status, $orderid,$proid);
        }elseif($status == 6){
             $updaters = MerchantTransactions::update_payu_status_cancel($status, $orderid,$proid);
        }elseif($status == 8){
             $updaters = MerchantTransactions::update_payu_status_returned($status, $orderid,$proid);
        }elseif($status == 10){
             $updaters = MerchantTransactions::update_payu_status_replaced($status, $orderid,$proid);
        }
       
        if ($updaters) {
        echo "success";
         $getdetails = MerchantTransactions::getdetails_customer_payu($orderid); 
         if($getdetails){ 
            $send_title = "Order Delivery Status"; 
            if($getdetails[0]->delivery_status == 2){
                    
                if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_PACKED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_PACKED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_PACKED');
                }
                
                if(Lang::has(Session::get('mer_lang_file').'.ORDER_PACKED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_PACKED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_PACKED');
                }
                     
            } else
                    
            if($getdetails[0]->delivery_status == 3){
                
                if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DISPATCHED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DISPATCHED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_DISPATCHED');
                }
                
                if(Lang::has(Session::get('mer_lang_file').'.ORDER_DISPATCHED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_DISPATCHED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_DISPATCHED');
                }
            
            } else
            if($getdetails[0]->delivery_status == 4){
                if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DELIVERED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.ADMIN_DELIVERY_STATUS_PRODUCT_DELIVERED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_DELIVERED');
                }
                
                if(Lang::has(Session::get('mer_lang_file').'.ORDER_DELIVERED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_DELIVERED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_DELIVERED');
                }
                
            } else 
            if($getdetails[0]->delivery_status == 6){
                if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_CANCELLED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_CANCELLED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_CANCELLED');
                }
                
                if(Lang::has(Session::get('mer_lang_file').'.ORDER_CANCELLED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_CANCELLED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_CANCELLED');
                }
            } else 
            if($getdetails[0]->delivery_status == 10){
                if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_REPLACED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_REPLACED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_REPLACED');
                }
                
                if(Lang::has(Session::get('mer_lang_file').'.ORDER_REPLACED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_REPLACED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_REPLACED');
                } 
            } else 
            if($getdetails[0]->delivery_status == 8){
                    if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_RETURNED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_RETURNED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_RETURNED');
                }
                
                if(Lang::has(Session::get('mer_lang_file').'.ORDER_RETURNED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_RETURNED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_RETURNED');
                }  
            }   
                $this->setLanguageLocaleFront();
                
            $CusEmail = $getdetails[0]->cus_email;
            if($CusEmail!=''){
             Mail::send('emails.adminOrderstatuschange', array(
                                    'transaction_id'    =>  $orderid,
                                    'prod_title'        =>  $getdetails[0]->pro_title,
                                    'customer_name'     =>  $getdetails[0]->cus_name,
                                    'approve_status'    =>  $send_status,
                                    'status_title'      =>  $send_title,
                                    'status_message'    =>  $send_msg,
                                    'date'  =>  $now,
                                    
                                ), function($message) use ($CusEmail)
                                {
                                    $message->to($CusEmail)->subject('Order Delivery Status By Merchant');
                                });   
                } 
            }           
                            
        }
        
    }
	
	
	public function update_deal_order_paypal()
    {
        $orderid = $_GET['order_id'];
        $status  = $_GET['id'];
		$proid  = $_GET['proid'];
		$now  = date('Y-m-d h:i:sa'); 
		
		$pay_status = 1;
		
        if($status == 1){
            $updaters = MerchantTransactions::update_deal_paypal_status($status, $orderid,$proid);
        }elseif($status == 2){
            $updaters = MerchantTransactions::update_deal_paypal_status($status, $orderid,$proid);
        }elseif($status == 3){
            $updaters = MerchantTransactions::update_deal_paypal_status($status, $orderid,$proid);
        }elseif($status == 4){
             $updaters = MerchantTransactions::update_deal_paypal_status($status, $orderid,$proid);
        }elseif($status == 6){
             $updaters = MerchantTransactions::update_deal_paypal_status_cancel($status, $orderid,$proid);
        }elseif($status == 8){
             $updaters = MerchantTransactions::update_deal_paypal_status_returned($status, $orderid,$proid);
        }elseif($status == 10){
             $updaters = MerchantTransactions::update_deal_paypal_status_replaced($status, $orderid,$proid);
        }
		
        if ($updaters) {
        echo "success";
		 $getdetails = MerchantTransactions::getdetails_customer_paypal_deal($orderid); 
		 if($getdetails){ 
			$send_title = "Order Delivery Status"; 
			if($getdetails[0]->delivery_status == 2){
					
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_PACKED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_PACKED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_PACKED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_PACKED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_PACKED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_PACKED');
                }
					 
			} else
					
			if($getdetails[0]->delivery_status == 3){
				
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DISPATCHED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DISPATCHED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_DISPATCHED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_DISPATCHED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_DISPATCHED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_DISPATCHED');
                }
			
			} else
			if($getdetails[0]->delivery_status == 4){
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DELIVERED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DELIVERED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_DELIVERED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_DELIVERED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_DELIVERED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_DELIVERED');
                }
				
			} else 
			if($getdetails[0]->delivery_status == 6){
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_CANCELLED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_CANCELLED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_CANCELLED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_CANCELLED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_CANCELLED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_CANCELLED');
                }
			} else 
			if($getdetails[0]->delivery_status == 10){
				if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_REPLACED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_REPLACED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_REPLACED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_REPLACED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_REPLACED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_REPLACED');
                } 
			} else 
			if($getdetails[0]->delivery_status == 8){
					if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_RETURNED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_RETURNED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_RETURNED');
                }
				
				if(Lang::has(Session::get('mer_lang_file').'.ORDER_RETURNED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_RETURNED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_RETURNED');
                }  
			}	
				$this->setLanguageLocaleFront();
				
			$CusEmail = $getdetails[0]->cus_email;
			if($CusEmail!=''){
			 Mail::send('emails.adminOrderstatuschange', array(
                                    'transaction_id'    =>  $orderid,
									'prod_title' 		=>  $getdetails[0]->deal_title,
									'customer_name' 	=>  $getdetails[0]->cus_name,
									'approve_status' 	=>  $send_status,
									'status_title' 		=>  $send_title,
									'status_message' 	=>  $send_msg,
									'date' 	=>  $now,
                                    
                                ), function($message) use ($CusEmail)
                                {
                                    $message->to($CusEmail)->subject('Order Delivery Status By Merchant');
                                });   
				} 
			}			
							
        }
        
    }
public function update_deal_order_payu()
    {
        $orderid = $_GET['order_id'];
        $status  = $_GET['id'];
        $proid  = $_GET['proid'];
        $now  = date('Y-m-d h:i:sa'); 
        
        $pay_status = 1;
        
        if($status == 1){
            $updaters = MerchantTransactions::update_deal_payu_status($status, $orderid,$proid);
        }elseif($status == 2){
            $updaters = MerchantTransactions::update_deal_payu_status($status, $orderid,$proid);
        }elseif($status == 3){
            $updaters = MerchantTransactions::update_deal_payu_status($status, $orderid,$proid);
        }elseif($status == 4){
             $updaters = MerchantTransactions::update_deal_payu_status($status, $orderid,$proid);
        }elseif($status == 6){
             $updaters = MerchantTransactions::update_deal_payu_status_cancel($status, $orderid,$proid);
        }elseif($status == 8){
             $updaters = MerchantTransactions::update_deal_payu_status_returned($status, $orderid,$proid);
        }elseif($status == 10){
             $updaters = MerchantTransactions::update_deal_payu_status_replaced($status, $orderid,$proid);
        }
        
        if ($updaters) {
        echo "success";
         $getdetails = MerchantTransactions::getdetails_customer_payu_deal($orderid); 
         if($getdetails){ 
            $send_title = "Order Delivery Status"; 
            if($getdetails[0]->delivery_status == 2){
                    
                if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_PACKED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_PACKED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_PACKED');
                }
                
                if(Lang::has(Session::get('mer_lang_file').'.ORDER_PACKED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_PACKED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_PACKED');
                }
                     
            } else
                    
            if($getdetails[0]->delivery_status == 3){
                
                if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DISPATCHED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DISPATCHED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_DISPATCHED');
                }
                
                if(Lang::has(Session::get('mer_lang_file').'.ORDER_DISPATCHED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_DISPATCHED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_DISPATCHED');
                }
            
            } else
            if($getdetails[0]->delivery_status == 4){
                if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DELIVERED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_DELIVERED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_DELIVERED');
                }
                
                if(Lang::has(Session::get('mer_lang_file').'.ORDER_DELIVERED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_DELIVERED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_DELIVERED');
                }
                
            } else 
            if($getdetails[0]->delivery_status == 6){
                if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_CANCELLED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_CANCELLED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_CANCELLED');
                }
                
                if(Lang::has(Session::get('mer_lang_file').'.ORDER_CANCELLED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_CANCELLED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_CANCELLED');
                }
            } else 
            if($getdetails[0]->delivery_status == 10){
                if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_REPLACED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_REPLACED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_REPLACED');
                }
                
                if(Lang::has(Session::get('mer_lang_file').'.ORDER_REPLACED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_REPLACED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_REPLACED');
                } 
            } else 
            if($getdetails[0]->delivery_status == 8){
                    if(Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_RETURNED')!= '') 
                {
                    $send_msg = trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS_PRODUCT_RETURNED');
                }
                else
                {
                    $send_msg =  trans($this->MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS_PRODUCT_RETURNED');
                }
                
                if(Lang::has(Session::get('mer_lang_file').'.ORDER_RETURNED')!= '') 
                {
                    $send_status = trans(Session::get('mer_lang_file').'.ORDER_RETURNED');
                }
                else
                {
                    $send_status =  trans($this->MER_OUR_LANGUAGE.'.ORDER_RETURNED');
                }  
            }   
                $this->setLanguageLocaleFront();
                
            $CusEmail = $getdetails[0]->cus_email;
            if($CusEmail!=''){
             Mail::send('emails.adminOrderstatuschange', array(
                                    'transaction_id'    =>  $orderid,
                                    'prod_title'        =>  $getdetails[0]->deal_title,
                                    'customer_name'     =>  $getdetails[0]->cus_name,
                                    'approve_status'    =>  $send_status,
                                    'status_title'      =>  $send_title,
                                    'status_message'    =>  $send_msg,
                                    'date'  =>  $now,
                                    
                                ), function($message) use ($CusEmail)
                                {
                                    $message->to($CusEmail)->subject('Order Delivery Status By Merchant');
                                });   
                } 
            }           
                            
        }
        
    }
    /*Merchant -> service Transcation*/
    public function service_all_orders(){
        if (Session::has('merchantid')) {
            $mer_id = Session::get('merchantid');
            $from_date      = Input::get('from_date');
            $to_date        = Input::get('to_date');
            $service_order_search = Transactions::mer_service_order_search($from_date, $to_date,$mer_id);
            if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_TRANSACTION');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_TRANSACTION');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_transaction');
            $orderdetails   = Transactions::mer_get_all_service_orders($mer_id);

            return view('sitemerchant.service_all_orders')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchantleftmenus', $merchantleftmenus)->with("allorders", $orderdetails)->with("service_order_search", $service_order_search);
        } else {
            return Redirect::to('sitemerchant');
        }
    }   //service all orders


    public function update_service_order()
    {
        $serv_transaction_id = $_GET['serv_transaction_id'];
        $status  = $_GET['id'];
        $cus_id  = $_GET['cus_id'];
        $pay_type = $_GET['pay_type'];

       if($status==1){
            $trans_id                    = $serv_transaction_id;
            $get_email                   = Home::get_email($cus_id);
           // $trans_id                    = Home::transaction_id($serv_transaction_id);
			$get_subtotal                = Transactions::get_subtotal($trans_id);
            
			$array = array('email_id'=>$get_email);
            //Customer Mail after order complete
            
            Mail::send('emails.service-order-status', array(    
                'status' => 'Success',
                'transaction_id' => $trans_id,
                'Sub_total' =>  $get_subtotal), function($message) use($array)
                {
					$customer_mail = $array['email_id'];
					if(Lang::has(Session::get('mer_lang_file').'.MER_YOUR_ORDER_STATUS_DETAILS')!= '') 
					{
					$session_message = trans(Session::get('mer_lang_file').'.MER_YOUR_ORDER_STATUS_DETAILS');
					}
					else 
					{
					$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_YOUR_ORDER_STATUS_DETAILS');
					}
					$message->to($customer_mail)->subject($session_message);
                }); 
       } //if order complete
       if($pay_type==2){    //if razorpay 
          $array = array(
             'ord_service_status'=>$status,    //already paid, now service completed
          );
       }else{                            // else cash on service
          $array = array(
            'order_status' => 1,        // payment completed
            'ord_service_status' => $status,  // service completed
          );
       }
        $update = Transactions::update_service_order($array, $serv_transaction_id);
       
        if ($update==TRUE) {
            echo "success";
        }else{
            echo "Failed";
        }
        
    }

    /* Merchant commission payment check with overall order table per merchant  */
        /* 
            --- Sum of (total online amount , total coupon, total wallet )  consider as admin amount
            --- Sum of (total offline amount)  consider as merchant amount --- 
            --- Total order amount * (merchant_comission/100)  is admin comission --- 
            --- (( (Admin Amount - admin commission) - withdraw_request amount by merchant - merchant paid commmision amount ) > 0 ) merchant fund request enable & admin pay request amount enable  ---
             --- (( (Admin Amount - admin commission) - withdraw_request amount by merchant - merchant paid commmision amount ) > 0 ) < 0 merchnat need to pay commission to admin , merchant fund request disabled, & admin withdraw request pay disable 

        */
    public function checkMerchantOverOrderReport(Request $request)    
    {
        $mer_id                 = $request->input('mer_id');
        $mer_request_amt        = $request->input('mer_request_amt');

        $commison_amt           = Fund::commison_amt($mer_id);
        $overallOrdDetails      = Fund::merchantOverallOrderDetails($mer_id);

        $merchantRequested      = $merchantPaidCommission   = 0;

        foreach($commison_amt as $commision) { } 
            $comminsion = $commision->mer_commission;

        //echo 'commsion -- '.$comminsion;

        if(count($overallOrdDetails)>0){
            $admin_amount     =  $overallOrdDetails->over_tot_online_amt + $overallOrdDetails->over_tot_coupon_amt +                 $overallOrdDetails->over_tot_wallet_amt;

            //$admin_commission =  $overallOrdDetails->over_tot_ord_amt *($comminsion/100);   

            $get_COD_commissionPay_details[$mer_id]      = Fund::get_COD_commissionPay_details($mer_id);
            $get_order_commissionPay_details[$mer_id]    = Fund::get_order_commissionPay_details($mer_id);

            $cod_commission =  $order_commission = 0;
            if(isset($get_COD_commissionPay_details[$mer_id]))
            {
              $cod_commission = $get_COD_commissionPay_details[$mer_id][0]->sumMerchantCommission!=''?$get_COD_commissionPay_details[$mer_id][0]->sumMerchantCommission:0;
            }
            if(isset($get_order_commissionPay_details[$mer_id]))
            {
              $order_commission = $get_order_commissionPay_details[$mer_id][0]->sumMerchantCommission!=''?$get_order_commissionPay_details[$mer_id][0]->sumMerchantCommission:0;
            }

            $admin_commission = $cod_commission + $order_commission  ;

            $merchantProfit   =  $admin_amount - $admin_commission;

            //echo 'admin amount'.$admin_commission .'<br>'.'<br>admin amt - admin_commission' .$merchantProfit;

            $merchantRequestedCheck = Fund::check_withdraw($mer_id);

            if(count($merchantRequestedCheck)>0){
                foreach ($merchantRequestedCheck as $result) { }
                if($result->wd_status=='1')    
                    $merchantRequested = $result->wd_submited_wd_amt;
            }   
            $merchantPayCommissionCheck = Fund::check_commission($mer_id);

            if(count($merchantPayCommissionCheck)>0){
                foreach ($merchantPayCommissionCheck as $result) { }
                $merchantPaidCommission = $result->paidAmount;
            } 

            //echo 'merchant requested : '.$merchantRequested . '<br> merchant paid : '. $merchantPaidCommission;
 
            $newMerchantProfit  = $merchantProfit - $merchantRequested - $merchantPaidCommission;

            if($mer_request_amt>0){
                $newMerchantProfit  = $newMerchantProfit - $mer_request_amt; // check ,merchant request amount to withdraw is allowed/not
            }
            // echo $newMerchantProfit ;
            //merchant fund request enable 
            if($newMerchantProfit>0){
                return 'merchant_request_allowed';    // admin need to pay merchant 
            }elseif($newMerchantProfit<0){
                return 'merchant_request_blocked'; // merchant need to pay admin
            }else{
                return 'merchant_request_balanced'; // amount not available to process
            }


        }else
        {
            return ' '; // no process 
        }

    }         

 
     /* Merchant commission payment check with overall order table per merchant ends  */

    /* Merchant Commission Paid Listing */ 
     public function commission_paid()
    {
        
        $merchant_id    = Session::get('merchantid');
        if (Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION')!= '')
            { 
            $session_messag =  trans(Session::get('mer_lang_file').'.MER_COMMISSION');
        }  
        else 
        { 
            $session_messag =  trans($this->MER_OUR_LANGUAGE.'.MER_COMMISSION');
        }

        $session_msg        = '';

        if(Session::has('fund_request_blocked')){
            $session_msg = Session::get('fund_request_blocked');
            Session::forget('fund_request_blocked');
        }
        if(Session::has('commission_paid_sucess')){
            $session_msg = Session::get('commission_paid_sucess');
            Session::forget('commission_paid_sucess');
        }
        
               
            
            $adminheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_messag);
            $adminfooter    = view('sitemerchant.includes.merchant_footer');
            $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_commission');
            
           
            $commissionPaidDetails = Fund::get_merchant_commissionPaid_details($merchant_id);
            
            return view('sitemerchant.commission_paid')->with('adminheader', $adminheader)->with('adminfooter', $adminfooter)->with('adminleftmenus', $adminleftmenus)->with('commissionPaidDetails',$commissionPaidDetails)->with('session_msg',$session_msg);
       
    }

      /* Merchant Commission Payment Listing */
     public function commission_listing()
    {
        if(Session::has('merchantid')){
            $merchant_id    = Session::get('merchantid');

            if (Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION')!= '')
            { 
                $session_messag =  trans(Session::get('mer_lang_file').'.MER_COMMISSION');
            }  
            else 
            { 
                $session_messag =  trans($this->MER_OUR_LANGUAGE.'.MER_COMMISSION');
            }

            $session_msg        = '';
            
            /* Merchant commission payment check with overall order table per merchant  */
            $merchantOverallPayment = $this->checkMerchantOverOrderReport_local($merchant_id,0);

            //echo $merchantOverallPayment ;exit();
            $merchant_trans_count = DB::table('nm_ordercod')->where('cod_merchant_id','=',$merchant_id)->count();
                $merchant_trans_count1 = DB::table('nm_order')->where('order_merchant_id','=',$merchant_id)->count();
                $merchant_trans_count2 = DB::table('nm_order_payu')->where('order_merchant_id','=',$merchant_id)->count();

            if($merchantOverallPayment == 'merchant_request_blocked'){          
                
                $adminheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_messag);
                $adminfooter    = view('sitemerchant.includes.merchant_footer');
                $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_commission');
                
                /* no need to calculate from order table , becaues overall order details are taken to merchnat overall order table from there we get the updates cur pay commision details */

               // $commissionDetails = Fund::get_COD_commissionPay_details($merchant_id);
               // print_r($commissionDetails);
                

                $commissionDetails      = Fund::overallOrder_nd_merchant($merchant_id);
                $commissionPaidDetails  = Fund::get_commissionPaid_details($merchant_id);
                $pendingCommission      = Fund::get_onPendingCommissionCount($merchant_id);

                $get_COD_commissionPay_details[$merchant_id]      = Fund::get_COD_commissionPay_details_allType($merchant_id);
                $get_order_commissionPay_details[$merchant_id]    = Fund::get_order_commissionPay_details_allType($merchant_id);

                return view('sitemerchant.commission_listing')->with('adminheader', $adminheader)->with('adminfooter', $adminfooter)->with('adminleftmenus', $adminleftmenus)->with('commissionDetails', $commissionDetails)->with('commissionPaidDetails',$commissionPaidDetails)->with('pendingCommission',$pendingCommission)->with('get_COD_commissionPay_details',$get_COD_commissionPay_details)->with('get_order_commissionPay_details',$get_order_commissionPay_details)->with('session_msg',$session_msg);
            } else{
                if($merchant_trans_count > 0 || $merchant_trans_count1 > 0 || $merchant_trans_count2 > 0 )
                {
                    if (Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_NO_NEED_TO_PAY_COMMISSION')!= ''){ 
                        $session_messag =  trans(Session::get('mer_lang_file').'.MER_MERCHANT_NO_NEED_TO_PAY_COMMISSION');
                    }else{ 
                            $session_messag =  trans($this->MER_OUR_LANGUAGE.'.MER_MERCHANT_NO_NEED_TO_PAY_COMMISSION');
                    }
                }
                else
                {
                     if (Lang::has(Session::get('mer_lang_file').'.MERCHANT_NO_NEED_TO_PAY_COMMISSION')!= ''){ 
                        $session_messag =  trans(Session::get('mer_lang_file').'.MERCHANT_NO_NEED_TO_PAY_COMMISSION');
                    }else{ 
                            $session_messag =  trans($this->MER_OUR_LANGUAGE.'.MERCHANT_NO_NEED_TO_PAY_COMMISSION');
                    }
                }

                Session::flash('fund_request_blocked',$session_messag);

                return Redirect::to('commission_paid');
            }  
        }else{
            return Redirect::to('sitemerchant');
        }  
    }



    /* Merchant commission payment check with overall order table per merchant  */
        /* 
            --- Sum of (total online amount , total coupon, total wallet )  consider as admin amount
            --- Sum of (total offline amount)  consider as merchant amount --- 
            --- Total order amount * (merchant_comission/100)  is admin comission --- 
            --- (( (Admin Amount - admin commission) - withdraw_request amount by merchant - merchant paid commmision amount ) > 0 ) merchant fund request enable & admin pay request amount enable  ---
             --- (( (Admin Amount - admin commission) - withdraw_request amount by merchant - merchant paid commmision amount ) > 0 ) < 0 merchnat need to pay commission to admin , merchant fund request disabled, & admin withdraw request pay disable 

        */
    public function checkMerchantOverOrderReport_local($mer_id,$mer_request_amt)
    {

        $commison_amt           = Fund::commison_amt($mer_id);
        $overallOrdDetails      = Fund::merchantOverallOrderDetails($mer_id);

        $merchantRequested      = $merchantPaidCommission   = 0;

        foreach($commison_amt as $commision) { } 
            $comminsion = $commision->mer_commission;

        //echo 'commsion -- '.$comminsion;

        if(count($overallOrdDetails)>0 ){
            $admin_amount     =  $overallOrdDetails->over_tot_online_amt + $overallOrdDetails->over_tot_coupon_amt +                 $overallOrdDetails->over_tot_wallet_amt;
            
            //$admin_commission =  $overallOrdDetails->over_tot_ord_amt *($comminsion/100);   

            $get_COD_commissionPay_details[$mer_id]      = Fund::get_COD_commissionPay_details_allType($mer_id);
            $get_order_commissionPay_details[$mer_id]    = Fund::get_order_commissionPay_details_allType($mer_id);

            $cod_commission =  $order_commission = 0;
            if(isset($get_COD_commissionPay_details[$mer_id]))
            {
              $cod_commission = $get_COD_commissionPay_details[$mer_id][0]->sumMerchantCommission!=''?$get_COD_commissionPay_details[$mer_id][0]->sumMerchantCommission:0;
            }
            if(isset($get_order_commissionPay_details[$mer_id]))
            {
              $order_commission = $get_order_commissionPay_details[$mer_id][0]->sumMerchantCommission!=''?$get_order_commissionPay_details[$mer_id][0]->sumMerchantCommission:0;
            }

            $admin_commission = $cod_commission + $order_commission  ;

            $merchantProfit   =  $admin_amount - $admin_commission;

            //echo 'admin amount'.$admin_commission .'<br>'.'<br>admin amt - admin_commission' .$merchantProfit;

            $merchantRequestedCheck = Fund::check_withdraw($mer_id);

            if(count($merchantRequestedCheck)>0){
                foreach ($merchantRequestedCheck as $result) { }
                $merchantRequested = $result->wd_submited_wd_amt;
            }   
            $merchantPayCommissionCheck = Fund::check_commission($mer_id);

            if(count($merchantPayCommissionCheck)>0){
                foreach ($merchantPayCommissionCheck as $result) { }
                $merchantPaidCommission = $result->paidAmount;
            } 

            //echo 'merchant requested : '.$merchantRequested . '<br> merchant paid : '. $merchantPaidCommission;
 
            $newMerchantProfit  = $merchantProfit - $merchantRequested - $merchantPaidCommission;

            if($mer_request_amt>0){
                $newMerchantProfit  = $newMerchantProfit - $mer_request_amt; // check ,merchant request amount to withdraw is allowed/not
            }
            // echo $newMerchantProfit ;
            //merchant fund request enable 
            if($newMerchantProfit>0){
                return 'merchant_request_allowed';    // admin need to pay merchant 
            }elseif($newMerchantProfit<0){
                return 'merchant_request_blocked'; // merchant need to pay admin
            }else{
                return 'merchant_request_balanced'; // amount not available to process
            }


        }else
        {
            return ' '; // no process 
        }

    }         

 
     /* Merchant commission payment check with overall order table per merchant ends  */

    /* merchant offline payment  */

    public function commission_offline_pay($data){
        if(Session::has("merchantid")){
            $result = explode('/**/', base64_decode($data));
            
            $dataAr = array('mr_id'   => $result[0],
                            'name'    => $result[1],
                            'paymail' => $result[2],
                            'amt'     => $result[3]
                        );

            if (Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION')!= '')
            { 
                $session_messag =  trans(Session::get('mer_lang_file').'.MER_COMMISSION');
            }  
            else 
            { 
                $session_messag =  trans($this->MER_OUR_LANGUAGE.'.MER_COMMISSION');
            }

            $adminheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_messag);
            $adminfooter    = view('sitemerchant.includes.merchant_footer');
            $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_commission');

            $session_msg ='';

            return view('sitemerchant.commission_offline_pay')->with('adminheader', $adminheader)->with('adminfooter', $adminfooter)->with('adminleftmenus', $adminleftmenus)->with("payTrans",$data)->with('dataAr', $dataAr)->with('session_msg',$session_msg);


        }else{
            return Redirect::to('sitemerchant');
        } 
    }


    public function commission_offline_pay_submit(){

        $txn_id  = Input::get('txn_id');
        $name    = Input::get('custom');
        $txn_id  = Input::get('txn_id');
        $paidamt = Input::get('paidAmount');
        $mer_id  = Input::get('mer_id');

         $result = explode('/**/', base64_decode(Input::get('payTrans')));
            
        $mer_id     = $result[0];
        $name       = $result[1];
        $paymail    = $result[2];
        $paidamt    = $result[3];
                    

        $entry   = array(
            'com_merchant_id'       => $mer_id,
            'com_mer_name'          => $name,
            'payment_type'          => '0',
            'paidAmount'            => $paidamt,
            'transaction_id'        => $txn_id,
            'com_status'            => '1'
        );  
        
        Fund::insert_commission($entry);

        /* Update overallorder table with current need to pay commision and merchant amount */
        $getOverallOrdDetails      = Fund::overallOrder_nd_merchant($mer_id);
        if(count($getOverallOrdDetails)>0){
            
            $entry = array(            
            'commissionAmt'        => $getOverallOrdDetails->commissionAmt - $paidamt
            );
            Home::updateMerchantOverallOrder($entry,$getOverallOrdDetails->overOrd_id); 
        }
        /* Update overallorder table ends */

        if(Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT_COMPLETED_SUCCESSFULLY')!= '') 
        { 
            $session_message = trans(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT_COMPLETED_SUCCESSFULLY');
        }  
        else 
        { 
            $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_COMMISSION_PAYMENT_COMPLETED_SUCCESSFULLY');
        }
        Session::flash('commission_paid_sucess',$session_message);
        return Redirect::to('commission_paid');
    }

    /* merchant offline payment  ends */

    /* Merchant Commission Online Payment starts */
    public function commission_paypal($data)
    {
        if(Session::has("merchantid")){
        
            $settings = Home::get_settings();
                foreach ($settings as $s) { 
                }

            $result = explode('/**/', base64_decode($data));
           // print_r($result);
            $id      = $result[0];
            $name    = $result[1];
            $paymail = $result[2];
            $amt     = $result[3];
            
            require 'paypal/paypal_new/paypal.class.php';
            $p             = new paypal_class; // initiate an instance of the class
            
           // $p->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; // testing paypal url
           
            
            // setup a variable for this script (ie: 'http://www.micahcarrick.com/paypal.php')
            $this_script = url('');
           //Multivendorurlquotes
            // if there is not action variable, set the default action of 'process'
        
            $product_amount = $amt;
            $item_name      = "commission from " . $name;
            $custom         = $name;
            $item_number    = $id;
         
           // $payment_email    = $paymail; //pay to merchant
            $payment_email    = $s->ps_paypal_email; // pay to admin

            $product_quantity = 1;
            $PayPalCurrencyCode = $s->ps_curcode;
            /*print_r( $PayPalCurrencyCode);
            print_r($payment_email);exit;*/
             
            $p->add_field('business', $payment_email);
            $p->add_field('return', $this_script . '/commission_paypal_success');
            $p->add_field('cancel_return', $this_script . '/commission_paypal_cancel');
            $p->add_field('notify_url', $this_script . '/commission_paypal_ipn');
            $p->add_field('item_name', $item_name);
            $p->add_field('amount', $product_amount);
            $p->add_field('quantity', $product_quantity);
            $p->add_field('custom', $custom);
            $p->add_field('item_number', $item_number);
            $p->add_field('currency_code', $PayPalCurrencyCode);
            $p->submit_paypal_post();
        }else{
            return Redirect::to('sitemerchant');
        }
    }
    
    public function commission_paypal_success(Request $request)
    {
       /* echo "<pre>";
        print_r($_GET);
        print_r($_POST);
        print_r($_REQUEST); exit;
      print_r($request);exit;*/
        $id = $request->get('paymentId');
        $token = $request->get('token');
        $payer_id = $request->get('PayerID');
     
        $txn_id  = Input::get('txn_id');
        $email   = Input::get('payer_email');
        $name    = Input::get('custom');
        $txn_id  = Input::get('txn_id');
        $paidamt = Input::get('mc_gross');
        $mer_id  = Input::get('item_number');
        $status  = Input::get('payment_status');
        $entry   = array(
            'com_merchant_id'       => $mer_id,
            'com_mer_name'          => $name,
            'payment_type'          => '1',
            'paidAmount'            => $paidamt,
            'transaction_id'        => $txn_id,
            'com_status'            => '1'
        );  
        //Save Commission Paid 
        Fund::insert_commission($entry);

        /* Update overallorder table with current need to pay commision and merchant amount */
        $getOverallOrdDetails      = Fund::overallOrder_nd_merchant($mer_id);
        if(count($getOverallOrdDetails)>0){
            
            $entry = array(            
            'commissionAmt'        => $getOverallOrdDetails->commissionAmt - $paidamt
            );
            Home::updateMerchantOverallOrder($entry,$getOverallOrdDetails->overOrd_id); 
        }
        /* Update overallorder table ends */

        if(Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT_COMPLETED_SUCCESSFULLY')!= '') 
        { 
            $session_message = trans(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT_COMPLETED_SUCCESSFULLY');
        }  
        else 
        { 
            $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_COMMISSION_PAYMENT_COMPLETED_SUCCESSFULLY');
        }
        return Redirect::to('commission_paid')->with('session_msg', $session_message);
    }
    
    public function commission_paypal_ipn()
    {
        $status = Input::get('payment_status');
        $txn_id = Input::get('txn_id');
        $entry  = array(
            'online_payment_returnStatus' => $status
        );
        Fund::update_commission_paypal($entry, $txn_id);
    }
    
    public function commission_paypal_cancel()
    {
        if(Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT_CANCELLED')!= '') 
        { 
            $session_message = trans(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT_CANCELLED');
        }  
        else 
        { 
            $session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_COMMISSION_PAYMENT_CANCELLED');
        }
        return Redirect::to('commission_paid')->with('session_msg', $session_message);
    }


    /* Merchant Commission Online Payment ends */




}

<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//

use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;
use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\Formbusinesstype;

//------------wisitech code end -----------//
class PaymentrequestController extends Controller
{  
    public function __construct(){
        parent::__construct();
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
     
	 
    public function requestpayment(Request $request)
    {
        $main_cat_id = $request->main_cat_id;
        $merchant_id = $request->merchant_id;
        $product_id  = $request->product_id;
        $order_id    = $request->order_id;
        $amount      = $request->amount;


         $getmerchnat = DB::table('nm_merchant')->where('mer_id',$merchant_id)->first();
          $fullname=$getmerchnat->mer_fname.' '.$getmerchnat->mer_lname;

        $getservice = DB::table('nm_subscription_plan')->where('cat_id',$main_cat_id)->where('status',1)->first();
        if(isset($getservice->services_id) && $getservice->services_id==1)
        {
          $percent        = 100-$getservice->payment;
          $payable_amount = ($percent*$amount)/100;
        }
        else if(isset($getservice->services_id) && $getservice->services_id==2)
        {
          $payable_amount = $amount;
        }
        else
        {
          $payable_amount = $amount;
        }


         $send_mail_data = array(
                        'merchent_name' => $fullname,
                        'payable_amount' => $payable_amount,
                        'order_id'=> $order_id
                    );
                    # It will show these lines as error but no issue it will work fine Line no 119 - 122
                    Mail::send('emails.paymentrequestmail', $send_mail_data, function($message)
                    {   
                        $toemail='info@goldencages.com';
                        $name='Golden Cages';       
                        $session_message='new payment request';
                        $message->to($toemail, $name)->subject($session_message);
                    });





        DB::table('nm_merchant_payment_history')->insert(['merchant_id'=>$merchant_id,'main_cat_id'=>$main_cat_id,'product_id'=>$product_id,'order_id'=>$order_id,'payable_amount'=>$payable_amount,'amount'=>$amount,'request_status'=>1]);
        $array_output = array('status'=>'approved');
        echo json_encode($array_output);
         exit;
   }
}

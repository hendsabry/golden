<?php
namespace App\Http\Controllers;
use DB;
use Lang;
use Session;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Country;
use App\Customer;
use App\City;
use App\Category;
use App\MerchantServiceTime;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class MerchantServiceTimeController extends Controller
{
    public function __construct(){
        parent::__construct();
       
        // set admin Panel language
        $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleMerchant();
                    return $next($request);
    });
       
    }
    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */
/*Add Service Time*/    
    public function mer_add_service_time()
    {
		if(Lang::has(Session::get('mer_lang_file').'.BACK_SETTINGS')!= '') 
		{
			$session_message = trans(Session::get('mer_lang_file').'.BACK_SETTINGS');
		}
		else 
		{
			$session_message =  trans($this->MER_OUR_LANGUAGE.'.BACK_SETTINGS');
		}
        $adminheader    = view('sitemerchant.includes.merchant_header')->with("routemenu",$session_message);
        $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_settings');
        $adminfooter    = view('sitemerchant.includes.merchant_footer');
        
        return view('sitemerchant.add_service_time')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter);
    }
/*mange service time function*/
    public function mer_manage_service_time()
    {
		if(Lang::has(Session::get('mer_lang_file').'.BACK_SETTINGS')!= '') 
		{
			$session_message = trans(Session::get('mer_lang_file').'.BACK_SETTINGS');
		}
		else 
		{
			$session_message =  trans($this->MER_OUR_LANGUAGE.'.BACK_SETTINGS');
		}
        $adminheader    = view('sitemerchant.includes.merchant_header')->with("routemenu",$session_message);
            $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_settings');
            $adminfooter    = view('sitemerchant.includes.merchant_footer');
        
        $Service_Time_Details = MerchantServiceTime::view_service_time_details();
		
		
        
        return view('sitemerchant.manage_service_time')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('Service_Time_Details', $Service_Time_Details);
    }
	
/*Edit Service Time Function*/
    public function mer_edit_service_time($id)
    {
		if(Lang::has(Session::get('mer_lang_file').'.BACK_SETTINGS')!= '') 
		{
			$session_message = trans(Session::get('mer_lang_file').'.BACK_SETTINGS');
		}
		else 
		{
			$session_message =  trans($this->MER_OUR_LANGUAGE.'.BACK_SETTINGS');
		}
        $adminheader    = view('sitemerchant.includes.merchant_header')->with("routemenu", $session_message);
            $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_settings');
            $adminfooter    = view('sitemerchant.includes.merchant_footer');
        
        $Get_Service_Time = MerchantServiceTime::showindividual_service_time_detail($id);
       
        return view('sitemerchant.edit_service_time')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('Get_Service_Time', $Get_Service_Time)->with('id', $id);
    }
/*Delete Service Time*/    
    public function mer_delete_service_time_admin($id)
    {
		if(Lang::has(Session::get('mer_lang_file').'.BACK_SETTINGS')!= '') 
		{
			$session_message = trans(Session::get('mer_lang_file').'.BACK_SETTINGS');
		}
		else 
		{
			$session_message =  trans($this->MER_OUR_LANGUAGE.'.BACK_SETTINGS');
		}
        $adminheader    = view('sitemerchant.includes.merchant_header')->with("routemenu",$session_message);
            $adminleftmenus = view('sitemerchant.includes.merchant_left_menu_settings');
            $adminfooter    = view('sitemerchant.includes.merchant_footer');
        
        $affected = MerchantServiceTime::delete_service_time_detail($id);
		if(Lang::has(Session::get('mer_lang_file').'.BACK_RECORD_DELETED_SUCCESSFULLY')!= '') 
		{
			$session_message = trans(Session::get('mer_lang_file').'.BACK_RECORD_DELETED_SUCCESSFULLY');
		}
		else 
		{
			$session_message =  trans($this->MER_OUR_LANGUAGE.'.BACK_RECORD_DELETED_SUCCESSFULLY');
		}
        return Redirect::to('mer_manage_service_time')->with('delete_result', $session_message);
    }
/*Change Status Function*/    
    public function mer_update_status_service_time($id, $status)
    {
		if(Lang::has(Session::get('mer_lang_file').'.BACK_RECORD_DELETED_SUCCESSFULLY')!= '') 
		{
			$session_message = trans(Session::get('mer_lang_file').'.BACK_RECORD_DELETED_SUCCESSFULLY');
		}
		else 
		{
			$session_message =  trans($this->MER_OUR_LANGUAGE.'.BACK_RECORD_DELETED_SUCCESSFULLY');
		}
        $return = MerchantServiceTime::update_status_service_time($id, $status);
        return Redirect::to('mer_manage_service_time')->with('updated_result', $session_message);
    }
/*Add Service Time Submit*/	
    public function mer_add_service_time_submit()
    {
        
        $data = Input::except(array(
            '_token'
        ));
        $rule = array(
            'service_time' => 'required',
            'Service_time_status' => 'required'
        );
        
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            return Redirect::to('mer_add_service_time')->withErrors($validator->messages())->withInput();
            
        } else {
            $service_timing = Input::get('service_time');
            $service_time = str_replace(' ', '', $service_timing);
            $entry = array(
                'service_time' => $service_time,
                'Service_time_status' => Input::get('Service_time_status')
                
            );
            
            $return = MerchantServiceTime::save_service_time($entry);
			if(Lang::has(Session::get('mer_lang_file').'.BACK_RECORD_INSERTED_SUCCESSFULLY')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.BACK_RECORD_INSERTED_SUCCESSFULLY');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.BACK_RECORD_INSERTED_SUCCESSFULLY');
			}
			return Redirect::to('mer_manage_service_time')->with('insert_result',$session_message);
			
        }
    }
/*Update Service Time*/   
    public function mer_update_service_time_submit()
    {
     
        $data      = Input::except(array(
            '_token'
        ));
        $rule      = array(
            'service_time' => 'required',
            'Service_time_status' => 'required'
            
        );
        $id        = Input::get('id');
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            return Redirect::to('mer_add_service_time')->withErrors($validator->messages())->withInput();
            
        } else {
            $service_timing = Input::get('service_time');
            $service_time = str_replace(' ', '', $service_timing);

            $entry = array(
                'service_time' => $service_time,
                'Service_time_status' => Input::get('Service_time_status')
                
            );
            
            $return = MerchantServiceTime::update_Service_Time_Details($id, $entry);
			if(Lang::has(Session::get('mer_lang_file').'.BACK_RECORD_UPDATED_SUCCESSFULLY')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.BACK_RECORD_UPDATED_SUCCESSFULLY');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.BACK_RECORD_UPDATED_SUCCESSFULLY');
			}
            return Redirect::to('mer_manage_service_time')->with('updated_result', $session_message);
        }
    }
}

?>

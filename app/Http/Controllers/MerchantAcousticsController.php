<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Models;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon;
use File;
use App\Reviewrating;
use App\Categorylist;
use Intervention\Image\ImageManagerStatic as Image; 
use Auth;
use Lang;
use App\ProductsMerchant;
use App\NmServicesAttribute;
use App\NmSize;
use App\HallOffer;
use App\NmProductGallery;
class MerchantAcousticsController extends Controller
{
      public function __construct(){
            parent::__construct();
            // set admin Panel language
            $this->middleware(function ($request, $next) {
                        $this->setLanguageLocaleMerchant();
                        Session::get('mer_lang_code');
                        return $next($request);
           });
     }
    
 
 

// Start For Acoustics Info
    public function Acoustics(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'acoustics')->where('category_id', $id)->first();
 
                return view('sitemerchant.acoustics.acoustics-info', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
   
 // Start For Acoustics Photo Video
    public function acousticsPhotoVideo(Request $request)
     {
        if (Session::has('merchantid')) 
             {  

            $mer_id   = Session::get('merchantid');
            $id = $request->id;
            $hid = $request->hid;
            $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
            $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 

            $getDbC = DB::table('nm_music_gallery')->where('vendor_id', $mer_id)->where('music_id', $hid)->get();

            $getVideos = DB::table('nm_music')->where('vendor_id', $mer_id)->where('id', $hid)->first();
            return view('sitemerchant.acoustics.acoustics-photo-video', compact('merchantheader','merchantfooter','id','getDbC','getVideos')); 


 
                  
            } else {
                 return Redirect::to('sitemerchant');
            }
     }  
   
 	// for displaying comment and reviews
	public function acousticsReviewsRatings (Request $request)
	{
		 $getPagelimit = config('app.paginate');
		if (Session::has('merchantid')) 
		{
		$merid  = Session::get('merchantid'); 
    $Cid = $request->Cid;
		$this->setLanguageLocaleMerchant();
		$mer_id              = Session::get('merchantid');             
		$merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
		$merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
		$reviewrating = Reviewrating::where('vandor_id',$mer_id)->where('shop_id',$Cid)->orderBy('created_at','desc')->paginate($getPagelimit)->appends(request()->query());
		return view('sitemerchant.acoustics.acoustics-comments-review', compact('merchantheader','merchantfooter','reviewrating'));       
		} else {
		return Redirect::to('sitemerchant');
		}


	}   
   	// For Displaying  Quote Requested 
	public function acousticsQuoteRequested(Request $request)
	   {    
            if (Session::has('merchantid')) 
            {
                  $mer_id   = Session::get('merchantid');
                  $id = $request->id;
                  $hid = $request->hid;
                  $Cid = $request->Cid;
                  $getPagelimit = config('app.paginate');
                  $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                  $merchantfooter     = view('sitemerchant.includes.merchant_footer');
                  $getusers = DB::table('nm_services_inquiry')
            ->join('nm_customer', 'nm_services_inquiry.user_id', '=', 'nm_customer.cus_id')
            ->select('nm_customer.cus_name','nm_customer.email','nm_customer.cus_phone','nm_services_inquiry.created_at','nm_services_inquiry.language_type','nm_services_inquiry.id','nm_services_inquiry.status','nm_services_inquiry.the_groooms_name','nm_services_inquiry.language_type','nm_services_inquiry.id','nm_services_inquiry.status','nm_services_inquiry.singer_name')
            ->where('nm_services_inquiry.merchant_id', $mer_id)
             ->where('nm_services_inquiry.quote_for','recording')->orderBy('nm_services_inquiry.id','DESC');
            
            if(isset($_REQUEST['acceptance']) && $_REQUEST['acceptance']!='')
            {
            $srchs = $_REQUEST['acceptance'];
            $getusers =   $getusers ->where('nm_services_inquiry.status', '=', $srchs);
            }


            if(isset($_REQUEST['search']) && $_REQUEST['search']!='')
            {
                $srch = $_REQUEST['search'];
            $getusers =   $getusers ->where('nm_customer.cus_name','like', '%'.$srch.'%');
            }

           $getusers = $getusers->paginate($getPagelimit)->appends(request()->query());

            return view('sitemerchant.acoustics.acoustics-quoted-requested-list', compact('merchantheader','merchantfooter','id','hid','Cid','getusers'));     
	       
		} else {
		return Redirect::to('sitemerchant');
		}


	}



public function addsingerinfo(Request $request)
{

 if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
               $Cid= $request->Cid;
         
                $singer_name = $request->singer_name;
                $singer_name_ar = $request->singer_name_ar;                
                $singer_city = $request->singer_city;
                $about_singer = $request->about_singer;
                $about_singer_ar = $request->about_singer_ar;
                $tnc = $request->tnc;
                $singer_image = $request->singer_image;
                $city = $request->singer_city;
                $address = $request->address;
                $addressar = $request->address_ar;
                $mc_status = $request->mc_status;

                if($file=$request->file('singer_image')){  

                $extension = $file->getClientOriginalExtension();
                $Adressimgname = rand(11111,99999).'_'.time().'.'.$extension;               
                $imageRealPath  =   $file->getRealPath();
                $thumbName      =   'thumb_'. $Adressimgname;       
                $img = Image::make($imageRealPath); // use this if you want facade style code
                  
                $thumb_width = 150;
                list($width,$height) = getimagesize($imageRealPath);
                $thumb_height = ($thumb_width/$width) * $height;
                $img->resize($thumb_width,$thumb_height);
 
                $img->save('singer' . '/'. $thumbName);
                $file->move('singer/', $Adressimgname);
                $thumbNameurl = url('').'/singer/'.$thumbName;
                }
                else
                {
               $thumbNameurl =  $request->singer_image_url;
                }


                if($tnc=$request->file('tnc')){  
                    $tncname =   $tnc->getClientOriginalName();  
                $extension = $tnc->getClientOriginalExtension();
                $tncs = rand(11111,99999).'_'.time().'.'.$extension;              
                $tnc->move('singer/certificate/', $tncs);
                $tncsurl = url('').'/singer/certificate/'.$tncs;
                }
                else
                {
               $tncsurl =  $request->tnc_url;
               $tncname = $request->tnc_name;
                }

              if($tnc_ar=$request->file('tnc_ar')){  
                    $tncname_ar =   $tnc_ar->getClientOriginalName();  
                $extension = $tnc_ar->getClientOriginalExtension();
                $tncs_ar = rand(11111,99999).'_'.time().'.'.$extension;              
                $tnc_ar->move('singer/certificate/', $tncs_ar);
                $tncsurl_ar = url('').'/singer/certificate/'.$tncs_ar;
                }
                else
                {
               $tncsurl_ar =  $request->tnc_url_ar;
               $tncname_ar = $request->tnc_name_ar;
                }
             if($file=$request->file('address_image')){  

                $extension = $file->getClientOriginalExtension();
                $Adressimgname = rand(11111,99999).'_'.time().'.'.$extension;               
                $imageRealPath  =   $file->getRealPath();
                $thumbName      =   'thumb_'. $Adressimgname;       
                $img = Image::make($imageRealPath); // use this if you want facade style code
                

                $thumb_width = 150;
                list($width,$height) = getimagesize($imageRealPath);
                $thumb_height = ($thumb_width/$width) * $height;
                $img->resize($thumb_width,$thumb_height);

                $img->save('singer' . '/'. $thumbName);
                $file->move('singer/', $Adressimgname);
                $thumbNameurls = url('').'/singer/'.$thumbName;
                }
                else
                {
               $thumbNameurls =  $request->address_image_name;
                }
              $address = $request->address;
              $google_map_url = $request->google_map_url;

              $longitude = $request->longitude;
              $latitude = $request->latitude;

                $getC = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_id', $id)->count();
                if($getC < 1){

              $Q =  DB::table('nm_music')->insert([
                ['category_id' => $id, 'vendor_id' => $mer_id,'name_ar' => $singer_name_ar,'name' => $singer_name,'about' => $about_singer,'about_ar'=>$about_singer_ar,'category_type'=>'acoustics','status'=>$mc_status,'image'=>$thumbNameurl,'terms_conditions'=>$tncsurl,'city_id' => $city,'terms_condition_name_ar' => $tncname_ar,'terms_condition_name' => $tncname,'terms_conditions_ar' => $tncsurl_ar,'address' => $address,'address_ar' => $addressar,'google_map_url' => $google_map_url,'longitude' => $longitude,'latitude' => $latitude,'address_image' => $thumbNameurls],
                ]); 
                   $hid = DB::getPdo()->lastInsertId();


 DB::table('nm_category')->insert([
                [ 'vendor_id' => $mer_id, 'parent_id'=>$id, 'mc_name_ar' => $singer_name_ar,'mc_name' => $singer_name,'mc_discription' => $about_singer,'mc_discription_ar'=>$about_singer_ar,'mc_status'=>$mc_status,'mc_img'=>$thumbNameurl,'terms_conditions'=>$tncsurl,'city_id' => $city,'terms_condition_name_ar' => $tncname_ar,'terms_condition_name' => $tncname,'terms_conditions_ar' => $tncname_ar,'address' => $address,'address_ar' => $addressar,'google_map_address' => $google_map_url,'longitude' => $longitude,'latitude' => $latitude,'address_image' => $thumbNameurls],
                ]); 

                $Cid = DB::getPdo()->lastInsertId();

                } 
                else
                {

                 
            DB::table('nm_music')->where('category_type','acoustics')->where('vendor_id',$mer_id)->where('category_id',$id)->update( ['name_ar'=>  $singer_name_ar,'name' => $singer_name,'about' => $about_singer,'about_ar'=>$about_singer_ar,'image'=>$thumbNameurl,'terms_conditions'=>$tncsurl,'terms_conditions_ar'=>$tncsurl_ar,'city_id' => $city,'status'=>$mc_status,'terms_condition_name' => $tncname,'terms_condition_name_ar' => $tncname_ar,'address' => $address,'address_ar' =>$addressar,'google_map_url' => $google_map_url,'longitude' => $longitude,'latitude' => $latitude,'address_image' => $thumbNameurls]); 
         
 
            DB::table('nm_category')->where('vendor_id',$mer_id)->where('mc_id',$Cid)->update( ['mc_name_ar'=>  $singer_name_ar,'mc_name' => $singer_name,'mc_discription' => $about_singer,'mc_discription_ar'=>$about_singer_ar,'mc_img'=>$thumbNameurl,'terms_conditions'=>$tncsurl,'terms_conditions_ar'=>$tncsurl_ar,'city_id' => $city,'terms_condition_name' => $tncname,'terms_condition_name_ar' => $tncname_ar,'address' => $address,'address_ar' =>$addressar,'google_map_address' => $google_map_url,'longitude' => $longitude,'latitude' => $latitude,'mc_status' => $mc_status,'address_image' => $thumbNameurls]); 

               }
                
 


                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();
 


 

                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تمت إضافة الخدمة");
                    }
                    else
                    {
                    Session::flash('message', "Record successfully saved");
                    }
                    // language for display message // 

            return redirect()->route('acoustics',['id' => $id,'hid' => $hid,'Cid'=> $Cid]); 
                      
            } else {
                 return Redirect::to('sitemerchant');
            }

}


 

    // for displaying Quoted Requested View 
 public function acousticsQuotedRequestedView(Request $request)
     {
        if (Session::has('merchantid')) 
             {
              $mer_id   = Session::get('merchantid');
              $id = $request->id;
              $hid = $request->hid;
              $itmid = $request->itmid;

              $autoid = $request->autoid;

              $getCities = DB::table('nm_city')->where('ci_status',1)->orderBy('ci_name','ASC')->get();
              $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
              $merchantfooter     = view('sitemerchant.includes.merchant_footer');
              $getquote  = DB::table('nm_vendor_comment')->where('id',$autoid)->first();
              $getusers = DB::table('nm_services_inquiry')
              ->join('nm_customer', 'nm_services_inquiry.user_id', '=', 'nm_customer.cus_id')
              ->select('nm_customer.cus_name','nm_customer.email','nm_customer.cus_phone','nm_services_inquiry.created_at','nm_services_inquiry.location','nm_services_inquiry.city_id','nm_services_inquiry.hall','nm_services_inquiry.occasion_type','nm_services_inquiry.duration','nm_services_inquiry.singer_name','nm_services_inquiry.time','nm_services_inquiry.date','nm_services_inquiry.language_type','nm_services_inquiry.status','nm_services_inquiry.user_id','nm_services_inquiry.the_groooms_name','nm_services_inquiry.song_name','nm_services_inquiry.music_type','nm_services_inquiry.id')
              ->where('nm_services_inquiry.id',  $itmid)
              ->where('nm_services_inquiry.merchant_id',$mer_id)
              ->first();
              return view('sitemerchant.acoustics.acoustics-quoted-requested-view', compact('merchantheader','merchantfooter','id','hid','itmid','getusers','getCities','autoid','getquote'));       
            } else {
                return Redirect::to('sitemerchant');
            }
     }    
   
    public function storeCommentBand(Request $request)
    {
            $enqueryid    = $request->enqueryid;
            $comment      = $request->comments;
            $catid        = $request->catid;
            $hid          = $request->hid;
            $itmid        = $request->itmid;
            $user_id      = $request->userid;
            $price        = $request->price;
            $mer_id       = Session::get('merchantid');
            $autoid       = $request->autoid;
            $languagetype = $request->languagetype;            
            $email        = $request->email;
             if($autoid!='')
             {
               DB::table('nm_services_inquiry')->where('id',$itmid)->update([
                'status' => '2'
                ]);
               $Q =  DB::table('nm_vendor_comment')->where('id',$autoid)->update([
                'cat_id' => $catid, 
                'vendor_id' => $mer_id,
                'user_id' => $user_id,
                'enq_id' => $itmid,
                'comment' => $comment,
                'price' => $price,
                'type' => 'singer',
                'status' => '2'
                ]);

                 $email = $email;
                 $username = $request->customername;
                 $comment = $request->comments;
                 $price = $request->price;
                Mail::send('emails.vendor-singer-reply', ['username' => $username,'languagetype' => $languagetype,'comment'=>$comment,'price'=>$price], function ($m) use ($username,$comment,$price,$email,$languagetype) {
                 $m->from($email, 'Goldencages');
                 $m->to($email, $username)->subject('Recording Pricing Quote');
         
                  });
      

            


             } else {

             DB::table('nm_services_inquiry')->where('id',$itmid)->update([
                'status' => '2'
                ]);

             $Q =  DB::table('nm_vendor_comment')->insert([
                'cat_id' => $catid, 
                'vendor_id' => $mer_id,
                'user_id' => $user_id,
                'enq_id' => $itmid,
                'comment' => $comment,
                'price' => $price,
                'type' => 'singer',
                'status' => '2'
                ]);


                 $email = $email;
                 $username = $request->customername;
                 $comment = $request->comments;
                 $price = $request->price;

                 

                Mail::send('emails.vendor-sound-reply', ['username' => $username,'languagetype' => $languagetype,'comment'=>$comment,'price'=>$price], function ($m) use ($username,$comment,$price,$email,$languagetype) {
                 $m->from($email, 'Goldencages');
                 $m->to($email, $username)->subject('Recording Pricing Quote');
         
                  });
                 
         }
            //Mail Content


         //Mail Implementations Code

            if($autoid =='') {
           
             $autoid = DB::getPdo()->lastInsertId();

             
        }



                  // language for display message //
                if (\Config::get('app.locale') == 'ar'){
                Session::flash('message', "ارسل التعليق بنجاح");
                }
                else
                {
                Session::flash('message', "Comment successfully sent");
                }
                // language for display message //
               return redirect()->route('acoustics-quoted-requested-view',['id' => $catid,'hid' => $hid,'itmid' =>$itmid,'autoid'=>$autoid]); 
          

      }

    public function storeCommentacoustics(Request $request)
    {
            $enqueryid    = $request->enqueryid;
            $comment      = $request->comments;
            $catid        = $request->catid;
            $hid          = $request->hid;
            $itmid        = $request->itmid;
            $user_id      = $request->userid;
            $price        = $request->price;
            $mer_id       = Session::get('merchantid');
            $autoid       = $request->autoid;
            $languagetype = $request->languagetype;            
            $email        = $request->email;
             if($autoid!='')
             {
               DB::table('nm_services_inquiry')->where('id',$itmid)->update([
                'status' => '2'
                ]);
               $Q =  DB::table('nm_vendor_comment')->where('id',$autoid)->update([
                'cat_id' => $catid, 
                'vendor_id' => $mer_id,
                'user_id' => $user_id,
                'enq_id' => $itmid,
                'comment' => $comment,
                'price' => $price,
                'type' => 'recording',
                'status' => '2'
                ]);

                 $email = $email;
                 $username = $request->customername;
                 $comment = $request->comments;
                 $price = $request->price;
                Mail::send('emails.vendor-acoustic-reply', ['username' => $username,'languagetype' => $languagetype,'comment'=>$comment,'price'=>$price], function ($m) use ($username,$comment,$price,$email,$languagetype) {
                 $m->from($email, 'Goldencages');
                 $m->to($email, $username)->subject('Recording Pricing Quote');
         
                  });
      

            


             } else {

             DB::table('nm_services_inquiry')->where('id',$itmid)->update([
                'status' => '2'
                ]);

             $Q =  DB::table('nm_vendor_comment')->insert([
                'cat_id' => $catid, 
                'vendor_id' => $mer_id,
                'user_id' => $user_id,
                'enq_id' => $itmid,
                'comment' => $comment,
                'price' => $price,
                'type' => 'recording',
                'status' => '2'
                ]);


                 $email = $email;
                 $username = $request->customername;
                 $comment = $request->comments;
                 $price = $request->price;

                 

                Mail::send('emails.vendor-acoustic-reply', ['username' => $username,'languagetype' => $languagetype,'comment'=>$comment,'price'=>$price], function ($m) use ($username,$comment,$price,$email,$languagetype) {
                 $m->from($email, 'Goldencages');
                 $m->to($email, $username)->subject('Recording Pricing Quote');
         
                  });
                 
         }
            //Mail Content


         //Mail Implementations Code

            if($autoid =='') {
           
             $autoid = DB::getPdo()->lastInsertId();

             
        }



                  // language for display message //
                if (\Config::get('app.locale') == 'ar'){
                Session::flash('message', "ارسل التعليق بنجاح");
                }
                else
                {
                Session::flash('message', "Comment successfully sent");
                }
                // language for display message //
               return redirect()->route('acoustics-quoted-requested-view',['id' => $catid,'hid' => $hid,'itmid' =>$itmid,'autoid'=>$autoid]); 
          

      }

     //Acoustics ProductsMerchant
     
       public function productItem(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $Cid = $request->Cid;
                $catname = $request->catid;
                $status = $request->status;
                $search = $request->search;
                $maincatid =  $request->maincatid;
                $getPagelimit = config('app.paginate');
                  $productdata        = ProductsMerchant::where('pro_mc_id',$Cid)->where('pro_mr_id',$mer_id);
                
                if($status != '')
                {
                $productdata = $productdata->where('pro_status',$status); 
                }

               if($search !='')
                {
                  $mc_name='pro_title';
                  $mer_selected_lang_code = Session::get('mer_lang_code'); 
                  if($mer_selected_lang_code !='en')
                  {
                  $mc_name= 'pro_title'.$mer_selected_lang_code;
                  }                                         
                  $productdata = $productdata->where($mc_name,'LIKE',"%{$search}%");                     
                } 
                 


                $productdata = $productdata->orderBy('pro_id','desc')->paginate($getPagelimit)->appends(request()->query());
                 $attrcat            = NmServicesAttribute::where('vendor_id',$mer_id)->where('services_id',$hid)->get();

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
 
                return view('sitemerchant.acoustics.acoustics-product', compact('merchantheader','merchantfooter','id','productdata','status','search','attrcat','catname'));       
            } else {
                 return Redirect::to('sitemerchant');
            }

     }  
    public function productAddItem(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $autoid = $request->autoid;

                 $Cid = $request->Cid;
                 $fetchfirstdata = ProductsMerchant::where('pro_id',$autoid)->where('pro_mr_id',$mer_id)->first();
                   


                        $getRentP = DB::table('nm_product_option_value')->where('vandor_id', $mer_id)->where('product_id', $autoid)->count(); 

                      $getRentPS = DB::table('nm_product_option_value')->where('vandor_id', $mer_id)->where('product_id', $autoid)->where('product_option_id','20')->first(); 

                       $discount = DB::table('nm_product_option_value')->where('vandor_id', $mer_id)->where('product_id', $autoid)->where('product_option_id','36')->first(); 



                    $getQuantity = DB::table('nm_product_option_value')->where('vandor_id', $mer_id)->where('product_id', $autoid)->where('product_option_id','32')->first();
                  

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');                 
              $productGallery = NmProductGallery::where('vendor_id',$mer_id)->where('product_id',$autoid)->get();  
                return view('sitemerchant.acoustics.acoustics-add-product', compact('merchantheader','merchantfooter','id','hid','fetchfirstdata','autoid','getRentP','getQuantity','Cid','getRentPS','discount','productGallery'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
         }    
     
     
   //Store dressesitem 
      public function storeAccousticProduct(Request $request)

            {
                $merid  = Session::get('merchantid');
                $sid = $request->hid;

                $id = $request->parentid;
                    $Cid = $request->Cid;
                $autoid = $request->autoid;
               if($autoid){
                  $savebranch =ProductsMerchant::where('pro_id',$autoid)->first();
                } else { 
                 $savebranch = new ProductsMerchant;
               }
                if($request->file('stor_img')){ 
                  $file = $request->stor_img;  
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                 $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 272;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('uploadimage/dresses' . '/'. $thumbName);
                  $file->move('uploadimage/dresses/', $Image);
                  $savebranch->pro_Img = url('').'/uploadimage/dresses/'.$thumbName;
                }
                  $savebranch ->pro_mr_id = $merid;
                  $savebranch ->pro_mc_id = $Cid;
                  $savebranch ->pro_title = $request->name;
                  $savebranch ->pro_title_ar = $request->name_ar;
                  $savebranch ->pro_price = $request->price;
                  $savebranch ->pro_discount_percentage = $request->discount;
                  $savebranch ->pro_qty = $request->quantity;
                  $savebranch ->pro_desc = $request->description;
                  $savebranch ->pro_desc_ar = $request->description_ar;
                  $savebranch ->pro_desc_ar = $request->description_ar;

                  $pricesell = $request->price;
                  $discount =  $request->discount;
                    if($discount!='' && $discount!='0') {
                     $discountprice =   $pricesell-($pricesell * ($discount/100));
                    } else {
                       $discountprice = 0;

                     } 

                   $savebranch ->pro_disprice = $discountprice;
                   $savebranch ->notes = $request->note;

                  $savebranch ->notes_ar = $request->note_ar;

                  $savebranch->pro_status  = 1;
                  $savebranch ->option_id = '18';
                  $avilable_on_rent =  $request->avilable_on_rent;
                
                  if($avilable_on_rent==1)
                  {

                  
                   $savebranch ->Insuranceamount = $request->insuranceamount;
                  }
                  else
                  {
                    $savebranch ->Insuranceamount ='';
                  }

                  $savebranch->save();


                  $getProID = $savebranch->pro_id; 
                  $size = $request->size;
                  $avilable_on_rent =  $request->avilable_on_rent;
                  if($avilable_on_rent==1)
                  {
                  $rentPrice =  $request->rent_price;
                  $discountrents = $request->discountrent;
 
                   if($discountrents!='' && $discountrents!='0') {
                     $discountprice =   $rentPrice-($rentPrice * ($discountrents/100));
                    } else {
                       $discountprice = 0;

                     }

                  $rentQuantity =  $request->quantity_for_rent;
                  }
                  else
                  {
                   $rentPrice =  '';

                   $rentQuantity =  ''; 
                  }
                   DB::table('nm_product_option_value')->where('vandor_id',$merid)->where('product_id',$getProID)->delete();
                   if($avilable_on_rent==1)
                  {
                  DB::table('nm_product_option_value')->insert(['vandor_id' => $merid,'product_id' => $getProID, 'product_option_id' =>'20', 'option_title' => 'Rent','price'=>$rentPrice,'discount'=>$discountrents, 'option_title_ar'=> 'تأجير']); 

                   //Quantity
                  DB::table('nm_product_option_value')->insert(['vandor_id' => $merid,'product_id' => $getProID, 'product_option_id' =>'32', 'option_title' => 'Quantity in stock','value'=>$rentQuantity,'discount'=>$discountrents, 'option_title_ar'=> 'الكميه في المخزن']);

                   //Discount price
                  DB::table('nm_product_option_value')->insert(['vandor_id' => $merid,'product_id' => $getProID, 'product_option_id' =>'36', 'option_title' => 'Discount Price','discount_price'=>$discountprice,'discount'=>$discountrents, 'option_title_ar'=> 'سعر الخصم']);

              
             
                }

                

                
// FOR PRODUCT MULTIPLE IMAGES UPLOAD //
        $files=$request->file('image');
        $newFiles = array();
        $merid  = Session::get('merchantid');
        if($files=$request->file('image')){
        foreach($files as $key=>$val){
        array_push($newFiles, $key);
        }

        $privius=$request->privius;
        $newFilesMatch = array();
        if(isset($privius) && $privius!='')
        {
        foreach($privius as $key=>$val){
        array_push($newFilesMatch, $key);
        }  
        }

        $CheckInfo = array_intersect($newFiles, $newFilesMatch);
        if(isset($privius) && $privius!='')
        {
        foreach($privius as $key=>$val){
        if(in_array($key, $CheckInfo))
        {
        DB::table('nm_product_gallery')->where('id',$val)->where('vendor_id',$merid)->delete(); 
        }
        }
        } 
        foreach($files as $file){
        $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111,99999).'_'.time().'.'.$extension;
        $imageRealPath  =   $file->getRealPath();
        $thumbName      =   'thumb_'. $fileName;       
        $img = Image::make($imageRealPath); // use this if you want facade style code
        $thumb_width = 272;
        list($width,$height) = getimagesize($imageRealPath);
        $thumb_height = ($thumb_width/$width) * $height;
        $img->resize($thumb_width,$thumb_height);                  
        $img->save('uploadimage/dresses' . '/'. $thumbName);
        $file->move('uploadimage/dresses/', $fileName);
        $shop_Img = url('/').'/uploadimage/dresses/'.$thumbName; 
        /*Insert data to db*/
        DB::table('nm_product_gallery')->insert( [
        'image'=>   $shop_Img,
        'product_id' =>$getProID,
        'status' => 1,
        'vendor_id' => $merid,
        ]);
        }  
        } 


 
// FOR PRODUCT MULTIPLE IMAGES UPLOAD END //


            
                  if (\Config::get('app.locale') == 'ar'){
                  Session::flash('message', "تم حفظ المنتج بنجاح ");
                  }
                  else
                  {
                  Session::flash('message', "Product successfully saved");
                  }

                 return redirect()->route('product-item',['id' => $id,'sid' =>$sid,'Cid' =>$Cid]);
                }   
         

    //End dressesitem   


     public function acousticOrder(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $subproducttype     = 'acoustic';
                $mer_id             = Session::get('merchantid');
                $id                 = $request->id;
                $hid                = $request->hid;
                $sid                = $request->sid;
                $Cid                = $request->Cid;
                $searchkeyword      = $request->searchkeyword;
                $date_to            = $request->date_to;
                $End_Date           = $request->from_to;
                $order_days         = $request->order_days;
                $status             = $request->status;                
                $serachfirstfrm     = $request->serachfirstfrm;
                $getorderedproducts = DB::table('nm_order_product')->whereIn('product_type',['recording','music'])->whereIn('product_sub_type',['recording','acoustic']);

                if($searchkeyword!='')
                {
                   $getorderedproducts = $getorderedproducts->where('order_id',$searchkeyword);
                }

                if($status!='')
                {
                   $getorderedproducts = $getorderedproducts->where('status',$status);
                } 

                if($date_to!='' && $End_Date!='')
                {
                   $getorderedproducts = $getorderedproducts->whereDate('created_at','>=', $date_to)->whereDate('created_at', '<=', $End_Date);
                } 

                $getorderedproducts = $getorderedproducts->where('merchant_id',$mer_id)->groupBy('order_id')->orderBy('created_at','DESC')->selectRaw('*,sum(nm_order_product.total_price) as sum,sum(nm_order_product.insurance_amount) as insurance_amou')->get();
                  

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.acoustics.acoustics-order', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid','sid','getorderedproducts','mer_id','Cid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }   
      
     public function refundInsuranceAmount(Request $request)
     {
        $input         = $request->all();
        $insuranceid   = $input['insuranceid']; 
        $refund_amount = $input['refund_amount'];
        $note          = $input['note'];
        $getdataorder  = DB::table('nm_order_product')->where('id',$insuranceid)->first();
        if($getdataorder->insurance_amount >= $refund_amount)
        {
          DB::table('nm_order_product')->where('id',$insuranceid)->update(['refund_amount'=>$refund_amount,'refund_message'=>$note]);
          if(\Config::get('app.locale') == 'ar')
          {
            Session::flash('message',"تم تحديث طلب رد الأموال بنجاح");
          }
          else
          {
            Session::flash('message', "Refund request succesfully updated");
          }
        }
        else
        {
           if(\Config::get('app.locale') == 'ar')
           {
            Session::flash('message',"مبلغ الاسترداد خاطئ");
           }
           else
           {
            Session::flash('message', "Refund amount wrong");
           }
        }
        return Redirect::back();
     }

///////////// update product status ////////////
       function getproductstatus(Request $request){
        $orderproductid=$request->orderproductid;
        $orderstatus=$request->orderstatus;
        $getInfo = DB::table('nm_order_product')->where('id', $orderproductid)->first();
        $merID = $getInfo->merchant_id;
        $order_id = $getInfo->order_id;
        $shopid=$getInfo->shop_id;
        
        DB::table('nm_order_product')->where('merchant_id', $merID)->where('order_id', $order_id)->where('shop_id', $shopid)->update(['status' => $orderstatus]);



        $array_output = array('status'    => 'approved', 'orderstatus'=>$orderstatus);
        echo json_encode($array_output);
         exit;
       }
////////////////end produc status///////////////////

   public function getorderdetail(Request $request)
   {
     if(Session::has('merchantid')) 
     {
       $mer_id         = Session::get('merchantid');
       $id             = $request->id;
       $hid            = $request->hid;
       $opid           = $request->Cid;
       $cusid          = $request->cusid;
       $oid            = $request->oid; 
        $Cid      = $request->opid;                 
       $merchantheader = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
       $merchantfooter = view('sitemerchant.includes.merchant_footer'); 
       
       $productdata = DB::table('nm_order_product')->whereIn('product_type',['recording','music'])->whereIn('product_sub_type',['recording','acoustic'])->where('order_id',$oid)->where('cus_id',$cusid)->where('order_id',$oid)->orderBy('created_at','DESC')->get();

    foreach($productdata as $value)
         {
             

          $getShippingC = DB::table('nm_shipping')->where('ship_order_id',$oid)->where('cart_sub_type','acoustic')->count();
          $value->shippingMethod = 'N/A';
          $value->shippingPrice = 'N/A';
          if($getShippingC >=1)
          { 
          $getShipping = DB::table('nm_shipping')->where('ship_order_id',$oid)->where('cart_sub_type','acoustic')->first();
          $shipping_type = $getShipping->shipping_type;
          if($shipping_type == 1){ $shipping_type = 'Aramex';}else { $shipping_type = 'Pick Logistics'; }
          $shipping_price =  $getShipping->shipping_price;
          $value->shippingMethod = $shipping_type;
          $value->shippingPrice = $shipping_price;
          }


         }



        return view('sitemerchant.acoustics.orderdetail', compact('merchantheader','merchantfooter','id','hid','opid','productdata','cusid','oid'));       
    } else {
         return Redirect::to('sitemerchant');
    }
  }


      //Tailor offer     



   public function acousticOffer(Request $request)
       {
        if (Session::has('merchantid')) 
             {
                
               if (Session::has('merchantid')) 
             {   $getPagelimit = config('app.paginate');
                  $id = $request->id;
                  $sid = $request->hid;
                  $autoid = $request->autoid;
                  $Cid=$request->Cid;
                  $status = $request->status;
                  $search = $request->search;

             $mer_id   = Session::get('merchantid');
            $hallofferlist =  HallOffer::where('vendor_id',$mer_id)->where('pro_id',$Cid);
            if($status!='')
            {
            $hallofferlist = $hallofferlist->where('status',$status);
            }
            if($search !='')
            {
            $mer_selected_lang_code = Session::get('mer_lang_code'); 
            if($mer_selected_lang_code !='en')
            {
            $hallofferlist = $hallofferlist->where('title_ar','LIKE','%' .$search.'%');
            }
            else
            {
            $hallofferlist = $hallofferlist->where('title','LIKE','%' .$search.'%');
 
            }
            }   
            $hallofferlist =  $hallofferlist->orderBy('id','DESC')->paginate($getPagelimit);

                $this->setLanguageLocaleMerchant();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
            return view('sitemerchant.acoustics.acoustic-offer',compact('merchantheader','merchantfooter','hallofferlist','status','autoid','sid','id','search'));       
            } else {
                return Redirect::to('sitemerchant');
            }
        }
     }      
      


     // Start For acoustic Add Offer
    public function acousticAddOffer(Request $request)
     {
           
           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $getPagelimit = config('app.paginate');
                $itemid = $request->hid;
                $autoid = $request->autoid;
                $Cid = $request->Cid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $reviewrating = Reviewrating::where('vandor_id',$mer_id)->paginate($getPagelimit)->appends(request()->query());
                $categorysave = HallOffer::where('id',$autoid)->where('vendor_id',$mer_id)->first(); 
                return view('sitemerchant.acoustics.acoustic-add-offer', compact('merchantheader','merchantfooter','id','itemid','autoid','reviewrating','categorysave','Cid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }

        }
// End For Makeup Add Offer 
    //STORE OFFER
       public function storeAcousticOffer(Request $request)
       {
            if (Session::has('merchantid')) 
        {      
        $mer_id   = Session::get('merchantid');
        $id = $request->parentid;
        $itemid = $request->hid;
         $Cid = $request->Cid;
        $autoid = $request->autoid;
        if($autoid =='')
        {
        $categorysave = new HallOffer; 
        }
        else
        {
        $categorysave = HallOffer::where('id',$autoid)->where('vendor_id',$mer_id)->first();
        }
        $categorysave->vendor_id    = $mer_id; 
        $categorysave->title        =$request->title;
        $categorysave->title_ar    = $request->title_ar;
        $categorysave->start_date     = $request->Date_from;
        $categorysave->end_date    = $request->date_to;
        $categorysave->discount   = $request->discount;
        $categorysave->coupon   = $request->coupon;
        $categorysave->pro_id   = $Cid;
        //$categorysave->pro_id   = $itemid;
        $categorysave->status    = 1;
        if (\Config::get('app.locale') == 'ar'){
        Session::flash('message', "تمت إضافة العرض بنجاح  ");
        }
        else
        {
        Session::flash('message', "Offer successfully saved");
        }
        $categorysave->save(); 
        return redirect()->route('acoustic-offer',['id' => $id,'hid' => $itemid,'Cid' => $Cid]);
       } else {
      return Redirect::to('sitemerchant');
      } 
       }
     //END OFFER 





public function addsingerpicture(Request $request)
{
  if (Session::has('merchantid')) 
             {  
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $Cid= $request->Cid;
                if(!$request->file('hallpics') && trim($request->youtubevideo)=='' &&   trim($request->about)==''){
                $validatedData = $request->validate([
                'youtubevideo' => 'required', 
                ]);
                }
               
                if($files=$request->file('hallpics')){
                foreach($files as $file){
                $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());            
                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111,99999).'_'.time().'.'.$extension;                        
                $imageRealPath  =   $file->getRealPath();
                $thumbName      =   'thumb_'. $fileName;       
                $img = Image::make($imageRealPath); // use this if you want facade style code
               

                    $thumb_width = 100;
                    list($width,$height) = getimagesize($imageRealPath);
                    $thumb_height = ($thumb_width/$width) * $height;
                    $img->resize($thumb_width,$thumb_height);
 
                $img->save('singer' . '/images/'. $thumbName);
                $file->move('singer/images/', $fileName);
                $thumbNameUrl = url('').'/singer/images/'.$thumbName;
                DB::table('nm_music_gallery')->insert([
                ['vendor_id' => $mer_id,'image' => $thumbNameUrl,'music_id' => $hid],
                ]); 

DB::table('nm_category_gallery')->insert([
                ['vendor_id' => $mer_id,'image' => $thumbNameUrl,'category_id' => $Cid],
                ]); 

                
                }
                }


                $about = $request->about;
                $about_ar = $request->about_ar;
                 $youtubevideo = str_replace('youtu.be','www.youtube.com/embed',$request->youtubevideo);

                DB::table('nm_music')->where('id',$hid)->where('vendor_id',$mer_id)->update( ['video_url'=>$youtubevideo,'video_description'=> $about,'video_description_ar'=> $about_ar]);

                DB::table('nm_category')->where('mc_id',$Cid)->where('vendor_id',$mer_id)->update( ['mc_video_url'=>$youtubevideo,'mc_video_description'=> $about,'mc_video_description_ar'=> $about_ar]);
                  
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();


                 

                     // language for display message //
                if (\Config::get('app.locale') == 'ar'){
                Session::flash('message', "تمت إضافة الخدمة");
                }
                else
                {
                Session::flash('message', "Record saved successfully");
                }
                // language for display message // 
                   
               

                return Redirect::back();
                      
            } else {
                 return Redirect::to('sitemerchant');
            }

}
 
     
   
 }



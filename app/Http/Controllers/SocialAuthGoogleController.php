<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Socialite;
use Auth;
use Exception;
use Session;


class SocialAuthGoogleController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }


    public function callback()
    {  
        
     
        try {

     
         $googleUser = Socialite::driver('google')->user();
           
            $existUser = User::where('email',$googleUser->email)->first();
            

            if($existUser) 
            {
              Auth::loginUsingId($existUser->id);
            }
            else 
            {
                $user = new User;
                $user->cus_name = $googleUser->name;
                $user->email = $googleUser->email;
                $user->social_unique_id = $googleUser->id;
                $user->password = md5(rand(1,10000));
                $user->save();
                Auth::loginUsingId($user->id);
            }
			$token = rand(11111,99999);
			$userInf =   Auth::id();
			$userinfos = User::where('email',$googleUser->email)->first();
			$returnuser = array('user_id' => $userinfos->cus_id, "user_email" => $userinfos->email, "user_name" => $userinfos->cus_name, "gender" => $userinfos->gender, "address" => $userinfos->cus_address1.
			' '.$userinfos->cus_address2, "telephone_number" => $userinfos->ship_phone, 'token' => $token);
			Session::put('customerdata', $returnuser);
            return redirect()->to('/');
        } 
        catch(Exception $e) 
        {
            return 'error';
        }
    }
}
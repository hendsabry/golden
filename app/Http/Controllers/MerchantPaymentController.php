<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Models;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon;
use File; 
use Auth;
use Lang;
use App\Category;
use App\Merchant;
use Khill\Lavacharts\Lavacharts;
use App\ServiceShipping;
use App\Categorylist;
use App\OrderSingleProduct;
class MerchantPaymentController extends Controller
{
      public function __construct(){
            parent::__construct();
            // set admin Panel language
            $this->middleware(function ($request, $next) {
                        $this->setLanguageLocaleMerchant();
                        Session::get('mer_lang_code');
                        return $next($request);
           });
     }
    
    public function paymentSuccess(Request $request)
    {
        if (Session::has('merchantid')) 
        {
         

        $id = $request->id;
        $status = 2;
        //$subscription_end_date = date('Y-m-d');
        $subscription_end_date = date('Y-m-d',strtotime(date("Y-m-d") . " + 365 day"));
        DB::table('nm_merchant_payment_subscription')->where('transaction_id',$id)->update(['status' => $status,'subscription_end_date'=> $subscription_end_date]);
        $merid  = Session::get('merchantid');                       
        $this->setLanguageLocaleMerchant();
        $mer_id              = Session::get('merchantid');             
        $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
        $merchantfooter     = view('sitemerchant.includes.merchant_footer');           
 
        $amount = "99";
        //$firstname = $checkvendorInfo->mer_fname;
        //$lastname = $checkvendorInfo->mer_lname;
        //$email = $checkvendorInfo->mer_email;
        $email = 'arun@wisitech.com';

          $user = Merchant::findOrFail($merid); 

        Mail::send('emails.paymentsuccess', ['user' => $user], function ($m) use ($user) {
            $m->from('info@goldencages.com', 'Goldencages');
            $m->to('arun@wisitech.com', $user->mer_fname)->subject('Payment Acknowledgement');
            // $m->to($user->mer_email, $user->mer_fname)->subject('Payment Acknowledgement');
        });
    

 



        return view('sitemerchant.payment.payment-success', compact('merchantheader','merchantfooter'));       
        } else {
        return Redirect::to('sitemerchant');
        }

    }
    public function paymentError(Request $request)
    {
        if (Session::has('merchantid')) 
        {
        $merid  = Session::get('merchantid');                         

        $this->setLanguageLocaleMerchant();
        $mer_id              = Session::get('merchantid');             
        $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
        $merchantfooter     = view('sitemerchant.includes.merchant_footer');           

        return view('sitemerchant.payment.payment-error', compact('merchantheader','merchantfooter'));       
        } else {
        return Redirect::to('sitemerchant');
        }

    }



     public function payNow(Request $request)
     {
        if (Session::has('merchantid')) 
        {
        $merid  = Session::get('merchantid');   
     $getservice_id = request()->serviceid;
     $getAllData = Category::where('mc_id',$getservice_id)->first();
    $productName = $getAllData->mc_name.'\'s Subscription';
    $category_name= $getAllData->mc_name;
    $returnURls = url('/').'/payment-success';
    $returnErrorURls = url('/').'/payment-error';

    $paygateway_url = "https://test.myfatoorah.com/pg/PayGatewayServiceV2.asmx";
    $user = 'testapi@myfatoorah.com';
    $password = 'E55D0';
    $code = "999999";

    $checkvendorInfo = Merchant::where('mer_id',$merid)->first();

    $nm_subscription_plan_amount=DB::table('nm_subscription_plan')->where('cat_id',$getservice_id)->first();
    
    $amount = $nm_subscription_plan_amount->payment;
    $firstname = $checkvendorInfo->mer_fname;
    $lastname = $checkvendorInfo->mer_lname;
    $email = $checkvendorInfo->mer_email;
    $phone = $checkvendorInfo->mer_phone;
    $subscription_start_date = date("Y-m-d");
    $subscription_end_date = date("Y-m-d");
    $subscription_amount = $amount;
  
    $productdata = "";
    $productdata .= '<ProductDC>';
    $productdata .= '<product_name>'.$productName.'</product_name>';
    $productdata .= '<unitPrice>' . $amount . '</unitPrice>';
    $productdata .= '<qty>1</qty>';
    $productdata .= '</ProductDC>';
    // Soap xml
$post_string = '<?xml version="1.0" encoding="windows-1256"?>
                        <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
                        <soap12:Body>
                        <PaymentRequest xmlns="http://tempuri.org/">
                          <req>
                            <CustomerDC>
                              <Name>' . $firstname . ' ' . $lastname . '</Name>
                              <Email>' . $email . '</Email>
                              <Mobile>' . $phone . '</Mobile>
                            </CustomerDC>
                            <MerchantDC>
                              <merchant_code>' . $code . '</merchant_code>
                              <merchant_username>' . $user . '</merchant_username>
                              <merchant_password>' . $password . '</merchant_password>
                              <merchant_ReferenceID>' . time() . '</merchant_ReferenceID>
                              <ReturnURL>'.$returnURls.'</ReturnURL>
                              <merchant_error_url>'.$returnErrorURls.'</merchant_error_url>
                            </MerchantDC>
                            <lstProductDC>' . $productdata . '</lstProductDC>
                            <totalDC>
                                <subtotal>' . $amount . '</subtotal>
                                </totalDC>
                                <paymentModeDC>
                            <paymentMode>BOTH</paymentMode>
                            </paymentModeDC>
                            <paymentCurrencyDC>
                              <paymentCurrrency>SAR</paymentCurrrency>
                            </paymentCurrencyDC>
                          </req>
                        </PaymentRequest>
                      </soap12:Body>
                    </soap12:Envelope>';
          $soap_do = curl_init();
          curl_setopt($soap_do, CURLOPT_URL, $paygateway_url);
          curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
          curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
          curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
          curl_setopt($soap_do, CURLOPT_POST, true);
          curl_setopt($soap_do, CURLOPT_POSTFIELDS, $post_string);
          curl_setopt($soap_do, CURLOPT_HTTPHEADER, array(
          'Content-Type: text/xml; charset=utf-8',
          'Content-Length: ' . strlen($post_string)
          ));
          curl_setopt($soap_do, CURLOPT_USERPWD, $user . ":" . $password);
          $result = curl_exec($soap_do);
          $err = curl_error($soap_do);
          $file_contents = htmlspecialchars($result);
          curl_close($soap_do);
          $doc = new \DOMDocument();

          if ($doc != null) {
          $doc->loadXML(html_entity_decode($file_contents));
          $ResponseCode = $doc->getElementsByTagName("ResponseCode");
          $ResponseCode = $ResponseCode->item(0)->nodeValue;
          $paymentUrl = $doc->getElementsByTagName("paymentURL");
          $paymentUrl = $paymentUrl->item(0)->nodeValue;
          $referenceID = $doc->getElementsByTagName("referenceID");
          $referenceID = $referenceID->item(0)->nodeValue;
          $ResponseMessage = $doc->getElementsByTagName("ResponseMessage");
          $ResponseMessage = $ResponseMessage->item(0)->nodeValue;



    } else {

 // language for display message //
             if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "هناك خطأ ما");
             }
             else
             {
             Session::flash('message', "Something went wrong....");
             }
     // language for display message //
            return Redirect::back();   
 
         
    }
    if ($ResponseCode == 0) {

  $transaction_id = str_replace('=','',strstr($paymentUrl,'='));

 
$status = 1;
 DB::table('nm_services')->where('service_id', '=', $getservice_id)->where('stor_merchant_id', '=', $merid)->update(array('status' => $status));
DB::table('nm_merchant_payment_subscription')->insert(['vendor_id'=>$merid,'category_id'=>$getservice_id,'category_name'=>$category_name,'subscription_start_date'=>$subscription_start_date,'subscription_end_date'=>$subscription_end_date,'subscription_amount'=>$subscription_amount,'status'=>$status,'transaction_id'=>$transaction_id]);

        echo '<script language="javascript">window.location.href="' . $paymentUrl . '";</script>';
   }
     } else {
        return Redirect::to('sitemerchant');
        }


    
     }


//-- Display mywallet page --//

  public function myWallet(Request $request)
    {
      if (Session::has('merchantid')) 
        {
        $getPagelimit = config('app.paginate');
        $merid  = Session::get('merchantid');                         
         $this->setLanguageLocaleMerchant();
        $mer_id              = Session::get('merchantid');             
        $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
        $merchantfooter     = view('sitemerchant.includes.merchant_footer');
        $getTotalAmount = DB::table('nm_wallet_transaction')->where('vendor_id',$merid);
       
        $amount = $request->amount;
        $dateTo = $request->dateTo;
        $dateFrom = $request->dateFrom;
 
        if($amount!='')
        {
        $getTotalAmount =  $getTotalAmount->where('amount',$amount);
        }
        if($dateTo !='' && $dateFrom !='') 
        {      
        $getTotalAmount =  $getTotalAmount->whereBetween('created_at', [$dateFrom, $dateTo]);
        }elseif($dateTo !='')
        {      
        $getTotalAmount = $getTotalAmount->where(DB::raw("DATE(created_at) = '".$dateTo."'"));
        } 
  
        $getTotalAmount = $getTotalAmount->orderBy('id','ASC')->paginate($getPagelimit);

 
        return view('sitemerchant.wallet.my-wallet', compact('merchantheader','merchantfooter','getTotalAmount'));       
        } else {
        return Redirect::to('sitemerchant');
        }

    }

  public function walletAddamount(Request $request)
  {
    if (Session::has('merchantid')) 
    {
        $merid  = Session::get('merchantid');                         
         $this->setLanguageLocaleMerchant();
        $mer_id              = Session::get('merchantid');             
        $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
        $merchantfooter     = view('sitemerchant.includes.merchant_footer');
        return view('sitemerchant.wallet.wallet-addamount', compact('merchantheader','merchantfooter','getTotalAmount')); 
    }
    else
    {
      return Redirect::to('sitemerchant');
    } 
  }
public function WalletSuccess(Request $request)
{
 if (Session::has('merchantid') && $request->id!='') 
    {
    $merid  = Session::get('merchantid'); 
 
    $id = $request->id;
    $status = 2;
    DB::table('nm_wallet_transaction')->where('vendor_id',$merid)->where('transction_id',$id)->update(['status' => $status]);
 
    // language for display message //
    if (\Config::get('app.locale') == 'ar'){
    Session::flash('message', "المبلغ المضافة في محفظتك");
    }
    else
    {
    Session::flash('message', "Amount added in your wallet");
    }
    // language for display message //
    return Redirect::to('my-wallet');
    }
    else
    {
      return Redirect::to('sitemerchant');
    } 
}
  public function updateWallet(Request $request)
  {
    if (Session::has('merchantid') && $request->_token!='' && (request()->amount!=''|| request()->amount!=0)) 
    {
            $merid  = Session::get('merchantid');                         
            $amount = request()->amount;     
            $returnURls = url('/').'/my-wallet-success';
            $returnErrorURls = url('/').'/wallet-addamount';
            $paygateway_url = "https://test.myfatoorah.com/pg/PayGatewayServiceV2.asmx";
            $user = 'testapi@myfatoorah.com';
            $password = 'E55D0';
            $code = "999999";
            $checkvendorInfo = Merchant::where('mer_id',$merid)->first();
            $productName = 'Wallet Amount';
            $firstname = $checkvendorInfo->mer_fname;
            $lastname = $checkvendorInfo->mer_lname;
            $email = $checkvendorInfo->mer_email;
            $phone = $checkvendorInfo->mer_phone;
            $subscription_start_date = date("Y-m-d");
            $subscription_end_date = date("Y-m-d");
            $subscription_amount = $amount;
            $productdata = "";
            $productdata .= '<ProductDC>';
            $productdata .= '<product_name>'.$productName.'</product_name>';
            $productdata .= '<unitPrice>' . $amount . '</unitPrice>';
            $productdata .= '<qty>1</qty>';
            $productdata .= '</ProductDC>';
            // Soap xml
                $post_string = '<?xml version="1.0" encoding="windows-1256"?>
                <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
                <soap12:Body>
                <PaymentRequest xmlns="http://tempuri.org/">
                <req>
                <CustomerDC>
                <Name>' . $firstname . ' ' . $lastname . '</Name>
                <Email>' . $email . '</Email>
                <Mobile>' . $phone . '</Mobile>
                </CustomerDC>
                <MerchantDC>
                <merchant_code>' . $code . '</merchant_code>
                <merchant_username>' . $user . '</merchant_username>
                <merchant_password>' . $password . '</merchant_password>
                <merchant_ReferenceID>' . time() . '</merchant_ReferenceID>
                <ReturnURL>'.$returnURls.'</ReturnURL>
                <merchant_error_url>'.$returnErrorURls.'</merchant_error_url>
                </MerchantDC>
                <lstProductDC>' . $productdata . '</lstProductDC>
                <totalDC>
                <subtotal>' . $amount . '</subtotal>
                </totalDC>
                <paymentModeDC>
                <paymentMode>BOTH</paymentMode>
                </paymentModeDC>
                <paymentCurrencyDC>
                <paymentCurrrency>SAR</paymentCurrrency>
                </paymentCurrencyDC>
                </req>
                </PaymentRequest>
                </soap12:Body>
                </soap12:Envelope>';
            $soap_do = curl_init();
            curl_setopt($soap_do, CURLOPT_URL, $paygateway_url);
            curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
            curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($soap_do, CURLOPT_POST, true);
            curl_setopt($soap_do, CURLOPT_POSTFIELDS, $post_string);
            curl_setopt($soap_do, CURLOPT_HTTPHEADER, array(
            'Content-Type: text/xml; charset=utf-8',
            'Content-Length: ' . strlen($post_string)
            ));
            curl_setopt($soap_do, CURLOPT_USERPWD, $user . ":" . $password);
            $result = curl_exec($soap_do);
            $err = curl_error($soap_do);
            $file_contents = htmlspecialchars($result);
            curl_close($soap_do);
            $doc = new \DOMDocument();
            if ($doc != null) {
            $doc->loadXML(html_entity_decode($file_contents));
            $ResponseCode = $doc->getElementsByTagName("ResponseCode");
            $ResponseCode = $ResponseCode->item(0)->nodeValue;
            $paymentUrl = $doc->getElementsByTagName("paymentURL");
            $paymentUrl = $paymentUrl->item(0)->nodeValue;
            $referenceID = $doc->getElementsByTagName("referenceID");
            $referenceID = $referenceID->item(0)->nodeValue;
            $ResponseMessage = $doc->getElementsByTagName("ResponseMessage");
            $ResponseMessage = $ResponseMessage->item(0)->nodeValue;
             } else {
            // language for display message //
            if (\Config::get('app.locale') == 'ar'){
            Session::flash('message', "هناك خطأ ما");
            }
            else
            {
            Session::flash('message', "Something went wrong....");
            }
            // language for display message //
            return Redirect::back();
            }
            if ($ResponseCode == 0) {
            $transaction_id = str_replace('=','',strstr($paymentUrl,'='));
            $status = 1;
  DB::table('nm_wallet_transaction')->insert(['user_id'=>0,'vendor_id'=>$merid,'amount'=>$amount,'transaction_type'=>1,'status'=>1,'transction_id'=>$transaction_id]);

            echo '<script language="javascript">window.location.href="' . $paymentUrl . '";</script>';
            }
      
    }
    else
    {
      return Redirect::to('sitemerchant');   
    }

  }
//-- Display mywallet page end --//

 public function orderReporting(Request $request)
      {
      if (Session::has('merchantid')) 
      {
          $merId = Session::get('merchantid'); 
          $dateFrom = $request->dateFrom;
          $dateTo = $request->dateTo;
          $getPendingOrders = DB::table('nm_order');
          if($dateFrom !='' && $dateTo!='')
          {
          $getPendingOrders = $getPendingOrders->whereDate('order_date', '>=', $dateFrom)->whereDate('order_date', '<=', $dateTo);
          }
          $getPendingOrders = $getPendingOrders->where('order_merchant_id',$merId)->where('order_status',1)->count(); 
          $getCompleteOrders = DB::table('nm_order');
          if($dateFrom !='' && $dateTo!='')
          {
          $getCompleteOrders = $getCompleteOrders->whereDate('order_date', '>=', $dateFrom)->whereDate('order_date', '<=',$dateTo);
          }
          $getCompleteOrders = $getCompleteOrders->where('order_merchant_id',$merId)->where('order_status',2)->count(); 

          $getHoldOrders = DB::table('nm_order');
          if($dateFrom !='' && $dateTo!='')
          {
          $getHoldOrders = $getHoldOrders->whereDate('order_date', '>=', $dateFrom)->whereDate('order_date', '<=', $dateTo);
          }
          $getHoldOrders = $getHoldOrders->where('order_merchant_id',$merId)->where('order_status',3)->count(); 
          $getFailedOrders = DB::table('nm_order');
          if($dateFrom !='' && $dateTo!='')
          {
          $getFailedOrders = $getFailedOrders->whereDate('order_date', '>=', $dateFrom)->whereDate('order_date', '<=', $dateTo);
          }
          $getFailedOrders = $getFailedOrders->where('order_merchant_id',$merId)->where('order_status',4)->count(); 
          $reasons = \Lava::DataTable();
          $reasons->addStringColumn('Reasons')
          ->addNumberColumn('Percent')
          ->addRow(['Complete Orders', $getCompleteOrders])
          ->addRow(['Pending Orders', $getPendingOrders])
          ->addRow(['Hold Orders', $getHoldOrders])
          ->addRow(['Fail Orders', $getFailedOrders]);
          \Lava::DonutChart('IMDB', $reasons, [
          'title' => 'Orders'
          ]);
          $year = $request->year;
          if($year==''){ $year = date("Y"); }
          $onemnth = DB::table('nm_order')->whereYear('order_date', '=', $year)->whereMonth('order_date', '=', '01')->count();
          $Twomnth = DB::table('nm_order')->whereYear('order_date', '=', $year)->whereMonth('order_date', '=', '02')->count();
          $Threemnth = DB::table('nm_order')->whereYear('order_date', '=', $year)->whereMonth('order_date', '=', '03')->count();
          $Fourmnth = DB::table('nm_order')->whereYear('order_date', '=', $year)->whereMonth('order_date', '=', '04')->count(); 
          $Fivemnth = DB::table('nm_order')->whereYear('order_date', '=', $year)->whereMonth('order_date', '=', '05')->count(); 
          $Sixmnth = DB::table('nm_order')->whereYear('order_date', '=', $year)->whereMonth('order_date', '=', '06')->count(); 
          $Svenmnth = DB::table('nm_order')->whereYear('order_date', '=', $year)->whereMonth('order_date', '=', '07')->count(); 
          $Eightmnth = DB::table('nm_order')->whereYear('order_date', '=', $year)->whereMonth('order_date', '=', '08')->count();
          $Ninemnth = DB::table('nm_order')->whereYear('order_date', '=', $year)->whereMonth('order_date', '=', '09')->count(); 
          $tenmnth = DB::table('nm_order')->whereYear('order_date', '=', $year)->whereMonth('order_date', '=', '10')->count(); 
          $Elvnmnth = DB::table('nm_order')->whereYear('order_date', '=', $year)->whereMonth('order_date', '=', '11')->count(); 
          $twlvmnth = DB::table('nm_order')->whereYear('order_date', '=', $year)->whereMonth('order_date', '=', '12')->count(); 
          $finances = \Lava::DataTable();
          $fin =  $finances->addDateColumn('Date')
          ->addNumberColumn('Sales')
          ->addRow(["$year-1-30", $onemnth])
          ->addRow(["$year-2-30", $Twomnth])
          ->addRow(["$year-3-30", $Threemnth])
          ->addRow(["$year-4-30", $Fourmnth])
          ->addRow(["$year-5-30", $Fivemnth])
          ->addRow(["$year-6-30", $Sixmnth])
          ->addRow(["$year-7-30", $Svenmnth])
          ->addRow(["$year-8-30", $Eightmnth])
          ->addRow(["$year-9-30", $Ninemnth])
          ->addRow(["$year-10-30", $tenmnth])
          ->addRow(["$year-11-30", $Elvnmnth])
          ->addRow(["$year-12-30", $twlvmnth]);
          \Lava::ColumnChart('Finances', $finances, [
          'title' => 'Yearly Report',
          'titleTextStyle' => [
          'color'    => '#eb6b2c',
          'fontSize' => 12
          ]
          ]);

          $merid  = Session::get('merchantid');
          $this->setLanguageLocaleMerchant();
          $mer_id              = Session::get('merchantid');             
          $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter     = view('sitemerchant.includes.merchant_footer');                
      return view('sitemerchant.report.order-reporting', compact('merchantheader','merchantfooter','year'));       
      } else {
      return Redirect::to('sitemerchant');
      }


 }
   

public function transations(Request $request)
{
    if (Session::has('merchantid')) 
    {
      $merid  = Session::get('merchantid');
      $this->setLanguageLocaleMerchant();
      $mer_id              = Session::get('merchantid');             
      $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
      $merchantfooter     = view('sitemerchant.includes.merchant_footer');  
      $getPagelimit = config('app.paginate');

        $getTotalAmount = DB::table('nm_wallet_transaction')->where('vendor_id',$merid);

        $amount = $request->amount;
        $dateTo = $request->dateTo;
        $dateFrom = $request->dateFrom;

        if($amount!='')
        {
        $getTotalAmount =  $getTotalAmount->where('amount',$amount);
        }
        if($dateTo !='' && $dateFrom !='') 
        {      
        $getTotalAmount =  $getTotalAmount->whereBetween('created_at', [$dateFrom, $dateTo]);
        }elseif($dateTo !='')
        {      
        $getTotalAmount = $getTotalAmount->where(DB::raw("DATE(created_at) = '".$dateTo."'"));
        } 

        $getTotalAmount = $getTotalAmount->orderBy('id','ASC')->paginate($getPagelimit);


       return view('sitemerchant.report.transations', compact('merchantheader','merchantfooter','getTotalAmount'));       
    } else {
       return Redirect::to('sitemerchant');
    }

}



public function fundRequest(Request $request)
{
    if (Session::has('merchantid')) 
    {
          $merid  = Session::get('merchantid');
      $this->setLanguageLocaleMerchant();
      $mer_id              = Session::get('merchantid');             
      $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
      $merchantfooter     = view('sitemerchant.includes.merchant_footer');  


        $getPagelimit = config('app.paginate');



           $merchantmanagerid   = Session::get('merchant_managerid');
                $LoginType   = Session::get('LoginType');
                if($LoginType==4){
                $mercshopids= Categorylist::where('branch_manager_id',$merchantmanagerid)->get();
                           $shopidarray=array();
                           $orderids=array();

             foreach ($mercshopids as $getshopid) {
                array_push($shopidarray, $getshopid->mc_id);               
             }
             $vendororder = OrderSingleProduct::where('merchant_id',$mer_id)->whereIn('shop_id',$shopidarray)->orderBy('order_id','desc')->get();

             foreach ($vendororder as $merorder) {

                       array_push($orderids, $merorder->order_id); 
               
             }

              $getTotalAmount = DB::table('nm_merchant_payment_history')->where('merchant_id',$merid)->whereIn('order_id',$orderids)->where('status','!=',1);



              }else{




        $getTotalAmount = DB::table('nm_merchant_payment_history')->where('merchant_id',$merid)->where('status','!=',1);

     }
        $amount= $request->amount;
        $dateTo = $request->dateTo;
        $dateFrom = $request->dateFrom;

        if($amount!='')
        {
        $getTotalAmount =  $getTotalAmount->where('amount',$amount);
        }
        if($dateTo !='' && $dateFrom !='') 
        {      
        $getTotalAmount =  $getTotalAmount->whereBetween('requested_date', [$dateFrom, $dateTo]);
        }elseif($dateTo !='')
        {      
        $getTotalAmount = $getTotalAmount->where(DB::raw("DATE(requested_date) = '".$dateTo."'"));
        } 

        $getTotalAmount = $getTotalAmount->orderBy('id','DESC')->paginate($getPagelimit);

 

       return view('sitemerchant.report.fund-request', compact('merchantheader','merchantfooter','getTotalAmount'));       
    } else {
       return Redirect::to('sitemerchant');
    }

  
}

public function accountDetails(Request $request)
{
    if (Session::has('merchantid')) 
    {
          $merid  = Session::get('merchantid');

    $getTotalAmount = DB::table('nm_merchant_bank_details')->where('merchant_id',$merid)->first();


      $this->setLanguageLocaleMerchant();
      $mer_id              = Session::get('merchantid');             
      $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
      $merchantfooter     = view('sitemerchant.includes.merchant_footer');  
       return view('sitemerchant.report.account-details', compact('merchantheader','merchantfooter','getTotalAmount'));       
    } else {
       return Redirect::to('sitemerchant');
    }


  
}


public function recievePayment(Request $request)
{

     if (Session::has('merchantid')) 
    {
          $merid  = Session::get('merchantid');
      $this->setLanguageLocaleMerchant();
      $mer_id              = Session::get('merchantid');             
      $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
      $merchantfooter     = view('sitemerchant.includes.merchant_footer');  



        $getPagelimit = config('app.paginate');



          $merchantmanagerid   = Session::get('merchant_managerid');
                $LoginType   = Session::get('LoginType');
                if($LoginType==4){
                $mercshopids= Categorylist::where('branch_manager_id',$merchantmanagerid)->get();
                           $shopidarray=array();
                           $orderids=array();

             foreach ($mercshopids as $getshopid) {
                array_push($shopidarray, $getshopid->mc_id);               
             }
             $vendororder = OrderSingleProduct::where('merchant_id',$mer_id)->whereIn('shop_id',$shopidarray)->orderBy('order_id','desc')->get();

             foreach ($vendororder as $merorder) {

                       array_push($orderids, $merorder->order_id); 
               
             }
              $getTotalAmount = DB::table('nm_merchant_payment_history')->where('merchant_id',$merid)->whereIn('order_id',$orderids)->where('status',1);

           }else{
     
        $getTotalAmount = DB::table('nm_merchant_payment_history')->where('merchant_id',$merid)->where('status',1);
}
     
        $amount= $request->amount;
        $dateTo = $request->dateTo;
        $dateFrom = $request->dateFrom;

        if($amount!='')
        {
        $getTotalAmount =  $getTotalAmount->where('amount',$amount);
        }
        if($dateTo !='' && $dateFrom !='') 
        {      
        $getTotalAmount =  $getTotalAmount->whereBetween('requested_date', [$dateFrom, $dateTo]);
        }elseif($dateTo !='')
        {      
        $getTotalAmount = $getTotalAmount->where(DB::raw("DATE(requested_date) = '".$dateTo."'"));
        } 

        $getTotalAmount = $getTotalAmount->orderBy('id','DESC')->paginate($getPagelimit);

 


       return view('sitemerchant.report.recieve-payment', compact('merchantheader','merchantfooter','getTotalAmount'));       
    } else {
       return Redirect::to('sitemerchant');
    }
 
}
  
public function storeBankinfo(Request $request)
{
    if (Session::has('merchantid')) 
    {
         
    $merid  = Session::get('merchantid');
    $bank_name = $request->bank_name;
    $ac_number = $request->ac_number;
    $note = $request->note;
    $getCount = DB::table('nm_merchant_bank_details')->where('merchant_id', $merid)->count(); 
if($getCount < 1)
{
   DB::table('nm_merchant_bank_details')->insert(['merchant_id'=>$merid,'bank_name'=>$bank_name,'account_number'=>$ac_number,'note'=>$note]);
}
else
{
  DB::table('nm_merchant_bank_details')->where('merchant_id',$merid)->update(['bank_name'=>$bank_name,'account_number'=>$ac_number,'note'=>$note]);
}
   

     // language for display message //
             if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "سجل تم تحديثه بنجاح");
             }
             else
             {
             Session::flash('message', "Record sucessfully updated");
             }
     // language for display message //
            return Redirect::back();   
 



    } else {
    return Redirect::to('sitemerchant');
    }
}


public function manageshipping(Request $request)
{

   if (Session::has('merchantid')) 
    {

      $merid  = Session::get('merchantid');
      $getShipping = ServiceShipping::where('vendor_id',$merid)->first();
 
      $this->setLanguageLocaleMerchant();
      $mer_id              = Session::get('merchantid');             
      $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
      $merchantfooter     = view('sitemerchant.includes.merchant_footer');  

 



       return view('sitemerchant.manage-shipping', compact('merchantheader','merchantfooter','getShipping'));       
    } else {
       return Redirect::to('sitemerchant');
    }


}

public function manage_shipping(Request $request)
{

 if (Session::has('merchantid')) 
    {

                $merid  = Session::get('merchantid');

                $alreadyReg = ServiceShipping::where('vendor_id',$merid)->count();
 
                if($alreadyReg >=1)
                 {   
                 $saveservice = ServiceShipping::where('vendor_id',$merid)->first();
                 }
                 else
                 {  
                $saveservice = new ServiceShipping; 
                $saveservice->vendor_id = $merid;
                 }
                  if(isset($request->needshipping_buffet) && $request->needshipping_buffet!='')
                  {
                  $Buffet = implode(",", $request->needshipping_buffet);
                  }
                  else
                  {
                  $Buffet = '';
                  }

                  if(isset($request->needshipping_dessert) && $request->needshipping_dessert!='')
                  {
                  $Desert = implode(",", $request->needshipping_dessert);
                  }
                  else
                  {
                  $Desert = '';
                  }

                  if(isset($request->needshipping_dates) && $request->needshipping_dates!='')
                  {
                  $Dates = implode(",", $request->needshipping_dates);
                  }
                  else
                  {
                  $Dates = '';
                  }

                  if(isset($request->needshipping_cosha) && $request->needshipping_cosha!='')
                  {
                  $Cosha = implode(",", $request->needshipping_cosha);
                  }
                  else
                  {
                  $Cosha = '';
                  }

                  if(isset($request->needshipping_roses) && $request->needshipping_roses!='')
                  {
                  $Roses = implode(",", $request->needshipping_roses);
                  }
                  else
                  {
                  $Roses = '';
                  }
                  if(isset($request->needshipping_special_event) && $request->needshipping_special_event!='')
                  {
                  $Special_Events = implode(",", $request->needshipping_special_event);
                  }
                  else
                  {
                  $Special_Events = '';
                  }

                  if(isset($request->needshipping_special_event) && $request->needshipping_special_event!='')
                  {
                  $Special_Events = implode(",", $request->needshipping_special_event);
                  }
                  else
                  {
                  $Special_Events = '';
                  }

                  if(isset($request->needshipping_makeup) && $request->needshipping_makeup!='')
                  {
                  $Makeup_Product = implode(",", $request->needshipping_makeup);
                  }
                  else
                  {
                  $Makeup_Product = '';
                  }

                  if(isset($request->needshipping_acoustics) && $request->needshipping_acoustics!='')
                  {
                  $Acoustics = implode(",", $request->needshipping_acoustics);
                  }
                  else
                  {
                  $Acoustics = '';
                  }

                  if(isset($request->needshipping_tailor) && $request->needshipping_tailor!='')
                  {
                  $Tailors = implode(",", $request->needshipping_tailor);
                  }
                  else
                  {
                  $Tailors = '';
                  }

                  if(isset($request->needshipping_dresses) && $request->needshipping_dresses!='')
                  {
                  $Dresses = implode(",", $request->needshipping_dresses);
                  }
                  else
                  {
                  $Dresses = '';
                  }


                  if(isset($request->needshipping_abaya) && $request->needshipping_abaya!='')
                  {
                  $Abaya = implode(",", $request->needshipping_abaya);
                  }
                  else
                  {
                  $Abaya = '';
                  }         
                  if(isset($request->needshipping_oudandperfumes) && $request->needshipping_oudandperfumes!='')
                  {
                  $Oud_and_Perfumes = implode(",", $request->needshipping_oudandperfumes);
                  }
                  else
                  {
                  $Oud_and_Perfumes = '';
                  }         
                  if(isset($request->needshipping_goldandjewelry) && $request->needshipping_goldandjewelry!='')
                  {
                  $Gold_and_Jewellery = implode(",", $request->needshipping_goldandjewelry);
                  }
                  else
                  {
                  $Gold_and_Jewellery = '';
                  }         



                $saveservice ->Buffet = $Buffet;
                $saveservice ->Desert = $Desert;
                $saveservice ->Dates = $Dates;
                $saveservice ->Cosha = $Cosha;
                $saveservice ->Roses = $Roses;
                $saveservice ->Special_Events = $Special_Events;
                $saveservice ->Makeup_Product = $Makeup_Product; 
                $saveservice->Acoustics =$Acoustics; 
                $saveservice->Tailors = $Tailors; 
                $saveservice->Dresses = $Dresses; 
                $saveservice->Abaya = $Abaya; 
                $saveservice->Oud_and_Perfumes = $Oud_and_Perfumes; 
                $saveservice->Gold_and_Jewellery = $Gold_and_Jewellery; 

                $saveservice->save();

// language for display message //
             if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "سجل تم تحديثه بنجاح");
             }
             else
             {
             Session::flash('message', "Record sucessfully updated");
             }
     // language for display message //


 
       return Redirect::to('manage-shipping');
     } else {
       return Redirect::to('sitemerchant');
    }

}

public function checkuniquecode(Request $request)
{
    if($request->code !='')
    {
    $code = $request->code;
    $getC = DB::table('nm_product_offer')->where('coupon',$code)->count();
    if($getC >=1){

      echo "1";
    }
    else
    {
         echo "0";
    }


    }
   

}

public function updateCarqty(Request $request)
{
  if($request->qbox !='')
    {
    $qbox = $request->qbox;
     $id = $request->id;
 $getQty = DB::table('nm_product')->where('pro_id',$id)->first();
$pro_qty = $getQty->pro_qty + $qbox;
   DB::table('nm_product')->where('pro_id',$id)->update(['pro_qty'=>$pro_qty]);
   

      echo "1";
    


    }

}

 } // end 

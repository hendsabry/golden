<?php 
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//

use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;
use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\Formbusinesstype;
use App\Shoplisthall;
use App\Foodcategorylist;
use App\Foodshopdate;
//------------wisitech code end -----------//
class FoodshopdessertController extends Controller
{  
    public function __construct(){
        parent::__construct();
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
     
    public function fooddessertshopandmenu(Request $request,$halltype,$typeofhallid,$branchid,$shopid)
    {
		
        $halltype=$halltype;
		$subcategory_id=$halltype;
		$shop_id=$shopid;
		$branch_id=$branchid;
		$city_id=Session::get('searchdata.cityid');
		if(Session::get('searchdata.basecategoryid')!=''){
		$basecategory_id=Session::get('searchdata.basecategoryid');
		}else {
		$basecategory_id=Session::get('searchdata.maincategoryid');
		}
		 $category_id=$typeofhallid;
		 $typeofhallid=$typeofhallid;
		 $budget=Session::get('searchdata.budget');
		$lang=Session::get('lang_file');


		
		//$request->session()->push('searchdata.typeofhallid', $category_id);
		
		 $foodsessioninfo = array('maincategory_id' => $halltype, "category_id" => $category_id, "shop_id" => $shopid); 
				Session::put('fooddata', $foodsessioninfo);
		
		
		$Formbusinesstype= new Formbusinesstype();
		  $bussinesstype=$Formbusinesstype->Getbussinesstype($basecategory_id,$lang);	
			 $value = $request->session()->get('searchdata');
		       $getsubcategories = '';
			   
			   $foodShoplisthall= new Foodcategorylist();
			   $foodShopdate= new Foodshopdate();
			 
			 $foodshopunderbugetincity=$foodShoplisthall->Getfoodshoplistbycity($category_id,$city_id,$budget,$lang);
			// print_r($foodshopunderbugetincity);
			 $fooddateshopdetails=$foodShopdate->Getfoodshopdate($shopid,$lang);
			  $fooddateshopgallery=$foodShopdate->Getgalleryimages($shopid);
			  $fooddateshopreview=$foodShopdate->GetshopReviews($shopid);
			 	$fooddateshopleftproduct=$foodShopdate->Getfoodshopdatefirstproduct($shopid,$lang,44);
			 $fooddateshopproducts=$foodShopdate->Getfoodshopdessertproduct($shopid,$lang,44);

   //print_r($fooddateshopproducts); die;

                ////////////// other branches////////////
                    $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $branchid)->first(); 
                if (!empty($category)) {
                 if ($category_id != '' && $lang == 'ar_lang') {
                  $hallsubcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $branchid)->where('city_id','=',$city_id)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get(); 
                    foreach ($hallsubcategory as $getchildelement) {
                          $cid=$getchildelement->mc_id;
                            $ischildthere[] = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $cid)->count();
                            $hallsubcategory->ischild=$ischildthere;
                         }

                             } else {
                    $hallsubcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $branchid)->where('city_id','=',$city_id)->select('mc_id', 'mc_name', 'mc_img')->get();

                        foreach ($hallsubcategory as $getchildelement) {
                          $cid=$getchildelement->mc_id;
                            $ischildthere[] = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $cid)->count();
                            $hallsubcategory->ischild=$ischildthere;
                         }

                  }
               }
               $otherbarnch=$hallsubcategory;

//print_r($otherbarnch);
             ////////////////end branches code////////////////


 
	 
        return view('newWebsite.dessert-shop', compact('getsubcategories','bussinesstype','foodshopunderbugetincity','halltype','fooddateshopdetails','category_id','subcategory_id','fooddateshopgallery','fooddateshopreview','fooddateshopproducts','fooddateshopleftproduct','shop_id','branch_id','otherbarnch'));
 
    }

    function getcartproduct(Request $request){
    			$pro_id=$request->product_id;
    			$lang=Session::get('lang_file');
    			$Subdatefoodfunc = new Foodshopdate();
		$getsubproduct=$Subdatefoodfunc->getajaxdatefoodproduct($pro_id,$lang);	
			return response()->json($getsubproduct);

    }
   

    function addcartproduct(Request $request){
       // dd($request->all());
       // echo "test";
    	 // print_r($request->all());
    	 //die;
    	$categoryreid=$request->category_id;
    	$subcategoryreid=$request->subcategory_id;
    	$shopreid=$request->shop_id;
    	$branch_id=$request->branch_id;
    	$userid=Session::get('customerdata.user_id');

    	 $product = Products::where('pro_id', '=', $request->product_id)->first();
    	 //print_r($product);option_title

// dd($product);
//die;
             //   if ($request->itemqty!='') {
                    $cart = Cart::where('user_id', '=', $userid)->first();
                    //dd($cart);
                    if (!empty($cart)) {
                    //    dd('hend');


                        $attribute = CartServiceAttribute::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('product_id', $request->product_id)->where('attribute_id', $request->attribute_id)->distinct()->first();
                        //  dd($attribute);
                        if (!empty($attribute)) {
                            $cart_pro = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('product_id', $attribute->product_id)->delete();
                            $cart_option = CartOption::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('product_id', $attribute->product_id)->delete();

                            $cart_option_value = CartOptionValue::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('product_id', $attribute->product_id)->delete();
                            $cart_attribute = CartServiceAttribute::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('product_id', $attribute->product_id)->where('attribute_id', $attribute->attribute_id)->delete();
                        }

                    } else {
                      //  dd('hend');
                        $cart_data = array();
                        $cart_data['user_id'] = $userid;
                        $cart = Cart::create($cart_data); //cart entery
                    }

                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $request->cart_type;
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['product_id'] = $request->product_id;
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['attribute_id'] = $request->attribute_id;
                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry

                    $cart_product_data = array();
                    $cart_product_data['cart_type'] = $request->cart_type;
                    $cart_product_data['cart_sub_type'] = 'dessert';
                    $cart_product_data['category_id'] = $product->pro_mc_id;
                    $cart_product_data['merchant_id'] = $product->pro_mr_id;
                    $cart_product_data['shop_id'] = $product->pro_mc_id;
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $request->product_id;
                    $cart_product_data['status'] = 1;


                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    
  
                      
                    if ($request->shopby == '') {
                        $cart_product_data['quantity'] = $request->itemqty;
                        $prodisprice = $product->pro_disprice;
                    if($prodisprice ==''){    $prodisprice =   $product->pro_price; }
                    $cart_product_data['pro_price'] = $prodisprice;
                    if($request->shopby=='21'){ $unititemorderprice=$request->pr21;}else{ $unititemorderprice=$request->pr22; }

                        if($unititemorderprice!=''){
                           $cart_product_data['total_price'] = $unititemorderprice * $request->itemqty;
                        }else{
                        $cart_product_data['total_price'] = 0;
                        }
                    } else {
                       if($request->shopby=='21'){ $unititemorderprice=$request->pr21;}else{ $unititemorderprice=$request->pr22; }
                       if($unititemorderprice!=''){
                           $cart_product_data['total_price'] = $unititemorderprice * $request->itemqty;
                        }
                        $cart_product_data['quantity'] = $request->itemqty;
                    }
                    
                      

                    $cart_product = CartProduct::create($cart_product_data); //cart product entry

                    $product_options = DB::table('nm_product_option_value')->where('product_id', $request->product_id)->where('product_option_id', $request->shopby)->distinct()->first();
                    
                    if (!empty($product_options)) {
                        $cart_option_data = array();
                        $cart_option_data['cart_type'] = $request->cart_type;
                        $cart_option_data['product_option_id'] = $product->option_id;
                        $cart_option_data['cart_id'] = $cart->id;
                        $cart_option_data['product_id'] = $request->product_id;
                        CartOption::create($cart_option_data); //cart option entry

                        $cart_option_value = array();
                        $cart_option_value['cart_type'] = $request->cart_type;
                        $cart_option_value['cart_id'] = $cart->id;
                        $cart_option_value['product_id'] = $request->product_id;
                        $cart_option_value['product_option_id'] = $product_options->product_option_id;
                        $cart_option_value['product_option_value_id'] = $product_options->id;
                        $cart_option_value['quantity'] = $request->itemqty;
                         if($request->shopby=='21'){ $unititemprice=$request->pr21;}else{ $unititemprice=$request->pr22; }
                        if($request->producttype=='dish'){
                            $unititemprice=$request->pr22;
                        }
                        $cart_option_value['value'] = $unititemprice;
                        $cart_option_value['status'] = 1;
                        

                        $cart_value = CartOptionValue::create($cart_option_value); //cart option value entry
                        $cart_product = CartProduct::where('product_id', '=', $cart_value->product_id)->first();
                        

                        //$total_price['total_price'] = $cart_product->total_price + $cart_value->value * $cart_value->quantity;
                        $total_price['total_price'] =  $cart_value->value * $cart_value->quantity;
                        $total_price['quantity'] =  $cart_value->quantity;
                      
                        $update_product = CartProduct::where('product_id', '=', $cart_product->product_id)->update($total_price);

                    }

                    

              //  }

if($request->producttype=='dish'){


                    if(\Config::get('app.locale') == 'ar')
                    {
                      Session::flash('status', "وأضاف المنتج في العربة.");
                    }
                    else
                    {
                      Session::flash('status', "Product added in the cart.");
                    } 
    return redirect('dessertdishshop/'.$categoryreid.'/'.$subcategoryreid.'/'.$branch_id.'/'.$shopreid);
}else{

                    if(\Config::get('app.locale') == 'ar')
                    {
                      Session::flash('status', "وأضاف المنتج في العربة.");
                    }
                    else
                    {
                      Session::flash('status', "Product added in the cart.");
                    } 
return redirect('dessertshop/'.$categoryreid.'/'.$subcategoryreid.'/'.$branch_id.'/'.$shopreid);
}

    }


/////////////////////////////////// food type desert dish/////////////////////////


     public function fooddessertshopanddishmenu(Request $request,$halltype,$typeofhallid,$branchid,$shopid)
    {
        $halltype=$halltype;
        $subcategory_id=$halltype;
        $shop_id=$shopid;
        $branch_id=$branchid;
        $city_id=Session::get('searchdata.cityid');
        if(Session::get('searchdata.basecategoryid')!=''){
        $basecategory_id=Session::get('searchdata.basecategoryid');
        }else {
        $basecategory_id=Session::get('searchdata.maincategoryid');
        }
         $category_id=$typeofhallid;
         $typeofhallid=$typeofhallid;
         $budget=Session::get('searchdata.budget');
        $lang=Session::get('lang_file');
        
        //$request->session()->push('searchdata.typeofhallid', $category_id);
        
         $foodsessioninfo = array('maincategory_id' => $halltype, "category_id" => $category_id, "shop_id" => $shopid); 
                Session::put('fooddata', $foodsessioninfo);
        
        
        $Formbusinesstype= new Formbusinesstype();
          $bussinesstype=$Formbusinesstype->Getbussinesstype($basecategory_id,$lang);   
             $value = $request->session()->get('searchdata');
               $getsubcategories = '';
               
               $foodShoplisthall= new Foodcategorylist();
               $foodShopdate= new Foodshopdate();
             
             $foodshopunderbugetincity=$foodShoplisthall->Getfoodshoplistbycity($category_id,$city_id,$budget,$lang);
            // print_r($foodshopunderbugetincity);
             $fooddateshopdetails=$foodShopdate->Getfoodshopdate($shopid,$lang);
              $fooddateshopgallery=$foodShopdate->Getgalleryimages($shopid);
              $fooddateshopreview=$foodShopdate->GetshopReviews($shopid);
                $fooddateshopleftproduct=$foodShopdate->Getfoodshopdatedessertdishfirstproduct($shopid,$lang,45);
             $fooddateshopproducts=$foodShopdate->Getfoodshopdessertdishproduct($shopid,$lang,45);
             
              ////////////// other branches////////////
                    $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $branchid)->first(); 
                if (!empty($category)) {
                 if ($category_id != '' && $lang == 'ar_lang') {
                  $hallsubcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $branchid)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get(); 
                    foreach ($hallsubcategory as $getchildelement) {
                          $cid=$getchildelement->mc_id;
                            $ischildthere[] = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $cid)->count();
                            $hallsubcategory->ischild=$ischildthere;
                         }

                             } else {
                    $hallsubcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $branchid)->select('mc_id', 'mc_name', 'mc_img')->get();

                        foreach ($hallsubcategory as $getchildelement) {
                          $cid=$getchildelement->mc_id;
                            $ischildthere[] = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $cid)->count();
                            $hallsubcategory->ischild=$ischildthere;
                         }

                  }
               }
               $otherbarnch=$hallsubcategory;

//print_r($otherbarnch);
             ////////////////end branches code////////////////

        return view('food.foodshopdishdessert', compact('getsubcategories','bussinesstype','foodshopunderbugetincity','halltype','fooddateshopdetails','category_id','subcategory_id','fooddateshopgallery','fooddateshopreview','fooddateshopproducts','fooddateshopleftproduct','shop_id','branch_id','otherbarnch'));
 
    }


}

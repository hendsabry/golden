<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//

use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;
use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\Formbusinesstype;
use App\CartProductRent;
use App\CartProductAttribute;

//------------wisitech code end -----------//

class TravelAgencyController extends Controller
{
	 public function __construct(){
        parent::__construct();
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
	
	 public function travelagency($id)
	 {	
	 	    $category_id = $id;
        if(Session::has('customerdata.user_id')) 
        {
    	 	if(Session::get('searchdata.basecategoryid')!='')
    	 	{
                 $basecategory_id = Session::get('searchdata.basecategoryid');
        }
        else 
        {
             $basecategory_id = Session::get('searchdata.maincategoryid');
        }
        $city_id           = Session::get('searchdata.cityid');
    	 	$lang              = Session::get('lang_file');
    		$Formbusinesstype  = new Formbusinesstype();
    		$bussinesstype     = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);	 
            if(!empty($category_id)) 
            {
                if(isset($lang) && $lang == 'ar_lang') 
                {
                    $getsubcategories = Category::where('mc_status',1)->where('parent_id',$category_id)->where('city_id',$city_id)->where('mer_activity',1)->select('mc_id','mc_name_ar as mc_name','mc_img')->paginate(9)->appends(request()->query());
                    foreach($getsubcategories as $val) 
                    {
                      $isproduct = DB::table('nm_product')->where('pro_status',1)->where('pro_mc_id',$val->mc_id)->count();
                      if($isproduct > 0)
                      {
                        $val->isproduct = $isproduct;
                      }
                      else
                      {
                        $val->isproduct = '';
                      }
                     }
                } 
                else 
                {                       
                    $getsubcategories = Category::where('mc_status',1)->where('parent_id',$category_id)->where('city_id',$city_id)->where('mer_activity',1)->select('mc_id','mc_name','mc_img')->paginate(9)->appends(request()->query());
                    foreach($getsubcategories as $val) 
                    {
                      $isproduct = DB::table('nm_product')->where('pro_status',1)->where('pro_mc_id',$val->mc_id)->count();
                      if($isproduct > 0)
                      {
                        $val->isproduct = $isproduct;
                      }
                      else
                      {
                        $val->isproduct = '';
                      }
                     }
                }
            }
            return view('travel-angencies.travelagences',compact('getsubcategories','bussinesstype','category_id')); 
          } 
          else
          {
           return Redirect::to('login-signup');
          }      
     }
	
	 public function travelagencydetail($id,$sid)
	 {	        
	    $category_id = $id;
	    $subcat_id   = $sid;
        if(Session::has('customerdata.user_id')) 
        {
    	 	if(Session::get('searchdata.basecategoryid')!='')
    	 	{
                 $basecategory_id = Session::get('searchdata.basecategoryid');
            }
            else 
            {
                 $basecategory_id = Session::get('searchdata.maincategoryid');
            }
            $city_id = Session::get('searchdata.cityid');
            $lang = Session::get('lang_file');
    		$Formbusinesstype = new Formbusinesstype();
    		$bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);            
            if(!empty($subcat_id)) 
            {
                if($category_id != '' && $lang == 'ar_lang') 
                {
                    $vendorinfo = Category::select('mc_id','vendor_id','mc_name_ar as mc_name','mc_discription_ar as mc_discription','city_id','occasion_id','mc_img','mc_video_url','address_ar as address','mc_video_description as mc_video_description_ar','google_map_address','terms_conditions_ar as terms_conditions','longitude','latitude')->where('parent_id',$category_id)->where('mc_id',$subcat_id)->first();
                    $vendor_id = $vendorinfo->vendor_id;
                    $alldata = Products::where('pro_status',1)->where('pro_mr_id',$vendor_id)->where('pro_mc_id',$subcat_id)->select('pro_id','pro_title_ar as pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc_ar as pro_desc','notes_ar as notes','pro_price','Insuranceamount','pro_disprice')->paginate(9)->appends(request()->query());
                } 
                else 
                {   
                    $vendorinfo = Category::select('mc_id','vendor_id','mc_name','mc_discription','city_id','occasion_id','mc_img','mc_video_url','address','mc_video_description','google_map_address','terms_conditions','longitude','latitude')->where('parent_id',$category_id)->where('mc_id',$subcat_id)->first(); 
                    $vendor_id = $vendorinfo->vendor_id;                   
                    $alldata = Products::where('pro_status',1)->where('pro_mr_id',$vendor_id)->where('pro_mc_id',$subcat_id)->select('pro_id','pro_title','pro_title_ar','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc','notes','pro_price','Insuranceamount','pro_disprice')->paginate(9)->appends(request()->query());
                }
            }
            $allreview =   DB::table('nm_review')->where('shop_id',$subcat_id)->where('review_type','shop')->where('status',1)->paginate(2);
            $vendordetails = $vendorinfo;
            $productlist   = $alldata;
            return view('newWebsite.travel',compact('vendordetails','productlist','bussinesstype','category_id','subcat_id','allreview'));
        } 
        else
        {
          return Redirect::to('login-signup');
        }      
     }
     
    function getTravelAgencyInfo(Request $request)
    {
        $id         = $request->product_id;
        $vendorId   = $request->vendor_id;
        $lang       = Session::get('lang_file');
         $Current_Currency = Session::get('currency');
        if($id != '' && $lang == 'ar_lang') 
        {
            $productdateshopinfo = DB::table('nm_product')->where('pro_status', 1)->where('pro_id', $id)->select('pro_id','pro_title_ar as pro_title','pro_Img','pro_desc_ar as pro_desc','pro_mr_id','pro_mc_id','pro_status','notes_ar as notes','pro_price','Insuranceamount','pro_disprice','pro_discount_percentage')->orderBy('pro_title')->get();   
        }
        else
        {
           $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_id',$id)->select('pro_id','pro_title','pro_Img','pro_desc','pro_mr_id','pro_mc_id','pro_status','notes','pro_price','Insuranceamount','notes','pro_disprice','pro_discount_percentage')->orderBy('pro_title')->get();       
        }



foreach ($productdateshopinfo as $productdateshopinfod) {
  $PriceNew = currency($productdateshopinfod->pro_price, 'SAR',$Current_Currency, $format = false);
    $PriceDis = currency($productdateshopinfod->pro_disprice, 'SAR',$Current_Currency, $format = false);
 $productdateshopinfo->pro_price  = $PriceNew;
  $productdateshopinfo->pro_disprice  = $PriceDis;
 
}
 //dd($productdateshopinfo);
      $lang     = Session::get('lang_file');
               if($lang != '' && $lang == 'ar_lang') 
               { 
                $productatrrlocation = DB::table('nm_product_attribute')->where('product_id',$id)->where('vendor_id',$vendorId)->where('attribute_title','Location')->select('attribute_title','value_ar as value')->first();
               }else{

                $productatrrlocation = DB::table('nm_product_attribute')->where('product_id',$id)->where('vendor_id',$vendorId)->where('attribute_title','Location')->select('attribute_title','value')->first();
      }
           if(isset($productatrrlocation->value) && $productatrrlocation->value!='')
           {             
             $productatrrlocation1        = $productatrrlocation->value;
           }
           else
           {
             $productatrrlocation1        = 1;
           }
           
           $productatrrpackagedate = DB::table('nm_product_attribute')->where('product_id',$id)->where('vendor_id',$vendorId)->where('attribute_title','Package Date')->select('attribute_title','value')->first();

           if(isset($productatrrpackagedate->value) && $productatrrpackagedate->value!='')
           {  
               $todaydate=date('Y-m-d'); 
               $ftoday=strtotime($todaydate);  
              $allDate = explode(',',$productatrrpackagedate->value);
              $html ='<select name="package_sechedule" id="package_sechedule" required><option value="">Select</option>';
              foreach($allDate as $val)
              {
                $packagetime=strtotime($val);
                   if($packagetime>$ftoday){ 
                $html .='<option value="'.$val.'">'.date('d M Y h:i A',strtotime($val)).'</option>';
                   }
              }
              $html .='</select>';
              $productatrrpackagedate2 = $html;
           }
           else
           {
              $productatrrpackagedate2     = 2;
           }

           if($lang != '' && $lang == 'ar_lang') 
               { 
                $productatrrpackageduration = DB::table('nm_product_attribute')->where('product_id',$id)->where('vendor_id',$vendorId)->where('attribute_title','Package Duration')->select('attribute_title','value_ar as value')->first();
               }else{

           $productatrrpackageduration = DB::table('nm_product_attribute')->where('product_id',$id)->where('vendor_id',$vendorId)->where('attribute_title','Package Duration')->select('attribute_title','value')->first();
           }
           if(isset($productatrrpackageduration->value) && $productatrrpackageduration->value!='')
           {             
              $productatrrpackageduration3 = $productatrrpackageduration->value;
           }
           else
           {
              $productatrrpackageduration3     = 3;
           }
           if($lang != '' && $lang == 'ar_lang') 
               { 
                  $productatrrserviceday = DB::table('nm_product_attribute')->where('product_id',$id)->where('vendor_id',$vendorId)->where('attribute_title','Service Days')->select('attribute_title','value_ar as value')->first();
               }else{
           $productatrrserviceday = DB::table('nm_product_attribute')->where('product_id',$id)->where('vendor_id',$vendorId)->where('attribute_title','Service Days')->select('attribute_title','value')->first();
           }
           if(!empty($productatrrserviceday->value))
           {           
             $productatrrserviceday4      = @$productatrrserviceday->value;
           }
           else
           {
              $productatrrserviceday4     = 4;
           }
              if($lang != '' && $lang == 'ar_lang') 
               { 
                  $productatrrextraservice = DB::table('nm_product_attribute')->where('product_id',$id)->where('vendor_id',$vendorId)->where('attribute_title','Extra Service')->select('attribute_title','value_ar as value')->first(); 
               }else{
               $productatrrextraservice = DB::table('nm_product_attribute')->where('product_id',$id)->where('vendor_id',$vendorId)->where('attribute_title','Extra Service')->select('attribute_title','value')->first();  
               }

           
           if(isset($productatrrextraservice->value) && $productatrrextraservice->value!='')
           {            
              $productatrrextraservice5    = $productatrrextraservice->value;
           }
           else
           {
              $productatrrextraservice5     = 5;
           }



        $productdatesshopfullinfo = array('productdateshopinfo'=>$productdateshopinfo,'productatrrlocation'=>$productatrrlocation1,'productatrrpackagedate'=>$productatrrpackagedate2,'productatrrpackageduration'=>$productatrrpackageduration3,'productatrrserviceday'=>$productatrrserviceday4,'productatrrextraservice'=>$productatrrextraservice5);
        return response()->json($productdatesshopfullinfo);
    }

    function addtocarttravelandcarproduct(Request $request)
    {
        //echo '<pre>';print_r($_REQUEST);die;
        $user_id            = Session::get('customerdata.user_id');
        
        $category_id        = $request->category_id;
        $subcat_id          = $request->subcat_id;
        $itemqty            = $request->itemqty;
        $product_id         = $request->product_id;
        $priceId            = $request->priceId;
        $vendor_id          = $request->vendor_id;
        $package_sechedule  = $request->package_sechedule;
        $actiontype         = $request->actiontype;
        $cart_type          = $request->cart_type;
        $cart_sub_type      = $request->cart_sub_type;
        $bookingdate        = $request->bookingdate;
        $returningdate      = $request->returningdate;
        
        $product            = DB::table('nm_product')->where('pro_id',$product_id)->where('pro_mc_id',$subcat_id)->first();
        //$product_options    = DB::table('nm_product_attribute')->where('product_id',$product_id)->where('id',$productoptionval)->distinct()->first();
        $cart               = Cart::where('user_id',$user_id)->first();
        if (!empty($cart)) 
        {
           $isallreadybooked= CartProductRent::where('cart_id',$cart->id)->where('return_date',$package_sechedule)->count();
           if($isallreadybooked>0){
                 $cart_pro      = CartProduct::where('cart_type',$cart_type)->where('cart_id',$cart->id)->delete();
               $cart_pro      = CartOption::where('cart_type',$cart_type)->where('cart_id',$cart->id)->delete();
                 $cart_pro      = CartOptionValue::where('cart_type',$cart_type)->where('cart_id',$cart->id)->delete();
                 $cart_pro_rent = CartProductRent::where('cart_id',$cart->id)->delete();
               $CartProductAttribute = CartProductAttribute::where('cart_id',$cart->id)->delete();
            }
            
        } 
        else 
        {
            $cart_data = array();
            $cart_data['user_id'] = $user_id;
            $cart = Cart::create($cart_data); //cart entery
        }

                    $cart_product_data                   = array();
                    $cart_product_data['cart_id']        = $cart->id;
                    $cart_product_data['merchant_id']    = $vendor_id;
                    $cart_product_data['cart_type']      = $cart_type;
                    $cart_product_data['cart_sub_type']  = $cart_sub_type;
                    $cart_product_data['product_id']     = $product_id;
                    $cart_product_data['shop_id']        = $subcat_id;

                    $cart_product_data['pro_title']        = $product->pro_title;
                    $cart_product_data['pro_title_ar']        = $product->pro_title_ar;
                    $cart_product_data['pro_Img']        = $product->pro_Img;
                      if(isset($product->pro_discount_percentage) && $product->pro_discount_percentage>0){ $rprice=$product->pro_disprice; }else{ $rprice=$product->pro_price;  }

                    $cart_product_data['pro_price']        = $rprice;


                    $cart_product_data['status']         = 1;                    
                    $cart_product_data['category_id']    = $category_id;
                    $cart_product_data['review_type']    = 'shop';
                    $cart_product_data['quantity']       = $itemqty;
                    $cart_product_data['total_price']    = ($priceId*$itemqty);
                   
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                    $cartProID = $cart_product->id;
                    //if(isset($productoptionval)) 
                    //{
                            $cart_option_data                       = array();
                            $cart_option_data['cart_type']          = $cart_type;
                            $cart_option_data['product_option_id']  = '';
                            $cart_option_data['cart_id']            = $cart->id;
                            $cart_option_data['quantity']           = $itemqty;
                            $cart_option_data['product_id']         = $product_id;
                            CartOption::create($cart_option_data); //cart option entry
                        
                            $cart_option_value                      = array();
                            $cart_option_value['cart_type']         = $cart_type;
                            $cart_option_value['cart_id']           = $cart->id;
                            $cart_option_value['product_id']        = $product_id;
                            $cart_option_value['quantity']          = $itemqty;
                            $cart_option_value['product_option_id'] = '';
                            $cart_option_value['product_option_value_id'] = '';                            
                            $cart_option_value['value'] = '';
                            $cart_option_value['status'] = 1;
                            
                            CartOptionValue::create($cart_option_value); //cart option value entry
                    //} 
                            if($bookingdate!='')
                            {
                              $cart_booking_data=array();
                              $cart_booking_data['cart_id']            = $cart->id;                              
                              $cart_booking_data['product_id']         = $product_id;
                              $cart_booking_data['cart_type']           = $cart_type;
                              $cart_booking_data['attribute_title']      = 'bookingdate';
                              $cart_booking_data['attribute_title_ar']   = 'bookingdate';
                              $cart_booking_data['value']           = $bookingdate;
                              CartProductAttribute::create($cart_booking_data);
                            }
                            if($returningdate!=''){
                              $cart_returning_data=array();
                              $cart_returning_data['cart_id']            = $cart->id;                              
                              $cart_returning_data['product_id']         = $product_id;
                              $cart_returning_data['cart_type']           = $cart_type;
                              $cart_returning_data['attribute_title']      = 'returningdate';
                              $cart_returning_data['attribute_title_ar']   = 'returningdate';
                              $cart_returning_data['value']           = $returningdate;
                              CartProductAttribute::create($cart_returning_data);
                            }


if($bookingdate!='' && $returningdate!='')
{

$cart_product_rent = array();
$cart_product_rent['cart_type']      = $cart_type;
$cart_product_rent['cart_id']        = $cart->id;
$cart_product_rent['product_id']     = $product_id;
$cart_product_rent['rental_date']    = $bookingdate;
$cart_product_rent['rental_time']    = '';
$cart_product_rent['return_date']    = $returningdate;
$cart_product_rent['service_id']     = $subcat_id;
$cart_product_rent['quantity']       = 1;
CartProductRent::create($cart_product_rent); //internal food entery



}


                    if($package_sechedule || $package_sechedule != '') 
                    {
                            $datetime = explode(' ',$package_sechedule);
                            $time = $datetime[1].' '.$datetime[2];  
                            $cart_product_rent = array();
                            $cart_product_rent['cart_type']      = $cart_type;
                            $cart_product_rent['cart_id']        = $cart->id;
                            $cart_product_rent['product_id']     = $product_id;
                            $cart_product_rent['rental_date']    = date("d M Y", strtotime($datetime[0]));
                            $cart_product_rent['rental_time']    = $time;
                            $cart_product_rent['return_date']    = $package_sechedule;
                            $cart_product_rent['service_id']     = $subcat_id;
                            $cart_product_rent['quantity']       = $itemqty;
                            $cart_product_rent['cart_product_id']       = $cartProID;
                            CartProductRent::create($cart_product_rent); //internal food entery
                    }
                    if($actiontype=='travelagencydetail')
                    {
                        if(\Config::get('app.locale') == 'ar')
                        {
                           Session::flash('status', "وأضاف إلى السلة بنجاح");
                        }
                        else
                        {
                           Session::flash('status', "Added to Cart Successfully");
                        }

                    }
                    else
                    {
                        if(\Config::get('app.locale') == 'ar')
                        {
                           Session::flash('status', "وأضاف إلى السلة بنجاح");
                        }
                        else
                        {
                           Session::flash('status', "Added to Cart Successfully");
                        }
                    }                  
                    
                    // language for display message //                
                    return Redirect::back();
     return redirect($actiontype.'/'.$category_id.'/'.$subcat_id);
    }

public function getChangedprice(Request $request)
{
  $Current_Currency  = Session::get('currency'); 
  if($Current_Currency =='') { 
  $Current_Currency = 'SAR'; 
  } 
  $price = $request->price;
  $updatedprice = currency($price, 'SAR',$Current_Currency, $format = false);
  return $updatedprice;

}


public function getmultipleImages(Request $request)
{
    
$pro_id = $request->product_id;
 return view('includes.product_multiimages',compact('pro_id'));
 }
     
}

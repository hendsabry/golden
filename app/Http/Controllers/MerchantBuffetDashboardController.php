<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Models;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon;
use File;
use App\Reviewrating;
use App\Categorylist;
use Intervention\Image\ImageManagerStatic as Image; 
use Auth;
use Lang; 
use App\City;
use App\Register;
 use App\Settings;
use App\Merchant;
use App\NmRelatedService;
use App\NmproductOptionValue;
use App\ProductsMerchant;
use App\NmProductToOptionPrice;
use App\NmServicesAttribute;
 

class MerchantBuffetDashboardController extends Controller
{
    	public function __construct(){
            parent::__construct();
            // set admin Panel language
            $this->middleware(function ($request, $next) {
                        $this->setLanguageLocaleMerchant();
                        Session::get('mer_lang_code');
                        return $next($request);
           });
     }
    
 
  //Buffet Dashboard
  public function buffetListBranch(Request $request)
  {
      if(Session::has('merchantid')) 
      {
          $mer_id   = Session::get('merchantid');
          $parent_id = request()->id;
          $sid = request()->sid;
          $status = $request->status;
          $search = $request->search;
          $cityname = $request->cityname;
          $getPagelimit = config('app.paginate');

          if(Session::get('LoginType') =='4')
           {
            //= CHECK THE MARCHANT ACCESS ==//
             $merchantManagerid = Session::get('merchant_managerid');
            $alldata = Categorylist::where('parent_id',$sid)->where('branch_manager_id',$merchantManagerid)->where('vendor_id',$mer_id);
             //= CHECK THE MARCHANT ACCESS ==//
            }
            else
            {
             $alldata = Categorylist::where('parent_id',$sid)->where('vendor_id',$mer_id);  
            }

           



                if($cityname != '')
                {
                $alldata = $alldata->where('city_id',$cityname); 
                }
                if($status != '')
                {
                $alldata = $alldata->where('mc_status',$status);
                }
                 if($search !='')
                {
                    $mer_selected_lang_code = Session::get('mer_lang_code'); 
                    if($mer_selected_lang_code !='en')
                    {
                    $alldata = $alldata->where('mc_name_ar','LIKE','%' .$search.'%');
                    }
                    else
                    {
                    $alldata = $alldata->where('mc_name','LIKE','%' .$search.'%');
         
                    }
              }  

                $alldata = $alldata->orderBy('mc_id','desc')->paginate($getPagelimit)->appends(request()->query());




          $servicedata =Categorylist::where('mc_id',$sid)->where('vendor_id',$mer_id)->get();
          $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
          return view('sitemerchant.buffet.buffet-list-branch', compact('merchantheader','merchantfooter','parent_id','sid','alldata'));       
      } 
      else 
      {
          return Redirect::to('sitemerchant');
      }
   }



  //Buffet Appetizers
  public function buffetAddCategory(Request $request)
  {
      if(Session::has('merchantid')) 
      {
          $mer_id   = Session::get('merchantid');
          $parent_id = request()->id;
          $sid = request()->sid;
          $itemid = request()->itemid;
          $autoid = request()->autoid;
          $datafirst = NmServicesAttribute::where('vendor_id',$mer_id)->where('id',$autoid)->first();
         

              
          $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
          return view('sitemerchant.buffet.add-buffet-menu-category', compact('merchantheader','merchantfooter','parent_id','sid','itemid','datafirst','autoid'));       
      } 
      else 
      {
          return Redirect::to('sitemerchant');
      }
  } 

  //Store buffet Category
   public function storeCategory(Request $request)

        {
             $mer_id   = Session::get('merchantid');
             $parent_id = $request->parent_id;
             $sid = $request->sid;
             $itemid = $request->itemid;
             
             $autoid = $request->autoid;
             if($autoid!=''){
             $categorysave = NmServicesAttribute::where('id',$autoid)->first();
             } else {  
             $categorysave = new NmServicesAttribute; 
             }
             

              if($request->file('catimage')){ 
                  $file = $request->catimage;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                  $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                   $img->save('uploadimage/categoryimage' . '/'. $thumbName);
                   $file->move('uploadimage/categoryimage/', $Image);
                   $categorysave->image = url('').'/uploadimage/categoryimage/'.$thumbName;
                } 
             $categorysave->attribute_title  = $request->category_name;
             $categorysave->attribute_title_ar   = $request->category_name_ar;
             $categorysave->vendor_id   = $mer_id;
             $categorysave->services_id   = $itemid;
             $categorysave->parent   = 0;
             $categorysave->status  = 1;
             if($categorysave->save()){ 
              // language for display message //
             if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "تم حفظ قائمة الفئة بنجاح");
             }
             else
             {
             Session::flash('message', "Category menu successfully saved");
             }
     // language for display message //
             
            return redirect()->route('buffet-menu-list',['id' =>$parent_id,'sid' =>$sid,'itmemid' =>$itemid]);
             //return redirect()->route('menulist',['hid' => $hid,'bid' =>$bid]);
             }


        }
 //End buffet category   

  //Buffet Main Cource
  public function buffetMainCource(Request $request)
  {
      if(Session::has('merchantid')) 
      {
          $mer_id   = Session::get('merchantid');
          $parent_id = request()->id;

          $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
          return view('sitemerchant.buffet.buffet-main-cource', compact('merchantheader','merchantfooter','parent_id'));       
      } 
      else 
      {
          return Redirect::to('sitemerchant');
      }
  }  

  //Buffet Menu
  public function buffetMenu(Request $request)
  {
      if (Session::has('merchantid')) 
             {
                $parent_id = request()->id;
                $sid = request()->sid;
                $itemid = request()->itemid;
                $status = $request->status;
                $search = $request->search;
                $getPagelimit = config('app.paginate');
                $mer_id   = Session::get('merchantid');

 $menulist =  NmServicesAttribute::where('vendor_id',$mer_id)->where('services_id',$itemid )->orderBy('id', 'desc');


                   if($status!='')
                {
                  $menulist =  $menulist->where('status',$status);
                }
               if($search !='')
                {      
                     $mc_name='attribute_title';
                     $mer_selected_lang_code = Session::get('mer_lang_code'); 
                   if($mer_selected_lang_code !='en')
                   {

                    
                    $menulist =  $menulist->where('attribute_title_ar','LIKE','%' .$search.'%');
                   }
                   else
                   {
                    $menulist =  $menulist->where('attribute_title','LIKE','%' .$search.'%');
                   }
                                      
           
                } 

            

 $menulist =  $menulist->paginate($getPagelimit)->appends(request()->query());

                $this->setLanguageLocaleMerchant();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
            return view('sitemerchant.buffet.buffet-menu-list', compact('merchantheader','merchantfooter','menulist','status','search','sid','parent_id','itemid'));       
            } else {
                return Redirect::to('sitemerchant');
            } 
         }
  //Buffet Orders
  public function buffetOrders(Request $request)
  {
      if(Session::has('merchantid')) 
      {
          $mer_id   = Session::get('merchantid');
          $parent_id = request()->id;
          $hid=request()->sid;
          $itemid=request()->itemid;
           $sid=$request->sid;
                 $searchkeyword=$request->searchkeyword;
                 $date_to=$request->date_to;
                 $End_Date=$request->from_to;
                 $order_days=$request->order_days;
                 $status=$request->status;                
                 $serachfirstfrm=$request->serachfirstfrm;


                  $getorderedproducts = DB::table('nm_order_product')->where('product_type','food')->where('product_sub_type','buffet');
                  if($searchkeyword!=''){
                  $getorderedproducts = $getorderedproducts->where('order_id',$searchkeyword);
                  }

                  if($status!=''){
                  $getorderedproducts = $getorderedproducts->where('status',$status);
                  } 

                  if($date_to!='' && $End_Date!=''){
                  $getorderedproducts = $getorderedproducts->whereDate('created_at', '>=', $date_to)->whereDate('created_at', '<=', $End_Date);
                  } 

                  $getorderedproducts = $getorderedproducts->where('merchant_id',$mer_id)->where('category_id',$itemid)->groupBy('order_id')->orderBy('created_at','DESC')->get();


          $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
          return view('sitemerchant.buffet.buffet-orders', compact('merchantheader','merchantfooter','parent_id','getorderedproducts','hid','mer_id'));       
      } 
      else 
      {
          return Redirect::to('sitemerchant');
      }
  } 



public function getorderdetail(Request $request){
      if (Session::has('merchantid')) 
             {

              $mer_id   = Session::get('merchantid');
                 $id = $request->id;
                 $opid = $request->opid;
                 $oid=$request->oid;
                 $cusid=$request->cusid;
                 $sid=$request->sid;
                  $itemid=$request->itemid;

              $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');
             
              $getbuffetfood_item = DB::table('nm_order_external_food_dish')->where('cus_id',$cusid)->where('category_id',$itemid)->where('order_id',$oid)->get();
              

              $gethallInfoC = DB::table('nm_order_product')->where('cus_id',$cusid)->where('product_sub_type','hall')->where('order_id',$oid)->count();


               $hallName = '';
               $hallLocation = '';   
               if($gethallInfoC >=1)
              {
              $gethallInfo = DB::table('nm_order_product')->where('cus_id',$cusid)->where('product_sub_type','hall')->where('order_id',$oid)->first();
              $proID = $gethallInfo->product_id;
              $CatID = $gethallInfo->category_id;

              $hallsC = DB::table('nm_product')->where('pro_id',$proID)->count();
                  if($hallsC >=1)
                  {
                  $halls = DB::table('nm_product')->where('pro_id',$proID)->first();
                  $Challs = DB::table('nm_category')->where('mc_id',$CatID)->first();
                  $hallName = $halls->pro_title;
                  $hallLocation = $Challs->address;
                  }              
              }
                

                return view('sitemerchant.buffet.buffet-orderdetail', compact('merchantheader','merchantfooter','id','hid','sid','getbuffetfood_item','hallName','hallLocation'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
  }

  //Buffet Reviews And Comments
  public function buffetReviewsAndComments(Request $request)
  {
      if(Session::has('merchantid')) 
      {
          
      $getPagelimit = config('app.paginate');
                $parent_id = request()->id;
                $sid = request()->sid;
                $itemid = request()->itemid;

$merid  = Session::get('merchantid');
    $this->setLanguageLocaleMerchant();
    $mer_id              = Session::get('merchantid');             
    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
   
    $reviewrating = Reviewrating::where('vandor_id',$mer_id)->where('shop_id',$itemid)->where('review_type','shop')->orderBy('created_at','desc')->paginate($getPagelimit);
    return view('sitemerchant.buffet.buffet-reviews-and-comments', compact('merchantheader','merchantfooter','reviewrating')); 

 

      } 
      else 
      {
          return Redirect::to('sitemerchant');
      }
  }
    //Buffet add Branch
    public function buffetAddBranch(Request $request)
  {
      if(Session::has('merchantid')) 
      {
          $mer_id   = Session::get('merchantid');
          $parent_id = request()->id;

          $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
          return view('sitemerchant.buffet.buffet-add-branch', compact('merchantheader','merchantfooter','parent_id'));       
      } 
      else 
      {
          return Redirect::to('sitemerchant');
      }
  }
    //Buffet Buffet Info
   public function buffetInfo(Request $request)
  {
      if(Session::has('merchantid')) 
      {
          $mer_id   = Session::get('merchantid');
          $parent_id = request()->id;
          $sid = request()->sid;

          $itemid = request()->itemid;


          $shopName = Categorylist::where('mc_id',$sid)->where('vendor_id',$mer_id)->first();
          $fetchdata = Categorylist::where('mc_id',$itemid)->where('vendor_id',$mer_id)->first();


           $city = City::where('ci_con_id',10)->get();
           $manager = Merchant::where('vendor_parent_id',$mer_id)->where('mer_staus',1)->get();
           
          $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
          return view('sitemerchant.buffet.buffet-info', compact('merchantheader','merchantfooter','parent_id','sid','city','manager','fetchdata','shopName'));       
      } 
      else 
      {
          return Redirect::to('sitemerchant');
      }
  }
  
  //Buffet store branch
 public function storeBranch(Request $request)
    {
               $merid  = Session::get('merchantid');
               $sid = $request->sid;
               $itemid = $request->itemid;

               if($itemid){
                  $savebranch =Categorylist::where('mc_id',$itemid)->first();
                } else { 
                 $savebranch = new Categorylist;
               }
          if($request->file('branchimage')){ 
                  $file = $request->branchimage;  

                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                 $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 220;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('buffetimage' . '/'. $thumbName);
                  $file->move('buffetimage/', $Image);
                  $savebranch->mc_img = url('').'/buffetimage/'.$thumbName;
                }
                //Addressimage
                if($request->file('address_image')){ 
                  $file = $request->address_image;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                  $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('buffetimage' . '/'. $thumbName);
                  $file->move('buffetimage/', $Image);
                  $savebranch->address_image = url('').'/buffetimage/'.$thumbName;
                }
              //Termaand condition
               if(!empty($request->mc_tnc)){ 
                  $file = $request->mc_tnc;
                  $orgname =  $file->getClientOriginalName();
                  $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                  $extension = $file->getClientOriginalExtension();
                  $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                  $file->move('./public/assets/storeimage/', $filename_c);                      
                  $Cname =   url('').'/public/assets/storeimage/'.$filename_c;
                  $savebranch->terms_conditions =$Cname;
                  
               }
               else {
                    $orgname = $request->tmcvalue; 
               }



               //Arabic documents
               //Termaand condition
               if(!empty($request->mc_tncar)){ 
                  $file = $request->mc_tncar;
                  $orgnamear =  $file->getClientOriginalName();
                  $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                  $extension = $file->getClientOriginalExtension();
                  $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                  $file->move('./public/assets/storeimage/', $filename_c);                      
                  $Cnamear =   url('').'/public/assets/storeimage/'.$filename_c;
                  $savebranch->terms_conditions_ar =$Cnamear;
                  
               }
               else {
                    $orgnamear = $request->tmcvaluear; 
               }
                 


                  $savebranch ->vendor_id = $merid;
                  $savebranch ->google_map_address = $request->google_map_address;
                  $savebranch ->longitude = $request->longitude;
                  $savebranch ->latitude = $request->latitude; 
                  $savebranch ->insurance_amount = $request->Insuranceamount;
                  $savebranch ->address = $request->address;
                  $savebranch ->address_ar = $request->address_ar;
                  $savebranch ->city_id = $request->city_id;
                  $savebranch ->parent_id = $sid;
                  $savebranch ->mc_name = $request->mc_name;
                  $savebranch ->mc_name_ar = $request->mc_name_ar;
                  $savebranch ->mc_discription = $request->about;
                  $savebranch ->mc_discription_ar = $request->about_ar;
                  $savebranch ->city_id = $request->city_id;
                  $savebranch ->terms_condition_name = $orgname;
                  $savebranch ->terms_condition_name_ar = $orgnamear;
                  $savebranch->mc_status  = 1;
                  $savebranch->branch_manager_id  =$request->manager;

                if($savebranch->save()){
                  $id = $request->parent_id;

if($itemid==''){
 $itemid = $savebranch->mc_id;
}
            if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "فرع حفظ بنجاح");
             }
             else
             {
             Session::flash('message', "Branch successfully saved");
             }

                 return redirect()->route('buffet-info',['id' => $id,'sid' =>$sid,'itmemid' =>$itemid]);
                }   
            }














  //Buffet Buffet Dish
   public function buffetDish(Request $request)
  {
      if(Session::has('merchantid')) 
      {
          $mer_id   = Session::get('merchantid');
          $getPagelimit = config('app.paginate');
               //SEARCH
                $parent_id = request()->id;
                $sid =  request()->sid;
                $itemid =  request()->itemid;
                
                $category = $request->category;
                $search = $request->search;
                $status = $request->status;
                $mer_id   = Session::get('merchantid');
                $mer_id   = Session::get('merchantid');
                 $containerlist = ProductsMerchant::where('pro_mr_id',$mer_id)->where('pro_mc_id',$itemid);
                 $menucat = NmServicesAttribute::where('vendor_id',$mer_id)->where('services_id',$itemid)->get();

                  if($category!='')
                {
               $containerlist = $containerlist->where('attribute_id',$category);

                }

                  if($status !='')
                  {             
                  $containerlist = $containerlist->where('pro_status',$status);
                  }


                  if($search !='')
                {
                    
                     $mer_selected_lang_code = Session::get('mer_lang_code'); 
                   if($mer_selected_lang_code !='en')
                   {
                    
                    $containerlist = $containerlist->where('pro_title_ar','LIKE','%' .$search.'%');
                   }
                                      
                  $containerlist = $containerlist->where('pro_title','LIKE','%' .$search.'%');
                }
                else {

               $containerlist =  $containerlist->orderBy('pro_id', 'desc');

            } 

          $containerlist =  $containerlist->orderBy('pro_id','desc')->paginate($getPagelimit)->appends(request()->query());
          $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
          return view('sitemerchant.buffet.buffet-dish', compact('merchantheader','merchantfooter','parent_id','sid','itemid','containerlist','menucat','category','search'));       
      } 
      else 
      {
          return Redirect::to('sitemerchant');
      }
  }
  
  //Buffet Buffet Add Dish
   public function buffetAddDish(Request $request)
  {
      if(Session::has('merchantid')) 
      {
          $mer_id   = Session::get('merchantid');
          $parent_id = request()->id;
           $itemid = request()->itemid;
           $sid = request()->sid;
           $autoid = request()->autoid;
           $fetchfirstdata = ProductsMerchant::where('pro_mr_id',$mer_id)->where('pro_id',$autoid)->first();
           $menucat = NmServicesAttribute::where('vendor_id',$mer_id)->where('services_id',$itemid)->get();

           $containerpackageC = NmproductOptionValue::where('vandor_id',$mer_id)->where('product_id',$itemid)->where('status',1)->orderBy('option_title','ASC')->count();

           if($containerpackageC < 1 )
           {
                if (\Config::get('app.locale') == 'ar'){
                Session::flash('message', "يرجى إضافة الحاوية أولاً ");
                }
                else
                {
                Session::flash('message', "Please add container first");
                }
   return redirect()->route('buffet-add-container',['id'=>$parent_id,'sid'=>$sid,'itemid'=>$itemid]);
            
        
           }

            $containerpackage = NmproductOptionValue::where('vandor_id',$mer_id)->where('product_id',$itemid)->where('status',1)->orderBy('option_title','ASC')->get();


           $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
           $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
          return view('sitemerchant.buffet.buffet-add-dish', compact('merchantheader','merchantfooter','parent_id','itemid','sid','menucat','containerpackage','fetchfirstdata','autoid'));       
      } 
      else 
      {
          return Redirect::to('sitemerchant');
      }
  }
  
  //Store buffet Dish
  public function storeBuffetDish(Request $request)
        {
             
              $parent_id = $request->parent_id;
              $sid = $request->sid;
              $itemid = $request->itemid;
              $mer_id   = Session::get('merchantid');
              $autoid = $request->autoid;



               //STORE DISH NAME IN PRODUCT TABLE
              if($autoid!='') {
                  $dishsave =ProductsMerchant::where('pro_id',$autoid)->first();
                 } else {
                 $dishsave = new ProductsMerchant;
                }
              
             if($request->file('dish_image')){ 
                      $file = $request->dish_image;  
                      $extension = $file->getClientOriginalExtension();
                      $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                      $imageRealPath  =   $file->getRealPath();
                      $thumbName      =   'thumb_'. $Image;       
                      $img = Image::make($imageRealPath); // use this if you want facade style code
                      $thumb_width = 150;
                     list($width,$height) = getimagesize($imageRealPath);
                        $thumb_height = ($thumb_width/$width) * $height;
                        $img->resize($thumb_width,$thumb_height);
                        $img->save('buffetimage' . '/'. $thumbName);
                        $file->move('buffetimage/', $Image);
                        $dishsave->pro_Img = url('').'/buffetimage/'.$thumbName;
                 } 
                     $dishsave->pro_mc_id     = $itemid;
                     $dishsave->attribute_id  = $request->menucategory;
                     $dishsave->pro_title  = $request->dish_name;
                     $dishsave->pro_title_ar  = $request->dish_name_ar;
                     $dishsave->pro_desc  = $request->about;
                     $dishsave->pro_desc_ar  = $request->about_ar;
                     $dishsave->pro_mr_id  = $mer_id;
                     $dishsave->pro_status  = 1;
                     $dishsave->option_id  = 22;

                     // $dishsave->pro_disprice  = $request->discount;
                     
                     $dishsave->save();
                     if($autoid=='') { 
                     $proid = DB::getPdo()->lastInsertId();
                      } else {
                       $proid = $autoid; 
                      }
                    if($proid) {
                       //$dishpricesave = new NmProductToOptionPrice;
                         $productid         =      $proid;
                         $product_option_id =      22;
                         $container_id      =      $request->container_id;
                         $containerprice    =      $request->containerprice;
                        
                         $containerprice = $request->containerprice;
                         $container_id = $request->container_id;
                         $discount = $request->discount;

                         DB::table('nm_product_to_option_price')->where('product_id',$autoid)->delete();  


                         for($i=0;$i<count($containerprice); $i++)
                          {
                            $Price = $containerprice[$i];
                            $ContainerID = $container_id[$i];
                            $Discount = $discount[$i];

                            if($Discount!='' && $Discount!='0') {
                                         $discountprice =   $Price-($Price * ($Discount/100));
                                        } else {
                                           $discountprice = 0;

                                        }
                            if($Price !='')
                            {
                              DB::table('nm_product_to_option_price')->insert([
                              'product_id' =>$productid,
                              'product_option_id' =>$product_option_id,
                              'price' =>$Price,
                              'discount' =>$Discount,
                              'discount_price' => $discountprice,
                              'product_option_value_id' => $ContainerID
                              ]);

                              }
                        
                            }
                               if (\Config::get('app.locale') == 'ar'){
                                 Session::flash('message', "دجاج تندوري");
                                 }
                                 else
                                 {
                                 Session::flash('message', "Dish successfully saved");
                                 }

                                  return redirect()->route('buffet-dish',['id'=>$parent_id,'sid'=>$sid,'itemid'=>$itemid]);
                                   
                                 }   
                               }


              
       
  //End buffet Dish
  
  //Buffet Buffet Container
   public function buffetListContainer(Request $request)
  {
      if(Session::has('merchantid')) 
      {
          $mer_id   = Session::get('merchantid');
          $parent_id = request()->id;
          $sid = request()->sid;
          $itemid = request()->itemid;
          $status = $request->status;
          $search = $request->search;
          $getPagelimit = config('app.paginate');
         if($status!='')
                {
                  $containerlist = NmproductOptionValue::where('vandor_id',$mer_id)->where('product_id',$itemid)->where('status',$status)->paginate($getPagelimit)->appends(request()->query());
                }
                else if($search !='')
                {
                     $mc_name='option_title';
                     $mer_selected_lang_code = Session::get('mer_lang_code'); 
                   if($mer_selected_lang_code !='en')
                   {
                    
                    $containerlist = NmproductOptionValue::where('vandor_id',$mer_id)->where('product_id',$itemid)->where('option_title','LIKE','%' .$search.'%')->paginate($getPagelimit)->appends(request()->query());
                   }
                                      
                  $containerlist = NmproductOptionValue::where('vandor_id',$mer_id)->where('product_id',$itemid)->where('option_title','LIKE','%' .$search.'%')->paginate($getPagelimit)->appends(request()->query());
                }else {
                    $containerlist =  NmproductOptionValue::where('vandor_id',$mer_id)->where('product_id',$itemid)->orderBy('id', 'desc')->paginate($getPagelimit)->appends(request()->query());
                }

          $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
          return view('sitemerchant.buffet.buffet-list-container', compact('merchantheader','merchantfooter','parent_id','sid','itemid','status','search','containerlist'));       
      } 
      else 
      {
          return Redirect::to('sitemerchant');
      }
  }
  
  //Buffet Buffet add Container
   public function buffetAddContainer(Request $request)
  {
      if(Session::has('merchantid')) 
      {
          
          $mer_id   = Session::get('merchantid');
          $parent_id = request()->id;
          $sid = request()->sid;
          $itemid = request()->itemid;
          $autoid = request()->autoid;
          $usecontainer = array();

          
          $count = NmproductOptionValue::select('option_title')->where('product_id',$itemid)->where('vandor_id',$mer_id)->count();
         
          if($count){
          $containerdata = NmproductOptionValue::select('option_title')->where('product_id',$itemid)->where('vandor_id',$mer_id)->get();
             foreach( $containerdata as $val) {
              $usecontainer[] = $val->option_title;

             } 

          } else {
             $containerdata = array();
          }  
          
          $letter1 = array('A','B','C','D','E','F','G','H','I','J','K','L');

          $letter = array_diff($letter1,$usecontainer);
          
         
          $fetchfirstdata = NmproductOptionValue::where('id',$autoid)->where('vandor_id',$mer_id)->first();

            if($autoid!='')
            {
             array_push( $letter, $fetchfirstdata->option_title);
             
            }


          $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
          return view('sitemerchant.buffet.buffet-add-container', compact('merchantheader','merchantfooter','parent_id','sid','itemid','autoid','fetchfirstdata','letter','containerdata'));       
      } 
      else 
      {
          return Redirect::to('sitemerchant');
      }
  }
   
   //Buffet Store Container
      public function storeBuffetContainer(Request $request)
        {    
             
             //echo $request->ssb_name;die
             $this->validate($request,[
               /*'city' => 'required|max:200',
               'hotelname' => 'required|max:150',
              hotel_image' => 'required'*/
           ]);
                 $sid = $request->sid;
                 $parent_id = $request->parent_id;
                 $itemid = $request->itemid;
                 $autoid = $request->autoid;

                 $mer_id   = Session::get('merchantid');
                 if($autoid!='') {
                 $saveservice = NmproductOptionValue::where('id',$autoid)->where('vandor_id',$mer_id)->first();
                   } else {
                    $saveservice = new NmproductOptionValue;
               }
                if($request->file('img')){ 
                  $file = $request->img;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                  $img = Image::make($imageRealPath); // use this if you want facade style code
                     $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);

                  $img->save('buffetimage' . '/'. $thumbName);
                  $file->move('buffetimage/', $Image);
                  $saveservice->image = url('').'/buffetimage/'.$thumbName;
                 }
                  $saveservice ->option_title = $request->title;     
                  $saveservice ->short_name =  $request->title;
                  $saveservice ->no_person = $request->no_people;
                  $saveservice ->product_option_id =22;
                  
                  $saveservice ->vandor_id = $mer_id;
                  $saveservice ->product_id = $itemid;
                  $saveservice->status = 1;
                if($saveservice->save()){
             if (\Config::get('app.locale') == 'ar'){
                Session::flash('message', "تم حفظ الحاوية بنجاح");
             }
             else
             {
                 Session::flash('message', "Container successfully saved");
             }
               return redirect()->route('buffet-list-container',['id'=>$parent_id,'sid'=>$sid,'itemid'=>$itemid]);
              }
                
        }

  //End 





  
   //Buffet Buffet Resturant
   public function buffetResturant(Request $request)
  {
      if(Session::has('merchantid')) 
      {
          $getPagelimit = config('app.paginate');

          $mer_id   = Session::get('merchantid');
          $id = request()->id;
           $status = $request->status;
           $search = $request->input('search');

           if(Session::get('LoginType') =='4')
           {
            //= CHECK THE MARCHANT ACCESS ==//
              $merchantManagerid = Session::get('merchant_managerid');
              $fetchB = Categorylist::where('parent_id',$id)->where('vendor_id',$mer_id)->get();
              $ChekArr = array();
              foreach($fetchB as $chekB)
              {
              $mcid  = $chekB->mc_id;
              $fetchBC = Categorylist::where('parent_id',$mcid)->where('vendor_id',$mer_id)->where('branch_manager_id',$merchantManagerid)->count();
              if($fetchBC >=1)
              {
              $BId = Categorylist::where('parent_id',$mcid)->where('vendor_id',$mer_id)->where('branch_manager_id',$merchantManagerid)->first();
              $CP= $BId->parent_id;
              array_push($ChekArr, $CP);
              }

              } 
              $fetchdata = Categorylist::where('parent_id',$id)->where('vendor_id',$mer_id)->whereIn('mc_id', $ChekArr)->orderBy('mc_id','DESC');
            //= CHECK THE MARCHANT ACCESS ==//
           }
           else
           {
            $fetchdata = Categorylist::where('parent_id',$id)->where('vendor_id',$mer_id)->orderBy('mc_id','DESC');
           }
            

                 if($search !='')
                {
                     $mc_name='mc_name';
                     $mer_selected_lang_code = Session::get('mer_lang_code'); 
                   if($mer_selected_lang_code !='en')
                   {
                             $mc_name= 'mc_name_'.$mer_selected_lang_code;
                   }
                                         
                   $fetchdata = $fetchdata->where('nm_category.'.$mc_name,'like', '%' .$search. '%');
                }

                 if($status !='')
                {
                $fetchdata = $fetchdata->where('mc_status','=',$status);

                } 
              
                 $fetchdata=$fetchdata->paginate($getPagelimit)->appends(request()->query()); 


              

          $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
          return view('sitemerchant.buffet.buffet-resturant', compact('merchantheader','merchantfooter','parent_id','fetchdata','id','search'));       
      } 
      else 
      {
          return Redirect::to('sitemerchant');
      }
  }
  
  //Buffet Buffet Add Resturant
   public function buffetAddResturant(Request $request)
  {
      if(Session::has('merchantid')) 
      {
          $mer_id   = Session::get('merchantid');
          $parent_id = request()->id;
          $sid = request()->sid;

          $fetchfirstdata = Categorylist::where('mc_id',$sid)->where('vendor_id',$mer_id)->first();


          $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
          return view('sitemerchant.buffet.buffet-add-resturant', compact('merchantheader','merchantfooter','parent_id','sid','fetchfirstdata'));       
      } 
      else 
      {
          return Redirect::to('sitemerchant');
      }
  }
  
   //Store Buffet Resturant

    public function storeResturant(Request $request)
        {    
             
             //echo $request->ssb_name;die
             $this->validate($request,[
               /*'city' => 'required|max:200',
               'hotelname' => 'required|max:150',
              hotel_image' => 'required'*/
         ]);
              $sid = $request->sid;
               if($sid){
                  $saveservice =Categorylist::where('mc_id',$sid)->first();
                } else { 
                 $saveservice = new Categorylist;
               }
                $mer_id   = Session::get('merchantid');
               if($request->file('mc_img')){ 
                  $file = $request->mc_img;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                  $img = Image::make($imageRealPath); // use this if you want facade style code
                     $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);

                  $img->save('buffetimage' . '/'. $thumbName);
                  $file->move('buffetimage/', $Image);
                  $saveservice->mc_img = url('').'/buffetimage/'.$thumbName;
                }
                  $saveservice ->mc_name = $request->mc_name;
                  $saveservice ->mc_name_ar = $request->mc_name_ar;
                  $saveservice ->mc_discription = $request->description;
                  $saveservice ->mc_discription_ar = $request->description_ar;
                  $saveservice ->vendor_id = $mer_id;
                  $saveservice ->parent_id = $request->parent_id;
                  $saveservice->mc_status = 1;
                if($saveservice->save()){
                  $id = $request->parent_id;
             if (\Config::get('app.locale') == 'ar'){
                Session::flash('message', "تم حفظ المطعم بنجاح");
             }
             else
             {
                 Session::flash('message', "Restaurant successfully saved");
             }

              }
               return redirect()->route('buffet-resturant',['id' => $id]); 
        }
    

    //Buffet Buffet Manager
   public function buffetManager(Request $request)
  {
      if(Session::has('merchantid')) 
      {
     


      $merid  = Session::get('merchantid');
      $getPagelimit = config('app.paginate');
      $status = $request->status;
      $search = $request->search;
      $id = request()->id;

  $userlist = Merchant::where('vendor_parent_id',$merid)->orderBy('mer_id','desc')->paginate($getPagelimit)->appends(request()->query());
   if($status!='')
                {
$userlist = Merchant::where('vendor_parent_id',$merid)->where('mer_staus',$status)->paginate($getPagelimit)->appends(request()->query());

                }
                 if($search !='')
                {
                     
        
             $userlist = Merchant::where('vendor_parent_id',$merid)->where('mer_fname','LIKE','%' .$search.'%')->orwhere('mer_lname','LIKE','%' .$search.'%')->paginate($getPagelimit)->appends(request()->query());
                }


         
           $this->setLanguageLocaleMerchant();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');
           return view('sitemerchant.buffet.buffet-manager',compact('userlist','merchantheader','merchantfooter','id'));     
      } 
      else 
      {
          return Redirect::to('sitemerchant');
      }


 


      
  }
  
      //Buffet Buffet add Manager
   public function buffetAddManager(Request $request)
  {
      if(Session::has('merchantid')) 
      {
          $mer_id   = Session::get('merchantid');
          $id = request()->id;


          $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
          return view('sitemerchant.buffet.buffet-add-manager', compact('merchantheader','merchantfooter','id'));       
      } 
      else 
      {
          return Redirect::to('sitemerchant');
      }
  }
  
//Buffet Buffet Video
 public function buffetPic(Request $request)
  {
      
                if (Session::has('merchantid')) 
                        {
                            $merid  = Session::get('merchantid');
                            $catid = $request->id;
                            $sid = $request->sid;
                            $itemid = $request->itemid;
                          

                            $this->setLanguageLocaleMerchant();
                            $mer_id              = Session::get('merchantid');             
                            $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                            $merchantfooter     = view('sitemerchant.includes.merchant_footer');           
                           
                            $proid = $request->id;
                            $getDbC = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->count();
                            
                            $getVideos = DB::table('nm_category')->where('mc_id',$itemid)->where('vendor_id',$merid)->first();
                            if($getDbC >=1)
                            {
                            $getDb = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->get(); 
                            $getDb = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->get();

                            }
                            else
                            {
                            $getDb = '';    
                            }

                          return view('sitemerchant.buffet.buffet-picture', compact('merchantheader','merchantfooter','getCatlists','GetAlreadyServices','getDb','getDbC','getVideos','sid','catid','itemid'));       
                        } else {
                            return Redirect::to('sitemerchant');
                    }
          }

    //Store picture
     public function buffetShopPicture(Request $request)

             { 

                if (Session::has('merchantid')) 
                {

                    $validatedData = $request->validate([
                          //'image' => 'required',
                         //'youtubevideo' => 'required',
                         'image.*' => 'image|mimes:jpg,jpeg,png,gif'
 
                    ]); 
 

                    $merid  = Session::get('merchantid');
                    $this->setLanguageLocaleMerchant();
                    $mer_id              = Session::get('merchantid');             
                    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              
                    //Insert images in folder
                 
                            $catid = $request->cat_id;
                            $sid = $request->sid;
                            $itemid = $request->itemid;


                  
                    if($files=$request->file('image')){
                    foreach($files as $file){
                    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
                    //$file->move('hallpics',$name); 
                    $extension = $file->getClientOriginalExtension();
                    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
                    //$file->move('hallpics', $fileName);               
                    $imageRealPath  =   $file->getRealPath();
                    $thumbName      =   'thumb_'. $fileName;       
                    $img = Image::make($imageRealPath); // use this if you want facade style code
                     $thumb_width = 100;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);

                  
                    $img->save('buffetimage' . '/'. $thumbName);
                    $file->move('buffetimage/', $fileName);
                    $shop_Img = url('/').'/buffetimage/'.$thumbName; 
                       
                    /*Insert data to db*/
                    DB::table('nm_category_gallery')->insert( [
                    'image'=>   $shop_Img,
                    'category_id' =>$itemid,
                    'vendor_id' => $merid,
                    ]);
                   }  

                 } 
                   
                    $youtubevideoa = str_replace('youtu.be','www.youtube.com/embed',$request->youtubevideo);
                    $mc_video_description = $request->about_video;
                    $mc_video_description_ar = $request->about_video_ar;

                       
                    /*Update data to db*/
                    DB::table('nm_category')->where('mc_id',$itemid)->where('vendor_id',$merid)->update( ['mc_video_url'=>$youtubevideoa,'mc_video_description'=> $mc_video_description,'mc_video_description_ar'=> $mc_video_description_ar]);
                   

                  
                    
                  
                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "حفظ السجل بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Record successfully saved");
                    }
                    // language for display message //   
                    return Redirect::back();      
                } else {
                        return Redirect::to('sitemerchant');
                }

             }

  public function storebuffetmanager(Request $request)
       {
           $merid               = Session::get('merchantid');
           $id = $request->id;
           $cemail = $request->mer_email;
           $check_email = Register::check_email_ajaxs_register($cemail);
        if (count($check_email)>0) {
            if(Lang::has(Session::get('lang_file').'.ALREADY_EMAIL_EXIST')!= '') 
            {
                $session_message = trans(Session::get('lang_file').'.ALREADY_EMAIL_EXIST');
            }
            else
            {
                $session_message =  trans($this->OUR_LANGUAGE.'.ALREADY_EMAIL_EXIST');
            }
            return Redirect::to('buffet-add-manager',['id' =>$id])->with('mail_exist',$session_message)->withInput();
        }else {
           $saveuser = New Merchant;
           if($request->file('stor_img')){ 
                $file = $request->stor_img;  
                $merid  = Session::get('merchantid');
                $extension = $file->getClientOriginalExtension();
                $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                $imageRealPath  =   $file->getRealPath();
                $thumbName      =   'thumb_'. $Image;       
                $img = Image::make($imageRealPath); // use this if you want facade style code
                $img->resize(150,150);
                $img->save('branchmanagerimage' . '/small/'. $thumbName);
                $file->move('branchmanagerimage/', $Image);
                $saveuser->stor_img = url('').'/branchmanagerimage/small/'.$thumbName;
                }

                 if(!empty($request->storcertifiction_img)){ 
                
                   $file = $request->storcertifiction_img;
                    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
                    //$file->move('hallpics',$name); 
                    $extension = $file->getClientOriginalExtension();
                    $filename_c = rand(11111,99999).'_'.time().'.'.$extension;
                       
                    $file->move('./public/assets/storeimage/', $filename_c);
                      
                   $filename_cc = url('').'/public/assets/storeimage/'.$filename_c; 
 
              }else {
                  $filename_cc ='';
              } 
              $date = date('m/d/Y');
              $current_date = date('Y-m-d'); 
              $saveuser->mer_fname =    $request->mer_fname;
              $saveuser->mer_fname_ar =    $request->mer_fname_ar;
              $saveuser->mer_lname =    $request->mer_lname;
              $saveuser->mer_lname_ar =    $request->mer_lname_ar;
              $saveuser ->mer_email =   $request->mer_email;
              $saveuser ->mer_phone =   $request->mobile;
              $saveuser ->city_name     =   $request->city;
              $saveuser->mer_password =bcrypt($request->mer_password);
              $saveuser->mer_logintype = 4;
              $saveuser->created_date = $current_date; 
              $saveuser->vendor_parent_id = $merid;
              $parentid =    $request->id;
              $saveuser->certificates = $filename_cc; 
          
              if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "تمت إضافة مدير الفرع بنجاح");
             }
             else
             {
             Session::flash('message', "Branch Manager successfully added");
             }

          if($saveuser->save())
          { 
             return redirect()->route('buffet-manager',['id' => $parentid]);
          }

        }

       }


  public function buffetEditManager(Request $request)
  {
      if(Session::has('merchantid')) 
      {
          $mer_id   = Session::get('merchantid');
          $sid = request()->sid;
          $id = request()->id;

          $edituser = Merchant::where('mer_id',$sid)->first();

          $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
          return view('sitemerchant.buffet.buffet-edit-manager', compact('merchantheader','merchantfooter','id','sid','edituser'));       
      } 
      else 
      {
          return Redirect::to('sitemerchant');
      }
  }
  




public function editbuffetuser(Request $request)
{
  
           $merid               = Session::get('merchantid');
           $id = $request->id;
           $sid = $request->sid;
           $cemail = $request->mer_email;
            $saveuser = Merchant::where('mer_id',$sid)->first();
           if($request->file('stor_img')){ 
                $file = $request->stor_img;  
                $merid  = Session::get('merchantid');
                $extension = $file->getClientOriginalExtension();
                $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                $imageRealPath  =   $file->getRealPath();
                $thumbName      =   'thumb_'. $Image;       
                $img = Image::make($imageRealPath); // use this if you want facade style code
                $img->resize(150,150);
                $img->save('branchmanagerimage' . '/small/'. $thumbName);
                $file->move('branchmanagerimage/', $Image);
                $saveuser->stor_img = url('').'/branchmanagerimage/small/'.$thumbName;
                }

                 if(!empty($request->storcertifiction_img)){ 
                
                   $file = $request->storcertifiction_img;
                    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
                    //$file->move('hallpics',$name); 
                    $extension = $file->getClientOriginalExtension();
                    $filename_c = rand(11111,99999).'_'.time().'.'.$extension;
                       
                    $file->move('./public/assets/storeimage/', $filename_c);
                      
                   $filename_cc = url('').'/public/assets/storeimage/'.$filename_c; 
 
              }else {
                  $filename_cc ='';
              } 
              $date = date('m/d/Y');
              $current_date = date('Y-m-d'); 
              $saveuser->mer_fname =    $request->mer_fname;
              $saveuser->mer_fname_ar =    $request->mer_fname_ar;
              $saveuser->mer_lname =    $request->mer_lname;
              $saveuser->mer_lname_ar =    $request->mer_lname_ar;
              $saveuser ->mer_email =   $request->mer_email;
              $saveuser ->mer_phone =   $request->mobile;
              $saveuser ->city_name     =   $request->city;
              $saveuser->mer_password =bcrypt($request->mer_password);
              $saveuser->mer_logintype = 4;
              $saveuser->created_date = $current_date; 
              $saveuser->vendor_parent_id = $merid;
              $parentid =    $request->id;
              $saveuser->certificates = $filename_cc; 
          
              if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "تمت إضافة مدير الفرع بنجاح");
             }
             else
             {
             Session::flash('message', "Branch Manager successfully updated");
             }

          if($saveuser->save())
          { 
             return redirect()->route('buffet-manager',['id' => $id]);
          }

        
    }

 }



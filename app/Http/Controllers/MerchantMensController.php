<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Models;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon;
use File;
use App\Reviewrating;
use App\Categorylist;
use Intervention\Image\ImageManagerStatic as Image; 
use Auth;
use Lang;
use App\ProductsMerchant;
use App\City;
use App\NmServicesAttribute;
use App\ServiceStaff;
use App\StaffExperties;
use App\HallOffer;
use App\NmProductGallery;
use App\Beautystaffvalidation;
class MerchantMensController extends Controller
{
      public function __construct(){
            parent::__construct();
            // set admin Panel language
            $this->middleware(function ($request, $next) {
                        $this->setLanguageLocaleMerchant();
                        Session::get('mer_lang_code');
                        return $next($request);
           });
     }
    
 
//Add Shop info

// Start For Popular band info
   
    public function beautyBranchList(Request $request)
     {
 
             //$q->where('name', 'like', "%{$breed}%");
        if (Session::has('merchantid')) 
        {   
                $getPagelimit = config('app.paginate');
                    $id = request()->id;  
                $status = $request->status;
                $search = $request->input('search');
                $mer_id   = Session::get('merchantid');
                $alldata = Categorylist::where('parent_id',$id)->where('vendor_id',$mer_id)->orderBy('mc_id','DESC');
               

                if($search !='')
                {
                $mc_name='mc_name';
                $mer_selected_lang_code = Session::get('mer_lang_code'); 
                if($mer_selected_lang_code !='en')
                {
                $mc_name= 'mc_name_'.$mer_selected_lang_code;
                }
                $alldata = $alldata->where('nm_category.'.$mc_name,'like', '%' .$search. '%');
                }


                if($status !='')
                {
                $alldata = $alldata->where('mc_status','=',$status);
                }

                $alldata=$alldata->paginate($getPagelimit)->appends(request()->query());
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');
                return view('sitemerchant.beauty.beauty-shop', compact('merchantheader','merchantfooter','alldata','id','search'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 
   


    public function beautyShopInfo(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              $getPagelimit = config('app.paginate');
$status = $request->status;
$search = $request->search;
$productdata = ProductsMerchant::where('pro_mc_id',$id)->where('pro_mr_id',$mer_id)->orderBy('pro_id','desc')->paginate($getPagelimit)->appends(request()->query());
if($status!='')
{
$productdata = ProductsMerchant::where('pro_mc_id',$id)->where('pro_mr_id',$mer_id)->where('pro_status',$status)->orderBy('pro_id','desc')->paginate($getPagelimit)->appends(request()->query());
}
if($search !='')
{
    $productdata = ProductsMerchant::where('pro_mc_id',$id)->where('pro_mr_id',$mer_id)->where('pro_title','like','%'.$search.'%')->orderBy('pro_id','desc')->paginate($getPagelimit)->appends(request()->query());
}



 
                return view('sitemerchant.beauty.beauty-shop-branch', compact('merchantheader','merchantfooter','id','productdata'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 
	 
    public function beautyAddShop(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = request()->hid;

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');   

                $getlisting = Categorylist::where('mc_id', $hid)->where('vendor_id',$mer_id)->first(); 
                return view('sitemerchant.beauty.beauty-add-shop', compact('merchantheader','merchantfooter','id','hid','getlisting'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 
	 
    public function beautyManager(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.beauty.beauty-manager', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 

    public function mensServices(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                 $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $sid = $request->sid;
                 $status = $request->status;
                 $search = $request->search;
                 $catname = $request->catid; 
                 $getPagelimit = config('app.paginate');


                  $Beautystaffvalidation= new Beautystaffvalidation();

                $allservicecheckstaff =  ProductsMerchant::where('pro_mc_id',$sid)->where('pro_mr_id',$mer_id)->get();
                foreach ($allservicecheckstaff as $productidfromproducttble) {
                   $productid=$productidfromproducttble->pro_id;
                   $Beautystaffvalidation->inactivebeautyandeleganceproduct($productid);
                }

                 

                 $productdata        = ProductsMerchant::where('pro_mc_id',$sid)->where('pro_mr_id',$mer_id);
                if($catname != '')
                {
                $productdata = $productdata->where('attribute_id',$catname); 
                }
                if($status != '')
                {
                $productdata = $productdata->where('pro_status',$status);
                }
                if($search != '')
                {
                    
                $productdata = $productdata->where('pro_title','like', '%'.$search.'%');
                }

                $productdata = $productdata->orderBy('pro_id','desc')->paginate($getPagelimit);

                $attrcat            = NmServicesAttribute::where('vendor_id',$mer_id)->where('services_id',$sid)->get();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities          = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();
                return view('sitemerchant.mens-barber.mens-service', compact('merchantheader','merchantfooter','id','getCities','productdata','sid','attrcat','status','search','catname'));
 
                   
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 
	 
public function mensAddServices(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                            $id = $request->id;
                            $itemid = $request->sid;
                            $autoid = $request->autoid;
                            
                           

                            $fetchdata = ProductsMerchant::where('pro_mr_id',$mer_id)->where('pro_id',$autoid)->first();
                            $attributesname =  NmServicesAttribute::where('services_id',$itemid)->get(); 
                            $proid = $request->id;
                            $getDbC = DB::table('nm_product_gallery')->where('vendor_id', $mer_id)->where('product_id', $autoid)->count();
                            $getVideos = DB::table('nm_product')->where('pro_id',$autoid)->where('pro_mr_id',$mer_id)->first();
                            if($getDbC >=1)
                            {
                            $getDb = DB::table('nm_product_gallery')->where('vendor_id', $mer_id)->where('product_id', $autoid)->get();
                            }
                            else
                            {
                            $getDb = '';    
                            }
   

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
                $productGallery =  NmProductGallery::where('vendor_id',$mer_id)->where('product_id',$autoid)->get();
                return view('sitemerchant.mens-barber.mens-add-service', compact('merchantheader','merchantfooter','id','getSinger','itemid','getDbC','getDb','attributesname','autoid','fetchdata','productGallery'));
 
                      
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 

	 
	  	
	 
	 
    public function beautyAddServices(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.beauty.beauty-add-service', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }		 	  
	 
	 
    public function beautyAddManager(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.beauty.Beauty-add-manager', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 	
	 
     public function mensReview(Request $request)
     {
        if(Session::has('merchantid')) 
        {
            $mer_id          = Session::get('merchantid');
            $id              = $request->id;
            $getPagelimit    = config('app.paginate');
            $sid             = $request->sid;
            $merchantheader  = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
            $merchantfooter  = view('sitemerchant.includes.merchant_footer'); 
            $reviewrating    = Reviewrating::where('vandor_id',$mer_id)->where('shop_id',$sid)->where('review_type','shop')->where('status',1)->orderBy('created_at','desc')->paginate($getPagelimit)->appends(request()->query());
            
            return view('sitemerchant.mens-barber.mens-reviews-and-comments', compact('merchantheader','merchantfooter','id','reviewrating'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 

        public function barberWorkerReview(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $workerID = $request->wid;
                $getPagelimit = config('app.paginate'); 
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
               $reviewrating = Reviewrating::where('worker_id',$workerID)->where('vandor_id',$mer_id)->where('review_type','worker')->paginate($getPagelimit)->appends(request()->query());
 
                return view('sitemerchant.mens-barber.mens-worker-review', compact('merchantheader','merchantfooter','id','reviewrating','workerID'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }        




	 
	  public function beautyWorkerReview(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $getPagelimit = config('app.paginate');
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
               $reviewrating = Reviewrating::where('vandor_id',$mer_id)->paginate($getPagelimit)->appends(request()->query());
 
                return view('sitemerchant.beauty.worker-review', compact('merchantheader','merchantfooter','id','reviewrating'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }		   
	 	
		
		
    public function mensPictures(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                 $merid  = Session::get('merchantid');
                            $parentid = $request->id;
                            $itemid = $request->sid;
                            $this->setLanguageLocaleMerchant();
                            $mer_id              = Session::get('merchantid');             
                            $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                            $merchantfooter     = view('sitemerchant.includes.merchant_footer');           
                            
                            $getDbC = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->count();
                            
                            $getVideos = DB::table('nm_category')->where('mc_id',$itemid)->where('vendor_id',$merid)->first();
                            if($getDbC >=1)
                            {
                          
                            $getDb = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->get();

                            }
                            else
                            {
                            $getDb = '';    
                            }

                          return view('sitemerchant.mens-barber.mens-picture', compact('merchantheader','merchantfooter','getCatlists','GetAlreadyServices','getDb','getDbC','getVideos','parentid','itemid'));  
 
                   
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 
	 
    public function mensAddServiceCatogory(Request $request)
     {
        if (Session::has('merchantid')) 
             {
               $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $sid = $request->sid;
                $autoid = $request->autoid;

               

                 $datafirst = NmServicesAttribute::where('vendor_id',$mer_id)->where('id',$autoid)->first();


                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.mens-barber.mens-add-service-catogory', compact('merchantheader','merchantfooter','id','getCities','getSinger','sid','autoid','datafirst'));  
       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 
	 
	 public function mensOffer(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $getPagelimit = config('app.paginate');
                $id = $request->id;
                $sid = $request->sid;
                $autoid = $request->autoid;
                $status = $request->status;
                $search = $request->search;

             $mer_id   = Session::get('merchantid');
            $hallofferlist =  HallOffer::where('vendor_id',$mer_id)->where('pro_id',$sid);
            if($status!='')
            {
            $hallofferlist = $hallofferlist->where('status',$status);
            }
            if($search !='')
            {
            $mer_selected_lang_code = Session::get('mer_lang_code'); 
            if($mer_selected_lang_code !='en')
            {
            $hallofferlist = $hallofferlist->where('title_ar','LIKE','%' .$search.'%');
            }
            else
            {
            $hallofferlist = $hallofferlist->where('title','LIKE','%' .$search.'%');
 
            }
            }   
            $hallofferlist =  $hallofferlist->orderBy('id','DESC')->paginate($getPagelimit);

                $this->setLanguageLocaleMerchant();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
            return view('sitemerchant.mens-barber.mens-offer', compact('merchantheader','merchantfooter','hallofferlist','status','autoid','sid','id','search'));
 
                    
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 
	  public function mensAddOffer(Request $request)
     {
        if (Session::has('merchantid')) 
             {
               $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $getPagelimit = config('app.paginate');
                $sid = $request->sid;
                $autoid = $request->autoid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
               $reviewrating = Reviewrating::where('vandor_id',$mer_id)->paginate($getPagelimit)->appends(request()->query());
                $categorysave = HallOffer::where('id',$autoid)->where('vendor_id',$mer_id)->first(); 
 
                return view('sitemerchant.mens-barber.mens-add-offer', compact('merchantheader','merchantfooter','id','sid','autoid','reviewrating','categorysave'));  
         
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 

	 
	 
	 
    public function mensServiceCatogory(Request $request)
     {
        if (Session::has('merchantid')) 
             {
              $parent_id = request()->id;
                $sid = request()->sid;
                $status = $request->status;
                $search = $request->search;
                $getPagelimit = config('app.paginate');
                $mer_id   = Session::get('merchantid');

      $menulist = NmServicesAttribute::where('vendor_id',$mer_id)->where('services_id',$sid);

                if($status!='')
                {
                $menulist = $menulist->where('status',$status);
                }
                
              if($search !='')
                {      
                $mc_name='attribute_title';
                $mer_selected_lang_code = Session::get('mer_lang_code'); 
                
                if($mer_selected_lang_code !='en')
                {
                $menulist = $menulist->where('attribute_title_ar','LIKE','%' .$search.'%');
                }
                else
                {
                   $menulist = $menulist->where('attribute_title','LIKE','%' .$search.'%'); 
                }

                
                } 


$menulist = $menulist->paginate($getPagelimit);



                $this->setLanguageLocaleMerchant();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
            return view('sitemerchant.mens-barber.mens-service-category', compact('merchantheader','merchantfooter','menulist','status','search','sid','parent_id','itemid'));  

            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 
	 
	  public function mensPackage(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.mens-barber.mens-package', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	  public function mensWorker(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $sid = $request->sid;
                $status = $request->status;
                $search = $request->search;
                $getPagelimit = config('app.paginate');
                $mer_id   = Session::get('merchantid');
                $alldata        = ServiceStaff::where('service_id',$sid);
                  if($status != '')
                 {
                  $alldata = $alldata->where('status',$status);
                  }
                  if($search != '')
                  {
                      
                  $alldata = $alldata->where('staff_member_name','like', '%'.$search.'%');
                 }

                 $alldata = $alldata->orderBy('id','desc')->paginate($getPagelimit)->appends(request()->query());



               // $alldata = ServiceStaff::where('shop_id',$sid)->get();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                return view('sitemerchant.mens-barber.mens-worker', compact('merchantheader','merchantfooter','id','sid','alldata','search'));  

            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 


     public function mensbooking(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                 $workerID = $request->workerid;
                   $shopid=$request->sid;
                   $serviceid  = $request->serviceid;
                $date_to        = $request->date_to;
                $End_Date       = $request->from_to;

                $getPagelimit = config('app.paginate'); 
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');
                 $mer_selected_lang_code = Session::get('mer_lang_code'); 
                      if($mer_selected_lang_code !='en')
                     {
                      $staffinformation = DB::table('nm_services_staff')->where('id',$workerID)->select('id', 'staff_member_name_ar as staff_member_name','image')->first(); 
                     }else{
                         $staffinformation = DB::table('nm_services_staff')->where('id',$workerID)->select('id', 'staff_member_name','image')->first(); 
                     }




                //$reviewrating = DB::table('nm_order_services_staff')->where('staff_id',$workerID)->where('shop_id',$shopid)->orderBy('id')->paginate($getPagelimit)->appends(request()->query());

                 $staffservices=DB::table('nm_staff_experties')->join('nm_product', 'nm_staff_experties.product_id', '=', 'nm_product.pro_id')->where('nm_staff_experties.staff_id',$workerID)->orderBy('nm_product.pro_title','ASC')->get();
                

                $reviewrating = DB::table('nm_order_services_staff')->where('staff_id',$workerID);
                if($serviceid!=''){
                  $reviewrating = $reviewrating->where('service_id',$serviceid);
                  }
                  if($date_to!='' && $End_Date!=''){
                  $reviewrating = $reviewrating->whereDate('booking_date', '>=', $date_to)->whereDate('booking_date', '<=', $End_Date);
                  } 
                   $reviewrating = $reviewrating->where('shop_id',$shopid)->orderBy('id','desc')->paginate($getPagelimit)->appends(request()->query());



 
                return view('sitemerchant.mens-barber.worker-barber-booking', compact('merchantheader','merchantfooter','id','reviewrating','staffinformation','staffservices'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     } 




	 public function mensAddWorker(Request $request)
     {
        if (Session::has('merchantid')) 
             {
               $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $sid = $request->sid;
                $autoid = $request->autoid;
                 
                 $procuctidcount = StaffExperties::select('product_id')->where('staff_id',$autoid)->count();
                  if($procuctidcount) {
                 $procuctid = StaffExperties::select('product_id')->where('staff_id',$autoid)->get();
                 foreach($procuctid as $val ) {

                            $proid[]  = $val->product_id;
                        }
                    } else
                    {    $proid = array();
                         
                    }
                   
                
                $fetchdata =ServiceStaff::where('id',$autoid)->first();

                

                 $getAttr = DB::table('nm_services_attribute')->where('vendor_id', '=', $mer_id)->where('services_id', '=', $sid)->get();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                return view('sitemerchant.mens-barber.mens-add-worker', compact('merchantheader','merchantfooter','id','sid','getAttr','fetchdata','proid','autoid')); 
 
                
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 public function mensOrder(Request $request)
     {
        if(Session::has('merchantid')) 
        {
            $mer_id         = Session::get('merchantid');
            $id             = $request->id;
            $hid            = $request->sid;
            $merchantheader = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
            $merchantfooter = view('sitemerchant.includes.merchant_footer');             
            $sid            = $request->sid;
            $searchkeyword  = $request->searchkeyword;
            $date_to        = $request->date_to;
            $End_Date       = $request->from_to;
            $order_days     = $request->order_days;
            $status         = $request->status;                
            $serachfirstfrm = $request->serachfirstfrm;
            $getorderedproducts = DB::table('nm_order_product')->where('product_type','beauty')->where('product_sub_type','men_saloon');
            if($searchkeyword!='')
            {
              $getorderedproducts = $getorderedproducts->where('order_id',$searchkeyword);
            }
            if($status!='')
            {
              $getorderedproducts = $getorderedproducts->where('status',$status);
            } 
            if($date_to!='' && $End_Date!='')
            {
              $getorderedproducts = $getorderedproducts->whereDate('created_at', '>=', $date_to)->whereDate('created_at', '<=', $End_Date);
            }
            $getorderedproducts = $getorderedproducts->where('merchant_id',$mer_id)->groupBy('order_id')->orderBy('created_at','DESC')->selectRaw('*,sum(nm_order_product.total_price) as sum')->get();
 
            return view('sitemerchant.mens-barber.mens-order', compact('merchantheader','merchantfooter','id','hid','sid','getorderedproducts','mer_id','sid','mer_id'));       
        } 
        else 
        {
          return Redirect::to('sitemerchant');
        }
     }	
	 
	  public function getorderdetail(Request $request)
      {
        if(Session::has('merchantid')) 
        {
            $mer_id   = Session::get('merchantid');
            $id       = $request->id;
            $opid     = $request->opid;
            $oid      = $request->oid;
            $cusid    = $request->cusid;
            $sid      = $request->sid;
            $itemid   = $request->hid;
            $pid      = $request->pid;
            $merchantheader = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
            $merchantfooter = view('sitemerchant.includes.merchant_footer');             
            $productdata = DB::table('nm_order_product')->where('product_type','beauty')->where('order_id',$oid)->where('product_sub_type','men_saloon')->where('cus_id',$cusid)->where('order_id',$oid)->orderBy('created_at','DESC')->get();
            foreach($productdata as $value)
            {
               $moredetail = DB::table('nm_order_services_staff')->where('product_type','beauty')->where('cus_id',$cusid)->where('order_id',$oid)->where('service_id',$value->product_id)->first();
               $value->bookingdetail = $moredetail; 
            }
            return view('sitemerchant.mens-barber.men-orderdetail', compact('merchantheader','merchantfooter','id','hid','sid','opid','productdata','pid','cusid','oid','itemid'));       
         } 
         else 
         {
            return Redirect::to('sitemerchant');
         }
       }
	 
	 
	 
	 	  public function mensAddBranch(Request $request)
            {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');                
 
                return view('sitemerchant.mens-barber.mens-add-branch', compact('merchantheader','merchantfooter','id','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 public function mensAddPackage(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.mens-barber.mens-add-package', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 	 	 	 
	 			 
	 
   
   
    public function mensshop(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $itemid = $request->sid;
                $fetchdata = array();
                $city = City::where('ci_con_id',10)->get();
                $getshopid = '';
                $fetchdatacount = Categorylist::where('mc_id',$itemid)->where('vendor_id',$mer_id)->count();
                if($fetchdatacount >0) {
                $fetchdata = Categorylist::where('mc_id',$itemid)->where('vendor_id',$mer_id)->first();
                //$getshopid = $fetchdata->mc_id;
                }


                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getPagelimit = config('app.paginate');



                return view('sitemerchant.mens-barber.mens-shop-info', compact('merchantheader','merchantfooter','id','itemid','fetchdata','city'));  
      
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
   
   

     public function savebeautyservice(Request $request)
        {    
          

              $merid  = Session::get('merchantid');
                if($request->mc_img){ 
                $file             = $request->mc_img; 
                $extension = $file->getClientOriginalExtension();
                $Adressimgname = rand(11111,99999).'_'.time().'.'.$extension;
                //$file->move('hallpics', $fileName);               
                $imageRealPath  =   $file->getRealPath();
                $thumbName      =   'thumb_'. $Adressimgname;       
                $img = Image::make($imageRealPath); // use this if you want facade style code
 
                $thumb_width = 100;
                list($width,$height) = getimagesize($imageRealPath);
                $thumb_height = ($thumb_width/$width) * $height;
                $img->resize($thumb_width,$thumb_height);
 
                 $img->save('beautyupload' . '/'. $thumbName);
                $file->move('beautyupload/', $Adressimgname);
                $Adressimgnames = url('').'/beautyupload/'.$thumbName;
                }
                else
                {
                $Adressimgnames = $request->mcimg; 
                }
                 if($request->hid!='')
                 {   
                    $pid = $request->hid;  
                    $saveservice = Categorylist::where('mc_id',$pid)->where('vendor_id',$merid)->first();
                 }
                 else
                 {
                   $saveservice = new Categorylist; 
                 }
                
                $saveservice ->parent_id = $request->parent_id;
                $saveservice ->mc_name = $request->mc_name;
                $saveservice ->mc_name_ar = $request->mc_name_ar;
                $saveservice ->mc_discription = $request->description;
                $saveservice ->mc_discription_ar = $request->description_ar;
                $saveservice ->vendor_id = $merid;
                $saveservice ->mc_img = $Adressimgnames; 
                $saveservice->mc_status = 1;
                if($saveservice->save()){
                 $id = $request->parent_id;
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تمت إضافة الفندق بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Shop successfully saved");
                    }
              
                }
             return redirect()->route('beauty-shop',['id' => $id]);
            
        }


 //Store shop Info
            public function storeMensShopInfo(Request $request)

            {
                $merid  = Session::get('merchantid');
                $parent_id = $request->parent_id;
                $itemid = $request->itemid;
               if($itemid){
                  $savebranch =Categorylist::where('mc_id',$itemid)->first();
                } else { 
                 $savebranch = new Categorylist;
               }
           if($request->file('branchimage')){ 
                  $file = $request->branchimage;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                 $img = Image::make($imageRealPath); // use this if you want facade style code
                   $thumb_width = 220;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('uploadimage/mensbarbersaloon' . '/'. $thumbName);
                  $file->move('uploadimage/mensbarbersaloon/', $Image);
                  $savebranch->mc_img = url('').'/uploadimage/mensbarbersaloon/'.$thumbName;
                }
                //Addressimage
                if($request->file('address_image')){ 
                  $file = $request->address_image;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                  $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('uploadimage/mensbarbersaloon' . '/'. $thumbName);
                  $file->move('uploadimage/mensbarbersaloon/', $Image);
                  $savebranch->address_image = url('').'/uploadimage/mensbarbersaloon/'.$thumbName;
                }
              //Termaand condition
               if(!empty($request->mc_tnc)){ 
                  $file = $request->mc_tnc;
                  $orgname =  $file->getClientOriginalName();
                  $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                  $extension = $file->getClientOriginalExtension();
                  $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                  $file->move('./public/assets/storeimage/', $filename_c);                      
                  $Cname =   url('').'/public/assets/storeimage/'.$filename_c;
                  $savebranch->terms_conditions =$Cname;
                  
               }
               else {
                    $orgname = $request->tmcvalue; 
                }

//Termaand condition
               if(!empty($request->mc_tnc_ar)){ 
                  $file = $request->mc_tnc_ar;
                    $orgname_ar =  $file->getClientOriginalName(); 
                  $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                  $extension = $file->getClientOriginalExtension();
                  $filename_ca = rand(11111,99999).'_'.time().'.'.$extension;                       
                  $file->move('./public/assets/storeimage/', $filename_ca);                      
                  $Cname_ar =   url('').'/public/assets/storeimage/'.$filename_ca;
                  $savebranch->terms_conditions_ar =$Cname_ar;
                  $savebranch ->terms_condition_name_ar = $orgname_ar;
               }
               else {
                    $orgname_ar = $request->tmcvalue_ar; 
                }





                  $savebranch ->latitude = $request->latitude;
                  $savebranch ->longitude = $request->longitude;


                  $savebranch ->mc_name = $request->mc_name;
                  $savebranch ->mc_name_ar = $request->mc_name_ar;
                  $savebranch ->vendor_id = $merid;
                  $savebranch ->google_map_address = $request->google_map_address;
                  $savebranch ->address = $request->address;
                  $savebranch ->address_ar = $request->address_ar;
                  $savebranch ->city_id = $request->city_id;
                  $savebranch ->parent_id = $parent_id;
                  $savebranch ->mc_discription = $request->about;
                  $savebranch ->mc_discription_ar = $request->about_ar;
                  $savebranch ->city_id = $request->city_id;
                  $savebranch ->terms_condition_name = $orgname;
                  $savebranch ->opening_time = $request->opening_time;
                  $savebranch ->closing_time = $request->closing_time;
                  $savebranch ->home_visit_charge = $request->home_visit_charge;
                  $serviceabailable =  $request->service_available;
                  if(count($serviceabailable) ==2)
                  {
                    $serviceavil = 3;
                    $savebranch->service_availability = $serviceavil;
                  }
                  else
                  {
                     $serviceavil =  $serviceabailable[0];
                     $savebranch->service_availability = $serviceavil;
                  }
                 // echo $serviceabailable;die;
                 
                  $savebranch ->mc_status = $request->mc_status;

                if($savebranch->save()){
                  $id = $request->parent_id;

            if($itemid==''){
            $itemid = $savebranch->mc_id;
            }
            if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "تم حفظ معلومات المتجر بنجاح");
             }
             else
             {
             Session::flash('message', "Shop information successfully saved");
             }

                 return redirect()->route('mens-shop-info',['id' => $id,'itmemid' =>$itemid]);
                }   
            }

      //End shop info
   
     //Store picture and  video url
      public function storePictureVideourl(Request $request)
      {
          
         if (Session::has('merchantid')) 
                {

                    $validatedData = $request->validate([
                          //'image' => 'required',
                         //'youtubevideo' => 'required',
                        // 'image.*' => 'image|mimes:jpg,jpeg,png,gif'
 
                    ]); 
 

                    $merid  = Session::get('merchantid');
                    $this->setLanguageLocaleMerchant();
                    $mer_id              = Session::get('merchantid');  
                    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              
                    //Insert images in folder
                          $catid = $request->parent_id;
                          $sid = $request->itemid;

                    if($files=$request->file('image')){
                    foreach($files as $file){
                    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
                    //$file->move('hallpics',$name); 
                    $extension = $file->getClientOriginalExtension();
                    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
                    //$file->move('hallpics', $fileName);               
                    $imageRealPath  =   $file->getRealPath();
                    $thumbName      =   'thumb_'. $fileName;       
                    $img = Image::make($imageRealPath); // use this if you want facade style code
                     $thumb_width = 100;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);

                  
                    $img->save('uploadimage/mensbarbersaloon/gallery' . '/'. $thumbName);
                    $file->move('uploadimage/mensbarbersaloon/gallery/', $fileName);
                    $shop_Img = url('/').'/uploadimage/mensbarbersaloon/gallery/'.$thumbName; 
                       
                    /*Insert data to db*/
                    DB::table('nm_category_gallery')->insert( [
                    'image'=>   $shop_Img,
                    'category_id' =>$sid,
                    'vendor_id' => $mer_id,
                    ]);
                   }  

                 } 
                   
                    $youtubevideoa = str_replace('youtu.be','www.youtube.com/embed',$request->youtubevideo);
                    $mc_video_description = $request->about_video;
                    $mc_video_description_ar = $request->about_video_ar;


                    /*Update data to db*/
  DB::table('nm_category')->where('mc_id',$sid)->where('vendor_id',$mer_id)->update( ['mc_video_url'=>$youtubevideoa,'mc_video_description'=> $mc_video_description,'mc_video_description_ar'=> $mc_video_description_ar]);





                      // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تمت إضافة الخدمة");
                    }
                    else
                    {
                    Session::flash('message', "Record saved successfully");
                    }
                    // language for display message //   
                    return Redirect::back();      
                } else {
                        return Redirect::to('sitemerchant');
                }
      }
     
   //Store buffet Category
   public function storeMakeUpArtistCategory(Request $request)

        {
             $mer_id   = Session::get('merchantid');
             $parent_id = $request->parent_id;
             $sid = $request->sid;
             $autoid = $request->autoid;
             if($autoid!=''){
             $categorysave = NmServicesAttribute::where('id',$autoid)->first();
             } else {  
             $categorysave = new NmServicesAttribute; 
             $categorysave->status  = 1;
             }
             
             if($request->file('catimage')){ 
                  $file = $request->catimage;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                  $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                   $img->save('uploadimage/categoryimage' . '/'. $thumbName);
                   $file->move('uploadimage/categoryimage/', $Image);
                   $categorysave->image = url('').'/uploadimage/categoryimage/'.$thumbName;
                } 
             $categorysave->attribute_title  = $request->category_name;
             $categorysave->attribute_title_ar   = $request->category_name_ar;
             $categorysave->vendor_id   = $mer_id;
             $categorysave->services_id   = $sid;
             $categorysave->parent   = $parent_id;
             
             if($categorysave->save()){ 
              // language for display message //
             if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "تم حفظ الفئة بنجاح");
             }
             else
             {
             Session::flash('message', "Category successfully saved");
             }
     // language for display message //
             
            return redirect()->route('mens-service-category',['id' =>$parent_id,'sid' =>$sid]);
             //return redirect()->route('menulist',['hid' => $hid,'bid' =>$bid]);
             }


        }

   //End Category  
//Store services
     public function storeService(Request $request)

      {        
                $merid  = Session::get('merchantid');
                $parent_id = $request->id;
                $itemid = $request->sid;
                $autoid = $request->autoid;
                $youtubevideoa = str_replace('youtu.be','www.youtube.com/embed',$request->video_url);
               if($autoid){
                  $savebranch =ProductsMerchant::where('pro_id',$autoid)->first();
                } else { 
                  $savebranch = new ProductsMerchant;
                }
             if($request->file('stor_img')){ 
                  $file = $request->stor_img;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                 $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('uploadimage/mensbarbersaloon' . '/'. $thumbName);
                  $file->move('uploadimage/mensbarbersaloon/', $Image);
                  $savebranch->pro_Img = url('').'/uploadimage/mensbarbersaloon/'.$thumbName;
                }
                   $price = $request->price;
                   $discount = $request->discount_price;

                   if($discount!='' && $discount!='0') {
                     $discountprice =   $price-($price * ($discount/100));
                    } else {
                       $discountprice = 0;

                    }


                  $savebranch ->attribute_id = $request->attribute_id;
                  $savebranch ->pro_title = $request->service_name;
                  $savebranch ->pro_title_ar = $request->service_name_ar;
                  $savebranch ->pro_mr_id = $merid;
                  $savebranch ->service_hour = $request->duration;
                  $savebranch ->pro_price = $request->price;
                  $savebranch ->pro_discount_percentage = $request->discount_price;
                  $savebranch ->pro_disprice = $discountprice;
                  $savebranch ->year_of_exp = $request->year_of_exp;
                  $savebranch ->video_url = $youtubevideoa;
                  $savebranch ->pro_desc = $request->description;
                  $savebranch ->pro_desc_ar = $request->description_ar;

                  $savebranch ->pro_mc_id = $itemid;

                  
                  $savebranch->pro_status  = 1;
                if($savebranch->save()){
                $id = $request->id;
                $pro_id = $savebranch->pro_id;
                //Store image





// FOR PRODUCT MULTIPLE IMAGES UPLOAD //
                $files=$request->file('image');
                    $newFiles = array();
                $merid  = Session::get('merchantid');
                if($files=$request->file('image')){
                    foreach($files as $key=>$val){
                    array_push($newFiles, $key);
                    }

                    $privius=$request->privius;
                    $newFilesMatch = array();
                    if(isset($privius) && $privius!='')
                    {
                    foreach($privius as $key=>$val){
                    array_push($newFilesMatch, $key);
                    }  
                    }



                  $CheckInfo = array_intersect($newFiles, $newFilesMatch);

                  if(isset($privius) && $privius!='')
                  {

                  foreach($privius as $key=>$val){

                  if(in_array($key, $CheckInfo))
                  {
                  DB::table('nm_product_gallery')->where('id',$val)->where('vendor_id',$merid)->delete(); 
                  }

                  }
                  }
 

                 
                   
                    foreach($files as $file){
                    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
                     $extension = $file->getClientOriginalExtension();
                    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
                     $imageRealPath  =   $file->getRealPath();
                    $thumbName      =   'thumb_'. $fileName;       
                    $img = Image::make($imageRealPath); // use this if you want facade style code
                     $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);                  
                    $img->save('uploadimage/mensbarbersaloon' . '/'. $thumbName);
                    $file->move('uploadimage/mensbarbersaloon/', $fileName);
                    $shop_Img = url('/').'/uploadimage/mensbarbersaloon/'.$thumbName; 
                     /*Insert data to db*/
                    DB::table('nm_product_gallery')->insert( [
                    'image'=>   $shop_Img,
                    'product_id' =>$pro_id,
                    'status' => 1,
                    'vendor_id' => $merid,
                    ]);
                   }  
                } 


 
// FOR PRODUCT MULTIPLE IMAGES UPLOAD END //


 

                 
            if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "تم حفظ معلومات المتجر بنجاح");
             }
             else
             {
             Session::flash('message', "Shop information successfully saved");
             }

                 return redirect()->route('mens-service',['id' => $parent_id,'sid' =>$itemid]);
                }   
        }

        //Save Worker Data
      public function storeWorker(Request $request)

             {
                  $merid  = Session::get('merchantid');
                  $parent_id = $request->parentid;
                  $sid = $request->sid;
                  $autoid = $request->autoid;
               if($autoid){
                    $saveworker =ServiceStaff::where('id',$autoid)->first();
                  } else { 
                    $saveworker = new ServiceStaff;
                     $saveworker ->status =   1;
                  }
                  $saveworker ->staff_member_name = $request->name;
                  $saveworker ->staff_member_name_ar = $request->name_ar;
                  $saveworker ->experience = $request->year_of_exp;
                  $saveworker ->service_id =$sid;
                  $saveworker ->shop_id =   $parent_id;
                 

                   if($request->file('stor_img')){ 
                      $file = $request->stor_img;  
                      $merid  = Session::get('merchantid');
                      $extension = $file->getClientOriginalExtension();
                      $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                      $imageRealPath  =   $file->getRealPath();
                      $thumbName      =   'thumb_'. $Image;       
                     $img = Image::make($imageRealPath); // use this if you want facade style code
                      $thumb_width = 150;
                        list($width,$height) = getimagesize($imageRealPath);
                       $thumb_height = ($thumb_width/$width) * $height;
                       $img->resize($thumb_width,$thumb_height);
                      $img->save('uploadimage/mensbarbersaloon' . '/'. $thumbName);
                      $file->move('uploadimage/mensbarbersaloon/', $Image);
                      $saveworker->image = url('').'/uploadimage/mensbarbersaloon/'.$thumbName;
                }
                if($saveworker->save()){
                  
                    $pro_id = $saveworker->id;
                   $productitems1 = $request->productitems;

                      
                   StaffExperties::where('staff_id',$pro_id)->delete();
                   foreach($productitems1 as $val) {
                         $saveitem = new StaffExperties;
                         $saveitem->staff_id = $pro_id;
                         $saveitem->product_id = $val;
                         $saveitem->save();
                      }
                     }  


                  if (\Config::get('app.locale') == 'ar'){
                   Session::flash('message', "تم حفظ البيانات بنجاح");
                   }
                   else
                   {
                   Session::flash('message', "Data successfully saved");
                   }
                
               
               return redirect()->route('mens-worker',['id' => $parent_id,'sid' =>$sid]);  
      }
  //End Worker data 

     //STORE OFFER
       public function storeMakeArtistOffer(Request $request)
       {
            if (Session::has('merchantid')) 
      {      
        $mer_id   = Session::get('merchantid');
        $id = $request->parentid;
        $sid = $request->sid;
        $autoid = $request->autoid;
        if($autoid =='')
        {
        $categorysave = new HallOffer; 
        $categorysave->status    = 1;
        }
        else
        {
        $categorysave = HallOffer::where('id',$autoid)->where('vendor_id',$mer_id)->first();
        }
        $categorysave->vendor_id    = $mer_id; 
        $categorysave->title        =$request->title;
        $categorysave->title_ar    = $request->title_ar;
        $categorysave->start_date     = $request->Date_from;
        $categorysave->end_date    = $request->date_to;
        $categorysave->discount   = $request->discount;
        $categorysave->coupon   = $request->coupon;
        $categorysave->pro_id   = $sid;
        
        if (\Config::get('app.locale') == 'ar'){
        Session::flash('message', "تمت إضافة العرض بنجاح  ");
        }
        else
        {
        Session::flash('message', "Offer successfully saved");
        }
        $categorysave->save(); 
        return redirect()->route('mens-offer',['id' => $id,'sid' => $sid]);
       } else {
      return Redirect::to('sitemerchant');
      } 
       }
     //END OFFER    
   
 } // end 



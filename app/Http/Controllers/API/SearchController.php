<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Http\Models;
use App\BusinessOccasionType;
use App\Category;
use App\SubCategory;
use App\City;
use App\Cart;
use App\CartProduct;
use App\SearchOccasion;
use Auth;
use App\User;
use Response;
use DB;
use App\SingerAndBand;
use App\CartExternalFoodDish;
use App\Products;
use App\HallOffer;

class SearchController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |   Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function __construct() {}

    public function getSearchOption(Request $request) {
        $validator = Validator::make($request->all(), [
            'top_category' => 'required', // '1' for business category and '2' for occasion category    
            'language_type' => 'required|max:255', // 'ar' for arabic language and 'en' for english language
            'token' => 'required'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

                $User = Auth::user();
                if ($input['language_type'] == 'ar') {
                    //get type of occasion or get type of business and cities in arabic language
                    $business_occasion = BusinessOccasiontype::select('id', 'title_ar as title')->where('top_category', '=', $input['top_category'])->orderBy('title', 'ASC')->get();
                    $cities = City::select('ci_id', 'ci_name_ar as ci_name', 'ci_con_id', 'ci_lati', 'ci_long', 'ci_default', 'ci_status')->orderBy('ci_name', 'ASC')->get();
                    $response_array = array('data' => ['success' => true, 'message' => 'تم العثور على البيانات', 'code' => 200], 'type_of_occasion' => $business_occasion, 'city' => $cities);
                } else {
                    //get type of occasion or get type of business and cities in english language
                    $business_occasion = BusinessOccasiontype::select('id', 'title')->where('top_category', '=', $input['top_category'])->orderBy('title', 'ASC')->get();
                    $cities = City::select('ci_id', 'ci_name', 'ci_con_id', 'ci_lati', 'ci_long', 'ci_default', 'ci_status')->orderBy('ci_name', 'ASC')->get();
                    $response_array = array('data' => ['success' => true, 'message' => 'Data found', 'code' => 200], 'type_of_occasion' => $business_occasion, 'city' => $cities);
                }                

                $response_code = 200;
                $response = Response::json($response_array, $response_code);
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }
        }

        return $response;

    }

    public function getSearchWeddingOccasion(Request $request) {
        $validator = Validator::make($request->all(), [
            'top_category' => 'required', // '1' for business category and '2' for occasion category    
            'language_type' => 'required|max:255', // 'ar' for arabic language and 'en' for english language
            'type_of_occasion_id' => 'required', //type_of_occasion_id for type of wedding occasion
            //'number_of_guests' => 'required',
            //'budget' => 'required',
            'city_id' => 'required',
           /* 'booking_date' => 'required',*/
            //'gender' => 'required',
            'token' => 'required'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();                
                
                    if ($input['language_type'] == 'ar') { //get data in arabic version
                    $category = SubCategory::where('man_cat_id', '=', $input['top_category'])->with('getCategory')->get(); //get Wedding Occasion type data( parent_id = 2 ) 
                    if (!$category->isEmpty()) {  
                        foreach($category as $value) {
                            $percentage = ($input['budget'] / 100) * $value->getCategory->budget_in_percentages;
                            //divide budget crossponding percentage 
                            $category_list[] = ['parent_category_id' => $value->man_cat_id,'category_id' => $value->getCategory->mc_id, 'category_name' => $value->getCategory->mc_name_ar, 'sar' => $percentage, 'mc_discription' => $value->getCategory->mc_discription_ar, 'budget_in_percentages' => $value->getCategory->budget_in_percentages, 'city_id' => $value->getCategory->city_id, 'mc_img' => $value->getCategory->mc_img];
                        }
                        $city = City::select('ci_id', 'ci_name_ar as ci_name', 'ci_con_id', 'ci_lati', 'ci_long', 'ci_default', 'ci_status')->where('ci_id', '=', $input['city_id'])->first();
                        // get cities
                        $search_criteria[] = ['city' => $city, 'budget' => $input['budget'], 'gender' => $input['gender'], 'number_of_guests' => $input['number_of_guests'], 'booking_date' => $input['booking_date']];
                        //show search criteria data
                        $response_array = array('data' => ['success' => true, 'message' => 'تم العثور على البيانات', 'code' => 200], 'search_criteria' => $search_criteria, 'category_list' => $category_list);
                    }else {
                        $response_array = array('data' => ['success' => false, 'error' => 'آسف! لم نتمكن من العثور على الخدمة التي تبحث عنها.', 'code' => 500]);
                        $response = Response::json($response_array, 500);
                    }
                    } else { //get data in english version
                        $category = SubCategory::where('man_cat_id', '=', $input['top_category'])->with('getCategory')->get(); //get Wedding Occasion type data( parent_id = 2 )  
                    if (!$category->isEmpty()) {
                        foreach($category as $value) {
                            $percentage = ($input['budget'] / 100) * $value->getCategory->budget_in_percentages;
                            //divide budget crossponding percentage 
                            $category_list[] = ['parent_category_id' => $value->man_cat_id,'category_id' => $value->getCategory->mc_id, 'category_name' => $value->getCategory->mc_name, 'sar' => $percentage, 'mc_discription' => $value->getCategory->mc_discription, 'budget_in_percentages' => $value->getCategory->budget_in_percentages, 'city_id' => $value->getCategory->city_id, 'mc_img' => $value->getCategory->mc_img];
                        }
                        $city = City::select('ci_id', 'ci_name', 'ci_con_id', 'ci_lati', 'ci_long', 'ci_default', 'ci_status')->where('ci_id', '=', $input['city_id'])->first();
                        // get cities
                        $search_criteria[] = ['city' => $city, 'budget' => $input['budget'], 'gender' => $input['gender'], 'number_of_guests' => $input['number_of_guests'], 'booking_date' => $input['booking_date']];
                        //show search criteria data
                        $response_array = array('data' => ['success' => true, 'message' => 'Data found', 'code' => 200], 'search_criteria' => $search_criteria, 'category_list' => $category_list);
                    }else {
                        $response_array = array('data' => ['success' => false, 'error' => 'Sorry! We could not find the service you looking for.', 'code' => 500]);
                        $response = Response::json($response_array, 500);
                    }
                    }
                    $occasion = SearchOccasion::where('user_id', '=', $User->cus_id)->where('main_cat_id', '=', $input['top_category'])->where('occasion_id', '=', $input['type_of_occasion_id'])->where('order_id','=', 'null')->first();
                    if(empty($occasion)){
                        $search_data = array();
                        $search_data['user_id'] = $User->cus_id;
                        $search_data['main_cat_id'] = $input['top_category'];
                        $search_data['occasion_id'] = $input['type_of_occasion_id'];
                        $search_data['budget'] = $input['budget'];
                        $search_data['total_member'] = $input['number_of_guests'];
                        $search_data['city_id'] = $input['city_id'];
                        $search_data['occasion_date'] = $input['booking_date'];
                        if($input['gender'] == 0)
                        {
                           $search_data['gender'] = 'Male'; 
                        }
                        elseif($input['gender'] == 1)
                        {
                           $search_data['gender'] = 'Female'; 
                        }
                        elseif($input['gender'] == 2)
                        {
                           $search_data['gender'] = 'Both';   
                        }
                        elseif($input['gender'] == 3)
                        {
                           $search_data['gender'] = 3;   
                        }
                        
                        SearchOccasion::create($search_data); //search data entry   
                    }
                    

                    $response = Response::json($response_array);
                

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }
        }

        return $response;

    }

    public function getSearchBusinessMeeting(Request $request) {
        $validator = Validator::make($request->all(), [
            'top_category' => 'required', // '1' for business category and '2' for occasion category     
            'language_type' => 'required|max:255', // 'ar' for arabic language and 'en' for english language
            'type_of_business_id' => 'required', //type_of_business_id for type of business meeting
            'number_of_attendence' => 'required',
            'budget' => 'required',
            'city_id' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'token' => 'required'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
               
                    if ($input['language_type'] == 'ar') { //get data in arabic version
                     $category = SubCategory::where('man_cat_id', '=', $input['top_category'])->with('getCategory')->get();
                    //get Business Meeting type data( parent_id = 1 )
                    if (!$category->isEmpty()) {    
                        foreach($category as $value) {
                            $percentage = ($input['budget'] / 100) * $value->getCategory->budget_in_percentages;
                            //divide budget crossponding percentage 
                            $category_list[] = ['parent_category_id' => $value->man_cat_id,'category_id' => $value->getCategory->mc_id, 'category_name' => $value->getCategory->mc_name_ar, 'sar' => $percentage, 'mc_discription' => $value->getCategory->mc_discription_ar, 'budget_in_percentages' => $value->getCategory->budget_in_percentages, 'city_id' => $value->getCategory->city_id, 'mc_img' => $value->getCategory->mc_img];
                        }
                        $city = City::select('ci_id', 'ci_name as ci_name', 'ci_con_id', 'ci_lati', 'ci_long', 'ci_default', 'ci_status')->where('ci_id', '=', $input['city_id'])->first();
                        // get cities
                        $search_criteria[] = ['city' => $city, 'budget' => $input['budget'], 'number_of_attendence' => $input['number_of_attendence'], 'start_date' => $input['start_date'], 'end_date' => $input['end_date']];
                        //show search criteria data
                        $response_array = array('data' => ['success' => true, 'message' => 'تم العثور على البيانات', 'code' => 200], 'search_criteria' => $search_criteria, 'category_list' => $category_list);
                    } else {
                    $response_array = array('data' => ['success' => false, 'error' => 'آسف! لم نتمكن من العثور على الخدمة التي تبحث عنها.', 'code' => 500]);
                    $response = Response::json($response_array, 500);
                    }    
                    } else { //get data in english version
                        $category = SubCategory::where('man_cat_id', '=', $input['top_category'])->with('getCategory')->get();
                        //get Business Meeting type data( parent_id = 1 )
                    if (!$category->isEmpty()) {    
                        foreach($category as $value) {
                            $percentage = ($input['budget'] / 100) * $value->getCategory->budget_in_percentages;
                            //divide budget crossponding percentage 
                            $category_list[] = ['parent_category_id' => $value->man_cat_id,'category_id' => $value->getCategory->mc_id, 'category_name' => $value->getCategory->mc_name, 'sar' => $percentage, 'mc_discription' => $value->getCategory->mc_discription, 'budget_in_percentages' => $value->getCategory->budget_in_percentages, 'city_id' => $value->getCategory->city_id, 'mc_img' => $value->getCategory->mc_img];
                        }
                        $city = City::select('ci_id', 'ci_name', 'ci_con_id', 'ci_lati', 'ci_long', 'ci_default', 'ci_status')->where('ci_id', '=', $input['city_id'])->first();
                        // get cities
                        $search_criteria[] = ['city' => $city, 'budget' => $input['budget'], 'number_of_attendence' => $input['number_of_attendence'], 'start_date' => $input['start_date'], 'end_date' => $input['end_date']];
                        //show search criteria data
                        $response_array = array('data' => ['success' => true, 'message' => 'Data found', 'code' => 200], 'search_criteria' => $search_criteria, 'category_list' => $category_list);
                    } else {
                        $response_array = array('data' => ['success' => false, 'error' => 'Sorry! We could not find the service you looking for.', 'code' => 500]);
                        $response = Response::json($response_array, 500);
                    }    
                    }

                    $occasion = SearchOccasion::where('user_id', '=', $User->cus_id)->where('main_cat_id', '=', $input['top_category'])->where('occasion_id', '=', $input['type_of_business_id'])->first();
                    if(empty($occasion)){
                        $search_data = array();
                        $search_data['user_id'] = $User->cus_id;
                        $search_data['main_cat_id'] = $input['top_category'];
                        $search_data['occasion_id'] = $input['type_of_business_id'];
                        $search_data['budget'] = $input['budget'];
                        $search_data['total_member'] = $input['number_of_attendence'];
                        $search_data['city_id'] = $input['city_id'];
                        $search_data['occasion_date'] = date("Y-m-d", strtotime($input['start_date']));
                        $search_data['occasion_date'] = date("Y-m-d", strtotime($input['end_date']));
                        SearchOccasion::create($search_data); //search data entry   
                    }
                    $response_code = 200;
                    $response = Response::json($response_array, $response_code);
                

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }
        }

        return $response;
    }

     /**Get sub category with pagination**/

    public function searchSubCategory(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main category ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array);
        } else {

            try {

               // $User = Auth::user();
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first(); //    GET MAIN CATEGORY
                if ($category->mc_status == 1) {

                    if ($input['category_id'] != '' && $input['language_type'] == 'ar') {

                        $subcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $input['category_id'])->select('mc_id', 'mc_name_ar as mc_name', 'mc_img','order_no','vendor_id')->orderBy('order_no','ASC')->paginate(9); //GET SUB CATEGORY LIST IN ARABIC
                        $response_array = array('data' => ['success' => true,'code' => 200, 'message' => 'تم العثور على البيانات', 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'status' => $category->mc_status], 'subcategory_list' => $subcategory);
                    } else {

                        //GET SUB CATEGORY LIST IN ENGLISH
                        $subcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $input['category_id'])->select('mc_id', 'mc_name', 'mc_img','order_no','vendor_id')->orderBy('order_no','ASC')->paginate(9);
                        $response_array = array('data' => ['success' => true,'code' => 200, 'message' => 'Data found', 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'status' => $category->mc_status], 'subcategory_list' => $subcategory);
                    }

                    
                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false,'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false,'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Get  vendor list
    /*
        subcategory_id become parent_id for next level category table which represent vendor.
    */
    public function searchVendor(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'city_id' => 'required',
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first(); //GET PARENT CATEGORY

                if (!empty($category)) {

                    if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {
                        /*
                        FOR ARABIC
                        */
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name_ar')->first(); //GET SUB CATEGORY
                        $vendor = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_img', 'budget_in_percentages','city_id','mc_name as name','vendor_id')->where('mc_status', '=', 1)->Where('parent_id', '=', $input['subcategory_id'])->Where('city_id', '=', $input['city_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->orderBy('name', 'ASC')->paginate(9);
                        
                        if (!$vendor->isEmpty()) {
                            $response_array = array('data' => ['success' => true,'code' => 200, 'status' => $category->mc_status, 'message' => 'تم العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'vendor_list' => $vendor);

                        } else {
                            $response_array = array('data' => ['success' => false, 'message' => 'لا يوجد بائع في مدينتك الفرعية', 'code' => 500], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar);
                        }
                        
                    } else {
                        /*
                        FOR ENGLISH
                        */
                        //GET SUB CATEGORY LIST
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name')->first();
                        //GET VENDOR LIST
                        $vendor = Category::select('mc_id', 'mc_name', 'mc_img', 'budget_in_percentages','city_id','vendor_id')->where('mc_status', '=', 1)->Where('parent_id', '=', $input['subcategory_id'])->Where('city_id', '=', $input['city_id'])->with('getCity:ci_id,ci_name')->orderBy('mc_name', 'ASC')->paginate(9);

                         /*$vendor = DB::table('nm_category as b')->select('s.mc_id','s.vendor_id','s.mc_name','s.mc_discription','s.city_id','s.occasion_id','s.mc_img','s.mc_video_url','s.address','s.mc_video_description')->join('nm_category AS s', 's.mc_id', '=', 'b.parent_id')->where('b.city_id', $input['city_id'])->where('s.parent_id', $input['subcategory_id'])->paginate(9));*/




                        
                        if (!$vendor->isEmpty()) {
                            $response_array = array('data' => ['success' => true,'code' => 200, 'status' => $category->mc_status, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_list' => $vendor);

                        } else {
                            $response_array = array('data' => ['success' => false, 'message' => 'No vendor exists in your selected city.', 'code' => 500], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name);
                        }

                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'Sorry! We could not find the service you looking for.']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }   



//New API for hall filter
public function searchHallFilter(Request $request)
{
    $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'hall_budget_type' => 'required',
            'city_id' => 'required',
            'language_type' => 'required|max:255',
            'token' => 'required',
            'hall_budget'=>'required',
            'hall_type'=> 'required',
            'top_category'=> 'required',
            'number_of_guests'=> 'required',
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                    $User = Auth::user();
                    $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['category_id'])->first(); //GET PARENT CATEGORY


                    // Get the branch IDs
                    $getCategory = Category::select('mc_id')->where('mc_status', '=', 1)->where('parent_id', '=', $input['subcategory_id'])->get(); 
 
                    $getCatarray = array();
                    foreach ($getCategory as $mainCategory) {
                    $ChkNxt = $mainCategory->mc_id;

                    $getACategoryC = Category::select('mc_id')->where('city_id', $input['city_id'])->where('mc_status', '=', 1)->where('parent_id', '=', $ChkNxt)->count(); 
                    if($getACategoryC >=1)
                    {
                    $getACategory = Category::select('mc_id')->where('city_id', $input['city_id'])->where('mc_status', '=', 1)->where('parent_id', '=', $ChkNxt)->first(); 
                    $brnchID = $getACategory->mc_id;
                    array_push($getCatarray, $brnchID);  
                    }


                    }
  
                    $halltype = $input['hall_type'];

                    if($halltype =='Both' || $halltype =='2') {

                    $hall_type = array('both','women','men');

                    } else if($halltype =='Female'  || $halltype =='1') {
                    $hall_type = array('women');
                    }else  {
                    $hall_type = array('men');
                    } 

                    if($input['top_category']==2)
                    {
                    $category_id = 1;
                    }
                    else
                    {
                    $category_id = 2;
                    }
 

                    $hotel = Category::where('mc_id', '=', $input['category_id'])->select('mc_id', 'mc_name', 'mc_img','vendor_id','terms_conditions','terms_condition_name')->where('mc_status', '=', 1)->first();

                    if (isset($hotel)) {
                    if ($input['hall_budget_type'] == 1) { 
                    //get halls of particulare hotel within budget


                    $gethalls = Products::select('pro_id', 'pro_mc_id')->whereIn('pro_mc_id', $getCatarray)->where('pro_price', '<=', $input['hall_budget'])->where('hallcapicity', '>=', $input['number_of_guests'])->where('pro_status', '=', 1)->whereIn('hall_type',$hall_type)->whereRaw("find_in_set($category_id,hall_category_type)")->get(); 

// if(isset($input['test']) && $input['test']==1)
// {
   
// print_r($gethalls );

// die;
// }


                    } else if ($input['hall_budget_type'] == 2) { //get halls of particulare hotel above budget
                    $gethalls = Products::select('pro_id', 'pro_mc_id')->whereIn('pro_mc_id', $getCatarray)->where('pro_price', '>', $input['hall_budget'])->where('hallcapicity', '>=', $input['number_of_guests'])->where('pro_status', '=', 1)->whereIn('hall_type',$hall_type)->whereRaw("find_in_set($category_id,hall_category_type)")->get(); 


                    } else if ($input['hall_budget_type'] == 3) { //get halls of particulare hotel on offer
                    $todaydate = date('Y-m-d');
                    $chkOferHall = HallOffer::where('status',1)->where('start_date', '<=',$todaydate)->where('end_date', '>=',$todaydate)->get();


                    $hallArr = array();
                    foreach ($chkOferHall as $chkOferHallue) {
                    $pro_ida = $chkOferHallue->pro_id;
                    array_push($hallArr, $pro_ida);
                    }




                    $gethalls = Products::select('pro_id', 'pro_mc_id')->whereIn('pro_mc_id', $getCatarray)->where('hallcapicity', '>=', $input['number_of_guests'])->whereIn('hall_type',$hall_type)->with('getOfferDetail:id,start_date,end_date,price')->whereRaw("find_in_set($category_id,hall_category_type)")->where('pro_status', '=', 1)->whereIn('pro_id', $hallArr)->get(); 
 
                    }


                    $getbeforeCat = array();
                    foreach ($gethalls as $getHalls) {
                    $ChkNxt =  $getHalls->pro_mc_id;


                    $getACategoryC = Category::select('parent_id')->where('mc_status', '=', 1)->where('mc_id', '=', $ChkNxt)->first(); 

                    $brnchIDD = $getACategoryC->parent_id;
                    array_push($getbeforeCat, $brnchIDD);  



                    }

 
 
                if (count($getbeforeCat) >=1 ) {

                    if ( $input['language_type'] == 'ar') {
                        /*
                        FOR ARABIC
                        */
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name_ar')->first(); //GET SUB CATEGORY                  

                     
                        $vendor = DB::table('nm_category as b')->select('s.mc_id','s.vendor_id','s.mc_name_ar as mc_name','s.mc_discription_ar as mc_discription','s.city_id','s.occasion_id','s.mc_img','s.mc_video_url','s.address as address','s.mc_video_description_ar as mc_video_description ','s.mc_status')->join('nm_category AS s', 's.mc_id', '=', 'b.parent_id')->where('b.city_id', $input['city_id'])->whereIn('s.mc_id', $getbeforeCat)->where('s.parent_id',$input['subcategory_id'])->where('s.mc_status', '=', 1)->distinct('mc_id')->paginate(900);
                        

                        
                        if (!$vendor->isEmpty()) {
                            $response_array = array('data' => ['success' => true,'code' => 200, 'status' => $category->mc_status, 'message' => 'تم العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'vendor_list' => $vendor);

                        } else {
                            $response_array = array('data' => ['success' => false, 'message' => 'لا يوجد بائع في مدينتك الفرعية', 'code' => 500], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar);
                        }
                        
                    } else {
                        /*
                        FOR ENGLISH
                        */
                        //GET SUB CATEGORY LIST
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name')->first();
                         
                   
                        $vendor = DB::table('nm_category as b')->select('s.mc_id','s.vendor_id','s.mc_name','s.mc_discription','s.city_id','s.occasion_id','s.mc_img','s.mc_video_url','s.address','s.mc_video_description','s.mc_status')->join('nm_category AS s', 's.mc_id', '=', 'b.parent_id')->where('b.city_id', $input['city_id'])->whereIn('s.mc_id', $getbeforeCat)->where('s.parent_id',$input['subcategory_id'])->where('s.mc_status', '=', 1)->distinct('mc_id')->paginate(900);
                     

 
                        if (!$vendor->isEmpty()) {
                            $response_array = array('data' => ['success' => true,'code' => 200, 'status' => $category->mc_status, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_list' => $vendor);

                        } else {
                            $response_array = array('data' => ['success' => false, 'message' => 'No vendor exists in your selected city.', 'code' => 500], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name);
                        }

                    }

                    $response = Response::json($response_array);

                } else {
                if ( $input['language_type'] == 'ar') {
                $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'آسف! لم نتمكن من العثور على الخدمة التي تبحث عنها.']);
                }
                else{
                $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'Sorry! We could not find the service you looking for.']);
                } 
                    $response = Response::json($response_array);

                }
            }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

}

    //Himanshu kumar search modification

    public function searchVendordata(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'city_id' => 'required',
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['category_id'])->first(); //GET PARENT CATEGORY
 
                if (!empty($category)) {

                    if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {
                        /*
                        FOR ARABIC
                        */
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name_ar')->first(); //GET SUB CATEGORY
                       
                        /*$vendor = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_img', 'budget_in_percentages','city_id','mc_name as name','vendor_id')->where('mc_status', '=', 1)->Where('parent_id', '=', $input['subcategory_id'])->Where('city_id', '=', $input['city_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->orderBy('name', 'ASC')->paginate(9);*/

                         $subcat = array('94','95','4','5','6','8','9','19','21','29','30');
                        if(in_array($input['subcategory_id'], $subcat)) {
                              if($input['subcategory_id'] ==27) 
                      {
                
                           $vendor = DB::table('nm_category as b')->select('s.mc_id','s.vendor_id','s.mc_name_ar as mc_name','s.mc_discription_ar as mc_discription','s.city_id','s.occasion_id','s.mc_img','s.mc_video_url','s.address as address','s.mc_video_description_ar as mc_video_description ','s.mc_status')->join('nm_category AS s', 's.mc_id', '=', 'b.parent_id')->where('b.city_id', $input['city_id'])->where('b.parent_id',$input['subcategory_id'])->where('s.mc_status', '=', 1)->distinct('mc_id')->paginate(9);

                    }  
                    else
                    {

                        $vendor = DB::table('nm_category as b')->select('s.mc_id','s.vendor_id','s.mc_name_ar as mc_name','s.mc_discription_ar as mc_discription','s.city_id','s.occasion_id','s.mc_img','s.mc_video_url','s.address as address','s.mc_video_description_ar as mc_video_description ','s.mc_status')->join('nm_category AS s', 's.mc_id', '=', 'b.parent_id')->where('b.city_id', $input['city_id'])->where('s.parent_id',$input['subcategory_id'])->where('s.mc_status', '=', 1)->distinct('mc_id')->paginate(9);
                    }
                        } else {
                                $subCat = $input['subcategory_id'];
                            if($subCat ==32)
                            {
                            $activeshop= array();
                                    $getAllvendor = Category::select('mc_id')->where('mc_status', '=', 1)->Where('parent_id', '=', $input['subcategory_id'])->Where('city_id', '=', $input['city_id'])->get();
                                    foreach( $getAllvendor as $vendors)
                                    {
                                    $catID = $vendors->mc_id;
                                    $CheckProduC = DB::table('nm_product')->select('pro_id')->where('pro_status',1)->where('pro_mc_id',$catID)->count();
                                    if($CheckProduC >=1) 
                                    {
                                    $CheckProdu =  DB::table('nm_product')->select('pro_id')->where('pro_status',1)->where('pro_mc_id',$catID)->first();
                                    $getproID = $CheckProdu->pro_id;
                                    $CheckProdoptC =  DB::table('nm_product_option_value')->select('short_name')->where('product_option_id',37)->where('product_id',$getproID)->count();
                                    if($CheckProdoptC >=1)
                                    {
                                    $CheckProdopt =  DB::table('nm_product_option_value')->select('short_name')->where('product_option_id',37)->where('product_id',$getproID)->get();

                                    foreach($CheckProdopt as $vals)
                                    {


                                    $shortname = $vals->short_name;
                                    $CheckfavC =  DB::table('nm_fabric')->where('id',$shortname)->where('status',1)->count();
                                    if($CheckfavC >=1)
                                    {
                                    array_push($activeshop, $catID);
                                    }
                                    }
                                    }
                                    }
                                    }
                        $vendor = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_img', 'budget_in_percentages','city_id','mc_name as name','vendor_id','mc_status')->where('mc_status', '=', 1)->Where('parent_id', '=', $input['subcategory_id'])->Where('city_id', '=', $input['city_id'])->WhereIn('mc_id', $activeshop)->with('getCity:ci_id,ci_name_ar as ci_name')->orderBy('name', 'ASC')->paginate(9);
                            }
                            else
                            {



                             $vendor = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_img', 'budget_in_percentages','city_id','mc_name as name','vendor_id','mc_status')->where('mc_status', '=', 1)->Where('parent_id', '=', $input['subcategory_id'])->Where('city_id', '=', $input['city_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->orderBy('name', 'ASC')->paginate(9);
                         }

                        }

                        
                        if (!$vendor->isEmpty()) {
                            $response_array = array('data' => ['success' => true,'code' => 200, 'status' => $category->mc_status, 'message' => 'تم العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'vendor_list' => $vendor);

                        } else {
                            $response_array = array('data' => ['success' => false, 'message' => 'لا يوجد بائع في مدينتك الفرعية', 'code' => 500], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar);
                        }
                        
                    } else {
                        /*
                        FOR ENGLISH
                        */
                        //GET SUB CATEGORY LIST
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name')->first();
 
                        //GET VENDOR LIST
                        /*$vendor = Category::select('mc_id', 'mc_name', 'mc_img', 'budget_in_percentages','city_id','vendor_id')->where('mc_status', '=', 1)->Where('parent_id', '=', $input['subcategory_id'])->Where('city_id', '=', $input['city_id'])->with('getCity:ci_id,ci_name')->orderBy('mc_name', 'ASC')->paginate(9);*/

                         
                         $subcat = array('94','95','4','5','6','8','9','19','21','29','30');
                            
                          

                        if(in_array($input['subcategory_id'], $subcat)) {

                      if($input['subcategory_id'] ==27) 
                      {
                        $vendor = DB::table('nm_category as b')->select('s.mc_id','s.vendor_id','s.mc_name','s.mc_discription','s.city_id','s.occasion_id','s.mc_img','s.mc_video_url','s.address','s.mc_video_description','s.mc_status')->join('nm_category AS s', 's.mc_id', '=', 'b.parent_id')->where('b.city_id', $input['city_id'])->where('b.parent_id',$input['subcategory_id'])->where('s.mc_status', '=', 1)->distinct('mc_id')->paginate(9);  
                    }  
                    else
                    {
                      $vendor = DB::table('nm_category as b')->select('s.mc_id','s.vendor_id','s.mc_name','s.mc_discription','s.city_id','s.occasion_id','s.mc_img','s.mc_video_url','s.address','s.mc_video_description','s.mc_status')->join('nm_category AS s', 's.mc_id', '=', 'b.parent_id')->where('b.city_id', $input['city_id'])->where('s.parent_id',$input['subcategory_id'])->where('s.mc_status', '=', 1)->distinct('mc_id')->paginate(9);  
                    }  



                        




                        } else {

                    // Tailoar case subcategory_id=32

                            $subCat = $input['subcategory_id'];
                            if($subCat ==32)
                            {
                                    $activeshop= array();
                                    $getAllvendor = Category::select('mc_id')->where('mc_status', '=', 1)->Where('parent_id', '=', $input['subcategory_id'])->Where('city_id', '=', $input['city_id'])->get();
                                    foreach( $getAllvendor as $vendors)
                                    {
                                    $catID = $vendors->mc_id;
                                    $CheckProduC = DB::table('nm_product')->select('pro_id')->where('pro_status',1)->where('pro_mc_id',$catID)->count();
                                    if($CheckProduC >=1) 
                                    {
                                    $CheckProdu =  DB::table('nm_product')->select('pro_id')->where('pro_status',1)->where('pro_mc_id',$catID)->first();
                                    $getproID = $CheckProdu->pro_id;
                                    $CheckProdoptC =  DB::table('nm_product_option_value')->select('short_name')->where('product_option_id',37)->where('product_id',$getproID)->count();
                                    if($CheckProdoptC >=1)
                                    {
                                    $CheckProdopt =  DB::table('nm_product_option_value')->select('short_name')->where('product_option_id',37)->where('product_id',$getproID)->get();
                                    foreach($CheckProdopt as $opts)
                                    {
 
                                    $shortname = $opts->short_name;
                                    $CheckfavC =  DB::table('nm_fabric')->where('id',$shortname)->where('status',1)->count();
                                    if($CheckfavC >=1)
                                    {
                                    array_push($activeshop, $catID);
                                    }
                                    }

                                    }
                                    }
                                    }



                                $vendor = Category::select('mc_id', 'mc_name', 'mc_img', 'budget_in_percentages','city_id','mc_name as name','vendor_id','mc_status')->where('mc_status', '=', 1)->Where('parent_id', '=', $input['subcategory_id'])->Where('city_id', '=', $input['city_id'])->WhereIn('mc_id', $activeshop)->with('getCity:ci_id,ci_name_ar as ci_name')->orderBy('name', 'ASC')->paginate(9);

                            }
                            else
                            {
                              $vendor = Category::select('mc_id', 'mc_name', 'mc_img', 'budget_in_percentages','city_id','mc_name as name','vendor_id','mc_status')->where('mc_status', '=', 1)->Where('parent_id', '=', $input['subcategory_id'])->Where('city_id', '=', $input['city_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->orderBy('name', 'ASC')->paginate(9);  
                            }


                             

                        }


 
                        if (!$vendor->isEmpty()) {
                            $response_array = array('data' => ['success' => true,'code' => 200, 'status' => $category->mc_status, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_list' => $vendor);

                        } else {
                            $response_array = array('data' => ['success' => false, 'message' => 'No vendor exists in your selected city.', 'code' => 500], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name);
                        }

                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'Sorry! We could not find the service you looking for.']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }    


   

    public function searchSubVendor(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', // Main Category ID
            'city_id' => 'required',
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false,'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status','=',1)->where('mc_id', '=', $input['category_id'])->first(); //GET PARENT CATEGORY

                if (!empty($category)) {

                    if ($input['language_type'] == 'ar') {
                        /*
                        FOR ARABIC
                        */                        
                        $vendor = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_img', 'budget_in_percentages','city_id','vendor_id')->where('mc_status', '=', 1)->where('parent_id', '=', $input['category_id'])->where('city_id', '=', $input['city_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->orderBy('mc_name', 'ASC')->paginate(9); //GET VENDOR LIST
                        
                        if (!$vendor->isEmpty()) {
                            $response_array = array('data' => ['success' => true,'code' => 200, 'status' => $category->mc_status, 'message' => 'تم العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'vendor_list' => $vendor);

                        } else {
                            $response_array = array('data' => ['success' => false, 'message' => 'لا يوجد بائع في مدينتك الفرعية', 'code' => 500], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar);
                        }
                        
                    } else {
                        /*
                        FOR ENGLISH
                        */
                        
                        //GET VENDOR LIST
                        
                        $vendor = Category::select('mc_id', 'mc_name', 'mc_img', 'budget_in_percentages','city_id','vendor_id')->where('mc_status', '=', 1)->where('parent_id', '=', $input['category_id'])->where('city_id', '=', $input['city_id'])->with('getCity:ci_id,ci_name')->orderBy('mc_name', 'ASC')->paginate(9);
                        
                        if (!$vendor->isEmpty()) {
                            $response_array = array('data' => ['success' => true,'code' => 200, 'status' => $category->mc_status, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'vendor_list' => $vendor);

                        } else {
                            $response_array = array('data' => ['success' => false, 'message' => 'No vendor exists in your city.', 'code' => 500], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name);
                        }

                        
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false,'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false,'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

     /**Get vendor's branch list**/
    /*
        vendor_id become parent_id for next level category table which represent vendor's branch.
    */
    public function searchBranch(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'vendor_id' => 'required',
            'language_type' => 'required|max:255',
            'city_id' => 'required',
            'token' => 'required'

        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first(); //GET MAIN CATEGORY

                if (!empty($category)) {

                    if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */

                        $city = City::select('ci_id', 'ci_name_ar as ci_name')->where('ci_id', '=', $input['city_id'])->first();
                        //Get City Name

                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name_ar', 'mc_img', 'budget_in_percentages')->first(); //GET SUB CATEGORY

                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name_ar', 'mc_img', 'budget_in_percentages','vendor_id')->first(); //GET VENDOR

                        $vendor_branch = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_img', 'budget_in_percentages', 'city_id', 'terms_conditions', 'insurance_amount', 'google_map_address', 'address_image','vendor_id')->where('mc_status', '=', 1)->where('parent_id', '=', $input['vendor_id'])->where('city_id', '=', $input['city_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->orderBy('mc_name', 'ASC')->paginate(9);
                        //GET VENDOR BRANCH LIST
                        if (!$vendor_branch->isEmpty()) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'تم العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name_ar, 'vendor_branch_list' => $vendor_branch);
                        } else {
                            $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => 'آسف! لم نتمكن من العثور على فرع بائع في المدينة المختارة.'], 'city' => $city, 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name_ar);
                        }

                    }else {
                        /*FOR ENGLISH LANGUAGE*/
                        $city = City::select('ci_id', 'ci_name')->where('ci_id', '=', $input['city_id'])->first();
                        // get city                        

                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name', 'mc_img', 'budget_in_percentages')->first(); //GET SUB CATEGORY
                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name', 'mc_img', 'budget_in_percentages','vendor_id')->first(); //GET VENDOR
                        $vendor_branch = Category::select('mc_id', 'mc_name', 'mc_img', 'budget_in_percentages', 'city_id', 'terms_conditions', 'insurance_amount', 'google_map_address', 'address_image','vendor_id')->where('mc_status', '=', 1)->where('parent_id', '=', $input['vendor_id'])->where('city_id', '=', $input['city_id'])->with('getCity:ci_id,ci_name')->orderBy('mc_name', 'ASC')->paginate(9); //GET VENDOR BRANCH LIST
                        if (!$vendor_branch->isEmpty()) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name, 'vendor_branch_list' => $vendor_branch);
                        } else {
                            $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => 'Sorry! We could not find a vendor branch in the selected city.'], 'city' => $city, 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name);
                        }

                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'Sorry! We could not find the service you looking for.']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }
    
   public function viewCartVendorList(Request $request) {        
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'language_type' => 'required|max:255'

        ]);

        $locale='';
        if(isset($request->lang) && $request->language_type=='ar' )
        {
        $locale='ar';
        }
        else
        {
        $locale='en';
        }
        app()->setLocale($locale); 

        $input = $request->all();
        if ($validator->fails()) {
        $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => trans($locale.'_mob_lang.INPUT_ERROR')]);
        $response = Response::json($response_array);
        } else {

            try {
                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                //get user's detail
                if (!empty($cart)) { 

                // buffet case only- because we are displaying only branch in cart  //
                $artarrya = array(0); $arshop = array();


                $getBuffetCount =   CartProduct::where('cart_sub_type','=','buffet')->where('cart_id', '=', $cart->id)->count();
                if($getBuffetCount >=1)
                {
                $getBuffet  =   CartProduct::where('cart_sub_type','=','buffet')->where('cart_id', '=', $cart->id)->get(); 
                foreach ($getBuffet as $getBuffetue) 
                {
                $Bshop_id = $getBuffetue->shop_id;
                $Bid = $getBuffetue->id; 
                if(in_array($Bshop_id, $arshop))
                {
                array_push($artarrya, $Bid);   
                }
                array_push($arshop, $Bshop_id);
                }

                $getBuffetSum  =   CartProduct::where('cart_sub_type','=','buffet')->where('cart_id', '=', $cart->id)->groupBy('category_id')->get(array(DB::raw('SUM(total_price) as total'),'shop_id')); 
                foreach( $getBuffetSum as $vals)
                {
                $shop_id = $vals->shop_id;
                $total = $vals->total; 
                CartProduct::where('cart_sub_type','=','buffet')->where('shop_id', '=', $shop_id)->where('cart_id', '=', $cart->id)->update(['new_price'=>$total]);
                }
                }
                // buffet case only- because we are displaying only branch in cart  //

                // Kosha case only- because we are displaying only branch in cart  //

                $Kcosha = array('design','readymade');

                $getcoshaCount =   CartProduct::where('cart_sub_type','=','cosha')->where('quantity', '>=', 1)->where('cart_id', '=', $cart->id)->count();
                if($getcoshaCount >=1)
                {
                $getcosha  =   CartProduct::where('cart_sub_type','=','cosha')->where('quantity', '>=', 1)->where('cart_id', '=', $cart->id)->get(); 

                foreach ($getcosha as $getcoshaue) 
                {                  
                $Bid = $getcoshaue->id;                      
                array_push($artarrya, $Bid); 
                }
                }
                // Kosha case only- because we are displaying only branch in cart  //

                if ($input['language_type'] == 'ar') {

                /* FOR ARABIC LANGUAGE */                       
                $getcartPro = CartProduct::distinct()->select('merchant_id')->where('cart_id', '=', $cart->id)->where('status', '!=', 0)->get();

                foreach($getcartPro as $merval)
                {

                $merID = $merval->merchant_id;
                if($merID != 0)
                {
                $getMerID = DB::table('nm_merchant')->where('mer_id',$merID)->first();



                if(isset($getMerID->mer_fname_ar) && $getMerID->mer_fname_ar!='')
                {
                $merval->fname = $getMerID->mer_fname_ar;
                $merval->lname = $getMerID->mer_lname_ar; 
                }
                
                else
                {
                  $merval->fname = '';
                $merval->lname = '';   
                }

 

                if(isset($getMerID->mer_fname_ar) && $getMerID->mer_fname_ar=='')
                {
                    if(isset($getMerID->mer_fname) && $getMerID->mer_fname!='')
                    {
                    $merval->fname = $getMerID->mer_fname;
                    $merval->lname = $getMerID->mer_lname; 
                    }
                    else
                    {
                    $merval->fname = '';
                    $merval->lname = '';     
                    }
               
                }

                }
                if($merID == 0)
                {
                $merval->fname = 'Admin';
                $merval->lname = '';

                }

            $cartPro = CartProduct::distinct()->select('cart_sub_type')->where('merchant_id', '=', $merID)->where('cart_id', '=', $cart->id)->where('status', '!=', 0)->get();

            $merval->cart_sub_type = $cartPro;

            foreach ($cartPro as  $value) { 

            $Subtype = $value->cart_sub_type;


                $cart_pro = CartProduct::select('id','new_price','merchant_id','shop_id','shop_vendor_id','cart_type','fabric_name','cart_sub_type','cart_id','category_id','product_id','pro_title_ar as pro_title','pro_desc_ar as pro_desc','pro_Img','quantity','paid_total_amount','total_price','product_size','pro_price','insurance_amount','status','created_at','rose_package_type','buy_rent','coupon_code_amount','price_after_coupon_applied','coupon_code')->where('cart_id', '=', $cart->id)->whereNotIn('id', $artarrya)->where('cart_sub_type', '=', $Subtype)->where('merchant_id', '=', $merID)->where('status', '!=', 0)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_Img,pro_price,Insuranceamount,pro_disprice')->with('getCategory:mc_id,mc_name,mc_img')->orderBy('updated_at', 'DESC')->get(); //get user's cart detail

              $value->products = $cart_pro;
                foreach($cart_pro as $val) {
                $CProID = $val->id;

                $cart_type = $val->cart_type;
                $buy_rent = $val->buy_rent;
                $category_id = $val->category_id;
                //display all related cosha products to cart //
                $val->cosha_products = '';
                $ChekCosha = DB::table('nm_cart_services_attribute')->where('cart_type','occasion')->where('attribute_title','Design Your Kosha')->where('category_id',$category_id)->where('cart_id',$cart->id)->count();
                if($ChekCosha >=1 && $buy_rent!='readymade')
                {

                $ChekCoshaInfo = DB::table('nm_cart_services_attribute')->where('cart_type','occasion')->where('attribute_title','Design Your Kosha')->where('category_id',$category_id)->where('cart_id',$cart->id)->get();  
                $Title = '';
                foreach ($ChekCoshaInfo as $key => $value) {
                $Title = $value->pro_title_ar.','.$Title;
                }
                $val->cosha_products = rtrim($Title,',');
                } 
                //display all related cosha products to cart //
                if($val->cart_sub_type=='buffet'){
                $shop_id = $val->shop_id; 
                $getexternalprod=CartExternalFoodDish::select('id','external_food_dish_id')->where('cart_id','=', $val->cart_id)->where('category_id','=', $shop_id)->with('getFood:pro_id,pro_title_ar as pro_title,pro_Img')->get();
                $val->buffetProduct = $getexternalprod;
                }

                $cart_type = $val->cart_type;
                if($cart_type == 'hall')
                {
                $val->total_price = $val->paid_total_amount;
                }

                $proIDD = $val->product_id;
                $ChkRent = DB::table('nm_cart_product_rent')->where('cart_id','=', $val->cart_id)->where('cart_product_id','=', $CProID)->count();
                if($ChkRent >=1)
                {
                $val->rental_product = '1';
                }
                else
                {
                $val->rental_product = '0'; 
                }

                $shopid = $val->shop_id;
                $cart_sub_type = $val->cart_sub_type;
                $cart_type = $val->cart_type;
                $getinfo = $this->getshippingInfo($proIDD,$cart_sub_type,$cart_type);
                $lang = 'ar';
                $shopname = $this->getshopname($shopid,$lang);
                $val->shipping_methods =  $getinfo;
                $val->shopname =  $shopname;
                }
                }
            }

                if (!empty($getcartPro)) {
                $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'تم العثور على البيانات'],'view_cart' => $getcartPro);
                } else {
                $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => 'لم يتم العثور على بيانات']);
                }

                } else {
                /*FOR ENGLISH LANGUAGE*/                     



                $getcartPro = CartProduct::distinct()->select('merchant_id')->where('cart_id', '=', $cart->id)->where('status', '!=', 0)->get();

                foreach($getcartPro as $merval)
                {

                $merID = $merval->merchant_id;

                $getMerID = DB::table('nm_merchant')->where('mer_id',$merID)->first();
                if($merID != 0)
                {
                $getMerID = DB::table('nm_merchant')->where('mer_id',$merID)->first();
                if(isset($getMerID->mer_fname) && $getMerID->mer_fname!='')
                {
                $merval->fname = $getMerID->mer_fname;
                $merval->lname = $getMerID->mer_lname;
                }
                else
                {
                $merval->fname = '';
                $merval->lname = ''; 
                }

                  
                }
                if($merID == 0)
                {
                $merval->fname = 'Admin';
                $merval->lname = '';
                }

                $cartPro = CartProduct::distinct()->select('cart_sub_type')->where('merchant_id', '=', $merID)->where('cart_id', '=', $cart->id)->where('status', '!=', 0)->get();

                $merval->cart_sub_type = $cartPro;

                foreach ($cartPro as  $value) { 

                $Subtype = $value->cart_sub_type;
               

                $cart_pro = CartProduct::select('id','new_price','merchant_id','shop_id','shop_vendor_id','cart_type','fabric_name','cart_sub_type','cart_id','category_id','product_id','pro_title','pro_desc','pro_Img','quantity','paid_total_amount','total_price','product_size','pro_price','insurance_amount','status','created_at','rose_package_type','buy_rent','coupon_code_amount','price_after_coupon_applied','coupon_code')->where('cart_sub_type', '=', $Subtype)->where('merchant_id', '=', $merID)->where('cart_id', '=', $cart->id)->where('status', '!=', 0)->whereNotIn('id', $artarrya)->with('getProduct:pro_id,pro_title,pro_Img,pro_price,Insuranceamount,pro_disprice')->with('getCategory:mc_id,mc_name,mc_img')->get();

 

                $value->products = $cart_pro;


                foreach($cart_pro as $val) {
 
                $CProID = $val->id;
                $cart_type = $val->cart_type;
                $category_id = $val->category_id;

                //display all related cosha products to cart //
                $val->cosha_products = '';
                $ChekCosha = DB::table('nm_cart_services_attribute')->where('cart_type','occasion')->where('attribute_title','Design Your Kosha')->where('category_id',$category_id)->where('cart_id',$cart->id)->count();


                if($ChekCosha >=1 && $val->buy_rent !='readymade')
                {

                $ChekCoshaInfo = DB::table('nm_cart_services_attribute')->where('cart_type','occasion')->where('attribute_title','Design Your Kosha')->where('category_id',$category_id)->where('cart_id',$cart->id)->get();  
                $Title = '';
                foreach ($ChekCoshaInfo as $key => $value) {
                $Title = $value->pro_title.','.$Title;
                }
                $val->cosha_products = rtrim($Title,',');
                } 

                //display all related cosha products to cart //
                if($val->cart_sub_type=='buffet'){
                $shop_id = $val->shop_id; 
                $getexternalprod=CartExternalFoodDish::select('id','external_food_dish_id')->where('cart_id','=', $val->cart_id)->where('category_id','=', $shop_id)->with('getFood:pro_id,pro_title,pro_Img')->get();
                $val->buffetProduct = $getexternalprod;
                }

                if($cart_type == 'hall')
                {
                $val->total_price = $val->paid_total_amount;
                }
                $proIDD = $val->product_id;
                $ChkRent = DB::table('nm_cart_product_rent')->where('cart_id','=', $val->cart_id)->where('cart_product_id','=', $CProID)->count();
                if($ChkRent >=1)
                {
                $val->rental_product = '1';
                }
                else
                {
                $val->rental_product = '0'; 
                }
                $shopid = $val->shop_id;
                $cart_sub_type = $val->cart_sub_type;                     
                $getinfo = $this->getshippingInfo($proIDD,$cart_sub_type,$cart_type);

                $val->shipping_methods =  $getinfo;


                $lang = 'en';
                if($shopid!=''){
                $shopname = $this->getshopname($shopid,$lang);
                $val->shopname =  $shopname;  
                }
                else
                {
                $val->shopname = '';
                }

                }
                }
                } 
                //get user's cart detail
                if (!empty($getcartPro)) {
                $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'],'view_cart' => $getcartPro);
                } else {
                $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => 'Data not found']);
                }

                }

                $response = Response::json($response_array);

                } else {

                $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => trans($locale.'_mob_lang.CART_EMPTY')]);
                $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR')]);
                $response = Response::json($response_array);
            }
        }

        return $response;

   }
 




    //View add to cart list
    public function viewCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'language_type' => 'required|max:255'

        ]);

        $locale='';
        if(isset($request->lang) && $request->language_type=='ar' )
        {
            $locale='ar';
        }
        else
        {
            $locale='en';
        }
        app()->setLocale($locale); 

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => trans($locale.'_mob_lang.INPUT_ERROR')]);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
               
                //get user's detail
               
                
                if (!empty($cart)) { 

// buffet case only- because we are displaying only branch in cart  //
                $artarrya = array(0); $arshop = array();


                $getBuffetCount =   CartProduct::where('cart_sub_type','=','buffet')->where('cart_id', '=', $cart->id)->count();
                if($getBuffetCount >=1)
                {
                    $getBuffet  =   CartProduct::where('cart_sub_type','=','buffet')->where('cart_id', '=', $cart->id)->get(); 
                    foreach ($getBuffet as $getBuffetue) 
                    {
                    $Bshop_id = $getBuffetue->shop_id;
                    $Bid = $getBuffetue->id; 
                    if(in_array($Bshop_id, $arshop))
                    {
                    array_push($artarrya, $Bid);   
                    }
                    array_push($arshop, $Bshop_id);
                    }

                    $getBuffetSum  =   CartProduct::where('cart_sub_type','=','buffet')->where('cart_id', '=', $cart->id)->groupBy('category_id')->get(array(DB::raw('SUM(total_price) as total'),'shop_id')); 
                    foreach( $getBuffetSum as $vals)
                    {
                    $shop_id = $vals->shop_id;
                    $total = $vals->total; 
                    CartProduct::where('cart_sub_type','=','buffet')->where('shop_id', '=', $shop_id)->where('cart_id', '=', $cart->id)->update(['new_price'=>$total]);
                    }
 
                }




                // buffet case only- because we are displaying only branch in cart  //

                // Kosha case only- because we are displaying only branch in cart  //

                $Kcosha = array('design','readymade');
 
                $getcoshaCount =   CartProduct::where('cart_sub_type','=','cosha')->where('quantity', '>=', 1)->where('cart_id', '=', $cart->id)->count();
                if($getcoshaCount >=1)
                {
                    $getcosha  =   CartProduct::where('cart_sub_type','=','cosha')->where('quantity', '>=', 1)->where('cart_id', '=', $cart->id)->get(); 

                  foreach ($getcosha as $getcoshaue) 
                    {                  
                    $Bid = $getcoshaue->id;                      
                  	  array_push($artarrya, $Bid); 
                    }
            }
 
// Kosha case only- because we are displaying only branch in cart  //



                    if ($input['language_type'] == 'ar') {
					
                      
						$cart_pro = CartProduct::select('id','new_price','merchant_id','shop_id','shop_vendor_id','cart_type','fabric_name','cart_sub_type','cart_id','category_id','product_id','pro_title_ar as pro_title','pro_desc_ar as pro_desc','pro_Img','quantity','paid_total_amount','total_price','product_size','pro_price','insurance_amount','status','created_at','rose_package_type','buy_rent')->where('cart_id', '=', $cart->id)->whereNotIn('id', $artarrya)->where('status', '!=', 0)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_Img,pro_price,Insuranceamount,pro_disprice')->with('getCategory:mc_id,mc_name,mc_img')->orderBy('updated_at', 'DESC')->get(); //get user's cart detail
                        
                            foreach($cart_pro as $val) {
                                $CProID = $val->id;
                                $cart_type = $val->cart_type;
                                $cart_sub_type = $val->cart_sub_type;
                                $category_id = $val->category_id;

                                $product_id = $val->product_id;
                                //incase of hospitality design

                                $val->hospitality_products = '';
                                $getPinfo = Products::where('pro_id',$product_id)->first();

                                if($getPinfo->packege =='no' && $cart_sub_type == 'hospitality')
                                {
                                
								 $ChekCoshaInfo = DB::table('nm_cart_services_attribute')->where('cart_type','occasion')->where('attribute_title','Design Your Package')->where('category_id',$category_id)->where('cart_id',$cart->id)->get();   
                                $Hos_Title = '';$Hos_totalPrice=0;
                                foreach ($ChekCoshaInfo as $key => $value) {
                                $Hos_Title = $value->pro_title.','.$Hos_Title;
                                $Hos_totalPrice = $Hos_totalPrice + $value->total_price;
                                }
                                $val->hospitality_products = rtrim($Hos_Title,',');

                                }

                                //display all related cosha products to cart //
                                $val->cosha_products = '';
                                 $ChekCosha = DB::table('nm_cart_services_attribute')->where('cart_type','occasion')->where('attribute_title','Design Your Kosha')->where('category_id',$category_id)->where('cart_id',$cart->id)->count();

                                if($ChekCosha >=1 && $val->buy_rent !='readymade')
                                {

                                
								 $ChekCoshaInfo = DB::table('nm_cart_services_attribute')->where('cart_type','occasion')->where('attribute_title','Design Your Kosha')->where('category_id',$category_id)->where('cart_id',$cart->id)->get();   
                                $Title = '';$totalPrice=0;
                                foreach ($ChekCoshaInfo as $key => $value) {
                                $Title = $value->pro_title_ar.','.$Title;
                                $totalPrice = $totalPrice + $value->total_price;
                                }
                                $val->cosha_design_price = $totalPrice;

                                 $val->total_price= $totalPrice;
                                $val->cosha_products = rtrim($Title,',');
                                } 

                                //display all related cosha products to cart //
                                if($val->cart_sub_type=='buffet'){
                                    $shop_id = $val->shop_id; 
                                    $getexternalprod=CartExternalFoodDish::select('id','external_food_dish_id')->where('cart_id','=', $val->cart_id)->where('category_id','=', $shop_id)->with('getFood:pro_id,pro_title,pro_Img')->get();
                                     $val->buffetProduct = $getexternalprod;
                                }


                            if($cart_type == 'hall')
                            {
                            $val->total_price = $val->paid_total_amount;
                            }


                            $proIDD = $val->product_id;



                            $ChkRent = DB::table('nm_cart_product_rent')->where('cart_id','=', $val->cart_id)->where('cart_product_id','=', $CProID)->count();
                            if($ChkRent >=1)
                            {
                            $val->rental_product = '1';
                            }
                            else
                            {
                            $val->rental_product = '0'; 
                            }
                            $shopid = $val->shop_id;
                            $cart_sub_type = $val->cart_sub_type;                     
                           $getinfo = $this->getshippingInfo($proIDD,$cart_sub_type,$cart_type);
    
                            $val->shipping_methods =  $getinfo;


                            $lang = 'ar';
                            if($shopid!=''){
                                $shopname = $this->getshopname($shopid,$lang);
                            $val->shopname =  $shopname;  
                            }
                            else
                            {
                            $val->shopname = '';
                            }
                          
                             }
                           
 
                        //get user's cart detail
                        if (!empty($cart_pro)) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'],'view_cart' => $cart_pro);
                        } else {
                            $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => 'Data not found']);
                        }
					
					} else {
                        /*FOR ENGLISH LANGUAGE*/
                        
                        /*
                        $cart_pro = CartProduct::select('id','merchant_id','shop_id','shop_vendor_id','cart_type','fabric_name','cart_sub_type','cart_id','category_id','product_id','pro_title','pro_desc','pro_Img','quantity','total_price','product_size','pro_price','insurance_amount','status','created_at','rose_package_type')->where('cart_id', '=', $cart->id)->with('getProduct:pro_id,pro_title,pro_Img,pro_price,Insuranceamount,pro_disprice')->with('getCategory:mc_id,mc_name,mc_img')->orderBy('updated_at', 'DESC')->distinct()->get();*/

                        
 
                        $cart_pro = CartProduct::select('id','new_price','merchant_id','shop_id','shop_vendor_id','cart_type','fabric_name','cart_sub_type','cart_id','category_id','product_id','pro_title','pro_desc','pro_Img','quantity','paid_total_amount','total_price','product_size','pro_price','insurance_amount','status','created_at','rose_package_type','buy_rent')->where('cart_id', '=', $cart->id)->where('status', '!=', 0)->whereNotIn('id', $artarrya)->with('getProduct:pro_id,pro_title,pro_Img,pro_price,Insuranceamount,pro_disprice')->with('getCategory:mc_id,mc_name,mc_img')->orderBy('updated_at', 'DESC')->get();
                        
                            foreach($cart_pro as $val) {
                                $CProID = $val->id;
                                $cart_type = $val->cart_type;
                                $cart_sub_type = $val->cart_sub_type;
                                $category_id = $val->category_id;

                                $product_id = $val->product_id;
                                //incase of hospitality design

                                $val->hospitality_products = '';
                                $getPinfo = Products::where('pro_id',$product_id)->first();

                                if($getPinfo->packege =='no' && $cart_sub_type == 'hospitality')
                                {
                                $ChekCoshaInfo = DB::table('nm_cart_services_attribute')->where('cart_type','occasion')->where('attribute_title','Design Your Package')->where('category_id',$category_id)->where('cart_id',$cart->id)->get();  
                                $Hos_Title = '';$Hos_totalPrice=0;
                                foreach ($ChekCoshaInfo as $key => $value) {
                                $Hos_Title = $value->pro_title.','.$Hos_Title;
                                $Hos_totalPrice = $Hos_totalPrice + $value->total_price;
                                }
                                $val->hospitality_products = rtrim($Hos_Title,',');

                                }

                                //display all related cosha products to cart //
                                $val->cosha_products = '';
                                $ChekCosha = DB::table('nm_cart_services_attribute')->where('cart_type','occasion')->where('attribute_title','Design Your Kosha')->where('category_id',$category_id)->where('cart_id',$cart->id)->count();


                                if($ChekCosha >=1 && $val->buy_rent !='readymade')
                                {

                                $ChekCoshaInfo = DB::table('nm_cart_services_attribute')->where('cart_type','occasion')->where('attribute_title','Design Your Kosha')->where('category_id',$category_id)->where('cart_id',$cart->id)->get();  
                                $Title = '';$totalPrice=0;
                                foreach ($ChekCoshaInfo as $key => $value) {
                                $Title = $value->pro_title.','.$Title;
                                $totalPrice = $totalPrice + $value->total_price;
                                }
                                $val->cosha_design_price = $totalPrice;

                                 $val->total_price= $totalPrice;
                                $val->cosha_products = rtrim($Title,',');
                                } 

                                //display all related cosha products to cart //
                                if($val->cart_sub_type=='buffet'){
                                    $shop_id = $val->shop_id; 
                                    $getexternalprod=CartExternalFoodDish::select('id','external_food_dish_id')->where('cart_id','=', $val->cart_id)->where('category_id','=', $shop_id)->with('getFood:pro_id,pro_title,pro_Img')->get();
                                     $val->buffetProduct = $getexternalprod;
                                }


                            if($cart_type == 'hall')
                            {
                            $val->total_price = $val->paid_total_amount;
                            }


                            $proIDD = $val->product_id;



                            $ChkRent = DB::table('nm_cart_product_rent')->where('cart_id','=', $val->cart_id)->where('cart_product_id','=', $CProID)->count();
                            if($ChkRent >=1)
                            {
                            $val->rental_product = '1';
                            }
                            else
                            {
                            $val->rental_product = '0'; 
                            }
                            $shopid = $val->shop_id;
                            $cart_sub_type = $val->cart_sub_type;                     
                           $getinfo = $this->getshippingInfo($proIDD,$cart_sub_type,$cart_type);
    
                            $val->shipping_methods =  $getinfo;


                            $lang = 'en';
                            if($shopid!=''){
                                $shopname = $this->getshopname($shopid,$lang);
                            $val->shopname =  $shopname;  
                            }
                            else
                            {
                            $val->shopname = '';
                            }
                          
                             }
                           
 
                        //get user's cart detail
                        if (!empty($cart_pro)) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'],'view_cart' => $cart_pro);
                        } else {
                            $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => 'Data not found']);
                        }

                    }

                      $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => trans($locale.'_mob_lang.CART_EMPTY')]);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR')]);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }


public static function getshopname($shopid,$lang)
{
    $shopname = '';
    if($shopid!=''){
    $getProInfo = DB::table('nm_category')->where('mc_id', $shopid)->first(); 
   
    if($lang =='en')
    {
    $shopname = $getProInfo->mc_name;
    }
    else
    {
    $shopname = $getProInfo->mc_name_ar;     
    }
    }
    return $shopname;

}


public static function getshippingInfo($proIDD,$cart_sub_type,$cart_type)
            {
        // $cart_sub_type='cosha';
                
                  $getProInfocount = DB::table('nm_product')->where('pro_id', $proIDD)->count(); 
               
                $getProInfo = DB::table('nm_product')->where('pro_id', $proIDD)->first();

                
               
               if($getProInfocount >0) {
                $pro_mc_cid = $getProInfo->pro_mc_id;

              }
              
                if($cart_sub_type=='invitations'  ) {
                     $merchantID = '';

                } else {
                $merchantID = $getProInfo->pro_mr_id;
                 }
 

                if(isset($pro_mc_cid)!='') {
                $getCatInfo = DB::table('nm_category')->where('mc_id', $pro_mc_cid)->first();
                 }

               if($cart_sub_type=='invitations') {
                     $parent_id = '';

                } else {
                $parent_id = $getCatInfo->parent_id;
                }


                $getCatInfoaa = DB::table('nm_category')->where('mc_id', $parent_id)->first();

                  if($cart_sub_type=='invitations') {
                     $parent_ida = '';

                } else {
                $parent_ida = $getCatInfoaa->parent_id;
                }

                $getCatInfoahu= DB::table('nm_category')->where('mc_id', $parent_ida)->first();

                 if($cart_sub_type=='invitations') {
                     $parent_idas = '';

                } else {
                $parent_idas = $getCatInfoahu->parent_id;
               }

                $pick =0; $aramax = 0;

                  $getInfo = DB::table('nm_service_shipping')->where('vendor_id', $merchantID)->count();
 


                if($getInfo >=1)
                {
                $getShippingMethod = DB::table('nm_service_shipping')->where('vendor_id', $merchantID)->first();
            
                if($cart_sub_type=='buffet')
                {


                 $Buffet = explode(",", $getShippingMethod->Buffet);
               
                if(isset($Buffet[0]) && $Buffet[0] !='')
                {
                if($Buffet[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Buffet[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Buffet[1]) && $Buffet[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }

                }


                if($cart_sub_type=='dessert')
                {

                $Desert = explode(",",  $getShippingMethod->Desert);
                if(isset($Desert[0]) && $Desert[0] !='')
                {
                if($Desert[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Desert[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Desert[1]) && $Desert[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }

                }
                if($cart_sub_type=='dates')
                {

                $Dates = explode(",",  $getShippingMethod->Dates); 
                if(isset($Dates[0]) && $Dates[0] !='')
                {
                if($Dates[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Dates[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Dates[1]) && $Dates[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }

                if($cart_sub_type=='cosha')
                {


                $Cosha = explode(",",  $getShippingMethod->Cosha); 
                if(isset($Cosha[0]) && $Cosha[0] !='')
                {
                if($Cosha[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Cosha[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Cosha[1]) && $Cosha[1] !='')
                {
                $pick = 0;  $aramax = 0;
                }
                }
                if($cart_sub_type=='roses')
                {


                $Roses = explode(",",  $getShippingMethod->Roses);
                if(isset($Roses[0]) && $Roses[0] !='')
                {
                if($Roses[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Roses[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Roses[1]) && $Roses[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }


                if($cart_sub_type=='events')
                {

                $Special_Events = explode(",",  $getShippingMethod->Special_Events);
                if(isset($Special_Events[0]) && $Special_Events[0] !='')
                {
                if($Special_Events[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Special_Events[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Special_Events[1]) && $Special_Events[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }



                if($cart_sub_type=='makeup')
                {

                $Makeup_Product = explode(",",  $getShippingMethod->Makeup_Product); 
                if(isset($Makeup_Product[0]) && $Makeup_Product[0] !='')
                {
                if($Makeup_Product[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Makeup_Product[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Makeup_Product[1]) && $Makeup_Product[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }


                if($cart_type=='music')
                {

                $Acoustics = explode(",",  $getShippingMethod->Acoustics); 
                if(isset($Acoustics[0]) && $Acoustics[0] !='')
                {
                if($Acoustics[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Acoustics[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Acoustics[1]) && $Acoustics[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }


                if($cart_sub_type=='tailor')
                {

                $Tailors = explode(",",  $getShippingMethod->Tailors); 
                if(isset($Tailors[0]) && $Tailors[0] !='')
                {
                if($Tailors[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Tailors[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Tailors[1]) && $Tailors[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }


                if($cart_sub_type=='dress')
                {

                $Dresses = explode(",",  $getShippingMethod->Dresses);
                if(isset($Dresses[0]) && $Dresses[0] !='')
                {
                if($Dresses[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Dresses[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Dresses[1]) && $Dresses[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }
                if($cart_sub_type=='abaya')
                {

                $Abaya = explode(",",  $getShippingMethod->Abaya); 
                if(isset($Abaya[0]) && $Abaya[0] !='')
                {
                if($Abaya[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Abaya[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Abaya[1]) && $Abaya[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }

                }
                if($cart_sub_type=='perfume')
                { 
                $Oud_and_Perfumes = explode(",",  $getShippingMethod->Oud_and_Perfumes); 
                if(isset($Oud_and_Perfumes[0]) && $Oud_and_Perfumes[0] !='')
                {
                if($Oud_and_Perfumes[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Oud_and_Perfumes[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Oud_and_Perfumes[1]) && $Oud_and_Perfumes[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                } 
 
                if(trim($cart_sub_type) == 'gold')
                {
 
                $Gold_and_Jewellery = explode(",",  $getShippingMethod->Gold_and_Jewellery);
                if(isset($Gold_and_Jewellery[0]) && $Gold_and_Jewellery[0] !='')
                {
                if($Gold_and_Jewellery[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Gold_and_Jewellery[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Gold_and_Jewellery[1]) && $Gold_and_Jewellery[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }


}
         

return ['aramax'=>$aramax,'pick_logistics'=>$pick];
 
            

            }








  //Display subcategory in cart
  
      //View add to cart list
    public function viewCartSubcategory(Request $request) {

        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'language_type' => 'required|max:255'

        ]);



        $locale='';
        if(isset($request->lang) && $request->language_type=='ar' )
        {
            $locale='ar';
        }
        else
        {
            $locale='en';
        }
        app()->setLocale($locale); 

        $input = $request->all();
         


        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => trans($locale.'_mob_lang.INPUT_ERROR')]);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                //get user's detail
                if (!empty($cart)) {                    

                    if ($input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */                       

                          $cart_pro = CartProduct::select('cart_sub_type','pro_Img','pro_price')->where('cart_id', '=', $cart->id)->orderBy('updated_at', 'DESC')->distinct('cart_sub_type')->get(); //get user's cart detail


                        if (!empty($cart_pro)) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'تم العثور على البيانات'],'view_cart' => $cart_pro);
                        } else {
                            $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => 'لم يتم العثور على بيانات']);
                        }

                    } else {
                        /*FOR ENGLISH LANGUAGE*/
                        

                        $cart_pro = CartProduct::select('cart_sub_type','pro_Img','pro_price')->where('cart_id', '=', $cart->id)->orderBy('updated_at', 'DESC')->distinct('cart_sub_type')->get(); //get user's cart detail
                        //get user's cart detail
                        if (!empty($cart_pro)) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'],'view_cart' => $cart_pro);
                        } else {
                            $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => 'Data not found']);
                        }

                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => trans($locale.'_mob_lang.CART_EMPTY')]);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR')]);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }  





  // Cart informations
  
   //View add to cart list
    public function viewCartList(Request $request) {

        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'language_type' => 'required|max:255'

        ]);

        $locale='';
        if(isset($request->lang) && $request->language_type=='ar' )
        {
            $locale='ar';
        }
        else
        {
            $locale='en';
        }
        app()->setLocale($locale); 

        $input = $request->all();

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => trans($locale.'_mob_lang.INPUT_ERROR')]);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();

                //get user's detail
               
                
                if (!empty($cart)) {                    

                    if ($input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */                       

                          $cart_pro = CartProduct::where('cart_id', '=', $cart->id)->where('cart_sub_type','=',$input['cart_sub_type'])->get(); //get user's cart detail



                         
                        if (!empty($cart_pro)) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'تم العثور على البيانات'],'view_cart' => $cart_pro);
                        } else {
                            $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => 'لم يتم العثور على بيانات']);
                        }

                    } else {
                        /*FOR ENGLISH LANGUAGE*/
                        

                          $cart_pro = CartProduct::where('cart_id', '=', $cart->id)->where('cart_sub_type', '=', $input['cart_sub_type'])->get();
                        //get user's cart detail
                        if (!empty($cart_pro)) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'],'view_cart' => $cart_pro);
                        } else {
                            $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => 'Data not found']);
                        }

                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => trans($locale.'_mob_lang.CART_EMPTY')]);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR')]);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }  




     //Details product cart informations

    //View add to cart list
    public function viewCartDetails(Request $request) {

        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'language_type' => 'required|max:255'

        ]);

        $locale='';
        if(isset($request->lang) && $request->language_type=='ar' )
        {
            $locale='ar';
        }
        else
        {
            $locale='en';
        }
        app()->setLocale($locale); 

        $input = $request->all();



        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => trans($locale.'_mob_lang.INPUT_ERROR')]);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();

                //get user's detail
               
                
                if (!empty($cart)) {                    

                    if ($input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */                       

                          $cart_pro = CartProduct::where('cart_id', '=', $cart->id)->where('product_id','=',$input['product_id'])->first(); //get user's cart detail

                         

                         
                        if (!empty($cart_pro)) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'تم العثور على البيانات'],'view_cart' => $cart_pro);
                        } else {
                            $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => 'لم يتم العثور على بيانات']);
                        }

                    } else {
                        /*FOR ENGLISH LANGUAGE*/
                        

                         $cart_pro = CartProduct::where('cart_id', '=', $cart->id)->where('product_id','=',$input['product_id'])->first(); //get user's cart detail
                        //get user's cart detail
                        if (!empty($cart_pro)) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'],'view_cart' => $cart_pro);
                        } else {
                            $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => 'Data not found']);
                        }

                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => trans($locale.'_mob_lang.CART_EMPTY')]);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR')]);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }  


    //Himanshu

   



}
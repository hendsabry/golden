<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Http\Models;
use App\Category;
use App\Products;
use App\ServiceAttribute;
use App\ProductOption;
use App\ProductOptionValue;
use App\ProductAttribute;
use App\City;
use App\CategoryGallery;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartBodyMeasurement;
use App\CartServiceAttribute;
use App\CartProductRent;
use App\CartProductAttribute;
use Auth;
use App\User;
use App\OrderProductShipping;
use Response;
use DB;


class ShoppingController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |   Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function __construct() {}

    /**Get Tailors info.**/

    /*
       vendor(3th lavel category in category table) become pro_mc_id in product table to get full Tailors info.
    */
    public function getTailorsInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id' => 'required', //Vendor ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();


        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->where('mc_status', '=', '1')->first();
                //GET SUB CATEGORY
                $sub_catg = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['subcategory_id'])->where('mc_status', '=', '1')->first();

                

                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($input['vendor_id']);

                if (!empty($category)) {
                    if ($input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                        //Vendor detail
                        $vendor = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name','longitude','latitude','mc_video_url')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();
                        $vId = $vendor->vendor_id;
                       
                        //GET VENDOR DETAIL
                        $product = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_price','pro_desc_ar as pro_desc','pro_disprice','deliver_day')->where('pro_mc_id', '=', $input['vendor_id'])->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                        $getFabricsC = DB::table('nm_fabric')->where('vendor_id', $vId)->where('status',1)->count();

                        $fabarray = array();
                        if($getFabricsC >=1)
                        {
                        $getFabrics = DB::table('nm_fabric')->where('vendor_id', $vId)->where('status',1)->get();
                        foreach($getFabrics as $key=>$val){
                        $Fname = $val->fabric_name;
                        array_push($fabarray, $Fname);

                        }

                        }
                          foreach($product as $key=>$val){

  

                          $optionvaluefabric = ProductOptionValue::select('id as fabric_id','option_title_ar as option_title','option_title as option_title_ar', 'price','image')->where('product_option_id','37')->whereIn('option_title',$fabarray)->where('product_id',$val->pro_id)->orderBy('option_title_ar', 'ASC')->paginate(1000);
 
                           $val->febric = $optionvaluefabric;
                           if($val->deliver_day !=''){
                                $val->deliver_day = $val->deliver_day;
                                }
                                else
                                {
                                $val->deliver_day =0;      
                                }

                        foreach($optionvaluefabric as $fabs)
                        {

                        $OT =  $fabs->option_title_ar;
                        $Vid =  $input['vendor_id'];
                        $getInfoc = DB::table('nm_fabric')->where('fabric_name',$OT)->where('status',1)->count();
                        

                        if($getInfoc >=1){
                        $getInfo = DB::table('nm_fabric')->where('fabric_name',$OT)->first();
  
                          $fabs->fabric_id = $getInfo->id;
                        }
 
                        } 

                          }

                        if (!$product->isEmpty()) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'sub_category_id' => $sub_catg->mc_id, 'sub_category_name' => $sub_catg->mc_name_ar, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'product' => $product);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                        $vendor = Category::select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name','longitude','latitude','mc_video_url')->where('mc_status', '=', '1')->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                        //GET VENDOR DETAIL
                        $vId = $vendor->vendor_id;
                        $product = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_price','pro_desc','pro_disprice','deliver_day')->where('pro_mc_id', '=', $input['vendor_id'])->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(100);


                            $getFabricsC = DB::table('nm_fabric')->where('vendor_id', $vId)->where('status',1)->count();

                            $fabarray = array();
                            if($getFabricsC >=1)
                            {
                            $getFabrics = DB::table('nm_fabric')->where('vendor_id', $vId)->where('status',1)->get();
                            foreach($getFabrics as $key=>$val){
                            $Fname = $val->fabric_name;
                            array_push($fabarray, $Fname);

                            }

                            }


                         foreach($product as $key=>$val){

                          $optionvaluefabric = ProductOptionValue::select('id as fabric_id','option_title', 'price','image')->where('product_option_id','37')->whereIn('option_title',$fabarray)->where('product_id',$val->pro_id)->orderBy('option_title', 'ASC')->paginate(1000);
                           $val->febric = $optionvaluefabric;
                                if($val->deliver_day !=''){
                                $val->deliver_day = $val->deliver_day;
                                }
                                else
                                {
                                $val->deliver_day =0;      
                                }


                        foreach($optionvaluefabric as $fabs)
                        {

                        $OT =  $fabs->option_title;
                        $Vid =  $input['vendor_id'];
                        $getInfoc = DB::table('nm_fabric')->where('fabric_name',$OT)->where('status',1)->count();
                        
                        if($getInfoc >=1){
                        $getInfo = DB::table('nm_fabric')->where('fabric_name',$OT)->first();
  
                          $fabs->fabric_id = $getInfo->id;
                        }
 
                        }

 
                          }


                        /*$optionvaluefabric = ProductOptionValue::select('option_title', 'price')->where('vandor_id', '=', $input['vendor_id'])->where('product_option_id','37')->orderBy('option_title', 'ASC')->paginate(100);
*/



                        if (!$product->isEmpty()) {
                            

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'sub_category_id' => $sub_catg->mc_id, 'sub_category_name' => $sub_catg->mc_name, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'product' => $product);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'Sorry, design not available']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Set Tailors data in add to cart
    public function setTailorsAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required', //Get Product ID
            'cart_type' => 'required', // Get Cart Type
            'cart_sub_type' => 'required', //Get Product Category
            'total_price' => 'required',
        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

                $User = Auth::user();
                $product = Products::where('pro_id', '=', $input['product_id'])->first();

                if (!empty($product)) {
                    $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();

 
                    if (!empty($cart)) { 
                        // $cart_pro = CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        // $cart_body_measurement = CartBodyMeasurement::where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $input['product_id'];
                    $cart_product_data['total_price'] = $input['total_price'];
                    $cart_product_data['status'] = 1;
                    $cart_product_data['shop_vendor_id'] = $category->parent_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['pro_price'] = $product->pro_price;
                    $cart_product_data['fabric_name'] = $input['fabric_name'];
                    $cart_product_data['quantity'] = $input['quantity'];


                 //Check Duplicate record then bypass it - start
                    $checkinproductsection =  CartProduct::where('merchant_id',$category->vendor_id)->where('shop_id',$category->mc_id)->where('shop_vendor_id',$category->parent_id)->where('cart_type',$input['cart_type'])->where('cart_sub_type',$input['cart_sub_type'])->where('cart_id',$cart->id)->where('product_id',$input['product_id'])->where('fabric_name',$input['fabric_name'])->count();
                    $checkinproductc = CartBodyMeasurement::where('cart_id',$cart->id)->where('customer_id',$User->cus_id)->where('product_id',$input['product_id'])->where('length',$input['length'])->where('chest_size',$input['chest_size'])->where('waistsize',$input['waistsize'])->where('soulders',$input['soulders'])->where('neck',$input['neck'])->where('arm_length',$input['arm_length'])->where('wrist_diameter',$input['wrist_diameter'])->where('cutting',$input['cutting'])->count();
                        if($checkinproductsection >=1 && $checkinproductc>=1)
                        {
                            $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                            $response = Response::json($response_array, 200);   
                            return $response;
                        }
                    //Check Duplicate record then bypass it - end




                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                    $getCartProID = $cart_product->id;
                    $cart_body_measurement = array();
                    if(!empty($input['length'])){

                        $cart_body_measurement['cart_id'] = $cart->id;
                        $cart_body_measurement['customer_id'] = $User->cus_id;
                        $cart_body_measurement['product_id'] = $input['product_id'];
                        $cart_body_measurement['length'] = $input['length'];                    
                        $cart_body_measurement['chest_size'] = $input['chest_size'];                  
                        
                        $cart_body_measurement['waistsize'] = $input['waistsize'];                  
                        $cart_body_measurement['soulders'] = $input['soulders'];
                        $cart_body_measurement['neck'] = $input['neck'];
                        $cart_body_measurement['arm_length'] = $input['arm_length'];                    
                        $cart_body_measurement['wrist_diameter'] = $input['wrist_diameter'];    
                        $cart_body_measurement['cutting'] = $input['cutting']; 
                        $cart_body_measurement['status'] = 1;  
                        $cart_body_measurement['cart_product_id'] = $getCartProID; 


                        CartBodyMeasurement::create($cart_body_measurement); //Product rental date and time entry 
                    }
                    

                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.CART_ERROR'), 'code' => 500]);
                    $response = Response::json($response_array, 500);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }

    /**Get Dresses info.**/

    /*
       vendor(3th lavel category in category table) become pro_mc_id in product table to get full Dress info.
    */
   public function getDressesInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id' => 'required', //Vendor ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['category_id'])->first();
                //GET SUB CATEGORY
                $sub_catg = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->first();

                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($input['vendor_id']);

                if (!empty($category)) {

                    if ($input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                       
                        //GET VENDOR DETAIL
                        $vendor = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();
                        //Get Dress Menu List

  
                        $chkCats = Products::select( 'attribute_id')->where('pro_mc_id', '=', $input['vendor_id'])->where('pro_status', '=', 1)->count();
                        $cats = array();
                        if($chkCats >=1)
                        {

                        $chkCatsinf = Products::select( 'attribute_id')->where('pro_mc_id', '=', $input['vendor_id'])->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->get();
                        foreach ($chkCatsinf as $key => $value) {
                        $attributid =  $value->attribute_id;
                        array_push($cats, $attributid);
                        }

                        }
 

                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'image', 'status')->whereIn('id',$cats)->where('status', '=', '1')->where('services_id', '=', $input['vendor_id'])->get();
                        if (isset($menu)) {

                            foreach($menu as $key => $value) {
                                //get style detail according menu
                                $dress_style = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour', 'Insuranceamount','pro_disprice')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                                $value->dress_style = $dress_style;
                                foreach($dress_style as $val) {
                                    $name = explode(',', $val->option_id);
                                    //get service attribute option detail
                                    $style_Option = ProductOption::select('id', 'option_title_ar as option_title', 'button_type_id', 'service_hour')->whereIn('id', $name)->with('getBoxType:id,option_title_ar as option_title,type')->get();
                                    //Get service attribute option value with container list
                                     $val->style_option = $style_Option;
                                     foreach($style_Option as $pro_opt) {
                                        $style_Option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title_ar as option_title', 'image', 'price','discount_price','experience', 'status','value','value_ar as value_rent')->where('product_id', '=', $val->pro_id)->where('product_option_id', '=', $pro_opt->id)->get();
                                            foreach ($style_Option_value as $value) {
                                            if($value->value_rent=='')
                                            {
                                            $value->value_rent=0;
                                             //for discount rent case
                                        $chkCount = ProductOptionValue::select('discount')->where('product_id', '=', $val->pro_id)->where('option_title', '=', 'Discount for rent')->count(); 
                                        if( $chkCount >=1)
                                        {
                                        $chkDis = ProductOptionValue::select('discount')->where('product_id', '=', $val->pro_id)->where('option_title', '=', 'Discount for rent')->first(); 
                                        //$value->value_rent=$chkDis->discount;
                                        $value->discount_price= ($value->price - ($value->price * $chkDis->discount)/100);
                                        }
                                            }
                                            }

                                           $pro_opt->style_option_value = $style_Option_value;

                                     }                                    
 
                                }

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'م العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'dress_menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                        //GET VENDOR DETAIL
                       $vendor = Category::select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                        //Get Dress Menu List
                          $chkCats = Products::select( 'attribute_id')->where('pro_mc_id', '=', $input['vendor_id'])->where('pro_status', '=', 1)->count();
                        $cats = array();
                        if($chkCats >=1)
                        {

                        $chkCatsinf = Products::select( 'attribute_id')->where('pro_mc_id', '=', $input['vendor_id'])->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->get();
                        foreach ($chkCatsinf as $key => $value) {
                        $attributid =  $value->attribute_id;
                        array_push($cats, $attributid);
                        }

                        }

                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->where('status', '=', '1')->whereIn('id',$cats)->where('services_id', '=', $input['vendor_id'])->get();
                        if (isset($menu)) {

                            foreach($menu as $key => $value) {
                                //get style detail according menu
                                $dress_style = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour', 'Insuranceamount','pro_disprice')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                                $value->dress_style = $dress_style;
                                foreach($dress_style as $val) {
                                    $name = explode(',', $val->option_id);
                                    //get service attribute option detail
                                    $style_Option = ProductOption::select('id', 'option_title', 'button_type_id', 'service_hour')->whereIn('id', $name)->with('getBoxType:id,option_title,type')->get();
                                    //Get service attribute option value with container list
                                    $val->style_option = $style_Option;
                                    foreach($style_Option as $pro_opt) {
                                        $style_Option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title', 'image', 'price', 'experience', 'status','discount_price','value','value_ar as value_rent')->where('product_id', '=', $val->pro_id)->where('product_option_id', '=', $pro_opt->id)->get();

                                        foreach ($style_Option_value as $value) {
                                           if($value->value_rent=='')
                                           {
                                        $value->value_rent=0;


                                        //for discount rent case
                                        $chkCount = ProductOptionValue::select('discount')->where('product_id', '=', $val->pro_id)->where('option_title', '=', 'Discount for rent')->count(); 
                                        if( $chkCount >=1)
                                        {
                                        $chkDis = ProductOptionValue::select('discount')->where('product_id', '=', $val->pro_id)->where('option_title', '=', 'Discount for rent')->first(); 
                                        //$value->value_rent=$chkDis->discount;
                                        $value->discount_price= ($value->price - ($value->price * $chkDis->discount)/100);
                                        }

  
                                           }
                                        }

                                    $pro_opt->style_option_value = $style_Option_value;
                                     } 

                                    
                                    

                                }

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'dress_menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Set Dresses data in add to cart
    public function setDressAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required', //Get Product ID
            'cart_type' => 'required', // Get Cart Type
            'cart_sub_type' => 'required', //Get Product ID
            'attribute_id' => 'required', //Get Product ID
            'size' => 'required', //Get Product Size
            'product_qty' => 'required', //Get Product Quantity
            'token' => 'required',
            'total_price' => 'required'
            

        ]);



        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

                $User = Auth::user();
                $product = Products::where('pro_id', '=', $input['product_id'])->first();
                $productquantity = DB::table('nm_product_option_value')->where('option_title',$input['size'])->where('product_id', '=', $input['product_id'])->first();
 
               

                 if(!empty($input['rental_date']))
                    {
                    $buy_rent = 'rent';
                    $qtys = $productquantity->value_ar;
                    }
                    else
                    {
                      $buy_rent = 'buy';  
                      $qtys = $productquantity->value;
                    }

  if($qtys < $input['product_qty']) {
$response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
 $response = Response::json($response_array, 204);
}

                  //echo $input['product_qty'];die;
               // if ($product->pro_qty >= $input['product_qty']) {
               if($qtys >= $input['product_qty']) {
 
                    $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                    if (!empty($cart)) {

   

                    if(!empty($input['rental_date']))
                    {
                    $buy_rent = 'rent';
                    }
                    else
                    {
                      $buy_rent = 'buy';  
                    }

                        $cart_pro = CartProduct::where('cart_type', '=','shopping')->where('cart_id', '=', $cart->id)->where('buy_rent', '=', $buy_rent)->where('product_id', '=', $input['product_id'])->where('product_size',$input['size'])->count();

                        if($cart_pro!='') {
                         $cart_pro = CartProduct::where('cart_type', '=','shopping')->where('cart_id', '=', $cart->id)->where('buy_rent', '=', $buy_rent)->where('product_id', '=', $input['product_id'])->where('product_size',$input['size'])->first();
                        $buyrent =  $cart_pro->buy_rent;
                        if($buyrent!='rent')
                        {
                            $tatalquantity = $cart_pro->quantity + $input['product_qty']; 

                         $totalproprice = $cart_pro->total_price + $input['total_price'];

                         CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])
                          ->update([
                            'quantity' => $tatalquantity,
                            'total_price' => $totalproprice

                            ]);

                        }
                         


                        }



                       // $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                       // $cart_pro_rent = CartProductRent::where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                       // $cart_pro_opt = CartOption::where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                       // $cart_pro_val = CartOptionValue::where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                 

                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }

                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $input['product_id'];

                    /*if (!empty($product_options_value)) {
                   
                    $date1=date_create($input['rental_time']);
                    $date2=date_create($input['return_time']);
                    $diff=date_diff($date1,$date2);
                    $diff->format("%H:%I"); 
                    $cart_product_data['total_price'] = (($product->Insuranceamount + $product_options_value->price)*$diff->format("%H")) * $input['product_qty'];

                    } else {
                       $cart_product_data['total_price'] = $product->pro_price * $input['product_qty'];
                    } */ 
                    $cart_product_data['total_price'] = $input['total_price']; 
                    $cart_product_data['quantity'] = $input['product_qty'];
                    $cart_product_data['product_size'] = $input['size'];
                    $cart_product_data['shop_vendor_id'] = $category->parent_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;

                    if(!empty($input['rental_date']))
                    {
                        $pprice = DB::table('nm_product_option_value')->where('option_title','Discount Price')->where('product_id', '=', $input['product_id'])->count();

                        if($pprice >=1)
                        {
                        $ppricea = DB::table('nm_product_option_value')->where('option_title','Discount Price')->where('product_id', '=', $input['product_id'])->first();  
                        $prixe = $ppricea->discount_price;
                        }
                        else
                        {
                        $pprice = DB::table('nm_product_option_value')->where('option_title','Rent')->where('product_id', '=', $input['product_id'])->first();
                        $prixe = $ppricea->price;
                        }

                    $cart_product_data['pro_price'] = $prixe;
                    }
                    else
                    {
                    $cart_product_data['pro_price'] = $product->pro_price;
                    }

                    $cart_product_data['insurance_amount'] = $product->Insuranceamount;
                    $cart_product_data['status'] = 1;
                     if(!empty($input['rental_date']))
                     {
                    $cart_product_data['buy_rent'] = 'rent';

                     }
                     else
                     {
                       $cart_product_data['buy_rent'] = 'buy'; 
                     }

                    $product_options_value = [];

                    //Check Duplicate record then bypass it - start
                    $chekCartPro1 =  CartProduct::where('merchant_id',$category->vendor_id)->where('shop_id',$category->mc_id)->where('shop_vendor_id',$category->parent_id)->where('cart_type',$input['cart_type'])->where('cart_sub_type',$input['cart_sub_type'])->where('cart_id',$cart->id)->where('product_id',$input['product_id'])->where('product_size',$input['size'])->count();
                    $chkserviceAttr2 =  CartServiceAttribute::where('cart_type',$input['cart_type'])->where('cart_id',$cart->id)->where('category_id',$product->pro_mc_id)->where('product_id',$input['product_id'])->where('attribute_id',$input['attribute_id'])->count();
                    
                    if(!empty($input['rental_date'])){
                    $rd = date("Y-m-d", strtotime($input['rental_date']));     
                    $rda = date("Y-m-d", strtotime($input['return_date']));
                    $chkserviceAttr3 =  CartProductRent::where('cart_type',$input['cart_type'])->where('cart_id',$cart->id)->where('service_id',$product->pro_mc_id)->where('product_id',$input['product_id'])->where('rental_date',$rd)->where('return_date',$rda)->count();
                    }

                    $checkinproductc4 = CartOption::where('cart_type',$input['cart_type'])->where('cart_id',$cart->id)->where('product_id',$input['product_id'])->count();

 

                $product_options_value = ProductOptionValue::select('id','product_id','price','product_option_id','option_title','option_title_ar')->where('id', '=', $input['product_option_value'])->with('getProductOption')->first();
                    
 
                    $checkinproductc5 = CartOptionValue::where('cart_type',$input['cart_type'])->where('cart_id',$cart->id)->where('product_id',$input['product_id'])->where('product_option_id',$product_options_value->product_option_id)->where('value',$product_options_value->price)->where('option_title',$product_options_value->getProductOption->option_title)->count();


                    if(!empty($input['rental_date'])){
                    if($chekCartPro1 >=1 && $chkserviceAttr3 >=1 && $chkserviceAttr2 >=1 && $checkinproductc4 >=1 && $checkinproductc5 >=1)
                    {
                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);   
                    return $response;
                    }
                    }
                    else
                    {
                    if($chekCartPro1 >=1 &&   $chkserviceAttr2 >=1 && $checkinproductc4 >=1 && $checkinproductc5 >=1)
                    {
                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);   
                    return $response;
                    }

                    }
                    //Check Duplicate record then bypass it - end

  
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                     

                    $getCartProID = $cart_product->id;
                   /* $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first(); */

                    $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first(); 

                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $input['cart_type'];
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['product_id'] = $input['product_id'];
                    $cart_attribute_data['attribute_id'] = $input['attribute_id'];
                    $cart_attribute_data['attribute_title'] = $menu->attribute_title;
                    $cart_attribute_data['attribute_title_ar'] = $menu->attribute_title_ar;
                    $cart_attribute_data['cart_product_id'] = $getCartProID;


 

                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry
                    if(!empty($input['rental_date'])){
                        $cart_product_rent = array();
                        $cart_product_rent['cart_id'] = $cart->id;
                        $cart_product_rent['cart_type'] = $input['cart_type'];
                        $cart_product_rent['service_id'] = $product->pro_mc_id;
                        $cart_product_rent['product_id'] = $input['product_id'];
                        $cart_product_rent['rental_date'] = date("Y-m-d", strtotime($input['rental_date']));          
                        $cart_product_rent['return_date'] = date("Y-m-d", strtotime($input['return_date']));
                        $cart_product_rent['rental_time'] = date("d M, Y", strtotime($input['rental_date'])).' '.$input['rental_time'];
                        $cart_product_rent['return_time'] = date("d M, Y", strtotime($input['return_date'])).' '.$input['return_time'];

  
                        $cart_product_rent['quantity'] = $input['product_qty'];
                        $cart_product_rent['insurance_amount'] = $product->Insuranceamount;
                        $cart_product_rent['cart_product_id'] = $getCartProID;

 
                        CartProductRent::create($cart_product_rent); //Product rental date and time entry  
                    }
                    if (!empty($product_options_value)) {

                        $cart_option_data = array();
                        $cart_option_data['cart_type'] = $input['cart_type'];
                        $cart_option_data['product_option_id'] = $product_options_value->product_option_id;
                        $cart_option_data['cart_id'] = $cart->id;
                        $cart_option_data['product_id'] = $input['product_id'];
                        $cart_option_data['cart_product_id'] = $getCartProID;
 

                        CartOption::create($cart_option_data); //cart option entry

                        $cart_option_value = array();
                        $cart_option_value['cart_type'] = $input['cart_type'];
                        $cart_option_value['cart_id'] = $cart->id;
                        $cart_option_value['product_id'] = $input['product_id'];
                        $cart_option_value['product_option_id'] = $product_options_value->product_option_id;
                        $cart_option_value['product_option_value_id'] = $product_options_value->id;
                        $cart_option_value['quantity'] = $input['product_qty'];
                        $cart_option_value['value'] = $product_options_value->price;
                        $cart_option_value['status'] = 1;

                       $cart_option_value['option_title'] = $product_options_value->getProductOption->option_title;
                       $cart_option_value['option_title_ar'] = $product_options_value->getProductOption->option_title_ar;
                        $cart_option_value['option_value_title'] = $product_options_value->option_title;
                        $cart_option_value['option_value_title_ar'] = $product_options_value->option_title_ar;
                        $cart_option_value['price'] = $product_options_value->price;
                        $cart_option_value['cart_product_id'] = $getCartProID;


 
                       $cart_value = CartOptionValue::create($cart_option_value); //cart option value entry
                    }                   
                    

                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);

                 } else if ($product->pro_qty <= $input['product_qty'] && $product->pro_qty > 0) {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.QUANTITY_ERROR').$product->pro_qty, 'code' => 200]);
                    $response = Response::json($response_array, 200);

                } else if ($product->pro_qty == 0) {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.PRODUCT_ERROR'), 'code' => 200]);
                    $response = Response::json($response_array, 200);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }

    /**Get Abaya info.**/

    /*
       vendor(3th lavel category in category table) become pro_mc_id in product table to get full Abaya info.
    */
   public function getAbayaInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id' => 'required', //Vendor ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['category_id'])->first();
                //GET SUB CATEGORY
                $sub_catg = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->first();

                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($input['vendor_id']);

                if (!empty($category)) {

                    if ($input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                       
                        //GET VENDOR DETAIL
                        $vendor = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();
                        //Get Saloon Menu List
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title','attribute_title as title', 'image', 'status')->where('services_id', '=', $input['subcategory_id'])->get();

                        if (isset($menu)) {

                            foreach($menu as $key => $value) {
                                //get style detail according menu
                                $dress_style = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_disprice','deliver_day','pro_qty')->where('pro_mc_id', '=', $input['vendor_id'])->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10000);
                                $value->dress_style = $dress_style;
                                foreach($dress_style as $val) {
                                    
                                    $name = explode(',', $val->option_id);
                                    //get service attribute option detail
                                    $style_Option = ProductOption::select('id', 'option_title_ar as option_title', 'button_type_id', 'service_hour')->whereIn('id', $name)->with('getBoxType:id,option_title,type')->get();
                                    //Get service attribute option value with container list
                                    $style_Option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title as option_title_ar', 'image', 'price', 'experience', 'status','value')->where('product_option_id', '!=', 37)->where('product_id', '=', $val->pro_id)->get();

                                $fabric_Option_values = ProductOptionValue::select('id', 'short_name as fabric_id','vandor_id', 'product_id', 'option_title', 'image', 'price')->where('product_id', '=', $val->pro_id)->where('product_option_id', 37)->get();
                                $arr = array();
                                foreach($fabric_Option_values as $valss)
                                {
                                $FabID = $valss->fabric_id;

                                $fabrice = DB::table('nm_fabric')->where('id', '=', $FabID)->where('status', 1)->count(); 
                                if($fabrice >=1)
                                {
                                array_push($arr, $FabID);  
                                }


                                } 

                                 $fabric_Option_value = ProductOptionValue::select('id', 'short_name as fabric_id','vandor_id', 'product_id', 'option_title_ar as option_title', 'image', 'price')->where('product_id', '=', $val->pro_id)->whereIn('short_name',  $arr)->where('product_option_id', 37)->get(); 

                                    if($value->title == 'Tailor'){
                                    $val->style_option = $style_Option;
                                  
                                    $val->fabric_Options = $fabric_Option_value;  
                                    }
                                    else
                                    {
                                      $val->style_option_value = $style_Option_value; 
                                    }
                                   
                                    if($val->deliver_day !=''){
                                    $val->deliver_day = $val->deliver_day;
                                    }
                                    else
                                    {
                                    $val->deliver_day =0;      
                                    }
                                
                                }

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'م العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                        //GET VENDOR DETAIL
                       $vendor = Category::select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                        //Get Saloon Menu List 41 42
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->where('services_id', '=', $input['subcategory_id'])->get();
 
                        if (isset($menu)) {

                            foreach($menu as $key => $value) {
                                //get style detail according menu
                                $dress_style = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_disprice','deliver_day','pro_qty')->where('pro_mc_id', '=', $input['vendor_id'])->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10000);

                                //print_r($dress_style); die;
                                $value->dress_style = $dress_style;

                                foreach($dress_style as $val) {
                                    
                                      $name = explode(',', $val->option_id);
                                    //get service attribute option detail
                                    $style_Option = ProductOption::select('id', 'option_title', 'button_type_id', 'service_hour')->whereIn('id', $name)->with('getBoxType:id,option_title,type')->get();
                                    //Get service attribute option value with container list
                                    $style_Option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title', 'image', 'price', 'experience', 'status','value')->where('product_option_id', '!=', 37)->where('product_id', '=', $val->pro_id)->get();

                                    $fabric_Option_values = ProductOptionValue::select('id', 'short_name as fabric_id','vandor_id', 'product_id', 'option_title', 'image', 'price')->where('product_id', '=', $val->pro_id)->where('product_option_id', 37)->get();
                                    $arr = array();
                                    foreach($fabric_Option_values as $valss)
                                    {
                                    $FabID = $valss->fabric_id;

                                    $fabrice = DB::table('nm_fabric')->where('id', '=', $FabID)->where('status', 1)->count(); 
                                    if($fabrice >=1)
                                    {
                                    array_push($arr, $FabID);  
                                    }


                                    } 

               $fabric_Option_value = ProductOptionValue::select('id', 'short_name as fabric_id','vandor_id', 'product_id', 'option_title', 'image', 'price')->whereIn('short_name',  $arr)->where('product_id', '=', $val->pro_id)->where('product_option_id', 37)->get(); 

                                    if($value->attribute_title == 'Tailor'){
                                    $val->style_option = $style_Option;                                
                                    $val->fabric_Options = $fabric_Option_value;  
                                    }
                                    else
                                    {
                                      $val->style_option_value = $style_Option_value; 
                                    }


                                    if($val->deliver_day !=''){
                                    $val->deliver_day = $val->deliver_day;

                                    }
                                    else
                                    {
                                    $val->deliver_day =0;      
                                    } 
                                }

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Set Abaya data in add to cart
    public function setAbayaAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required', //Get Product ID
            'cart_type' => 'required', // Get Cart Type
            'cart_sub_type' => 'required', //Get Product ID
            'attribute_id' => 'required', //Get Attribute ID
            'total_price' => 'required',
        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

                $User = Auth::user();
                $product = Products::where('pro_id', '=', $input['product_id'])->first();

                if (!empty($product)) {
                    $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                    if (!empty($cart)) {
                        // $cart_pro = CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        // $cart_body_measurement = CartBodyMeasurement::where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        // $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', $input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();


                         $cart_pro = CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->where('product_size', '=', $input['size'])->count();

                           if($cart_pro!='') {
                           $cartquantity = CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->where('product_size', '=', $input['size'])->first();

                           $quantity1 = $cartquantity->quantity;
                           $totalpricepro = $cartquantity->total_price;
                          // $proprice =  $cartquantity->price;


                           
                            if(isset($input['quantity']) && $input['quantity']!='') {
                            $inputquantity = $input['quantity'];
                            }

                            if(isset($input['product_quantity']) && $input['product_quantity']!='') {
                            $inputquantity = $input['product_quantity'];
                            }

                           //$inputquantity = $input['quantity'];

                           $totalquantity =  $quantity1+$inputquantity;

                           $totalprice = $input['total_price'];

                           $totalproprice =($totalprice)+($totalpricepro); 



                         
                         CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->where('product_size', '=', $input['size'])
                          ->update([
                            'quantity' => $totalquantity,
                            'total_price' =>  $totalproprice
                            ]);
                      }
                       $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                  
                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                    
                    if($cart_pro==0) {
                   
                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $input['product_id'];
                    $cart_product_data['total_price'] = $input['total_price'];
                    if(isset($input['size']) && $input['size']!='')
                    {
                       $cart_product_data['product_size'] = $input['size']; 
                    }
                    $cart_product_data['status'] = 1;

                    $cart_product_data['shop_vendor_id'] = $category->parent_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['pro_price'] = $product->pro_price;



                    

                    if(isset($input['quantity']) && $input['quantity']!='') {
                    $cart_product_data['quantity'] = $input['quantity'];
                    } else {
                    $cart_product_data['quantity'] = '1';
                    }

                    if(isset($input['product_quantity']) && $input['product_quantity']!='') {
                    $cart_product_data['quantity'] = $input['product_quantity'];
                    }

 
                    if(isset($input['fabric_name']) && $input['fabric_name']!='')
                    {
                       $cart_product_data['fabric_name'] = $input['fabric_name']; 
                    }
                    

                    //Check Duplicate record then bypass it - start
                    $chekCartPro =  CartProduct::where('merchant_id',$category->vendor_id)->where('shop_id',$category->mc_id)->where('shop_vendor_id',$category->parent_id)->where('cart_type',$input['cart_type'])->where('cart_sub_type',$input['cart_sub_type'])->where('cart_id',$cart->id)->where('product_id',$input['product_id'])->where('product_size',$input['size'])->count();
                    $chkserviceAttr =  CartServiceAttribute::where('cart_type',$input['cart_type'])->where('cart_id',$cart->id)->where('category_id',$product->pro_mc_id)->where('product_id',$input['product_id'])->where('attribute_id',$input['attribute_id'])->count();
                    $checkinproductc = CartBodyMeasurement::where('cart_id',$cart->id)->where('customer_id',$User->cus_id)->where('product_id',$input['product_id'])->where('length',$input['length'])->where('chest_size',$input['chest_size'])->where('waistsize',$input['waistsize'])->where('soulders',$input['soulders'])->where('neck',$input['neck'])->where('arm_length',$input['arm_length'])->where('wrist_diameter',$input['wrist_diameter'])->where('cutting',$input['cutting'])->count();
                    if($chekCartPro >=1 && $chkserviceAttr >=1 && $checkinproductc >=1)
                    {
                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);   
                    return $response;
                    }
                    //Check Duplicate record then bypass it - end

 

                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                       
                 $getCartProID = $cart_product->id;
                    $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first();
                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $input['cart_type'];
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['product_id'] = $input['product_id'];
                    $cart_attribute_data['attribute_id'] = $input['attribute_id'];
                    $cart_attribute_data['attribute_title'] = $menu->attribute_title;
                    $cart_attribute_data['attribute_title_ar'] = $menu->attribute_title_ar;
 
                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry
                  
                    $cart_body_measurement = array();
                    if(!empty($input['length'])){

                        $cart_body_measurement['cart_id'] = $cart->id;
                        $cart_body_measurement['customer_id'] = $User->cus_id;
                        $cart_body_measurement['product_id'] = $input['product_id'];
                        $cart_body_measurement['length'] = $input['length'];                    
                        $cart_body_measurement['chest_size'] = $input['chest_size'];                  
                        
                        $cart_body_measurement['waistsize'] = $input['waistsize'];                  
                        $cart_body_measurement['soulders'] = $input['soulders'];
                        $cart_body_measurement['neck'] = $input['neck'];
                        $cart_body_measurement['arm_length'] = $input['arm_length'];                    
                        $cart_body_measurement['wrist_diameter'] = $input['wrist_diameter'];    
                        $cart_body_measurement['cutting'] = $input['cutting']; 
                        $cart_body_measurement['status'] = 1;  
                        $cart_body_measurement['cart_product_id'] = $getCartProID;                  
                        CartBodyMeasurement::create($cart_body_measurement); //Product rental date and time entry 
                    }
                    


                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                  }  
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.CART_ERROR'), 'code' => 500]);
                    $response = Response::json($response_array, 500);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }

    /**Get Oud & Perfumes info.**/

    /*
       vendor(3th lavel category in category table) become pro_mc_id in product table to get full Oud & Perfumes info.
    */
   public function getOudAndPerfumesInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id' => 'required', //Vendor ID
            'branch_id' => 'required', //Branch ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['category_id'])->first();
                //GET SUB CATEGORY
                $sub_catg = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->first();
                $vendor = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->first();

 

                //GET CLIENT REVIEW
               

                if (!empty($category)) {

                    if ($input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                       
                        //GET BRANCH DETAIL
                       $branch = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['branch_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();

                        $mc_ids = $branch->mc_id; 
                        $client_review = Category::get_category_review($mc_ids);
                        //Get Saloon Menu List
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'image', 'status')->where('status', 1)->where('services_id', '=', $input['branch_id'])->get();
                        if (isset($menu)) {

                            foreach($menu as $key => $value) {
                                //get style detail according menu
                                $dress_style = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'about_ar as pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_disprice','pro_qty')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                                $value->dress_style = $dress_style;
                               

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'م العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name_ar, 'branch_detail' => $branch, 'client_comments' => $client_review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                        //GET BRANCH DETAIL
                       $branch = Category::select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['branch_id'])->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                       
                        $mc_ids = $branch->mc_id; 
                        $client_review = Category::get_category_review($mc_ids);
                        //Get Oup & Perfume Menu List
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->where('services_id', '=', $input['branch_id'])->where('status', 1)->get();
                        if (isset($menu)) { 

                            foreach($menu as $key => $value) {
                                //get style detail according menu
                                $dress_style = Products::select('pro_id', 'pro_title', 'pro_Img', 'about as pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_disprice','pro_qty')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                                $value->dress_style = $dress_style;
                               

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name, 'branch_detail' => $branch, 'client_comments' => $client_review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Set Oud And Perfumes data in add to cart
    public function setOudAndPerfumesAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required', //Get Product ID
            'cart_type' => 'required', // Get Cart Type
            'cart_sub_type' => 'required', //Get Product ID
            'attribute_id' => 'required', //Get Product ID
            'total_price' => 'required',
            'product_qty' => 'required',

        ]);
        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);
        $input = $request->all();

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

                $User = Auth::user();
                $product = Products::where('pro_id', '=', $input['product_id'])->first();
                 $proqntity =  $product->pro_qty;

                 if($proqntity<$input['product_qty'] && $proqntity > 0) {
                     
                      $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.QUANTITY_ERROR').$proqntity, 'code' => 200]);
                    $response = Response::json($response_array, 200);
                    return $response;
                 }

                if (!empty($product)) {
                    $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                    if (!empty($cart)) {
                       //  $cart_pro = CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                       // $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', $input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                      $cart_pro = CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->count();

                           if($cart_pro!='') {
                           $cartquantity = CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->first();

                           $quantity1 = $cartquantity->quantity;
                           $totalpricepro = $cartquantity->total_price;
                          // $proprice =  $cartquantity->price;

                           $inputquantity = $input['product_qty'];

                           $totalquantity =  $quantity1+$inputquantity;

                           $totalprice = $input['total_price'];

                           $totalproprice =($totalprice)+($totalpricepro); 



                         
                         CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])
                          ->update([
                            'quantity' => $totalquantity,
                            'total_price' =>  $totalproprice

                            ]);
                      }
                       $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                     $response = Response::json($response_array, 200);    

                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                    if($cart_pro==0) {
                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $input['product_id'];
                    $cart_product_data['total_price'] = $input['total_price'];
                    $cart_product_data['status'] = 1;
                    $cart_product_data['shop_vendor_id'] = $category->parent_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['pro_price'] = $product->pro_price;
                     $cart_product_data['quantity'] = $input['product_qty'];
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                    $getCartProID = $cart_product->id;
                    $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first();
                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $input['cart_type'];
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['product_id'] = $input['product_id'];
                    $cart_attribute_data['attribute_id'] = $input['attribute_id'];
                    $cart_attribute_data['attribute_title'] = $menu->attribute_title;
                    $cart_attribute_data['attribute_title_ar'] = $menu->attribute_title_ar;
                    $cart_attribute_data['cart_product_id'] = $getCartProID; 

                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry
                    
                    
                    

                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                 }
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.CART_ERROR'), 'code' => 500]);
                    $response = Response::json($response_array, 500);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }

    /**Get Gold & Jewelry info.**/

    /*
       vendor(3th lavel category in category table) become pro_mc_id in product table to get full Gold & Jewelry info.
    */
   public function getGoldAndJewelryInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id' => 'required', //Vendor ID
            'branch_id' => 'required', //Branch ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['category_id'])->first();
                //GET SUB CATEGORY
                $sub_catg = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->first();
                $vendor = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->first();

                   $client_review = Category::get_category_review($input['branch_id']);

                if (!empty($category)) {

                    if ($input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                       
                        //GET BRANCH DETAIL
                       $branch = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['branch_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();
                       //GET CLIENT REVIEW
                            $dress_stylesheet = Products::select('attribute_id')->where('pro_mc_id', '=', $input['branch_id'])->where('pro_status', '=', 1)->get();
                            $menuCat = array();
                            foreach ($dress_stylesheet as $value) {
                            $attribute_id = $value->attribute_id;
                            array_push($menuCat, $attribute_id);
                            }

             
                        //Get Saloon Menu List status
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'image', 'status','write_on_jewellery')->where('services_id', '=', $input['branch_id'])->where('status', 1)->whereIn('id',$menuCat)->get();
                        if (isset($menu)) {

                            foreach($menu as $key => $value) {
                                //get style detail according menu
                                $dress_style = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_disprice','pro_qty')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                               $value->gold_style = $dress_style;
                                foreach($dress_style as $value) {
                                $product_attr = ProductAttribute::select('id','product_id','vendor_id','attribute_title_ar as attribute_title','value')->where('product_id', '=', $value->pro_id)->get(); //GET PRODUCT ATTRIBUTE
                               $value->product_attribute =  $product_attr;

                            }

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'م العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name_ar, 'branch_detail' => $branch, 'client_comments' => $client_review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                        //GET BRANCH DETAIL
                       $branch = Category::select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['branch_id'])->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                        //Get Oup & Perfume Menu List


                        $dress_stylesheet = Products::select('attribute_id')->where('pro_mc_id', '=', $input['branch_id'])->where('pro_status', '=', 1)->get();
                        $menuCat = array();
                        foreach ($dress_stylesheet as $value) {
                         $attribute_id = $value->attribute_id;
                         array_push($menuCat, $attribute_id);
                        }

 
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status','write_on_jewellery')->where('services_id', '=', $input['branch_id'])->where('status', 1)->whereIn('id',$menuCat)->get();
                        if (isset($menu)) {

                            foreach($menu as $key => $value) {
                                //get style detail according menu
                                $dress_style = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_disprice','pro_qty')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                                $value->gold_style = $dress_style;
                                foreach($dress_style as $value) {
                                $product_attr = ProductAttribute::select('id','product_id','vendor_id','attribute_title','value')->where('product_id', '=', $value->pro_id)->get(); //GET PRODUCT ATTRIBUTE
                               $value->product_attribute =  $product_attr;

                            }

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name, 'branch_detail' => $branch, 'client_comments' => $client_review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

     //Set Gold And Jewelry data in add to cart
    public function setGoldAndJewelryAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required', //Get Product ID
            'cart_type' => 'required', // Get Cart Type
            'cart_sub_type' => 'required', //Get Product ID
            'attribute_id' => 'required', //Get Product ID
            'total_price' => 'required',

        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

                $User = Auth::user();
                $product = Products::where('pro_id', '=', $input['product_id'])->first();

                if (!empty($product)) {
                    $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                     $cartC = Cart::where('user_id', '=', $User->cus_id)->count();

                    
                  if ($cartC >=1) {
                        $cart = Cart::where('user_id', '=', $User->cus_id)->first();

                         $cart_pro = CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->count();
                           
                           if($cart_pro!='') {
                           $cartquantity = CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->first();

                           $quantity1 = $cartquantity->quantity;
                           $totalpricepro = $cartquantity->total_price;
                          // $proprice =  $cartquantity->price;

                           $inputquantity = $input['product_quantity'];

                           $totalquantity =  $quantity1+$inputquantity;

                           $totalprice = $input['total_price'];

                           $totalproprice =($totalprice)+($totalpricepro); 



                         
                         CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])
                          ->update([
                            'quantity' => $totalquantity,
                            'total_price' =>  $totalproprice

                            ]);
                      }

                    /*$cart_service_attribute = CartServiceAttribute::where('cart_type', '=', $input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();*/


                     $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);


                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                         $cart_pro = CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->count();
                    }
                     if($cart_pro < 1) {
                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $input['product_id'];
                    $cart_product_data['total_price'] = $input['total_price'];
                    if($input['write_on_ring']){
                      $cart_product_data['product_size'] = $input['write_on_ring'];  
                    }
                    if($input['product_quantity']){
                      $cart_product_data['quantity'] = $input['product_quantity'];  
                    }       







                    $cart_product_data['status'] = 1;
                    $cart_product_data['shop_vendor_id'] = $category->parent_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['pro_price'] = $product->pro_price;
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                    $getCartProID = $cart_product->id;
                    $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first();
                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $input['cart_type'];
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['product_id'] = $input['product_id'];
                    $cart_attribute_data['attribute_id'] = $input['attribute_id'];
                    $cart_attribute_data['attribute_title'] = $menu->attribute_title;
                    $cart_attribute_data['attribute_title_ar'] = $menu->attribute_title_ar;
                    $cart_attribute_data['cart_product_id'] = $getCartProID; 

                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry

                    $product_attr = ProductAttribute::select('id', 'product_id', 'attribute_title_ar','attribute_title', 'value')->where('product_id', '=', $input['product_id'])->get();
                    if(!empty($product_attr)){
                        foreach ($product_attr as $key => $value) {
                    
                    $cart_product_attr = array();
                    $cart_product_attr['cart_id'] = $cart->id;
                    $cart_product_attr['cart_type'] = $input['cart_type'];
                    $cart_product_attr['product_id'] = $input['product_id'];
                    $cart_product_attr['attribute_title'] = $value->attribute_title;
                    $cart_product_attr['attribute_title_ar'] = $value->attribute_title_ar;
                    $cart_product_attr['value'] = $value->value;
                    $cart_product_attr['cart_product_id'] = $getCartProID; 
                    CartProductAttribute::create($cart_product_attr); //Product Attribute entry
                        }
                    }
                    
                    
                    

                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                }
                }  else {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.CART_ERROR'), 'code' => 500]);
                    $response = Response::json($response_array, 500);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }

    /**Get Shopping data from add to cart**/
    public function getShoppingAddToCartData(Request $request) {  
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
        ]);
        $input = $request->all();

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();


                $shopping_pro = array(); 
                $cart_pro = CartProduct::where('cart_type', '=', 'shopping')->where('id', '=', $input['id'])->where('product_id', '=', $input['product_id'])->where('cart_id', '=', $cart->id)->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,pro_disprice')->first();

                $getfabc = DB::table('nm_fabric')->where('id',$cart_pro->fabric_name)->count();
                if($getfabc >=1)
                {
                $getfab = DB::table('nm_fabric')->where('id',$cart_pro->fabric_name)->first();
                if (isset($request->language_type) && $request->language_type == 'ar') {
                $shopping_pro['fabric_name'] =$getfab->fabric_name_ar;
                }
                else
                {
                $shopping_pro['fabric_name'] =$getfab->fabric_name;
                }
                $shopping_pro['fabric_image'] =$getfab->image;
                $shopping_pro['fabric_price'] =$getfab->price;
                }
                else
                {
                $shopping_pro['fabric_name'] ='';           
                $shopping_pro['fabric_image'] ='';
                $shopping_pro['fabric_price'] ='';
                }
             


                $category = Category::select('mc_id','mc_img', 'mc_name', 'mc_name_ar','address','address_ar', 'mc_status','longitude','latitude')->where('mc_id', '=', $cart_pro->getProduct[0]->pro_mc_id)->first();

                //Product Attributes
                 $quantityattr = '';

                if (!empty($cart) && !empty($cart_pro)) {
                    //get  product 
                   $size =  $cart_pro->product_size;
                 $quantityattr = ProductOptionValue::select('value')->where('product_id','=',$input['product_id'])->where('option_title',$size)->first();

                            $attribute = CartServiceAttribute::where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('cart_product_id', '=',$input['id'])->where('product_id', '=',$cart_pro->product_id)->with('getProductServiceAttribute:id,services_id,attribute_title,attribute_title_ar')->first();

                            $shopping_pro['pro_id'] = $cart_pro->getProduct[0]->pro_id;
                            if ($input['language_type'] == 'en') {
                                if(!empty($category)){
                                   $shopping_pro['category_name'] = $category->mc_name;
                                   $shopping_pro['category_img'] = $category->mc_img;
                                   $shopping_pro['address'] = $category->address; 
                                   $shopping_pro['longitude'] = $category->longitude;
                                   $shopping_pro['latitude'] = $category->latitude;
                                   $shopping_pro['write_on_rings'] = $cart_pro->product_size;
                                   $shopping_pro['pro_qty'] = $cart_pro->quantity;
                                   if(isset($quantityattr->value) && $quantityattr->value!='' ) {
                                   $shopping_pro['quantity'] = $quantityattr->value;
                                    } else {

                                   $shopping_pro['quantity'] = $cart_pro->getProduct[0]->pro_qty;
                                    }
                                   
                                    
                                }
                                if(!empty($attribute)){
                                  $shopping_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title;  
                                }                                
                                $shopping_pro['pro_title'] = $cart_pro->getProduct[0]->pro_title;
                                $shopping_pro['pro_desc'] = $cart_pro->getProduct[0]->pro_desc;
                                $product_attr = CartProductAttribute::select('id', 'product_id', 'attribute_title', 'value')->where('cart_product_id', '=',$input['id'])->where('product_id', '=', $input['product_id'])->orderBy('attribute_title', 'ASC')->get();
                                   if(!empty($product_attr)){
                                    $shopping_pro['product_attr'] = $product_attr; 
                                   }
                            } else {
                                if(!empty($category)){
                                   $shopping_pro['category_name'] = $category->mc_name_ar;
                                   $shopping_pro['category_img'] =  $category->mc_img;
                                   $shopping_pro['address'] =       $category->address_ar;
                                   $shopping_pro['longitude'] =     $category->longitude;
                                   $shopping_pro['latitude'] =      $category->latitude; 
                                   $shopping_pro['write_on_rings'] = $cart_pro->product_size; 
                                   $shopping_pro['pro_qty'] =        $cart_pro->quantity;
                                  if(isset($quantityattr->value) && $quantityattr->value!='' ) {
                                   $shopping_pro['quantity'] = $quantityattr->value;
                                    }else {
                                   $shopping_pro['quantity'] = $cart_pro->getProduct[0]->pro_qty;
                                   
                                    } 
                                }
                                if(!empty($attribute)){
                                  $shopping_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title_ar;  
                                }
                                
                                $shopping_pro['pro_title'] = $cart_pro->getProduct[0]->pro_title_ar;
                                $shopping_pro['pro_desc'] = $cart_pro->getProduct[0]->pro_desc_ar;
                               $product_attr = CartProductAttribute::select('id', 'product_id', 'attribute_title_ar', 'value')->where('cart_product_id', '=',$input['id'])->where('product_id', '=', $input['product_id'])->orderBy('attribute_title', 'ASC')->get();
                                if(!empty($product_attr)){
                                    $shopping_pro['product_attr'] = $product_attr; 
                                   }
                            }
                            $shopping_pro['size'] = $cart_pro->product_size;
                            //$shopping_pro['pro_qty'] = $cart_pro->quantity;

                            if($cart_pro->getProduct[0]->pro_disprice=='' || $cart_pro->getProduct[0]->pro_disprice==0) {
                                 $shopping_pro['pro_price'] = $cart_pro->getProduct[0]->pro_price;

                               } else {

                                  $shopping_pro['pro_price'] = $cart_pro->getProduct[0]->pro_disprice;
                               }

                           // $shopping_pro['pro_price'] = $cart_pro->getProduct[0]->pro_price;
                            $shopping_pro['pro_Img'] = $cart_pro->getProduct[0]->pro_Img;
                            $shopping_pro['pro_status'] = $cart_pro->getProduct[0]->pro_status;
                            $shopping_pro['total_price'] = $cart_pro->total_price;
                            $rental_pro = CartProductRent::where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('cart_product_id', '=',$input['id'])->where('product_id', '=',$cart_pro->product_id)->first(); //get rental data
                          if (!empty($rental_pro)) {

                            $shopping_pro['rental_date'] = date("d-m-Y", strtotime($rental_pro->rental_date));
                            $shopping_pro['return_date'] = date("d-m-Y", strtotime($rental_pro->return_date));
                            $shopping_pro['insurance_amount'] = $rental_pro->insurance_amount;

                          }

                          


                          $measurement = CartBodyMeasurement::where('cart_id', '=', $cart->id)->where('product_id', '=',$cart_pro->product_id)->where('cart_product_id', '=',$input['id'])->where('cart_product_id', '=',$cart_pro->id)->first(); // get measurement data
                          if (!empty($measurement)) {
                            $shopping_pro['length']     = $measurement->length;
                            $shopping_pro['chest_size'] = $measurement->chest_size;
                            $shopping_pro['waistsize']  = $measurement->waistsize;
                            $shopping_pro['soulders']   = $measurement->soulders;
                            $shopping_pro['neck']       = $measurement->neck;
                            $shopping_pro['arm_length'] = $measurement->arm_length;
                            $shopping_pro['wrist_diameter'] = $measurement->wrist_diameter;
                            $shopping_pro['cutting'] = $measurement->cutting;
                          }


                   
                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'cart_id' => $cart->id,'shopping_data' => $shopping_pro,);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'cart data not found & cart type must be Shopping', 'code' => 500]);
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    /**Get to add to cart  data**/
    public function getDressAddToCartData(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'token' => 'required',
            'language_type' => 'required',
        ]);
        $input = $request->all();

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                

                $dress_data = array();


                if(isset($input['id']) && $input['id']!='')
                {
                $cart_pro = CartProduct::where('id', '=',$input['id'])->where('cart_type', '=','shopping')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_disprice,pro_Img,attribute_id,pro_status,pro_mc_id,Insuranceamount')->first();

                }
                else
                {
                $cart_pro = CartProduct::where('cart_type', '=','shopping')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_disprice,pro_Img,attribute_id,pro_status,pro_mc_id,Insuranceamount')->first();
                }
 

                $ProType = $cart_pro->buy_rent;



                if (!empty($cart) && !empty($cart_pro)) {

                    $attribute = CartServiceAttribute::where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('product_id', '=',$cart_pro->product_id)->with('getProductServiceAttribute:id,services_id,attribute_title,attribute_title_ar')->first();



                    $cart_option_value = CartOptionValue::where('cart_id', '=', $cart->id)->where('cart_type', '=', 'shopping')->where('product_id', '=', $input['product_id'])->with('getProductOption:id,services_id,option_title,option_title_ar,status')->with('getProductOptionValue:id,product_id,product_option_id,option_title,option_title_ar,price,status')->first();



                    $rental_productsC = CartProductRent::where('cart_id', '=', $cart->id)->where('cart_type', '=', 'shopping')->where('cart_product_id', '=', $input['id'])->where('product_id', '=', $input['product_id'])->count();

                    

                    $category = Category::select('mc_id','mc_img', 'mc_name', 'mc_name_ar','address','address_ar', 'mc_status')->where('mc_id', '=', $cart_pro->getProduct[0]->pro_mc_id)->first();

                    $quantityattr = '';
                    $pro_disprice = '';
                     $productid = $cart_option_value->product_id;
                   // $len =  strlen($input['rental_date']);



                   if(isset($input['rental_date']) && $input['rental_date']!="") {
                       
                       
                    $rentalpricedata = ProductOptionValue::where('option_title','Rent')->where('product_id',$productid)->first();
                    $price = $rentalpricedata->price;
                    $rentalpricedatacc = ProductOptionValue::where('option_title','Discount for rent')->where('product_id',$productid)->count();
                    if($rentalpricedatacc >=1)
                    {
                    $rentalpricedataa = ProductOptionValue::where('option_title','Discount for rent')->where('product_id',$productid)->first();
                    $pro_disprice = $price- ($price * $rentalpricedataa->discount)/100;
                    }
                    else
                    {

                    $pro_disprice = 0;
                    } 
                   // $pro_disprice = 0;

                         } else {
                      $rentalpricedata = Products::where('pro_id',$productid)->first();
                       $price = $rentalpricedata->pro_price;
                      
                       $pro_disprice = $rentalpricedata->pro_disprice;

                         } 

                // echo $quantityattr->value;die;
             
                    if (isset($cart_pro)) { //get product data
                
                $size =  $cart_pro->product_size;

                $quantityattr = ProductOptionValue::select('value','value_ar','option_title')->where('product_id','=',$input['product_id'])->where('option_title',$size)->first();


                        $dress_data['pro_id'] = $cart_pro->getProduct[0]->pro_id;
                        if ($input['language_type'] == 'en') {
                            if(!empty($category)){
                               $dress_data['category_name'] = $category->mc_name;
                               $dress_data['category_img'] = $category->mc_img;
                               $dress_data['address'] = $category->address;

                            }
                             
                          
                            if(isset($quantityattr->value) && $quantityattr->value!='' ) {
                                   $dress_data['quantity'] = $quantityattr->value;

                                   if($quantityattr->value_ar!=''){
                                    $dress_data['quantity_rent'] = $quantityattr->value_ar;
                                   }
                                   else
                                   {
                                    $dress_data['quantity_rent'] =0;
                                   }
                                    

                                    }

                             if(isset($attribute->getProductServiceAttribute[0]->attribute_title)){
                            $dress_data['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title;
                         } else {
                             $dress_data['attribute_title'] = '';
                         } 
                             if(isset($cart_option_value->getProductOptionValue[0]->option_title)){
                            $dress_data['option_title'] = $cart_option_value->getProductOptionValue[0]->option_title;
                             } else {
                                 $dress_data['option_title'] = '';

                             }
                            if(isset($cart_pro->getProduct[0]->pro_title)){
                            $dress_data['pro_title'] = $cart_pro->getProduct[0]->pro_title;
                           } else {
                              $dress_data['pro_title'] = '';
                           } 
                             if(isset($cart_pro->getProduct[0]->pro_desc)){
                             $dress_data['pro_desc'] = $cart_pro->getProduct[0]->pro_desc;
                           } else {
                                $dress_data['pro_desc'] = '';
                        }

                        } else {
                            if(!empty($category)){
                               $dress_data['category_name'] = $category->mc_name_ar;
                               $dress_data['category_img'] = $category->mc_img;
                               $dress_data['address'] = $category->address_ar; 
                            }
                          
                            if(isset($quantityattr->value) && $quantityattr->value!='' ) {
                                   $dress_data['quantity'] = $quantityattr->value;
                                    }

                            
                            $dress_data['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title_ar;
                             if(isset($cart_option_value->getProductOptionValue[0]->option_title_ar)) {
                            $dress_data['option_title'] = $cart_option_value->getProductOptionValue[0]->option_title_ar;
                         } else {
                                $dress_data['option_title'] ='';

                         } 
                            $dress_data['pro_title'] = $cart_pro->getProduct[0]->pro_title_ar;
                            $dress_data['pro_desc'] = $cart_pro->getProduct[0]->pro_desc_ar;
                        }
               
                        $dress_data['product_size'] = $cart_pro->product_size;
                        $dress_data['pro_qty'] = $cart_pro->quantity;
                        $dress_data['pro_Img'] = $cart_pro->getProduct[0]->pro_Img;
                        $dress_data['pro_status'] = $cart_pro->getProduct[0]->pro_status;
                        if(isset($cart_option_value->product_option_value_id)) {

                            $dress_data['Insuranceamount'] = $cart_pro->getProduct[0]->Insuranceamount;
                           if(isset($cart_option_value->getProductOptionValue[0]->option_title_ar)) {

                            //$dress_data['pro_price'] = $cart_option_value->getProductOptionValue[0]->price;

                            $dress_data['pro_price'] = $price;
                        } else {
                              $dress_data['pro_price'] ='';

                         }

                            $dress_data['total_price'] = $cart_pro->total_price;
                            /*$dress_data['pro_price'] = $cart_pro->getProduct[0]->pro_price;
                            $dress_data['pro_disprice'] = $cart_pro->getProduct[0]->pro_disprice;*/
                        } else {

                            $dress_data['pro_price'] = $cart_pro->getProduct[0]->pro_price;

                            $dress_data['total_price'] = $cart_pro->total_price;
                        }
                    }
                    $dress_data['pro_disprice'] = $pro_disprice;


                    if ($rental_productsC >=1) { //get rental product


                $rental_products = CartProductRent::where('cart_id', '=', $cart->id)->where('cart_type', '=', 'shopping')->where('cart_product_id', '=', $input['id'])->where('product_id', '=', $input['product_id'])->first();

                        $dress_data['rental_date'] = $rental_products->rental_date;;
                        $dress_data['rental_time'] = $rental_products->rental_time;
                        $dress_data['return_date'] =  $rental_products->return_date;;
                        $dress_data['return_time'] = $rental_products->return_time;
 
                       

                        $date1=date_create(substr($rental_products->rental_time,-9));
                        $date2=date_create(substr($rental_products->return_time,-9));
                        //$diff=date_diff($date1,$date2);
                       // $diff->format("%H:%I");
                        //$dress_data['total_time'] = $diff->format("%H").' hours';
                        $dress_data['total_time'] =' hours';
                    }
 $dress_data['pro_price'] = $cart_pro->pro_price;
                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'cart_id' => $cart->id, 'dress_data' => $dress_data);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'cart data not found & cart type must be dress', 'code' => 500]);
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    /**delete to add to cart  data**/
    public function deleteShoppingData(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'token' => 'required'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();

                $cart = Cart::where('user_id', '=', $User->cus_id)->first();


                $cart_pro = CartProduct::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->first();
                if (!empty($cart) && !empty($cart_pro)) {

                if(isset($input['id']) && $input['id']!='')
                {

                $cart_rental_pro = CartProductRent::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('cart_product_id', '=', $input['id'])->delete();

                $cart_pro = CartProduct::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('id', '=', $input['id'])->delete();
                }
                else
                {
                $cart_rental_pro = CartProductRent::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->delete();
                //delete car rent services
                $cart_body_measurement = CartBodyMeasurement::where('product_id', '=', $input['product_id'])->where('cart_id', '=', $cart->id)->delete();
                //delete body measurement data

                $cart_attribute_data= CartServiceAttribute::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->delete();
                //delete attribute data

                $cart_pro_opt = CartOption::where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                //delete cart option

                $cart_pro_val = CartOptionValue::where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                //delete cart option value


                $cart_pro = CartProduct::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->delete();
                //delete product

                }


                    

                    $response_array = array('data' => ['success' => true, 'message' => 'Successfully deleted your cart data', 'code' => 200]);

                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }


    //Shipping charges according product id Multiple insert

     
    public function AddShippingCharge(Request $request) {
        $validator = Validator::make($request->all(), [
            /*'product_id' => 'required',
            'token' => 'required',
            'shipping_charge'=>'required'*/
        ]);
        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
              

                        $User = Auth::user();
                        $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                        if(isset($input['product_id'][0]) && $input['product_id'][0]=='') {
                       $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.SUCCESS'), 'code' => 200]);
                        return $response_array;
                        
                        }

                         if(trim($input['product_id'][0])=='' && trim($input['shipping_charge'][0])=='' && trim($input['shipping_id'][0])=='') {

                         $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.SUCCESS'), 'code' => 200]);

                          } else {
                         $productid = explode(',',$input['product_id']);

                         $shippingcharge = explode(',',$input['shipping_charge']);

                         $shippingid = explode(',',$input['shipping_id']);

                      foreach($productid as $key =>$val) {

                        $saverecord = new OrderProductShipping;

                        $shippingids =     $shippingid[$key];
                        $prodid =          $productid[$key]; 

                         $shippingcharges =  $shippingcharge[$key];    
                        $saverecord ->product_id = $prodid; 
                        $saverecord ->shipping_charge = $shippingcharges; 
                        $saverecord ->shipping_id = $shippingids;
                        $saverecord ->cus_id = $cart->user_id;
                        $cart_pro =  $saverecord->save();
                } 


                 if($cart_pro) { 
                     
                $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.SUCCESS'), 'code' => 200]);
                 } else {

                      $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.FAIL'), 'code' => 203]);


                 } }


                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.FAIL'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }
            
        }
        return $response;
    }

    
 public function checkDressrent(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'bookingdate_from' => 'required',
            'product_size' => 'required',
            'bookingdate_to' => 'required',
            'token' => 'required',
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

            $product_id = $request->product_id;
            $product_size = $request->product_size;
            $bookingf = $request->bookingdate_from;
            $bookingt = $request->bookingdate_to;
            //From front end
              $CarBookingdate_from =date("Y-m-d H:i A", strtotime($bookingf));
             $CarBookingdate_to = date("Y-m-d H:i A", strtotime($bookingt)); 



            $getcarQtya = DB::table('nm_product_option_value')->select('value_ar')->where('option_title',$product_size)->where('product_id',$product_id)->first();



            $carTotalQty = $getcarQtya->value_ar;
 
              $Cbdate_from = strtotime($bookingf); 
             $Cbdate_to = strtotime($bookingt);
 
            $CarDatebook = array();
            for ($i=$Cbdate_from; $i<=$Cbdate_to; $i+=3600) {  
            array_push($CarDatebook, date("Y-m-d H:i A", $i));  
            } 
 


            //From front end 
            $getInfocount = DB::table('nm_order_product_rent')->where('product_id',$product_id)->where('product_type','shopping')->count();

            $alreadyBook = array();
            if($getInfocount >=1)
            {
            $getInfo = DB::table('nm_order_product_rent')->where('product_id',$product_id)->where('product_type','shopping')->get();
            $datetoarray = array();
            $datefromarray = array();
            $k=1;
            foreach($getInfo as $getinf)
            {

            $dateChk = date("Y-m-d H:i A", strtotime($getinf->rental_time)); 
            $dateChka = date("Y-m-d H:i A", strtotime($getinf->return_time)); 
            array_push($datetoarray, $dateChka);
            array_push($datefromarray, $dateChk);


            }

 


            $dateFix = array();
            for($j=0; $j<count($datefromarray);$j++)
            {
             $Fromdate = strtotime($datefromarray[$j]); 
             $Todate = strtotime($datetoarray[$j]); 
            for ($M=$Fromdate; $M<=$Todate; $M+=3600) {  
            array_push($dateFix, date("Y-m-d H:i A", $M));  
            } 
            } 

            $getArry = array_count_values($dateFix); 
 
 

            foreach($CarDatebook as $bookd)
            {
            foreach($getArry as $key=>$val)
            {

 

            if($key==$bookd)
            {
            array_push($alreadyBook, $val);
            }
            }
            }
            } 

  

            if(count($alreadyBook) >=1){
            $totalQtyBook = max($alreadyBook);
            }
            else
            {
            $totalQtyBook =0;
            }
  
 

            if($carTotalQty > $totalQtyBook)
            {
            $can_book = 'Yes';
            }
            else
            {
            $can_book = 'No';
            }


 

           $response_array = array('data' => ['success' => true, 'can_book' => $can_book, 'code' => 200]);

           $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'Something went wrong', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }



}
<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\JWT;
use Tymon\JWTAuth\JWTGuard;
use JWTAuth;
use App\User;
use Auth;
use Response;
use Validator;
use Exception;
use Mail;
use Storage;
use App\Country;
use App\City;
use App\CustomerShipping;
use DB;
use App\CartProduct;
use App\ProductOptionValue;

class ApiController extends Controller {

    public function login(\Illuminate\Http\Request $request) {
        $credentials = $request->only('email', 'password');

        if(isset($request->lang) && $request->lang=='ar' )
        {
            $locale='ar';
        }
        else
        {
            $locale='en';
        }
        app()->setLocale($locale);       
         if($request->lang=='ar')
        {
             $message_cre  = trans('ar_mob_lang.INVALID_CREDENTIAL');  
        }
        else
        {
             $message_cre  = trans('en_mob_lang.INVALID_CREDENTIAL'); 

        }

        if($request->lang=='ar')
        {
             $messag_token  = trans('ar_mob_lang.INVALID_TOKEN_ERROR');  
        }
        else
        {
             $messag_token  = trans('en_mob_lang.INVALID_TOKEN_ERROR'); 

        }

        try {
            if (!$token = JWTAuth::attempt($credentials)) { // attempt to verify the credentials and create a token for the user
                return response()->json(['status' => 'error','error_code' => 401, 'message' => $message_cre]);
            }
        } catch (JWTException $e) {
            return response()->json(['status' => 'error','error_code' => 203, 'message' => $messag_token]);
            // something went wrong whilst attempting to encode the token
        }
        $user = $userid = Auth::id();
        $User = User::find($user);
        $returnuser = array('user_id' => $User->cus_id, "user_email" => $User->email, "user_name" => $User->cus_name, "address" => $User->cus_address1.
            ' '.$User->cus_address2,
            "telephone_number" => $User->ship_phone, 'token' => $token,'cus_country' => $User->cus_country,'cus_city' => $User->cus_city);
        if($request->lang=='ar')
        {
             $message  = trans('ar_mob_lang.LOGIN_SUCCESS');  
        }
        else
        {
             $message  = trans('en_mob_lang.LOGIN_SUCCESS'); 

        }
        $currentDate =  date('h:i d M, Y');
        User::where('cus_id',$User->cus_id)->update(['last_log_in'=>$currentDate]);

        return response()->json(['status' => "success",'code' => 200, 'message' => $message, 'data' => $returnuser]);
    }

     //User Mobile Verification
    public function userMobileVerification(Request $request) {
        $validator = Validator::make($request->all(), [
            'telephone_number' => 'required',
            //'email' => 'required',
        ]);

        $locale='';
         if(isset($request->lang) && $request->lang=='ar' )
        {
            $locale='ar';
        }
        else
        {
            $locale='en';
        }
        app()->setLocale($locale);   

        try {
            if ($validator->fails()) {

                return response()->json(['status' => 'error','error_code' => 203, 'message' => $validator->errors()]);

            }
            
            $telephone_number = $request->input('telephone_number');
            $email            = $request->input('email');

          
          

            $user_phone = User::where('cus_phone','=',$telephone_number)->orWhere('email','=',$email)->first();
           
           
         

            if ($user_phone->cus_phone == $telephone_number) {
               
                $response_array = array('data' => ['status' => 'error','error_code' => 403, 'message' => trans($locale.'_mob_lang.ALREADY_PHONE_REGISTERD')]);

                    $response = Response::json($response_array, 403);  
                                     
            }elseif($user_phone->email ==$email){
               
                  
            $response_array = array('data' => ['status' => 'error','error_code' => 403, 'message' => trans($locale.'_mob_lang.ALREADY_EMAIL_REGISTERD')]);
                    $response = Response::json($response_array, 403);
                     

            }else{
                $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => trans($locale.'_mob_lang.MOBILE_SUCCESS')]);
                    $response = Response::json($response_array, 200);

            }
        }catch (Exception $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }
        return $response;    

    }

    //User Registration Process
    public function userRegister(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'user_name' => 'required',
            'password' => 'required',
            'telephone_number' => 'required',
        ]);

        $locale='';
         if(isset($request->lang) && $request->lang=='ar' )
        {
            $locale='ar';
        }
        else
        {
            $locale='en';
        }
        app()->setLocale($locale);   

        try {
            if ($validator->fails()) {

                return response()->json(['status' => 'error','error_code' => 203, 'message' => $validator->errors()]);

            }

            $name = $request->input('user_name');
            $email = $request->input('email');
            $password = bcrypt($request->input('password'));
            $language_type = $request->input('language_type');
            $telephone_number = $request->input('telephone_number');
            $gender = $request->input('gender');


            $user_email = User::where('email', '=', $email)->first();
            if ($user_email != null) {
                return response()->json(['status' => 'error','error_code' => 403, 'message' => trans($locale.'_mob_lang.ALREADY_REGISTERD')]);
            }
            $user_phone = User::where('cus_phone', '=', $telephone_number)->first();
            if ($user_phone != null) {
                return response()->json(['status' => 'error','error_code' => 403, 'message' => trans($locale.'_mob_lang.ALREADY_PHONE_REGISTERD')]);
            }

            $saveUser = new User();
            $saveUser->cus_name = $name;
            $saveUser->email = $email;
            $saveUser->password = $password;
            $saveUser->cus_phone = $telephone_number;
            $saveUser->gender = $gender;

            $result = $saveUser->save();
            if (!$result)

                return response()->json(['status' => 'error','error_code' => 401, 'message' => 'Error in registration. Please try again']);

            $credentials = $request->only('email', 'password');

            try {
                if (!$token = JWTAuth::attempt($credentials)) { // attempt to verify the credentials and create a token for the user
                    return response()->json(['error' => 'invalid_credentials','error_code' => 401]);
                }
            } catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token','error_code' => 203]); // something went wrong whilst attempting to encode the token
            }

            $user = $userid = Auth::id();
            $User = User::find($user);
            $returnuser = array('user_id' => $User->cus_id, "user_email" => $User->email, "user_name" => $User->cus_name, "gender" => $User->gender, "address" => $User->cus_address1.
                ' '.$User->cus_address2, "telephone_number" => $User->cus_phone, 'token' => $token);
            return response()->json(['status' => "success", 'status_code' => 200, 'message' =>trans($locale.'_mob_lang.REGISTER_SUCCESS'), 'data' => $returnuser]);
            // return Response::json(array('status' => false, 'message'=>'User registered successfully'));
        } catch (Exception $e) {
            $data = [
                'status' => 'fail',
                'status_code' => $e->getCode(),
                'message' => $e->getMessage(),
            ];
            $result = Response::json($data);
            return $result;
        }
    }

    //Forget Password
    public function Forgotpassword(Request $request) {

        $email = $request->email;

        $validator = Validator::make($request->all(), [
            'email' => 'required',

        ]);
         $locale='';
        if(isset($request->lang) && $request->lang=='ar' )
        {
            $locale='ar';
        }
        else
        {
            $locale='en';
        }
        app()->setLocale($locale); 

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => $error_messages,
                'error_code' => 204, 'error_messages' => trans($locale.'_mob_lang.INVALID_INPUT'));

            $response = Response::json($response_array);
            return $response;
        } else {

            $user = User::where('email', $email)->first();

            if (!$user) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.EMAIL_NOT_FOUND') , 'error_code' => 203]);

              
                $response = Response::json($response_array);
                return $response;

            }

            $new_password = time();
            $new_password .= rand();
            $new_password = sha1($new_password);
            $new_password = substr($new_password, 0, 8);
            $user->password = Hash::make($new_password);

            $subject = "Your New Password";
            $email_data['name'] = $user->author_name;
            $email_data['password'] = $new_password;
            $email_data['email'] = $user->email;
            
            $user_id = base64_encode($user->cus_id);
            $email_data2 = base64_encode($user->email);
            $params = 'id='.$user_id.'&e_i='.$email_data2;

            //Sent Email to Registered user email ID
            if ($user) {
                try {

                    Mail::send('emails.password', array('email_data' => $email_data,'token' => $params,'email' => $user->email,'pwd' => $new_password), function($message) use($email, $subject) {
                        $message->to($email)->subject($subject);
                    });
                } catch (Exception $e) {
                    $response_array = array('data' => ['success' => false, 'error' => 'Mail Config is wrong', 'error_code' => 204]);
                }

                $user->save();
                $response_array = array('data' => ['success' => true,'code' => 200, 'message' => trans($locale.'_mob_lang.SUCESS_FORGOT')]);

            } else {
                $response_array = array('data' => ['success' => false, 'message' => 'Something went wrong', 'error_code' => 500]);
            }

        }

        $response = Response::json($response_array);
        return $response;

    }

    // Changed Password
    public function changePassword(Request $request) {        

        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed',
            'old_password' => 'required',
        ]);
         $locale='';
        if(isset($request->lang) && $request->lang=='ar' )
        {
            $locale='ar';
        }
        else
        {
            $locale='en';
        }
        app()->setLocale($locale); 

        if ($validator->fails()) {

            $error_messages = $validator->messages()->all();

            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 204, 'error_messages' => $error_messages);

           

        } else {
            try {
            $user = Auth::user();
            $old_password = $request->old_password;
            $new_password = $request->password;
            $confirm_password = $request->password_confirmation;

                if (Hash::check($old_password, $user->password)) {
                    $user->password = Hash::make($new_password);
                    $user->save();

                    $response_array = array('data' => ['success' => true,'code' => 200, 'message' => trans($locale.'_mob_lang.PASS_SUCESS_CHANGE')]);
                   

                } else {
                    $response_array = array('data' => ['success' => false,'error_code' => 500, 'error' => trans($locale.'_mob_lang.PASS_ERROR_CHANGE')]);
                    
                }

            } catch (ModelNotFoundException $e) {

                $response_array = array('data' => ['success' => false,'error_code' => 203, 'error' => 'User ID not found']);
                
            }

        }

        $response = Response::json($response_array);

        return $response;
    }

    //Edit User Profile
    public function editProfile(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',

        ]);
         $locale='';
        if(isset($request->lang) && $request->lang=='ar' )
        {
            $locale='ar';
        }
        else
        {
            $locale='en';
        }
        app()->setLocale($locale); 

        if ($validator->fails()) {

            $response_array = array('data' => ['success' => false,'error_code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {
            try {

                $User = Auth::user();

                if ($request->has('name'))
                    $User->cus_name = $request->name;
                if ($request->has('phone'))
                    $User->cus_phone = $request->phone;

                if ($request->has('city'))
                    $User->cus_city = $request->city;
                if ($request->has('country'))
                    $User->cus_country = $request->country;
                if ($request->has('zipcode'))
                    $User->cus_zipcode = $request->zipcode;

                if ($request->has('address'))
                    $User->cus_address1 = $request->address;
                if ($request->has('dob'))
                    $User->dob = $request->dob;
                if ($request->has('gender'))
                    $User->gender = $request->gender;

                if ($request->has('bank_name'))
                    $User->bank_name = $request->bank_name;
                if ($request->has('bank_account'))
                    $User->bank_account = $request->bank_account;
                if ($request->has('map_url'))
                    $User->map_url = $request->map_url;
                
                if ($request->hasFile('avatar')) {
                    Storage::delete($User->avatar);
                    $User->avatar = $request->avatar->store('User/profile');
                    $ckh= $User->avatar;
 

                     $User->cus_pic = url('/').'/storage/app/'.$ckh;
                }

                 $User->save();
               
               
                 $checkuserid = CustomerShipping::where('customer_id',$User->cus_id)->first();
                 if(empty($checkuserid)) {
                    $shipdetails = new CustomerShipping;
                 if ($request->has('name'))
                    $shipdetails->ship_name = $request->name;
                    $shipdetails->customer_id =$User->cus_id;  
                if ($request->has('phone'))
                    $shipdetails->ship_phone = $request->phone;
                if ($request->has('city'))
                    $shipdetails->ship_city_id = $request->city;
                if ($request->has('country'))
                    $shipdetails->ship_country = $request->country;
                if ($request->has('zipcode'))
                    $shipdetails->ship_postalcode = $request->zipcode;
                if ($request->has('address'))
                    $shipdetails->ship_address1 = $request->address;
                    $shipdetails->save();
                   }



                $response_array = array('data' => ['success' => true,'code' => 200, 'message' =>trans($locale.'_mob_lang.PROFILE_UPDATE_SUCESS')]);
                $response = Response::json($response_array);
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false,'error_code' => 203, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR')]);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }
    

    //Get Country
    public function getCountry(Request $request) 
    {
        $countrylist = Country::where('co_status',1)->orderBy('co_name','country_code', 'ASC')->get();
        $response_array = array('data' => ['success' => true,'code' => 200, 'country' => $countrylist]);
        $response = Response::json($response_array);
        return $response;
    }
    
    //Get City
    public function getCity(Request $request) 
    {
        $country_id = $request->country_id;
        $citylist = City::where('ci_con_id', '=', $country_id)->orderBy('ci_name', 'ASC')->get();
        $response_array = array('data' => ['success' => true,'code' => 200, 'city' => $citylist]);
        $response = Response::json($response_array);
        return $response;
    }

    // Signup process with social login
    public function socialsignup(Request $request) {
        $name = $request->name;
        $email = $request->email;
        $social_unique_id = $request->social_unique_id;
        $gender = $request->gender;
        $dob = $request->dob;
        $lang = $request->lang;
        $validator = Validator::make(
            $request->all(), [
                'social_unique_id' => 'required'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['status' => false,'error_code' => 204, 'message' => $validator->messages()->all()]);
        }

        try {

            $user = User::where('email', '=', $email)->first();
            $usercount = User::where('email', '=', $email)->count();

            if ($usercount > 0) {
                try {
                    if (!$token = JWTAuth::fromUser($user)) { // attempt to verify the credentials and create a token for the user
                        $AuthUser = new User();
                        $AuthUser->email = $email;
                        $AuthUser->cus_name = $name;
                        $AuthUser->social_unique_id = $request->social_unique_id;
                        $AuthUser->gender = $gender;
                        $AuthUser->save();
                        //$token = JWTAuth::attempt($credentials) ;
                        $user = User::where('email', '=', $email)->first();
                        $token = JWTAuth::fromUser($user);
                    }
                } catch (JWTException $e) {
                    return response()->json(['status' => 'error','error_code' => 401, 'message' => 'could_not_create_token']);
                    // something went wrong whilst attempting to encode the token
                }
            } else {
                $AuthUser = new User();
                $AuthUser->email = $email;
                $AuthUser->cus_name = $name;
                $AuthUser->social_unique_id = $request->social_unique_id;
                $AuthUser->gender = $gender;
                $AuthUser->save();
                //$token = JWTAuth::attempt($credentials) ;
                $user = User::where('email', '=', $email)->first();
                $token = JWTAuth::fromUser($user);

            }

            // $userdetails = User::where('email',$email)->get(); 
            $User = User::where('email', '=', $email)->first();

            if ($User) {

                $returnuser = array('user_id' => $User->cus_id,
                    "user_email" => $User->email,
                    "user_name" => $User->cus_name,
                    "address" => $User->cus_address1.
                    ' '.$User->cus_address2,
                    "telephone_number" => $User->ship_phone,
                    'token' => $token);
                if($lang=='ar')
                {
                 return response()->json(['status' => "success",'code' => 200, 'message' => 'أهلا بك! ابدأ التخطيط ', 'data' => $returnuser]); 
                }
                else
                {
                 return response()->json(['status' => "success",'code' => 200, 'message' => 'Welcome! Start Planning', 'data' => $returnuser]);
                }
                

            } else {
                return response()->json(['status' => false,'error_code' => 204, 'message' => "Invalid credentials!"]);
            }
        } catch (Exception $e) {
            return response()->json(['status' => false,'error_code' => 401, 'message' => $e->getMessage()]);
        }

    }

    public function merchantLogin(Request $request) 
    {
        $username     = $_REQUEST['username'];
        $password     = $_REQUEST['password'];
        $divice_token = $_REQUEST['divice_token'];
        $getmerchantInfo  = DB::table('nm_merchant')->where('mer_email',$username)->where('mer_staus','=','1')->first();
        if (\Hash::check($password, $getmerchantInfo->mer_password)) 
        {
        $getmerchant  = DB::table('nm_merchant')->where('mer_email',$username)->where('mer_staus','=','1')->first();
        }
        else
        {
        $getmerchant  = DB::table('nm_merchant')->where('mer_email',$username)->where('mer_staus','=','1')->where('mer_password',$password)->first();         
        }
       
        //echo '<pre>';print_r($getmerchant);die;
       
        if(isset($getmerchant) && $getmerchant!='')
        {
            $currentDate = date("Y-m-d H:i:s");

        DB::table('nm_merchant')->where('mer_email',$username)->update(['last_login'=>$currentDate]);    
 
          $returnuser = array('mer_id' => $getmerchant->mer_id,"mer_email"=>$getmerchant->mer_email,"user_name"=>$getmerchant->mer_fname, "mer_phone" =>$getmerchant->mer_phone,'token'=>'');
          return response()->json(['status' => "success",'code' => 200, 'message' => 'Welcome to Merchant','data'=>$returnuser]);
        }
        else
        {
           return response()->json(['status' => 'fail','error_code' => 204, 'message' => "Invalid credentials!"]);
        }
        
    }


public function hashOccasion(Request $request)
{ 
    try {
    $cityname = $request->cityname;

    $getCitycount = DB::table('nm_city')->where('ci_name', '=', $cityname)->count();
  
    if($getCitycount>=1)
    {
    $getCity = DB::table('nm_city')->where('ci_name', '=', $cityname)->first();
    $cid = $getCity->ci_id;
 

    $getCatID = array();
    $getOccsaion = DB::table('nm_occasions')->where('status', '=', 1)->where('city_id', '=', $cid)->get();
 

  

    foreach($getOccsaion as $val)
    {
         $gets = array();
   $getts = array();

        $getCatID = $val->id;
        $posted_by = $val->posted_by;
$getCinfoc = DB::table('nm_customer')->where('cus_id', '=', $posted_by)->count();
if($getCinfoc >=1)
{
     $getCinfo = DB::table('nm_customer')->where('cus_id', '=', $posted_by)->first();
        $val->posted_by_name = $getCinfo->cus_name;  
}
else
{
     $val->posted_by_name ='';  
}
      
        $getPhotos = DB::table('nm_occasions_gallery')->where('occasion_id', $getCatID)->get(); 
        foreach($getPhotos as $valu)
        {      
        array_push($gets, $valu->image_url);
        array_push($getts, $valu->created_at);
        }
        $val->image_url = $gets;
        $val->created_at = $getts;
    }
 if(isset($request->test) && $request->test==1)
 {
    print_r($getOccsaion); die;
 }
    return response()->json(['status' => "success",'code' => 200, 'data' => $getOccsaion]);
    }
    else
    {  
    return response()->json(['status' => "success",'code' => 401,  'message' => "No city found in database"]);   
    }


    } catch (Exception $e) {
    return response()->json(['status' => false,'error_code' => 401, 'message' => "Something went wrong"]);
    }


}

 








public function staticContent(Request $request)
{
 
            try {
            $pageid = $request->pageid;
            $lang = $request->lang;
            if($lang == 'en')
            { 

            if($pageid == 16){
            //About us
            $getContent = DB::table('nm_cms')->where('id', '=', 16)->select('title','content')->first();
            }
            if($pageid == 17){
            //Privacy Policy
            $getContent = DB::table('nm_cms')->where('id', '=', 17)->select('title','content')->first();
            }
            if($pageid == 18){
            //Return Policy
            $getContent = DB::table('nm_cms')->where('id', '=', 18)->select('title','content')->first();
            }


            if($pageid == 19){
            //Return Policy
            $getContent = DB::table('nm_cms')->where('id', '=', 19)->select('title','content','mapurl')->first();
            }

 

            if($pageid == 15){
            //Testimonial
            $getContent = DB::table('nm_cms')->where('id', '=', 15)->select('title','content')->first();
            }
             if($pageid == 23){
            //Testimonial
            $getContent = DB::table('nm_cms')->where('id', '=', 23)->select('title','content')->first();
            }
            }
            else
            {
            if($pageid == 16){
            //About us
            $getContent = DB::table('nm_cms')->where('id', '=', 16)->select('title_ar as title','content_ar as content')->first();
            }
            if($pageid == 17){
            //About us
            $getContent = DB::table('nm_cms')->where('id', '=', 17)->select('title_ar as title','content_ar as content')->first();
            }

            if($pageid == 18){
            //About us
            $getContent = DB::table('nm_cms')->where('id', '=', 18)->select('title_ar as title','content_ar as content')->first();
            }

            if($pageid == 15){
            //About us
            $getContent = DB::table('nm_cms')->where('id', '=', 15)->select('title_ar as title','content_ar as content')->first();
            }
             if($pageid == 23){
            //Testimonial
            $getContent = DB::table('nm_cms')->where('id', '=', 23)->select('title_ar as title','content_ar as content')->first();
            }


            if($pageid == 19){
            //Return Policy
            $getContent = DB::table('nm_cms')->where('id', '=', 19)->select('title_ar as title','content_ar as content','mapurl')->first();
            }


            }


            return response()->json(['status' => "success",'code' => 200, 'data' => $getContent]);

            } catch (Exception $e) {
            return response()->json(['status' => false,'error_code' => 401, 'message' => 'Something went wrong']);
            }

 

}


public function checkQty(Request $request)
{

try {
 
        //Check product remaining qty
        $ProIds = array();
        $ExtraQty =  array();
       $cart_pro = CartProduct::where('cart_id', '=', $request->cart_id)->with('getProduct:pro_id,pro_title,pro_Img,pro_price,Insuranceamount,pro_mc_id,pro_qty')->with('getCategory:mc_id,mc_name,mc_img')->orderBy('updated_at', 'DESC')->distinct()->get();

        foreach($cart_pro as $cartPro)
        {
        $checkQty = $cartPro->quantity;


  $cart_sub_type = $cartPro->cart_sub_type; $buy_rent = $cartPro->buy_rent;

  if($cart_sub_type == 'beauty_centers' || $cart_sub_type == 'hall' || $cart_sub_type == 'invitations' || $cart_sub_type == 'travel' || $cart_sub_type == 'car'  || $cart_sub_type == 'cosmetic'   || $cart_sub_type == 'skin'  || $cart_sub_type == 'men_saloon'  || $cart_sub_type == 'spa' || $cart_sub_type == 'makeup_artists'  || $cart_sub_type == 'photography'  || $cart_sub_type == 'videography' || $cart_sub_type == 'hospitality'  || $cart_sub_type == 'tailor' || $cart_sub_type == 'buffet' || $cart_sub_type == 'acoustic') {continue; }

        if($cart_sub_type == 'abaya'  && $buy_rent == 'buy'){continue; }


        $productid = $cartPro->product_id; 

        if($productid!='' && ($checkQty!='' || $checkQty!='0'))
        {
        $getPrototalQty = $cartPro->getProduct[0]->pro_qty;
        // In case of dress and abaya we need to get qty from option section

        $productsize = $cartPro->product_size;
        if($productsize!='')
        {
        $productOptioncount = ProductOptionValue::where('product_id',$productid)->where('option_title',$productsize)->count();
        if($productOptioncount >=1)
        {
        $productOption = ProductOptionValue::where('product_id',$productid)->where('option_title',$productsize)->first();
                if( $buy_rent == 'rent' && ($cart_sub_type == 'dress' || $cart_sub_type == 'abaya')){
                $getPrototalQty = $productOption->value_ar;
                }
                else
                {
                $getPrototalQty = $productOption->value;
                }

        }   
        }
        // In case of dress and abaya we need to get qty from option section


        if($getPrototalQty < $checkQty)
        {
        array_push($ProIds, $productid);
        array_push($ExtraQty, $getPrototalQty);
        }  
        }
        }

        if(count($ExtraQty)>=1)
        {
            $productname = array();
            $productqty = array();
            for($i=0;$i < count($ProIds); $i++ )
            {
            $getProname =  DB::table('nm_product')->where('pro_id',$ProIds[$i])->first();
            $getProname = $getProname->pro_title; 
            $getProqty = $ExtraQty[$i]; 

            array_push($productname, $getProname);
            array_push($productqty, $getProqty);

            }
            

        return response()->json(['status' => "success",'code' => 200, 'message' => 'Product not available', 'productname' => $productname, 'maxqty' => $productqty]);


       // return Redirect::route( 'mycart' )->with( 'ProIds', $ProIds )->with( 'ExtraQty', $ExtraQty );
        }
        else
        {
return response()->json(['status' => "success",'code' => 200, 'message' => 'All Ok']);

        }

        //Check product remaining qty

 } catch (Exception $e) {
            return response()->json(['status' => false,'error_code' => 401, 'message' => 'Something went wrong']);
            }
}


public function docBookedtime(Request $request)
{

        $validator = Validator::make($request->all(), [
        'docid' => 'required',
        'date' => 'required',
        'shop_id' => 'required',
        ]);
        $locale='';
        if(isset($request->lang) && $request->lang=='ar' )
        {
        $locale='ar';
        }
        else
        {
        $locale='en';
        }
        app()->setLocale($locale);   

        try {
        if ($validator->fails()) {
        return response()->json(['status' => 'error','error_code' => 203, 'message' => $validator->errors()]);
        }

        $docid = $request->docid;
        $date = strtotime($request->date);   
        $shop_id = $request->shop_id;     
         
       $getClinicInfo =  DB::table('nm_order_services_staff')->where('product_type','clinic')->where('service_id',$docid)->where('shop_id',$shop_id)->get();
      $getContent=array();

       

       foreach ($getClinicInfo as $key => $value) {
          $getBookingD = strtotime($value->booking_date);
          $TimeD = $value->start_time;
          if($getBookingD == $date)
          {
        array_push($getContent, $TimeD);
          }
       } 
         return response()->json(['status' => "success",'code' => 200, 'data' => $getContent]);

        } catch (Exception $e) {
        $data = [
        'status' => 'fail',
        'status_code' => 500,
        'message' => 'Something went wrong',
        ];
        $result = Response::json($data);
        return $result;
        }

}
 

}
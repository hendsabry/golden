<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\JWT;
use Tymon\JWTAuth\JWTGuard;
use JWTAuth;
use App\User;
use Auth;
use Response;
use Validator;
use Exception;
use Mail;
use Storage;
use App\Country;
use App\City;
use App\CustomerShipping;
use DB;
class ElectronicInvitationCategoryController extends Controller {

    //Get Country
    public function getElectronicCatgeory(Request $request) 
    {
        
        $eicatlist=array();
    	 $getArrayOfOcc = array('1'=>'Business Meeting','2'=>'Wedding Occasion'); 
    	  foreach($getArrayOfOcc as $key=>$ocval){

    	  	$elect_invite =  DB::table('nm_product')->where('event_type',$key)->where('pro_mc_id', '=',16)->where('pro_id', '!=',454)->where('pro_status', 1)->where('pro_id', '!=',455)->orderBy('created_at','desc')->get();

    	  	$eicatlist[$ocval]=$elect_invite;

    	  }

        $eicatlist=$eicatlist;

        $response_array = array('data' => ['success' => true,'code' => 200, 'catgeory' => $eicatlist]);
        $response = Response::json($response_array);
        return $response;
    }
    
   

}
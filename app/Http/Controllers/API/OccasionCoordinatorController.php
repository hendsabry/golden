<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Http\Models;
use App\Category;
use App\Products;
use App\City;
use App\ServiceAttribute;
use App\CategoryGallery;
use App\ProductAttribute;
use App\ProductOption;
use App\ProductOptionValue;
use App\ProductPackage;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartProductRent;
use App\CartInvitation;
use App\CartServiceAttribute;
use App\CartHospitality;
use App\CartProductAttribute;
use App\CartProductPackage;
use App\StaffNationality;
use Auth;
use App\User;
use Response;
use Storage;
use App\InvitationList;
use DB;
class OccasionCoordinatorController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |   Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function __construct() {}

    /**Get Car Rental info.**/

    /*
       subcategory_id(2th lavel category in category table) become pro_mc_id in product table to get full Car Rental info.
    */
    public function getCoshaInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id'    => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id'      => 'required', //Vendor ID
            'language_type'  => 'required|max:255',
            'token'          => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first();
                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($input['vendor_id']);


                if (!empty($category)) {

                    if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                        //GET SUB CATEGORY
                        $sub_catg = Category::select('mc_id','mc_name_ar as mc_name', 'mc_status')->where('mc_id', '=', $input['subcategory_id'])->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name','insurance_amount','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();
                        //GET VENDOR DETAIL
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title','parent', 'image', 'status','attribute_title as title')->where('status', '=', 1)->where('services_id', '=', $input['subcategory_id'])->orderBy('title', 'ASC')->get();
                        if (!$menu->isEmpty()) {                    

                            foreach($menu as $key => $value) {
                                if($value->id == 47){
                                    $sub_menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title','parent', 'image', 'status','attribute_title as title')->where('status', '=', 1)->where('services_id', '=', $input['vendor_id'])->where('parent', '=', $input['subcategory_id'])->orderBy('title', 'ASC')->get(); 
                                $value->sub_menu = $sub_menu;
                                    //get cosha detail according cosha sub category.
                            foreach($sub_menu as $key => $menu_val) {
                                $product = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price','pro_disprice','pro_discount_percentage', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','parent_attribute_id','pro_title as title','Insuranceamount','pro_qty')->where('pro_mc_id', '=', $menu_val->services_id)->where('parent_attribute_id', '=','1')->where('attribute_id', '=', $menu_val->id)->where('pro_status', '=', 1)->orderBy('title', 'ASC')->paginate(9);
                                $menu_val->product = $product;
                               

                            }

                                }else if($value->id == 46){
                                 $product = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price','pro_disprice','pro_discount_percentage','pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_title as title','Insuranceamount','pro_qty')->where('pro_mc_id', '=', $input['vendor_id'])->where('parent_attribute_id', '=','2')->where('pro_status', '=', 1)->orderBy('title', 'ASC')->paginate(9);
                                $value->product = $product;   
                                }
                                
                                
                               
                            }


                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'sub_category_id' => $sub_catg->mc_id, 'sub_category_name' => $sub_catg->mc_name_ar, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                         //GET SUB CATEGORY
                        $sub_catg = Category::select('mc_id','mc_name', 'mc_status')->where('mc_id', '=', $input['subcategory_id'])->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name','insurance_amount','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                        //GET VENDOR DETAIL
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title','parent', 'image', 'status')->where('status', '=', 1)->where('services_id', '=', $input['subcategory_id'])->orderBy('attribute_title', 'ASC')->get();
                        if (!$menu->isEmpty()) {                    

                            foreach($menu as $key => $value) {
                                if($value->id == 47){

                                /*$productchk = Products::select( 'attribute_id')->where('pro_mc_id', '=', $VendorID)->where('parent_attribute_id', '=','1')->where('pro_status', '=', 1)->get();
                                $serviceChk = [];
                                foreach ($productchk as $productchk) {
                                array_push($serviceChk, $productchk->attribute_id);
                                }
 */

 


                                    $sub_menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title','parent', 'image', 'status')->where('status', '=', 1)->where('services_id', '=', $input['vendor_id'])->where('parent', '=', $input['subcategory_id'])->orderBy('attribute_title', 'ASC')->get(); 
                                $value->sub_menu = $sub_menu;
                                    //get cosha detail according cosha sub category.
                            foreach($sub_menu as $key => $menu_val) {
                                $product = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price','pro_disprice','pro_discount_percentage', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','parent_attribute_id','Insuranceamount','pro_qty')->where('pro_mc_id', '=', $menu_val->services_id)->where('parent_attribute_id', '=','1')->where('attribute_id', '=', $menu_val->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                                $menu_val->product = $product;
                               
                            }
                                }else if($value->id == 46){
                                 $product = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price','pro_disprice','pro_discount_percentage', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','Insuranceamount','pro_qty')->where('pro_mc_id', '=', $input['vendor_id'])->where('parent_attribute_id', '=','2')->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                                $value->product = $product;   
                                }
                                
                                
                               
                            }
                            


                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'sub_category_id' => $sub_catg->mc_id, 'sub_category_name' => $sub_catg->mc_name, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Set Cosha Data in add to Cart
    public function setCoshaAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required',//Get product id
            'cart_type' => 'required',//Get cart type
            'cart_sub_type' => 'required',//Get cart sub type
            'attribute_id' => 'required',//Get attribute id
            'total_price' => 'required'
        ]);
   $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

                $User = Auth::user();

                if(count($input['products'])>=1)
                {
                $pro_catg = Products::where('pro_mc_id', '=', $input['product_id'])->whereIn('pro_id',$input['products'])->first();
                }
                else
                {
                    $pro_catg = Products::where('pro_mc_id', '=', $input['product_id'])->first();
                }
               

                     



              $rand= rand(1,100);

                if (!empty($pro_catg)) {
                    $product = Category::where('mc_id', '=', $input['product_id'])->first();

                   
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                    if (!empty($cart)) {
                         $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('category_id', '=', $input['product_id'])->delete();

                       
                         $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('category_id', '=', $input['product_id'])->where('attribute_id', '=', '47')->delete();
                  




                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                   
                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $product->vendor_id;
                    $cart_product_data['shop_id'] = $product->mc_id;
                    $cart_product_data['shop_vendor_id'] = $product->parent_id;
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['category_id'] = $input['product_id'];
                    $cart_product_data['product_id'] = $pro_catg->pro_id;
                    $cart_product_data['status'] = 1;
                    $cart_product_data['pro_title'] = $product->mc_name;
                    $cart_product_data['pro_title_ar'] = $product->mc_name_ar;
                    $cart_product_data['pro_desc'] = $product->mc_discription;
                    $cart_product_data['pro_desc_ar'] = $product->mc_discription_ar;
                    $cart_product_data['pro_Img'] = $product->mc_img;
                    $cart_product_data['total_price'] = $input['total_price'];
                     $cart_product_data['container_id'] = $rand;

                    
                     if (isset($input['product_id']) && count($input['product_id'])>=1) {
                       if($input['attribute_id'] == 47){   $cart_product_data['buy_rent'] = 'design'; }
                        $cart_product_data['showcartproduct'] = 1;
                    }

                   // print_r($cart_product_data); 
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                     
                     // 47 - Design your cosha


                    $cart_attribute_data = array();
                    if($input['attribute_id'] == 47){
                      
                     if (isset($input['products'])) {
                         
                          
                            $quantitycount = $input['quantity'];                           

                            $insurancesdata = $input['insurance'];

                            foreach($input['products'] as $key => $value) {
                            $getProID = $value;
                             $num_of_quantity = $quantitycount[$key];

                             $insuranceamount   =  $insurancesdata[$key];
 
                            $cosha_product = Products::select('pro_id','pro_mc_id','parent','pro_mr_id','attribute_id','pro_disprice','pro_title','pro_title_ar','pro_desc','pro_desc_ar','pro_Img','pro_price')->where('pro_id', '=', $value)->first();

                            $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $cosha_product->attribute_id)->first(); 

                                $cart_attribute_data['cart_id'] = $cart->id;
                                $cart_attribute_data['cart_type'] = $input['cart_type'];
                                $cart_attribute_data['category_id'] = $input['product_id'];
                                $cart_attribute_data['attribute_id'] =  $input['attribute_id'];
                                $cart_attribute_data['sub_attribute_id'] = $cosha_product->attribute_id;
                                $cart_attribute_data['product_id'] = $getProID;
                                $cart_attribute_data['attribute_title'] = 'Design Your Kosha';
                                $cart_attribute_data['attribute_title_ar'] = 'تصميم كوشا الخاص بك';
                                $cart_attribute_data['sub_attribute_title'] = $menu->attribute_title;
                                $cart_attribute_data['sub_attribute_title_ar'] = $menu->attribute_title_ar;
                                $cart_attribute_data['pro_title'] = $cosha_product->pro_title;
                                $cart_attribute_data['pro_title_ar'] = $cosha_product->pro_title_ar;
                                $cart_attribute_data['pro_desc'] = $cosha_product->pro_desc;
                                $cart_attribute_data['pro_desc_ar'] = $cosha_product->pro_desc_ar;
                                $cart_attribute_data['pro_img'] = $cosha_product->pro_Img;
                                if(isset($num_of_quantity) && $num_of_quantity!='') {
                                $cart_attribute_data['quantity'] = $num_of_quantity;
                              } else {
                                 $cart_attribute_data['quantity'] = '';
                              }

                              
                            $cart_attribute_data['insurance_amount'] = $insuranceamount;
                          
                            

                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $cosha_product->pro_mr_id;
                    $cart_product_data['shop_id'] = $cosha_product->pro_mc_id;
                    $cart_product_data['shop_vendor_id'] = $product->parent_id;
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['category_id'] = $input['product_id'];
                    $cart_product_data['product_id'] = $getProID;
                    $cart_product_data['status'] = 1;
                    $cart_product_data['insurance_amount'] = $num_of_quantity * $insuranceamount;
                    $cart_product_data['quantity'] = $num_of_quantity;


                    $pro_disprices = $cosha_product->pro_disprice;
                    $pro_prices = $cosha_product->pro_price;


                    if($pro_disprices >=1)
                    {
                    $cart_product_data['total_price'] = $insuranceamount *  $num_of_quantity + $pro_disprices *  $num_of_quantity;
                    }
                    else
                    {
                    $cart_product_data['total_price'] =  $insuranceamount *  $num_of_quantity + $pro_prices *  $num_of_quantity;
                    }


                    $cart_product_data['pro_title'] = $cosha_product->pro_title;
                    $cart_product_data['pro_title_ar'] = $cosha_product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $cosha_product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $cosha_product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $cosha_product->pro_Img;
                    $cart_product_data['buy_rent'] = 'design';
                    $cart_product_data['new_price'] = $input['total_price'];
                    $cart_product_data['container_id'] = $rand;

                $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                $PID = $cart_product->id;
                $cart_attribute_data['cart_product_id'] = $PID;
                 if($cosha_product->pro_disprice >=1 )
                    {
                    $cart_attribute_data['price'] = $cosha_product->pro_disprice;  
                    }
                    else
                    {
                    $cart_attribute_data['price'] = $cosha_product->pro_price;   
                    }
                $cart_attribute_data['total_price'] = $cart_product_data['total_price'];
                $cart_attribute_data['shop_vendor_id'] =   $product->vendor_id;
                $cart_value =  CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry*/                         
                           
                            }
                        }   
                    }
 

                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                } else {
                    $product = Products::where('pro_id', '=', $input['product_id'])->first();

                if (!empty($product)) {
                    $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                    if (!empty($cart)) {
                        $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $input['product_id'];
                   
                    $cart_product_data['status'] = 1;
                    $cart_product_data['shop_vendor_id'] = $category->vendor_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    
                     if($product->pro_disprice >=1)
                     {
                      $cart_product_data['pro_price'] = $product->pro_disprice;  
                     }
                     else
                     {
                        $cart_product_data['pro_price'] = $product->pro_price;

                     }


                    $cart_product_data['container_id'] = $rand;
                    $cart_product_data['buy_rent'] = 'readymade';
                    
                     
         $cart_product_data['total_price'] = $input['insurance'] *  $input['quantity'] + $cart_product_data['pro_price'] *  $input['quantity'];

 //$cart_product_data['category_id'] = $product->pro_mc_id;
           $cart_product_data['insurance_amount'] = $input['insurance'];  

                   // $cart_product_data['quantity'] = $input['quantity'];
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
$UID = $cart_product->id;
                    $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first();
                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $input['cart_type'];
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['product_id'] = $input['product_id'];
                    $cart_attribute_data['attribute_id'] = $input['attribute_id'];
                    $cart_attribute_data['attribute_title'] = $menu->attribute_title;
                    $cart_attribute_data['attribute_title_ar'] = $menu->attribute_title_ar;
                    $cart_attribute_data['quantity'] = $input['quantity'];
                    
                    if($product->pro_disprice >=1 )
                    {
                    $cart_attribute_data['price'] = $product->pro_disprice;  
                    }
                    else
                    {
                    $cart_attribute_data['price'] = $product->pro_price;   
                    }
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['pro_title'] =  $product->pro_title;
                    $cart_attribute_data['pro_title_ar'] =  $product->pro_title_ar;
                    $cart_attribute_data['pro_desc'] =  $product->pro_desc;
                    $cart_attribute_data['pro_desc_ar'] =  $product->pro_desc_ar;
                    $cart_attribute_data['pro_img'] =  $product->pro_Img;
                    $cart_attribute_data['insurance_amount'] = $input['insurance'];                  
                    
                    $cart_attribute_data['total_price'] = $input['insurance'] *  $input['quantity'] + $cart_attribute_data['price'] *  $input['quantity'];
                    
 
                    $cart_attribute_data['cart_product_id'] =  $UID;
                    $cart_attribute_data['shop_vendor_id'] =   $category->vendor_id;

                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry 
                    

                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.CART_ERROR'), 'code' => 500]);
                    $response = Response::json($response_array, 500);
                }
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }

    /**Get Cosha  data from add to cart  data**/
    public function getCoshaAddToCartData(Request $request) {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                $cosha_pro = array(); 
 
                $cart_pro = CartProduct::where('cart_type','=','occasion')->where('cart_id','=',$cart->id)->where('product_id','=',$input['product_id'])->where('quantity','<','1')->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,pro_disprice')->first();
 

  $productInfo = Products::select('parent_attribute_id')->where('pro_id', $input['product_id'])->first();


 $getAttr = $productInfo->parent_attribute_id;

                $cart_pro_count = CartProduct::where('cart_type','=','occasion')->where('cart_id','=',$cart->id)->where('product_id','=',$input['product_id'])->count();

                //get cosha product 
 
                if (!empty($cart) &&  $getAttr ==2 ) {
                    $cart_pro = CartProduct::where('cart_type','=','occasion')->where('cart_id','=',$cart->id)->where('product_id','=',$input['product_id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,pro_disprice')->first();

                    if (isset($cart_pro)) {
                    //get cosha product 
                        
                        $attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=',$input['product_id'])->with('getProductServiceAttribute:id,services_id,attribute_title,attribute_title_ar')->with('getSubProductAttribute:id,services_id,attribute_title,attribute_title_ar')->first();
                   
 
                        $category = Category::select('mc_id','mc_img', 'mc_name', 'mc_name_ar','address','address_ar','mc_status','city_id')->where('mc_id', '=', $cart_pro->getProduct[0]->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();
                        
                            $cosha_pro['pro_id'] = $cart_pro->getProduct[0]->pro_id;
                            if ($input['language_type'] == 'en') {

                                if(!empty($category)){
                                   $cosha_pro['category_name'] = $category->mc_name;
                                   $cosha_pro['category_img'] = $category->mc_img;
                                   $cosha_pro['address'] = $category->address; 
                                   $cosha_pro['city_name'] = $category->getCity->ci_name;
                                }
                                if(isset($attribute->getProductServiceAttribute) && count($attribute->getProductServiceAttribute)>=1)
                                {
     $cosha_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title;
                                }
                                else
                                {
                                        $cosha_pro['attribute_title'] = ''; 
                                }
                           
                                $cosha_pro['pro_title'] = $cart_pro->getProduct[0]->pro_title;
                                $cosha_pro['pro_desc'] = $cart_pro->getProduct[0]->pro_desc;
                               
                            } else {

                                if(!empty($category)){
                                   $cosha_pro['category_name'] = $category->mc_name_ar;
                                   $cosha_pro['category_img'] = $category->mc_img;
                                   $cosha_pro['address'] = $category->address_ar;
                                   $cosha_pro['city_name'] = $category->getCity->ci_name_ar; 
                                }

                                $cosha_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title_ar;
                                $cosha_pro['pro_title'] = $cart_pro->getProduct[0]->pro_title_ar;
                                $cosha_pro['pro_desc'] = $cart_pro->getProduct[0]->pro_desc_ar;
                                
                            }
                             

                                  if($attribute!='') {
                                  $cosha_pro['pro_qty'] = $attribute->quantity;
                                   $cosha_pro['insurance_amount'] = $attribute->insurance_amount;
                                    $cosha_pro['quantity'] = $attribute->quantity;

                                } else {
                                  $cosha_pro['pro_qty'] = $cart_pro->quantity;
                                   $cosha_pro['insurance_amount'] = $productInfo->Insuranceamount;
                                   $cosha_pro['quantity'] = 1;

                                } 

                         

                             
                            if($cart_pro->getProduct[0]->pro_disprice=='' || $cart_pro->getProduct[0]->pro_disprice==0) {
                                 $cosha_pro['pro_price'] = $cart_pro->getProduct[0]->pro_price;

                               } else {

                                  $cosha_pro['pro_price'] = $cart_pro->getProduct[0]->pro_disprice;
                               }
                             
                            
                            $cosha_pro['pro_Img'] =     $cart_pro->getProduct[0]->pro_Img;
                            $cosha_pro['pro_status'] =  $cart_pro->getProduct[0]->pro_status;
                            $cosha_pro['total_price'] = $cart_pro->total_price;
                    }
                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'cart_id' => $cart->id,'Cosha_data' => $cosha_pro);
                } else {
                     $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id','=',$input['product_id'])->where('quantity','<',1)->first();
                //get cosha product  product. ->where('quantity','=','')


                   $CatID = $cart_pro->category_id;
                    if (!empty($cart) && !empty($cart_pro)) {

                    if (isset($cart_pro)) {
                    //get cosha product with service attribute
                    $cosha_pro = array(); 
                    $cosha_branch = array();
                    $attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')
                    ->where('cart_id', '=', $cart->id)
                    ->where('category_id', '=', $CatID)
                    ->where('attribute_title', '=', 'Design Your Kosha')                    
                    ->with('getProductServiceAttribute:id,services_id,attribute_title,attribute_title_ar')
                    ->with('getSubProductAttribute:id,services_id,attribute_title,attribute_title_ar')
                    ->with('getProducts:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_disprice')
                    ->get();



                        foreach($attribute as $key => $value){
                            $cosha_pro[$key]['pro_id'] = $value->getProducts[0]->pro_id;
                            if ($input['language_type'] == 'en') {
                                
                                $cosha_pro[$key]['attribute_title'] = $value->getProductServiceAttribute[0]->attribute_title;
                                $cosha_pro[$key]['sub_attribute_title'] = $value->getSubProductAttribute[0]->attribute_title;
                                $cosha_pro[$key]['pro_title'] = $value->getProducts[0]->pro_title;
                                $cosha_pro[$key]['pro_desc'] = $value->getProducts[0]->pro_desc;
                                $cosha_pro[$key]['quantity'] = $value->quantity;


                            } else {
                                $cosha_pro[$key]['attribute_title'] = $value->getProductServiceAttribute[0]->attribute_title_ar;
                                $cosha_pro[$key]['sub_attribute_title'] = $value->getSubProductAttribute[0]->attribute_title_ar;
                                $cosha_pro[$key]['pro_title'] = $value->getProducts[0]->pro_title_ar;
                                $cosha_pro[$key]['pro_desc'] = $value->getProducts[0]->pro_desc_ar;
                                $cosha_pro[$key]['quantity'] = $value->quantity;
                            }
                            
                            $cosha_pro[$key]['pro_qty'] = $value->getProducts[0]->pro_qty;



                             if($value->getProducts[0]->pro_disprice <= 1) {
                                 $cosha_pro[$key]['pro_price'] = $value->getProducts[0]->pro_price;

                               } else {

                                 $cosha_pro[$key]['pro_price'] = $value->getProducts[0]->pro_disprice;
                               }



                            $cosha_pro[$key]['pro_Img'] = $value->getProducts[0]->pro_Img;
                            $cosha_pro[$key]['pro_status'] = $value->getProducts[0]->pro_status;
                            $cosha_pro[$key]['total_price'] = $value->price;
                            $cosha_pro[$key]['insuranceAmount'] = $value->insurance_amount;
                            

                        }
                        $branch = Category::where('mc_status', '=', 1)->where('mc_id', '=', $CatID)->select('mc_id', 'mc_name_ar', 'mc_name', 'mc_img', 'mc_discription', 'city_id','address','address_ar')->with('getCity:ci_id,ci_name_ar,ci_name')->first();

 

                        if (!empty($branch)) {
                                    if ($input['language_type'] == 'en') {

                                        $cosha_branch['mc_name'] = $branch->mc_name;
                                        $cosha_branch['mc_img'] = $branch->mc_img;
                                        $cosha_branch['mc_discription'] = $branch->mc_discription;
                                        $cosha_branch['city_name'] = $branch->getCity->ci_name;
                                        $cosha_branch['address'] = $branch->address;
                                    } else {
                                        $cosha_branch['mc_name_ar'] = $branch->mc_name_ar;
                                        $cosha_branch['mc_img'] = $branch->mc_img;
                                        $cosha_branch['mc_discription'] = $branch->mc_discription;
                                        $cosha_branch['city_name'] = $branch->getCity->ci_name_ar;
                                        $cosha_branch['address'] = $branch->address_ar;
                                    }
                                }
                            
                    }
                     //echo '<pre>';print_r($cosha_pro);die;
                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'cart_id' => $cart->id,'cosha_branch' => $cosha_branch,'Cosha_data' => $cosha_pro);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'cart data not found & cart type must be cosha', 'code' => 500]);
                }

                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    public function getPhotographyStudioInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id'    => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id'      => 'required', //Vendor ID
            'language_type'  => 'required|max:255',
            'token'          => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id','opening_time','closing_time', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first();
                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($input['vendor_id']);

                if (!empty($category)) {

                    if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                        //GET SUB CATEGORY
                        $sub_catg = Category::select('mc_id','mc_name_ar as mc_name','opening_time','closing_time', 'mc_status')->where('mc_id', '=', $input['subcategory_id'])->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::select('mc_id','opening_time','closing_time', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();
                        //GET VENDOR DETAIL
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title','parent', 'image', 'status')->where('services_id', '=', $input['subcategory_id'])->where('parent', '=', '0')->orderBy('attribute_title', 'ASC')->get();
                        if (!$menu->isEmpty()) {                    

                            foreach($menu as $key => $value) {
                                $product = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price','pro_disprice','pro_discount_percentage', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour')->where('pro_mc_id', '=', $input['vendor_id'])->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                                $value->product = $product;
                                foreach($product as $pro_attr) {
                                $product_attr = ProductAttribute::select('id','product_id','vendor_id','attribute_title_ar as attribute_title','value','status')->where('status', '=', 1)->where('product_id', '=', $pro_attr->pro_id)->get(); //GET PRODUCT ATTRIBUTE
                               $pro_attr->product_attribute =  $product_attr;

                            }
                               
                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'sub_category_id' => $sub_catg->mc_id, 'sub_category_name' => $sub_catg->mc_name_ar, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                         //GET SUB CATEGORY
                        $sub_catg = Category::select('mc_id','mc_name', 'opening_time','closing_time','mc_status')->where('mc_id', '=', $input['subcategory_id'])->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::select('mc_id', 'opening_time','closing_time','mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                        //GET VENDOR DETAIL
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title','parent', 'image', 'status')->where('services_id', '=', $input['subcategory_id'])->where('parent', '=', '0')->orderBy('attribute_title', 'ASC')->get();
                        if (!$menu->isEmpty()) {                    

                            foreach($menu as $key => $value) {
                                $product = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_disprice','pro_discount_percentage')->where('pro_mc_id', '=', $input['vendor_id'])->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                                $value->product = $product;
                                foreach($product as $pro_attr) {
                                $product_attr = ProductAttribute::select('id','product_id','vendor_id','attribute_title','value','status')->where('status', '=', 1)->where('product_id', '=', $pro_attr->pro_id)->get(); //GET PRODUCT ATTRIBUTE
                               $pro_attr->product_attribute =  $product_attr;

                            }
                               
                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'sub_category_id' => $sub_catg->mc_id, 'sub_category_name' => $sub_catg->mc_name, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Set Photography And Studio Data in add to Cart
    public function setPhotographyStudioAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required',//Get product id
            'cart_type' => 'required',//Get cart type
            'cart_sub_type' => 'required',//Get cart type
            'service_date' => 'required',//Get service date
            'service_time' => 'required',//Get service time
            'attribute_id' => 'required',//Get attribute id
            'total_price' => 'required'
        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

                $User = Auth::user();
                
                $product = Products::where('pro_id', '=', $input['product_id'])->first();

                if (!empty($product)) {
                    $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                    if (!empty($cart)) {
                        $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                        $cart_pro_rent = CartProductRent::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                        $cart_pro_attr = CartProductAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        CartHospitality::where('category_id', '=', $input['product_id'])->delete();


                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $input['product_id'];
                    $cart_product_data['total_price'] = $input['total_price'];
                    $cart_product_data['status'] = 1;
                    $cart_product_data['shop_vendor_id'] = $category->parent_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['pro_price'] = $product->pro_price;
                    $cart_product_data['location'] = $input['location'];
                    $cart_product_data['quantity'] = 1;
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 


                      $cart_photography_data                = array();
                    $cart_photography_data['cart_id']     = $cart->id;
                    $cart_photography_data['category_id'] = $input['product_id'];



                    $cart_photography_data['date'] = date("Y-m-d", strtotime($input['service_date']));
                    $cart_photography_data['time'] = $input['service_time'];

                    $cart_photography_data['elocation']    = $input['location'];
                    $cart_photography_data = CartHospitality::create($cart_photography_data); 



                    
                    $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first();
                    $cart_attribute_data = array();  
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['cart_type'] = $input['cart_type'];
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['attribute_id'] =  $input['attribute_id'];
                    $cart_attribute_data['sub_attribute_id'] = $product->attribute_id;
                    $cart_attribute_data['product_id'] = $input['product_id'];
                    $cart_attribute_data['attribute_title'] = $menu->attribute_title;
                    $cart_attribute_data['attribute_title_ar'] = $menu->attribute_title_ar;
                    CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry*/
                    
                    $cart_product_rent = array();
                    $cart_product_rent['cart_id'] = $cart->id;
                    $cart_product_rent['cart_type'] = $input['cart_type'];
                    $cart_product_rent['product_id'] = $input['product_id'];
                    $cart_product_rent['rental_date'] = date("Y-m-d", strtotime($input['service_date']));
                    $cart_product_rent['rental_time'] = $input['service_time'];
                    CartProductRent::create($cart_product_rent); //Product rental date and time entry

                    $product_attr = ProductAttribute::select('id', 'product_id', 'attribute_title_ar','attribute_title', 'value')->where('product_id', '=', $input['product_id'])->get();
                    if(!empty($product_attr)){
                        foreach ($product_attr as $key => $value) {
                    
                    $cart_product_attr = array();
                    $cart_product_attr['cart_id'] = $cart->id;
                    $cart_product_attr['cart_type'] = $input['cart_type'];
                    $cart_product_attr['product_id'] = $input['product_id'];
                    $cart_product_attr['attribute_title'] = $value->attribute_title;
                    $cart_product_attr['attribute_title_ar'] = $value->attribute_title_ar;
                    $cart_product_attr['value'] = $value->value;
                    CartProductAttribute::create($cart_product_attr); //Product Attribute entry
                        }
                    }
                    

                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.CART_ERROR'), 'code' => 500]);
                    $response = Response::json($response_array, 500);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }

    /**Get Photography Studio  data from add to cart  data**/
    public function getPhotographyAddToCartData(Request $request) {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                $photography_pro = array(); 


                $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id','=',$input['product_id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,pro_disprice')->first();
                //get rose product  product 

                $category = Category::select('mc_id','mc_img', 'mc_name', 'mc_name_ar','address','address_ar', 'mc_status','city_id')->where('mc_id', '=', $cart_pro->getProduct[0]->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();
                
                if (!empty($cart) && !empty($cart_pro)) {

                    if (isset($cart_pro)) {
                    //get date(type) food product 
                        $attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=',$input['product_id'])->with('getProductServiceAttribute:id,services_id,attribute_title,attribute_title_ar')->with('getSubProductAttribute:id,services_id,attribute_title,attribute_title_ar')->first();

                        $cart_rental_service = CartProductRent::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $cart_pro->product_id)->first();
                        $photography_pro['location']  = $cart_pro->location;
                            $photography_pro['pro_id'] = $cart_pro->getProduct[0]->pro_id;
                            if ($input['language_type'] == 'en') {
                                if(!empty($category)){
                                   $photography_pro['category_name'] = $category->mc_name;
                                   $photography_pro['category_img'] = $category->mc_img;
                                   $photography_pro['address'] = $category->address; 
                                   $photography_pro['city_name'] = $category->getCity->ci_name; 
                                } 
                                
                                $photography_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title;
                                $photography_pro['sub_attribute_title'] = $attribute->getSubProductAttribute[0]->attribute_title;
                                $photography_pro['pro_title'] = $cart_pro->getProduct[0]->pro_title;
                                $photography_pro['pro_desc'] = $cart_pro->getProduct[0]->pro_desc;
                            } else {
                                if(!empty($category)){
                                   $photography_pro['category_name'] = $category->mc_name_ar;
                                   $photography_pro['category_img'] = $category->mc_img;
                                   $photography_pro['address'] = $category->address_ar; 
                                   $photography_pro['city_name'] = $category->getCity->ci_name_ar;
                                }
                                $photography_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title_ar;
                                $photography_pro['sub_attribute_title'] = $attribute->getSubProductAttribute[0]->attribute_title_ar;
                                $photography_pro['pro_title'] = $cart_pro->getProduct[0]->pro_title_ar;
                                $photography_pro['pro_desc'] = $cart_pro->getProduct[0]->pro_desc_ar;
                            }
                            
                            $photography_pro['pro_qty'] = $cart_pro->quantity;

                           if($cart_pro->getProduct[0]->pro_disprice=='' || $cart_pro->getProduct[0]->pro_disprice==0) {
                                 $photography_pro['pro_price'] = $cart_pro->getProduct[0]->pro_price;

                               } else {

                                  $photography_pro['pro_price'] = $cart_pro->getProduct[0]->pro_disprice;
                               }
                           

                            $photography_pro['pro_Img'] = $cart_pro->getProduct[0]->pro_Img;
                            $photography_pro['pro_status'] = $cart_pro->getProduct[0]->pro_status;
                            $photography_pro['total_price'] = $cart_pro->total_price;
                            $photography_pro['service_date'] = date("d-m-Y", strtotime($cart_rental_service->rental_date));
                            $photography_pro['service_time'] = $cart_rental_service->rental_time;
                     
                    }      
                    if ($input['language_type'] == 'en') {
                        
                        $product_attr = CartProductAttribute::select('id', 'product_id', 'attribute_title', 'value')->where('product_id', '=', $input['product_id'])->orderBy('attribute_title', 'ASC')->get(); //GET PRODUCT ATTRIBUTE
                        if(!empty($product_attr)){
                            $photography_pro['product_attribute'] = $product_attr;
                        } 
                        
                        
                    } else {

                        $product_attr = CartProductAttribute::select('id', 'product_id', 'attribute_title_ar', 'value')->where('product_id', '=', $input['product_id'])->orderBy('attribute_title', 'ASC')->get(); //GET PRODUCT ATTRIBUTE 

                        if(!empty($product_attr)){
                            $photography_pro['product_attribute'] = $product_attr;
                        } 
                        
                    }
                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'cart_id' => $cart->id,'photography_data' => $photography_pro);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'cart data not found & cart type must be special event', 'code' => 500]);
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }



    public function getRosesPackageInfo(Request $request) {

         $validator = Validator::make($request->all(), [
             'category_id' => 'required', //Main Category ID
             'subcategory_id' => 'required', //Sub Category ID
             'vendor_id' => 'required', //Vendor ID
             'language_type' => 'required|max:255',
             'token' => 'required'
         ]);

         $input = $request->all();
         if ($validator->fails()) {
             $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
             $response = Response::json($response_array);
         } else {

             try {

                 $User = Auth::user();
                 //GET MAIN CATEGORY
                 $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first();
                 //GET CLIENT REVIEW
                 $client_review = Category::get_category_review($input['vendor_id']);

                 if (!empty($category)) {

                     if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {

                         /* FOR ARABIC LANGUAGE */
                         //GET SUB CATEGORY
                         $sub_catg = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_status')->where('mc_id', '=', $input['subcategory_id'])->first();
                         //GET VENDOR DETAIL
                         $vendor = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();
                         //GET VENDOR DETAIL
                         $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'attribute_title as attribute_title_arr', 'parent', 'image', 'status')->where('services_id', '=', $input['subcategory_id'])->where('parent', '=', '0')->orderBy('attribute_title', 'ASC')->get();
                         if (!$menu->isEmpty()) {

                             foreach($menu as $key => $value) {
                                 $product = Products::select('pro_id','packege', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price','pro_disprice','pro_discount_percentage', 'pro_mc_id', 'attribute_id', 'option_id', 'pro_qty')->where('pro_mc_id', '=', $input['vendor_id'])->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10000);
                                 $value->product = $product;
                                foreach($product as $val) {
                                  if($val->packege == 'yes'){
                                    $packege = ProductPackage::where('shop_id','=',$val->pro_mc_id)->where('packege_id','=',$val->pro_id)->with('getProductPackage:pro_id,pro_title_ar as pro_title,pro_Img,pro_desc_ar as pro_desc,pro_price,attribute_id')->get();
                                   $val->product_packege =  $packege;
                                }   
                                $name = explode(',', $val->option_id);
                                
                            if($val->packege != 'yes'){  $name =array(25,26);  }

 
                                //get product option detail
                                $pro_Option = ProductOption::select('id', 'option_title_ar as option_title', 'button_type_id', 'services_id')->whereIn('id', $name)->where('status', 1)->with('getBoxType:id,option_title,type')->get();

                                $val->product_option = $pro_Option;
                                foreach($pro_Option as $option_val) {
                                    if($value->attribute_title_arr == 'Single'){
                                        $product_Option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title_ar as option_title', 'image', 'price', 'status')->where('product_option_id', '=', $option_val->id)->where('vandor_id', '=', $vendor->vendor_id)->where('status', '=', 1)->get();

                                    }else{
                                           
                                        $product_Option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title_ar as  option_title', 'image', 'price', 'status')->where('product_option_id', '=', $option_val->id)->where('product_id', '=', $val->pro_id)->get();

                                    }
                                     
                                $option_val->product_option_value = $product_Option_value;
                                } 

                                } 
                                    
                             }

                             $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'sub_category_id' => $sub_catg->mc_id, 'sub_category_name' => $sub_catg->mc_name_ar, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'menu' => $menu);
                         } else {
                             $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                             $response = Response::json($response_array);
                         }
                     } else {
                         /*
                         Language English
                         */
                         //GET SUB CATEGORY
                         $sub_catg = Category::select('mc_id', 'mc_name', 'mc_status')->where('mc_id', '=', $input['subcategory_id'])->first();
                         //GET VENDOR DETAIL
                         $vendor = Category::select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();


                         //GET VENDOR DETAIL
                         $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'parent', 'image', 'status')->where('services_id', '=', $input['subcategory_id'])->where('parent', '=', '0')->orderBy('attribute_title', 'ASC')->get();
                         if (!$menu->isEmpty()) {

                             foreach($menu as $key => $value) {
                                 $product = Products::select('pro_id','packege', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price','pro_disprice','pro_discount_percentage', 'pro_mc_id', 'attribute_id', 'option_id', 'pro_qty')->where('pro_mc_id', '=', $input['vendor_id'])->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10000);


                              //print_r($product);  
                                 $value->product = $product;
                             foreach($product as $val) {
                                if($val->packege == 'yes'){
 
                                    $packege = ProductPackage::where('shop_id','=',$val->pro_mc_id)->where('packege_id','=',$val->pro_id)->with('getProductPackage:pro_id,pro_title,pro_Img,pro_desc,pro_price,attribute_id')->get();
                                   $val->product_packege =  $packege;

                                
                                } 
                                  
                                $name = explode(',', $val->option_id);
                            if($val->packege != 'yes'){  $name =array(25,26);  }

 
                                //get product option detail
                                $pro_Option = ProductOption::select('id', 'option_title', 'button_type_id', 'services_id')->whereIn('id', $name)->where('status', 1)->with('getBoxType:id,option_title,type')->get();

                                $val->product_option = $pro_Option;
                                foreach($pro_Option as $option_val) {
                                    if($value->attribute_title == 'Single'){
                                        $product_Option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title', 'image', 'price', 'status')->where('product_option_id', '=', $option_val->id)->where('vandor_id', '=', $vendor->vendor_id)->where('status', '=', 1)->get();

                                    }else{
                                           
                                        $product_Option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title', 'image', 'price', 'status')->where('product_option_id', '=', $option_val->id)->where('product_id', '=', $val->pro_id)->get();

                                    }
                                    
                                
                                $option_val->product_option_value = $product_Option_value;
                                }

                                }    
                             }
 
                             $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'sub_category_id' => $sub_catg->mc_id, 'sub_category_name' => $sub_catg->mc_name, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'menu' => $menu);
                         } else {
                             $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                             $response = Response::json($response_array);
                         }
                     }

                     $response = Response::json($response_array);

                 } else {

                     $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                     $response = Response::json($response_array);

                 }
             } catch (ModelNotFoundException $e) {
                 $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                 $response = Response::json($response_array);
             }
         }

         return $response;

    } 
    //Set roses data in add to cart
    public function setRosesPackageAddToCart(Request $request) {

        $cart_pro=0;

        $validator = Validator::make($request->all(), [
            'product_id' => 'required',//Get product id
            'cart_type' => 'required',//Get cart type
            'cart_sub_type' => 'required',//Get cart type
            'attribute_id' => 'required',//Get attribute id
            'total_price' => 'required'
        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

                $User = Auth::user();                
                $product = Products::where('pro_id', '=', $input['product_id'])->first();
                if($product->pro_qty < $input['product_qty'] && $product->pro_qty > 0) {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.QUANTITY_ERROR').$product->pro_qty, 'code' => 200]);
                    $response = Response::json($response_array, 200);
                    return $response;

                }


                if (!empty($product)) {

                   
 

                    $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                    if (!empty($cart)) {
 
                     
                      $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->count();
                        
                        if($cart_pro!='') {


                 CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
 
                        $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        $cart_option = CartOption::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        $cart_option_value = CartOptionValue::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        $cart_package = CartProductPackage::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
 
                      $cart_pro =0;




/*
                           $cartquantity = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->first();

                           $quantity1 = $cartquantity->quantity;
                           $totalpricepro = $cartquantity->total_price;
                          // $proprice =  $cartquantity->price;

                           $inputquantity = $input['product_qty'];

                           $totalquantity =  $quantity1+$inputquantity;

                           $totalprice = $input['total_price'];

                           $totalproprice =($totalprice)+($totalpricepro); 



                         
                         CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])
                          ->update([
                            'quantity' => $totalquantity,
                            'total_price' =>  $totalproprice

                            ]);

*/




                      }

                    /*$cart_service_attribute = CartServiceAttribute::where('cart_type', '=', $input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();*/


                     $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                     


                        /*$cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        $cart_option = CartOption::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        $cart_option_value = CartOptionValue::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        $cart_package = CartProductPackage::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();*/
                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                     if($cart_pro==0) {
                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    
                    $cart_product_data['rose_package_type'] = $input['rose_package_type'];  

                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $input['product_id'];
                    if ($input['product_qty'] != '') {
                        $cart_product_data['quantity'] = $input['product_qty'];
                    } else {
                        $cart_product_data['quantity'] = '';
                    }
                    $cart_product_data['total_price'] = $input['total_price'];
                    $cart_product_data['status'] = 1;
                    $cart_product_data['shop_vendor_id'] = $category->parent_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['pro_price'] = $product->pro_price;
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 

                    $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first();
                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $input['cart_type'];
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['product_id'] = $input['product_id'];
                    $cart_attribute_data['attribute_id'] = $input['attribute_id'];
                    //$cart_attribute_data['attribute_title'] = $menu->attribute_title;
                    //$cart_attribute_data['attribute_title_ar'] = $menu->attribute_title_ar;
                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry*/
                    
                     
                  
                    if (count($input['product_option_value'])>0) {
                        $product_options = ProductOptionValue::select('product_option_id')->whereIn('id', $input['product_option_value'])->distinct()->get()->toArray();
                       //print_r($product_options);
                        //die;
                        $product_options_ids = array();
                        foreach($product_options as $key => $value) {
                            $product_options_ids[] = $value['product_option_id'];
                        }

                        foreach($product_options_ids as $key => $value) {
                            $cart_option_data = array();
                            $cart_option_data['cart_type'] = $input['cart_type'];
                            $cart_option_data['product_option_id'] = $value;
                            $cart_option_data['cart_id'] = $cart->id;
                            $cart_option_data['product_id'] = $input['product_id'];
                            CartOption::create($cart_option_data); //cart option entry
                        }

                        $product_options_value = ProductOptionValue::whereIn('id', $input['product_option_value'])->with('getProductOption')->get();
                        $total_price = array();
                        
                        foreach($product_options_value as $value) {
                            $cart_option_value = array();
                            $cart_option_value['cart_type'] = $input['cart_type'];
                            $cart_option_value['cart_id'] = $cart->id;
                            $cart_option_value['product_id'] = $input['product_id'];
                            $cart_option_value['product_option_id'] = $value->product_option_id;
                            $cart_option_value['product_option_value_id'] = $value->id;
                            $cart_option_value['value'] = $value->price;
                            $cart_option_value['status'] = 1;
                            $cart_option_value['option_title'] = $value->option_title;
                            //$cart_option_value['option_title_ar'] = $value->getProductOption->option_title_ar;
                            $cart_option_value['option_value_title'] = $value->getProductOption->option_title;
                            //$cart_option_value['option_value_title_ar'] = $value->option_title_ar;
                            $cart_option_value['price'] = $value->price;

                            
                        $cart_value = CartOptionValue::create($cart_option_value); //cart option value entry
                           /*$cart_product = CartProduct::where('product_id','=',$cart_value->product_id)->first();
                           $total_price['total_price'] = $cart_product->total_price + $cart_value->value;
                           $update_product = CartProduct::where('product_id','=',$cart_value->product_id)->update($total_price);*/

                        }
                    }

                    $packege = ProductPackage::select('id', 'shop_id', 'attribute_id', 'item_id', 'packege_id')->where('packege_id','=',$input['product_id'])->with('getProductPackage:pro_id,pro_title,pro_title_ar,pro_Img,pro_desc,pro_desc_ar,pro_price,attribute_id,pro_disprice')->get();
                    if(!empty($packege)){
                        $cart_package = array();
                        foreach ($packege as $key => $value) {
                            
                        $cart_package['cart_id'] = $cart->id;
                        $cart_package['cart_type'] = $input['cart_type'];
                        $cart_package['product_id'] = $input['product_id'];
                        $cart_package['package_id'] = $value->getProductPackage->pro_id;
                        $cart_package['pro_title'] = $value->getProductPackage->pro_title;
                        $cart_package['pro_title_ar'] = $value->getProductPackage->pro_title_ar;
                        $cart_package['pro_desc'] = $value->getProductPackage->pro_desc;
                        $cart_package['pro_desc_ar'] = $value->getProductPackage->pro_desc_ar;
                        $cart_package['pro_price'] = $value->getProductPackage->pro_price;
                        $cart_package['pro_disprice'] = $value->getProductPackage->pro_disprice;
                        $cart_package['pro_img'] = $value->getProductPackage->pro_Img;
                        CartProductPackage::create($cart_package); //Cart Service Entery entery
                        }
                        
                    }
                    

                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                  }  
                }  else {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.CART_ERROR'), 'code' => 500]);
                    $response = Response::json($response_array, 500);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }

    /**Get Roses Package  data from add to cart  data**/
    public function getRoseAddToCartData(Request $request) {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
        ]);
        $input = $request->all();


        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();

                $roses_pro = array(); 
               
                $cart_pro = CartProduct::where('cart_type', '=','occasion')->where('cart_id', '=', $cart->id)->where('product_id','=',$input['product_id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,pro_disprice')->first();
              //  print_r($cart_pro);die;

                //get rose product  product 

                $category = Category::select('mc_id','mc_img', 'mc_name', 'mc_name_ar','address','address_ar', 'mc_status','city_id')->where('mc_id', '=', $cart_pro->getProduct[0]->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();


                if (!empty($cart) && !empty($cart_pro)) {

                    if (isset($cart_pro)) {
                    //get date(type) food product 
                            $attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=',$input['product_id'])->with('getProductServiceAttribute:id,services_id,attribute_title,attribute_title_ar')->first();

                            $roses_pro['pro_id'] = $cart_pro->getProduct[0]->pro_id;


                            if ($input['language_type'] == 'en') {
                                
                                if(!empty($category)){
                                   $roses_pro['category_name'] = $category->mc_name;
                                   $roses_pro['category_img'] = $category->mc_img;
                                   $roses_pro['address'] = $category->address;
                                   $roses_pro['city_name'] = $category->getCity->ci_name; 
                                }

                                if(isset($attribute->getProductServiceAttribute) && count($attribute->getProductServiceAttribute)>=1){
                                   $roses_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title;  
                                }
                                else
                                {
                                     $roses_pro['attribute_title'] = '';
                                }
                               
                                $roses_pro['pro_title'] = $cart_pro->getProduct[0]->pro_title;
                                $roses_pro['pro_desc'] = $cart_pro->getProduct[0]->pro_desc;

                            } else {

                                if(!empty($category)){
                                   $roses_pro['category_name'] = $category->mc_name_ar;
                                   $roses_pro['category_img'] = $category->mc_img;
                                   $roses_pro['address'] = $category->address_ar;
                                   $roses_pro['city_name'] = $category->getCity->ci_name_ar;  
                                } 
                                 if(isset($attribute->getProductServiceAttribute) && count($attribute->getProductServiceAttribute)>=1){
                                $roses_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title_ar;
                            }
                            else
                            {
                                $roses_pro['attribute_title'] = '';
                            }
                                $roses_pro['pro_title'] = $cart_pro->getProduct[0]->pro_title_ar;
                                $roses_pro['pro_desc'] = $cart_pro->getProduct[0]->pro_desc_ar;
                            }
                           
                            $roses_pro['pro_qty'] = $cart_pro->quantity;
                            if($cart_pro->getProduct[0]->pro_disprice=='' || $cart_pro->getProduct[0]->pro_disprice==0) {
                                 $roses_pro['pro_price'] = $cart_pro->getProduct[0]->pro_price;

                               } else {

                                  $roses_pro['pro_price'] = $cart_pro->getProduct[0]->pro_disprice;
                               }

                            $roses_pro['pro_Img'] = $cart_pro->getProduct[0]->pro_Img;
                            $roses_pro['pro_status'] = $cart_pro->getProduct[0]->pro_status;
                            $roses_pro['total_price'] = $cart_pro->total_price;
                            $roses_pro['quantity'] = $cart_pro->getProduct[0]->pro_qty;
                     
                    } 
                    if ($input['language_type'] == 'en') {
                        $packege = CartProductPackage::select('package_id','pro_title','pro_desc','pro_price','pro_img','pro_disprice')->where('cart_type','=','occasion')->where('product_id','=',$input['product_id'])->get();
                        if(!empty($packege)){
                            $roses_pro['product_packege'] = $packege; 
                        }

                    }else{
                        $packege = CartProductPackage::select('package_id','pro_title_ar','pro_desc_ar','pro_price','pro_img','pro_disprice')->where('cart_type','=','occasion')->where('product_id','=',$input['product_id'])->get();
                        if(!empty($packege)){
                            $roses_pro['product_packege'] = $packege; 
                        }

                    }

                    $cart_option_value = CartOptionValue::where('product_id', '=', $input['product_id'])->where('cart_id', '=', $cart->id)->where('cart_type', '=', 'occasion')->with('getProductOption:id,vendor_id,services_id,option_title,option_title_ar,service_hour')->with('getProductOptionValue:id,vandor_id,product_id,product_option_id,option_title,option_title_ar,short_name,value,image,price,no_person,status')->get();
                   
                    $option_value_data = array();
                    if (isset($cart_option_value)) { //get service
                        foreach($cart_option_value as $key => $value) {
                            
                            if ($input['language_type'] == 'en') {
                                $option_value_data[$key]['option_title'] = $value->getProductOption->option_title;
                                $option_value_data[$key]['value_title'] = $value->getProductOptionValue[0]->option_title;
                            } else {
                                $option_value_data[$key]['option_title'] = $value->getProductOption->option_title_ar;
                                $option_value_data[$key]['value_title'] = $value->getProductOptionValue[0]->option_title_ar;
                            }

                            $option_value_data[$key]['price'] = $value->getProductOptionValue[0]->price;
                            $option_value_data[$key]['no_person'] = $value->getProductOptionValue[0]->no_person;
                            $option_value_data[$key]['status'] = $value->getProductOptionValue[0]->status;
                        }
                    }
                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'cart_id' => $cart->id,'roses_data' => $roses_pro,'roses_option_data' => $option_value_data);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'cart data not found & cart type must be special event', 'code' => 500]);
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }



    public function getSpecialEventInfo(Request $request) {

         $validator = Validator::make($request->all(), [
             'category_id' => 'required', //Main Category ID
             'subcategory_id' => 'required', //Sub Category ID
             'vendor_id' => 'required', //Vendor ID
             'language_type' => 'required|max:255',
             'token' => 'required'
         ]);

         $input = $request->all();
         if ($validator->fails()) {
             $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
             $response = Response::json($response_array);
         } else {

             try {

                 $User = Auth::user();
                 //GET MAIN CATEGORY
                 $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first();
                 //GET CLIENT REVIEW
                 $client_review = Category::get_category_review($input['vendor_id']);

                 if (!empty($category)) {

                     if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {

                         /* FOR ARABIC LANGUAGE */
                         //GET SUB CATEGORY
                         $sub_catg = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_status')->where('mc_id', '=', $input['subcategory_id'])->first();
                         //GET VENDOR DETAIL
                         $vendor = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();
                         //GET VENDOR DETAIL


                        $productC = Products::select( 'attribute_id')->where('pro_mc_id', '=', $input['vendor_id'])->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->count();
                        $CatArr =  array();
                        if($productC >=1)
                        {
                        $productCm = Products::select( 'attribute_id')->where('pro_mc_id', '=', $input['vendor_id'])->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->get();
                        foreach ($productCm as $key => $value) {
                        $attributid =  $value->attribute_id;
                        array_push($CatArr,  $attributid);
                        }
                        }
 
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'parent', 'image', 'status')->where('status', '=', 1)->where('services_id', '=', $input['vendor_id'])->whereIn('id', $CatArr)->orderBy('attribute_title', 'ASC')->get();
                         if (!$menu->isEmpty()) {

                             foreach($menu as $key => $value) {
                                 $product = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'pro_qty','pro_disprice','pro_discount_percentage')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(100000);
                                 $value->product = $product;
                             }

                             $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'sub_category_id' => $sub_catg->mc_id, 'sub_category_name' => $sub_catg->mc_name_ar, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'menu' => $menu);
                         } else {
                             $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                             $response = Response::json($response_array);
                         }
                     } else {
                         /*
                         Language English
                         */
                         //GET SUB CATEGORY
                         $sub_catg = Category::select('mc_id', 'mc_name', 'mc_status')->where('mc_id', '=', $input['subcategory_id'])->first();
                         //GET VENDOR DETAIL
                         $vendor = Category::select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                            $productC = Products::select( 'attribute_id')->where('pro_mc_id', '=', $input['vendor_id'])->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->count();
                        $CatArr =  array();
                        if($productC >=1)
                        {
                        $productCm = Products::select( 'attribute_id')->where('pro_mc_id', '=', $input['vendor_id'])->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->get();
                        foreach ($productCm as $key => $value) {
                        $attributid =  $value->attribute_id;
                        array_push($CatArr,  $attributid);
                        }
                        }
                         //GET VENDOR DETAIL
                         $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'parent', 'image', 'status')->where('status', '=', 1)->whereIn('id', $CatArr)->where('services_id', '=', $input['vendor_id'])->orderBy('attribute_title', 'ASC')->get();
                         if (!$menu->isEmpty()) {

                             foreach($menu as $key => $value) {
                                 $product = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'pro_qty','pro_disprice','pro_discount_percentage')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(100000);
                                 $value->product = $product;
                             }

                             $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'sub_category_id' => $sub_catg->mc_id, 'sub_category_name' => $sub_catg->mc_name, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'menu' => $menu);
                         } else {
                             $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                             $response = Response::json($response_array);
                         }
                     }

                     $response = Response::json($response_array);

                 } else {

                     $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                     $response = Response::json($response_array);

                 }
             } catch (ModelNotFoundException $e) {
                 $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                 $response = Response::json($response_array);
             }
         }

         return $response;

    } 

    //Set special event data in add to cart
    public function setSpecialEventAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required',//Get product id
            'cart_type' => 'required',//Get cart type
            'cart_sub_type' => 'required',//Get cart type
            'attribute_id' => 'required',//Get attribute id
            'total_price' => 'required'
        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

                $User = Auth::user();
                
                $product = Products::where('pro_id', '=', $input['product_id'])->first();

               if($product->pro_qty >= $input['product_qty']){
                    $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                    if (!empty($cart)) {
                        $cart_pro_qnty = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id','=', $input['product_id'])->first();
                    if(!empty($cart_pro_qnty)){
                        $qnty = $product->pro_qty + $cart_pro_qnty->quantity;
                       //Products::where('pro_id','=', $input['product_id'])->update(['pro_qty'=> $qnty]);
                    }
                       
                        $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        
                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $input['product_id'];
                    $cart_product_data['quantity'] = $input['product_qty'];
                    $cart_product_data['total_price'] = $input['total_price'];
                    $cart_product_data['status'] = 1;
                    $cart_product_data['shop_vendor_id'] = $category->parent_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['pro_price'] = $product->pro_price;
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                    $updated_pro = Products::where('pro_id', '=', $input['product_id'])->first();
                    $dta = $updated_pro->pro_qty - $input['product_qty'];
                    //Products::where('pro_id','=', $input['product_id'])->update(['pro_qty'=> $dta]);

                    $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first(); 
                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $input['cart_type'];
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['product_id'] = $input['product_id'];
                    $cart_attribute_data['attribute_id'] = $input['attribute_id'];
                    $cart_attribute_data['attribute_title'] = $menu->attribute_title;
                    $cart_attribute_data['attribute_title_ar'] = $menu->attribute_title_ar;
                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry*/
                    
                    

                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                 }
                
                else if ($product->pro_qty <= $input['product_qty'] && $product->pro_qty > 0)
                {
                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.QUANTITY_ERROR'). $product->pro_qty, 'code' => 200]);
                    $response = Response::json($response_array, 200);
                    

                } 
                else if ($product->pro_qty == 0) 
                {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.PRODUCT_ERROR'), 'code' => 500]);
                    $response = Response::json($response_array, 500);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }

    /**Get Special Event  data from add to cart  data**/
    public function getEventAddToCartData(Request $request) {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                $event_pro = array(); 
                $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('product_id', '=', $input['product_id'])->where('cart_id', '=', $cart->id)->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,pro_disprice')->first();

                $category = Category::select('mc_id','mc_img', 'mc_name', 'mc_name_ar','address','address_ar', 'mc_status','city_id')->where('mc_id', '=', $cart_pro->getProduct[0]->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();

                if (!empty($cart) && !empty($cart_pro)) {

                    if (isset($cart_pro)) { //get date(type) food product 
                            $attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=',$cart_pro->product_id)->with('getProductServiceAttribute:id,services_id,attribute_title,attribute_title_ar')->first();

                            $event_pro['pro_id'] = $cart_pro->getProduct[0]->pro_id;
                            if ($input['language_type'] == 'en') {

                                if(!empty($category)){
                                   $event_pro['category_name'] = $category->mc_name;
                                   $event_pro['category_img'] = $category->mc_img;
                                   $event_pro['address'] = $category->address;
                                   $event_pro['city_name'] = $category->getCity->ci_name; 
                                }
                                
                                $event_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title;
                                $event_pro['pro_title'] = $cart_pro->getProduct[0]->pro_title;
                                $event_pro['pro_desc'] = $cart_pro->getProduct[0]->pro_desc;
                                $event_pro['city_name'] = $category->getCity->ci_name; 
                            } else {

                                if(!empty($category)){
                                   $event_pro['category_name'] = $category->mc_name_ar;
                                   $event_pro['category_img'] = $category->mc_img;
                                   $event_pro['address'] = $category->address_ar;
                                   $event_pro['city_name'] = $category->getCity->ci_name_ar;  
                                } 
                                $event_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title_ar;
                                $event_pro['pro_title'] = $cart_pro->getProduct[0]->pro_title_ar;
                                $event_pro['pro_desc'] = $cart_pro->getProduct[0]->pro_desc_ar;
                            }
                            
                            $event_pro['pro_qty'] = $cart_pro->quantity;
                            //$event_pro['pro_price'] = $cart_pro->getProduct[0]->pro_price;

                            if($cart_pro->getProduct[0]->pro_disprice=='' || $cart_pro->getProduct[0]->pro_disprice==0) {
                                 $event_pro['pro_price'] = $cart_pro->getProduct[0]->pro_price;
                                $event_pro['pro_disprice'] = $cart_pro->getProduct[0]->pro_disprice;
                               } else {

                                  $event_pro['pro_price'] = $cart_pro->getProduct[0]->pro_price;
                                  $event_pro['pro_disprice'] = $cart_pro->getProduct[0]->pro_disprice;
                               }

                            


                            $event_pro['pro_Img'] = $cart_pro->getProduct[0]->pro_Img;
                            $event_pro['pro_status'] = $cart_pro->getProduct[0]->pro_status;
                            $event_pro['total_price'] = $cart_pro->total_price;
                    } 
                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'cart_id' => $cart->id,'event_data' => $event_pro,);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'cart data not found & cart type must be special event', 'code' => 500]);
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    public function getElectronicInvitations(Request $request) {

         $validator = Validator::make($request->all(), [
             'category_id' => 'required', //Main Category ID
             'subcategory_id' => 'required', //Sub Category ID
             'language_type' => 'required|max:255',
             'token' => 'required'
         ]);

         $input = $request->all();
         if ($validator->fails()) {
             $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
             $response = Response::json($response_array);
         } else {

             try {

                 $User = Auth::user();
                 //GET MAIN CATEGORY
                 $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first();
                 //GET CLIENT REVIEW
                 $client_review = Category::get_category_review($input['subcategory_id']);

                 if (!empty($category)) {

                     if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {

                         /* FOR ARABIC LANGUAGE */
                       
                         //GET VENDOR DETAIL
                         $vendor = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();
                         //GET VENDOR DETAIL
                         
                         if (!empty($vendor)) {

                             
                             $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'vendor_detail' => $vendor, 'client_comments' => $client_review);
                         } else {
                             $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                             $response = Response::json($response_array);
                         }
                     } else {
                         /*
                         Language English
                         */
                         
                         //GET VENDOR DETAIL
                         $vendor = Category::select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                        
                         
                         if (!empty($vendor)) {                             

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'vendor_detail' => $vendor, 'client_comments' => $client_review);
                         } else {
                             $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                             $response = Response::json($response_array);
                         }
                     }

                     $response = Response::json($response_array);

                 } else {

                     $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                     $response = Response::json($response_array);

                 }
             } catch (ModelNotFoundException $e) {
                 $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                 $response = Response::json($response_array);
             }
         }

         return $response;

    } 


    public function getDesignerCard(Request $request) {

        $product_id = $request->product_id;
        $invitation_type = $request->invitation_type;
        $occasion_id = $request->occasion_id;
        if($invitation_type == 1){
            $product = Products::select('pro_id','pro_title','pro_Img','pro_mc_id','pro_price','pro_status')->where('pro_title', '=', 'message')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $product_id)->get(); 

        } else if($invitation_type == 2){
           $product = Products::select('pro_id','pro_title','pro_Img','pro_mc_id','pro_price','pro_status')->where('pro_title', '=', 'email')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $product_id)->get();    


        }else if($invitation_type == 3){


$product = Products::select('pro_id','pro_title','pro_Img','pro_mc_id','pro_price','pro_status')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $product_id)->where('pro_id','=', $occasion_id)->get();            

           // $product = Products::select('pro_id','pro_title','pro_Img','pro_mc_id','pro_price','pro_status')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $product_id)->whereIn('pro_id',array('120','124','126','142','155'))->get();            

        }
        else if($invitation_type == 4){

           

            if($occasion_id !='')
            {
        $product =  Products::select('pro_id','pro_title','pro_Img','pro_mc_id','pro_price','pro_status')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $product_id)->where('pro_id','=', $occasion_id)->get(); 
            }
            else
            {
               $product =  Products::select('pro_id','pro_title','pro_Img','pro_mc_id','pro_price','pro_status')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $product_id)->get();  
            }

                      

        }
        
        $response_array = array('data' => ['success' => true,'code' => 200, 'product_price' => $product]);
        $response = Response::json($response_array);
        return $response;

    }

    //Set electronic invitations data in add to cart


 


    public function setElectronicInvitationAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required', //Get product id
            'cart_type' => 'required', //Get cart type
            'cart_sub_type' => 'required', //Get cart type
            'occasion_name' => 'required', //Get occasion name
            'occasion_type' => 'required', //Get occasion type
            'venue' => 'required', //Get venue
            'date' => 'required', //Get date
            'time' => 'required', //Get time
            'invitation_type' => 'required', //Get Invitation Type
            'no_of_invitees' =>   'required', // Get Total number of attendence
            'count_row' =>   'required', // Get Total number of attendence
            'total_price' =>   'required'
        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => trans($locale.'_mob_lang.INPUT_ERROR')]);
            $response = Response::json($response_array);
        } else {
             try {

            $User = Auth::user();
            $product = Category::where('mc_id', '=', $input['product_id'])->first();

            if (!empty($product)) {
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                if (!empty($cart)) {
                    $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('category_id', '=', $input['product_id'])->delete();

                    $cart_invitation = CartInvitation::where('cart_id', '=', $cart->id)->where('services_id', '=', $input['product_id'])->delete();

                } else {
                    $cart_data = array();
                    $cart_data['user_id'] = $User->cus_id;
                    $cart = Cart::create($cart_data); //cart entery
                }

                $cart_invitation_data = array();

                $cart_invitation_data['cart_type'] = $input['cart_type'];
                $cart_invitation_data['services_id'] = $input['product_id'];
                $cart_invitation_data['customer_id'] = $User->cus_id;
                $cart_invitation_data['cart_id'] = $cart->id;
                $cart_invitation_data['occasion_name'] = $input['occasion_name'];
                $cart_invitation_data['occasion_type'] = $input['occasion_type'];


 

                $Getprod = Products::where('pro_id',$input['occasion_type'])->first();
                $ProLabel = $Getprod->pro_label;
                $ProLabel_ar = $Getprod->pro_label_ar;
                $ProID = $Getprod->pro_id;
 
                $cart_invitation_data['venue'] = $input['venue'];
                $cart_invitation_data['date'] = date("Y-m-d", strtotime($input['date']));
                $cart_invitation_data['time'] = $input['time'];
                $cart_invitation_data['no_of_invitees'] = $input['count_row'];
                $cart_invitation_data['invitaion_mode'] = $input['invitation_type'];

                $cart_invitation_data['packge_id'] = $input['occasion_type'];

                if ($input['invitation_type'] == 3) {
                    $card = Products::select('pro_id', 'pro_title','pro_title_ar','pro_desc','pro_desc_ar', 'pro_Img', 'pro_mc_id', 'pro_price')->where('pro_id', '=', $input['invitation_card'])->first();
                    $cart_invitation_data['packge_id'] = $card->pro_id;
                    $cart_invitation_data['card_price'] = $card->pro_price * $input['count_row'];


                // }else{
                //     $card = Products::select('pro_id', 'pro_title','pro_title_ar','pro_desc','pro_desc_ar', 'pro_Img', 'pro_mc_id', 'pro_price')->where('pro_id', '=', $input['invitation_card'])->first();
                //     $cart_invitation_data['packge_id'] = $card->pro_id;
                //     $cart_invitation_data['card_price'] = $card->pro_price * $input['count_row'];
                //    
                // }


                 $cart_invitation_data['invitation_msg'] = $input['invitation_data'];

                $cart_invitation_data['total_member_invitation'] = $input['count_row'];
               

                $cart_invitation_data['packge_title'] = $card->pro_title;
                $cart_invitation_data['packge_title_ar'] = $card->pro_title_ar;
                $cart_invitation_data['packge_desc'] = $card->pro_desc;
                $cart_invitation_data['packge_desc_ar'] = $card->pro_desc_ar;
                $cart_invitation_data['packge_img'] = $card->pro_Img;
}



$Urls = url('/').$input['no_of_invitees'];
  
$cart_invitation_data['file_url'] = $Urls;


                $cart_invitation_data['status'] = 1;

                if(isset($input['invitation_data']) && $input['invitation_data']!='')
                {
                $cart_invitation_data['invitation_msg'] = $input['invitation_data'];
                }
                if(isset($input['count_row']) && $input['count_row']!='')
                {
                 $cart_invitation_data['total_member_invitation'] = $input['count_row'];   
                }
                 
                      

             
                $cart_invitation_data['pro_label'] = $Getprod->pro_label;
                $cart_invitation_data['pro_label_ar'] = $Getprod->pro_label_ar;



                $cart_product = CartInvitation::create($cart_invitation_data); //cart product attribute entry*/

                

                $total_price = array();
                if(isset($total_price)){
                    $total_price['total_price'] = $cart_product->card_price; 
                }
                
                $cart_product_data = array();


$Getprod = Products::where('pro_id',$input['occasion_type'])->first();           

$ProID = $Getprod->pro_id;

                 $cart_product_data['product_id'] = $ProID;
                $cart_product_data['merchant_id'] = $product->vendor_id;
                $cart_product_data['shop_id'] = $product->mc_id;
                $cart_product_data['shop_vendor_id'] = $product->parent_id;
                $cart_product_data['cart_type'] = $input['cart_type'];
                $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                $cart_product_data['cart_id'] = $cart->id;
                $cart_product_data['category_id'] = $input['product_id'];
                $cart_product_data['total_price'] = $input['total_price'];
                $cart_product_data['status'] = 1;
                $cart_product_data['pro_title'] = $product->mc_name;
                $cart_product_data['pro_title_ar'] = $product->mc_name_ar;
                $cart_product_data['pro_desc'] = $product->mc_discription;
                $cart_product_data['pro_desc_ar'] = $product->mc_discription_ar;
                $cart_product_data['pro_Img'] = $product->mc_img;
              


                $cart_product = CartProduct::create($cart_product_data); //cart product entry   

                $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                $response = Response::json($response_array, 200);
            } else {
                $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.CART_ERROR'), 'code' => 500]);
                $response = Response::json($response_array, 500);
            }

        } catch (ModelNotFoundException $e) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
            $response = Response::json($response_array, 203);
        }

            
        }

        return $response;

    }


    /**Get Invitation data from add to cart  data**/
    public function getInvitationAddToCartData(Request $request) {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
        ]); 
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else { 
            try {  
                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
 
                $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('category_id', '=',$input['product_id'])->where('cart_id', '=', $cart->id)->first();

                if (!empty($cart) && !empty($cart_pro)) {

                
                $invitation_data = array();
                $cart_invitation = CartInvitation::where('services_id', '=', $cart_pro->category_id)->where('cart_id', '=', $cart->id)->with('getInvitaionCategory:mc_id,mc_img,mc_name,mc_name_ar,address,address_ar')->with('getInvitaionCard:pro_id,pro_title,pro_title_ar,pro_desc,pro_desc_ar,pro_Img,pro_price,pro_status')->first();
                // get first Category attribute data from add to cart
               
                if (isset($cart_invitation)) {

                        if ($input['language_type'] == 'en') {
                            $invitation_data['category_name'] = $cart_invitation->getInvitaionCategory->mc_name;
                            $invitation_data['category_img'] = $cart_invitation->getInvitaionCategory->mc_img;
                            $invitation_data['address'] = $cart_invitation->getInvitaionCategory->address; 
                             $invitation_data['label'] = $cart_invitation->pro_label; 
                        } else {
                            $invitation_data['category_name'] = $cart_invitation->getInvitaionCategory->mc_name_ar;
                            $invitation_data['category_img'] = $cart_invitation->getInvitaionCategory->mc_img;
                            $invitation_data['address'] = $cart_invitation->getInvitaionCategory->address_ar;
                            $invitation_data['label'] = $cart_invitation->pro_label_ar;  
                        }

                        $invitation_data['occasion_name'] = $cart_invitation->occasion_name;                 
                        $invitation_data['occasion_type'] = $cart_invitation->occasion_type;
                        $invitation_data['venue'] = $cart_invitation->venue;
                        $invitation_data['date'] = date("d-m-Y", strtotime($cart_invitation->date));
                        $invitation_data['time'] = $cart_invitation->time;
                        $invitation_data['no_of_invitees'] = $cart_invitation->no_of_invitees;                                         
                        $invitation_data['invitation_type'] = $cart_invitation->invitaion_mode;
                        if(isset($cart_invitation->getInvitaionCard) && $cart_invitation->getInvitaionCard!='')
                        {
                        $invitation_data['pro_id'] = $cart_invitation->getInvitaionCard->pro_id;   

                        if ($input['language_type'] == 'en') {

                        $invitation_data['pro_title'] = $cart_invitation->getInvitaionCard->pro_title;
                        $invitation_data['pro_desc'] = $cart_invitation->getInvitaionCard->pro_desc;
                        } else {

                        $invitation_data['pro_title'] = $cart_invitation->getInvitaionCard->pro_title_ar;
                        $invitation_data['pro_desc'] = $cart_invitation->getInvitaionCard->pro_desc_ar;
                        }

                        $invitation_data['pro_Img'] = $cart_invitation->getInvitaionCard->pro_Img;
                        $invitation_data['pro_status'] = $cart_invitation->getInvitaionCard->pro_status;
                        $invitation_data['pro_price'] = $cart_invitation->getInvitaionCard->pro_price;
                        }
                        else
                        {
                        $invitation_data['pro_id'] = '';
                        $invitation_data['pro_title'] = '';
                        $invitation_data['pro_desc'] = '';
                        $invitation_data['pro_Img'] = '';
                        $invitation_data['pro_status'] = 1;
                        $invitation_data['pro_price'] = '';
                        }

             
                            
                           
                            $invitation_data['total_member_invitation'] = $cart_invitation->total_member_invitation;
                           
                            $invitation_data['total_price'] = $cart_pro->total_price;
                            $invitation_data['invitation_msg'] = $cart_invitation->invitation_msg;
                              
                        
                     
                } 


                
                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'cart_id' => $cart->id,'invitation_data' => $invitation_data,);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'cart data not found & cart type must be Invitation ', 'code' => 500]);
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }
    /**Get Reception And Hospitality data info**/
    public function getReceptionAndHospitalityInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id' => 'required', //Vendor ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {
                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first();
                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($input['vendor_id']);

                if (!empty($category)) {

                    if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                        //GET SUB CATEGORY
                        $sub_catg = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_status')->where('mc_id', '=', $input['subcategory_id'])->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();
                        //GET VENDOR DETAIL
                        $data = array('dsign_your_package' => 'Dsign Your Package',
                            'packages' => 'Packages'
                        );
                        if (!empty($data)) {
                            foreach($data as $key => $cat_menu) {
                                if ($cat_menu == 'Dsign Your Package') {
                                    $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'parent', 'image', 'status')->where('status', '=', 1)->where('services_id', '=', $input['vendor_id'])->orderBy('attribute_title', 'ASC')->get();
                                    if (!$menu->isEmpty()) {
                                        foreach($menu as $key => $value) {
                                            //get staff price
                                            $staff_price = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'value', 'status')->where('status', '=', 1)->where('services_id', '=', $input['vendor_id'])->where('attribute_title', '=', 'Worker Price')->get();
                                            $value->staff_price = $staff_price;
                                            //get staff nationality
                                            $staff_nationality = StaffNationality::select('id', 'vendor_id', 'shop_id', 'nation_id','worker_price')->where('shop_id', '=', $input['vendor_id'])->with('getNationality:co_id,co_name_ar as co_name')->get();
                                            $value->staff_nationality = $staff_nationality;

                                            //get product detail
                                            $product = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price','pro_discount_percentage','pro_disprice', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour', 'no_of_staff', 'staff_nationality','packege','pro_qty')->where('packege', '=', 'no')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->with('getNationality:co_id,co_name_ar')->orderBy('pro_title', 'ASC')->paginate(9);
                                            $value->product = $product;
                                        }

                                    }
                                } else if ($cat_menu == 'Packages') {
                                    $product = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price', 'pro_discount_percentage','pro_disprice', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour', 'packege', 'no_of_staff', 'staff_nationality','pro_qty')->where('packege', '=', 'yes')->where('pro_mc_id', '=', $input['vendor_id'])->where('pro_status', '=', 1)->with('getNationality:co_id,co_name_ar')->orderBy('pro_title', 'ASC')->paginate(9);
                                    if (!$product->isEmpty()) {
                                        foreach($product as $pro_val) {
                                            if ($pro_val->packege == 'yes') {
                                                $packege = ProductPackage::where('shop_id', '=', $pro_val->pro_mc_id)->where('packege_id', '=', $pro_val->pro_id)->with('getServiceAttribute:id,services_id,attribute_title_ar')->with('getProductPackage:pro_id,pro_title_ar as pro_title,pro_Img,pro_desc_ar as pro_desc,pro_price,attribute_id')->get();
                                                $pro_val->product_packege = $packege;
                                            }
                                        }
                                    }
                                }
                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'sub_category_id' => $sub_catg->mc_id, 'sub_category_name' => $sub_catg->mc_name_ar, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'design_your_packege' => $menu, 'packege' => $product);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                        //GET SUB CATEGORY
                        $sub_catg = Category::select('mc_id', 'mc_name', 'mc_status')->where('mc_id', '=', $input['subcategory_id'])->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                        //GET VENDOR DETAIL
                        $data = array('dsign_your_package' => 'Dsign Your Package',
                            'packages' => 'Packages'
                        );
                        if (!empty($data)) {
                            foreach($data as $key => $cat_menu) {
                                if ($cat_menu == 'Dsign Your Package') {
                                    $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'parent', 'image', 'status')->where('status', '=', 1)->where('services_id', '=', $input['vendor_id'])->orderBy('attribute_title', 'ASC')->get();
                                    if (!$menu->isEmpty()) {
                                        foreach($menu as $key => $value) {
                                            //get staff price
                                            $staff_price = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'value', 'status')->where('status', '=', 1)->where('services_id', '=', $input['vendor_id'])->where('attribute_title', '=', 'Worker Price')->get();
                                            $value->staff_price = $staff_price;
                                            //get staff nationality
                                            $staff_nationality = StaffNationality::select('id', 'vendor_id', 'shop_id', 'nation_id','worker_price')->where('shop_id', '=', $input['vendor_id'])->with('getNationality:co_id,co_name')->get();
                                            $value->staff_nationality = $staff_nationality;

                                            //get product detail
                                            $product = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price','pro_discount_percentage','pro_disprice', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour', 'no_of_staff', 'staff_nationality','packege','pro_qty')->where('packege', '=', 'no')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->with('getNationality:co_id,co_name')->orderBy('pro_title', 'ASC')->paginate(9);
                                            $value->product = $product;
                                        }

                                    }
                                } else if ($cat_menu == 'Packages') {
                                    $product = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price','pro_discount_percentage','pro_disprice', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour', 'packege', 'no_of_staff', 'staff_nationality','pro_qty')->where('packege', '=', 'yes')->where('pro_mc_id', '=', $input['vendor_id'])->where('pro_status', '=', 1)->with('getNationality:co_id,co_name')->orderBy('pro_title', 'ASC')->paginate(9);
                                    if (!$product->isEmpty()) {
                                        foreach($product as $pro_val) {
                                            if ($pro_val->packege == 'yes') {
                                                $packege = ProductPackage::where('shop_id', '=', $pro_val->pro_mc_id)->where('packege_id', '=', $pro_val->pro_id)->with('getServiceAttribute:id,services_id,attribute_title')->with('getProductPackage:pro_id,pro_title,pro_Img,pro_desc,pro_price,attribute_id')->get();
                                                $pro_val->product_packege = $packege;
                                            }
                                        }
                                    }
                                }
                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'sub_category_id' => $sub_catg->mc_id, 'sub_category_name' => $sub_catg->mc_name, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'design_your_packege' => $menu, 'packege' => $product);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Set Reception and Hospitality Data in add to Cart
    public function setHospitalityAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required',//Get product id
            'cart_type' => 'required',//Get cart type
            'cart_sub_type' => 'required',//Get cart sub type
            'attribute_id' => 'required',//Get attribute id
            'total_price' => 'required',
            //'location'   => 'required'
         ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();

      

           

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                
                $User = Auth::user();
           
                $catg_pro = Products::where('pro_mc_id', '=', $input['product_id'])->first();
                if (!empty($catg_pro)) {
                    $product = Category::where('mc_id', '=', $input['product_id'])->first();
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();

                    if (!empty($cart)) {
                        $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('category_id', '=', $input['product_id'])->delete();

                        $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('category_id', '=', $input['product_id'])->delete();
                        $cart_hospitality = CartHospitality::where('cart_id', '=', $cart->id)->where('category_id', '=', $input['product_id'])->delete();
                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                    
 
                   
                    $cart_attribute_data = array();

                  
                    if (isset($input['products'])) {

                        $quantity = array();
                        $quantity =  $input['quantity'];
                       
                            $K=1;
                            foreach($input['products'] as $key => $value) {
                               // $quantity =  $input['quantity'][$key];
                             $Mainproid= $value;

                            $hos_product = Products::where('pro_id', '=', $value)->first();
                              $num_of_quant = $quantity[$key];

                         $menucount = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $hos_product->attribute_id)->count();

                          if($menucount > 1) {
                       $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $hos_product->attribute_id)->first();

                          }
                        
                           /* $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $hos_product->attribute_id)->first();*/
                              

                                $cart_attribute_data['cart_id'] = $cart->id;
                                $cart_attribute_data['cart_type'] = $input['cart_type'];
                                $cart_attribute_data['category_id'] = $input['product_id'];
                                $cart_attribute_data['attribute_id'] =  $input['attribute_id'];
                                $cart_attribute_data['sub_attribute_id'] = $hos_product->attribute_id;
                                $cart_attribute_data['product_id'] = $hos_product->pro_id;
                                if($hos_product->pro_disprice >=1)
                                {
                                $cart_attribute_data['price'] = $hos_product->pro_disprice;
                                }
                                else
                                {
                                $cart_attribute_data['price'] = $hos_product->pro_price;  
                                }


                                
                                $cart_attribute_data['attribute_title'] = 'Design Your Package';
                                $cart_attribute_data['attribute_title_ar'] = 'تصميم الحزمة الخاصة بك';
                                if(isset($menu->attribute_title) && $menu->attribute_title!='') { 
                                $cart_attribute_data['sub_attribute_title'] = $menu->attribute_title;
                                }
                                if(isset($menu->sub_attribute_title_ar) && $menu->sub_attribute_title_ar!='') {  
                                $cart_attribute_data['sub_attribute_title_ar'] = $menu->attribute_title_ar;
                              }

                                $cart_attribute_data['pro_title'] = $hos_product->pro_title;
                                $cart_attribute_data['pro_title_ar'] = $hos_product->pro_title_ar;
                                $cart_attribute_data['pro_desc'] = $hos_product->pro_desc;
                                $cart_attribute_data['pro_desc_ar'] = $hos_product->pro_desc_ar;
                                $cart_attribute_data['pro_img'] = $hos_product->pro_Img;
                                $cart_attribute_data['quantity'] = $num_of_quant;


                            $cart_attribute_data['shop_id'] =$hos_product->pro_mc_id;
                            $cart_attribute_data['total_price'] = $num_of_quant * $cart_attribute_data['price'];
                            $cart_attribute_data['shop_vendor_id'] =$hos_product->pro_mr_id;

                            
                            if(isset($input['worker_price']) && $input['worker_price']!='')
                            {
                              $cart_attribute_data['worker_price'] = $input['worker_price'];  
                            }
                            
                          //  $cart_attribute_data['cart_product_id'] = $num_of_quant;
 

                               $cart_value =  CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry*/
                         
                    
                    $cart_hospitality_data = array();
                    $cart_hospitality_data['cart_id'] = $cart->id;
                    $cart_hospitality_data['category_id'] = $input['product_id'];
                    $cart_hospitality_data['no_of_staff'] = $input['no_of_staff'];
                    $cart_hospitality_data['nationality'] = $input['nationality'];
                    $cart_hospitality_data['date'] = date("Y-m-d", strtotime($input['date']));
                    $cart_hospitality_data['time'] = $input['time'];
                    $cart_hospitality_data['elocation'] = $input['location'];
                    $cart_hospitality_data['product_id'] = $hos_product->pro_id;

                    $cart_hospitality_data['order_type'] = 'Design Your Package';
                    if($K==1){

                    $cart_hospitality_data = CartHospitality::create($cart_hospitality_data); //cart hospitality data entry 
                    }
                    $staff_price = ServiceAttribute::select('id', 'services_id','attribute_title', 'value', 'status')->where('status', '=', 1)->where('services_id', '=', $input['product_id'])->where('attribute_title', '=', 'Worker Price')->first(); 
                    $cart_pro_value = CartServiceAttribute::where('category_id','=',$input['product_id'])->get();
                    
                    $pro_price = 0;
                    foreach ($cart_pro_value as $key => $value) {
                        $pro_price += $value->price;
                    }
                     
                     //$exp  = explode(',',$input['products']);


                     
                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $product->vendor_id;
                    $cart_product_data['shop_id'] = $product->mc_id;
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['category_id'] = $product->mc_id;
                    /*$cart_product_data['total_price'] = $pro_price + $staff_price->value * $input['no_of_staff'];*/
                    $cart_product_data['total_price'] = $input['total_price'];

                    if(isset($input['worker_price']) && $input['worker_price']!='')
                    {
                         $cart_product_data['worker_price'] =$input['worker_price'];
                    }
                   

                    $cart_product_data['status'] = 1;
                    $cart_product_data['shop_vendor_id'] = $product->parent_id;
                    $cart_product_data['pro_title'] = $product->mc_name;
                    $cart_product_data['pro_title_ar'] = $product->mc_name_ar;
                    $cart_product_data['pro_desc'] = $product->mc_discription;
                    $cart_product_data['pro_desc_ar'] = $product->mc_discription_ar;
                    $cart_product_data['pro_Img'] = $product->mc_img;
                    if($input['location']!=''){
                    $cart_product_data['location'] =  $input['location'];
                    } else {
                    $cart_product_data['location'] =  '';

                    }
                    $cart_product_data['buy_rent'] = 'design';
                    $cart_product_data['product_id'] = $Mainproid;
                    if($K==1){
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                    $cartIDS =$cart_product->id;
                    CartServiceAttribute::where('cart_id',$cart->id)->where('attribute_title','Design Your Package')->update(['cart_product_id'=>$cartIDS]);
                    }
                     
                    $K = $K+1;           
                   
   
                            }
                        } 
                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                } else {
                    $product = Products::where('pro_id', '=', $input['product_id'])->first();

                if (!empty($product)) {
                    $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                    if (!empty($cart)) {
                        $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        $cart_package = CartProductPackage::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $input['product_id'];
                    $cart_product_data['total_price'] = $input['total_price'];
                    $cart_product_data['status'] = 1;
                    $cart_product_data['shop_vendor_id'] = $category->parent_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['pro_price'] = $product->pro_price;
                    $cart_product_data['location'] =  $input['location'];
                    $cart_product_data['buy_rent'] = 'package';
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                    $CartProID = $cart_product->id;
                    $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first();

                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $input['cart_type'];
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['product_id'] = $input['product_id'];    
                    $cart_attribute_data['attribute_id'] = $input['attribute_id'];
                    $cart_attribute_data['attribute_title'] = 'Package';
                    $cart_attribute_data['attribute_title_ar'] = 'صفقة';
                   

                    if(isset($input['quantity']) && $input['quantity']!='')
                    {
                        $quantity =  $input['quantity'];
                    }
                    else
                    {
                        $quantity =  1;
                    }
                    

                    $cart_attribute_data['shop_id'] = $category->mc_id;
                    $cart_attribute_data['total_price'] = $input['total_price'];
                    $cart_attribute_data['shop_vendor_id'] = $category->vendor_id;                        
                    $cart_attribute_data['cart_product_id'] = $CartProID;
                    $cart_attribute_data['pro_title'] = $product->pro_title;
                    $cart_attribute_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_attribute_data['quantity'] = $quantity;
                    $cart_attribute_data['price'] = ($input['total_price'] / $quantity);
 
                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry*/ 

                    $packege = ProductPackage::select('id', 'shop_id', 'attribute_id', 'item_id', 'packege_id')->where('packege_id','=',$input['product_id'])->with('getProductPackage:pro_id,pro_title,pro_title_ar,pro_Img,pro_desc,pro_desc_ar,pro_price,attribute_id')->get();
                    if(!empty($packege)){
                        $cart_package = array();
                        foreach ($packege as $key => $value) {
                            
                        $cart_package['cart_id'] = $cart->id;
                        $cart_package['cart_type'] = $input['cart_type'];
                        $cart_package['product_id'] = $input['product_id'];
                        $cart_package['package_id'] = $value->getProductPackage->pro_id;
                        $cart_package['pro_title'] = $value->getProductPackage->pro_title;
                        $cart_package['pro_title_ar'] = $value->getProductPackage->pro_title_ar;
                        $cart_package['pro_desc'] = $value->getProductPackage->pro_desc;
                        $cart_package['pro_desc_ar'] = $value->getProductPackage->pro_desc_ar;
                        $cart_package['pro_price'] = $value->getProductPackage->pro_price;
                        $cart_package['pro_img'] = $value->getProductPackage->pro_Img;
                        CartProductPackage::create($cart_package); //Cart Service Entery entery
                        }
                        
                    } 
                    
 
 
                    $cart_hospitality_data = array();
                    $cart_hospitality_data['cart_id'] = $cart->id;
                    $cart_hospitality_data['category_id'] = $category->mc_id;                  
                    $cart_hospitality_data['date'] = date("Y-m-d", strtotime($input['date']));
                    $cart_hospitality_data['time'] = $input['time'];
                    $cart_hospitality_data['elocation'] = $input['location'];
                    
                    $cart_hospitality_data['product_id'] = $product->pro_id;

                    $cart_hospitality_data['order_type'] = 'Package';

                  CartHospitality::create($cart_hospitality_data); //cart hospitality data entry 
 

                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.CART_ERROR'), 'code' => 500]);
                    $response = Response::json($response_array, 500);
                }
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }

    /**Get Reception and Hospitality  data from add to cart**/
    public function getHospitalityAddToCartData(Request $request) {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                $hospitality_pro = array();
                $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_sub_type', '=', 'hospitality')->where('cart_id', '=', $cart->id)->where('product_id','=',$input['product_id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_disprice,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,packege')->first();
 
                //get hospitality product

                if (!empty($cart) && !empty($cart_pro)) {
 
                    if (isset($cart_pro) ) {
                    //get hospitality product 
                        $attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=',$input['product_id'])->with('getProductServiceAttribute:id,services_id,attribute_title,attribute_title_ar')->with('getSubProductAttribute:id,services_id,attribute_title,attribute_title_ar')->first();

                        $category = Category::select('mc_id','mc_img', 'mc_name', 'mc_name_ar','address','address_ar', 'mc_status','city_id')->where('mc_id', '=', $cart_pro->getProduct[0]->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();

                            $hospitality_pro['pro_id'] = $cart_pro->getProduct[0]->pro_id;
                            if ($input['language_type'] == 'en') {

                                if(!empty($category)){
                                   $hospitality_pro['category_name'] = $category->mc_name;
                                   $hospitality_pro['category_img'] = $category->mc_img;
                                   $hospitality_pro['address'] = $category->address; 
                                   $hospitality_pro['city_name'] = $category->getCity->ci_name; 
                                }
                                
                                $hospitality_pro['attribute_title'] = 'Packages';
                                if(isset($cart_pro->getProduct[0]->pro_title)) {
                                $hospitality_pro['pro_title'] = $cart_pro->getProduct[0]->pro_title;
                               } else {
                                      $hospitality_pro['pro_title'] = '';
                               }
                                if(isset($cart_pro->getProduct[0]->pro_desc)) {
                                $hospitality_pro['pro_desc'] = $cart_pro->getProduct[0]->pro_desc;
                                 }else {
                                  $hospitality_pro['pro_desc'] ='';

                                 }
                               
                            } else {

                                if(!empty($category)){
                                   $hospitality_pro['category_name'] = $category->mc_name_ar;
                                   $hospitality_pro['category_img'] = $category->mc_img;
                                   $hospitality_pro['address'] = $category->address_ar; 
                                   $hospitality_pro['city_name'] = $category->getCity->ci_name_ar; 
                                }
                                $hospitality_pro['attribute_title'] = 'حزم';
                                if(isset($cart_pro->getProduct[0]->pro_title_ar)) {
                                $hospitality_pro['pro_title'] = $cart_pro->getProduct[0]->pro_title_ar;
                                } else {

                                      $hospitality_pro['pro_title'] ='';
                                }
                                 if(isset( $cart_pro->getProduct[0]->pro_desc_ar)) {
                                $hospitality_pro['pro_desc'] = $cart_pro->getProduct[0]->pro_desc_ar;
                              } else {
                                  $hospitality_pro['pro_desc'] ='';
                              }  
                                
                            }
                            
                            $hospitality_pro['pro_qty'] = $cart_pro->quantity;
                            if($cart_pro->quantity < 1)
                            {
                            $hospitality_pro['pro_qty'] = $attribute->quantity;
                            }



                            if(isset($cart_pro->getProduct[0]->pro_price)) {
                            $hospitality_pro['pro_price'] = $cart_pro->getProduct[0]->pro_price;
                             }else {
                                 $hospitality_pro['pro_price'] ='';

                             }
                            if(isset($cart_pro->getProduct[0]->pro_disprice) && $cart_pro->getProduct[0]->pro_disprice>=1) {
                             $hospitality_pro['pro_price'] = $cart_pro->getProduct[0]->pro_disprice;
                            }
                            $hospitality_pro['pro_Img'] = $cart_pro->getProduct[0]->pro_Img;
                            $hospitality_pro['pro_status'] = $cart_pro->getProduct[0]->pro_status;
                            $hospitality_pro['total_price'] = $cart_pro->total_price;


      $hosta = CartHospitality::where('product_id', '=',$input['product_id'])->where('cart_id', '=', $cart->id)->with('getNationality:co_id,co_name,co_name_ar')->first();




                             $hospitality_pro['location'] = $hosta->elocation; 
                            $hospitality_pro['date'] = date("d-m-Y", strtotime($hosta->date)); 
                            $hospitality_pro['time'] =$hosta->time; 


                        if ($input['language_type'] == 'en') {
                        $packege = CartProductPackage::select('package_id','pro_title','pro_desc','pro_price','pro_img')->where('cart_type','=','occasion')->where('product_id','=',$input['product_id'])->get();
                        if(!empty($packege)){
                            $hospitality_pro['product_packege'] = $packege; 
                        }

                    }else{
                        $packege = CartProductPackage::select('package_id','pro_title_ar','pro_desc_ar','pro_price','pro_img')->where('cart_type','=','occasion')->where('product_id','=',$input['product_id'])->get();
                        if(!empty($packege)){
                            $hospitality_pro['product_packege'] = $packege; 
                        }

                    } 
                    }
                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'cart_id' => $cart->id,'hospitality_data' => $hospitality_pro);
                } else {
                     $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_sub_type', '=', 'hospitality')->where('cart_id', '=', $cart->id)->where('category_id','=',$input['product_id'])->first();



                
                //get cosha product  product 
                    if (!empty($cart) && !empty($cart_pro)) {

                    if (isset($cart_pro)) {
                    //get cosha product with service attribute
                    $hospitality_pro = array(); 
                    $hospitaliy_branch = array();
                    $attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')
                    ->where('cart_id', '=', $cart->id)
                    ->where('category_id', '=',$input['product_id'])
                    ->with('getProductServiceAttribute:id,services_id,attribute_title,attribute_title_ar')
                    ->with('getSubProductAttribute:id,services_id,attribute_title,attribute_title_ar')
                    ->with('getProducts:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_disprice')
                    ->get();


                        foreach($attribute as $key => $value){
                            $hospitality_pro[$key]['pro_id'] = $value->getProducts[0]->pro_id;
                            if ($input['language_type'] == 'en') {
                                
                                $hospitality_pro[$key]['attribute_title'] = 'Design Your Package';
                                $hospitality_pro[$key]['sub_attribute_title'] = $value->getSubProductAttribute[0]->attribute_title;
                                $hospitality_pro[$key]['quantity'] = $value->getProductServiceAttribute[0]->quantity;
                               
                                $hospitality_pro[$key]['pro_title'] = $value->getProducts[0]->pro_title;
                                $hospitality_pro[$key]['pro_desc'] = $value->getProducts[0]->pro_desc;
                                $hospitality_pro[$key]['quantity'] = $value->quantity;
                               
                               
                            } else {
                                $hospitality_pro[$key]['attribute_title'] = 'تصميم الحزمة الخاصة بك';
                                $hospitality_pro[$key]['sub_attribute_title'] = $value->getSubProductAttribute[0]->attribute_title_ar;

                                $hospitality_pro[$key]['quantity'] = $value->quantity;
                               
                                $hospitality_pro[$key]['pro_title'] = $value->getProducts[0]->pro_title_ar;
                                $hospitality_pro[$key]['pro_desc'] = $value->getProducts[0]->pro_desc_ar;
                               

                            }
                            
                            $hospitality_pro[$key]['pro_qty'] = $value->getProducts[0]->pro_qty;
                            if($value->getProducts[0]->pro_disprice >=1)
                            {
                            $hospitality_pro[$key]['pro_price'] = $value->getProducts[0]->pro_disprice;    
                            }
                            else
                            {
                             $hospitality_pro[$key]['pro_price'] = $value->getProducts[0]->pro_price;   
                            }
 

                            
                            $hospitality_pro[$key]['pro_Img'] = $value->getProducts[0]->pro_Img;
                            $hospitality_pro[$key]['pro_status'] = $value->getProducts[0]->pro_status;
                                if($value->getProducts[0]->pro_disprice >=1)
                                {
                                $hospitality_pro[$key]['total_price'] = $value->getProducts[0]->pro_disprice;
                                }
                                else
                                {
                                $hospitality_pro[$key]['total_price'] = $value->getProducts[0]->pro_price;
                                }

                        }
                         $host = CartHospitality::where('category_id', '=',$input['product_id'])->where('cart_id', '=', $cart->id)->with('getNationality:co_id,co_name,co_name_ar')->first();

                         $nationalworkerid =  $host->nationality;
                        $shop_id = $cart_pro->shop_id;
                         $nationworkerprice =DB::table('nm_service_staff_nationality')->select('worker_price')->where('nation_id', $nationalworkerid)->where('shop_id', $shop_id)->first();
                         

                         
                        $category = Category::select('mc_id','mc_img', 'mc_name', 'mc_name_ar','address','address_ar', 'mc_status','city_id')->where('mc_id', '=', $input['product_id'])->with('getCity:ci_id,ci_name_ar,ci_name')->first();

                        $staff_price = ServiceAttribute::select('id', 'services_id','attribute_title', 'value', 'status')->where('status', '=', 1)->where('services_id', '=', $input['product_id'])->where('attribute_title', '=', 'Worker Price')->first();
                         
                            $hospitaliy_branch['category_id'] = $category->mc_id;
                        if ($input['language_type'] == 'en') {
                            if(!empty($category)){
                               $hospitaliy_branch['category_name'] = $category->mc_name;
                               $hospitaliy_branch['category_img'] = $category->mc_img;
                               $hospitaliy_branch['address'] = $category->address;
                               $hospitaliy_branch['city_name'] = $category->getCity->ci_name;  
                            }
                           $hospitaliy_branch['nationality'] = $host->getNationality->co_name; 
                           $hospitaliy_branch['location'] = $cart_pro->location; 
                           $hospitaliy_branch['worker_price'] =$nationworkerprice->worker_price; 
                           
                       }else{
                        if(!empty($category)){
                               $hospitaliy_branch['category_name'] = $category->mc_name_ar;
                               $hospitaliy_branch['category_img'] = $category->mc_img;
                               $hospitaliy_branch['address'] = $category->address_ar; 
                               $hospitaliy_branch['city_name'] = $category->getCity->ci_name_ar;
                            }
                         $hospitaliy_branch['nationality'] = $host->getNationality->co_name_ar; 
                         $hospitaliy_branch['location'] = $cart_pro->location; 
                         $hospitaliy_branch['worker_price'] =$nationworkerprice->worker_price; 
                            
                       }
                        $hospitaliy_branch['no_of_staff'] = $host->no_of_staff;
                        if(isset($staff_price->value) && $staff_price->value!='') {
                        $hospitaliy_branch['staff_price'] = $staff_price->value;
                         }

                        $hospitaliy_branch['date'] = date("d-m-Y", strtotime($host->date));
                        $hospitaliy_branch['time'] = $host->time;

                    }
                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'cart_id' => $cart->id,'hospitaliy_branch' => $hospitaliy_branch,'hospitality_data' => $hospitality_pro);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'cart data not found & cart type must be reception and hospitality', 'code' => 500]);
                }

                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    /**delete to add to cart  data**/
    public function deleteOccasionData(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'token' => 'required'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();

                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                $cart_pro = CartProduct::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->first();
                if (!empty($cart) && !empty($cart_pro)) {

                    $getInf = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->first(); 
                    $getContainerid  = $getInf->container_id;
                    $getContaineridd  = $getInf->id;
                    if( $getContainerid !='')
                    {
                    $getCID = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('container_id', '=', $getContainerid)->get(); 
                    $arr = array();
                    foreach ($getCID as $key => $value) {
                    $Ciid = $value->id;
                    array_push($arr, $Ciid);
                    }


                    $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('container_id', '=', $getContainerid)->delete();  
                    CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('attribute_title', '=', 'Design Your Kosha')->whereIn('cart_product_id',$arr)->delete();

                    }
                    

 
                    //delete product
                    $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    //delete attribute data
                  
                    $cart_pro_rent = CartProductRent::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    //delete car rent services

                    $cart_option = CartOption::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                    $cart_option_value = CartOptionValue::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    
                 CartHospitality::where('cart_id', '=', $cart->id)->where('category_id', '=', $input['product_id'])->delete();


                    $response_array = array('data' => ['success' => true, 'message' => 'Successfully deleted your cart data', 'code' => 200]);

                }else{
                    $cart_pro = CartProduct::where('category_id', '=', $input['product_id'])->where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->first();
                if (!empty($cart) && !empty($cart_pro)) {

                    $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('category_id', '=', $input['product_id'])->delete();

                    $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('category_id', '=', $input['product_id'])->delete();

                    $cart_hospitality = CartHospitality::where('cart_id', '=', $cart->id)->where('category_id', '=', $input['product_id'])->delete();

                    $cart_invitation = CartInvitation::where('cart_id', '=', $cart->id)->where('services_id', '=', $input['product_id'])->delete();

                    $response_array = array('data' => ['success' => true, 'message' => 'Successfully deleted your cart data', 'code' => 200]);

                }
            }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    public function uploadCSV(Request $request) {

        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'image' => 'required'
        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
               
                $upload_data = array();

                if (isset($input['image'])) //image upload functioning 
                {
                    $uploaded_thumb_rules = array(
                        'image' => 'required' //Get image
                    );

                    $thumbnail_validator = Validator::make($request->all(), $uploaded_thumb_rules);
                    if ($thumbnail_validator->passes()) {

                        $uploaded_partner_thumbnail = time().
                        '_'.$request->file('image')->getClientOriginalName();

                        $imageUploadSuccess = $request->file('image')->move(base_path().
                            '/public/assets/invitation/', $uploaded_partner_thumbnail);
                        if ($imageUploadSuccess) {
                            $input['image'] = '/public/assets/invitation/'.$uploaded_partner_thumbnail;

                            $upload_data['images'] = $input['image'];
                        }else {
                        $response_array = array('data' => ['success' => false, 'message' => 'Something went wrong please try again', 'code' => 500]);
                        $response = Response::json($response_array, 500);
                    }
                    } 
                }

                $file= url('/').$upload_data['images'];
                $linecount = 0;
                $Fopen = fopen($file, "r");
                $line = fgetcsv($Fopen);
                if(count($line) == 3 && count($line) >= 3)
                {
                    while(!feof($Fopen))
                    {
                        //$line = fgets($Fopen);
                        $line = fgetcsv($Fopen);
                        $linecount++;

                    } 
                    $count = $linecount+1;
                    $upload_data['images'] = $input['image'];
                    $upload_data['count_row'] = $count;

                    $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'data found'],'csv_file' => $upload_data);
 
                }else {
                        $response_array = array('data' => ['success' => false, 'code' => 200, 'message' => 'Something went wrong please check your format again']);
                    }

                $response = Response::json($response_array);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Himanshu

     public function checkBarCode(Request $request)
      {
          $validator = Validator::make($request->all(), [
            'barcodename' => 'required',
            
        ]); 
         
         $assign_vendor = $request->mer_id;
         $barcodename = $request->barcodename;
         $language = $request->lang; 
         $todayDate = date("Y-m-d"); 
         if($barcodename !='') { 
            $barcodedatacount =  InvitationList::where('assign_vendor',$assign_vendor)->where('barcode_value',$barcodename)->where('occasion_date','=',$todayDate)->where('status',0)->count();
             if($barcodedatacount>=1) {
              $barcodedata =  InvitationList::where('barcode_value',$barcodename)->where('assign_vendor',$assign_vendor)->first();
            InvitationList::where('barcode_value',$barcodename)->update(['status'=>1]);
                if($language =='en')
                {
                $response_array = array('data' => ['success' => true, 'message' => 'The bar code has been scanned successfully. The user is now valid for the entry.  Kindly permit him/her for the same.', 'code' => 200]);
                }
                else
                {
                $response_array = array('data' => ['success' => true, 'message' => 'تم مسح الرمز الشريطي بنجاح. المستخدم الآن صالحا للإدخال.  يرجى السماح له/لها لنفسه.', 'code' => 200]);        
                }


             } else {

                $barcodedatacounat =  InvitationList::where('assign_vendor',$assign_vendor)->where('barcode_value',$barcodename)->where('status',1)->count();
                if($barcodedatacounat >=1)
                {
                    if($language =='en')
                    {
                    $response_array = array('data' => ['success' => false, 'code' => 203, 'message' => 'The bar code has already been scanned. Please retry again or contact system admin']);
                    }
                    else
                    {
                    $response_array = array('data' => ['success' => false, 'code' => 203, 'message' => 'تم مسح الرمز الشريطي بالفعل. الرجاء أعاده المحاولة مره أخرى أو الاتصال بمسؤول النظام']);
                    } 
                }
                else
                {
                    if($language =='en')
                    {
                    //$response_array = array('data' => ['success' => false, 'code' => 203, 'message' => 'The bar code can\'t be verified right now due to an unknown/technical error.  Kindly contact the system admin. ']); 
                    $response_array = array('data' => ['success' => false, 'code' => 203, 'message' => 'you are not authorized merchant to access this bar code.  Kindly contact the system admin. ']); 
                    }
                    else
                    {
                    $response_array = array('data' => ['success' => false, 'code' => 203, 'message' => 'لا يمكن التحقق من الرمز الشريطي في الوقت الحالي بسبب خطا غير معروف/فني.  يرجى الاتصال بمشرف النظام. ']);    
                    }   
                }


               

             }

              return $response = $response_array;

         }

      }

      //Update quantity by Himanshu

       //Update Quantity
    public function updateQuantityAttributes(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'token' => 'required',
            'quantity'=>'required',
            'total_price'=>'required'
        ]);
        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
       
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();

               

                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
     
                $catid = Products::select('pro_mc_id','pro_qty','pro_id')->where('pro_id',$input['product_id'])->first();

                $productqty = $catid->pro_qty;
                 if ($productqty< $input['quantity'] && $productqty > 0 && $input['qtyact']==1) {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.QUANTITY_ERROR').$productqty, 'code' => 200]);
                    $response = Response::json($response_array, 200);

                } else {

                  $cart_pro =   CartServiceAttribute::where('cart_id', '=', $cart->id)->where('product_id','=', $input['product_id'])->update(['quantity'=> $input['quantity']]);
                 if($cart_pro!='') {

                    
            CartProduct::where('cart_id', '=', $cart->id)->where('category_id','=',$catid->pro_mc_id )->update(['total_price'=> $input['total_price']]);



                     $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.UPDATE_QUNATITY'), 'code' => 200]);
                 } } /*else {

                      $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.UPDATE_QUNATITY_ERROR'), 'code' => 203]);
                 }*/


                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.UPDATE_QUNATITY_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }
            
        }
         return $response;
    }




}
<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image; 
use Tymon\JWTAuth\JWT;
use Tymon\JWTAuth\JWTGuard;
use App\Occasion;
use App\OccasionImage;
use App\ProductReview;
use App\OrderProduct;
use App\SearchOccasion;
use App\BusinessOccasionType;
use App\OrderSingleProduct;
use App\Products;
use App\ServiceInquiry;
use App\InsuranceAmount;
use App\City;
use JWTAuth;
use App\User;
use Auth;
use Response;
use Carbon;
use File;
use DB;
use App\Category;
use Illuminate\Support\Facades\Mail;
use App\OrderExternalFoodDish;


class MyAccountController extends Controller {

    //Get User Profile Detail
    public function getProfileInfo(Request $request) {

        $id = Auth::id(); 
        $returnuser = User::select('cus_id','email','cus_name','cus_address1','cus_address2','email','cus_phone','ship_phone','gender','avatar','cus_pic','dob','cus_city','cus_country','cus_zipcode','bank_name','bank_account','map_url')
            ->where('cus_id','=',$id)
            ->with('getCountry:co_id,co_name')
            ->with('getCity:ci_id,ci_name')
            ->first();         
        

        $response_array = array('data' => ['success' => true,'code' => 200, 'profile' => $returnuser]);
        $response = Response::json($response_array);
        return $response;

    }



public function getInsurenceInfo(Request $request)
{


        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'language_type' => 'required'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();

                if ($input['language_type'] == 'ar') {
                    $orderlist = OrderProduct::where('order_cus_id', '=', $User->cus_id)->orderBy('created_at', 'DESC')->get();
                    if (!$orderlist->isEmpty()){
                         $insArry = array('hall','cosha','acoustic');
                        $insRentArry = array('abaya','dress');
                    //foreach ($orderlist as $key => $value) {
                        $occasionlist = SearchOccasion::where('user_id', '=', $User->cus_id)->where('order_id','!=', 'Null')->with('getCity:ci_id,ci_name_ar as ci_name')->with('getOccasionType:id,top_category,title_ar as title')->orderBy('created_at', 'DESC')->paginate(1005);
 
                         foreach($occasionlist as $val)
                        {   $OID = $val->order_id;
                        $getinsType = DB::table('nm_order_product')->where('order_id',$OID)->whereIn('product_sub_type',$insArry)->count();
                        if($getinsType >=1)
                        {
                        $val->has_insurence = 'yes';  
                        }
                        else
                        {
                            $getinsTypeR = DB::table('nm_order_product')->where('buy_rent','rent')->where('order_id',$OID)->whereIn('product_sub_type',$insRentArry)->count();
                            if($getinsTypeR >=1)
                            {
                            $val->has_insurence = 'yes'; 
                            }
                            else
                            {
                            $val->has_insurence = 'no'; 
                            }
                        }

                       $val->occasion_date = strtotime($val->created_at." GMT");

                        }
                   // }//GET OOCCASION LIST IN ARABIC
                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'تم العثور على البيانات'], 'occasion_list' => $occasionlist);

                    } else {
                        $response_array = array('data' => ['success' => false, 'message' => 'لم يتم العثور على بيانات.', 'code' => 200]);
                    }

                } else {

                    $orderlist = OrderProduct::where('order_cus_id', '=', $User->cus_id)->orderBy('created_at', 'DESC')->get();
                    if (!$orderlist->isEmpty()){
                    
                    $insArry = array('hall','cosha','acoustic');
                    $insRentArry = array('abaya','dress');

                    //foreach ($orderlist as $value) {
                        $occasionlist = SearchOccasion::where('user_id', '=', $User->cus_id)->where('order_id','!=', 'Null')->with('getCity:ci_id,ci_name')->with('getOccasionType:id,top_category,title')->orderBy('created_at', 'DESC')->paginate(1005);

                        foreach($occasionlist as $val)
                        { 
                        $OID = $val->order_id;
                        $getinsType = DB::table('nm_order_product')->where('order_id',$OID)->whereIn('product_sub_type',$insArry)->count();
                        if($getinsType >=1)
                        {
                        $val->has_insurence = 'yes';  
                        }
                        else
                        {

                        $getinsTypeR = DB::table('nm_order_product')->where('buy_rent','rent')->where('order_id',$OID)->whereIn('product_sub_type',$insRentArry)->count();
                        if($getinsTypeR >=1)
                        {
                        $val->has_insurence = 'yes'; 
                        }
                        else
                        {
                        $val->has_insurence = 'no'; 
                        }


                         
                        }

                         $val->occasion_date = strtotime($val->created_at." GMT");
                        }

                         

                   // } //GET OOCCASION LIST IN ENGLISH
                   
                  
                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'occasion_list' => $occasionlist);

                    } else {
                        $response_array = array('data' => ['success' => false, 'message' => 'data not found.', 'code' => 200]);
                    }

                }

                $response = Response::json($response_array);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

}





    //Get User's Occasion Detail
    public function getOccasionInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'language_type' => 'required'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();

                if ($input['language_type'] == 'ar') {
                    $orderlist = OrderProduct::where('order_cus_id', '=', $User->cus_id)->orderBy('created_at', 'DESC')->get();
                    if (!$orderlist->isEmpty()){
                    foreach ($orderlist as $key => $value) {
                        $occasionlist = SearchOccasion::where('user_id', '=', $User->cus_id)->where('order_id','!=', 'Null')->with('getCity:ci_id,ci_name_ar as ci_name')->with('getOccasionType:id,top_category,title_ar as title')->orderBy('created_at', 'DESC')->get();
                         foreach($occasionlist as $val)
                        {
                              $val->occasion_date = strtotime($val->created_at." GMT");


                          //$val->occasion_date = strtotime($val->created_at);

                        }
                    }//GET OOCCASION LIST IN ARABIC
                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'تم العثور على البيانات'], 'occasion_list' => $occasionlist);

                    } else {
                        $response_array = array('data' => ['success' => false, 'message' => 'لم يتم العثور على بيانات.', 'code' => 200]);
                    }

                } else {

                    $orderlist = OrderProduct::where('order_cus_id', '=', $User->cus_id)->orderBy('created_at', 'DESC')->get();
                    if (!$orderlist->isEmpty()){
                    foreach ($orderlist as $value) {
                        $occasionlist = SearchOccasion::where('user_id', '=', $User->cus_id)->where('order_id','!=', 'Null')->with('getCity:ci_id,ci_name')->with('getOccasionType:id,top_category,title')->orderBy('created_at', 'DESC')->get();

                        foreach($occasionlist as $val)
                        {
                          //$val->occasion_date = strtotime($val->created_at);  
                         $val->occasion_date = strtotime($val->created_at." GMT");
                        }

                         

                    } //GET OOCCASION LIST IN ENGLISH
                   
                   //print_r($occasionlist); 
                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'occasion_list' => $occasionlist);

                    } else {
                        $response_array = array('data' => ['success' => false, 'message' => 'data not found.', 'code' => 200]);
                    }

                }

                $response = Response::json($response_array);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Get User's Occasion Product Detail
    public function getOccasionProduct(Request $request) {

        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'language_type' => 'required',
            'order_id' => 'required'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();

              //  print_r($User); die;
                $notorders =  array();
                $shippinglist=array();

                $hall_pending_amount = 0.0;

                $totalshippingamount = 0;


// buffet case only- because we are displaying only branch in cart  //
                $artarrya = array(0); $arshop = array();
                $getBuffetCount =   OrderSingleProduct::where('product_sub_type','=','buffet')->where('order_id',  '=', $input['order_id'])->count();
                if($getBuffetCount >=1)
                {
                    $getBuffet  =   OrderSingleProduct::where('product_sub_type','=','buffet')->where('order_id', '=', $input['order_id'])->get(); 
                    foreach ($getBuffet as $getBuffetue) 
                    {
                        $Bshop_id = $getBuffetue->shop_id;
                        $Bid = $getBuffetue->id; 
                            if(in_array($Bshop_id, $arshop))
                            {
                            array_push($artarrya, $Bid);   
                            }
                        array_push($arshop, $Bshop_id);
                    }
 
 $getBuffetSum  =   OrderSingleProduct::where('product_sub_type','=','buffet')->where('order_id', '=', $input['order_id'])->groupBy('category_id')->get(array(DB::raw('SUM(total_price) as total'),'shop_id')); 
  foreach( $getBuffetSum as $vals)
  {
$shop_id = $vals->shop_id;
$total = $vals->total; 
 OrderSingleProduct::where('product_sub_type','=','buffet')->where('shop_id', '=', $shop_id)->where('order_id', '=', $input['order_id'])->update(['new_price'=>$total]);
  }
  
                }
// buffet case only- because we are displaying only branch in cart  //


 
 // KOSHA Check//

    $getKoshaOrdersa  =   OrderSingleProduct::where('product_sub_type','=','cosha')->where('order_id', '=', $input['order_id'])->where('quantity', '<=', 0)->count(); 
     if($getKoshaOrdersa >=1)
    {
    $getKoshaOrders  =   OrderSingleProduct::where('product_sub_type','=','cosha')->where('order_id', '=', $input['order_id'])->where('quantity', '<=', 0)->first(); 
     $Cats = $getKoshaOrders->category_id;
     $getKoshaOrdersaa  =   OrderSingleProduct::where('product_sub_type','=','cosha')->where('order_id', '=', $input['order_id'])->where('category_id', '=', $Cats)->where('quantity', '>', 0)->get(); 
    foreach ($getKoshaOrdersaa as $key => $value) {
     $Kid = $value->id;
    array_push($artarrya, $Kid); 
    }
 
    }
    
 // KOSHA Check//
   

                if ($input['language_type'] == 'ar') {
                    $orderlist = OrderProduct::where('order_cus_id', '=', $User->cus_id)->where('order_id', '=', $input['order_id'])->first();
                    if (!empty($orderlist)){





                        //get user's product detail
                    $productlist = OrderSingleProduct::where('cus_id', '=', $User->cus_id)->where('order_id', '=', $orderlist->order_id)->whereNotIn('id',  $artarrya)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_Img,pro_price,Insuranceamount,parent_attribute_id')->with('getCategory:mc_id,mc_name_ar as mc_name,mc_img')->orderBy('updated_at', 'DESC')->get();
                        $shopname = '';
                        $TAmt = 0;




                        foreach($productlist as $val) {
                           $catid = $val->shop_id;

                            $category_id = $val->category_id;
                                //display all related cosha products to cart //
                                $val->cosha_products = '';
                                $val->shippingid='';
                                $shippinglistcount=0;
                                $shippinglist=array();

                                $shippinglistcount=Db::table('nm_shipping')->where('ship_order_id',$orderlist->order_id)->where('cart_sub_type',$val->product_sub_type)->count();
                                if($shippinglistcount>=1){
                                    $shippinglist=Db::table('nm_shipping')->select('shipping_type','shipping_price')->where('ship_order_id',$orderlist->order_id)->where('cart_sub_type',$val->product_sub_type)->first();
                                 $totalshippingamount=$totalshippingamount+$shippinglist->shipping_price;
                                }
                               

                                $ChekCosha = DB::table('nm_order_services_attribute')->where('product_type','occasion')->where('attribute_title','Design Your Kosha')->where('category_id',$category_id)->where('order_id',$orderlist->order_id)->count();
                                if($ChekCosha >=1)
                                {

                                $ChekCoshaInfo = DB::table('nm_order_services_attribute')->where('product_type','occasion')->where('attribute_title','Design Your Kosha')->where('category_id',$category_id)->where('order_id',$orderlist->order_id)->get();  
                                $Title = '';
                                foreach ($ChekCoshaInfo as $key => $value) {
                                $Title = $value->pro_title_ar.','.$Title;
                                }
                                $val->cosha_products = rtrim($Title,',');
                                if($val->buy_rent !='design')
                                {
                                $val->cosha_products ='';  
                                }

                                } 
                                //display all related cosha products to cart //


                           $TAmt = $TAmt + $val->total_price;
                        if($val->product_sub_type=='buffet'){
                        $shop_id = $val->shop_id;  
                        $getexternalprod=OrderExternalFoodDish::select('id','external_food_dish_id')->where('order_id','=', $orderlist->order_id)->where('category_id','=', $shop_id)->with('getFood:pro_id,pro_title_ar as pro_title,pro_Img')->get();
                        $val->buffetProduct = $getexternalprod;
                        }

 $val->hospitality_products = '';
                    if($val->product_sub_type=='hospitality'){
                        $shop_id = $val->shop_id;  
                        $product_id = $val->product_id; 

                    $ChekHosInfox = DB::table('nm_order_services_attribute')->where('product_type','occasion')->where('attribute_title','Design Your Package')->where('category_id',$category_id)->where('order_id',$orderlist->order_id)->count(); 

                    if($ChekHosInfox >=1)
                    {
                    $ChekHosInfo = DB::table('nm_order_services_attribute')->where('product_type','occasion')->where('attribute_title','Design Your Package')->where('category_id',$category_id)->where('order_id',$orderlist->order_id)->get();  
                    $Hos_Title = '';
                    foreach ($ChekHosInfo as $key => $value) {
                    $Hos_Title = $value->pro_title_ar.','.$Hos_Title;
                    }
                    $val->hospitality_products = rtrim($Hos_Title,',');  
                    }
                     
                              
                        }

                         $val->invitation_products_info = array();
                          if($val->product_sub_type=='invitations'){
                                 $ChekinvInfox = DB::table('nm_order_invitation')->where('order_id',$orderlist->order_id)->count();
                                 if($ChekinvInfox>=1){
                                  $ChekinvInfo = DB::table('nm_order_invitation')->where('order_id',$orderlist->order_id)->get(); 
                                  $val->invitation_products_info=$ChekinvInfo;
                                 }
                          }


                           //$datacount = Category::where('mc_id',$catid)->count();

                        $productsubtype = $val->product_sub_type;
                        $musictype =0;
                         $MusicT = array('band','singer','recording');
                        if(in_array($productsubtype, $MusicT))
                        {
                        $datacount = DB::table('nm_music')->where('id',$catid)->count();
                        $musictype =1;
                        $val->product_type = 'music';
                        }
                        else
                        {
                        $datacount = Category::where('mc_id',$catid)->count();
                        }
 
                       if ($datacount) {
                        if($musictype == 0){ 
                        $data = Category::where('mc_id',$catid)->first();
                        $shopname = $data->mc_name_ar;
                        }else { 
                        $data = DB::table('nm_music')->where('id',$catid)->first();
                        $shopname = $data->name_ar;
                        }  
                        $val->shop_name = $shopname;
                        }

                        if($val->product_sub_type =='hall')
                        {
                              $hall_pending_amount = $hall_pending_amount + $val->total_price - $val->paid_total_amount;  
                        }
                      }
                      
                         $vat = $orderlist->order_taxAmt;
 
                            if($orderlist->order_amt < 1)
                            {
                             $total_paid_amount = $TAmt;
                            }
                            else
                            {
                               $total_paid_amount = $orderlist->order_amt;  
                            }
 
     
                        //$hall_pending_amount = $orderlist->hall_pending_amount;
                         

                        $order_paytype = $orderlist->order_paytype;
                        $order_id = $orderlist->order_id;
                        $vat = $orderlist->order_taxAmt;
                    //GET OOCCASION LIST IN ARABIC
                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'تم العثور على البيانات'], 'order_id' => $order_id, 'product_list' => $productlist, 'total_paid_amount' => $total_paid_amount, 'hall_pending_amount' => $hall_pending_amount, 'order_paytype' => $order_paytype, 'vat_amount' => $vat, 'shipping_amount'=>$totalshippingamount);

                    } else {
                        $response_array = array('data' => ['success' => false, 'message' => 'لم يتم العثور على بيانات.', 'code' => 200]);
                    }

                } else {
 
                    $orderlist = OrderProduct::where('order_cus_id', '=', $User->cus_id)->where('order_id', '=', $input['order_id'])->first();
                    if (!empty($orderlist)){


                    $productlist = OrderSingleProduct::where('cus_id', '=', $User->cus_id)->where('order_id', '=', $orderlist->order_id)->whereNotIn('id',  $artarrya)->with('getProduct:pro_id,pro_title,pro_Img,pro_price,Insuranceamount,parent_attribute_id')->with('getCategory:mc_id,mc_name,mc_img')->orderBy('updated_at', 'DESC')->get();



                        $TAmt = 0;
                        foreach($productlist as $val) {

                        $TAmt = $TAmt + $val->total_price;

                                $val->shippingid='';
                                $shippinglistcount=0;
                                $shippinglist=array();

                                $shippinglistcount=Db::table('nm_shipping')->where('ship_order_id',$orderlist->order_id)->where('cart_sub_type',$val->product_sub_type)->count();
                                if($shippinglistcount>=1){
                                    $shippinglist=Db::table('nm_shipping')->select('shipping_type','shipping_price')->where('ship_order_id',$orderlist->order_id)->where('cart_sub_type',$val->product_sub_type)->first();
                                    $totalshippingamount=$totalshippingamount+$shippinglist->shipping_price;
                                }
                                

 
                                $category_id = $val->category_id;
                                //display all related cosha products to cart //
                                $val->cosha_products = '';
                                $ChekCosha = DB::table('nm_order_services_attribute')->where('product_type','occasion')->where('attribute_title','Design Your Kosha')->where('category_id',$category_id)->where('order_id',$orderlist->order_id)->count();
                                if($ChekCosha >=1)
                                {

                                $ChekCoshaInfo = DB::table('nm_order_services_attribute')->where('product_type','occasion')->where('attribute_title','Design Your Kosha')->where('category_id',$category_id)->where('order_id',$orderlist->order_id)->get();  
                                $Title = '';
                                foreach ($ChekCoshaInfo as $key => $value) {
                                $Title = $value->pro_title.','.$Title;
                                }
                                $val->cosha_products = rtrim($Title,',');
                                if($val->buy_rent !='design')
                                {
                                $val->cosha_products ='';  
                                }
                                } 
                                //display all related cosha products to cart //
 
                        $val->hospitality_products = '';
                        if($val->product_sub_type=='hospitality'){
                        $shop_id = $val->shop_id;  
                        $product_id = $val->product_id; 

                    $ChekHosInfox = DB::table('nm_order_services_attribute')->where('product_type','occasion')->where('attribute_title','Design Your Package')->where('category_id',$category_id)->where('order_id',$orderlist->order_id)->count(); 
                    
                    if($ChekHosInfox >=1)
                    {
                    $ChekHosInfo = DB::table('nm_order_services_attribute')->where('product_type','occasion')->where('attribute_title','Design Your Package')->where('category_id',$category_id)->where('order_id',$orderlist->order_id)->get();  
                    $Hos_Title = '';
                    foreach ($ChekHosInfo as $key => $value) {
                    $Hos_Title = $value->pro_title.','.$Hos_Title;
                    }
                    $val->hospitality_products = rtrim($Hos_Title,',');  
                    }
                     
                              
                        }
                            $val->invitation_products_info = array();
                          if($val->product_sub_type=='invitations'){
                                 $ChekinvInfox = DB::table('nm_order_invitation')->where('order_id',$orderlist->order_id)->count();
                                 if($ChekinvInfox>=1){
                                  $ChekinvInfo = DB::table('nm_order_invitation')->where('order_id',$orderlist->order_id)->get(); 
                                  $val->invitation_products_info=$ChekinvInfo;
                                 }
                          }



                        if($val->product_sub_type=='buffet'){
                        $shop_id = $val->shop_id;  
                        $getexternalprod=OrderExternalFoodDish::select('id','external_food_dish_id')->where('order_id','=', $orderlist->order_id)->where('category_id','=', $shop_id)->with('getFood:pro_id,pro_title,pro_Img')->get();
                        $val->buffetProduct = $getexternalprod;
                        } 

                         $catid = $val->shop_id;
                         $productsubtype = $val->product_sub_type;
                         $musictype =0;
                         $MusicT = array('band','singer','recording');
                        if(in_array($productsubtype, $MusicT))
                        {
                        $datacount = DB::table('nm_music')->where('id',$catid)->count();
                        $musictype =1;
                         $val->product_type = 'music';
                        }
                        else
                        {
                        $datacount = Category::where('mc_id',$catid)->count();
                        }
             
                       
                       if ($datacount) {
                        if($musictype == 0){ 
                        $data = Category::where('mc_id',$catid)->first();
                         $shopname = $data->mc_name;
                        }else { 
                        $data = DB::table('nm_music')->where('id',$catid)->first();
                         $shopname = $data->name;
                        }         
                        $val->shop_name = $shopname;
                       }
                       
                        if($val->product_sub_type =='hall')
                        {
                              $hall_pending_amount = $hall_pending_amount + $val->total_price - $val->paid_total_amount;  
                        }
                      }
                      


                        $vat = $orderlist->order_taxAmt;
                         //$total_paid_amount = $orderlist->order_amt+$vat;
                         $total_paid_amount = $orderlist->order_amt;
                        //$hall_pending_amount = $orderlist->hall_pending_amount;
 
                      if($orderlist->order_amt < 1)
                      {
                    $total_paid_amount = $TAmt;

                      } 
                        
     

                        $order_paytype = $orderlist->order_paytype;
                        $order_id = $orderlist->order_id;//GET OOCCASION LIST IN ENGLISH
                   
                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'order_id' => $order_id, 'product_list' => $productlist, 'total_paid_amount' => $total_paid_amount, 'hall_pending_amount' => $hall_pending_amount, 'order_paytype' => $order_paytype, 'vat_amount' => $vat, 'shipping_amount'=>$totalshippingamount);

                    } else {
                        $response_array = array('data' => ['success' => false, 'message' => 'data not found.', 'code' => 200]);
                    }

                }

                $response = Response::json($response_array);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Get dropdown list of Occasion and cities 
    public function getOccasionType(Request $request) 
    {
        $business_occasion = BusinessOccasionType::select('id','title', 'title_ar')->orderBy('title', 'ASC')->get();
        $cities = City::select('ci_id','ci_name', 'ci_name_ar','ci_status')->orderBy('ci_name', 'ASC')->get();
        $response_array = array('data' => ['success' => true,'code' => 200, 'occasio_type' => $business_occasion, 'city' => $cities]);
        $response = Response::json($response_array);
        return $response;
    }
    //Get User's Photography detail
    public function getStudioInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'language_type' => 'required'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $business_occasion = BusinessOccasionType::select('id','title', 'title_ar')->orderBy('title', 'ASC')->get();
                $cities = City::select('ci_id','ci_name', 'ci_name_ar','ci_status')->orderBy('ci_name', 'ASC')->get();

                if ($input['language_type'] == 'ar') {

                if(!empty($input['city_id']))

                  {
                    $getImagelist = Occasion::select('id', 'occasion_name_ar as occasion_name', 'occasion_date', 'occasion_venue','images','city_id','posted_by','studio_date')->where('posted_by', '=', $User->cus_id)->where('city_id','=',$input['city_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->with('getImage')->orderBy('created_at', 'desc')->get(); //GET OOCCASION LIST IN ARABIC
                  }
                  else
                  {
                    $getImagelist = Occasion::select('id', 'occasion_name_ar as occasion_name', 'occasion_date', 'occasion_venue','images','city_id','posted_by','studio_date')->where('posted_by', '=', $User->cus_id)->with('getCity:ci_id,ci_name_ar as ci_name')->with('getImage')->orderBy('created_at', 'desc')->get();
                  }

                    
                    if (!$getImagelist->isEmpty()) {
                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'تم العثور على البيانات'], 'getImagelist' => $getImagelist);

                    } else {
                        $response_array = array('data' => ['success' => false, 'message' => 'لم يتم العثور على بيانات.', 'code' => 200]);
                    }

                } else {

                    if(!empty($input['city_id']))
                    {
                    $cityId = $_REQUEST['city_id'];
                    $getImagelist = Occasion::select('id', 'occasion_name', 'occasion_date', 'occasion_venue','images','city_id','posted_by','studio_date')->where('posted_by', '=', $User->cus_id)->where('city_id','=',$input['city_id'])->with('getCity:ci_id,ci_name')->with('getImage')->orderBy('created_at', 'desc')->get(); //GET OOCCASION LIST IN ARABIC
                    }
                    else
                    {
                    $getImagelist = Occasion::select('id', 'occasion_name', 'occasion_date', 'occasion_venue','images','city_id','posted_by','studio_date')->where('posted_by', '=', $User->cus_id)->with('getCity:ci_id,ci_name')->with('getImage')->orderBy('created_at', 'desc')->get();
                    }
                    if (!$getImagelist->isEmpty()) {
                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'getImagelist' => $getImagelist);

                    } else {
                        $response_array = array('data' => ['success' => false, 'message' => 'data not found.', 'code' => 200]);
                    }

                }

                $response = Response::json($response_array);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Upload Event Photo's
    public function uploadPhoto(Request $request) {

         $validator = Validator::make($request->all(), [
             'token' => 'required',
             'city_id' => 'required',
             'date' => 'required',
             'occasion_id' => 'required',
             'venue' => 'required',

         ]);

         $locale = '';
         if (isset($request->language_type) && $request->language_type == 'ar') {
             $locale = 'ar';
         } else {
             $locale = 'en';
         }
         app()->setLocale($locale);

         $input = $request->all();
         if ($validator->fails()) {
             $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
             $response = Response::json($response_array);
         } else {
             try {

                 $User = Auth::user();
                 $getOcType = BusinessOccasionType::select('id', 'title', 'title_ar', 'top_category')->where('id', '=', $input['occasion_id'])->first();

                 if (isset($getOcType) && !empty($getOcType)) {

                     $dataArr = array(

                         'city_id' => $request->city_id,
                         'studio_date' => $request->date,
                         'occasion_name' => $getOcType->title,
                         'occasion_name_ar' => $getOcType->title_ar,
                         'occasion_type' => $request->occasion_id,
                         'occasion_venue' => $request->venue,
                         'posted_by' => $User->cus_id,
                         'images' => 'image.jpg');
                     $getOcList = Occasion::where('posted_by', $User->cus_id)->where('occasion_type', $request->occasion_id)->where('city_id', $request->city_id)->where('studio_date', $request->date)->first();

                     if (isset($getOcList) && !empty($getOcList)) {
                         $occID = $getOcList->id;
                         if ($files = $request->file('image')) {

                             foreach($files as $file) {
                                 $name = str_replace(' ', '_', time().
                                     '_'.$file->getClientOriginalName());
                                 $extension = $file->getClientOriginalExtension();
                                 $fileName = rand(11111, 99999).
                                 '_'.time().
                                 '.'.$extension;
                                 $imageRealPath = $file->getRealPath();
                                 $thumbName = 'thumb-'.$fileName;
                                 $img = Image::make($imageRealPath); // use this if you want facade style code
                                 $thumb_width = 300;
                                 list($width, $height) = getimagesize($imageRealPath);
                                 $thumb_height = ($thumb_width / $width) * $height;
                                 $img->resize($thumb_width, $thumb_height);
                                 $img->save('public/studio_img'.
                                     '/'.$thumbName);
                                 $imageUploadSuccess = $file->move('public/studio_img/', $fileName);
                                 if ($imageUploadSuccess) {
                                     $shop_Img = url('').
                                     '/public/studio_img/'.$thumbName;
                                 } else {
                                     $response_array = array('data' => ['success' => false, 'message' => 'Something went wrong please try again', 'code' => 500]);
                                     $response = Response::json($response_array, 500);
                                 }

                                 // Insert data to db

                                 $gallery = OccasionImage::insert(array('occasion_id' => $occID, 'image_url' => $shop_Img));

                             }

                         }

                     } else {

                         $insertData = Occasion::insertGetId($dataArr);
                         if ($files = $request->file('image')) {

                             foreach($files as $file) {
                                 $name = str_replace(' ', '_', time().
                                     '_'.$file->getClientOriginalName());
                                 $extension = $file->getClientOriginalExtension();
                                 $fileName = rand(11111, 99999).
                                 '_'.time().
                                 '.'.$extension;
                                 $imageRealPath = $file->getRealPath();
                                 $thumbName = 'thumb-'.$fileName;
                                 $img = Image::make($imageRealPath); // use this if you want facade style code
                                 $thumb_width = 300;
                                 list($width, $height) = getimagesize($imageRealPath);
                                 $thumb_height = ($thumb_width / $width) * $height;
                                 $img->resize($thumb_width, $thumb_height);
                                 $img->save('public/studio_img'.
                                     '/'.$thumbName);
                                 $imageUploadSuccess = $file->move('public/studio_img/', $fileName);
                                 if ($imageUploadSuccess) {
                                     $shop_Img = url('').
                                     '/public/studio_img/'.$thumbName;
                                 } else {
                                     $response_array = array('data' => ['success' => false, 'message' => 'Something went wrong please try again', 'code' => 500]);
                                     $response = Response::json($response_array, 500);
                                 }

                                 // Insert data to db

                                 $gallery = OccasionImage::insert(array('occasion_id' => $insertData, 'image_url' => $shop_Img));
                                 $ccgallery = OccasionImage::where('occasion_id', '=', $insertData)->first();

                             }

                             $dta['images'] = $ccgallery->image_url;
                             $insertgallery = Occasion::where('id', '=', $insertData)->update($dta);

                         }

                     }

                     $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => trans($locale.
                         '_mob_lang.UPLOAD_SUCESS')]);

                 } else {

                     $response_array = array('data' => ['success' => false, 'code' => 200, 'message' => trans($locale.
                         '_mob_lang.UPLOAD_ERROR')]);

                 }
                 $response = Response::json($response_array);

             } catch (ModelNotFoundException $e) {
                 $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                 $response = Response::json($response_array);
             }
         }

         return $response;

    }
     //delete Event Photo's
    public function deletestudio(Request $request) {
        $validator = Validator::make($request->all(), [
            'event_id' => 'required',
            'token' => 'required',
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $id = $_REQUEST['event_id'];
                $getoccasion = Occasion::where('id', '=', $id)->first();
                if ($getoccasion) {
                    $getgallery = OccasionImage::where('occasion_id', '=', $id)->get();
                    foreach($getgallery as $value) {
                        $fullurlmore = explode('-', $value->image_url);
                        @unlink('public/studio_img/'.$fullurlmore[1]);
                        @unlink('public/studio_img/thumb-'.$fullurlmore[1]);
                        $deletegallery = OccasionImage::where('occasion_id', $id)->delete();
                    }

                    $dtoccasion = Occasion::where('id', '=', $id)->delete();

                }
                $response_array = array('data' => ['success' => true, 'message' => 'Successfully deleted your images', 'code' => 200]);
                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    //Get User Wallet Detail
    public function getWalletInfo(Request $request) {

        $id = Auth::id(); 
        $wallet = User::select('cus_id','wallet')
            ->where('cus_id','=',$id)
            ->first(); 
        $response_array = array('data' => ['success' => true,'code' => 200, 'wallet' => $wallet]);
        $response = Response::json($response_array);
        return $response;

    }

    //Add Amount Wallet
    public function addWalletAmount(Request $request) {

        $validator = Validator::make($request->all(), [
            'amount' => 'required',
            'token' => 'required'
        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $wallet = User::select('cus_id','wallet')
                ->where('cus_id','=',$User->cus_id)
                ->first();
                    if (!empty($wallet)){ 
                    $dta['wallet'] = $wallet->wallet  + $input['amount'];
                    User::where('cus_id','=',$User->cus_id)->update($dta);
                    $amount = User::select('cus_id','wallet')
                    ->where('cus_id','=',$User->cus_id)
                    ->first();
                   
                    $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => trans($locale.
                        '_mob_lang.WALLET_SUCESS')], 'cus_id' => $amount->cus_id, 'wallet' => $amount->wallet);

                    } else {
                        $response_array = array('data' => ['success' => false, 'code' => 200, 'message' => trans($locale.
                        '_mob_lang.WALLET_ERROR')]);
                    }

                $response = Response::json($response_array);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR')]);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Get User Review Detail
    public function getReviewInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'order_id' => 'required',
            'token' => 'required',
            'language_type' => 'required'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();

                if ($input['language_type'] == 'ar') {
                //get review data in arabic
                $reviewsC = ProductReview::where('customer_id', '=', $User->cus_id)->where('order_id', '=', $input['order_id'])->where('product_id', '=', $input['product_id'])->with('getCategoryReview:mc_id,mc_name_ar as mc_name')->with('getProductReview:pro_id,pro_title_ar as pro_title')->with('getWorkerReview:id,staff_member_name_ar as staff_member_name')->count();
                    if($reviewsC >=1)
                    { 
                    $review = ProductReview::where('customer_id', '=', $User->cus_id)->where('order_id', '=', $input['order_id'])->where('product_id', '=', $input['product_id'])->with('getCategoryReview:mc_id,mc_name_ar as mc_name')->with('getProductReview:pro_id,pro_title_ar as pro_title')->with('getWorkerReview:id,staff_member_name_ar as staff_member_name')->first();
                       
                      $produidcount = DB::table('nm_review')->where('product_id',$input['product_id'])->where('review_type','worker')->where('order_id',$input['order_id'])->where('customer_id', $User->cus_id)->count();
                      if($produidcount >0) {
                      $productdata = DB::table('nm_review')->where('product_id',$input['product_id'])->where('review_type','worker')->where('order_id',$input['order_id'])->where('customer_id', $User->cus_id)->first();
                        


                            if(isset($productdata->comments) && $productdata->comments!='')
                            {
                            $review->worker_review = $productdata->comments; 
                            }
                            else
                            {
                            $review->worker_review = ''; 
                            }


                            if(isset($productdata->ratings) && $productdata->ratings!='')
                            {
                            $review->worker_rating = $productdata->ratings;
                            }
                            else
                            {
                            $review->worker_rating = '';
                            }


                            if(isset($productdata->status) && $productdata->status!='')
                            {
                            $review->worker_status = $productdata->status;
                            }
                            else
                            {
                            $review->worker_status = '';
                            }

                         
              }
              else
              {
                $review->worker_review = ''; 
                $review->worker_rating = '';
                 $review->worker_status = '';

              }           
                        
                      }
                      else
                      {
                        $review =null;

                      }

                     //Review start

                      $catid = DB::table('nm_order_product')->where('order_id',$input['order_id'])->count();
                       $staffname ='';
                       $shopname ='';
                       $staffname ='';
                       $rating = '';
                       $status = '';
                       $staffname = '';
                       $staffid = null;
                      if($catid) {
                        $catname = DB::table('nm_order_product')->where('order_id',$input['order_id'])->first();
                        $shopid = $catname->shop_vendor_id;
                       if( $shopid ==''){  $shopid = $catname->shop_id;}
                        $catname = DB::table('nm_category')->where('mc_id',$shopid)->first();

                     
                        $shopname = $catname->mc_name_ar;
                        } 
                      
                       $reviewcount = DB::table('nm_review')->where('order_id',$input['order_id'])->count();
                       if($reviewcount) {

                          $reviewrat = DB::table('nm_review')->where('order_id',$input['order_id'])->first();

                          $rating = $reviewrat->ratings;
                          $status = $reviewrat->status;
 
                       }

                      //Staff

                       $staffcount = DB::table('nm_order_services_staff')->where('order_id',$input['order_id'])->where('service_id',$input['product_id'])->where('product_type','beauty')->count();
                       if($staffcount>0) {

                          $staffdata =  DB::table('nm_order_services_staff')->where('order_id',$input['order_id'])->where('service_id',$input['product_id'])->where('product_type','beauty')->first();

                           $staffid = $staffdata->staff_id;
                         
                           $staffnamecount = DB::table('nm_services_staff')->where('id',$staffid)->count(); 

                           if($staffnamecount>0) {

                            $staffnamed = DB::table('nm_services_staff')->where('id',$staffid)->first();

                            $staffname =  $staffnamed->staff_member_name;

                            $staffid =   $staffid;

                           }
 
                       }

                       
                    if (!empty($catid)){
                   
                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'review' => $review,'workername'=>$staffname,'shopname'=>$shopname,'rating'=>$rating,'status'=>$status,'staffid'=>$staffid);

                    } else {

                        $review = ProductReview::where('customer_id', '=', $User->cus_id)->where('order_id', '=', $input['order_id'])->where('shop_id', '=', $input['product_id'])->with('getCategoryReview:mc_id,mc_name')->with('getWorkerReview:id,staff_member_name')->first();

                     //Staff

                       $staffcount = DB::table('nm_order_services_staff')->where('order_id',$input['order_id'])->where('service_id',$input['product_id'])->where('product_type','beauty')->count();
                       if($staffcount) {

                          $staffdata =  DB::table('nm_order_services_staff')->where('order_id',$input['order_id'])->where('service_id',$input['product_id'])->first();

                          $staffid = $staffdata->staff_id;
                         
                           $staffnamecount = DB::table('nm_services_staff')->where('id',$staffid)->count(); 

                           if($staffnamecount>0) {

                            $staffnamed = DB::table('nm_services_staff')->where('id',$staffid)->first();

                            $staffname =  $staffnamed->staff_member_name;

                            $staffid =   $staffid;

                           }
                       }




                            

                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'review' => $review,'workername'=>$staffname,'shopname'=>$shopname,'rating'=>$rating,'status'=>$status,'staffid'=>$staffid);
                    }


                } else {
                    //Get review list in english

                    $reviewsC = ProductReview::where('customer_id', '=', $User->cus_id)->where('order_id', '=', $input['order_id'])->where('product_id', '=', $input['product_id'])->with('getCategoryReview:mc_id,mc_name')->with('getProductReview:pro_id,pro_title')->with('getWorkerReview:id,staff_member_name')->count();
                    if($reviewsC >=1)
                    {



                    $review = ProductReview::where('customer_id', '=', $User->cus_id)->where('order_id', '=', $input['order_id'])->where('product_id', '=', $input['product_id'])->with('getCategoryReview:mc_id,mc_name')->with('getProductReview:pro_id,pro_title')->with('getWorkerReview:id,staff_member_name')->first();
                       
                      $produidcount = DB::table('nm_review')->where('product_id',$input['product_id'])->where('review_type','worker')->where('order_id', '=', $input['order_id'])->where('customer_id', $User->cus_id)->count();

                      if($produidcount >0) {
                      $productdata = DB::table('nm_review')->where('product_id',$input['product_id'])->where('review_type','worker')->where('order_id', '=', $input['order_id'])->where('customer_id', $User->cus_id)->first();
                        


                            if(isset($productdata->comments) && $productdata->comments!='')
                            {
                            $review->worker_review = $productdata->comments; 
                            }
                            else
                            {
                            $review->worker_review = ''; 
                            }


                            if(isset($productdata->ratings) && $productdata->ratings!='')
                            {
                            $review->worker_rating = $productdata->ratings;
                            }
                            else
                            {
                            $review->worker_rating = '';
                            }


                            if(isset($productdata->status) && $productdata->status!='')
                            {
                            $review->worker_status = $productdata->status;
                            }
                            else
                            {
                            $review->worker_status = '';
                            }

                         
              }  else
              {
                $review->worker_review = ''; 
                $review->worker_rating = '';
                 $review->worker_status = '';

              }           
                            
                        
                      }
                      else
                      {
                        $review =null;

                      }

                     //Review start

                      $catid = DB::table('nm_order_product')->where('order_id',$input['order_id'])->count();
                       $staffname ='';
                       $shopname ='';
                       $staffname ='';
                       $rating = '';
                       $status = '';
                       $staffname = '';
                       $staffid = null;
                      if($catid) {
                        $catname = DB::table('nm_order_product')->where('order_id',$input['order_id'])->first();
                        $shopid = $catname->shop_vendor_id;
                       
                        $catname = DB::table('nm_category')->where('mc_id',$shopid)->first();
                        $shopname = $catname->mc_name;
                        } 
                      
                       $reviewcount = DB::table('nm_review')->where('order_id',$input['order_id'])->count();
                       if($reviewcount) {

                          $reviewrat = DB::table('nm_review')->where('order_id',$input['order_id'])->first();

                          $rating = $reviewrat->ratings;
                          $status = $reviewrat->status;
 
                       }

                      //Staff

                       $staffcount = DB::table('nm_order_services_staff')->where('order_id',$input['order_id'])->where('service_id',$input['product_id'])->where('product_type','beauty')->count();
                       if($staffcount>0) {

                          $staffdata =  DB::table('nm_order_services_staff')->where('order_id',$input['order_id'])->where('service_id',$input['product_id'])->where('product_type','beauty')->first();

                           $staffid = $staffdata->staff_id;
                         
                           $staffnamecount = DB::table('nm_services_staff')->where('id',$staffid)->count(); 

                           if($staffnamecount>0) {

                            $staffnamed = DB::table('nm_services_staff')->where('id',$staffid)->first();

                            $staffname =  $staffnamed->staff_member_name;

                            $staffid =   $staffid;

                           }
 
                       }

                       
                    if (!empty($catid)){
                   
                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'review' => $review,'workername'=>$staffname,'shopname'=>$shopname,'rating'=>$rating,'status'=>$status,'staffid'=>$staffid);

                    } else {

                        $review = ProductReview::where('customer_id', '=', $User->cus_id)->where('order_id', '=', $input['order_id'])->where('shop_id', '=', $input['product_id'])->with('getCategoryReview:mc_id,mc_name')->with('getWorkerReview:id,staff_member_name')->first();

                     //Staff

                       $staffcount = DB::table('nm_order_services_staff')->where('order_id',$input['order_id'])->where('service_id',$input['product_id'])->where('product_type','beauty')->count();
                       if($staffcount) {

                          $staffdata =  DB::table('nm_order_services_staff')->where('order_id',$input['order_id'])->where('service_id',$input['product_id'])->first();

                          $staffid = $staffdata->staff_id;
                         
                           $staffnamecount = DB::table('nm_services_staff')->where('id',$staffid)->count(); 

                           if($staffnamecount>0) {

                            $staffnamed = DB::table('nm_services_staff')->where('id',$staffid)->first();

                            $staffname =  $staffnamed->staff_member_name;

                            $staffid =   $staffid;

                           }
                       }
 
                            

                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'review' => $review,'workername'=>$staffname,'shopname'=>$shopname,'rating'=>$rating,'status'=>$status,'staffid'=>$staffid);
                    }

                }

                $response = Response::json($response_array);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Add your review
    public function addReview(Request $request) {

        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'product_id' => 'required',
            'order_id' => 'required',
            'ratings' => 'required',
            'comments' => 'required',
            'review_type'=>'required'
        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();


        
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();

                if($input['review_type'] == 'shop'){
                    $addreview = OrderSingleProduct::where('cus_id', '=', $User->cus_id)->where('category_id', '=', $input['product_id'])->where('order_id', '=', $input['order_id'])->first();
                    if(!empty($addreview)){
                    $review = ProductReview::where('customer_id', '=', $User->cus_id)->where('shop_id', '=', $input['product_id'])->where('order_id', '=', $input['order_id'])->first();
                    if(!empty($review)){
                    $data = array();
                    $data['comments'] = $request->comments;
                    $data['ratings'] = $request->ratings;
                    ProductReview::where('customer_id','=',$User->cus_id)->where('shop_id','=',$input['product_id'])->update($data);

                    }else{
                        $data = new ProductReview;
                     if($request->order_id!='') {
                    $data->order_id = $request->order_id;
                    } else {
                      $data->order_id ='';

                    }
                    $data->product_id = $request->product_id;
                    $data->shop_id = $request->shop_id;
                    $data->customer_id = $User->cus_id;
                    $data->comments = $request->comments;
                    $data->ratings = $request->ratings;
                    $data->review_type = 'shop';
                    $data->status = '0';
                    $data->vandor_id = $addreview->merchant_id; 
                    $data->save();
                    //cart product attribute entry*/
                    }

                    
                    }


                }else if($input['review_type'] == 'product'){
 
                    $addreview = OrderSingleProduct::where('cus_id', '=', $User->cus_id)->where('product_id', '=', $input['product_id'])->where('order_id', '=', $input['order_id'])->first();

                    if(!empty($addreview)){

                    $review = ProductReview::where('customer_id', '=', $User->cus_id)->where('product_id', '=', $input['product_id'])->where('order_id', '=', $input['order_id'])->first();
                    if(!empty($review)) {
                    $data = array();
                    $data['comments'] = $request->comments;
                    $data['ratings'] = $request->ratings;
                    ProductReview::where('customer_id','=',$User->cus_id)->where('product_id','=',$input['product_id'])->update($data);

                    } else{

                    $data = new ProductReview;

                    $data->order_id = $request->order_id;
                    $data->product_id = $request->product_id;
                    $data->shop_id = $request->shop_id;
                    $data->customer_id = $User->cus_id;
                    $data->comments = $request->comments;
                    $data->ratings = $request->ratings;
                    $data->review_type = 'shop';
                    $data->status = '0';
                    $data->vandor_id = $addreview->merchant_id; 
                    $data->save();
                    //cart product attribute entry*/

                    }  
                 }

                    if(isset($input['worker_rating']) && $input['worker_rating'] != ''){

                    $worker_review=$input['worker_review'];
                    $worker_rating=$input['worker_rating'];
                    $addreview = OrderSingleProduct::where('cus_id', '=', $User->cus_id)->where('product_id', '=', $input['product_id'])->where('order_id', '=', $input['order_id'])->first();
                   

                    if(!empty($addreview)){
                    $review = ProductReview::where('customer_id', '=', $User->cus_id)->where('shop_id', '=', $input['product_id'])->where('order_id', '=', $input['order_id'])->first();


                    if(!empty($review)){
                    $data = array();
                    $data['comments'] = $request->comments;
                    $data['ratings'] = $request->ratings;
                    ProductReview::where('customer_id','=',$User->cus_id)->where('shop_id','=',$input['product_id'])->update($data);

                    }else{
                        $data = new ProductReview;
                     if($request->order_id!='') {
                    $data->order_id = $request->order_id;
                    } else {
                      $data->order_id ='';
                    }
                    $data->product_id = $request->product_id;
                    $data->shop_id = $request->shop_id;
                    $data->customer_id = $User->cus_id;
                    $data->comments = $worker_review;
                    $data->ratings = $worker_rating;
                    $data->review_type = 'worker';
                    $data->worker_id = $input['worker_id'];
                    $data->status = '0';
                     $data->vandor_id = $addreview->merchant_id; 

                     if($worker_review!='')
                     {
                          $data->save();
                     }
                  
                    //cart product attribute entry*/

                       }
                    }
                  }
                
 
 
                $vendorid = $addreview->merchant_id; 
 


 
                $subject = 'Review Received'; 
                
                $userid = $User->cus_id;
                $userdata = '';
                $venddata = '';
                $vendcount = DB::table('nm_merchant')->select('mer_fname')->where('mer_id',$vendorid)->count();
                 if($vendcount > 0) {
                 $venddata = DB::table('nm_merchant')->select('mer_fname')->where('mer_id',$vendorid)->first();
                 }
                 $usercount = DB::table('nm_customer')->select('cus_name')->where('cus_id',$userid)->count();
                 if($usercount > 0) {
                 $userdata = DB::table('nm_customer')->select('cus_name')->where('cus_id',$userid)->first();
                 }
                 $worker_rating = $request->worker_rating;
                 $worker_comment = $request->worker_review;
                 $shop_comment =  $request->comments;
                 $shopratings  =  $request->ratings; 
                $subject = 'Review Received';
                Mail::send('emails.mobile_reviewmail',
                array(
                'vendor_name' => $venddata->mer_fname,
                'user_name' => $userdata->cus_name, 
                'worker_rating' => $worker_rating,      
                'worker_comment' =>$worker_comment,
                'shop_rating' => $shopratings,   
                'shop_comment' => $shop_comment,
                ), function($message) use($subject)
                {
                $message->to('himanshu@wisitech.com')->subject($subject);
                });
                }
                
                
                if (!empty($data)) {
                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => trans($locale.
                        '_mob_lang.UPLOAD_SUCESS')]);

                    } else {
                        $response_array = array('data' => ['success' => false, 'code' => 200, 'message' => trans($locale.
                        '_mob_lang.UPLOAD_ERROR')]);
                    }

                $response = Response::json($response_array);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Get User's Photography detail
    public function getRequestaQuote(Request $request) {

        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'language_type' => 'required'
        ]);
        $input = $request->all();
       
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
               $search_key = $request->search_key;


               // $search_key = 'singer'; 

                if (isset($search_key) && $search_key == 'singer') {

                 
                  $getinquiry = ServiceInquiry::where('user_id', $User->cus_id)->where('quote_for', $search_key)->orderBy('id', 'DESC')->paginate(30);
                    $currentdate = date("Y-m-d");
                   foreach($getinquiry as $key=> $val) {

                        $bookingdate = $val->date;
                      
                        $status = $val->status;
                        if($status==3) {
                        if($bookingdate < $currentdate) {
                        
                        $getinquiry[$key]->rev_status = '1';
                        } else {
                          $getinquiry[$key]->rev_status = '0';

                        }
                     }

                    }
                  //  print_r($getinquiry);die;  
                   
                }


                elseif(isset($search_key) && $search_key == 'band') {
                    $getinquiry = ServiceInquiry::where('user_id', $User->cus_id)->where('quote_for', $search_key)->orderBy('id', 'DESC')->paginate(30);
                }
                elseif(isset($search_key) && $search_key == 'recording') {
                    $getinquiry = ServiceInquiry::where('user_id', $User->cus_id)->where('quote_for', $search_key)->orderBy('id', 'DESC')->paginate(30);
                } else {
                    $getinquiry = ServiceInquiry::where('user_id', $User->cus_id)->with('getVendorComment')->orderBy('id', 'DESC')->paginate(30);
                }

                foreach($getinquiry as $val)
                {
               // $val->date = strtotime($val->date);
                 $val->date = strtotime($val->date." GMT");
                $val->occasion_type_chk = '';
                $singerName = $val->singer_name;
                $occasionType = $val->occasion_type;
                $occasion_type_id = $val->occasion_type_id;






                if($occasion_type_id !=0)
                {
                
                $TNCsaC = DB::table('nm_business_occasion_type')->where('id',$occasion_type_id)->count();
                if($TNCsaC >=1)   
                {
                  $TNCsa = DB::table('nm_business_occasion_type')->where('id',$occasion_type_id)->first();   
                $val->occasion_type_chk = $TNCsa->title;   
                }
                

                }
   
                    $relate_with = $val->relate_with;
                if($input['language_type']=='ar')
                {
                    $Fnamec = DB::table('nm_music')->where('id',$relate_with)->count();     
                    if($Fnamec >=1)
                    {
                    $Fname = DB::table('nm_music')->where('id',$relate_with)->first();   
                    $val->singer_name = $Fname->name_ar;  
                    }


                    $Tn = DB::table('nm_business_occasion_type')->where('id',$occasion_type_id)->count();     
                    if($Tn >=1)
                    {
                    $TNCs = DB::table('nm_business_occasion_type')->where('id',$occasion_type_id)->first();   
                    $val->occasion_type = $TNCs->title_ar;  
                    }

                }
                else
                {

                    $Tn = DB::table('nm_business_occasion_type')->where('id',$occasion_type_id)->count();     
                    if($Tn >=1)
                    {
                    $TNCs = DB::table('nm_business_occasion_type')->where('id',$occasion_type_id)->first();   
                    $val->occasion_type = $TNCs->title;  
                    }
  
                }
 


                }



                if (!$getinquiry->isEmpty()) {

                    $response_array = array('data' => ['success' => false, 'message' => 'data found', 'code' => 200], 'inquiry_list' => $getinquiry);
                    $response = Response::json($response_array);

                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'data found', 'code' => 200], 'inquiry_list' => $getinquiry);
                    $response = Response::json($response_array);

                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //User Quote for singer

     public function returnUserQuoteforMusic(Request $request) {
            $validator = Validator::make($request->all(), [
            /*'token' => 'required',
            'language_type' => 'required',*/
            'quote_for' => 'required',
        ]);
         $input = $request->all();

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $returnquey = array();
                 $returnqueycount = ServiceInquiry::where('user_id',$User->cus_id)->where('id',$request->singer_id)->where('quote_for',$input['quote_for'])->count();

                if($returnqueycount > 0) {
                 $returnquey = ServiceInquiry::where('user_id', $User->cus_id)->where('id',$request->singer_id)->where('quote_for',$input['quote_for'])->get();

                    foreach ($returnquey as $val) {
                      $cityid = $val->city_id;

                       $cityid = $val->city_id;


                    $occasion_type_id = $val->occasion_type_id;
                  $CBook =   DB::table('nm_business_occasion_type')->where('id',$occasion_type_id)->count();
                if($CBook >=1)
                {
                $CBooks =   DB::table('nm_business_occasion_type')->where('id',$occasion_type_id)->first();
                if ($input['language_type'] == 'ar') {
                $val->occasion_type = $CBooks->title_ar;
                }
                else
                {
                $val->occasion_type = $CBooks->title;
                }
                }
 
                if ($input['language_type'] == 'ar') {
                if($val->mc_name_ar!='')
                {
                //$val->singer_name = $val->mc_name_ar;      
                }

                }
 

                      $querycity =City::select('ci_id','ci_name','ci_name_ar')->where('ci_id',$cityid)->first();
                     if ($input['language_type'] == 'ar') {  $cityname = $querycity->ci_name_ar; } else {
                        $cityname = $querycity->ci_name;}
                       $val->bookingdate = $val->date;
                         $val->city_name =$cityname;
                         //$val->date = strtotime($val->date);
                       $val->date = strtotime($val->date." GMT");
                    }

                }

                
                 if($returnquey!='') {
                
                
                $response_array = array('data' => ['success' => true, 'message' => $returnquey, 'code' => 200]);
                $response = Response::json($response_array);
                  return $response;
             }  else {
                  
                 $response_array = array('data' => ['success' => true, 'message' => '0', 'code' => 200]);
                $response = Response::json($response_array);
                  return $response;
            }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'Data not found']);
                $response = Response::json($response_array);
            }
        }



     }





   //Singer review

     //Get singer review Himanshu
    public function singerReview(Request $request) {

        $validator = Validator::make($request->all(), [
            /*'token' => 'required',
            'language_type' => 'required',*/
            'merchant_id' => 'required',
        ]);
         $input = $request->all();

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $merchantid = $input['merchant_id'];
                 $status = ServiceInquiry::where('user_id', $User->cus_id)->where('merchant_id','=', $merchantid)->where('quote_for','singer')->where('status',3)->first();

                 if($status!='') {
                 $bookingdate = $status->date;

                 $currentdate = date("Y-m-d");
                 if($bookingdate < $currentdate) { 
                $response_array = array('data' => ['success' => true, 'message' => '1', 'code' => 200]);
                $response = Response::json($response_array);
                  return $response;
            } } else {
                  
                 $response_array = array('data' => ['success' => true, 'message' => '0', 'code' => 200]);
                $response = Response::json($response_array);
                  return $response;
            }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

      

    }


    //Get User's Photography detail
    public function updateRequestaQuote(Request $request) {

        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'language_type' => 'required',
            'inquiry_id' => 'required',
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $inquiry = $request->inquiry_id;
                ServiceInquiry::where('user_id', $User->cus_id)->where('id','=', $inquiry)->update(['status'=>'4']);
                if ($input['language_type'] == 'ar') {
                $response_array = array('data' => ['success' => true, 'message' => 'ينكر بنجاح', 'code' => 200]);
                }
                else
                {
                $response_array = array('data' => ['success' => true, 'message' => 'Successfully deny', 'code' => 200]);
                }

                $response = Response::json($response_array);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Get User's Insurance Amount Product Detail
    public function getInsuranceAmountProduct(Request $request) {

        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'language_type' => 'required',
            'order_id' => 'required'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();

                if ($input['language_type'] == 'ar') {
                    $orderlist = OrderProduct::where('order_cus_id', '=', $User->cus_id)->where('order_id', '=', $request->order_id)->first();
                    if (!empty($orderlist)){
                        //get user's product detail
                    //$productlist = InsuranceAmount::select('id','merchant_id','product_type','order_id','requested_date','status','transation_id','product_id','pro_title_ar as pro_title','img','updated_at','amount')->where('order_id', '=', $request->order_id)->orderBy('updated_at', 'DESC')->get();

                     $productlist = DB::table('nm_order_product')->select('id','product_id','pro_title_ar as pro_title','updated_at','insurance_amount as amount','refund_status as status','refund_amount','refund_message')->where('order_id', '=', $request->order_id)->where('showcartproduct','=',0)->orderBy('updated_at', 'DESC')->get();


                        foreach($productlist as $val)
                        {
                        $amount = $val->amount;
                        $Amt = $amount;
                        $product_id = $val->product_id;
                        if($amount < 1)
                        { 
                        $productlistC = DB::table('nm_order_services_attribute')->select('insurance_amount as amount')->where('product_id', '=', $product_id)->where('order_id', '=', $request->order_id)->count();
                        if( $productlistC >=1)
                        {

                        $productlists = DB::table('nm_order_services_attribute')->select('insurance_amount as amount')->where('product_id', '=', $product_id)->where('order_id', '=', $request->order_id)->first();
                        $Amt =  $productlists->amount;
                        }

                        }  
                        $val->amount = $Amt;
                        }


                    //GET OOCCASION LIST IN ARABIC
                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'تم العثور على البيانات'], 'product_list' => $productlist,);

                    } else {
                        $response_array = array('data' => ['success' => false, 'message' => 'لم يتم العثور على بيانات.', 'code' => 200]);
                    }

                } else {

                    $orderlist = OrderProduct::where('order_cus_id', '=', $User->cus_id)->where('order_id', '=', $request->order_id)->first();
                    if (!empty($orderlist)){

  //$productlist = InsuranceAmount::select('id','merchant_id','product_type','order_id','requested_date','status','transation_id','product_id','pro_title','img','updated_at','amount')->where('order_id', '=', $request->order_id)->orderBy('updated_at', 'DESC')->get();



                    $productlist = DB::table('nm_order_product')->select('id','product_id','pro_title','product_sub_type','buy_rent','updated_at','insurance_amount as amount','refund_status as status','refund_amount','refund_message','quantity')->where('order_id', '=', $request->order_id)->where('showcartproduct','=',0)->orderBy('updated_at', 'DESC')->get();

                    foreach($productlist as $val)
                    {
                      if($val->buy_rent=='readymade')
                            { 
                                $amount = 0;
                            }elseif($val->product_sub_type=='dress' && $val->buy_rent=='rent'){
                                 $amount = $val->amount*$val->quantity;
                            }elseif($val->product_sub_type=='acoustic' && $val->buy_rent=='rent'){
                                 $amount = $val->amount*$val->quantity;
                            }else{ 
                                $amount = $val->amount;
                            } 
                    
                    $Amt = $amount;
                    $product_id = $val->product_id;
                    if($amount < 1)
                    { 
                     $productlistC = DB::table('nm_order_services_attribute')->select('insurance_amount as amount')->where('product_id', '=', $product_id)->where('order_id', '=', $request->order_id)->count();
                            if( $productlistC >=1)
                            {

                             $productlists = DB::table('nm_order_services_attribute')->select('insurance_amount as amount','quantity')->where('product_id', '=', $product_id)->where('order_id', '=', $request->order_id)->first();
                             if($val->buy_rent=='readymade'){ 
                                $Amt =  $productlists->amount*$productlists->quantity;
                                }else{
                                 $Amt =  $productlists->amount;
                                }
                            }

                    }  
                    $val->amount = $Amt;
                    }



                        //GET OOCCASION LIST IN ENGLISH
                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'product_list' => $productlist);

                    } else {
                        $response_array = array('data' => ['success' => false, 'message' => 'data not found.', 'code' => 200]);
                    }
                }

                $response = Response::json($response_array);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }
    
public function apisendContact(Request $request)
{
 

        

            try {
 
        $Ename = $request->name;
        $Email = $request->email;
        $Message = $request->message;
        $lang = $request->lang;

   
 
        Mail::send('emails.contact-enquiry-app', ['Ename' => $Ename,'Email' => $Email,'Message' => $Message], function ($m) use ($Ename,$Email) {
            $m->from($Email, 'Goldencages');
            $m->to('info@goldencages.com', 'Goldencages')->subject('New contact enquiry');
        });
  
     
        if($lang != 'en')
        {
        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'شكرا لكم! تم ارسال رسالتك بنجاح.']);
        }
        else
        {
        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Thank you! Your message has been sent successfully.']);  
        }
       $response = Response::json($response_array);
 

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'Something went wrong']);
                $response = Response::json($response_array);
            }
         

        return $response;



}

public function getInvitionlist(Request $request)
{

  try {
                $User = Auth::user();
                $order = $request->orderid;
                $cusid = $request->cusid;

                if($cusid == '')
                {
                $cusid =  $User->cus_id;
                }

               

                $list = DB::table('nm_invitation_list')->where('customer_id',$cusid)->where('order_id',$order)->get();
  
                $response_array = array('data' => ['success' => true, 'code' => 200, 'list' => $list]);  
                $response = Response::json($response_array);
 

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'Something went wrong']);
                $response = Response::json($response_array);
            }
         

        return $response;

}


}
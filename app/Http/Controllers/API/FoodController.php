<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Http\Models;
use App\Category;
use App\Products;
use App\ProductOption;
use App\ProductOptionValue;
use App\City;
use App\ServiceAttribute;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\FoodContainerPrice;
use App\CartExternalFoodDish;
use App\CartServiceAttribute;
use Auth;
use App\User;
use Response;
use App\Gallery;
use App\NmProductGallery;
use DB;
use App\NmproductOptionValue;
class FoodController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |   Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function __construct() {}

    /**Get Food shop Info for Dates**/
    /*
       vendor_id(3th lavel category in category table) become pro_mc_id in product table to get full shop info.
    */
    public function getDateFoodShopInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id' => 'required', //Vendor ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['subcategory_id'])->first();
                $client_review = Category::get_category_review($input['vendor_id']);

                if (!empty($category)) {

                    if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                        if (!$client_review->isEmpty()) {
                            $review = $client_review;
                        } else {
                            $review = array();
                        }
                        //GET SUB CATEGORY
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name_ar', 'mc_img', 'budget_in_percentages')->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name', 'insurance_amount', 'google_map_address', 'address_image','longitude','latitude','mc_video_url')->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:title_ar as title,image,category_id')->first();
                        //GET VENDOR FOOD SHOP LIST
                        //Get Dates Menu List
                        /*$menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'image', 'status','attribute_title as title')->where('services_id', '=', $input['vendor_id'])->orderBy('title', 'ASC')->get();*/

                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'image', 'status','attribute_title as title')->whereIn('id', [34,35])->orderBy('title', 'ASC')->get();

      
 

                        if (isset($menu)) {
                            foreach($menu as $key => $value) {
                                //get product detail according food menu category.
                                $product = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_title as title','pro_qty','pro_desc_ar as pro_desc')->where('pro_mc_id', '=', $input['vendor_id'])->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('title', 'ASC')->paginate(10000);
                                $value->product_list = $product;
                                foreach($product as $val) {
                                    $name = explode(',', $val->option_id);
                                    $name = ['8','9'];
                                    //get service attribute option detail
                                    $pro_Option = ProductOption::select('id', 'option_title_ar as option_title', 'button_type_id', 'services_id')->whereIn('id', $name)->with('getBoxType:id,option_title_ar as option_title,type')->get();
                                    $val->product_option = $pro_Option;
                                   
                                        //Get service attribute option value for type.
                                        foreach ($pro_Option as $key => $value) {
                                            $optionid = ['22','9'];
                                            $pro_Option_val = ProductOptionValue::select('id','vandor_id','product_id','option_title_ar as option_title','product_option_id','value','discount','discount_price')->whereIn('product_option_id', $optionid)->where('product_id', $val->pro_id)->get();

                                        $value->product_option_value = $pro_Option_val;
                                        }
                                   
                                    
                                }
                            }
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'status' => $category->mc_status, 'message' => 'تم العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'vendor_detail' => $vendor, 'client_comments' => $review, 'dates_menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'يبدو أنه لا توجد طعام متاحة مع القائمة']);
                        }

                    } else {
                        /*FOR ENGLISH LANGUAGE*/
                        if (!$client_review->isEmpty()) {
                            $review = $client_review;
                        } else {
                            $review = array();
                        }
                        //GET SUB CATEGORY
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name', 'mc_img', 'budget_in_percentages')->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name', 'insurance_amount', 'google_map_address', 'address_image','longitude','latitude','mc_video_url')->with('getCity:ci_id,ci_name')->with('CategoryGallery:title,image,category_id')->first();

                        //GET VENDOR FOOD SHOP LIST

                        //Get Dates Menu List
                        //$menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->where('services_id', '=', $input['vendor_id'])->orderBy('attribute_title', 'ASC')->get();
            $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->whereIn('id', [34,35])->orderBy('attribute_title', 'ASC')->get();

 
         if (isset($menu)) {
                            foreach($menu as $key => $value) {   
                                //get product detail according food menu category.
                                $product = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_qty','pro_desc')->where('pro_mc_id', '=', $input['vendor_id'])->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(100);

 


                                $value->product_list = $product;
                                foreach($product as $val) {
                                    $name = explode(',', $val->option_id);
                                     $name = ['8','9'];
                                    //get service attribute option detail
                                    $pro_Option = ProductOption::select('id', 'option_title', 'button_type_id', 'services_id')->whereIn('id', $name)->with('getBoxType:id,option_title,type')->get();
                                    $val->product_option = $pro_Option;
                                     
                                        //Get service attribute option value for type.
                                        foreach ($pro_Option as $key => $value) {
                                             $optionid = ['22','9'];
                                            $pro_Option_val = ProductOptionValue::select('id','vandor_id','product_id','option_title','product_option_id','value','discount','discount_price')->where('product_id', $val->pro_id)->whereIn('product_option_id', $optionid)->get();


                                        $value->product_option_value = $pro_Option_val;
                                      

                                     
                                    }
                                }
                            }    
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'status' => $category->mc_status, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_detail' => $vendor, 'client_comments' => $review, 'dates_menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'It seems no Food are available with the menu.']);
                        }

                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    
//Himanshu  Product Gallery Images

      public function ProductGalleryImage(Request $request)

      {
         
           $validator = Validator::make($request->all(), [
            'product_id' => 'required' //Product
            
        ]);

        $input = $request->all();

       
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {
               
                $User = Auth::user();
                        /*
                        Language English
                        */
                        $productgallery = array();



 
$productsgallery = NmProductGallery::select('nm_product_gallery.image')->where('product_id', $input['product_id'])->get();

$productsMainImg = products::select('nm_product.pro_Img')->where('pro_id', $input['product_id'])->first();


 

                             
                        if (isset($productsgallery)) {
                           
                           $productgallery = array();


                           if(count($productsgallery) >=1)
                           {
                                foreach($productsgallery as $val) {


                                if($val['image'] !='')
                                {
                                $productgallery[]['image'] = $val['image'];
                                }

                                }
                           }

 $productgallery[]['image'] = $productsMainImg->pro_Img;
                             
 


                     $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'product_gallery' => $productgallery);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'يبدو أنه لا يوجد طبيب متاح مع هذه الفئة.']);
                            $response = Response::json($response_array);
                        }
                   

                    $response = Response::json($response_array);

               
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;
      }








    /**Get Food shop Info for  Desert (Sweets)**/
    /*
       vendor_id(3th lavel category in category table) become pro_mc_id in product table to get full shop info.
    */
    public function getDessertFoodShopInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id' => 'required', //Vendor ID
            'branch_id' => 'required', //Branch ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first();

                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($input['branch_id']);

                if (!empty($category)) {

                    if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                        if (!$client_review->isEmpty()) {
                            $review = $client_review;
                        } else {
                            $review = array();
                        }
                        //GET SUB CATEGORY
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name_ar as mc_name','mc_img')->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->with('getCity:ci_id,ci_name_ar as ci_name')->first();
                        //GET VENDOR DETAIL
                        $branch = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['branch_id'])->select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name', 'insurance_amount', 'google_map_address', 'address_image','longitude','latitude')->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:title_ar as title,image,category_id')->first();
                        //Get clinic category List


                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'image', 'status','attribute_title as title')->whereIn('id', [44,45])->orderBy('title', 'ASC')->get();

                      

                        if (isset($menu)) {
                            foreach($menu as $key => $value) {
                                //get product detail according food menu category.
                                $product = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_title as title','pro_disprice','pro_discount_percentage','pro_qty')->where('pro_mc_id', '=', $input['branch_id'])->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('title', 'ASC')->paginate(10000);
                                $value->product_list = $product;
                                foreach($product as $val) {
                                    $name = explode(',', $val->option_id);
                                    $name = [21,23];
                                    //get service attribute option detail
                                    $pro_Option = ProductOption::select('id', 'option_title_ar as option_title ', 'button_type_id', 'services_id')->whereIn('id', $name)->with('getBoxType:id,option_title_ar as option_title,type')->get();

                                   // print_r($pro_Option);die;
                                    $val->product_option = $pro_Option;
                                   // if ($val->attribute_id == 44) {
                                        //Get service attribute option value for type.
                                        foreach ($pro_Option as $key => $value) {
                                            $optionname = [22,21];
                                            $pro_Option_val = ProductOptionValue::select('id','vandor_id','product_id','option_title_ar as option_title','product_option_id','value as value','discount','discount_price')->whereIn('product_option_id', $optionname)->where('product_id', $val->pro_id)->get();


                                        $value->product_option_value = $pro_Option_val;
                                        }
                                   // }
                                }
                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name, 'vendor_branch_detail' => $branch, 'client_comments' => $review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'يبدو أنه لا يوجد طبيب متاح مع هذه الفئة.']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                        if (!$client_review->isEmpty()) {
                            $review = $client_review;
                        } else {
                            $review = array();
                        }
                        //GET SUB CATEGORY
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name', 'mc_img')->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name', 'mc_img')->with('getCity:ci_id,ci_name')->first();
                        //GET VENDOR DETAIL
                        $branch = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['branch_id'])->select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name', 'insurance_amount', 'google_map_address', 'address_image','longitude','latitude')->with('getCity:ci_id,ci_name')->with('CategoryGallery:title,image,category_id')->first();
                        //Get clinic category List



              $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->whereIn('id', [44,45])->orderBy('attribute_title', 'ASC')->get();


                       // $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->where('services_id', '=', $input['branch_id'])->orderBy('attribute_title', 'ASC')->get();
                        if (isset($menu)) {
                            foreach($menu as $key => $value) {
                                //get product detail according food menu category.
                                $product = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_disprice','pro_discount_percentage','pro_qty')->where('pro_mc_id', '=', $input['branch_id'])->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10000);
                                $value->product_list = $product;
                                foreach($product as $val) {
                                    $name = explode(',', $val->option_id);

                                    $name = [21,23];


                                    //get service attribute option detail
                                    $pro_Option = ProductOption::select('id', 'option_title', 'button_type_id', 'services_id')->whereIn('id', $name)->with('getBoxType:id,option_title,type')->get();


                                    $val->product_option = $pro_Option;
                                   // if ($val->attribute_id == 44) {
                                        //Get service attribute option value for type.
                                        foreach ($pro_Option as $key => $value) {

                                            $optionname = [22,21];

                                            $pro_Option_val = ProductOptionValue::select('id','vandor_id','product_id','option_title','product_option_id','value','discount','discount_price')->whereIn('product_option_id', $optionname)->where('product_id', $val->pro_id)->get();


                                        $value->product_option_value = $pro_Option_val;
                                        }
                                   // }
                                }
                            } 

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name, 'vendor_branch_detail' => $branch, 'client_comments' => $review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'It seems no Doctor are available with the clinic category.']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }


    //  Buffe  details Api
     public function getBuffeFoodShopInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id' => 'required', //Vendor ID
            'branch_id' => 'required', //Branch ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first();

                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($input['branch_id']);

                if (!empty($category)) {

                    if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                        if (!$client_review->isEmpty()) {
                            $review = $client_review;
                        } else {
                            $review = array();
                        }
                        //GET SUB CATEGORY
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name_ar as mc_name','mc_img')->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->with('getCity:ci_id,ci_name_ar as ci_name')->first();
                        //GET VENDOR DETAIL
                        $branch = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['branch_id'])->select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name', 'insurance_amount', 'google_map_address', 'address_image','longitude','latitude')->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:title_ar as title,image,category_id')->first();
                        //Get clinic category List


                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'image', 'status','attribute_title as title')->where('services_id', $input['branch_id'])->orderBy('title', 'ASC')->get();

                        print_r($menu);die;

                      

                        if (isset($menu)) {
                            foreach($menu as $key => $value) {
                                //get product detail according food menu category.
                                $product = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_title as title','pro_disprice','pro_discount_percentage')->where('pro_mc_id', '=', $input['branch_id'])->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('title', 'ASC')->paginate(10000);
                                $value->product_list = $product;
                                foreach($product as $val) {
                                    $name = explode(',', $val->option_id);
                                    $name = [21,23];
                                    //get service attribute option detail
                                    $pro_Option = ProductOption::select('id', 'option_title_ar as option_title ', 'button_type_id', 'services_id')->whereIn('id', $name)->with('getBoxType:id,option_title_ar as option_title,type')->get();

                                   // print_r($pro_Option);die;
                                    $val->product_option = $pro_Option;
                                   // if ($val->attribute_id == 44) {
                                        //Get service attribute option value for type.
                                        foreach ($pro_Option as $key => $value) {
                                            $optionname = [22,21];
                                            $pro_Option_val = ProductOptionValue::select('id','vandor_id','product_id','option_title_ar as option_title','product_option_id','value_ar as value','discount','discount_price')->whereIn('product_option_id', $optionname)->where('product_id', $val->pro_id)->get();


                                        $value->product_option_value = $pro_Option_val;
                                        }
                                   // }
                                }
                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name, 'vendor_branch_detail' => $branch, 'client_comments' => $review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'يبدو أنه لا يوجد طبيب متاح مع هذه الفئة.']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                        if (!$client_review->isEmpty()) {
                            $review = $client_review;
                        } else {
                            $review = array();
                        }
                        //GET SUB CATEGORY
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name', 'mc_img')->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name', 'mc_img')->with('getCity:ci_id,ci_name')->first();
                        //GET VENDOR DETAIL
                        $branch = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['branch_id'])->select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name', 'insurance_amount', 'google_map_address', 'address_image','longitude','latitude')->with('getCity:ci_id,ci_name')->with('CategoryGallery:title,image,category_id')->first();
                        //Get clinic category List



              $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->where('services_id', $input['branch_id'])->orderBy('attribute_title', 'ASC')->get();

                    

                       // $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->where('services_id', '=', $input['branch_id'])->orderBy('attribute_title', 'ASC')->get();
                        if (isset($menu)) {
                            foreach($menu as $key => $value) {
                                //get product detail according food menu category.
                                $product = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_disprice','pro_discount_percentage')->where('pro_mc_id', '=', $input['branch_id'])->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10000);
                                $value->product_list = $product;
                                foreach($product as $val) {
                                    $name = explode(',', $val->option_id);

                                    $name = [21,23];


                                    //get service attribute option detail
                                    $pro_Option = ProductOption::select('id', 'option_title', 'button_type_id', 'services_id')->whereIn('id', $name)->with('getBoxType:id,option_title,type')->get();


                                    $val->product_option = $pro_Option;
                                   // if ($val->attribute_id == 44) {
                                        //Get service attribute option value for type.
                                        foreach ($pro_Option as $key => $value) {

                                            $optionname = [22,21];

                                            $pro_Option_val = ProductOptionValue::select('id','vandor_id','product_id','option_title','product_option_id','value','discount','discount_price')->whereIn('product_option_id', $optionname)->where('product_id', $val->pro_id)->get();


                                        $value->product_option_value = $pro_Option_val;
                                        }
                                   // }
                                }
                            } 

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name, 'vendor_branch_detail' => $branch, 'client_comments' => $review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'It seems no Doctor are available with the clinic category.']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }



    //End Buffe details Api

    /**Get food menu **/
    public function getFoodMenu(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'vendor_id' => 'required',
            'branch_id' => 'required',
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error_code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {
            try {
                $User = Auth::user();
                $search_criteria = ['category_id' => $input['category_id'], 'subcategory_id' => $input['subcategory_id'], 'vendor_id' => $input['vendor_id'], 'branch_id' => $input['branch_id']];
                if ($input['language_type'] == 'ar') {
                    /*
                    Language Arabic
                    */
                    //Get Menu Detail


 $menuas = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->where('services_id', '=', $input['branch_id'])->where('status', '=', 1)->orderBy('attribute_title', 'ASC')->get();
                    $check = array();
                    foreach($menuas as $key => $value) {

                    $foodc = Products::select('attribute_id')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->count();
                    if($foodc >=1)
                    {
                    $foodca = Products::select('attribute_id')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->first();
                    array_push($check, $foodca->attribute_id);  
                    }
                    }


                    $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'image', 'status')->whereIn('id',$check)->where('services_id', '=', $input['branch_id'])->orderBy('attribute_title', 'ASC')->get();

                    if (isset($menu)) {
                        foreach($menu as $key => $value) {
                            //get food detail according menu
                            $food = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_qty', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10000);
                            $value->food_list = $food;
                            foreach($food as $val) {
                                $name = explode(',', $val->option_id);
                                //get service attribute option detail
                                $pro_Option = ProductOption::select('id', 'option_title_ar as option_title', 'button_type_id')->whereIn('id', $name)->with('getBoxType:id,option_title_ar as option_title,type')->get();
                                //Get service attribute option value with container list
                               /* $container = ProductOptionValue::select('id', 'product_option_id', 'option_title_ar as option_title', 'image', 'no_person', 'status', 'short_name')->where('product_option_id', '=', $name)->where('product_id', '=', $value->services_id)->orderBy('short_name', 'ASC')->get();*/

                                 // Change by Himanshu

 $container_pricea = FoodContainerPrice::select('id', 'product_id','product_option_id', 'product_option_value_id', 'price','discount','discount_price')->where('product_id', '=', $val->pro_id)->get();
$CiDS = array();
foreach($container_pricea as $vals)
{
$valus = $vals->product_option_value_id;
array_push($CiDS, $valus);
}

 
                                $container = ProductOptionValue::select('id', 'product_option_id', 'option_title_ar as option_title', 'image', 'no_person', 'status', 'short_name')->where('product_id', '=', $value->services_id)->where('status', '=', 1)->orderBy('short_name', 'ASC')->whereIn('id',$CiDS)->get();
                                $val->container = $pro_Option;
                                $val->container_list = $container;
                                foreach($container as $cont_value) {
                                    //Get Container Price
                                    $container_price = FoodContainerPrice::select('id', 'product_id','product_option_id', 'product_option_value_id', 'price','discount','discount_price')->where('product_id', '=', $val->pro_id)->where('product_option_value_id', '=', $cont_value->id)->get();
                                    $cont_value->container_price = $container_price;
                                }

                            }

                        }

                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'تم العثور على البيانات'], 'search_criteria' => $search_criteria, 'menu' => $menu);
                    } else {
                        $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                        $response = Response::json($response_array);
                    }

                } else {
                    /*
                    Language English
                    */
                    //Get menu detail

//      $foodC = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_qty', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id')->where('pro_mc_id', '=', $value->services_id)->where('pro_status', '=', 1)->get();
// $check = array();
// foreach($foodC as $foodCrt)
// {
//     array_push($check, $foodCrt->attribute_id);
// }


  $containerlist = NmproductOptionValue::where('product_id',$input['branch_id'])->where('status',1)->get();

 


                    $menuas = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->where('status', '=', 1)->where('services_id', '=', $input['branch_id'])->orderBy('attribute_title', 'ASC')->get();
                    $check = array();
                    foreach($menuas as $key => $value) {

                    $foodc = Products::select('attribute_id')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->count();
                    if($foodc >=1)
                    {
                    $foodca = Products::select('attribute_id')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->first();
                    array_push($check, $foodca->attribute_id);  
                    }
                    }


     

                    $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->whereIn('id', $check)->where('services_id', '=', $input['branch_id'])->orderBy('attribute_title', 'ASC')->get();

 

                    if (isset($menu)) {
                        foreach($menu as $key => $value) {
                            //get food detail according menu



                            $food = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_qty', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);


                            $value->food_list = $food;
                            foreach($food as $val) {
                                $name = explode(',', $val->option_id);
                                //get service attribute option detail
                                $pro_Option = ProductOption::select('id', 'option_title', 'button_type_id')->whereIn('id', $name)->with('getBoxType:id,option_title,type')->get();
                                //Get service attribute option value with container list


                                 $container_pricea = FoodContainerPrice::select('id', 'product_id','product_option_id', 'product_option_value_id', 'price','discount','discount_price')->where('product_id', '=', $val->pro_id)->get();
                                $CiDS = array();
                                foreach($container_pricea as $vals)
                                {
                                $valus = $vals->product_option_value_id;
                                array_push($CiDS, $valus);
                                }

 
                                $container = ProductOptionValue::select('id', 'product_option_id', 'option_title', 'image', 'no_person', 'status', 'short_name')->where('product_option_id', '=', $name)->where('status', '=', 1)->whereIn('id',$CiDS)->where('product_id', '=', $value->services_id)->orderBy('short_name', 'ASC')->get(); 
                                $val->container = $pro_Option;
                                $val->container_list = $container;
                                foreach($container as $cont_value) {
                                    //Get Container Price
                                    $container_price = FoodContainerPrice::select('id', 'product_id','product_option_id', 'product_option_value_id', 'price','discount','discount_price')->where('product_id', '=', $val->pro_id)->where('product_option_value_id', '=', $cont_value->id)->get();
                                    $cont_value->container_price = $container_price;
                                }

                            }

                        }

                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'search_criteria' => $search_criteria, 'menu' => $menu);
                    } else {
                        $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                        $response = Response::json($response_array);
                    }
                }

                $response = Response::json($response_array);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }
        return $response;
    }

    //Food Add To Cart
    public function FoodAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'cart_type' => 'required',
            'attribute_id' => 'required',
            'total_price' => 'required',
	        'cart_sub_type' => 'required'

        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();


      

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                //$product = Products::where('pro_id', '=', $input['product_id'])->where('food_type', '=', 'external')->first();
                $product = Products::where('pro_id', '=', $input['product_id'])->first();

 
                if ($product->pro_qty >= $input['product_qty']) {

                           
                    $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                    if (!empty($cart)) {
                        $attribute = CartServiceAttribute::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('product_id', $input['product_id'])->where('attribute_id', $input['attribute_id'])->distinct()->first();
                        if (!empty($attribute)) {
                            $cart_pro_qnty = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('product_id','=', $input['product_id'])->first();
                        if(!empty($cart_pro_qnty)){
                        $qnty = $product->pro_qty + $cart_pro_qnty->quantity;
                       //Products::where('pro_id','=', $input['product_id'])->update(['pro_qty'=> $qnty]);
                        }
                            /*$cart_pro = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('product_id', $attribute->product_id)->delete();*/

                     //Update quantity

                               $cart_pro = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('product_id', $attribute->product_id)->count();

                           if($cart_pro!='') {
                           $cartquantity = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('product_id', $attribute->product_id)->first();

                           $quantity1 = $cartquantity->quantity;
                           $totalpricepro = $cartquantity->total_price;
                          // $proprice =  $cartquantity->price;
                           $inputquantity = $input['product_qty'];
                           $totalquantity =  $quantity1+$inputquantity;
                           $totalprice = $input['total_price'];
                           $totalproprice =($totalprice)+($totalpricepro); 

                         CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])
                          ->update([
                            'quantity' => $totalquantity,
                            'total_price' =>  $totalproprice

                            ]);
                      }
                       $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                     $response = Response::json($response_array, 200);
                       return $response;






                           /* $cart_option = CartOption::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('product_id', $attribute->product_id)->delete();

                            $cart_option_value = CartOptionValue::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('product_id', $attribute->product_id)->delete();
                            $cart_attribute = CartServiceAttribute::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('product_id', $attribute->product_id)->where('attribute_id', $attribute->attribute_id)->delete();*/
                        }

                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }

                    $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first(); 
                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $input['cart_type'];
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['product_id'] = $input['product_id'];
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['attribute_id'] = $input['attribute_id'];
                    if(!empty($menu)){

                    $cart_attribute_data['attribute_title'] = $menu->attribute_title;
                    $cart_attribute_data['attribute_title_ar'] = $menu->attribute_title_ar;

                    }
                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry

                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;

                   // $cart_product_data['category_id'] = $category->mc_id;


                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $input['product_id'];
                    $cart_product_data['status'] = 1;
                    if (!empty($input['product_qty'])) {
                        $cart_product_data['quantity'] = $input['product_qty'];
                       
                    } 
                    $cart_product_data['total_price'] = $input['total_price'];                     
                    $cart_product_data['shop_vendor_id'] = $category->parent_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['pro_price'] = $product->pro_price;
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry

                    $updated_pro = Products::where('pro_id', '=', $input['product_id'])->first();
                    $dta = $updated_pro->pro_qty - $input['product_qty'];
                    //Products::where('pro_id','=', $input['product_id'])->update(['pro_qty'=> $dta]);

                    //$product_options = ProductOptionValue::where('id', $input['product_option_value'])->with('getProductOption')->distinct()->first();
                    $product_options = ProductOptionValue::where('product_id', $input['product_id'])->with('getProductOption')->distinct()->first();


                    if (!empty($product_options)) {
                        $cart_option_data = array();
                        $cart_option_data['cart_type'] = $input['cart_type'];
                        $cart_option_data['product_option_id'] = $product->option_id;
                        $cart_option_data['cart_id'] = $cart->id;
                        $cart_option_data['product_id'] = $input['product_id'];
                        CartOption::create($cart_option_data); //cart option entry

                        $cart_option_value = array();
                        $cart_option_value['cart_type'] = $input['cart_type'];
                        $cart_option_value['cart_id'] = $cart->id;
                        $cart_option_value['product_id'] = $input['product_id'];
                        $cart_option_value['product_option_id'] = $product_options->product_option_id;
                        $cart_option_value['product_option_value_id'] = $product_options->id;
                        $cart_option_value['quantity'] = $input['product_qty'];
                      
                        if($product_options->value !='0' && $product_options->value!='')
                        {
                        $cart_option_value['value'] = $product_options->value;
                        }
                        else
                        {
                        $cart_option_value['value'] = $input['total_price'];
                        }

                      
                   

                        $cart_option_value['status'] = 1;

                        $cart_option_value['option_title'] = $product_options->getProductOption->option_title;
                        $cart_option_value['option_title_ar'] = $product_options->getProductOption->option_title_ar;
                        $cart_option_value['option_value_title'] = $product_options->option_title;
                        $cart_option_value['option_value_title_ar'] = $product_options->option_title_ar;
                        $cart_option_value['price'] = $product_options->price;
                        $cart_option_value['no_person'] = $product_options->no_person;

                        
                        $cart_value = CartOptionValue::create($cart_option_value); //cart option value entry

                        /*$total_price['total_price'] = $cart_product->total_price + $cart_value->value * $cart_value->quantity;

                        $update_product = CartProduct::where('product_id', '=', $input['product_id'])->update($total_price);*/

                    }

                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);

                } else if ($product->pro_qty <= $input['product_qty'] && $product->pro_qty > 0) {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.QUANTITY_ERROR').$product->pro_qty, 'code' => 200]);
                    $response = Response::json($response_array, 200);

                } else if ($product->pro_qty == 0) {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.PRODUCT_ERROR'), 'code' => 200]);
                    $response = Response::json($response_array, 200);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }
    //External Food Add To Cart
    public function externalFoodAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'cart_type' => 'required'
        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.
                '_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

                 $catg_pro = Products::where('pro_mc_id', '=', $input['product_id'])->first();

             
                $User = Auth::user();
                $product = Category::where('mc_id', '=', $input['product_id'])->first();
                if (!empty($product)) {

                    
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                    if (!empty($cart)) {
                        $cart_pro = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('category_id', $input['product_id'])->first();
                        if (!empty($cart_pro)) {

                            $cart_food_dish = CartExternalFoodDish::where('cart_id', '=', $cart->id)->where('category_id', $cart_pro->category_id)->delete();
                            $cart_attribute = CartServiceAttribute::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('category_id', $cart_pro->category_id)->delete();
                            $cart_pro = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('category_id', $input['product_id'])->delete();
                        }

                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }

                        $cart_product_data = array();
                        $cart_product_data['merchant_id'] = $product->vendor_id;
                        $cart_product_data['shop_id'] = $product->mc_id;

                        


                        $cart_product_data['cart_type'] = $input['cart_type'];
                        $cart_product_data['cart_sub_type'] = 'buffet';
                        $cart_product_data['cart_id'] = $cart->id;
                        $cart_product_data['category_id'] = $input['product_id'];
                       
                          

                        $cart_product_data['shop_vendor_id'] = $catg_pro->pro_mc_id;
                        $cart_product_data['product_id'] = $catg_pro->pro_id;

                        //$cart_product_data['shop_vendor_id'] = $product->parent_id;
                        $cart_product_data['pro_title'] = $product->mc_name;
                        $cart_product_data['pro_title_ar'] = $product->mc_name_ar;
                        $cart_product_data['pro_desc'] = $product->mc_discription;
                        $cart_product_data['pro_desc_ar'] = $product->mc_discription_ar;
                        $cart_product_data['pro_Img'] = $product->mc_img;
                    
                   // $cart_product = CartProduct::create($cart_product_data); //cart product entry
                    if (isset($input['menu']['item'])) {
                        $ShopArray = array();
                        foreach($input['menu']['item'] as $value) {
                        $menu = ServiceAttribute::select('id', 'attribute_title_ar','attribute_title')->where('id', '=', $value['menu_id'])->first();
                        
                        
                            $cart_attribute_data = array();
                            $cart_attribute_data['cart_type'] = $input['cart_type'];
                            $cart_attribute_data['cart_id'] = $cart->id;
                            $cart_attribute_data['category_id'] = $input['product_id'];
                            
                             $cart_product_data['status'] = 1;
 


                            array_push($ShopArray, $input['product_id']);

                            $cart_attribute_data['attribute_id'] = $value['menu_id'];
                            $cart_attribute_data['attribute_title'] = $menu->attribute_title;
                            $cart_attribute_data['attribute_title_ar'] = $menu->attribute_title_ar;
                            $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry*/
                             
                            foreach($value['dish'] as $val) {
                            $cart_product_data['product_id'] = $val['dish_id'];
                            $cart_product_data['quantity'] = $val['qty'];
                            $cart_product_data['container_id'] = $val['container_id'];


                            $getInfoCount = DB::table('nm_product_to_option_price')->where('product_id',$val['dish_id'])->where('product_option_value_id',$val['container_id'])->count();

                            if($getInfoCount >=1)
                            {
                            $getInfos = DB::table('nm_product_to_option_price')->where('product_id',$val['dish_id'])->where('product_option_value_id',$val['container_id'])->first();


                            $discounPrice = $getInfos->discount_price;
                            if($discounPrice>=1)
                            {
                            $OpPrice =  $getInfos->discount_price;  
                            }
                            else
                            {
                            $OpPrice =  $getInfos->price;    
                            }

                            $Tprice = $val['qty'] * $OpPrice;
                            $cart_product_data['total_price'] = $Tprice;

                            }
                            else
                            {
                            $cart_product_data['total_price'] = $input['total_price'];
                            }


                            CartProduct::create($cart_product_data); //cart product entry

                             $container_price = FoodContainerPrice::where('product_id', '=', $val['dish_id'])->where('product_option_value_id', '=', $val['container_id'])->first();

                            $container = ProductOptionValue::select('id', 'product_id', 'short_name', 'image', 'no_person')->where('id', '=',$val['container_id'])->where('product_id', '=', $input['product_id'])->first(); 

                            $food = Products::select('pro_id', 'pro_title_ar','pro_title', 'pro_Img', 'pro_price')->where('pro_id', '=', $val['dish_id'])->first();


                                $cart_external_food = array();
                                $cart_external_food['cart_id'] = $cart->id;
                                $cart_external_food['category_id'] = $input['product_id'];
                                $cart_external_food['external_food_menu_id'] = $value['menu_id'];
                                $cart_external_food['external_food_dish_id'] = $val['dish_id'];
                                     $cart_external_food['product_id'] = $val['dish_id'];
                                $cart_external_food['container_id'] = $val['container_id'];
                                $cart_external_food['quantity'] = $val['qty'];


                                if($container_price->discount_price !='' && $container_price->discount_price >=1)
                                {
                                  $cart_external_food['price'] = $container_price->discount_price * $val['qty'];  
                                }
                                else
                                {
                                    $cart_external_food['price'] = $container_price->price * $val['qty'];
                                }


                                
			                    $cart_external_food['status'] = 1;
                                $cart_external_food['dish_name'] = $food->pro_title;
                                $cart_external_food['dish_name_ar'] = $food->pro_title_ar;
                                $cart_external_food['container_title'] = $container->short_name;
                                $cart_external_food['img'] = $food->pro_Img;
                                $cart_external_food['no_people'] = $container->no_person;
                                $cart_external_food['container_image'] = $container->image;
                                $cart_value = CartExternalFoodDish::create($cart_external_food); //external food entery 

                            
                               }
			    }
			}
			
		 
		 /*$cart_dish_price = CartExternalFoodDish::where('cart_id', '=', $cart->id)->where('category_id', '=', $input['product_id'])->get();
		 $total = 0;
		 foreach($cart_dish_price as $key => $value) {
		 	$total += $value->price;
                 }
		 $total_price['total_price'] = $total;
		 $update_product = CartProduct::where('category_id', '=', $input['product_id'])->update($total_price);*/

                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.
                        '_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);

                } else {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.
                        '_mob_lang.CART_ERROR'), 'code' => 500]);
                    $response = Response::json($response_array, 500);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.
                    '_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }

    /**Get add to cart  data**/
    public function getFoodAddToCart(Request $request) {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();

                $food_pro = array();
                $cart_pro = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_mc_id,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_disprice')->first();

             

                //get food product  product 

                if (!empty($cart) && !empty($cart_pro)) {

                    if (isset($cart_pro)) {
                            // get Date(type) data from add to cart  
                            $attribute = CartServiceAttribute::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->with('getProductServiceAttribute:id,services_id,attribute_title,attribute_title_ar')->first();

                            $category = Category::select('mc_id','mc_img', 'mc_name', 'mc_name_ar','address','address_ar', 'mc_status','city_id','longitude','latitude')->where('mc_id', '=', $cart_pro->getProduct[0]->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();

                            $food_pro['pro_id'] = $cart_pro->getProduct[0]->pro_id;
                            if ($input['language_type'] == 'en') {

                            if(!empty($category)){
                            $food_pro['category_name'] = $category->mc_name;
                            $food_pro['category_img'] = $category->mc_img;
                            $food_pro['address'] = $category->address;
                            $food_pro['city_name'] = $category->getCity->ci_name;
                            $food_pro['longitude'] = $category->longitude;
                            $food_pro['latitude'] = $category->latitude;  

                            }


                            if(isset($attribute->getProductServiceAttribute[0]->attribute_title) && $attribute->getProductServiceAttribute[0]->attribute_title!='')
                            {
                            $food_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title;
                            }
                            else
                            {
                            $food_pro['attribute_title'] = '';          
                            }
 
                            $food_pro['pro_title'] = $cart_pro->getProduct[0]->pro_title;
                            $food_pro['pro_desc'] = $cart_pro->getProduct[0]->pro_desc;


                            $product_optionsa = ProductOptionValue::where('option_title','!=', 'Per Piece')->where('product_id', $input['product_id'])->with('getProductOption')->distinct()->count();
                            //$food_pro['pro_price'] = $cart_pro->getProduct[0]->pro_price;
 

                                if($product_optionsa >=1){
                                    $product_options = ProductOptionValue::where('option_title','!=', 'Per Piece')->where('product_id', $input['product_id'])->with('getProductOption')->distinct()->first();

                                    if($product_options->value !='0' && $product_options->value!='')
                                    {
                                    $food_pro['pro_price'] = $product_options->value;
                                    }
                                    else
                                    {
                                    $food_pro['pro_price'] = $cart_pro->getProduct[0]->pro_price;  
                                    }


                                    if($product_options->discount_price !='0' && $product_options->discount_price!='')
                                    {
                                    $food_pro['pro_disprice'] = $product_options->discount_price;
                                    }
                                    else
                                    {
                                    $food_pro['pro_disprice'] = $cart_pro->getProduct[0]->pro_price;   
                                    }

                                }
                                else
                                {
                                    $food_pro['pro_price'] = $cart_pro->getProduct[0]->pro_price;  
                                    $food_pro['pro_disprice'] = $cart_pro->getProduct[0]->pro_price;  
                                }
                            

                        } else {
                            if(!empty($category)){
                               $food_pro['category_name'] = $category->mc_name_ar;
                               $food_pro['category_img'] =  $category->mc_img;
                               $food_pro['address'] =       $category->address_ar; 
                               $food_pro['city_name'] =     $category->getCity->ci_name_ar;
                               $food_pro['longitude'] =     $category->longitude;
                               $food_pro['latitude'] =      $category->latitude;
                            } 

                        if(isset($attribute->getProductServiceAttribute[0]->attribute_title_ar) && $attribute->getProductServiceAttribute[0]->attribute_title_ar!='')
                        {
                          $food_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title_ar;  
                        }
                        else
                        {
                            $food_pro['attribute_title'] ='';
                        }

                     $product_options = ProductOptionValue::where('option_title','!=', 'Per Piece')->where('product_id', $input['product_id'])->with('getProductOption')->distinct()->first();  
                            $food_pro['pro_title'] = $cart_pro->getProduct[0]->pro_title_ar;
                            $food_pro['pro_desc'] = $cart_pro->getProduct[0]->pro_desc_ar;
                           

                            if($product_options->value !='0' && $product_options->value!='')
                            {
                            $food_pro['pro_price'] = $product_options->value;
                            }
                            else
                            {
                            $food_pro['pro_price'] = $cart_pro->getProduct[0]->pro_price;  
                            }


                            if($product_options->discount_price !='0' && $product_options->discount_price!='')
                            {
                            $food_pro['pro_disprice'] = $product_options->discount_price;
                            }
                            else
                            {
                            $food_pro['pro_disprice'] = $cart_pro->getProduct[0]->pro_price;   
                            }

                        }

                        /*if($cart_pro->getProduct[0]->pro_disprice=='' || $cart_pro->getProduct[0]->pro_disprice==0) {
                                 $food_pro['pro_price'] = $cart_pro->getProduct[0]->pro_price;

                               } else {

                                  $food_pro['pro_price'] = $cart_pro->getProduct[0]->pro_disprice;
                               }*/
                        //$food_pro['pro_price'] = $cart_pro->getProduct[0]->pro_price;

                              

                        $food_pro['pro_Img'] = $cart_pro->getProduct[0]->pro_Img;
                        $food_pro['pro_status'] = $cart_pro->getProduct[0]->pro_status;
                        if($cart_pro->quantity > 0){
                           $food_pro['pro_qty'] = $cart_pro->quantity; 
                           $food_pro['total_price'] = $cart_pro->total_price;
                        }
                        

                    }

                  

  $cart_option_value_count = CartOptionValue::where('product_id', '=', $input['product_id'])->where('cart_id', '=', $cart->id)->where('cart_type', '=', 'food')->with('getProductOption:id,vendor_id,services_id,option_title,option_title_ar,service_hour')->with('getProductOptionValue:id,vandor_id,product_id,product_option_id,option_title,option_title_ar,short_name,value,image,price,no_person,status')->count();

$product_options = ProductOptionValue::where('option_title','!=', 'Per Piece')->where('product_id', $input['product_id'])->with('getProductOption')->distinct()->first();

                     $option_value_data = array();
                    if ( $cart_option_value_count >=1) {



 $cart_option_value  = CartOptionValue::where('product_id', '=', $input['product_id'])->where('cart_id', '=', $cart->id)->where('cart_type', '=', 'food')->with('getProductOption:id,vendor_id,services_id,option_title,option_title_ar,service_hour')->with('getProductOptionValue:id,vandor_id,product_id,product_option_id,option_title,option_title_ar,short_name,value,image,price,no_person,status')->get();






                       //get service
                        foreach($cart_option_value as $key => $value) {

                            if ($input['language_type'] == 'en') {
                                $option_value_data[$key]['option_title'] = $value->getProductOption->option_title;
                                $option_value_data[$key]['value_title'] = $value->getProductOptionValue[0]->option_title;
                            } else {
                                $option_value_data[$key]['option_title'] = $value->getProductOption->option_title_ar;
                                $option_value_data[$key]['value_title'] = $value->getProductOptionValue[0]->option_title_ar;
                            }
                            $option_value_data[$key]['quantity'] = $value->quantity;
                           

                           


                            if($product_options->value !='0' && $product_options->value!='')
                            {
                           $option_value_data[$key]['price'] = $product_options->value;
                            }
                            else
                            {
                           $option_value_data[$key]['price'] = $value->getProductOptionValue[0]->price; 
                            }


                            if($product_options->discount_price !='0' && $product_options->discount_price!='')
                            {
                           $option_value_data[$key]['price'] = $product_options->discount_price;
                            }
                            else
                            {
                           $option_value_data[$key]['price'] =$value->getProductOptionValue[0]->price; 
                            } 

                            $option_value_data[$key]['no_person'] = $value->getProductOptionValue[0]->no_person;
                            $option_value_data[$key]['status'] = $value->getProductOptionValue[0]->status;
                            $option_value_data[$key]['final_price'] = $value->getProductOptionValue[0]->price * $value->quantity;
                        }
                    }

                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'cart_id' => $cart->id, 'food_data' => $food_pro, 'food_option_data' => $option_value_data);
                } else {



                    //Get Buffet data from add to cart
                    $cart_pro = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('category_id', '=', $input['product_id'])->first();
                    //get buffet data
                    if (!empty($cart) && !empty($cart_pro)) {

                        if (isset($cart_pro)) {
                            $external_foods = array();
                            $buffet_branch = array(); 
                            $external_food = CartExternalFoodDish::where('cart_id', '=', $cart->id)->where('category_id', '=',  $input['product_id'])->with('getServiceAttribute')->with('getFood:pro_id,pro_title,pro_title_ar,pro_Img,pro_qty,pro_price,pro_status')->with('getContainer:id,vandor_id,product_id,product_option_id,option_title,option_title_ar,short_name,price,no_person')->get();

                            if (isset($external_food)) {
                                foreach($external_food as $key => $value) {
                                    //get external food

                                    $container_price = FoodContainerPrice::where('product_id', '=', $value->external_food_dish_id)->where('product_option_value_id', '=', $value->container_id)->first();

                                    if ($input['language_type'] == 'en') {
                                        $external_foods[$key]['menu_name'] = $value->getServiceAttribute[0]->attribute_title;
                                        $external_foods[$key]['dish_name'] = $value->getFood[0]->pro_title;
                                        $external_foods[$key]['container_title'] = $value->getContainer[0]->option_title;
                                    } else {
                                        $external_foods[$key]['menu_name'] = $value->getServiceAttribute[0]->attribute_title_ar;
                                        $external_foods[$key]['dish_name'] = $value->getFood[0]->pro_title_ar;
                                        $external_foods[$key]['container_title'] = $value->getContainer[0]->option_title;
                                    }
                                    $external_foods[$key]['dish_id'] = $value->getFood[0]->pro_id;

                                    $external_foods[$key]['menu_id'] = $value->external_food_menu_id;

                                    $external_foods[$key]['container_id'] = $value->container_id;

                                    $external_foods[$key]['category_id'] = $value->category_id;


                                    $external_foods[$key]['quantity'] = $value->quantity;
                                   
                                     if(trim($container_price->discount_price)=='' || trim($container_price->discount_price)==0) {
                                     $external_foods[$key]['container_price'] = $container_price->price;

                                 } else {

                                  $external_foods[$key]['container_price'] = $container_price->discount_price;
                                }

                                   // $external_foods[$key]['container_price'] = $container_price->price;


                                    $external_foods[$key]['dish_image'] = $value->getFood[0]->pro_Img;
                                    $external_foods[$key]['status'] = $value->getFood[0]->pro_status;

                                    $external_foods[$key]['no_people'] = $value->getContainer[0]->no_person;
                                    if (!empty($container_price)) {

                                        $external_foods[$key]['final_price'] = $container_price->price * $value->quantity;

                                    }

                                    $buffet = Category::where('mc_status', '=', 1)->where('mc_id', '=', $value->category_id)->select('mc_id', 'mc_name_ar', 'mc_name', 'mc_img', 'mc_discription', 'city_id','address','address_ar')->with('getCity:ci_id,ci_name_ar,ci_name')->first();

                                }

                                if (isset($buffet)) {
                                    if ($input['language_type'] == 'en') {

                                        $buffet_branch[$key]['mc_name'] = $buffet->mc_name;
                                        $buffet_branch[$key]['mc_img'] = $buffet->mc_img;
                                        $buffet_branch[$key]['mc_discription'] = $buffet->mc_discription;
                                        $buffet_branch[$key]['city_name'] = $buffet->getCity->ci_name;
                                        $buffet_branch[$key]['address'] = $buffet->address;
                                    } else {
                                        $buffet_branch[$key]['mc_name_ar'] = $buffet->mc_name_ar;
                                        $buffet_branch[$key]['mc_img'] = $buffet->mc_img;
                                        $buffet_branch[$key]['mc_discription'] = $buffet->mc_discription;
                                        $buffet_branch[$key]['city_name'] = $buffet->getCity->ci_name_ar;
                                        $buffet_branch[$key]['address'] = $buffet->address_ar;
                                    }
                                }
                            }
                        }
                        $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'cart_id' => $cart->id, 'external_foods' => $external_foods, 'buffet_branch' => $buffet_branch);
                    } else {
                        $response_array = array('data' => ['success' => false, 'message' => 'cart data not found & cart type must be food', 'code' => 500]);
                    }
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    /**delete Food Data add to cart  data**/
    public function deleteFoodData(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'token' => 'required',
        ]);
        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();

                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                $cart_pro = CartProduct::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->first();
                $product = Products::where('pro_id','=', $input['product_id'])->first();
                $qnty = $product->pro_qty + $cart_pro->quantity;
                //Products::where('pro_id','=', $input['product_id'])->update(['pro_qty'=> $qnty]);
                if (!empty($cart) && !empty($cart_pro)) {

                    $cart_option = CartOption::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->delete(); //delete product option 

                    $cart_option_value = CartOptionValue::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->delete(); //delete product option value
                    $cart_service_attribute = CartServiceAttribute::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->delete();
                    //delete service attribute

                    $cart_pro = CartProduct::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->delete();
                    //delete product

                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.DELETE_CART_DATA'), 'code' => 200]);

                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

     //Update Quantity
    public function updateQuantity(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'token' => 'required',
            'quantity'=>'required',
            'total_price'=>'required'
        ]);
        $locale = '';
        if(isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();


      

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
              //CheckQuantity
                   if(isset($input['type']) && $input['type']=='Rent') {
                    $productquantity = DB::table('nm_product_option_value')->select('value','product_id')->where('product_id',$input['product_id'])->first();
                    $quantitycheck = $productquantity->value;

                   } else {
                    $productquantity = Products::select('pro_qty','pro_id')->where('pro_id',$input['product_id'])->first();
                    $quantitycheck = $productquantity->pro_qty;

                 }

                 if($quantitycheck < $input['quantity'] && $quantitycheck > 0  && $input['qtyact']==1) {
                     
                      $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.QUANTITY_ERROR').$quantitycheck, 'code' => 200]);
                    $response = Response::json($response_array, 200);
                    return $response;


                 } else {


                    if(isset($input['id']) && $input['id']!='')
                    {

                     $cart_pro =   CartProduct::where('id', '=', $input['id'])->where('cart_id', '=', $cart->id)->where('product_id','=', $input['product_id'])->update(['quantity'=> $input['quantity'],'total_price'=> $input['total_price']]);   
                    }
                    else
                    {
                        $cart_pro =   CartProduct::where('cart_id', '=', $cart->id)->where('product_id','=', $input['product_id'])->update(['quantity'=> $input['quantity'],'total_price'=> $input['total_price']]);

                         $datescart_pro =   DB::table('nm_cart_option_value')->where('cart_id', '=', $cart->id)->where('product_id','=', $input['product_id'])->update(['quantity'=> $input['quantity']]);
                    }
                                     
                     $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.UPDATE_QUNATITY'), 'code' => 200]);
                      $response = Response::json($response_array, 200);
                      return $response;
                 } 


               

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.UPDATE_QUNATITY_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
                 return $response;
            }
            
        }
       
    }


 //Update pacckages

       //Update Quantity
    public function updateQuantityPackage(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'token' => 'required',
            'quantity'=>'required',
            'total_price'=>'required'
        ]);

        $locale = '';
        if(isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();

              //CheckQuantity
                $productquantity = Products::select('pro_qty','pro_id')->where('pro_id',$input['product_id'])->first();
                    $quantitycheck = $productquantity->pro_qty;
                 if($quantitycheck < $input['quantity'] && $quantitycheck > 0 && $input['qtyact']==1) {
                     
                      $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.QUANTITY_ERROR').$quantitycheck, 'code' => 200]);
                    $response = Response::json($response_array, 200);
                    return $response;


                 } else {




                if(isset($input['id']) && $input['id']!='')
                {
                //Main KOSHA design case only
                $cart_pro =   CartServiceAttribute::where('cart_id', '=', $cart->id)->where('product_id','=', $input['product_id'])->update(['quantity'=> $input['quantity'], 'total_price'=> $input['total_price']]);

                CartProduct::where('quantity', '!=', 0)->where('cart_id', '=', $cart->id)->where('product_id','=', $input['product_id'])->update(['total_price'=> $input['total_price'],'quantity'=> $input['quantity']]);

                    $total_price = 0;


                    $getProinfo = CartProduct::where('cart_id', '=', $cart->id)->where('product_id','=', $input['product_id'])->first();
                    $Cid = $getProinfo->category_id;
 
                    $Infos = CartProduct::where('quantity', '!=', 0)->where('buy_rent','design')->where('cart_sub_type', '=', 'cosha')->where('cart_id', '=', $cart->id)->where('category_id',$Cid)->get();
                    foreach($Infos as $vals)
                    {
                    $total_price= $total_price+ $vals->total_price;
                    }

                    CartProduct::where('quantity', 0)->where('buy_rent','design')->where('category_id',$Cid)->where('cart_id', '=', $cart->id)->where('cart_sub_type','=', 'cosha')->update(['total_price'=> $total_price]);

                }
                else
                {
                $cart_pro =   CartServiceAttribute::where('cart_id', '=', $cart->id)->where('product_id','=', $input['product_id'])->update(['quantity'=> $input['quantity'],'total_price'=> $input['total_price']]); 
                CartProduct::where('cart_id', '=', $cart->id)->where('product_id','=', $input['product_id'])->update(['total_price'=> $input['total_price']]);

                }



                 

                     
                     $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.UPDATE_QUNATITY'), 'code' => 200]);
                      $response = Response::json($response_array, 200);
                      return $response;
                 } 


               

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.UPDATE_QUNATITY_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
                 return $response;
            }
            
        }
       
    }


 //End packages    


    /**delete Buffet Data add to cart  data**/
    public function deleteBuffetData(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'token' => 'required',
        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);
        
        $input = $request->all();

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();

                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                $cart_pro = CartProduct::where('category_id', '=', $input['product_id'])->where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->first();
                if (!empty($cart) && !empty($cart_pro)) {

                    $cart_service_attribute = CartServiceAttribute::where('category_id', '=', $input['product_id'])->where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->delete();
                    //delete service attribute

                    $cart_food_dish = CartExternalFoodDish::where('category_id', '=', $input['product_id'])->where('cart_id', '=', $cart->id)->delete();
                    //delete external food dish

                    $cart_pro = CartProduct::where('category_id', '=', $input['product_id'])->where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->delete();
                    //delete product

                    $response_array = array('data' => ['success' => true, 'message' => 'Successfully deleted your cart data', 'code' => 200]);

                } else {

                    $cart_food = CartExternalFoodDish::where('external_food_dish_id', '=', $input['product_id'])->where('cart_id', '=', $cart->id)->first();
                    if (!empty($cart_food)) {

                        $cart_service_attribute = CartServiceAttribute::where('attribute_id', '=', $cart_food->external_food_menu_id)->where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->delete();
                        //delete service attribute

                        $cart_food_dish = CartExternalFoodDish::where('external_food_dish_id', '=', $input['product_id'])->where('cart_id', '=', $cart->id)->delete();
                        //delete external food dish

                        $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.DELETE_CART_DATA'), 'code' => 200]);

                    }

                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    // Increase qunatity in 

      //Update Quantity
    public function updateDishQuantity(Request $request) {
        $validator = Validator::make($request->all(), [
            'menu_id' => 'required',
            'token' => 'required',
            'dish_id'=>'required',
            'container_id'=>'required',
            'quantity'=>'required',
            'total_price'=>'required',
            'category_id'=>'required',
            'container_total_price'=>'required',
        ]);
                               

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();



      

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
              
                  $cart_pro =   CartExternalFoodDish::where('cart_id', '=', $cart->id)->where('category_id','=', $input['category_id'])->where('external_food_menu_id','=', $input['menu_id'])->where('external_food_dish_id','=', $input['dish_id'])->where('container_id','=', $input['container_id'])->update(['quantity'=> $input['quantity'], 'price'=> $input['container_total_price']]);
 

                   CartProduct::where('product_id', '=', $input['dish_id'])->where('cart_id', '=', $cart->id)->where('category_id','=', $input['category_id'])->where('container_id','=', $input['container_id'])->update(['total_price'=> $input['container_total_price']]);



                
                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.UPDATE_QUNATITY'), 'code' => 200]);
                      $response = Response::json($response_array, 200);
                      return $response;
             

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.UPDATE_QUNATITY_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
                 return $response;
            }
            
        }
       
    }

}

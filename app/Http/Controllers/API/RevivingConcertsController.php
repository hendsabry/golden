<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Http\Models;
use App\Category;
use App\MusicGallery;
use App\SingerAndBand;
use App\ServiceInquiry;
use App\City;
use App\ProductOption;
use App\ProductOptionValue;
use App\Products;
use App\ServiceAttribute;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartProductRent;
use App\Merchant;
use Auth;
use App\User;
use Response;
use DB;

class RevivingConcertsController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |   Route::get('/', 'HomeController@showWelcome');
    |
    */
    public $confirmation_user_email;
    public $confirmation_user_name;
    public $confirmation_merchant_email;
    public $confirmation_merchant_name;
    public function __construct() {}

    /**Get Singer's and Band info.**/

    /*
       Sub_Category_id(2th lavel category in category table) become category_id in music table to get full Singer's List.
    */
    public function getMusicAndBandList(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'language_type' => 'required|max:255',
            'city_id' => 'required',
            'token' => 'required'

        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['category_id'])->first(); //GET MAIN CATEGORY

                if (!empty($category)) {
                    $sub_catg = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->first(); //GET SUB CATEGORY

                    $city = City::select('ci_id', 'ci_name', 'ci_name_ar')->where('ci_id', '=', $input['city_id'])->first();
                    // get city         

                    if ($input['city_id'] != '' && $input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */

                        $singer_list = SingerAndBand::select('id', 'name_ar as name', 'about_ar as about', 'vendor_id', 'category_id', 'city_id', 'address_ar as address','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name', 'video_url', 'image', 'category_type')->where('status', '=', 1)->where('category_id', '=', $input['subcategory_id'])->with('getCity:ci_id,ci_name')->orderBy('name', 'ASC')->paginate(9); //GET SINGER"S LIST
                        if (!$singer_list->isEmpty()) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'تم العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'longitude' => $category->longitude, 'latitude' => $category->latitude, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'singer_list' => $singer_list);
                        } else {
                            $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => 'آسف! لم نتمكن من العثور على مطربين أو فرقة موسيقية في المدينة المختارة.'], 'city_id' => $city->ci_id, 'city_name' => $city->ci_name_ar, 'category_id' => $category->mc_id, 'longitude' => $category->longitude, 'latitude' => $category->latitude, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar);
                        }

                    } else {
                        /*FOR ENGLISH LANGUAGE*/

                        $singer_list = SingerAndBand::select('id', 'name', 'about', 'vendor_id', 'category_id', 'city_id', 'address','terms_conditions','terms_condition_name', 'video_url', 'image', 'category_type')->where('status', '=', 1)->where('category_id', '=', $input['subcategory_id'])->with('getCity:ci_id,ci_name')->orderBy('name', 'ASC')->paginate(9); //GET SINGER"S LIST
                        if (!$singer_list->isEmpty()) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name,'longitude' => $category->longitude, 'latitude' => $category->latitude, 'singer_list' => $singer_list);
                        } else {
                            $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => 'Sorry! We could not find a singers or band in the selected city.'], 'city_id' => $city->ci_id, 'city_name' => $city->ci_name, 'category_id' => $category->mc_id, 'longitude' => $category->longitude, 'latitude' => $category->latitude,'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name);
                        }

                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'Sorry! We could not find the service you looking for.']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    /**Get Singer's and Band info.**/

    /*
       Sub_Category_id(2th lavel category in category table) become category_id in music table to get full Singer's info.
    */
    public function getMusicAndBandInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
            'token' => 'required'

        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['category_id'])->first(); //GET MAIN CATEGORY
                $client_review = Category::get_singer_band_review($input['subcategory_id']);

                if (!empty($category)) {
                    $sub_catg = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->first(); //GET SUB CATEGORY

                    if ($input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                        if (!$client_review->isEmpty()) {
                            $review = $client_review;
                        } else {
                            $review = array();
                        }

                        $singer_list = SingerAndBand::select('id', 'name_ar as name', 'about_ar as about', 'vendor_id', 'category_id', 'city_id', 'address_ar as address','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name', 'video_url', 'image', 'category_type')->where('status', '=', 1)->where('category_id', '=', $input['subcategory_id'])->where('id', '=', $input['product_id'])->with('getCity:ci_id,ci_name')->with('CategoryGallery:title,image,music_id')->first(); //GET SINGER"S INFO
                        if (!empty($singer_list)) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'تم العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'singer_list' => $singer_list, 'client_comments' => $review,'longitude' => $category->longitude, 'latitude' => $category->latitude);
                        } else {
                            $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => 'لم يتم العثور على بيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar,'longitude' => $category->longitude, 'latitude' => $category->latitude);
                        }

                    } else {
                        /*FOR ENGLISH LANGUAGE*/
                        if (!$client_review->isEmpty()) {
                            $review = $client_review;
                        } else {
                            $review = array();
                        }

                        $singer_list = SingerAndBand::select('id', 'name', 'about', 'vendor_id', 'category_id', 'city_id', 'address', 'terms_conditions','terms_condition_name', 'video_url', 'image', 'category_type')->where('status', '=', 1)->where('category_id', '=', $input['subcategory_id'])->where('id', '=', $input['product_id'])->with('getCity:ci_id,ci_name')->with('CategoryGallery:title,image,music_id')->first(); //GET SINGER"S INFO
                        if (!empty($singer_list)) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'singer_list' => $singer_list, 'client_comments' => $review,'longitude' => $category->longitude, 'latitude' => $category->latitude);
                        } else {
                            $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => 'Data not found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name,'longitude' => $category->longitude, 'latitude' => $category->latitude);
                        }

                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'Sorry! We could not find the service you looking for.']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    /**Service Inquiry for Singer's and Band info.**/
    public function Inquiry(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'hall' => 'required',
            'occasion_type' => 'required',
            'city_id' => 'required',
            'location' => 'required|max:255',
            'duration' => 'required',
            'date' => 'required',
            'time' => 'required',
            'quote_for' => 'required',
            'user_comment' => 'required|max:255',
            'token' => 'required',
            'language_type' => 'required|max:255',

        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $singer_list = SingerAndBand::where('status', '=', 1)->where('id', '=', $input['product_id'])->first();
                $merchant = Merchant::where('mer_staus', '=', 1)->where('mer_id', '=', $singer_list->vendor_id)->first();
                $city = City::select('ci_id', 'ci_name', 'ci_name_ar')->where('ci_id', '=', $input['city_id'])->first();
                if (!empty($singer_list)) {
                    if($input['language_type'] == 'en'){
                      $input['language_type'] = 1;  
                    }else{
                      $input['language_type'] = 2;
                    }
                    $input['relate_with'] = $singer_list->id;
                    $input['merchant_id'] = $singer_list->vendor_id;
                    $input['singer_name'] = $singer_list->name;
                    $input['user_id'] = $User->cus_id;

                    $input['mc_name'] = $singer_list->name;
                    $input['mc_name_ar'] = $singer_list->name_ar;
                    $input['mc_discription'] = $singer_list->about;
                    $input['mc_discription_ar'] = $singer_list->about_ar;
                    $input['mc_city'] = $singer_list->city_id;
                    $input['mc_img'] = $singer_list->image;
                    $input['mc_address'] = $singer_list->address_image;
                    $input['mc_address_ar'] = $singer_list->address_image;
                    $input['status'] = 1;
                    $input['occasion_type_id'] = $input['occasion_type'];
                   $getOccasion    = DB::table('nm_business_occasion_type')->where('id',$request->occasion_type)->first();        
                 
                   if($request->language_type!= 'en') 
                    {
                    $title  = $getOccasion->title_ar;
                   
                    }
                    else
                    {
                    $title = $getOccasion->title;
                    
                    }
                      
                    $input['occasion_type'] = $title;

                


                    $input['date'] = date("Y-m-d", strtotime($input['date']));
                  
                    ServiceInquiry::create($input); //cart entery

                  
                    $this->confirmation_merchant_name = $merchant->mer_fname.' '.$merchant->mer_lname;
                    $this->confirmation_merchant_email = $merchant->mer_email;
                    $this->confirmation_user_email = $User->email;
                    $this->confirmation_user_name = "Golden Cage";


                    if($input['quote_for'] == 'singer'){
                    $subject = 'Singer Quote Request'; 
                    }elseif ($input['quote_for'] == 'band') {
                    $subject = 'Popular Band Quote Request'; 
                    }


                    Mail::send('emails.mobile_singer_band_Inquiry',
                    array(

                    'name' => $merchant->mer_fname.' '.$merchant->mer_lname,
                    'vendor_name' => $merchant->mer_fname.' '.$merchant->mer_lname,
                    'singer_name' => $singer_list->name,
                    'hall' => $request->hall,
                    'occasion_type' => $request->occasion_type,
                    'city' => $city->ci_name,
                    'location' => $request->location,
                    'duration' => $request->duration,
                    'date' => $request->date,
                    'time' => $request->time,
                    'comments' => $request->user_comment,
                    'lang' => 'ar_mob_lang',
                    'LANUAGE' => 'en_mob_lang',
                    ), function($message) use($subject)
                    {
                    $message->to($this->confirmation_merchant_email,$this->confirmation_merchant_name)->subject($subject);
                    });

                    $subject = 'Thank you for Contacting Us ';
                    Mail::send('emails.mobile_revertInquirymail',
                    array(
                    'name' => $User->cus_name,
                    'vendor_name' => $User->cus_name,
                    'lang' => 'ar_mob_lang',
                    'LANUAGE' => 'en_mob_lang',
                    ), function($message) use($subject)
                    {
                    $message->from($this->confirmation_merchant_email, 'Get a Quote | Golden Cage');
                    $message->to($this->confirmation_user_email, $this->confirmation_user_name)->subject($subject);
                    }); 
                    $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => trans($locale.
                        '_mob_lang.INQUIRY_SUCESS')]);
                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'error_code' => 500, 'error' => trans($locale.
                        '_mob_lang.INQUIRY_ERROR')]);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error_code' => 203, 'error' => trans($locale.
                    '_mob_lang.PROFILE_UPDATE_ERROR')]);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    /**Get Music Acoustic info.**/

    /*
       vendor_id(3th lavel category in category table) become pro_mc_id in product table to get Music Acoustic info.
    */
    public function getMusicAcousticInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id' => 'required', //Vendor ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first();

                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($input['vendor_id']);

                if (!empty($category)) {

                    if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                        if (!$client_review->isEmpty()) {
                            $review = $client_review;
                        } else {
                            $review = array();
                        }
                        //GET SUB CATEGORY
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->first();

  
                        //GET VENDOR DETAIL
                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name', 'insurance_amount', 'google_map_address', 'address_image', 'vendor_id','longitude','latitude')->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:title_ar as title,image,category_id')->first();
                        //Get clinic category List
                        $music_acoustic = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'image', 'status')->where('services_id', '=', $input['subcategory_id'])->get();
                        if (!$music_acoustic->isEmpty()) {

                          /*  foreach($music_acoustic as $key => $value) {
                                //get doctor detail according clinic category.
                                $list = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_qty', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price','pro_disprice','pro_discount_percentage','pro_mc_id', 'attribute_id', 'option_id','notes_ar as notes', 'service_hour', 'Insuranceamount')->where('pro_mc_id', '=', $input['vendor_id'])->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(1000); 
                                if ($value->id == 32) {
                                    $product_list = 'Get a quote';
                                } else {
                                    $product_list = $list;
                                }
                                $value->product_list = $product_list;
                                foreach($list as $val) {
                                    $name = explode(',', $val->option_id);
                                    //get service attribute option detail
                                    $product_Option = ProductOption::select('id', 'option_title_ar as option_title', 'button_type_id')->whereIn('id', $name)->with('getBoxType:id,option_title_ar as option_title,type')->get();
                                    //Get service attribute option value.
                                    $product_Option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title_ar as option_title', 'image', 'price','discount_price', 'no_person', 'status','value')->where('product_id', '=', $val->pro_id)->get();

                                    $val->product_Option = $product_Option;
                                    $val->product_Option_value = $product_Option_value;

                                }

                            }

*/



 


       foreach($music_acoustic as $key => $value) {
                                //get doctor detail according clinic category.
                                $list = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_qty', 'pro_desc_ar as pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour', 'Insuranceamount','pro_disprice','notes_ar as notes','pro_discount_percentage')->where('pro_mc_id', '=', $input['vendor_id'])->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10000);
                                if ($value->id == 32) {
                                    $product_list = 'Get a quote';
                                } else {
                                    $product_list = $list;
                                }
                                $value->product_list = $product_list;
                                foreach($list as $val) {
                                    $name = explode(',', $val->option_id);
                                    //get service attribute option detail
                                    $product_Option = ProductOption::select('id', 'option_title_ar as option_title', 'button_type_id')->whereIn('id', $name)->with('getBoxType:id,option_title_ar as option_title,type')->get();
                                    //Get service attribute option value.
                                    $product_Option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title', 'image','price','discount_price','discount', 'no_person', 'status','value')->where('product_id', '=', $val->pro_id)->get();

                                    foreach( $product_Option_value as $vals)
                                    {
                                    $vals->price;
                                    if($vals->discount>=1){
                                     $vals->discount_price = $vals->price - ($vals->price * $vals->discount)/100;   
                                    } 
                                    else
                                    {
                                         $vals->discount_price = 0;   
                                    } 
                                    }

                                    $val->product_Option = $product_Option;
                                    $val->product_Option_value = $product_Option_value;
                                }
                            }

 


                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_detail' => $vendor, 'client_comments' => $review, 'music_acoustic_category' => $music_acoustic);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'يبدو أنه لا يوجد طبيب متاح مع هذه الفئة.']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                        if (!$client_review->isEmpty()) {
                            $review = $client_review;
                        } else {
                            $review = array();
                        }
                        //GET SUB CATEGORY
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name', 'mc_img')->first();

                        //GET VENDOR DETAIL
                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','terms_conditions','terms_condition_name', 'insurance_amount', 'google_map_address', 'address_image', 'vendor_id','longitude','latitude')->with('getCity:ci_id,ci_name')->with('CategoryGallery:title,image,category_id')->first();
                        //Get clinic category List
                        $music_acoustic = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->where('services_id', '=', $input['subcategory_id'])->get();
                        if (!$music_acoustic->isEmpty()) {

                            foreach($music_acoustic as $key => $value) {
                                //get doctor detail according clinic category.
                                $list = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_qty', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour', 'Insuranceamount','pro_disprice','notes','pro_discount_percentage')->where('pro_mc_id', '=', $input['vendor_id'])->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10000);
                                if ($value->id == 32) {
                                    $product_list = 'Get a quote';
                                } else {
                                    $product_list = $list;
                                }
                                $value->product_list = $product_list;
                                foreach($list as $val) {
                                    $name = explode(',', $val->option_id);
                                    //get service attribute option detail
                                    $product_Option = ProductOption::select('id', 'option_title', 'button_type_id')->whereIn('id', $name)->with('getBoxType:id,option_title,type')->get();
                                    //Get service attribute option value.
                                    $product_Option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title', 'image','price','discount_price','discount', 'no_person', 'status','value')->where('product_id', '=', $val->pro_id)->get();
                                    foreach( $product_Option_value as $vals)
                                    {
                                    $vals->price;
                                    if($vals->discount>=1){
                                     $vals->discount_price = $vals->price - ($vals->price * $vals->discount)/100;   
                                    }
                                    else
                                    {
                                         $vals->discount_price = 0;   
                                    } 
                                    }
                                    $val->product_Option = $product_Option;
                                    $val->product_Option_value = $product_Option_value;
                                }
                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_detail' => $vendor, 'client_comments' => $review, 'music_acoustic_category' => $music_acoustic);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'It seems no music acoustic are available with the music acoustic category.']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    /**Service Inquiry for Music Acoustic info.**/
    public function getMusicAcousticInquiry(Request $request) {

        $validator = Validator::make($request->all(), [
           /* 'category_id' => 'required',
            'the_groooms_name' => 'required',
            'hall' => 'required',
            'occasion_type' => 'required',
            'date' => 'required',
            'time' => 'required',
            //'recording_secation' => 'required',
            'music_type' => 'required',
            'singer_name' => 'required',
            'quote_for' => 'required',
            'song_name' => 'required',
            'token' => 'required'*/

        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();


        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $music_acoustic = Category::where('mc_id', '=', $input['category_id'])->first();
 
                $merchant = Merchant::where('mer_staus', '=', 1)->where('mer_id', '=', $music_acoustic->vendor_id)->first();



                if (!empty($music_acoustic)) {
                    if($input['language_type'] == 'en'){
                      $input['language_type'] = 1;  
                    }else{
                      $input['language_type'] = 2;
                    }
                    $input['relate_with'] = $music_acoustic->mc_id;
                    $input['merchant_id'] = $music_acoustic->vendor_id;
                    $input['user_id'] = $User->cus_id;
                    $input['city_id'] = $request->city_id;

                    $input['status'] = 1;
                    $input['date'] = date("Y-m-d", strtotime($input['date']));
                    $input['mc_name'] = $music_acoustic->mc_name;
                    $input['mc_name_ar'] = $music_acoustic->mc_name_ar;
                    $input['mc_discription'] = $music_acoustic->mc_discription;
                    $input['mc_discription_ar'] = $music_acoustic->mc_discription_ar;
                    $input['mc_city'] = $request->city_id;
                    $input['song_name'] = $request->song_name;
                    $input['mc_img'] = $music_acoustic->mc_img;
                    $input['mc_address'] = $music_acoustic->address;
                    $input['mc_address_ar'] = $music_acoustic->address_ar;
                    $input['quote_for'] = 'recording';
                    $input['occasion_type_id'] = $input['occasion_type'];
                    ServiceInquiry::create($input); //cart entery

                    $this->confirmation_merchant_name = $merchant->mer_fname.' '.$merchant->mer_lname;
                    $this->confirmation_merchant_email = $merchant->mer_email;
                    $this->confirmation_user_email = $User->email;
                    $this->confirmation_user_name = "Golden Cage";

                    $subject = 'Acoustics Quote Request'; 

                    Mail::send('emails.mobile_accoustic_Inquiry',
                    array(

                    'name' => $merchant->mer_fname.' '.$merchant->mer_lname,
                    'vendor_name' => $merchant->mer_fname.' '.$merchant->mer_lname,
                    'groom_name' => $request->the_groooms_name,
                    'hall' => $request->hall,
                    'occasion_type' => $request->occasion_type,
                    'date' => $request->date,
                    'time' => $request->time,
                    //'recording_section' => $request->recording_secation,
                    'music_type' => $request->music_type,
                    'singer_name' => $request->singer_name,
                    'song_name' => $request->song_name,
                    'lang' => 'ar_mob_lang',
                    'LANUAGE' => 'en_mob_lang',
                    ), function($message) use($subject)
                    {
                    $message->to($this->confirmation_merchant_email,$this->confirmation_merchant_name)->subject($subject);
                    }); 

                    $subject = 'Thank you for Contacting Us ';
                    Mail::send('emails.mobile_revertInquirymail',
                    array(
                    'name' => $User->cus_name,
                    'vendor_name' => $User->cus_name,
                    'lang' => 'ar_mob_lang',
                    'LANUAGE' => 'en_mob_lang',
                    ), function($message) use($subject)
                    {
                    $message->from($this->confirmation_merchant_email, 'Get a Quote | Golden Cage');
                    $message->to($this->confirmation_user_email, $this->confirmation_user_name)->subject($subject);
                    });                   

                    $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => trans($locale.
                        '_mob_lang.INQUIRY_SUCESS')]);
                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'error_code' => 500, 'error' => trans($locale.
                        '_mob_lang.INQUIRY_ERROR')]);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error_code' => 203, 'error' => trans($locale.
                    '_mob_lang.PROFILE_UPDATE_ERROR')]);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Music Acoustic Add To Cart

    public function MusicAcousticAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required', //Get Product ID
            'product_qty' => 'required', //Get Product Quantity
            //'product_option_value' => 'required', //Get Product Option Value
            'cart_type' => 'required', // Get Cart Type
            'total_price' => 'required',
            'token' => 'required'
        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);
        
        $input = $request->all();

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

                $User = Auth::user();




                $LastID = 0;
              


                 $product = Products::where('pro_id', '=', $input['product_id'])->first();

                if (isset($input['product_option_value']) && trim($input['product_option_value'])!='') {
                 $products = DB::table('nm_product_option_value')->where('option_title', 'Quantity in stock')->where('product_id', '=', $input['product_id'])->first();
                   $quentycheck = $products->value;
              
 
                
                } else {
                
                 $quentycheck = $product->pro_qty;
 
                 }

                if ($quentycheck >= $input['product_qty']) {
                    $category = Category::select('mc_id','insurance_amount', 'mc_name', 'mc_name_ar','address','address_ar', 'mc_status','city_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();


                    if (!empty($cart)) {
 
                $carRent = date("Y-m-d", strtotime($input['rental_date']));
                $ChkProRent = CartProductRent::where('rental_date',$carRent)->where('product_id',$input['product_id'])->where('cart_id',$cart->id)->where('rental_time',$input['rental_time'])->count(); 
                if($ChkProRent >=1)
                {
                // Delete rent product
                $ChkProRentC = CartProductRent::where('rental_date',$carRent)->where('product_id',$input['product_id'])->where('cart_id',$cart->id)->where('rental_time',$input['rental_time'])->first();   

                $ChkIDs = $ChkProRentC->cart_product_id;                
                CartProduct::where('id',$ChkIDs)->delete();
             
                CartOption::where('cart_product_id',$ChkIDs)->delete();
                CartOptionValue::where('cart_product_id',$ChkIDs)->delete();
                CartProductRent::where('cart_product_id',$ChkIDs)->delete();
                $ChkProRent = 0;
                } 
                    $cart_pro = CartProduct::where('cart_type', '=', 'music')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
 

                       $cart_pro = CartProduct::where('buy_rent', '=', 'buy')->where('cart_type', '=', 'music')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->count();  
                      

                        
                    //QUANTITY UPDATE FOR 
                       
 
                          /* if($cart_pro!='' &&   $ChkProRent < 1) {
                           $cartquantity = CartProduct::where('buy_rent', '=', 'buy')->where('cart_type', '=', 'music')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->first();

                           $quantity1 = $cartquantity->quantity;
                           $totalpricepro = $cartquantity->total_price;
                          // $proprice =  $cartquantity->price;

                           $inputquantity = $input['product_qty'];

                           $totalquantity =  $quantity1+$inputquantity;

                           $totalprice = $input['total_price'];

                           $totalproprice =($totalprice)+($totalpricepro); 



                        CartProduct::where('cart_type', '=', 'music')->where('buy_rent', '=', 'buy')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])
                          ->update([
                            'quantity' => $totalquantity,
                            'total_price' =>  $totalproprice

                            ]);
                      }
                       $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                        $response = Response::json($response_array, 200);

*/
                       // return $response;
 

                   
                    $cart_pro_opt = CartOption::where('cart_type', '=', 'music')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    $cart_pro_val = CartOptionValue::where('cart_type', '=', 'music')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    $cart_pro_rent = CartProductRent::where('cart_type', '=', 'music')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                        $cart_pro = 0;
                        $ChkProRent = 0;
                    }

                    if($cart_pro < 1  &&   $ChkProRent < 1) {
                    $cart_product_data = array();
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = 'acoustic';
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $input['product_id'];
                    $cart_product_data['quantity'] = $input['product_qty'];

                    if (isset($input['product_option_value']) && $input['product_option_value']) {
                        /*$cart_product_data['total_price'] = $category->insurance_amount + $product->pro_price * $input['product_qty'];*/
                        $cart_product_data['insurance_amount'] = $product->Insuranceamount;
                        $cart_product_data['total_price'] = $input['total_price'];
                        
                    } else {
                        /*$cart_product_data['total_price'] = $product->pro_price * $input['product_qty'];*/
                        $cart_product_data['total_price'] = $input['total_price'];
                        
                    }
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['pro_price'] = $product->pro_price;
                    $cart_product_data['shop_vendor_id'] = $category->parent_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;                    
                    $cart_product_data['status'] = 1;
                   if(isset($input['rental_time']) && $input['rental_time']!='')
                   {
                    $cart_product_data['buy_rent'] = 'rent';
                   }
                   else
                   {
                    $cart_product_data['buy_rent'] = 'buy';
                   }
                    

                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                    $LastID = $cart_product->id;
                }
                    if (isset($input['product_option_value']) && trim($input['product_option_value'])!='') {


                        $product_options = ProductOptionValue::where('product_id', '=', $input['product_id'])->where('id', '=', $input['product_option_value'])->with('getProductOption')->first();

                        $cart_option_data = array();
                        $cart_option_data['cart_type'] = $input['cart_type'];
                        $cart_option_data['product_option_id'] = $product_options->product_option_id;
                        $cart_option_data['cart_id'] = $cart->id;
                        $cart_option_data['product_id'] = $input['product_id'];
                        $cart_option_data['cart_product_id'] = $LastID;
                        
                        CartOption::create($cart_option_data); //cart option entry

                        $cart_option_value = array();
                        $cart_option_value['cart_type'] = $input['cart_type'];
                        $cart_option_value['cart_id'] = $cart->id;
                        $cart_option_value['product_id'] = $input['product_id'];
                        $cart_option_value['product_option_id'] = $product_options->product_option_id;
                        $cart_option_value['product_option_value_id'] = $input['product_option_value'];
                        $cart_option_value['quantity'] = $input['product_qty'];
                        $cart_option_value['value'] = $product_options->price;
                        $cart_option_value['status'] = 1;
                        
                        $cart_option_value['option_title'] = $product_options->getProductOption->option_title;
                        $cart_option_value['option_title_ar'] = $product_options->getProductOption->option_title_ar;
                        $cart_option_value['option_value_title'] = $product_options->option_title;
                        $cart_option_value['option_value_title_ar'] = $product_options->option_title_ar;
                        $cart_option_value['price'] = $product_options->price;
                        $cart_option_value['no_person'] = $product_options->no_person;
                        $cart_option_value['cart_product_id'] = $LastID;

                        CartOptionValue::create($cart_option_value); //cart option value entry
                    }
                  if (isset($input['product_option_value']) && $input['product_option_value']) {
                    
                        $cart_product_rent = array();
                        $cart_product_rent['cart_type'] = $input['cart_type'];
                        $cart_product_rent['cart_id'] = $cart->id;
                        $cart_product_rent['product_id'] = $input['product_id'];
                        $cart_product_rent['rental_date'] = date("Y-m-d", strtotime($input['rental_date']));
                        $cart_product_rent['rental_time'] = $input['rental_time'];
                        $cart_product_rent['return_date'] = date("Y-m-d", strtotime($input['return_date']));
                        $cart_product_rent['return_time'] = $input['return_time'];
                        $cart_product_rent['service_id'] = $product_options->id;
                        $cart_product_rent['quantity'] = $input['product_qty'];
                        $cart_product_rent['insurance_amount'] = $category->insurance_amount;
                        $cart_product_rent['cart_product_id'] = $LastID;


                        CartProductRent::create($cart_product_rent); //internal food entery
                    }
                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);

                } else if ($quentycheck <= $input['product_qty'] && $quentycheck > 0) {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.QUANTITY_ERROR').$quentycheck, 'code' => 200]);
                    $response = Response::json($response_array, 200);

                } else if ($quentycheck == 0) {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.PRODUCT_ERROR'), 'code' => 200]);
                    $response = Response::json($response_array, 200);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }

    /**Get to add to cart  data**/
    public function getMusicAcousticAddToCart(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'token' => 'required',
            'language_type' => 'required|max:255'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                $music_data = array();

if(isset($input['id']) && $input['id']!='')
{
$cart_pro = CartProduct::where('id', '=', $input['id'])->where('cart_type', '=', 'music')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_disprice,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,Insuranceamount')->first();
}
else
{
 $cart_pro = CartProduct::where('cart_type', '=', 'music')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_disprice,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,Insuranceamount')->first();   
}

                

                if (!empty($cart) && !empty($cart_pro)) {
                    
                    // delete pervious data code start /////////////
                        // $cart_option_delete = CartOption::where('cart_type', '=', 'music')->where('cart_product_id', '=', $input['id'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete(); 
                        //delete product option 

                    //$cart_option_value_delete = CartOptionValue::where('cart_type', '=', 'music')->where('cart_product_id', '=', $input['id'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    //delete product option value
                    //$cart_pro_rent_delete = CartProductRent::where('cart_type', '=', 'music')->where('cart_product_id', '=', $input['id'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    //delete service attribute

                   // $cart_pro_delete = CartProduct::where('cart_type', '=', 'music')->where('id', '=', $input['id'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    //delete product

                    // delete pervious data code end /////////////


if(isset($input['id']) && $input['id']!='')
{
    $cart_option_valuec = CartOptionValue::where('cart_product_id', '=',$input['id'])->where('cart_id', '=', $cart->id)->where('cart_type', '=', 'music')->where('product_id', '=', $input['product_id'])->count(); 
}
else
{

     $cart_option_valuec = CartOptionValue::where('cart_id', '=', $cart->id)->where('cart_type', '=', 'music')->where('product_id', '=', $input['product_id'])->count();
}

               


                if($cart_option_valuec >=1)
                {
                if(isset($input['id']) && $input['id']!='')
                {
                $cart_option_value = CartOptionValue::where('cart_product_id', '=',$input['id'])->where('cart_id', '=', $cart->id)->where('cart_type', '=', 'music')->where('product_id', '=', $input['product_id'])->with('getProductOption:id,services_id,option_title,option_title_ar,status')->with('getProductOptionValue:id,product_id,product_option_id,option_title,option_title_ar,status,value')->first();
                }
                else
                {
                $cart_option_value = CartOptionValue::where('cart_id', '=', $cart->id)->where('cart_type', '=', 'music')->where('product_id', '=', $input['product_id'])->with('getProductOption:id,services_id,option_title,option_title_ar,status')->with('getProductOptionValue:id,product_id,product_option_id,option_title,option_title_ar,status,value')->first();
                }
            }

                 
 
if(isset($input['id']) && $input['id']!='')
{
 $rental_productsC = CartProductRent::where('cart_product_id', '=',$input['id'])->where('cart_id', '=', $cart->id)->where('cart_type', '=', 'music')->where('product_id', '=', $input['product_id'])->count(); 
  

$rental_products = CartProductRent::where('cart_product_id', '=',$input['id'])->where('cart_id', '=', $cart->id)->where('cart_type', '=', 'music')->where('product_id', '=', $input['product_id'])->first();
}
else
{
     $rental_productsC = CartProductRent::where('cart_id', '=', $cart->id)->where('cart_type', '=', 'music')->where('product_id', '=', $input['product_id'])->count(); 

$rental_products = CartProductRent::where('cart_id', '=', $cart->id)->where('cart_type', '=', 'music')->where('product_id', '=', $input['product_id'])->first();
}


                    $category = Category::select('mc_id','mc_img', 'mc_name', 'mc_name_ar','address','address_ar', 'mc_status','city_id')->where('mc_id', '=', $cart_pro->getProduct[0]->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();

                    if (isset($cart_pro)) { //get product data


                        $music_data['pro_id'] = $cart_pro->getProduct[0]->pro_id;

                        if ($input['language_type'] == 'en') {
                            if(!empty($category)){
                               $music_data['category_name'] = $category->mc_name;
                               $music_data['category_img'] = $category->mc_img;
                               $music_data['address'] = $category->address; 
                               $music_data['city_name'] = $category->getCity->ci_name;
                            }

 
                            if($cart_option_valuec >=1 && count($cart_option_value->getProductOptionValue) >=1)
                            {

 
                        $music_data['option_title'] = $cart_option_value->getProductOptionValue[0]->option_title;
                            $music_data['quantity'] = $cart_option_value->getProductOptionValue[0]->value;

                            }
                            else
                            {
                            $music_data['option_title'] = '';
                            $music_data['quantity'] = '';

                            }

      
                            $music_data['pro_title'] = $cart_pro->getProduct[0]->pro_title;
                            $music_data['pro_desc'] = $cart_pro->getProduct[0]->pro_desc;
                           

                        } else {
                            if(!empty($category)){
                               $music_data['category_name'] = $category->mc_name_ar;
                               $music_data['category_img'] = $category->mc_img;
                               $music_data['address'] = $category->address_ar;
                               $music_data['city_name'] = $category->getCity->ci_name_ar; 
                            } 
                      if($cart_option_valuec >=1 &&  count($cart_option_value->getProductOptionValue) >=1)
                            {

                            $music_data['option_title'] = $cart_option_value->getProductOptionValue[0]->option_title_ar;
                            $music_data['quantity'] = $cart_option_value->getProductOptionValue[0]->value;
                            }
                            else
                            {
                            $music_data['option_title'] = '';
                            $music_data['quantity'] = '1';

                            }

                            $music_data['pro_title'] = $cart_pro->getProduct[0]->pro_title_ar;
                            $music_data['pro_desc'] = $cart_pro->getProduct[0]->pro_desc_ar;
                            
                        }
                        $music_data['pro_qty'] = $cart_pro->quantity;
                         $music_data['pro_price'] = $cart_pro->getProduct[0]->pro_price;

                        $getOpta =  DB::table('nm_product_option_value')->where('product_id', '=', $input['product_id'])->where('option_title','Rent')->count();


                        if($cart_pro->getProduct[0]->pro_disprice >=1)
                        {

                            if($cart_option_valuec >=1)
                            { 

                            $music_data['pro_price'] = $cart_option_value->value;
                            if($rental_productsC >=1)
                            {
                            $getOpt =  DB::table('nm_product_option_value')->where('product_id', '=', $input['product_id'])->where('option_title','Rent')->first();

                            $aPrice = $getOpt->price - ($getOpt->price * $getOpt->discount)/100;

                            $music_data['pro_price'] = $aPrice;
                            }

                            }
                            else
                            {
                            $music_data['pro_price'] = $cart_pro->getProduct[0]->pro_disprice;

                            }


                             
                        }
                        else
                        {
                            if($cart_option_valuec >=1)
                            {
                            if($rental_productsC >=1)
                            {
                            $getOpt =  DB::table('nm_product_option_value')->where('product_id', '=', $input['product_id'])->where('option_title','Rent')->first();

                            $aPrice = $getOpt->price - ($getOpt->price * $getOpt->discount)/100;

                            $music_data['pro_price'] = $aPrice;
                            }
                            }
                            else
                            {
                            $music_data['pro_price'] = $cart_pro->getProduct[0]->pro_price;
                            }


                             
                        }

               
 
if($music_data['pro_price'] =='')
{
    if($music_data['pro_qty'] >=1)
    {
      $music_data['pro_price'] = $cart_pro->total_price/$music_data['pro_qty'];  
    }
    else
    {
        $music_data['pro_price'] = $cart_pro->total_price;
    }
    
}
                        $music_data['Insuranceamount'] = $cart_pro->getProduct[0]->Insuranceamount;
                        $music_data['pro_Img'] = $cart_pro->getProduct[0]->pro_Img;
                        $music_data['pro_status'] = $cart_pro->getProduct[0]->pro_status;
                        $music_data['total_price'] = $cart_pro->total_price;
                        /*if ($cart_option_value->product_option_value_id == 44) {
                            $music_data['total_price'] = $cart_pro->getProduct[0]->pro_price * $cart_pro->quantity;
                        } else {
                            $music_data['total_price'] = $cart_pro->getProduct[0]->Insuranceamount + $cart_pro->getProduct[0]->pro_price * $cart_pro->quantity;
                        }*/
                    }

                    if (!empty($rental_products)) { //get rental product

                        $music_data['rental_date'] = date("d-m-Y", strtotime($rental_products->rental_date));
                        $music_data['rental_time'] = $rental_products->rental_time;
                        $music_data['return_date'] = date("d-m-Y", strtotime($rental_products->return_date));
                        $music_data['return_time'] = $rental_products->return_time;
                    }

                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'cart_id' => $cart->id, 'music_data' => $music_data);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'cart data not found & cart type must be music', 'code' => 500]);
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    /**delete to add to cart  data**/
   public function deleteMusicAcousticData(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'token' => 'required',
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();

                $cart = Cart::where('user_id', '=', $User->cus_id)->first();


                $cart_pro = CartProduct::where('id', '=', $input['id'])->where('cart_type', '=', 'music')->where('cart_id', '=', $cart->id)->first();

                if (!empty($cart) && !empty($cart_pro)) {
                    
                    $cart_option = CartOption::where('cart_type', '=', 'music')->where('cart_product_id', '=', $input['id'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete(); //delete product option 

                    $cart_option_value = CartOptionValue::where('cart_type', '=', 'music')->where('cart_product_id', '=', $input['id'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();//delete product option value
                    $cart_pro_rent = CartProductRent::where('cart_type', '=', 'music')->where('cart_product_id', '=', $input['id'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    //delete service attribute

                    $cart_pro = CartProduct::where('cart_type', '=', 'music')->where('id', '=', $input['id'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    //delete product

                   $response_array = array('data' => ['success' => true, 'message' => 'Successfully deleted your cart data', 'code' => 200]);

                    
                }
               

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }






 public function checkAcosticsrent(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'bookingdate_from' => 'required',
            'bookingdate_to' => 'required',
            'token' => 'required',
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
               

            $product_id = $request->product_id;
            $bookingf = $request->bookingdate_from;
            $bookingt = $request->bookingdate_to;
            //From front end
            $CarBookingdate_from =date("Y-m-d H:i A", strtotime($bookingf));
            $CarBookingdate_to = date("Y-m-d H:i A", strtotime($bookingt)); 
            $getcarQty = DB::table('nm_product')->select('pro_qty')->where('pro_id',$product_id)->first();
            $getcarQtya = DB::table('nm_product_option_value')->select('value')->where('option_title','Quantity in stock')->where('product_id',$product_id)->first();



            $carTotalQty = $getcarQtya->value;
            $Cbdate_from = strtotime($CarBookingdate_from); 
            $Cbdate_to = strtotime($CarBookingdate_to);
 
            $CarDatebook = array();
            for ($i=$Cbdate_from; $i<=$Cbdate_to; $i+=3600) {  
            array_push($CarDatebook, date("Y-m-d H:i A", $i));  
            } 
            //From front end 
            $getInfocount = DB::table('nm_order_product_rent')->where('product_id',$product_id)->where('product_type','music')->count();

            $alreadyBook = array();
            if($getInfocount >=1)
            {
            $getInfo = DB::table('nm_order_product_rent')->where('product_id',$product_id)->where('product_type','music')->get();
            $datetoarray = array();
            $datefromarray = array();
            $k=1;
            foreach($getInfo as $getinf)
            {

            $Froms = $getinf->rental_date.' '.$getinf->rental_time;
            $tos = $getinf->return_date.' '.$getinf->return_time;
            $dateChk = date("Y-m-d H:i A", strtotime($Froms)); 
            $dateChka = date("Y-m-d H:i A", strtotime($tos)); 
            array_push($datetoarray, $dateChka);
            array_push($datefromarray, $dateChk);


            }

            $dateFix = array();
            for($j=0; $j<count($datefromarray);$j++)
            {
            $Fromdate = strtotime($datefromarray[$j]);
            $Todate = strtotime($datetoarray[$j]);
            for ($M=$Fromdate; $M<=$Todate; $M+=3600) {  
            array_push($dateFix, date("Y-m-d H:i A", $M));  
            } 
            } 

            $getArry = array_count_values($dateFix); 
            foreach($CarDatebook as $bookd)
            {
            foreach($getArry as $key=>$val)
            {
            if($key==$bookd)
            {
            array_push($alreadyBook, $val);
            }
            }
            }
            } 

            if(count($alreadyBook) >=1){
            $totalQtyBook = max($alreadyBook);
            }
            else
            {
            $totalQtyBook =0;
            }

            if($carTotalQty > $totalQtyBook)
            {
            $can_book = 'Yes';
            }
            else
            {
            $can_book = 'Yes';
            }






                 $response_array = array('data' => ['success' => true, 'can_book' => $can_book, 'code' => 200]);

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'Something went wrong', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }





}
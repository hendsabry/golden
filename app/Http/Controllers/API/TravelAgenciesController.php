<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Http\Models;
use App\Category;
use App\ProductAttribute;
use App\Products;
use App\City;
use App\Cart;
use App\CartProduct;
use App\CartProductRent;
use App\CartProductAttribute;
use Auth;
use App\User;
use Response;

class TravelAgenciesController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |   Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function __construct() {}

    /**Get Car Rental info.**/

    /*
       subcategory_id(2th lavel category in category table) become pro_mc_id in product table to get full Car Rental info.
    */
    public function getTravelAgenciesInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first();
                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($input['subcategory_id']);

                if (!empty($category)) {

                    if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                        //GET SUB CATEGORY
                        $sub_catg = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name','google_map_address','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();
                        //GET VENDOR DETAIL
                        $product = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_price','pro_desc_ar as pro_desc','pro_disprice','pro_discount_percentage')->where('pro_mc_id', '=', $input['subcategory_id'])->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(1000);


                        if (!$product->isEmpty()) {
                            foreach($product as $value) {
                                $product_attr = ProductAttribute::select('id','product_id','vendor_id','attribute_title_ar as attribute_title','value','value_ar')->where('product_id', '=', $value->pro_id)->orderBy('id', 'ASC')->get(); //GET PRODUCT ATTRIBUTE

                                foreach($product_attr as $val)
                                {
                                if($val->value_ar!=''){
                                $val->value = $val->value_ar;
                                }
                                else
                                {
                                $val->value = $val->value;   
                                }
 
                                }

                               $value->product_attribute =  $product_attr;

                            }


                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'vendor_detail' => $sub_catg, 'client_comments' => $client_review, 'product' => $product);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                        $sub_catg = Category::select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name','google_map_address','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                        //GET VENDOR DETAIL
                        $product = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_price','pro_desc','pro_disprice','pro_discount_percentage')->where('pro_mc_id', '=', $input['subcategory_id'])->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(1000);
                        if (!$product->isEmpty()) {
                            foreach($product as $value) {
                                $product_attr = ProductAttribute::select('id','product_id','vendor_id','attribute_title','value')->where('product_id', '=', $value->pro_id)->orderBy('id', 'ASC')->get(); //GET PRODUCT ATTRIBUTE
                               $value->product_attribute =  $product_attr;
                            }


                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'vendor_detail' => $sub_catg, 'client_comments' => $client_review, 'product' => $product);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Set Travel Agencies Data in add to Cart
    public function setTravelAgenciesAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required', //Get Product ID
            'cart_type' => 'required', // Get Cart Type
            'service_date' => 'required', //Get Product ID
            'total_price' => 'required',
        ]);
        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

                $User = Auth::user();
                $product = Products::where('pro_id', '=', $input['product_id'])->first();

                if (!empty($product)) {
                    $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();




                    if (!empty($cart)) {


                     $cart_pro_count = CartProduct::where('cart_type', '=', 'travel')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->count();


                    $splitTimeStamp = explode(" ",$input['service_date']);
                     $cart_prodd = strtotime($splitTimeStamp[0]);                   
                   $cart_proddhrs = $splitTimeStamp[1].' '.$splitTimeStamp[2]; 


                      $cart_pro_rent_count = CartProductRent::where('cart_type', '=', 'travel')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->count();
 
                  

                    if($cart_pro_count==1 && $cart_pro_rent_count >=1)
                    {

                         $cart_pro_rent_get = CartProductRent::where('cart_type', '=', 'travel')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->get();

                        foreach ($cart_pro_rent_get as $value) {
                        $rentalDate = strtotime($value->rental_date);
                        $renval =  $value->rental_time;
                        $renvalid =  $value->id;
                        $cart_product_id = $value->cart_product_id;
                        if($cart_prodd  == $rentalDate)
                        {
                        $cart_pro = CartProduct::where('id', '=', $cart_product_id)->where('cart_type', '=', 'travel')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                        $cart_pro_rent = CartProductRent::where('cart_type', '=', 'travel')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->where('id', '=', $renvalid)->delete();
                        $cart_pro_attr = CartProductAttribute::where('cart_product_id', $cart_product_id)->where('cart_type', '=', 'travel')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete(); 
                        } 
                        }

                     }
 
                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }




                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = 'travel';
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $input['product_id'];
                    
                    $cart_product_data['status'] = 1;
                    $cart_product_data['total_price'] = $input['total_price'];  
                    
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['pro_price'] = $product->pro_price;
                    $cart_product_data['quantity'] = $input['noofpeople'];

                    $cart_product_data['shop_id'] = $product->pro_mc_id;
                    $cart_product_data['review_type'] = 'shop';
                    $cart_product_data['shop_vendor_id'] =$product->pro_mc_id;
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                    $cartProID = $cart_product->id;
                   $splitTimeStamp = explode(" ",$input['service_date']);

                    $cart_product_rent = array();
                    $cart_product_rent['cart_id'] = $cart->id;
                    $cart_product_rent['cart_type'] = $input['cart_type'];
                    $cart_product_rent['product_id'] = $input['product_id'];
                    $cart_product_rent['return_date'] = $input['service_date'];  
                    
                    $cart_product_rent['cart_product_id'] = $cartProID;
      

                    $cart_product_rent['rental_date'] = $splitTimeStamp[0];                    
                    $cart_product_rent['rental_time'] = $splitTimeStamp[1]. " " .$splitTimeStamp[2];
                    CartProductRent::create($cart_product_rent); //Product rental date and time entry

                    $product_attr = ProductAttribute::select('id', 'product_id', 'attribute_title_ar','attribute_title', 'value','value_ar')->where('product_id', '=', $input['product_id'])->get();
                    if(!empty($product_attr)){
                        foreach ($product_attr as $key => $value) {
                    
                    $cart_product_attr = array();
                    $cart_product_attr['cart_id'] = $cart->id;
                    $cart_product_attr['cart_type'] = $input['cart_type'];
                    $cart_product_attr['product_id'] = $input['product_id'];
                    $cart_product_attr['attribute_title'] = $value->attribute_title;
                    $cart_product_attr['attribute_title_ar'] = $value->attribute_title_ar;
                    if($locale=='ar'){
                    $cart_product_attr['value'] = $value->value_ar;
                    }else{
                    $cart_product_attr['value'] = $value->value;
                    }
                    $cart_product_attr['cart_product_id'] = $cartProID;
                    CartProductAttribute::create($cart_product_attr); //Product Attribute entry
                        }
                    }

                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.CART_ERROR'), 'code' => 500]);
                    $response = Response::json($response_array, 500);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }

    /**Get Travel Agencies data from add to cart**/
    public function getTravelAgenciesAddToCartData(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'id' => 'required',
            'token' => 'required',
            'language_type' => 'required|max:255',
        ]);
        $input = $request->all();

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                $Id = $input['id'];
                $cart_pro = CartProduct::where('cart_type', '=', 'travel')->where('id', '=', $Id)->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,pro_disprice')->first();


                if (!empty($cart) && !empty($cart_pro)) {
                    $cart_rental_service = CartProductRent::where('cart_type', '=', 'travel')->where('cart_product_id', '=', $Id)->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->first();

                    $category = Category::select('mc_id','mc_img', 'mc_name', 'mc_name_ar','address','address_ar', 'mc_status','city_id')->where('mc_id', '=', $cart_pro->getProduct[0]->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();

                    $catg_data = array();
                    $catg_data['pro_id'] = $cart_pro->getProduct[0]->pro_id;

                    if ($input['language_type'] == 'en') {
                        if(!empty($category)){
                               $catg_data['category_name'] = $category->mc_name;
                               $catg_data['category_img'] = $category->mc_img;
                               $catg_data['address'] = $category->address;
                               $catg_data['city_name'] = $category->getCity->ci_name; 
                            } 
                         $catg_data['pro_title'] = $cart_pro->getProduct[0]->pro_title;
                         $catg_data['pro_desc'] = $cart_pro->getProduct[0]->pro_desc;
                         $catg_data['quantity'] = $cart_pro->quantity;

                        $product_attr = CartProductAttribute::select('id', 'product_id', 'attribute_title', 'value')->where('product_id', '=', $cart_pro->product_id)->where('cart_product_id', '=', $Id)->orderBy('attribute_title', 'ASC')->get(); //GET PRODUCT ATTRIBUTE 
                        $catg_data['product_attribute'] = $product_attr;

                    } else {
                        if(!empty($category)){
                               $catg_data['category_name'] = $category->mc_name_ar;
                               $catg_data['category_img'] = $category->mc_img;
                               $catg_data['address'] = $category->address_ar; 
                               $catg_data['city_name'] = $category->getCity->ci_name_ar;
                               $catg_data['quantity'] = $cart_pro->quantity; 
                            }
                        $catg_data['pro_title'] = $cart_pro->getProduct[0]->pro_title_ar;
                        $catg_data['pro_desc'] = $cart_pro->getProduct[0]->pro_desc_ar;
                        $product_attr = CartProductAttribute::select('id', 'product_id', 'attribute_title_ar', 'value')->where('product_id', '=', $cart_pro->product_id)->where('cart_product_id', '=', $Id)->orderBy('attribute_title', 'ASC')->get(); //GET PRODUCT ATTRIBUTE 

                        $catg_data['product_attribute'] = $product_attr;
                    }
                    $catg_data['pro_Img'] = $cart_pro->getProduct[0]->pro_Img;
                    $catg_data['status'] = $cart_pro->getProduct[0]->pro_status;
                    $catg_data['price'] = $cart_pro->total_price;

                    if($cart_pro->getProduct[0]->pro_disprice < 1)
                    {
                    $catg_data['pro_price'] = $cart_pro->getProduct[0]->pro_price;
                    }
                    else
                    {
                    $catg_data['pro_price'] = $cart_pro->getProduct[0]->pro_disprice;
                    }
            

                   

                    if(isset($cart_rental_service) && count($cart_rental_service)>0){
                    $catg_data['service_date'] = date("d-m-Y", strtotime($cart_rental_service->rental_date));
                    $catg_data['service_time'] = $cart_rental_service->rental_time;
                }else{
                    $catg_data['service_date']='';
                    $catg_data['service_time'] ='';
                }





                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'cart_id' => $cart->id, 'makeup_cart_data' => $catg_data, );
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'cart data not found & cart type must be Travel Agencies ', 'code' => 500]);
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    /**delete to add to cart  data**/
    public function deleteTravelData(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'id' => 'required',
            'token' => 'required',
            'cart_type' => 'required'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();

                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                $cart_pro = CartProduct::where('product_id', '=', $input['product_id'])->where('id', '=', $input['id'])->where('cart_type', '=', 'travel')->where('cart_id', '=', $cart->id)->first();
                if (!empty($cart) && !empty($cart_pro)) {

                    $cart_pro = CartProductRent::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'travel')->where('cart_product_id', '=', $input['id'])->where('cart_id', '=', $cart->id)->delete();
                    //delete car rent services

                    $cart_pro = CartProduct::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'travel')->where('id', '=', $input['id'])->where('cart_id', '=', $cart->id)->delete();
                    //delete product

                    $response_array = array('data' => ['success' => true, 'message' => 'Successfully deleted your cart data', 'code' => 200]);

                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    

}
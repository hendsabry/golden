<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Models;
use App\Category;
use App\Products;
use App\OrderProduct;
use App\OrderSingleProduct;
use App\OrderOption;
use App\OrderOptionValue;
use App\OrderServiceAttribute;
use App\OrderInternalFoodDish;
use App\OrderExternalFoodDish;
use App\OrderProductRent;
use App\OrderServiceStaff;
use App\OrderHospitality;
use App\OrderInvitation;
use App\OrderBodyMeasurement;
use App\InternalFoodContainerPrice;
use Illuminate\Support\Facades\Validator;
use App\FoodContainerPrice;
use App\ProductPackage;
use App\ProductAttribute;
use Auth;
use App\User;
use Response;
use DB;
use App\ProductOptionValue;


class OrderProductController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |   Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function __construct() {}

    /**Get order hall  data**/
    public function getHallOrderData(Request $request) {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required',
            'product_id' => 'required',
            'token' => 'required',
            'language_type' => 'required|max:255'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();

                $order = OrderProduct::where('order_cus_id', '=', $User->cus_id)->where('order_id', '=', $input['order_id'])->first();

                $vatAmount = $order->order_taxAmt;
                $order_pro = OrderSingleProduct::where('product_type', '=', 'hall')->where('order_id', '=', $order->order_id)->where('product_id', '=', $input['product_id'])->with('getHall')->first();
 
                $branch_city = Category::select('mc_id', 'city_id', 'insurance_amount','longitude','latitude')->where('mc_id', '=', $order_pro->getHall[0]->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();

                if (!empty($order) && $order_pro->order_type = 'hall') {
                    $order_option_value = OrderOptionValue::where('order_id', '=', $order->order_id)->where('product_id', '=', $input['product_id'])->where('product_type', '=', 'hall')->with('getProductOption:id,vendor_id,services_id,option_title,option_title_ar,service_hour')->with('getProductOptionValue:id,vandor_id,product_id,product_option_id,option_title,option_title_ar,short_name,value,image,price,no_person,status')->get();

                    $internal_food = OrderInternalFoodDish::where('order_id', '=', $order->order_id)->where('product_id', '=', $input['product_id'])->with('getServiceAttribute')->with('getFood')->with('getContainer')->get();

                    $hall = OrderSingleProduct::where('order_id', '=', $order->order_id)->where('product_id', '=', $input['product_id'])->where('product_type', '=', 'hall')->get();

                    $order_service = array();
                    $internal_foods = array();
                    $hall_data = array();
                    if (isset($order_option_value)) { //get service
                        foreach($order_option_value as $key => $value) {

                            if ($input['language_type'] == 'en') {
                                $order_service[$key]['option_title'] = $value->getProductOption->option_title;
                                $order_service[$key]['value_title'] = $value->getProductOptionValue[0]->option_title;
                            } else {
                                $order_service[$key]['option_title'] = $value->getProductOption->option_title_ar;
                                $order_service[$key]['value_title'] = $value->getProductOptionValue[0]->option_title_ar;
                            }

                            $order_service[$key]['price'] = $value->getProductOptionValue[0]->price;
                            $order_service[$key]['no_person'] = $value->getProductOptionValue[0]->no_person;
                            $order_service[$key]['status'] = $value->getProductOptionValue[0]->status;
                        }
                    }
                    if (isset($internal_food)) {
                        foreach($internal_food as $key => $value) { //get internal food
                            $container_price = InternalFoodContainerPrice::where('dish_id', '=', $value->internal_food_dish_id)->where('container_id', '=', $value->container_id)->first();

                            if ($input['language_type'] == 'en') {
                                $internal_foods[$key]['menu_name'] = $value->getServiceAttribute[0]->attribute_title;
                                $internal_foods[$key]['dish_name'] = $value->getFood[0]->dish_name;
                                $internal_foods[$key]['container_title'] = $value->getContainer[0]->title;
                            } else {
                                $internal_foods[$key]['menu_name'] = $value->getServiceAttribute[0]->attribute_title_ar;
                                $internal_foods[$key]['dish_name'] = $value->getFood[0]->dish_name_ar;
                                $internal_foods[$key]['container_title'] = $value->getContainer[0]->title;
                            }
                            $internal_foods[$key]['dish_id'] = $value->getFood[0]->id;
                            $internal_foods[$key]['quantity'] = $internal_food[$key]->quantity;
 
                            if(isset($container_price->discount_price) && ($container_price->discount_price!='' && $container_price->discount_price!='0'))
                            {
                              $internal_foods[$key]['container_price'] = $container_price->discount_price;
                              $DisPrices =   $container_price->discount_price;
                            }
                            else
                            {
                             $internal_foods[$key]['container_price'] = $container_price->container_price; 
                                $DisPrices =   $container_price->container_price;  
                            }


                            
                            $internal_foods[$key]['dish_image'] = $value->getFood[0]->dish_image;
                            $internal_foods[$key]['status'] = $value->getFood[0]->status;

                            $internal_foods[$key]['no_people'] = $value->getContainer[0]->no_people;
                            if (!empty($container_price)) {

                                $internal_foods[$key]['final_price'] = $DisPrices * $internal_food[$key]->quantity;

                            }

                        }
                    }

                    if (isset($hall)) { //get hall
                        if ($input['language_type'] == 'en') {
                            $hall_data['pro_title'] = $order_pro->getHall[0]->pro_title;
                            $hall_data['pro_desc'] = $order_pro->getHall[0]->pro_desc;
                            $hall_data['city'] = $branch_city->getCity->ci_name;
                            $hall_data['longitude'] = $branch_city->longitude;
                            $hall_data['latitude'] = $branch_city->latitude;
                          
                        } else {
                            $hall_data['pro_title'] = $order_pro->getHall[0]->pro_title_ar;
                            $hall_data['pro_desc'] = $order_pro->getHall[0]->pro_desc_ar;
                            $hall_data['city'] = $branch_city->getCity->ci_name_ar;
                            $hall_data['longitude'] = $branch_city->longitude;
                            $hall_data['latitude'] = $branch_city->latitude;
                        }
                        $hall_data['Insuranceamount'] = $order_pro->insurance_amount;
                         
//$order_pro->getHall[0]->pro_price;

                        $hall_data['price'] = $order_pro->getHall[0]->pro_netprice;

 

                        $hall_data['pro_qty'] = $order_pro->getHall[0]->pro_qty;
                        $hall_data['pro_Img'] = $order_pro->getHall[0]->pro_Img;
                        $hall_data['pro_status'] = $order_pro->getHall[0]->pro_status;
                        $hall_data['total_price'] = $order->order_amt;
                    }
                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'order_id' => $order->order_id, 'service', 'service' => $order_service, 'internal_food' => $internal_foods, 'hall_data' => $hall_data);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'order data not found & order type must be hall', 'code' => 500]);
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    /**Get order food  data**/
    public function getFoodOrderData(Request $request) {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'order_id' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $order = OrderProduct::where('order_cus_id', '=', $User->cus_id)->where('order_id', '=', $input['order_id'])->first();
                $food_pro = array(); 

                $order_pro = OrderSingleProduct::where('product_type', '=', 'food')->where('order_id', '=', $order->order_id)->where('product_id', '=', $input['product_id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_mc_id,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status')->first();

              
              
                //get food product

                if (!empty($order) && !empty($order_pro)) {

              

                    if (isset($order_pro)) {
                        // get Date(type) data from add to order  
                        $attribute = OrderServiceAttribute::where('product_type', '=', 'food')->where('order_id', '=', $order->order_id)->where('product_id', '=', $input['product_id'])->with('getProductServiceAttribute:id,services_id,attribute_title,attribute_title_ar')->first();

                        $category = Category::select('mc_id', 'mc_img', 'mc_name', 'mc_name_ar', 'address', 'address_ar', 'mc_status', 'city_id','longitude','latitude')->where('mc_id', '=', $order_pro->getProduct[0]->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();

                        $food_pro['pro_id'] = $order_pro->getProduct[0]->pro_id;
                        if ($input['language_type'] == 'en') {

                            if (!empty($category)) {
                                $food_pro['category_name'] = $category->mc_name;
                                $food_pro['category_img'] = $category->mc_img;
                                $food_pro['address'] = $category->address;
                                $food_pro['city_name'] = $category->getCity->ci_name;
                                $food_pro['longitude'] = $category->longitude;
                                $food_pro['latitude'] = $category->latitude;
                            }
                             
                             if(isset($attribute->getProductServiceAttribute[0]->attribute_title) && $attribute->getProductServiceAttribute[0]->attribute_title!='') {
                            $food_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title;
                               }

                            $food_pro['pro_title'] = $order_pro->getProduct[0]->pro_title;

                            $food_pro['pro_desc'] = $order_pro->getProduct[0]->pro_desc;

                        } else {
                            if (!empty($category)) {
                                $food_pro['category_name'] = $category->mc_name_ar;
                                $food_pro['category_img'] = $category->mc_img;
                                $food_pro['address'] = $category->address_ar;
                                $food_pro['city_name'] = $category->getCity->ci_name_ar;
                                $food_pro['longitude'] = $category->longitude;
                                $food_pro['latitude'] = $category->latitude;
                            }
                            $food_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title_ar;
                            $food_pro['pro_title'] = $order_pro->getProduct[0]->pro_title_ar;
                            $food_pro['pro_desc'] = $order_pro->getProduct[0]->pro_desc_ar;
                        }
                          
                            if($order_pro->getProduct[0]->pro_price < 1)
                            {
                            $Check = DB::table('nm_product_option_value')->where('product_id',$input['product_id'])->where('option_title','!=','Per Piece')->count();

                            if($Check >=1)
                            {
                            $Checks = DB::table('nm_product_option_value')->where('product_id',$input['product_id'])->where('option_title','!=','Per Piece')->first();

                            if($Checks->discount_price >=1)
                            {
                            $food_pro['pro_price'] = $Checks->discount_price; 
                            }
                            else
                            {
                            $food_pro['pro_price'] = $Checks->value;  
                            } 
                            }
                             else
                            {
                            $food_pro['pro_price'] = $order_pro->getProduct[0]->pro_price;
                            }

                            }
                            else
                            {
                            $food_pro['pro_price'] = $order_pro->getProduct[0]->pro_price;
                            }
                       
                        $food_pro['pro_Img'] = $order_pro->getProduct[0]->pro_Img;
                        $food_pro['pro_status'] = $order_pro->getProduct[0]->pro_status;
                        if ($order_pro->quantity > 0) {
                            $food_pro['pro_qty'] = $order_pro->quantity;
                            $food_pro['total_price'] = $order_pro->getProduct[0]->pro_price * $order_pro->quantity;
                        }

                    }

                    $order_option_value = OrderOptionValue::where('product_id', '=', $input['product_id'])->where('order_id', '=', $order->order_id)->where('product_type', '=', 'food')->with('getProductOption:id,vendor_id,services_id,option_title,option_title_ar,service_hour')->with('getProductOptionValue:id,vandor_id,product_id,product_option_id,option_title,option_title_ar,short_name,value,image,price,discount_price,no_person,status')->get();
 

                    $option_value_data = array();

                    if (!empty($order_option_value)) { //get service

                       // $order_option_value
 

               foreach($order_option_value as $key => $value) {

                if ($input['language_type'] == 'en') {
                $option_value_data[$key]['option_title'] = $value->getProductOption->option_title;

                if(isset($value->getProductOptionValue[0]->option_title) && $value->getProductOptionValue[0]->option_title!='')
                {
                $option_value_data[$key]['value_title'] = $value->getProductOptionValue[0]->option_title;
                }
                else
                {

                $option_value_data[$key]['value_title'] = '';

                }

                } else {
                $option_value_data[$key]['option_title'] = $value->getProductOption->option_title_ar;
                $option_value_data[$key]['value_title'] = $value->getProductOptionValue[0]->option_title_ar;
                }

                $option_value_data[$key]['quantity'] = $value->quantity;
                if(isset($value->getProductOptionValue[0]->price) && $value->getProductOptionValue[0]->price!='')
                {
                    if($value->getProductOptionValue[0]->price < 1)
                    {
                        $option_value_data[$key]['dis_price'] = $value->getProductOptionValue[0]->discount_price;
                      $option_value_data[$key]['price'] = $value->getProductOptionValue[0]->price;  
                    }
                    else
                    {


                      $option_value_data[$key]['dis_price'] = $value->getProductOptionValue[0]->discount_price;  
                     $option_value_data[$key]['price'] = $value->getProductOptionValue[0]->value;   
                    }
                

                
                }
                else
                {
                    $option_value_data[$key]['dis_price']= $value->getProductOptionValue[0]->discount_price; 
                $option_value_data[$key]['price'] = $value->getProductOptionValue[0]->value;       
                } 

                if(isset($value->getProductOptionValue[0]->no_person) && $value->getProductOptionValue[0]->no_person!='')
                {
                $option_value_data[$key]['no_person'] = $value->getProductOptionValue[0]->no_person;
                }
                else
                {
                $option_value_data[$key]['no_person'] = 1;    
                }  

                if(isset($value->getProductOptionValue[0]->status) && $value->getProductOptionValue[0]->status!='')
                {
                $option_value_data[$key]['status'] = $value->getProductOptionValue[0]->status;
                }
                else
                {
                $option_value_data[$key]['status'] = 1;    
                }  

                if(isset($value->getProductOptionValue[0]->price) && $value->getProductOptionValue[0]->price!='')
                {
                $option_value_data[$key]['final_price']= $value->getProductOptionValue[0]->price * $value->quantity;
                }
                else
                {
                $option_value_data[$key]['final_price'] = 1;    
                }       


                }
                }

                $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'order_id' => $order->order_id, 'food_data' => $food_pro, 'food_option_data' => $option_value_data);
                } else {
                    //Get Buffet data from add to order


                    $order_pro = OrderSingleProduct::where('product_type', '=', 'food')->where('order_id', '=', $order->order_id)->where('category_id', '=', $input['product_id'])->first();
                    //get buffet data
                    if (!empty($order) && !empty($order_pro)) {

                        if (isset($order_pro)) {
                            $external_foods = array();
                            $buffet_branch = array();
                            $external_food = OrderExternalFoodDish::where('order_id', '=', $order->order_id)->where('category_id', '=', $input['product_id'])->with('getServiceAttribute')->with('getFood:pro_id,pro_title,pro_title_ar,pro_Img,pro_qty,pro_price,pro_status')->with('getContainer:id,vandor_id,product_id,product_option_id,option_title,option_title_ar,short_name,price,no_person')->get();

                            if (isset($external_food)) {
                                foreach($external_food as $key => $value) {
                                    //get external food
                                    $container_price = FoodContainerPrice::where('product_id', '=', $value->external_food_dish_id)->where('product_option_value_id', '=', $value->container_id)->first();

                                    if ($input['language_type'] == 'en') {
                                        $external_foods[$key]['menu_name'] = $value->getServiceAttribute[0]->attribute_title;
                                        $external_foods[$key]['dish_name'] = $value->getFood[0]->pro_title;
                                        $external_foods[$key]['container_title'] = $value->getContainer[0]->option_title;
                                    } else {
                                        $external_foods[$key]['menu_name'] = $value->getServiceAttribute[0]->attribute_title_ar;
                                        $external_foods[$key]['dish_name'] = $value->getFood[0]->pro_title_ar;
                                        $external_foods[$key]['container_title'] = $value->getContainer[0]->option_title;
                                    }
                                    $external_foods[$key]['dish_id'] = $value->getFood[0]->pro_id;
                                    $external_foods[$key]['quantity'] = $value->quantity;

                                    if($container_price->discount_price >=1 && $container_price->discount_price!='')
                                    {
                                    $external_foods[$key]['container_price'] = $container_price->discount_price;
                                    }
                                    else
                                    {
                                      $external_foods[$key]['container_price'] = $container_price->price;  
                                    }
                                                                
     
                                    


                                    $external_foods[$key]['dish_image'] = $value->getFood[0]->pro_Img;
                                    $external_foods[$key]['status'] = $value->getFood[0]->pro_status;

                                    $external_foods[$key]['no_people'] = $value->getContainer[0]->no_person;
                                    if (!empty($container_price)) {

                                        $external_foods[$key]['final_price'] = $container_price->price * $value->quantity;

                                    }

                                    $buffet = Category::where('mc_status', '=', 1)->where('mc_id', '=', $value->category_id)->select('mc_id', 'mc_name_ar', 'mc_name', 'mc_img', 'mc_discription', 'city_id', 'address', 'address_ar','longitude','latitude')->with('getCity:ci_id,ci_name_ar,ci_name')->first();

                                }

                                if (isset($buffet)) {
                                    if ($input['language_type'] == 'en') {

                                        $buffet_branch['mc_name'] = $buffet->mc_name;
                                        $buffet_branch['mc_img'] = $buffet->mc_img;
                                        $buffet_branch['mc_discription'] = $buffet->mc_discription;
                                        $buffet_branch['city_name'] = $buffet->getCity->ci_name;
                                        $buffet_branch['address'] = $buffet->address;
                                        $buffet_branch['longitude'] = $buffet->longitude;
                                        $buffet_branch['latitude'] = $buffet->latitude;
                                    } else {
                                        $buffet_branch['mc_name_ar'] = $buffet->mc_name_ar;
                                        $buffet_branch['mc_img'] = $buffet->mc_img;
                                        $buffet_branch['mc_discription'] = $buffet->mc_discription;
                                        $buffet_branch['city_name'] = $buffet->getCity->ci_name_ar;
                                        $buffet_branch['address'] = $buffet->address_ar;
                                        $buffet_branch['longitude'] = $buffet->longitude;
                                        $buffet_branch['latitude'] = $buffet->latitude;
                                    }
                                }
                            }
                        }
                        $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'order_id' => $order->order_id, 'external_foods' => $external_foods, 'buffet_branch' => $buffet_branch);
                    } else {
                        $response_array = array('data' => ['success' => false, 'message' => 'order data not found & order type must be food', 'code' => 500]);
                    }
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    /**Get Makeup, Beauty Centers, Makeup Artist, Spa and Men's Saloon data from Order table**/
    public function getMakeupOrderData(Request $request) {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'order_id' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $order = OrderProduct::where('order_cus_id', '=', $User->cus_id)->where('order_id', '=', $input['order_id'])->first();
                $catg_data = array();
                $order_pro = OrderSingleProduct::where('product_type', '=', 'beauty')->where('order_id', '=', $order->order_id)->where('product_id', '=', $input['product_id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,pro_disprice')->first();
                //get food product  product 

                if (!empty($order) && !empty($order_pro)) {

                    // get Date(type) data from add to order  
                    $attribute = OrderServiceAttribute::where('product_type', '=', 'beauty')->where('order_id', '=', $order->order_id)->where('product_id', '=', $input['product_id'])->with('getProductServiceAttribute:id,services_id,attribute_title,attribute_title_ar')->first();

                    $category = Category::select('mc_id', 'mc_img', 'mc_name', 'mc_name_ar', 'address', 'address_ar', 'mc_status', 'city_id', 'home_visit_charge','longitude','latitude')->where('mc_id', '=', $order_pro->getProduct[0]->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();

                    $catg_data['pro_id'] = $order_pro->getProduct[0]->pro_id;
                    if ($input['language_type'] == 'en') {
                        if (!empty($category)) {
                            $catg_data['category_name'] = $category->mc_name;
                            $catg_data['category_img'] = $category->mc_img;
                            $catg_data['address'] = $category->address;
                            $catg_data['city_name'] = $category->getCity->ci_name;
                            $catg_data['longitude'] = $category->longitude;
                            $catg_data['latitude'] = $category->latitude;
 
                        }

                        if (!empty($attribute) && count($attribute->getProductServiceAttribute)>=1) {
                            $catg_data['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title;
                        }
                        $catg_data['pro_title'] = $order_pro->getProduct[0]->pro_title;
                        $catg_data['pro_desc'] = $order_pro->getProduct[0]->pro_desc;
                    } else {
                        if (!empty($category)) {
                            $catg_data['category_name'] = $category->mc_name_ar;
                            $catg_data['category_img'] = $category->mc_img;
                            $catg_data['address'] = $category->address_ar;
                            $catg_data['city_name'] = $category->getCity->ci_name_ar;
                            $catg_data['longitude'] = $category->longitude;
                            $catg_data['latitude'] = $category->latitude;

                        }
                        if (!empty($attribute)  && count($attribute->getProductServiceAttribute)>=1) {
                            $catg_data['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title_ar;
                        }

                        $catg_data['pro_title'] = $order_pro->getProduct[0]->pro_title_ar;
                        $catg_data['pro_desc'] = $order_pro->getProduct[0]->pro_desc_ar;
                    }



                    $catg_data['pro_qty'] = $order_pro->quantity;
                  //  $catg_data['pro_qty'] = $order_pro->getProduct[0]->pro_qty;
                   // $catg_data['pro_price'] = $order_pro->getProduct[0]->pro_price;
                    $catg_data['pro_price'] = $order_pro->getProduct[0]->pro_price;

                    $catg_data['home_visit_charge'] = $category->home_visit_charge;
                    $catg_data['pro_Img'] = $order_pro->getProduct[0]->pro_Img;
                    $catg_data['pro_status'] = $order_pro->getProduct[0]->pro_status;
                    //$catg_data['pro_disprice'] = $order_pro->getProduct[0]->pro_disprice;
                    $catg_data['pro_disprice'] = $order_pro->getProduct[0]->pro_disprice;
                    if (!empty($order_pro->quantity)) {
                        $catg_data['total_price'] = $order_pro->total_price;
                    } else {
                        $catg_data['total_price'] = $order_pro->getProduct[0]->pro_price;
                    }
  

                    $order_service_staff = OrderServiceStaff::where('product_type', '=', 'beauty')->where('order_id', '=', $order->order_id)->where('service_id', $input['product_id'])->with('getStaff:id,staff_member_name,staff_member_name_ar,staff_member_address,staff_member_address_ar,image,experience,availability_status')->first();
 
                    $catg_data['booking_place'] = '';


if(isset($order_service_staff->booking_place) && $order_service_staff->booking_place!='')
{


                      if($order_service_staff->booking_place == 'home' && $input['language_type'] == 'en')
                        {
                        $catg_data['booking_place'] = 'home';
                        }
                        if($order_service_staff->booking_place == 'home' && $input['language_type'] == 'ar')
                        {
                        $catg_data['booking_place'] = 'المنزل';
                        }

                        if($order_service_staff->booking_place == 'shop' && $input['language_type'] == 'en')
                        {
                        $catg_data['booking_place'] = 'shop';
                        }
                        if($order_service_staff->booking_place == 'shop' && $input['language_type'] == 'ar')
                        {
                        $catg_data['booking_place'] = 'متجر';
                        }
}
 
                    if (!empty($order_service_staff) && $order_service_staff->staff_id!='') {

                       
                        
                        if ($order_service_staff->booking_place == 'home') {
                            $dbquerycount = DB::table('nm_customer_shipping_details')->where('customer_id',$User->cus_id)->count(); 
                            if($dbquerycount > 0) {
                            $homeaddress = DB::table('nm_customer_shipping_details')->where('customer_id',$User->cus_id)->first();
                            
                        }
                        $catg_data['address'] = $homeaddress->ship_address1;
                        }
                        $catg_data['staff_id'] = $order_service_staff->getStaff->id;
                        if ($input['language_type'] == 'en') {
                            $catg_data['staff_member_name'] = $order_service_staff->getStaff->staff_member_name;
                            $catg_data['staff_member_address'] = $order_service_staff->getStaff->staff_member_address;
                        } else {
                            $catg_data['staff_member_name'] = $order_service_staff->getStaff->staff_member_name_ar;
                            $catg_data['staff_member_address_ar'] = $order_service_staff->getStaff->staff_member_address;

                        }
                        $catg_data['booking_date'] = date("d-m-Y", strtotime($order_service_staff->booking_date));
                        $catg_data['start_time'] = $order_service_staff->start_time;
                        $catg_data['end_time'] = $order_service_staff->end_time;
                        $catg_data['image'] = $order_service_staff->getStaff->image;
                        $catg_data['experience'] = $order_service_staff->getStaff->experience;
                        $catg_data['availability_status'] = $order_service_staff->getStaff->availability_status;

                    }
                    else
                    {


                        if(isset($order_service_staff->booking_date) && $order_service_staff->booking_date!='')
                        {
                        $catg_data['booking_date'] = date("d-m-Y", strtotime($order_service_staff->booking_date));
                        $catg_data['start_time'] = $order_service_staff->start_time;
                        $catg_data['end_time'] = $order_service_staff->end_time;
                        }
                        else
                        {
                        $catg_data['booking_date'] = '';
                        $catg_data['start_time'] = '';
                        $catg_data['end_time'] = '';
                        }




                    }
                    $packege_data = array();
                    $packege = ProductPackage::select('id', 'shop_id', 'attribute_id', 'item_id', 'packege_id')->where('item_id', '=', $input['product_id'])->with('getProductPackage:pro_id,pro_title,pro_Img,pro_desc,pro_price,attribute_id')->get();
                    foreach($packege as $key => $value) {

                        $packege_data[$key]['pro_id'] = $value->getProductPackage->pro_id;
                        if ($input['language_type'] == 'en') {

                            $packege_data[$key]['pro_title'] = $value->getProductPackage->pro_title;
                            $packege_data[$key]['pro_desc'] = $value->getProductPackage->pro_desc;
                        } else {

                            $packege_data[$key]['pro_title'] = $value->getProductPackage->pro_title_ar;
                            $packege_data[$key]['pro_desc'] = $value->getProductPackage->pro_desc_ar;
                        }

                        $packege_data[$key]['pro_price'] = $value->getProductPackage->pro_price;
                        $packege_data[$key]['pro_Img'] = $value->getProductPackage->pro_Img;

                    }

                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'order_id' => $order->order_id, 'makeup_order_data' => $catg_data);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'order data not found & order type must be beauty and elegance', 'code' => 500]);
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    /**Get Clinic(Cosmetic and Skin) order data**/
    public function getClinicOrderData(Request $request) {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'order_id' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $order = OrderProduct::where('order_cus_id', '=', $User->cus_id)->where('order_id', '=', $input['order_id'])->first();
                $catg_data = array();
                $order_pro = OrderSingleProduct::where('product_type', '=', 'clinic')->where('order_id', '=', $order->order_id)->where('product_id', '=', $input['product_id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,profession_bio')->first();

                //get Clinic's Doctor Data

                if (!empty($order) && !empty($order_pro)) {

                    // get Patient  data from add to order  

                    $attribute = OrderServiceStaff::where('product_type', '=', 'clinic')->where('order_id', '=', $order->order_id)->where('service_id', '=', $input['product_id'])->with('getDoctorServiceAttribute:id,services_id,attribute_title,attribute_title_ar')->first();
     
                    $category = Category::select('mc_id', 'mc_img', 'mc_name', 'mc_name_ar', 'address', 'address_ar', 'mc_status', 'city_id','latitude','longitude')->where('mc_id', '=', $order_pro->getProduct[0]->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();
                 

                    $catg_data['pro_id'] = $order_pro->getProduct[0]->pro_id;
                    if ($input['language_type'] == 'en') {
                        if (!empty($category)) {
                            $catg_data['category_name'] = $category->mc_name;
                            $catg_data['category_img'] = $category->mc_img;
                            $catg_data['address'] = $category->address;
                            $catg_data['city_name'] = $category->getCity->ci_name;
                            $catg_data['latitude'] = $category->latitude;
                            $catg_data['longitude'] = $category->longitude;
                        }

$gertAttribute = DB::table('nm_product')->where('pro_id', '=', $input['product_id'])->select('attribute_id')->first();
                        $Attributeid = $gertAttribute->attribute_id;
                        $gertAttributename = DB::table('nm_services_attribute')->where('id', '=', $Attributeid)->first();
                        $attribute_titl= $gertAttributename->attribute_title;
                        //$attribute_titl=  $gertAttributename->attribute_title_ar;

                        $catg_data['attribute_title'] = $attribute_titl;
                        $catg_data['pro_title'] = $order_pro->getProduct[0]->pro_title;
                        $catg_data['pro_desc'] = $order_pro->getProduct[0]->pro_desc;
                        $name = explode(',', $order_pro->getProduct[0]->profession_bio);
                        $doctor_attr = ProductAttribute::select('id', 'attribute_title')->whereIn('id', $name)->get();
                        $catg_data['profession_bio'] = $doctor_attr;
                    } else {
                        if (!empty($category)) {
                            $catg_data['category_name'] = $category->mc_name_ar;
                            $catg_data['category_img'] = $category->mc_img;
                            $catg_data['address'] = $category->address_ar;
                            $catg_data['city_name'] = $category->getCity->ci_name_ar;
                            $catg_data['latitude'] = $category->latitude;
                            $catg_data['longitude'] = $category->longitude;

                        }
$gertAttribute = DB::table('nm_product')->where('pro_id', '=', $input['product_id'])->select('attribute_id')->first();
                        $Attributeid = $gertAttribute->attribute_id;
                        $gertAttributename = DB::table('nm_services_attribute')->where('id', '=', $Attributeid)->first();
                        //$attribute_titl= $gertAttributename->attribute_title;
                        $attribute_titl=  $gertAttributename->attribute_title_ar;
                        $catg_data['attribute_title'] =$attribute_titl;
                        $catg_data['pro_title'] = $order_pro->getProduct[0]->pro_title_ar;
                        $catg_data['pro_desc'] = $order_pro->getProduct[0]->pro_desc_ar;
                        $name = explode(',', $order_pro->getProduct[0]->profession_bio);
                        $doctor_attr = ProductAttribute::select('id', 'attribute_title_ar as attribute_title')->whereIn('id', $name)->get();
                        $catg_data['profession_bio'] = $doctor_attr;
                    }

                    $catg_data['pro_price'] = $order_pro->getProduct[0]->pro_price;
                    $catg_data['pro_Img'] = $order_pro->getProduct[0]->pro_Img;
                    $catg_data['pro_status'] = $order_pro->getProduct[0]->pro_status;
                    $catg_data['file_no'] = $attribute->file_no;
                    $catg_data['booking_date'] = date("d-m-Y", strtotime($attribute->booking_date));
                    $catg_data['start_time'] = $attribute->start_time;
                    $catg_data['end_time'] = $attribute->end_time;
                    $catg_data['total_price'] = $order_pro->getProduct[0]->pro_price;
                    $catg_data['file_no'] = $attribute->file_no;

                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'order_id' => $order->order_id, 'doctor_data' => $catg_data);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'order data not found & order type must be clinic.', 'code' => 500]);
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    /**Get Music Acoustic Order Data**/
    public function getMusicAcousticOrderData(Request $request) {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'order_id' => 'required',
            'product_id' => 'required',
             'id' => 'required',
            'language_type' => 'required|max:255'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $order = OrderProduct::where('order_cus_id', '=', $User->cus_id)->where('order_id', '=', $input['order_id'])->first();
                $music_data = array();
                $order_pro = OrderSingleProduct::where('id', '=',  $input['id'])->where('product_type', '=', 'music')->where('order_id', '=', $order->order_id)->where('product_id', '=', $input['product_id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_disprice,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,Insuranceamount')->first();
                $music_data['option_title'] =  $order_pro->buy_rent;
                if (!empty($order) && !empty($order_pro)) {

                     $order_option_value_c = OrderOptionValue::where('order_id', '=', $order->order_id)->where('product_type', '=', 'music')->where('product_id', '=', $input['product_id'])->with('getProductOption:id,services_id,option_title,option_title_ar,status')->with('getProductOptionValue:id,product_id,product_option_id,option_title,option_title_ar,status')->count();

                        if($order_option_value_c >=1)
                        {
                        $order_option_value = OrderOptionValue::where('order_id', '=', $order->order_id)->where('product_type', '=', 'music')->where('product_id', '=', $input['product_id'])->with('getProductOption:id,services_id,option_title,option_title_ar,status')->with('getProductOptionValue:id,product_id,product_option_id,option_title,option_title_ar,status')->first();
                        }
           
                   $rental_productsC = OrderProductRent::where('order_product_id', '=',  $input['id'])->where('order_id', '=', $order->order_id)->where('product_type', '=', 'music')->where('product_id', '=', $input['product_id'])->count();  


                    $rental_products = OrderProductRent::where('order_product_id', '=',  $input['id'])->where('order_id', '=', $order->order_id)->where('product_type', '=', 'music')->where('product_id', '=', $input['product_id'])->first();

                    $category = Category::select('mc_id', 'mc_img', 'mc_name', 'mc_name_ar', 'address', 'address_ar', 'mc_status', 'city_id','longitude','latitude')->where('mc_id', '=', $order_pro->getProduct[0]->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();

                    if (isset($order_pro)) { //get product data
                        $music_data['pro_id'] = $order_pro->getProduct[0]->pro_id;

                        if ($input['language_type'] == 'en') {
                            if (!empty($category)) {
                                $music_data['category_name'] = $category->mc_name;
                                $music_data['category_img'] = $category->mc_img;
                                $music_data['address'] = $category->address;
                                $music_data['city_name'] = $category->getCity->ci_name;
                                $music_data['longitude'] = $category->longitude;
                                $music_data['latitude'] = $category->latitude;
                            }


                            if(isset($order_option_value) && count($order_option_value->getProductOptionValue) && $order_option_value_c >=1)
                            {    
                            $music_data['option_title'] = $order_option_value->getProductOptionValue[0]->option_title;
                            }
                            $music_data['pro_title'] = $order_pro->getProduct[0]->pro_title;
                            $music_data['pro_desc'] = $order_pro->getProduct[0]->pro_desc;
 
                        } else {
                            if (!empty($category)) {
                                $music_data['category_name'] = $category->mc_name_ar;
                                $music_data['category_img'] = $category->mc_img;
                                $music_data['address'] = $category->address_ar;
                                $music_data['city_name'] = $category->getCity->ci_name_ar;
                                $music_data['longitude'] = $category->longitude;
                                $music_data['latitude'] = $category->latitude;
                            }
                            if(isset($order_option_value) && count($order_option_value->getProductOptionValue) && $order_option_value_c >=1)
                            {
                            $music_data['option_title'] = $order_option_value->getProductOptionValue[0]->option_title_ar;
                        }
                            $music_data['pro_title'] = $order_pro->getProduct[0]->pro_title_ar;
                            $music_data['pro_desc'] = $order_pro->getProduct[0]->pro_desc_ar;
                        }
                        $music_data['pro_qty'] = $order_pro->quantity;

                        if($order_pro->getProduct[0]->pro_disprice >=1)
                        {
                            $Amt = $order_pro->getProduct[0]->pro_disprice;
                        }
                        else
                        {
                           $Amt = $order_pro->getProduct[0]->pro_price;
                        }







                        if($order_option_value_c >=1 && $rental_productsC >= 1)
                        { 

 
                    $getOpta =  DB::table('nm_product_option_value')->where('product_id', '=', $input['product_id'])->where('option_title','Rent')->count();



                            if($getOpta >=1)
                            {
                            $getOpt =  DB::table('nm_product_option_value')->where('product_id', '=', $input['product_id'])->where('option_title','Rent')->first();

                            $Amt = $getOpt->price - ($getOpt->price * $getOpt->discount)/100;
                            


                            //$music_data['pro_price'] = $aPrice; 
                            }

                        }

 
                        $music_data['pro_price'] = $Amt;
                        $music_data['Insuranceamount'] = $order_pro->getProduct[0]->Insuranceamount;
                        $music_data['pro_Img'] = $order_pro->getProduct[0]->pro_Img;
                        $music_data['pro_status'] = $order_pro->getProduct[0]->pro_status;


                        if(  $rental_productsC < 1 )
                        {

                        if (isset($order_option_value->product_option_value_id) && $order_option_value->product_option_value_id == 44) {
                        $music_data['total_price'] = $Amt * $order_pro->quantity;
                        } else {
                        
                        //Change for acostic detail price //
                        //$music_data['total_price'] = $order_pro->getProduct[0]->Insuranceamount + ($Amt * $order_pro->quantity);

                        $music_data['total_price'] = $order_pro->total_price;
 
                        }


                        }
                        else
                        {
                    $music_data['total_price'] = $order_pro->total_price;
                        //$music_data['total_price'] = $order_pro->getProduct[0]->Insuranceamount + $order_pro->getProduct[0]->pro_price * $order_pro->quantity;
                        }
 
                    } 

                $music_data['total_price'] =  number_format((float)$music_data['total_price'], 2, '.', '');
 
                    if (!empty($rental_products)) { //get rental product

                        $music_data['rental_date'] = date("d-m-Y", strtotime($rental_products->rental_date));
                        $music_data['rental_time'] = $rental_products->rental_time;
                        $music_data['return_date'] = date("d-m-Y", strtotime($rental_products->return_date));
                        $music_data['return_time'] = $rental_products->return_time;
                    }


                    $music_data['buy_rent'] = $order_pro->buy_rent;
 
 


                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'order_id' => $order->order_id, 'music_data' => $music_data);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'order data not found & order type must be music', 'code' => 500]);
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    /**Get Car Rental data from order table**/
    public function getCarRentalOrderData(Request $request) {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'order_id' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $order = OrderProduct::where('order_cus_id', '=', $User->cus_id)->where('order_id', '=', $input['order_id'])->first();

                $order_pro = OrderSingleProduct::where('product_type', '=', 'car_rental')->where('order_id', '=', $order->order_id)->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,pro_disprice')->first();
                if (!empty($order) && !empty($order_pro)) {
                    $order_rental_service = OrderProductRent::where('product_type', '=', 'car_rental')->where('order_id', '=', $order->order_id)->where('product_id', '=', $order_pro->product_id)->first();
                

                    $category = Category::select('mc_id', 'mc_img', 'mc_name', 'mc_name_ar', 'address', 'address_ar', 'mc_status', 'city_id','longitude','latitude')->where('mc_id', '=', $order_pro->getProduct[0]->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();
                    $catg_data = array();
                    $catg_data['pro_id'] = $order_pro->getProduct[0]->pro_id;
                    if ($input['language_type'] == 'en') {
                        if (!empty($category)) {
                            $catg_data['category_name'] = $category->mc_name;
                            $catg_data['category_img'] = $category->mc_img;
                            $catg_data['address'] = $category->address;
                            $catg_data['city_name'] = $category->getCity->ci_name;
                            $catg_data['longitude'] = $category->longitude;
                            $catg_data['latitude'] = $category->latitude;
                        }
                        $catg_data['pro_title'] = $order_pro->getProduct[0]->pro_title;
                        $catg_data['pro_desc'] = $order_pro->getProduct[0]->pro_desc;
                        $product_attr = ProductAttribute::select('id', 'product_id', 'attribute_title', 'value')->where('product_id', '=', $order_pro->getProduct[0]->pro_id)->orderBy('attribute_title', 'ASC')->get(); //GET PRODUCT ATTRIBUTE 
                        $catg_data['product_attribute'] = $product_attr;

                    } else {
                        if (!empty($category)) {
                            $catg_data['category_name'] = $category->mc_name_ar;
                            $catg_data['category_img'] = $category->mc_img;
                            $catg_data['address'] = $category->address_ar;
                            $catg_data['city_name'] = $category->getCity->ci_name_ar;
                            $catg_data['longitude'] = $category->longitude;
                            $catg_data['latitude'] = $category->latitude;
                        }
                        $catg_data['pro_title'] = $order_pro->getProduct[0]->pro_title_ar;
                        $catg_data['pro_desc'] = $order_pro->getProduct[0]->pro_desc_ar;
                        $product_attr = ProductAttribute::select('id', 'product_id', 'attribute_title_ar', 'value')->where('product_id', '=', $order_pro->getProduct[0]->pro_id)->orderBy('attribute_title', 'ASC')->get(); //GET PRODUCT ATTRIBUTE 

                        $catg_data['product_attribute'] = $product_attr;
                    }

                    $catg_data['pro_Img'] = $order_pro->getProduct[0]->pro_Img;
                    $catg_data['status'] = $order_pro->getProduct[0]->pro_status;
                    $catg_data['price'] = $order_pro->getProduct[0]->pro_price;
                    $catg_data['pro_disprice'] = $order_pro->getProduct[0]->pro_disprice;
                    $catg_data['total_price'] = $order_pro->total_price;

 
                    if(isset($order_rental_service->rental_date) && $order_rental_service->rental_date!='')
                    {
                           
                    $catg_data['rental_date'] = date("d-m-Y", strtotime($order_rental_service->rental_date));
                    $catg_data['return_date'] = date("d-m-Y", strtotime($order_rental_service->return_date));
                     
                    }
                    else
                    {
                    $catg_data['rental_date'] ='';
                    $catg_data['return_date'] = '';  
                    }
             
                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'order_id' => $order->order_id, 'makeup_order_data' => $catg_data, );
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'order data not found & order type must be Car Rental ', 'code' => 500]);
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    /**Get Travel Agencies data from Order table**/
    public function getTravelOrderData(Request $request) {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'order_id' => 'required',
            'id' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $order = OrderProduct::where('order_cus_id', '=', $User->cus_id)->where('order_id', '=', $input['order_id'])->first();

                $order_pro = OrderSingleProduct::where('product_type', '=', 'travel')->where('order_id', '=', $order->order_id)->where('id', '=', $input['id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,pro_disprice')->first();
                if (!empty($order) && !empty($order_pro)) {
                    $order_rental_service = OrderProductRent::where('product_type', '=', 'travel')->where('order_id', '=', $order->order_id)->where('order_product_id', '=', $input['id'])->where('product_id', '=', $order_pro->product_id)->first();

             

                    $category = Category::select('mc_id', 'mc_img', 'mc_name', 'mc_name_ar', 'address', 'address_ar', 'mc_status', 'city_id','longitude','latitude')->where('mc_id', '=', $order_pro->getProduct[0]->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();

                    $catg_data = array();
                    $catg_data['pro_id'] = $order_pro->getProduct[0]->pro_id;

                    if ($input['language_type'] == 'en') {
                        if (!empty($category)) {
                            $catg_data['category_name'] = $category->mc_name;
                            $catg_data['category_img'] = $category->mc_img;
                            $catg_data['address'] = $category->address;
                            $catg_data['city_name'] = $category->getCity->ci_name;
                            $catg_data['longitude'] = $category->longitude;
                            $catg_data['latitude'] = $category->latitude;
                        }
                        $catg_data['pro_title'] = $order_pro->getProduct[0]->pro_title;
                        $catg_data['pro_desc'] = $order_pro->getProduct[0]->pro_desc;

                        $catg_data['quantity'] = $order_pro->quantity;

                        $product_attr = ProductAttribute::select('id', 'product_id', 'attribute_title', 'value')->where('product_id', '=', $order_pro->getProduct[0]->pro_id)->orderBy('attribute_title', 'ASC')->get(); //GET PRODUCT ATTRIBUTE 
                        $catg_data['product_attribute'] = $product_attr;

                    } else {
                        if (!empty($category)) {
                            $catg_data['category_name'] = $category->mc_name_ar;
                            $catg_data['category_img'] = $category->mc_img;
                            $catg_data['address'] = $category->address_ar;
                            $catg_data['city_name'] = $category->getCity->ci_name_ar;
                            $catg_data['longitude'] = $category->longitude;
                            $catg_data['latitude'] = $category->latitude;
                        }
                        $catg_data['pro_title'] = $order_pro->getProduct[0]->pro_title_ar;
                        $catg_data['pro_desc'] = $order_pro->getProduct[0]->pro_desc_ar;
                          $catg_data['quantity'] = $order_pro->quantity;
                        $product_attr = ProductAttribute::select('id', 'product_id', 'attribute_title_ar', 'value')->where('product_id', '=', $order_pro->getProduct[0]->pro_id)->orderBy('attribute_title', 'ASC')->get(); //GET PRODUCT ATTRIBUTE 

                        $catg_data['product_attribute'] = $product_attr;
                    }

                    $catg_data['pro_Img'] = $order_pro->getProduct[0]->pro_Img;
                    $catg_data['status'] = $order_pro->getProduct[0]->pro_status;
                    $catg_data['price'] = $order_pro->getProduct[0]->pro_price;
                    $catg_data['pro_disprice'] = $order_pro->getProduct[0]->pro_disprice;

                    
                    $catg_data['service_date'] = date("d-m-Y", strtotime($order_rental_service->rental_date));
                    $catg_data['service_time'] = $order_rental_service->rental_time;

                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'order_id' => $order->id, 'makeup_order_data' => $catg_data, );
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'order data not found & order type must be Travel Agencies ', 'code' => 500]);
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    /**Get Shopping data from Order Table**/
    public function getShoppingOrderData(Request $request) {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'order_id' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $order = OrderProduct::where('order_cus_id', '=', $User->cus_id)->where('order_id', '=', $input['order_id'])->first();


                $shopping_pro = array();

if(isset($input['id']) && $input['id']!='')
{
  $order_pro = OrderSingleProduct::where('id',  '=', $input['id'])->where('product_type', '=', 'shopping')->where('product_id', '=', $input['product_id'])->where('order_id', '=', $order->order_id)->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,pro_disprice,Insuranceamount,pro_no_of_purchase,deliver_day')->first();  
}
else
{
    $order_pro = OrderSingleProduct::where('product_type', '=', 'shopping')->where('product_id', '=', $input['product_id'])->where('order_id', '=', $order->order_id)->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,pro_disprice,Insuranceamount,pro_no_of_purchase,deliver_day')->first();
}
          $buy_rent = $order_pro->buy_rent;     

               
                $category = Category::select('mc_id', 'mc_img', 'mc_name', 'mc_name_ar', 'address', 'address_ar', 'mc_status','longitude','latitude')->where('mc_id', '=', $order_pro->getProduct[0]->pro_mc_id)->first();
                    //print_r($order_pro);
                   // die;
                  
                if (!empty($order) && !empty($order_pro)) {

                    //get  product 

                    $attribute = OrderServiceAttribute::where('product_type', '=', 'shopping')->where('order_id', '=', $order->order_id)->where('product_id', '=', $order_pro->product_id)->with('getProductServiceAttribute:id,services_id,attribute_title,attribute_title_ar')->first();

                    $order_option_value = OrderOptionValue::where('order_id', '=', $order->order_id)->where('product_type', '=', 'shopping')->where('product_id', '=', $input['product_id'])->with('getProductOption:id,services_id,option_title,option_title_ar,status')->with('getProductOptionValue:id,product_id,product_option_id,option_title,option_title_ar,price,status')->first();

                    $shopping_pro['pro_id'] = $order_pro->getProduct[0]->pro_id;


                    $getfabc = DB::table('nm_fabric')->where('id',$order_pro->fabric_id)->count();
                    if($getfabc >=1)
                    {
                    $getfab = DB::table('nm_fabric')->where('id',$order_pro->fabric_id)->first();
                    if (isset($request->language_type) && $request->language_type == 'ar') {
                    $shopping_pro['fabric_name'] =$getfab->fabric_name_ar;
                    }
                    else
                    {
                    $shopping_pro['fabric_name'] =$getfab->fabric_name;
                    }
                    $shopping_pro['fabric_image'] =$getfab->image;
                    $shopping_pro['fabric_price'] =$getfab->price;
                    }
                    else
                    {
                    $shopping_pro['fabric_name'] ='';           
                    $shopping_pro['fabric_image'] ='';
                    $shopping_pro['fabric_price'] ='';
                    } 
                    if ($input['language_type'] == 'en') {
                        if (!empty($category)) {
                            $shopping_pro['category_name'] = $category->mc_name;
                            $shopping_pro['category_img'] = $category->mc_img;
                            $shopping_pro['address'] = $category->address;
                            $shopping_pro['longitude'] = $category->longitude;
                            $shopping_pro['latitude'] = $category->latitude;
                        }

                        if (!empty($attribute)) {
                            $shopping_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title;


                        }
                        if (!empty($order_option_value)) {

              if(isset($order_option_value->getProductOptionValue) && count($order_option_value->getProductOptionValue)>=1)
                            {
                              $shopping_pro['option_title'] = $order_option_value->getProductOptionValue[0]->option_title;  
                            }
                            else
                            {
                                $shopping_pro['option_title'] = ''; 
                            }
                          

                        }



                        $shopping_pro['pro_title'] = $order_pro->getProduct[0]->pro_title;
                        $shopping_pro['pro_desc'] = $order_pro->getProduct[0]->pro_desc;
                        $shopping_pro['Insuranceamount'] = $order_pro->getProduct[0]->Insuranceamount;

                         $shopping_pro['deliverydays'] = $order_pro->getProduct[0]->deliver_day;
                      
                         $shopping_pro['created_at'] = strtotime($order_pro->created_at);
 
 

                    } else {
                        if (!empty($category)) {
                            $shopping_pro['category_name'] = $category->mc_name_ar;
                            $shopping_pro['category_img'] = $category->mc_img;
                            $shopping_pro['address'] = $category->address_ar;
                            $shopping_pro['longitude'] = $category->longitude;
                            $shopping_pro['latitude'] = $category->latitude;
                        }
                        if (!empty($attribute)) {
                            $shopping_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title_ar;
                        }
                        
                    if (!empty($order_option_value)) {

                    if(isset($order_option_value->getProductOptionValue) && count($order_option_value->getProductOptionValue)>=1)
                    {
                    $shopping_pro['option_title'] = $order_option_value->getProductOptionValue[0]->option_title_ar;  
                    }
                    else
                    {
                    $shopping_pro['option_title'] = ''; 
                    }
 
                    } 

                        $shopping_pro['pro_title'] = $order_pro->getProduct[0]->pro_title_ar;
                        $shopping_pro['pro_desc'] = $order_pro->getProduct[0]->pro_desc_ar;

                    $shopping_pro['Insuranceamount'] = $order_pro->getProduct[0]->Insuranceamount;

                         $shopping_pro['deliverydays'] = $order_pro->getProduct[0]->deliver_day;
                      
                         $shopping_pro['created_at'] = strtotime($order_pro->created_at);
 





                    }

                        $getProID = $order_pro->getProduct[0]->pro_id;

                        $ChkCliberr = DB::table('nm_product_attribute')->where('attribute_title', 'caliber')->where('product_id', $getProID)->count();
                        if($ChkCliberr >=1)
                        {
                        $ChkCliber = DB::table('nm_product_attribute')->where('attribute_title', 'caliber')->where('product_id', $getProID)->first();
                        $ChkWight = DB::table('nm_product_attribute')->where('attribute_title', 'weight')->where('product_id', $getProID)->first(); 
                        $shopping_pro['caliber'] = $ChkCliber->value;
                        $shopping_pro['weight'] =$ChkWight->value;
                        }
                        else
                        {
                          $shopping_pro['caliber'] ='';
                        $shopping_pro['weight'] ='';  
                        }

                    $shopping_pro['size'] = $order_pro->product_size;
                    $shopping_pro['pro_qty'] = $order_pro->quantity;

                    $shopping_pro['pro_Img'] = $order_pro->getProduct[0]->pro_Img;
                    $shopping_pro['pro_status'] = $order_pro->getProduct[0]->pro_status;

                    if (!empty($order_option_value) && $order_option_value->product_option_id == 20) {

                        $shopping_pro['Insuranceamount'] = $order_pro->getProduct[0]->Insuranceamount;

                        $shopping_pro['pro_price'] = $order_pro->getProduct[0]->price;
                     
                        $shopping_pro['total_price'] = $order_pro->total_price;

                        $shopping_pro['pro_disprice'] = $order_pro->getProduct[0]->pro_disprice;


                         $shopping_pro['deliverydays'] = $order_pro->getProduct[0]->deliver_day;
                      
                         $shopping_pro['created_at'] = strtotime($order_pro->created_at);
 

                        
                    } else {
                        $shopping_pro['pro_price'] = $order_pro->getProduct[0]->pro_price;
                        $shopping_pro['pro_disprice'] = $order_pro->getProduct[0]->pro_disprice;
                        $shopping_pro['total_price'] = $order_pro->total_price;
                         $shopping_pro['deliverydays'] = $order_pro->getProduct[0]->deliver_day;                      
                         $shopping_pro['created_at'] = strtotime($order_pro->created_at);
                        
                        
                    }
                    $rental_pro = OrderProductRent::where('product_type', '=', 'shopping')->where('order_id', '=', $order->order_id)->where('product_id', '=', $order_pro->product_id)->first(); //get rental data
                    if (!empty($rental_pro) && $buy_rent=='rent') {

                        $shopping_pro['rental_date'] = date("d-m-Y", strtotime($rental_pro->rental_date));
                        $shopping_pro['return_date'] = date("d-m-Y", strtotime($rental_pro->return_date));
                        $shopping_pro['insurance_amount'] = $rental_pro->insurance_amount;

                        $shopping_pro['rental_time'] = $rental_pro->rental_time;




                    $rentalpricedata = ProductOptionValue::where('option_title','Rent')->where('product_id',$getProID)->first();
                    $price = $rentalpricedata->price;
                    $rentalpricedatacc = ProductOptionValue::where('option_title','Discount for rent')->where('product_id',$getProID)->count();
                    if($rentalpricedatacc >=1)
                    {
                    $rentalpricedataa = ProductOptionValue::where('option_title','Discount for rent')->where('product_id',$getProID)->first();
                    $pro_disprice = $price- ($price * $rentalpricedataa->discount)/100;
                    }
                    else
                    {

                    $pro_disprice = 0;
                    }


 

                         $shopping_pro['pro_price'] = $price;
                        $shopping_pro['pro_disprice'] = $pro_disprice;


                        $shopping_pro['total_price'] = $order_pro->total_price;

                        $shopping_pro['return_time'] = $rental_pro->return_time;


                        // $date1 = date_create($rental_pro->rental_time);
                        // $date2 = date_create($rental_pro->return_time);
                        // $diff = date_diff($date1, $date2);
                        // $diff->format("%H:%I");
                        // $shopping_pro['total_time'] = $diff->format("%H").
                        // ' hours';

                         $shopping_pro['total_time'] ='1 hours';

                         $shopping_pro['option_title'] = 'Rent';
                    
                    }
                    else
                    {
                        $shopping_pro['option_title'] = 'Buy';
                    }
                    $measurement = OrderBodyMeasurement::where('order_id', '=', $order->order_id)->where('product_id', '=', $order_pro->product_id)->first(); // get measurement data
                    if (!empty($measurement)) {

                        $shopping_pro['length'] = $measurement->length;
                        $shopping_pro['chest_size'] = $measurement->chest_size;
                        $shopping_pro['waistsize'] = $measurement->waistsize;
                        $shopping_pro['soulders'] = $measurement->soulders;
                        $shopping_pro['neck'] = $measurement->neck;
                        $shopping_pro['arm_length'] = $measurement->arm_length;
                        $shopping_pro['wrist_diameter'] = $measurement->wrist_diameter;
                        $shopping_pro['cutting'] = $measurement->cutting;

                    }
if($shopping_pro['pro_price']==null)
{
    $shopping_pro['pro_price'] = $order_pro->pro_price;
}
                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'order_id' => $order->id, 'shopping_data' => $shopping_pro, );
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'order data not found & order type must be Shopping', 'code' => 500]);
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    /**Get Occasion  data from Order table**/
    public function getOccasionOrderData(Request $request) {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'order_id' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
 
                $order = OrderProduct::where('order_cus_id', '=', $User->cus_id)->where('order_id', '=', $input['order_id'])->first();
                $occasion_pro = array();
                 //$order_pro = OrderSingleProduct::where('product_type', '=', 'occasion')->where('order_id', '=', $order->order_id)->where('product_id', '=', $input['product_id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_disprice,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id')->first();
 

                 $order_pro = OrderSingleProduct::where('product_type','=','occasion')->where('order_id','=',$order->order_id)->where('product_id','=',$input['product_id'])->where('quantity','<','1')->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,pro_disprice')->first();

 
                $cart_pro_count = OrderSingleProduct::where('product_type','=','occasion')->where('order_id','=',$order->order_id)->where('product_id','=',$input['product_id'])->count();
                 $cart_pro_count_kosha = OrderSingleProduct::where('product_sub_type','=','cosha')->where('product_type','=','occasion')->where('order_id','=',$order->order_id)->where('product_id','=',$input['product_id'])->count();

                $productInfo = Products::select('parent_attribute_id','packege')->where('pro_id', $input['product_id'])->first();
                  $getAttr = $productInfo->parent_attribute_id;
                     $packege = $productInfo->packege;
                    if($cart_pro_count_kosha < 1) { $getAttr =2; }
                //get cosha product product_sub_type
                
                $Hosorder_pro = OrderSingleProduct::where('product_sub_type','=','hospitality')->where('order_id','=',$order->order_id)->where('product_id','=',$input['product_id'])->count();
                    if($Hosorder_pro >=1 )
                    {
                    if($packege =='yes')
                    {
                    $getAttr =2; 
                    }
                    else
                    {
                    $getAttr =3;
                    }
                    } 

                //if (!empty($order) && !empty($order_pro)) {
                      if (!empty($order) &&  $getAttr ==2  ){
                    //get cosha product 
 
                        $order_pro = OrderSingleProduct::where('product_type', '=', 'occasion')->where('order_id', '=', $order->order_id)->where('product_id', '=', $input['product_id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_disprice,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,Insuranceamount')->first();

$getInfp = DB::table('nm_order_reception_hospitality')->where('order_id', '=', $order->order_id)->where('order_type', '=', 'Package')->count();




                    $attribute = OrderServiceAttribute::where('product_type', '=', 'occasion')->where('order_id', '=', $order->order_id)->where('product_id', '=', $input['product_id'])->with('getProductServiceAttribute:id,services_id,attribute_title,attribute_title_ar')->with('getSubProductAttribute:id,services_id,attribute_title,attribute_title_ar')->first();
                       // print_r($order_pro); die;

                    $category = Category::select('mc_id', 'mc_img', 'mc_name', 'mc_name_ar', 'address', 'address_ar', 'mc_status', 'city_id','longitude','latitude')->where('mc_id', '=', $order_pro->getProduct[0]->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();
                    
                    $occasion_pro['pro_id'] = $order_pro->getProduct[0]->pro_id;
                    if ($input['language_type'] == 'en') {
                     
                        if (!empty($category)) {
 
                            $occasion_pro['category_name'] = $category->mc_name;
                            $occasion_pro['category_img'] = $category->mc_img;
                            $occasion_pro['address'] = $category->address;
                            if(isset($category->getCity->ci_name) && $category->getCity->ci_name!='')
                            {
                             $occasion_pro['city_name'] = $category->getCity->ci_name;   
                            }
                            else
                            {
                                $occasion_pro['city_name'] = '';
                            }
                            
                            $occasion_pro['longitude'] = $category->longitude;
                            $occasion_pro['latitude'] = $category->latitude;
                        }
                        if(isset($attribute->getProductServiceAttribute) && count($attribute->getProductServiceAttribute)>=1)
                        {
                        $occasion_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title; 
                        }
                        else
                        {
                        $occasion_pro['attribute_title'] ='';
                        }
                
                        if(isset($attribute->quantity) && $attribute->quantity!='')
                        {
                        $occasion_pro['quantity'] = $attribute->quantity;  
                        }
                        else
                        {
                        $occasion_pro['quantity'] = 1;
                        }
               

                        
                        $occasion_pro['pro_title'] = $order_pro->getProduct[0]->pro_title;
                        $occasion_pro['pro_desc'] = $order_pro->getProduct[0]->pro_desc;
                    } else {

                        if (!empty($category)) {


                            $occasion_pro['category_name'] = $category->mc_name_ar;
                            $occasion_pro['category_img'] = $category->mc_img;
                            $occasion_pro['address'] = $category->address_ar;
                            if(isset($category->getCity->ci_name_ar) && $category->getCity->ci_name_ar!='')
                            {
                               $occasion_pro['city_name'] = $category->getCity->ci_name_ar;  
                            }
                            else
                            {
                                 $occasion_pro['city_name'] = '';
                            }
                           
                            $occasion_pro['longitude'] = $category->longitude;
                            $occasion_pro['latitude'] = $category->latitude;
                        }
                    if(isset($attribute->getProductServiceAttribute) && count($attribute->getProductServiceAttribute)>=1)
                    {
                       $occasion_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title_ar;

                    }
                    else
                    {
                      $occasion_pro['attribute_title'] = '';      
                    }

                        if(isset($attribute->quantity) && $attribute->quantity!='')
                        {
                        $occasion_pro['quantity'] = $attribute->quantity;
                        }
                        else
                        {
                         $occasion_pro['quantity'] = 1;   
                        }
                        if($order_pro->getProduct[0]->pro_title_ar !='')
                        {
                           $occasion_pro['pro_title'] = $order_pro->getProduct[0]->pro_title_ar;  
                        }
                        else
                        {
                          $occasion_pro['pro_title'] = $order_pro->getProduct[0]->pro_title;  
                        }
                                       

                        $occasion_pro['pro_desc'] = $order_pro->getProduct[0]->pro_desc_ar;
                    }
                     
                    $occasion_pro['pro_qty'] = $order_pro->quantity;
                    $occasion_pro['pro_price'] = $order_pro->getProduct[0]->pro_price;
                    $occasion_pro['pro_disprice'] = $order_pro->getProduct[0]->pro_disprice;
                    $occasion_pro['pro_Img'] = $order_pro->getProduct[0]->pro_Img;
                    $occasion_pro['pro_status'] = $order_pro->getProduct[0]->pro_status;
                    $occasion_pro['total_price'] = $order_pro->total_price;
                    $occasion_pro['location'] = $order_pro->location;
                   $order_rental_service = OrderProductRent::where('product_type', '=', 'occasion')->where('order_id', '=', $order->order_id)->where('product_id', '=', $order_pro->product_id)->first();
                    
                    $occasion_pro['insurance'] = $order_pro->getProduct[0]->Insuranceamount;  
                    if (!empty($order_rental_service)) {
                    $occasion_pro['service_date'] = date("d-m-Y", strtotime($order_rental_service->rental_date));
                   
                    $occasion_pro['service_time'] = $order_rental_service->rental_time;
                    }

                    $product_attr = ProductAttribute::select('id', 'product_id', 'vendor_id', 'attribute_title_ar', 'attribute_title', 'value', 'status')->where('product_id', '=', $input['product_id'])->get();
                    $product_attribute = array();
                    if (isset($product_attr)) { //get service
                        foreach($product_attr as $key => $value) {

                            $product_attribute[$key]['id'] = $value->id;

                            if ($input['language_type'] == 'en') {
                                $product_attribute[$key]['attribute_title'] = $value->attribute_title;
                            } else {
                                $product_attribute[$key]['attribute_title'] = $value->attribute_title_ar;
                            }

                            $product_attribute[$key]['product_id'] = $value->product_id;
                            $product_attribute[$key]['value'] = $value->value;
                            $product_attribute[$key]['status'] = $value->status;
                        }
                    }

                    $order_option_value = OrderOptionValue::where('product_id', '=', $input['product_id'])->where('order_id', '=', $order->order_id)->where('product_type', '=', 'occasion')->with('getProductOption:id,vendor_id,services_id,option_title,option_title_ar,service_hour')->with('getProductOptionValue:id,vandor_id,product_id,product_option_id,option_title,option_title_ar,short_name,value,image,price,no_person,status')->get();
                    $option_value_data = array();
                    if (isset($order_option_value)) { //get service
                        foreach($order_option_value as $key => $value) {

                            if ($input['language_type'] == 'en') {
                                $option_value_data[$key]['option_title'] = $value->getProductOption->option_title;
                                $option_value_data[$key]['value_title'] = $value->getProductOptionValue[0]->option_title;                             
                            } else {
                                $option_value_data[$key]['option_title'] = $value->getProductOption->option_title_ar;
                                $option_value_data[$key]['value_title'] = $value->getProductOptionValue[0]->option_title_ar;
                            }
                             $option_value_data[$key]['option_title_id'] = $value->getProductOption->id;
                            $option_value_data[$key]['price'] = $value->getProductOptionValue[0]->price;
                            $option_value_data[$key]['no_person'] = $value->getProductOptionValue[0]->no_person;
                            $option_value_data[$key]['status'] = $value->getProductOptionValue[0]->status;
                        }
                    }

 

 $order_invitationC = OrderInvitation::where('services_id', '=', $order_pro->category_id)->where('order_id', '=', $order->order_id)->with('getPackageProduct:pro_id,pro_title,pro_title_ar,pro_desc,pro_desc_ar,pro_Img,pro_price,pro_status')->count();
  $occasion_pro['invitation_msg'] ='';
                        if ($order_invitationC >=1) {
$order_invitation = OrderInvitation::where('services_id', '=', $order_pro->category_id)->where('order_id', '=', $order->order_id)->with('getPackageProduct:pro_id,pro_title,pro_title_ar,pro_desc,pro_desc_ar,pro_Img,pro_price,pro_status')->first();
                            $occasion_pro['occasion_name'] = $order_invitation->occasion_name;
                            $occasion_pro['occasion_type'] = $order_invitation->occasion_type;
                            $occasion_pro['venue'] = $order_invitation->venue;
                            $occasion_pro['date'] = date("d-m-Y", strtotime($order_invitation->date));
                            $occasion_pro['time'] = $order_invitation->time;
                            $occasion_pro['no_of_invitees'] = $order_invitation->no_of_invitees;
                            $occasion_pro['invitation_type'] = $order_invitation->invitation_type;

                            $occasion_pro['pro_id'] = $order_invitation->getPackageProduct->pro_id;
                            if ($input['language_type'] == 'en') {
                                $occasion_pro['label'] = $order_invitation->pro_label;
                                $occasion_pro['pro_title'] = $order_invitation->getPackageProduct->pro_title;
                                $occasion_pro['pro_desc'] = $order_invitation->getPackageProduct->pro_desc;
                            } else {

                                $occasion_pro['pro_title'] = $order_invitation->getPackageProduct->pro_title_ar;
                                $occasion_pro['pro_desc'] = $order_invitation->getPackageProduct->pro_desc_ar;
                                $occasion_pro['label'] = $order_invitation->pro_label_ar;
                            }
                                $goldeninvitationuntiprice=$order_pro->total_price/$order_invitation->total_member_invitation;
                            //$occasion_pro['pro_price'] = $order_invitation->getPackageProduct->pro_price;
                                $occasion_pro['pro_price'] = $goldeninvitationuntiprice;
                            $occasion_pro['total_member_invitation'] = $order_invitation->total_member_invitation;
                            $occasion_pro['pro_Img'] = $order_invitation->getPackageProduct->pro_Img;
                            $occasion_pro['total_price'] = $order_pro->total_price;
                            $occasion_pro['invitation_msg'] = $order_invitation->invitation_msg;
                             
                        }
 
                        if($getInfp >=1)
                        {
                        $getInfoas = DB::table('nm_order_reception_hospitality')->where('order_id', '=', $order->order_id)->where('order_type', '=', 'Package')->first();
                        $occasion_pro['location'] = $getInfoas->elocation;
                        $occasion_pro['time'] =$getInfoas->time;
                        $occasion_pro['date'] = date("d-m-Y", strtotime($getInfoas->date));  
                        }
 

                      $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'order_id' => $order->order_id, 'Occasion_data' => $occasion_pro, 'product_attribute' => $product_attribute, 'product_option_data' => $option_value_data);
                } else {

                 $order_pro = OrderSingleProduct::where('product_type', '=', 'occasion')->where('order_id', '=', $order->order_id)->where('product_id','=',$input['product_id'])->first();
                   $cats = $order_pro->category_id;  
                    //$order_pro = OrderSingleProduct::where('product_type', '=', 'occasion')->where('order_id', '=', $order->order_id)->where('category_id', '=', $input['product_id'])->first();
                    //get cosha product  product.
                    if (!empty($order) && !empty($order_pro)) {
                        //get cosha product with service attribute
                        $occasion_pro = array();
                        $occasion_branch = array();
                        $attribute = OrderServiceAttribute::where('product_type', '=', 'occasion')->where('order_id', '=', $order->order_id)->where('category_id', '=', $cats)->with('getProductServiceAttribute:id,services_id,attribute_title,attribute_title_ar')->with('getSubProductAttribute:id,services_id,attribute_title,attribute_title_ar')->with('getProducts:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,Insuranceamount,pro_disprice')->get();



                        foreach($attribute as $key => $value) {
                            if(isset($value->getProducts[0]->pro_id) && $value->getProducts[0]->pro_id!='') {
                       
                            $ProiIID = $value->getProducts[0]->pro_id;
                            $productInfos = Products::select('parent_attribute_id')->where('pro_id', $ProiIID)->first();
                            if($productInfos->parent_attribute_id==2) { continue;}
                            $occasion_pro[$key]['pro_id'] = $value->getProducts[0]->pro_id;
                            $occasion_pro[$key]['Insuranceamount'] = $value->getProducts[0]->Insuranceamount;
                            }
                            if ($input['language_type'] == 'en') {
                                if ($value->getProductServiceAttribute[0]->id == 1) {

                                    $occasion_pro[$key]['attribute_title'] = 'Design Your Package';

                                } else {

                                    $occasion_pro[$key]['attribute_title'] = $value->getProductServiceAttribute[0]->attribute_title;
                                }

                               if(isset($value->getSubProductAttribute[0]->attribute_title) && $value->getSubProductAttribute[0]->attribute_title!='') {

                                $occasion_pro[$key]['sub_attribute_title'] = $value->getSubProductAttribute[0]->attribute_title;
                                 }

                              $occasion_pro[$key]['quantity'] = $value->quantity;
                             if(isset($value->getProducts[0]->pro_title) && $value->getProducts[0]->pro_title!='') {
                                $occasion_pro[$key]['pro_title'] = $value->getProducts[0]->pro_title;
                                 }

                             if(isset($value->getProducts[0]->pro_desc) && $value->getProducts[0]->pro_desc!='') {
                                $occasion_pro[$key]['pro_desc'] = $value->getProducts[0]->pro_desc;
                            }
                            } else {
                                if ($value->getProductServiceAttribute[0]->id == 1) {

                                    $occasion_pro[$key]['attribute_title'] = 'تصميم الحزمة الخاصة بك';

                                } else {
                                    $occasion_pro[$key]['attribute_title'] = $value->getProductServiceAttribute[0]->attribute_title_ar;
                                }

                               if(isset($value->getSubProductAttribute[0]->attribute_title_ar) && $value->getSubProductAttribute[0]->attribute_title_ar!='') { 

                                $occasion_pro[$key]['sub_attribute_title'] = $value->getSubProductAttribute[0]->attribute_title_ar;
                              }

                                $occasion_pro[$key]['quantity'] = $value->quantity;

                           if(isset($value->getProducts[0]->pro_title_ar) && $value->getProducts[0]->pro_title_ar!='') {   
                                $occasion_pro[$key]['pro_title'] = $value->getProducts[0]->pro_title_ar;

                            }
                            else
                            {
                             $occasion_pro[$key]['pro_title'] = $value->getProducts[0]->pro_title;   
                            }
  

                             if(isset($value->getProducts[0]->pro_desc_ar) && $value->getProducts[0]->pro_desc_ar!='') {
                                $occasion_pro[$key]['pro_desc'] = $value->getProducts[0]->pro_desc_ar;

                             }
                            }

                          if(isset($value->getProducts[0]->pro_qty) && $value->getProducts[0]->pro_qty!='') {
                            $occasion_pro[$key]['pro_qty'] = $value->getProducts[0]->pro_qty;

                             }

                            if(isset($value->getProducts[0]->pro_price) &&$value->getProducts[0]->pro_price!='') {
                            if($value->getProducts[0]->pro_disprice >=1)
                            {
                             $occasion_pro[$key]['pro_price'] = $value->getProducts[0]->pro_disprice;    
                            }
                            else
                            {
                             $occasion_pro[$key]['pro_price'] = $value->getProducts[0]->pro_price;   
                            }
                            
                          }


                            $occasion_pro[$key]['pro_Img'] = $value->getProducts[0]->pro_Img;
                            $occasion_pro[$key]['pro_status'] = $value->getProducts[0]->pro_status;

                            if($value->getProducts[0]->pro_disprice >=1)
                            {
                            $occasion_pro[$key]['total_price'] = $value->getProducts[0]->pro_disprice;   
                            }
                            else
                            {
                            $occasion_pro[$key]['total_price'] = $value->getProducts[0]->pro_price;
                            }
                            
 
                      }

                        $branch = Category::where('mc_status', '=', 1)->where('mc_id', '=', $cats)->select('mc_id', 'mc_name_ar', 'mc_name', 'mc_img', 'mc_discription', 'city_id', 'address', 'address_ar', 'longitude', 'latitude')->with('getCity:ci_id,ci_name_ar,ci_name')->first();

                        if (!empty($branch)) {
                            $occasion_branch['mc_id'] = $branch->mc_id;
                            if ($input['language_type'] == 'en') {

                                $occasion_branch['longitude'] = $branch->longitude;
                                $occasion_branch['latitude'] = $branch->latitude;
                                $occasion_branch['mc_name'] = $branch->mc_name;
                                $occasion_branch['mc_img'] = $branch->mc_img;
                                $occasion_branch['mc_discription'] = $branch->mc_discription;
                                if(isset($branch->getCity->ci_name) && $branch->getCity->ci_name) {
                                $occasion_branch['city_name'] = $branch->getCity->ci_name;
                                 }
                                $occasion_branch['address'] = $branch->address;
                            } else {
                                 $occasion_branch['longitude'] = $branch->longitude;
                                $occasion_branch['latitude'] = $branch->latitude;
                                $occasion_branch['mc_name_ar'] = $branch->mc_name_ar;
                                $occasion_branch['mc_img'] = $branch->mc_img;
                                $occasion_branch['mc_discription'] = $branch->mc_discription;
                                $occasion_branch['city_name'] = $branch->getCity->ci_name_ar;
                                $occasion_branch['address'] = $branch->address_ar;
                            }
                        }



                        $Occasion_nationlaity = OrderHospitality::where('order_id', '=', $order->order_id)->with('getNationality')->first();
                        if (isset($Occasion_nationlaity)) {
                        $nationality = $Occasion_nationlaity->nationality;
$getPrice = DB::table('nm_service_staff_nationality')->where('nation_id', $nationality)->where('shop_id',$order_pro->category_id)->first();
if(isset($getPrice))
{
$Occasion_nationlaity->nation_price = $getPrice->worker_price;    
}

}

                        $order_invitation = OrderInvitation::where('services_id', '=', $order_pro->category_id)->where('order_id', '=', $order->order_id)->with('getPackageProduct:pro_id,pro_title,pro_title_ar,pro_desc,pro_desc_ar,pro_Img,pro_price,pro_status')->first();

                        if (isset($order_invitation)) {

                            $occasion_pro['occasion_name'] = $order_invitation->occasion_name;
                            $occasion_pro['occasion_type'] = $order_invitation->occasion_type;
                            $occasion_pro['venue'] = $order_invitation->venue;
                            $occasion_pro['date'] = date("d-m-Y", strtotime($order_invitation->date));
                            $occasion_pro['time'] = $order_invitation->time;
                            $occasion_pro['no_of_invitees'] = $order_invitation->no_of_invitees;
                            $occasion_pro['invitation_type'] = $order_invitation->invitation_type;

                            $occasion_pro['pro_id'] = $order_invitation->getPackageProduct->pro_id;
                            if ($input['language_type'] == 'en') {

                                $occasion_pro['pro_title'] = $order_invitation->getPackageProduct->pro_title;
                                $occasion_pro['pro_desc'] = $order_invitation->getPackageProduct->pro_desc;
                            } else {

                                $occasion_pro['pro_title'] = $order_invitation->getPackageProduct->pro_title_ar;
                                $occasion_pro['pro_desc'] = $order_invitation->getPackageProduct->pro_desc_ar;
                            }

                            $goldeninvitationuntiprice=$order_pro->total_price/$order_invitation->total_member_invitation;
                            //$occasion_pro['pro_price'] = $order_invitation->getPackageProduct->pro_price;
                                $occasion_pro['pro_price'] = $goldeninvitationuntiprice;
                            $occasion_pro['total_member_invitation'] = $order_invitation->total_member_invitation;
                            $occasion_pro['pro_Img'] = $order_invitation->getPackageProduct->pro_Img;
                            $occasion_pro['total_price'] = $order_pro->total_price;
                            $occasion_pro['invitation_msg'] = $order_invitation->invitation_msg;


                        }

                        $response_array = array('data' => ['success' => true, 'message' => 'Data Found1', 'code' => 200], 'order_id' => $order->order_id, 'occasion_branch' => $occasion_branch, 'Occasion_data' => $occasion_pro, 'Occasion_nationlaity' => $Occasion_nationlaity);
                    } else {
                        $response_array = array('data' => ['success' => false, 'message' => 'order data not found & order type must be cosha', 'code' => 500]);
                    }

                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

}
<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Http\Models;
use App\Category;
use App\Products;
use App\ServiceAttribute;
use App\OrderServicesStaff;
use App\ProductAttribute;
use App\Cart;
use App\CartProduct;
use App\CartServiceStaff;
use App\DoctorPrescription;
use Auth;
use App\User;
use Response;
use DB;
class ClinicsController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |   Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function __construct() {}

    /**Get Men's Saloon info.**/

    /*
       vendor_id(3th lavel category in category table) become pro_mc_id in product table to get full Men's Saloon info.
    */
    public function getClinicInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id' => 'required', //Vendor ID
            'branch_id' => 'required', //Branch ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
       
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first();

                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($input['branch_id']);

                if (!empty($category)) {

                    if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                        
                        //GET SUB CATEGORY
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->with('getCity:ci_id,ci_name_ar as ci_name')->first();
                        //GET VENDOR DETAIL
                        $branch = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['branch_id'])->select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name', 'insurance_amount', 'google_map_address', 'address_image','opening_time','closing_time','address_ar as address','longitude','latitude')->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:title_ar as title,image,category_id')->first();
                        //Get clinic category List



                        $Chkdoctor_list = Products::select('attribute_id')->where('pro_mc_id', '=', $input['branch_id'])->where('pro_status', '=', 1)->get();
                        $Doclist = array();
                        foreach ($Chkdoctor_list as $key => $value) {
                        $attr = $value->attribute_id;
                        array_push($Doclist, $attr);
                        }


 //$clinic_category = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'image', 'status')->whereIn('id',$Doclist)->where('services_id', '=', $input['subcategory_id'])->get();

                        $clinic_category = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'image', 'status')->where('services_id', '=', $input['subcategory_id'])->get();
                       
                        if (!$clinic_category->isEmpty()) {

                            foreach($clinic_category as $key => $value) {
                                //get doctor detail according clinic category.
                                $doctor_list = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','specialistion_ar as specialistion', 'profession_bio','opening_time','closing_time')->where('pro_mc_id', '=', $input['branch_id'])->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10000);
                                
                                $value->doctor_list = $doctor_list;

                                foreach($doctor_list as $val) {
                                    $name = explode(',', $val->profession_bio);

                                    $productid = $val->pro_id;
                                    /*$doctor_attr = ProductAttribute::select('id', 'product_id', 'vendor_id', 'attribute_title', 'value')->whereIn('id', $name)->get();*/

                                    $doctor_attr = ProductAttribute::select('id', 'product_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'value')->distinct('attribute_title_ar')->where('product_id', $productid)->get();

                                    $val->profession_bio = $doctor_attr;
                                }

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name, 'vendor_branch_detail' => $branch, 'client_comments' => $client_review, 'clinic_category' => $clinic_category);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'يبدو أنه لا يوجد طبيب متاح مع هذه الفئة.']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                        
                        //GET SUB CATEGORY
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name', 'mc_img')->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name', 'mc_img')->with('getCity:ci_id,ci_name')->first();
                        //GET VENDOR DETAIL
                        $branch = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['branch_id'])->select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name', 'insurance_amount', 'google_map_address', 'address_image','opening_time','closing_time','address','longitude','latitude')->with('getCity:ci_id,ci_name')->with('CategoryGallery:title,image,category_id')->first();
                        //Get clinic category List

                        $Chkdoctor_list = Products::select('attribute_id')->where('pro_mc_id', '=', $input['branch_id'])->where('pro_status', '=', 1)->get();
                        $Doclist = array();
                        foreach ($Chkdoctor_list as $key => $value) {
                        $attr = $value->attribute_id;
                        array_push($Doclist, $attr);
                        }
 
                       // $clinic_category = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->whereIn('id',$Doclist)->where('services_id', '=', $input['subcategory_id'])->get();

                      $clinic_category = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->where('services_id', '=', $input['subcategory_id'])->get();

                       
                        if (!$clinic_category->isEmpty()) {
                           
                            foreach($clinic_category as $key => $value) {
                                //get doctor detail according clinic category.
                                $doctor_list = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','specialistion', 'profession_bio','opening_time','closing_time')->where('pro_mc_id', '=', $input['branch_id'])->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10000);



                                $value->doctor_list = $doctor_list;

                                foreach($doctor_list as $val) {
                                    $name = explode(',', $val->profession_bio);

                                    $productid = $val->pro_id;
                                    /*$doctor_attr = ProductAttribute::select('id', 'product_id', 'vendor_id', 'attribute_title', 'value')->whereIn('id', $name)->get();*/

                                    $doctor_attr = ProductAttribute::select('id', 'product_id', 'vendor_id', 'attribute_title', 'value')->distinct('attribute_title')->where('product_id', $productid)->get();

                                    $val->profession_bio = $doctor_attr;
                                }

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name, 'vendor_branch_detail' => $branch, 'client_comments' => $client_review, 'clinic_category' => $clinic_category);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'It seems no Doctor are available with the clinic category.']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Get Doctor's Booking List
    public function getDoctorBookingList(Request $request) {

        $validator = Validator::make($request->all(), [
            'branch_id' => 'required', //Vendor ID
            'product_id' => 'required', //Product ID
            'attribute_id' => 'required', //Product ID

        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $booking_list = OrderServicesStaff::where('staff_id', '=', $input['attribute_id'])->where('service_id', '=', $input['product_id'])->where('booking_date', '>=', date("Y-m-d"))->get();
                if (!$booking_list->isEmpty()) {
                    $response_array = array('data' => ['success' => true, 'message' => 'Data found', 'code' => 200], 'booking_list' => $booking_list);
                } else {
                    if ($input['language_type'] == 'ar') {
                        /*
                        Arabic language
                        */
                        $response_array = array('data' => ['success' => false, 'message' => 'الحجز غير متوفر', 'code' => 500]);

                    } else {
                        /*
                        English language
                        */
                        $response_array = array('data' => ['success' => false, 'message' => 'Booking is not available', 'code' => 500]);
                    }

                }

                $response = Response::json($response_array);
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    // Set Data in add to cart
    public function setBookDoctorAppointment(Request $request) {

        $validator = Validator::make($request->all(), [
            'branch_id' => 'required', //Vendor ID
            'product_id' => 'required', //Product ID
            'attribute_id' => 'required', //Product ID
            'date' => 'required',
            'cart_type' => 'required',
            'cart_sub_type' => 'required',
            'total_price' => 'required',
            'language_type' => 'required|max:255',
            'time' => 'required'

        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => trans($locale.'_mob_lang.INPUT_ERROR')]);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $product = Products::select('pro_id','pro_title','pro_title_ar','pro_desc', 'pro_desc_ar','pro_Img', 'pro_price','service_hour','pro_mc_id')->where('pro_id','=',$input['product_id'])->first(); //GET Product Detail 
/*
                $vendor_shop = Category::select('mc_id', 'mc_name', 'opening_time', 'closing_time','vendor_id','parent_id')->where('mc_id','=',$input['branch_id'])->where('opening_time','<=',$input['time'])->where('closing_time','>=',$input['time'])->first(); //GET VENDOR Detail*/

                $vendor_shop = Category::select('mc_id', 'mc_name', 'opening_time', 'closing_time','vendor_id','parent_id')->where('mc_id','=',$input['branch_id'])->first();
                if (!empty($vendor_shop)) {

                   /* $file_no = DoctorPrescription::where('clinic_id', '=', $input['branch_id'])->where('doc_id', '=', $input['product_id'])->where('file_number', '=', $input['file_no'])->first();*/
                   // Himanshu kumar if (!empty($file_no)) {
                   $file_no = '';
                    if (empty($file_no)) {
                        $order_staff = OrderServicesStaff::where('shop_id', '=', $input['branch_id'])->where('service_id', '=', $input['product_id'])->where('staff_id', '=', $input['attribute_id'])->where('start_time', '=', $input['time'])->where('booking_date', '=', date("Y-m-d", strtotime($input['date'])))->first(); //Get Staff detail

                        $end_time = date('H:i', strtotime($input['time'].
                            '+'.$product->service_hour.
                            'hour')); //add time duration 

                        if (empty($order_staff)) {
                            $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                            if (!empty($cart)) {

                                $cart_pro = CartProduct::where('cart_type', '=', 'clinic')->where('cart_id', '=', $cart->id)->where('product_id', $input['product_id'])->delete();
                                $cart_option = CartServiceStaff::where('cart_id', '=', $cart->id)->where('service_id', $input['product_id'])->delete();

                            } else {
                                $cart_data = array();
                                $cart_data['user_id'] = $User->cus_id;
                                $cart = Cart::create($cart_data); //cart entery
                            }
                            if (!empty($cart)) {
                                $cart_product_data = array();
                                $cart_product_data['merchant_id'] = $vendor_shop->vendor_id;
                                $cart_product_data['shop_id'] = $vendor_shop->mc_id;
                                $cart_product_data['category_id'] = $vendor_shop->mc_id;
                                $cart_product_data['cart_type'] = $input['cart_type'];
                                $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                                $cart_product_data['cart_id'] = $cart->id;
                                $cart_product_data['product_id'] = $input['product_id'];
                                $cart_product_data['total_price'] = $input['total_price'];  
                               
                                $cart_product_data['shop_vendor_id'] = $vendor_shop->parent_id;
                                $cart_product_data['pro_title'] = $product->pro_title;
                                $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                                $cart_product_data['pro_desc'] = $product->pro_desc;
                                $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                                $cart_product_data['pro_Img'] = $product->pro_Img;
                                $cart_product_data['pro_price'] = $product->pro_price;
                                $cart_product_data['status'] = 1;
                                $cart_product = CartProduct::create($cart_product_data); //cart product entry  
                               
                                     $date =  date("Y-m-d", strtotime($input['date']));
                                     $time = $input['time'];
                                     $endtime = $end_time;

                             //$checdocount = CartServiceStaff::where('booking_date',$date)->where('start_time',$time)->where('end_time',$endtime)->where('cart_type','clinic')->first();
                            $checdocount = array();

                              if($checdocount) {
                                $response_array = array('data' => ['success' => true, 'message' => trans($locale.
                              '_mob_lang.APPOINTMENT_ALLREADY_BOOKED'), 'code' => 200]);
                                return $response_array;
                                
                               } else {


                                if (isset($input['attribute_id'])) {
                                    $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first(); 

                                    $cart_service_staff = array();
                                    $cart_service_staff['cart_id'] = $cart->id;
                                    $cart_service_staff['cart_type'] = $input['cart_type'];
                                    if ($input['file_no']) {
                                        $cart_service_staff['file_no'] = $input['file_no'];
                                    }
                                    $cart_service_staff['shop_id'] = $input['branch_id'];
                                    $cart_service_staff['service_id'] = $input['product_id'];
                                    $cart_service_staff['staff_id'] = $input['attribute_id'];
                                    $cart_service_staff['booking_date'] = date("Y-m-d", strtotime($input['date']));
                                    $cart_service_staff['start_time'] = $input['time'];
                                    $cart_service_staff['end_time'] = $end_time;
                                    $cart_service_staff['staff_member_name'] = $menu->attribute_title;
                                    $cart_service_staff['staff_member_name_ar'] = $menu->attribute_title_ar;
                                    CartServiceStaff::create($cart_service_staff); //Cart Service Entery entery

                                }
                              }
                                $response_array = array('data' => ['success' => true, 'message' => trans($locale.
                        '_mob_lang.APPOINTMENT_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                                $response = Response::json($response_array, 200);
                            } else {
                                $response_array = array('data' => ['success' => false, 'message' => trans($locale.
                        '_mob_lang.CART_ERROR'), 'code' => 200]);
                                $response = Response::json($response_array, 200);
                            }
                        } else {

                            $response_array = array('data' => ['success' => false, 'error_code' => 200, 'message' => trans($locale.
                        '_mob_lang.DOCTOR_BOOKING_ERROR')]);
                            $response = Response::json($response_array, 200);
                        }
                    }
                    elseif($input['file_no'] == '') {
                        $order_staff = OrderServicesStaff::where('shop_id', '=', $input['branch_id'])->where('service_id', '=', $input['product_id'])->where('staff_id', '=', $input['attribute_id'])->where('start_time', '=', $input['time'])->where('booking_date', '=', date("Y-m-d", strtotime($input['date'])))->first(); //Get Staff detail

                        $end_time = date('H:i', strtotime($input['time'].
                            '+'.$product->service_hour.
                            'hour')); //add time duration 

                        if (empty($order_staff)) {
                            $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                            if (!empty($cart)) {

                                $cart_pro = CartProduct::where('cart_type', '=', 'clinic')->where('cart_id', '=', $cart->id)->where('product_id', $input['product_id'])->delete();
                                $cart_option = CartServiceStaff::where('cart_id', '=', $cart->id)->where('service_id', $input['product_id'])->delete();

                            } else {
                                $cart_data = array();
                                $cart_data['user_id'] = $User->cus_id;
                                $cart = Cart::create($cart_data); //cart entery
                            }
                            if (!empty($cart)) {
                                $cart_product_data = array();
                                $cart_product_data['merchant_id'] = $vendor_shop->vendor_id;
                                $cart_product_data['shop_id'] = $vendor_shop->mc_id;
                                $cart_product_data['cart_type'] = $input['cart_type'];
                                $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                                $cart_product_data['cart_id'] = $cart->id;
                                $cart_product_data['product_id'] = $input['product_id'];
                                $cart_product_data['total_price'] = $input['total_price'];  
                                    $cart_product_data['category_id'] = $vendor_shop->mc_id;
                                $cart_product_data['shop_vendor_id'] = $vendor_shop->parent_id;
                                $cart_product_data['pro_title'] = $product->pro_title;
                                $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                                $cart_product_data['pro_desc'] = $product->pro_desc;
                                $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                                $cart_product_data['pro_Img'] = $product->pro_Img;
                                $cart_product_data['pro_price'] = $product->pro_price;
                                $cart_product_data['status'] = 1;
                                $cart_product = CartProduct::create($cart_product_data); //cart product entry  
                                if (isset($input['attribute_id'])) {

                                    $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first(); 

                                    $cart_service_staff = array();
                                    $cart_service_staff['cart_id'] = $cart->id;
                                    $cart_service_staff['cart_type'] = $input['cart_type'];
                                    $cart_service_staff['shop_id'] = $input['branch_id'];
                                    $cart_service_staff['service_id'] = $input['product_id'];
                                    $cart_service_staff['staff_id'] = $input['attribute_id'];
                                    $cart_service_staff['booking_date'] = date("Y-m-d", strtotime($input['date']));
                                    $cart_service_staff['start_time'] = $input['time'];
                                    $cart_service_staff['end_time'] = $end_time;
                                    $cart_service_staff['staff_member_name'] = $menu->attribute_title;
                                    $cart_service_staff['staff_member_name_ar'] = $menu->attribute_title_ar;
                                    CartServiceStaff::create($cart_service_staff); //Cart Service Entery entery

                                }

                                $response_array = array('data' => ['success' => true, 'message' => trans($locale.
                        '_mob_lang.APPOINTMENT_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                                $response = Response::json($response_array, 200);
                            } else {
                                $response_array = array('data' => ['success' => false, 'message' => trans($locale.
                        '_mob_lang.CART_ERROR'), 'code' => 500]);
                                $response = Response::json($response_array, 500);
                            }
                        } else {

                            $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => trans($locale.
                        '_mob_lang.DOCTOR_BOOKING_ERROR')]);
                            $response = Response::json($response_array, 200);
                        }
                    } else {

                        $response_array = array('data' => ['success' => false, 'error_code' => 200, 'message' =>  trans($locale.
                        '_mob_lang.File_ERROR')]);
                        $response = Response::json($response_array, 200);
                    }
                } else {

                    $response_array = array('data' => ['success' => false, 'error_code' => 200, 'message' => trans($locale.
                        '_mob_lang.CLINIC_ERROR')]);
                    $response = Response::json($response_array, 200);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => trans($locale.
                        '_mob_lang.PROFILE_UPDATE_ERROR')]);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    /**Get Clinic(Cosmetic and Skin) data from add to cart**/
    public function getClinicAddToCartData(Request $request) {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                $catg_data = array();
                $cart_pro = CartProduct::where('cart_type', '=', 'clinic')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,profession_bio')->first();
                //get Clinic's Doctor Data

                if (!empty($cart) && !empty($cart_pro)) {

                    // get Patient  data from add to cart  

                    $attribute = CartServiceStaff::where('cart_type', '=', 'clinic')->where('cart_id', '=', $cart->id)->where('service_id', '=', $input['product_id'])->with('getDoctorServiceAttribute:id,services_id,attribute_title,attribute_title_ar')->first();
                    $category = Category::select('mc_id','mc_img', 'mc_name', 'mc_name_ar','address','address_ar', 'mc_status','city_id')->where('mc_id', '=', $cart_pro->getProduct[0]->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();
                    

                    $catg_data['pro_id'] = $cart_pro->getProduct[0]->pro_id;
                    if ($input['language_type'] == 'en') {
                        if(!empty($category)){
                               $catg_data['category_name'] = $category->mc_name;
                               $catg_data['category_img'] = $category->mc_img;
                               $catg_data['address'] = $category->address; 
                               $catg_data['city_name'] = $category->getCity->ci_name; 
                            } 

                            $gertAttribute = DB::table('nm_product')->where('pro_id', '=', $input['product_id'])->first();
                            $Attributeid = $gertAttribute->attribute_id;
                            $gertAttributename = DB::table('nm_services_attribute')->where('id', '=', $Attributeid)->first();
                            $attribute_titl= $gertAttributename->attribute_title;
                            //$attribute_titl=  $gertAttributename->attribute_title_ar;




                        $catg_data['attribute_title'] = $attribute_titl;
                        $catg_data['pro_title'] = $cart_pro->getProduct[0]->pro_title;
                        $catg_data['pro_desc'] = $cart_pro->getProduct[0]->pro_desc;

                        $name = explode(',', $cart_pro->getProduct[0]->profession_bio);
                        $doctor_attr = ProductAttribute::select('id', 'attribute_title')->whereIn('id', $name)->get();
                        $catg_data['profession_bio'] = $doctor_attr;
                    } else {
                        if(!empty($category)){
                               $catg_data['category_name'] = $category->mc_name_ar;
                               $catg_data['category_img'] = $category->mc_img;
                               $catg_data['address'] = $category->address_ar; 
                               $catg_data['city_name'] = $category->getCity->ci_name_ar; 
                            } 
                        $gertAttribute = DB::table('nm_product')->where('pro_id', '=', $input['product_id'])->first();
                        $Attributeid = $gertAttribute->attribute_id;
                        $gertAttributename = DB::table('nm_services_attribute')->where('id', '=', $Attributeid)->first();
                        //$attribute_titl= $gertAttributename->attribute_title;
                        $attribute_titl=  $gertAttributename->attribute_title_ar;
                        $catg_data['attribute_title'] = $attribute_titl;
                        $catg_data['pro_title'] = $cart_pro->getProduct[0]->pro_title_ar;
                        $catg_data['pro_desc'] = $cart_pro->getProduct[0]->pro_desc_ar;
                        $name = explode(',', $cart_pro->getProduct[0]->profession_bio);
                        $doctor_attr = ProductAttribute::select('id', 'attribute_title_ar as attribute_title')->whereIn('id', $name)->get();
                        $catg_data['profession_bio'] = $doctor_attr;
                    }

                    $catg_data['pro_price'] = $cart_pro->getProduct[0]->pro_price;
                    $catg_data['pro_Img'] = $cart_pro->getProduct[0]->pro_Img;
                    $catg_data['pro_status'] = $cart_pro->getProduct[0]->pro_status;
                    $catg_data['file_no'] = $attribute->file_no;
                    $catg_data['booking_date'] = date("d-m-Y", strtotime($attribute->booking_date));
                    $catg_data['start_time'] = $attribute->start_time;
                    $catg_data['end_time'] = $attribute->end_time;
                    $catg_data['total_price'] = $cart_pro->total_price;
                    $catg_data['file_no'] = $attribute->file_no;

                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'cart_id' => $cart->id, 'doctor_data' => $catg_data);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'cart data not found & cart type must be clinic.', 'code' => 500]);
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    /**delete to add to cart  data**/
    public function deleteClinicData(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'token' => 'required'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                $cart_pro = CartProduct::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'clinic')->where('cart_id', '=', $cart->id)->first();
                if (!empty($cart) && !empty($cart_pro)) {

                    $cart_service_staff = CartServiceStaff::where('service_id', '=', $input['product_id'])->where('cart_type', '=', 'clinic')->where('cart_id', '=', $cart->id)->delete();
                    //delete service staff

                    $cart_pro = CartProduct::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'clinic')->where('cart_id', '=', $cart->id)->delete();
                    //delete product

                    $response_array = array('data' => ['success' => true, 'message' => 'Successfully deleted your cart data', 'code' => 200]);

                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

}
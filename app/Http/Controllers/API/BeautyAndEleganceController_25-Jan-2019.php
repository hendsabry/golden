<?php
namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Http\Models;
use App\Category;
use App\Products;
use App\ProductOption;
use App\ProductOptionValue;
use App\ProductPackage;
use App\City;
use App\Gallery;
use App\CategoryGallery;
use App\ServiceAttribute;
use App\ServicesStaff;
use App\StaffExperties;
use App\OrderServicesStaff;
use App\Cart;
use App\CartProduct;
use App\CartServiceStaff;
use App\CartServiceAttribute;
use App\CartProductPackage;
use Auth;
use App\User;
use Response;
use DB;
use App\NmServicesAttribute;


class BeautyAndEleganceController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |   Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function __construct() {}

    /**Get Men's Saloon and Makeup info.**/

    /*
       vendor_id(3th lavel category in category table) become pro_mc_id in product table to get full Men's Saloon info.
    */
    public function getSaloonInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id' => 'required', //Vendor ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first();

                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($input['vendor_id']);
                $gallery = CategoryGallery::select('image')->where('category_id', '=', $input['vendor_id'])->get(); //GET Category GALLERY

                if (!empty($category)) {

                    if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                        //GET SUB CATEGORY
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name', 'mc_img', 'budget_in_percentages')->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','home_visit_charge','opening_time','closing_time','vendor_id','terms_conditions_ar as terms_conditions ','terms_condition_name_ar as terms_condition_name','address_ar as address','service_availability','longitude','latitude')->with('getCity:ci_id,ci_name_ar as ci_name')->first();
                        //Get Saloon Menu List
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'image', 'status')->where('status', '=', '1')->where('services_id', '=', $input['vendor_id'])->orderBy('attribute_title_ar', 'ASC')->get();

                         


                        if (isset($menu)) {

                            foreach($menu as $key => $value) {
                                //get saloon detail according menu

                                $style = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar  as pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id','service_hour','packege','pro_disprice','pro_discount_percentage','pro_qty')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10000);
                                $value->style_list = $style;
                                foreach($style as $val) {
                                   if($val->packege == 'yes'){
                                    $packege = ProductPackage::select('id', 'shop_id', 'attribute_id', 'item_id', 'packege_id')->where('shop_id','=',$val->pro_mc_id)->where('item_id','=',$val->pro_id)->where('attribute_id','=',$val->attribute_id)->with('getProductPackage:pro_id,pro_title_ar as pro_title,pro_Img,pro_desc_ar as pro_desc,pro_price,attribute_id')->get();
                                   $val->product_packege =  $packege;
                                 }

                                }

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'م العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_detail' => $vendor, 'category_gallery' => $gallery, 'client_comments' => $client_review, 'saloon_menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                        //Get menu detail
                        //GET SUB CATEGORY
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name', 'mc_img')->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','home_visit_charge','vendor_id','terms_conditions','terms_condition_name','opening_time','closing_time','address','service_availability','longitude','latitude')->with('getCity:ci_id,ci_name')->first();
                        //Get Saloon Menu List
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->where('status', '=', '1')->where('services_id', '=', $input['vendor_id'])->orderBy('attribute_title', 'ASC')->get();
                        if (isset($menu)) {

                            foreach($menu as $key => $value) {
                                //get style detail according menu
                                $style = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','packege','pro_disprice','pro_discount_percentage','pro_qty')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(1000);
                                $value->style_list = $style;
                                foreach($style as $val) {
                                    if($val->packege == 'yes'){
                                    $packege = ProductPackage::select('id', 'shop_id', 'attribute_id', 'item_id', 'packege_id')->where('shop_id','=',$val->pro_mc_id)->where('item_id','=',$val->pro_id)->where('attribute_id','=',$val->attribute_id)->with('getProductPackage:pro_id,pro_title,pro_Img,pro_desc,pro_price,attribute_id')->get();
                                   $val->product_packege =  $packege;
                                 }

                                }

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_detail' => $vendor, 'category_gallery' => $gallery, 'client_comments' => $client_review, 'saloon_menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    /**Get Beauty Centers and Spa info.**/

    /*
       branch_id(4th lavel category in category table) become pro_mc_id in product table to get full Beauty Centers and Spa info.
    */
    public function getBeautyAndSpaInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id' => 'required', //Vendor ID
            'branch_id' => 'required', //Get Branch ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first();

                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($input['branch_id']);
                $gallery = CategoryGallery::select('image')->where('category_id', '=', $input['branch_id'])->get(); //GET Category GALLERY

                if (!empty($category)) {

                    if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {





  $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->first();
                        //GET vendor detail
                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->first();
                        //GET branch DETAIL
                        $branch = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['branch_id'])->select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img','budget_in_percentages', 'insurance_amount', 'google_map_address', 'address_image','home_visit_charge','vendor_id','terms_conditions','terms_condition_name','address_ar as address','opening_time','closing_time','service_availability','longitude','latitude')->with('getCity:ci_id,ci_name_ar as ci_name')->first();
                        //Get Saloon Menu List
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'image', 'status')->where('status', '=', '1')->where('services_id', '=', $input['branch_id'])->Orwhere('id', '=', 299)->orderBy('attribute_title', 'ASC')->get();
 

 
                   



                        if (isset($menu)) {

                            foreach($menu as $key => $value) {


                            if($value->id =='299'){
                            //get saloon detail according menu
                            $style = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar  as pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','packege','pro_disprice','pro_discount_percentage')->where('pro_mc_id', '=',  $input['branch_id'])->where('packege', '=', 'yes')->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10000);
                            $value->style_list = $style;
                            foreach($style as $val) {
                            if($val->packege == 'yes'){
                            $packege = ProductPackage::select('id', 'shop_id', 'attribute_id', 'item_id', 'packege_id')->where('shop_id','=',$val->pro_mc_id)->where('packege_id','=',$val->pro_id)->with('getProductPackage:pro_id,pro_title_ar as pro_title,pro_Img,pro_desc_ar as pro_desc,pro_price,attribute_id')->get();
                            $val->product_packege =  $packege;
                            }

                            }
                            }
                            else
                            {

                            //get saloon detail according menu
                            $style = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar  as pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','packege','pro_disprice','pro_discount_percentage')->where('pro_mc_id', '=', $value->services_id)->where('pro_status', '=', 1)->where('attribute_id', '=', $value->id)->where('packege', '=', 'no')->orderBy('pro_title', 'ASC')->paginate(10000);
                            $value->style_list = $style;
                            foreach($style as $val) {
                            if($val->packege == 'yes'){
                            $packege = ProductPackage::select('id', 'shop_id', 'attribute_id', 'item_id', 'packege_id')->where('shop_id','=',$val->pro_mc_id)->where('packege_id','=',$val->pro_id)->with('getProductPackage:pro_id,pro_title_ar as pro_title,pro_Img,pro_desc_ar as pro_desc,pro_price,attribute_id')->get();
                            $val->product_packege =  $packege;
                            }

                            } 
                            }

 

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'م العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor' => $vendor, 'branch_detail' => $branch, 'category_gallery' => $gallery, 'client_comments' => $client_review, 'saloon_menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                        //Get menu detail
                        //GET SUB CATEGORY
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name', 'mc_img')->first();
                        //GET vendor detail
                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name', 'mc_img')->first();
                        //GET branch DETAIL
                        $branch = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['branch_id'])->select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img','budget_in_percentages', 'insurance_amount', 'google_map_address', 'address_image','home_visit_charge','vendor_id','terms_conditions','terms_condition_name','address','opening_time','closing_time','service_availability','longitude','latitude')->with('getCity:ci_id,ci_name')->first();
                        //Get Saloon Menu List
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->where('status', '=', '1')->where('services_id', '=', $input['branch_id'])->Orwhere('id', '=', 299)->orderBy('attribute_title', 'ASC')->get();
                        if (isset($menu)) {

                            foreach($menu as $key => $value) {


                            if($value->id =='299'){
                            //get style detail according menu
                            $style = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','packege','pro_disprice','pro_discount_percentage')->where('pro_mc_id', '=', $input['branch_id'])->where('pro_status', '=', 1)->where('packege', '=', 'yes')->orderBy('pro_title', 'ASC')->paginate(10000);
                            $value->style_list = $style;
                            foreach($style as $val) {
                            if($val->packege == 'yes'){ 

                            $packege = ProductPackage::select('id', 'shop_id', 'attribute_id', 'item_id', 'packege_id')->where('shop_id','=',$val->pro_mc_id)->where('packege_id','=',$val->pro_id)->with('getProductPackage:pro_id,pro_title,pro_Img,pro_desc,pro_price,attribute_id')->get();
                            $val->product_packege =  $packege;
                            }

                            }  

                            }
                            else
                            {
                            //get style detail according menu 
                            $style = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','packege','pro_disprice','pro_discount_percentage')->where('packege', '!=', 'yes')->where('attribute_id', '=', $value->id)->where('pro_mc_id', '=', $value->services_id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10000);
                            $value->style_list = $style;
                            foreach($style as $val) {
                            if($val->packege == 'yes'){ 

                            $packege = ProductPackage::select('id', 'shop_id', 'attribute_id', 'item_id', 'packege_id')->where('shop_id','=',$val->pro_mc_id)->where('packege_id','=',$val->pro_id)->with('getProductPackage:pro_id,pro_title,pro_Img,pro_desc,pro_price,attribute_id')->get();
                            $val->product_packege =  $packege;
                            }

                            }  
                            } 

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor' => $vendor, 'branch_detail' => $branch, 'category_gallery' => $gallery, 'client_comments' => $client_review, 'saloon_menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }
    /**Get Makeup Artist Info.**/

    /*
       vendor_id(3th lavel category in category table) become pro_mc_id in product table to get Makeup Artist Info.
    */
    public function getMakeupArtistInfo(Request $request) {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id' => 'required', //Vendor ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first();

                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($input['vendor_id']);
                $gallery = CategoryGallery::select('image', 'category_id')->where('category_id', '=', $input['vendor_id'])->get(); //GET Category GALLERY
                // dd($gallery);

                if (!empty($category)) {

                    if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                        //GET SUB CATEGORY
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name_ar', 'mc_img', 'budget_in_percentages')->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','home_visit_charge','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name','opening_time','closing_time','address_ar as address','service_availability','longitude','latitude')->with('getCity:ci_id,ci_name_ar as ci_name')->first();
                        //Get Saloon Menu List
                        //Get Makeup Artist product List
                        $mer_id = $vendor->vendor_id;
                        $sid = $input['vendor_id'];
                        $menulist =  NmServicesAttribute::where('vendor_id',$mer_id)->where('services_id',$sid )->where('status', 1)->get();
                        $catarry = array();
                        foreach($menulist as $menu)
                        {
                            $IDs = $menu->id;
                            array_push($catarry, $IDs);
                     $makeupproduct= Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_desc_ar as pro_desc', 'pro_Img', 'video_url', 'pro_price','pro_disprice','pro_discount_percentage', 'service_hour', 'pro_mc_id', 'option_id','video_url')->where('attribute_id', $menu->id)->where('pro_status', '=', 1)->where('pro_mc_id', '=', $input['vendor_id'])->with('ProductGallery:image,product_id')->with('getPoductOption:id,services_id,option_title_ar as option_title ,service_hour,button_type_id')->get();
                    $menu->products =  $makeupproduct;
                        }


                        $makeup_product = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_desc_ar as pro_desc', 'pro_Img', 'video_url', 'pro_price','pro_disprice','pro_discount_percentage', 'service_hour', 'pro_mc_id', 'option_id','video_url')->whereIn('attribute_id', $catarry)->where('pro_status', '=', 1)->where('pro_mc_id', '=', $input['vendor_id'])->with('ProductGallery:image,product_id')->with('getPoductOption:id,services_id,option_title_ar as option_title ,service_hour,button_type_id')->orderBy('pro_title', 'ASC')->paginate(10000);

                        if (isset($makeup_product)) {
                            /*foreach($makeup_product as $value) {

                                $product_option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title_ar as option_title', 'value', 'image', 'price', 'experience')->where('product_option_id', '=', $value->option_id)->get();

                                $value->getPoductOption->product_option_value = $product_option_value; //GET PRODUCT OPTION VALUE

                            }*/

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'م العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'vendor_detail' => $vendor, 'category_gallery' => $gallery, 'client_comments' => $client_review, 'makeup_product' => $makeup_product, 'menulist' => $menulist);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                        //Get menu detail
                        //GET SUB CATEGORY
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name', 'mc_img')->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','home_visit_charge','vendor_id','terms_conditions','terms_condition_name','opening_time','closing_time','address','service_availability','longitude','latitude')->with('getCity:ci_id,ci_name')->first();
                        //Get Makeup Artist product List
                        $mer_id = $vendor->vendor_id;
                        $sid = $input['vendor_id'];
                        

                        $menulist =  NmServicesAttribute::where('vendor_id',$mer_id)->where('services_id',$sid )->where('status', 1)->get();
                        $catarry = array();
                        foreach($menulist as $menu)
                        {
                            $IDs = $menu->id;
                            array_push($catarry, $IDs);
                             $makeupproduct= Products::select('pro_id', 'pro_title', 'pro_desc', 'pro_Img', 'video_url', 'pro_price', 'service_hour', 'pro_mc_id', 'option_id','video_url','pro_disprice','pro_discount_percentage')->where('attribute_id', $IDs)->where('pro_mc_id', '=', $input['vendor_id'])->where('pro_status', '=', 1)->with('ProductGallery:image,product_id')->with('getPoductOption:id,services_id,option_title,service_hour,button_type_id')->get();
                        $menu->products =  $makeupproduct;
                        }

                        $makeup_product = Products::select('pro_id', 'pro_title', 'pro_desc', 'pro_Img', 'video_url', 'pro_price', 'service_hour', 'pro_mc_id', 'option_id','video_url','pro_disprice','pro_discount_percentage')->whereIn('attribute_id', $catarry)->where('pro_mc_id', '=', $input['vendor_id'])->where('pro_status', '=', 1)->with('ProductGallery:image,product_id')->with('getPoductOption:id,services_id,option_title,service_hour,button_type_id')->orderBy('pro_title', 'ASC')->paginate(10000);

                        if (isset($makeup_product)) {
                            /*foreach($makeup_product as $value) {
                                $product_option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title', 'value', 'image', 'price', 'experience')->where('product_option_id', '=', $value->option_id)->get();

                                $value->getPoductOption->product_option_value = $product_option_value; //GET PRODUCT OPTION VALUE

                            }*/  

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_detail' => $vendor, 'category_gallery' => $gallery, 'client_comments' => $client_review, 'makeup_product' => $makeup_product, 'menulist' => $menulist);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }
    // get available worker list
    public function getWorkerList(Request $request) {

        $validator = Validator::make($request->all(), [
            'vendor_id' => 'required', //Vendor ID
            'product_id' => 'required', //Product ID
            'language_type' => 'required|max:255',
            'token' => 'required',
            'date' => 'required',
            'time' => 'required'

        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {
                 
               //total staf
               
           /*  $total_staff = ServicesStaff::select('id', 'shop_id', 'status')->where('service_id', $input['product_id'])->get();  */
               



                $User = Auth::user();
                $order_staff = OrderServicesStaff::select('staff_id', 'shop_id')->where('shop_id', '=', $input['vendor_id'])->where('start_time', '=', $input['time'])->where('booking_date', '=', date("Y-m-d", strtotime($input['date'])))->get()->toArray();


                $staff_ids = array();
                $service_ids = array();
                $shop_ids = array();
                foreach($order_staff as $key => $value) {
                    $staff_ids[] = $value['staff_id'];
                    $shop_ids[] = $value['shop_id'];
                }
                 $bookedworker = array();
                if ($input['language_type'] == 'ar') {
                    /*
                    Arabic language
                    */
                    //GET Service Staff detail
                    
                        
                         if (!empty($order_staff)) {
                        $bookedworker = ServicesStaff::select('nm_services_staff.id', 'nm_services_staff.shop_id','nm_services_staff.status','nm_services_staff.staff_member_name','nm_services_staff.staff_member_name_ar','nm_services_staff.image','nm_order_services_staff.start_time','nm_order_services_staff.end_time','nm_services_staff.experience','nm_order_services_staff.product_type')
                                ->join('nm_order_services_staff', 'nm_order_services_staff.staff_id', '=', 'nm_services_staff.id')
                                ->whereIn('nm_order_services_staff.staff_id', $staff_ids)
                                ->whereIn('nm_order_services_staff.shop_id', $shop_ids)
                                ->where('nm_order_services_staff.product_type','beauty')
                                ->get();

                             


                        
                        if (!$bookedworker->isEmpty()) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'م العثور على البيانات'], 'bookedworker' => $bookedworker);
                        } else {
                            $response_array = array('data' => ['success' => false, 'message' => ' يتم العثور على بيانات', 'code' => 500]);
                        }
                       }
                    


                    if (!empty($order_staff)) {
                        $services_staff = ServicesStaff::select('id', 'shop_id', 'status')->whereNotIn('id', $staff_ids)->whereIn('shop_id', $shop_ids)->get();
                    $staff_expert = StaffExperties::select('id', 'staff_id', 'product_id')->whereNotIn('staff_id', $staff_ids)->where('product_id', '=', $input['product_id'])->with('getStaff:id,shop_id,staff_member_name_ar as staff_member_name,staff_member_address_ar as staff_member_address,image,experience,status')->get();
                        
                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'م العثور على البيانات'], 'services_staff' => $staff_expert, 'bookedworker' => $bookedworker);

                    } else {

                       $services_staff = ServicesStaff::select('id', 'shop_id', 'status')->where('status', '=', 1)->where('shop_id', '=', $input['vendor_id'])->get();

                        $staff_expert = StaffExperties::select('id', 'staff_id', 'product_id')->where('product_id', '=', $input['product_id'])->with('getStaff:id,shop_id,staff_member_name_ar as staff_member_name,staff_member_address_ar as staff_member_address,image,experience,status')->get();

                        if (!$staff_expert->isEmpty()) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'م العثور على البيانات'], 'services_staff' => $staff_expert,'bookedworker' => $bookedworker);
                        } else {
                            $response_array = array('data' => ['success' => false, 'message' => ' يتم العثور على بيانات', 'code' => 500]);
                        }
                         $response_array =  array_push($response_array,$bookedworker);

                    }

                } else {
                    /*
                    English language
                    */
                    //GET Service Staff detail

                     if (!empty($order_staff)) {
                        $bookedworker = ServicesStaff::select('nm_services_staff.id', 'nm_services_staff.shop_id','nm_services_staff.status','nm_services_staff.staff_member_name','nm_services_staff.staff_member_name_ar','nm_services_staff.image','nm_order_services_staff.start_time','nm_order_services_staff.end_time','nm_services_staff.experience','nm_order_services_staff.product_type')
                                ->join('nm_order_services_staff', 'nm_order_services_staff.staff_id', '=', 'nm_services_staff.id')
                                ->whereIn('nm_order_services_staff.staff_id', $staff_ids)
                                ->whereIn('nm_order_services_staff.shop_id', $shop_ids)
                                ->where('nm_order_services_staff.product_type','beauty')
                                ->get();




                        
                        if (!$bookedworker->isEmpty()) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'م العثور على البيانات'], 'bookedworker' => $bookedworker);
                        } else {
                            $response_array = array('data' => ['success' => false, 'message' => ' يتم العثور على بيانات', 'code' => 500]);
                        }
                       }


                    if (!empty($order_staff)) {

                        $services_staff = ServicesStaff::select('id', 'shop_id', 'status')->whereNotIn('id', $staff_ids)->whereIn('shop_id', $shop_ids)->get();

                        $staff_expert = StaffExperties::select('id', 'staff_id', 'product_id')->whereNotIn('staff_id', $staff_ids)->where('product_id', '=', $input['product_id'])->with('getStaff:id,shop_id,staff_member_name,staff_member_address,image,experience,status')->get();
                       
                        
                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'م العثور على البيانات'], 'services_staff' => $staff_expert, 'bookedworker' => $bookedworker);



                    } else {

                        //GET Service Staff detail
                        $services_staff = ServicesStaff::select('id', 'shop_id', 'status')->where('status', '=', 1)->where('shop_id', '=', $input['vendor_id'])->get();
                           $staff_expert = StaffExperties::select('id', 'staff_id', 'product_id')->where('product_id', '=', $input['product_id'])->with('getStaff:id,shop_id,staff_member_name,staff_member_address,image,experience,status')->get();

                        if (!$staff_expert->isEmpty()) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'services_staff' => $staff_expert,'bookedworker' =>$bookedworker);
                        } else {
                            $response_array = array('data' => ['success' => false, 'message' => 'Data not found', 'code' => 500]);
                        }
                         


                         $response_array =  $response_array;
                        

                    }

                }

                $response = Response::json($response_array);
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }


    //Staff  worker list
     // get available worker list
    public function staffworkerlist(Request $request) {

      
        $validator = Validator::make($request->all(), [
            'vendor_id' => 'required', //Vendor ID
            'product_id' => 'required', //Product ID
            'language_type' => 'required|max:255',
            'token' => 'required',
            'date' => 'required',
            'time' => 'required'

        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {
                 
           
              
                
                if ($input['language_type'] == 'ar') {
                    /*
                    Arabic language
                    */
                    //GET Service Staff detail
                     $servicestaffexpert = DB::table('nm_staff_experties')->where('product_id', '=', $input['product_id'])->select('id', 'staff_id')->get(); 


             

             foreach ($servicestaffexpert as $staffreviewexp) {
                         $servicestaff = DB::table('nm_services_staff')->where('status', '=', 1)->where('id', '=', $staffreviewexp->staff_id)->select('id', 'staff_member_name_ar as staff_member_name','experience','image')->get();
                             
                           $data[] =  $staffreviewexp->staffinformation=$servicestaff;

                    $dtimetimeslot =[];

                   foreach ($servicestaff as $staffreviewA) {                              
                    $staffid = $staffreviewA->id;
                    $service_duration=1;
                    $f_date = date('Y-m-d', strtotime( $request->date ));
                    $bookingdate=$f_date;
                    $bookingtime=$request->time;
                    $isstaffavailable=array();
                    $bookingappliedtime= strtotime($bookingtime);                
                    $bookingappliedendtime = strtotime($bookingtime) + 60*60*$service_duration;               
                    $end_time = date('h:i', $bookingappliedendtime);
                    $timeslots=array('bstart'=>$bookingappliedtime,'bend'=>$bookingappliedendtime,'bookingtime'=>$bookingtime,'endtime'=>$end_time);
                    $bookedstaff = DB::table('nm_order_services_staff')->where('booking_date', '=', $bookingdate)->where('staff_id', '=', $staffid)->select('id', 'start_time','end_time')->orderBy('id')->get();
                    $k=1;
                    foreach ($bookedstaff as $bookingtimeslot) {
                    $dbstarttime=strtotime($bookingtimeslot->start_time);
                    $dbendtime=strtotime($bookingtimeslot->end_time);
                    if(($bookingappliedtime >= $dbstarttime) && ($bookingappliedtime < $dbendtime)){
                    $isstaffavailable[]=$k;
                    }
                    if(($bookingappliedendtime >= $dbstarttime) && ($bookingappliedendtime < $dbendtime)){
                    $isstaffavailable[]=$k;
                    }
                    if(($bookingappliedtime <= $dbstarttime) && ($bookingappliedendtime > $dbendtime)){
                    $isstaffavailable[]=$k;
                    }

                    $U_date = date('Y-m-d', strtotime( $request->date ));
                    $C_date = date("Y-m-d",$dbstarttime);

                    if($U_date == $C_date)
                    {
                    $dtimetimeslot[]=array('dbstart'=>date("H:i:s A",$dbstarttime),'dbend'=>date("H:i:s A",$dbendtime));
                    }




                    $k++;
                    }



                   $staffreviewexp->staffbooked=$dtimetimeslot;

                    }
                             foreach ($servicestaff as $staffreview) {                                
                     $servicestaffreview = DB::table('nm_review')->where('status', '=', 1)->where('worker_id', '=', $staffreview->id)->where('review_type', '=','worker')->select('comment_id', 'customer_id','comments','ratings')->get(); 
                    
                     if(count($servicestaffreview)>0){
                         $staffreviewexp->staffreviews=$servicestaffreview;
                        }else{

                            $staffreviewexp->staffreviews=[];
                        }

                     $servicestaffratings = DB::table('nm_review')->where('review_type', '=','worker')->where('status', '=', 1)->where('worker_id', '=', $staffreview->id)->avg('ratings'); 

                      if($servicestaffratings>0){
                          $staffreviewexp->staffratingsavg=$servicestaffratings;
                        }else{

                             $staffreviewexp->staffratingsavg=5;
                        }


 if($servicestaffratings>0){

                          $staffratingcount = DB::table('nm_review')->select('ratings')->where('review_type', '=','worker')->where('status', '=', 1)->where('worker_id', '=', $staffreview->id)->count(); 


                          $staffreviewexp->ratingcount=$staffratingcount;
                        }else{

                             $staffreviewexp->ratingcount=0;
                        } 






                        
                }
                }
                 
                 $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'م العثور على البيانات'], 'stafflist' => $servicestaffexpert);
                

                } else {
                    /*
                    English language
                    */
                    //GET Service Staff detail

                      $servicestaffexpert = DB::table('nm_staff_experties')->where('product_id', '=', $input['product_id'])->select('id', 'staff_id')->get(); 
 
              
             foreach ($servicestaffexpert as $staffreviewexp) {
                            $servicestaff = DB::table('nm_services_staff')->where('status', '=', 1)->where('id', '=', $staffreviewexp->staff_id)->select('id', 'staff_member_name','experience','image')->get();
                             
                           $data[] =  $staffreviewexp->staffinformation=$servicestaff;


                  $dtimetimeslot =[];

                   foreach ($servicestaff as $staffreviewA) {                              
                    $staffid = $staffreviewA->id;
                    $service_duration=1;
                    $f_date = date('Y-m-d', strtotime( $request->date ));
                    $bookingdate=$f_date;
                    $bookingtime=$request->time;
                    $isstaffavailable=array();
                    $bookingappliedtime= strtotime($bookingtime);                
                    $bookingappliedendtime = strtotime($bookingtime) + 60*60*$service_duration;               
                    $end_time = date('h:i A', $bookingappliedendtime);
                    $timeslots=array('bstart'=>$bookingappliedtime,'bend'=>$bookingappliedendtime,'bookingtime'=>$bookingtime,'endtime'=>$end_time);
                    $bookedstaff = DB::table('nm_order_services_staff')->where('booking_date', '=', $bookingdate)->where('staff_id', '=', $staffid)->select('id', 'start_time','end_time')->orderBy('id')->get();
                    $k=1;
                   // $staffreviewexp->staffbookeds = $bookedstaff;
                    foreach ($bookedstaff as $bookingtimeslot) {
                    $dbstarttime=strtotime($bookingtimeslot->start_time);
                    $dbendtime=strtotime($bookingtimeslot->end_time);
                    if(($bookingappliedtime >= $dbstarttime) && ($bookingappliedtime < $dbendtime)){
                    $isstaffavailable[]=$k;
                    }
                    if(($bookingappliedendtime >= $dbstarttime) && ($bookingappliedendtime < $dbendtime)){
                    $isstaffavailable[]=$k;
                    }
                    if(($bookingappliedtime <= $dbstarttime) && ($bookingappliedendtime > $dbendtime)){
                    $isstaffavailable[]=$k;
                    }
                   
 
                   
                    $dtimetimeslot[]=array('dbstart'=>date("H:i:s A",$dbstarttime),'dbend'=>date("H:i:s A",$dbendtime));
                   
                    
                    $k++;
                    }
                   $staffreviewexp->staffbooked=$dtimetimeslot;

                    }


 
                     foreach ($servicestaff as $staffreview) { 

                     $servicestaffreview = DB::table('nm_review')->where('status', '=', 1)->where('worker_id', '=', $staffreview->id)->where('review_type', '=','worker')->select('comment_id', 'customer_id','comments','ratings')->get(); 
                    
                     if(count($servicestaffreview)>0){
                       
                         $staffreviewexp->staffreviews=$servicestaffreview;
                        }else{

                            $staffreviewexp->staffreviews=[];
                        }

                     $servicestaffratings = DB::table('nm_review')->where('status', '=', 1)->where('worker_id', '=', $staffreview->id)->where('review_type', '=','worker')->avg('ratings'); 

                      if($servicestaffratings>0){
                          $staffreviewexp->staffratingsavg=$servicestaffratings;
                        }else{

                             $staffreviewexp->staffratingsavg=5;
                        }


                        if($servicestaffratings>0){

                          $staffratingcount = DB::table('nm_review')->select('ratings')->where('review_type', '=','worker')->where('status', '=', 1)->where('worker_id', '=', $staffreview->id)->count(); 


                          $staffreviewexp->ratingcount=$staffratingcount;
                        }else{

                             $staffreviewexp->ratingcount=0;
                        } 
                }
                }
                 
                 $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'stafflist' => $servicestaffexpert);


                   
                }



                $response = Response::json($response_array);
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }
 


     //Staff Availability

      function staffavailability(Request $request){

                $staffid = $request->staffid;

                $service_duration=$request->service_duration;
                $bookingdate=$request->bookingdate;
                $bookingtime=$request->bookingtime;
                $serviceid=$request->service_id;
                $isstaffavailable=array();
                $bookingappliedtime= strtotime($bookingtime);
               
               $bookingappliedendtime = strtotime($bookingtime) + 60*60*$service_duration;

              // $end_time = date('h:i A', strtotime($bookingtime.
                    //'+'.$service_duration.
                    //'hour'));
               $end_time = date('h:i A', $bookingappliedendtime);

               $dtimetimeslot = array();
              // $bookingappliedendtime= strtotime($end_time);
               $timeslots=array('bstart'=>$bookingappliedtime,'bend'=>$bookingappliedendtime,'bookingtime'=>$bookingtime,'endtime'=>$end_time);

                          $bookedstaff = DB::table('nm_order_services_staff')->where('booking_date', '=', $bookingdate)->where('staff_id', '=', $staffid)->where('service_id','=',$serviceid)->select('id', 'start_time','end_time')->orderBy('id')->get();



                $k=1;
                foreach ($bookedstaff as $bookingtimeslot) {
                  $dbstarttime=strtotime($bookingtimeslot->start_time);
                
                //  echo $dbstarttime;die;
                  $dbendtime=strtotime($bookingtimeslot->end_time);
                 

                 $opentime=$bookingtimeslot->start_time;

                 $closetime=$bookingtimeslot->end_time;


                 // $bookedtime = array('starttime'=>$dbstarttime,'endtime'=>$dbendtime);
                  if(($bookingappliedtime >= $dbstarttime) && ($bookingappliedtime <= $dbendtime)){
                    $isstaffavailable=$k;
                  }
                   if(($bookingappliedendtime >= $dbstarttime) && ($bookingappliedendtime <= $dbendtime)){
                    $isstaffavailable=$k;
                  }
                   if(($bookingappliedtime <= $dbstarttime) && ($bookingappliedendtime > $dbendtime)){
                    $isstaffavailable=$k;
                  }
                  if(($bookingappliedendtime >= $dbstarttime) && ($bookingappliedendtime <= $dbendtime)){
                    $isstaffavailable=$k;
                  }
                  $dtimetimeslot[]=array('dbstart'=>$opentime,'dbend'=>$closetime);
                  $k++;
                } 
                 if($isstaffavailable) {
                   
                $isbookingopen=array('bookingopen'=>$isstaffavailable,'dtimeslot'=>$dtimetimeslot);
                return response()->json($isbookingopen);

            } else {
                 $isstaffavailable =  0;
                  ;
                 $isbookingopen=array('bookingopen'=>$isstaffavailable);
                return response()->json($isbookingopen);

            }
    }

     //GetuserDetails
       public function getUserReviewDetails(Request $request) {
        $validator = Validator::make($request->all(), [
            'worker_id' => 'required', //Product ID
            'language_type' => 'required|max:255',
            'token' => 'required',
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {
                $User = Auth::user();

                if ($input['language_type'] == 'ar') {
                    
                    /*
                    Arabic language
                    */
                    //GET Service Staff detail
                 /*    $servicestaffexpert = DB::table('nm_staff_experties')->where('product_id', '=', $input['product_id'])->select('id', 'staff_id')->get(); */
                 $usersdetails = DB::table('nm_customer')
                    ->join('nm_review', 'nm_customer.cus_id', '=', 'nm_review.customer_id')
                   // ->where('nm_review.customer_id',$User->cus_id)
                    ->where('nm_review.worker_id',$input['worker_id'])->where('nm_review.review_type','worker')
                    ->select('nm_customer.cus_name','nm_customer.cus_name','nm_customer.cus_pic','nm_review.ratings', 'nm_review.comments','nm_review.created_at')
                    ->orderBy('nm_review.created_at','DESC')
                    ->get();
                    if(!empty($usersdetails)) {
                 
                  $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'م العثور على البيانات'], 'userdetails' => $usersdetails);
              }
                

                } else {

                    /*
                    English language
                    */
                    //GET Service Staff detail
                   $usersdetails='';
                   $response_array = '';
                    $User = Auth::user();
                 $usersdetails = DB::table('nm_customer')
                    ->join('nm_review', 'nm_customer.cus_id', '=', 'nm_review.customer_id')
                   // ->where('nm_review.customer_id',$User->cus_id)
                    ->where('nm_review.worker_id',$input['worker_id'])->where('nm_review.review_type','worker') 
                    ->select('nm_customer.cus_name','nm_customer.cus_name','nm_customer.cus_pic','nm_review.ratings', 'nm_review.comments','nm_review.created_at')
                    ->orderBy('nm_review.created_at','DESC')
                    ->get();
                    if(!empty($usersdetails)) {

                 $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Find the data'], 'userdetails' => $usersdetails);
                  $response = Response::json($response_array);
                  return $response;
              } else {
                   $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'Review Not Found']);
                $response = Response::json($response_array);

              }

                }

                $response = Response::json($response_array);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'Review Not Found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }
     

    //Get End Review
     
       public function workerDuration(Request $request) {
        $validator = Validator::make($request->all(), [
            'opening_time' => 'required', //Vendor ID
            'closing_time' => 'required', //Product ID
            'duration' => 'required', //Product ID
            'token' => 'required',

        ]);

       

       

         $input = $request->all();
         
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

                        $date = $input['closing_time'];
                        $duration = $input['duration'];
                        $newdate = strtotime ( "-$duration hour" , strtotime ($date) ) ;
                        //$newdate = time ( 'Y-m-j' , $newdate );
                        $hour = date ( 'H' , $newdate );
                        $starttime = $input['opening_time'];
                        $endtime = $hour;
                        $duration = $input['duration'];
                        $ar = array();
                        $durations = $duration; 

               for($i=$duration;$i<=11;$i++) {
    
                            $newdate1 = strtotime ( "+$duration hour" , strtotime ($starttime) ) ;
                            $hour1 = date ( 'h A' , $newdate1 );
                            $compairetime = date ( 'H' , $newdate1 );
                            if(trim($endtime) <= trim($compairetime) )
                            {
                            array_push($ar, $hour1);
                            break;                     
                            }
                            else
                            {
                            array_push($ar, $hour1);
                            }                          
  
                          $duration = $duration + $durations;   
                       
 
               }
            
             //$timeinterval=array('timeduration'=>$ar);
                

             // $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'duration' => $ar);
           //print_r($ar);
                
                $tmp = array();
                foreach($ar as $key => $val){
                    $tmp[$key]['k'] = $val;
                }

                /*$tmp = array();
                foreach($ar as $key => $val){
                    $tmp['k'.$key] = $val;
                }*/
                
               $response_array1 = $tmp;
                $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'duration' =>$response_array1);
                
               $response = Response::json($response_array);
              
              return $response;
               
             // echo $newdate; die;

                



              }
        }       



    //Set Beauty Centers,Makeup Artist, Spa and Men's Saloon Data in add to Cart
    public function setBookAppointment(Request $request) {
      
        $validator = Validator::make($request->all(), [
            'vendor_id' => 'required', //Vendor ID
            'product_id' => 'required',//Get product id
            'cart_type' => 'required',//Get cart type
            'cart_sub_type' => 'required',//Get cart sub type
            'staff_id' => 'required', //Get Staff ID
            'date' => 'required',
            'time' => 'required',
            'total_price' => 'required'

            
        ]);


        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();

       




        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => trans($locale.'_mob_lang.INPUT_ERROR')]);
            $response = Response::json($response_array);
        } else {

            try {
                
                 
                $User = Auth::user();
                $product = Products::select('pro_id', 'pro_title','pro_title_ar','pro_desc','pro_desc_ar','pro_Img','pro_price', 'service_hour', 'pro_mc_id')->where('pro_id', '=', $input['product_id'])->first(); //GET Product Detail




               /* $vendor_shop = Category::select('mc_id', 'mc_name', 'opening_time', 'closing_time','home_visit_charge','vendor_id','parent_id')->where('mc_id', '=', $input['vendor_id'])->where('opening_time', '<=', $input['time'])->where('closing_time', '>=', $input['time'])->first(); //GET VENDOR Detail*/


                $vendor_shop = Category::select('mc_id', 'mc_name', 'opening_time', 'closing_time','home_visit_charge','vendor_id','parent_id')->where('mc_id', '=', $input['vendor_id'])->first(); //GET VENDOR Detail


                    
                if (!empty($vendor_shop)) {

                      

                $order_staff = OrderServicesStaff::where('staff_id', '=', $input['staff_id'])->where('start_time', '=', $input['time'])->where('booking_date', '=', date("Y-m-d", strtotime($input['date'])))->first(); //Get Staff detail


                $end_time = date('H:i', strtotime($input['time'].
                    '+'.$product->service_hour.
                    'hour')); //add time duration 
                if (empty($order_staff)) {
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                    if (!empty($cart)) {
                      
                        $cart_pro = CartProduct::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('product_id', $input['product_id'])->delete();

                        $cart_option = CartServiceStaff::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('service_id', $input['product_id'])->delete();

                        $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->where('attribute_id', '=', $input['attribute_id'])->delete();

                        $cart_package = CartProductPackage::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                        

                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                        if (!empty($cart)) {
                            $cart_product_data = array();                            
                            $cart_product_data['cart_type'] = $input['cart_type'];
                            $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                            $cart_product_data['cart_id'] = $cart->id;
                            $cart_product_data['product_id'] = $input['product_id'];
                            /*dd($product->pro_price +  $vendor_shop->home_visit_charge);*/
                            /*if($input['home'] == 1){                              
                              $cart_product_data['total_price'] = $product->pro_price +  $vendor_shop->home_visit_charge;   
                            } else if($input['home'] == 2){
                              $cart_product_data['total_price'] = $product->pro_price +  $vendor_shop->home_visit_charge;   
                            } else if($input['home'] == 3){
                               $cart_product_data['total_price'] = $product->pro_price;  
                            }*/ 
                            $cart_product_data['total_price'] = $input['total_price'];          
                            $cart_product_data['status'] = 1;
                            $cart_product_data['merchant_id'] = $vendor_shop->vendor_id;
                            $cart_product_data['shop_id'] = $vendor_shop->mc_id;
                            $cart_product_data['shop_vendor_id'] = $vendor_shop->parent_id;
                            $cart_product_data['pro_title'] = $product->pro_title;
                            $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                            $cart_product_data['pro_desc'] = $product->pro_desc;
                            $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                            $cart_product_data['pro_Img'] = $product->pro_Img;
                            $cart_product_data['pro_price'] = $product->pro_price;

                            $cart_product = CartProduct::create($cart_product_data); //cart product entry  
                            if (!empty($input['attribute_id'])) {
                            $menuc = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->count();  

                            $cart_attribute_data = array();
                            $cart_attribute_data['cart_type'] = $input['cart_type'];
                             $cart_attribute_data['cart_id'] = $cart->id;
                            $cart_attribute_data['product_id'] = $input['product_id'];
                            $cart_attribute_data['category_id'] = $input['vendor_id'];
                            $cart_attribute_data['attribute_id'] = $input['attribute_id'];
                            if($menuc >=1)
                            {

                            $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first();     
                            $cart_attribute_data['attribute_title'] = $menu->attribute_title;
                            $cart_attribute_data['attribute_title_ar'] = $menu->attribute_title_ar;
                            }
                         
                            $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry
                            }

                            if (isset($input['staff_id']) && $input['staff_id']!=0) {

                            $services_staff = ServicesStaff::select('id', 'staff_member_name', 'staff_member_name_ar','image')->where('id','=',$input['staff_id'])->first();
                                $cart_service_staff = array();
                                $cart_service_staff['cart_id'] = $cart->id;
                                $cart_service_staff['cart_type'] = $input['cart_type'];
                                $cart_service_staff['shop_id'] = $input['vendor_id'];
                                $cart_service_staff['service_id'] = $input['product_id'];
                                $cart_service_staff['staff_id'] = $input['staff_id'];
                                if($input['home'] == 1){
                                  $cart_service_staff['booking_place'] = 'home';   
                                } else if($input['home'] == 2){
                                  $cart_service_staff['booking_place'] = 'resort';  
                                } else if($input['home'] == 3){
                                   $cart_service_staff['booking_place'] = 'shop';
                                } 
                                $cart_service_staff['booking_date'] = date("Y-m-d", strtotime($input['date']));
                                $cart_service_staff['start_time'] = $input['time'];

 
                                 $time= $input['time'];
                                  $service_hour=$product->service_hour;
                                  $newdate= date("H:i", strtotime($time));
                                  $end_time = date('h:i A', strtotime($newdate.
                                                      '+'.$service_hour.
                                                      'hour'));
                               $cart_service_staff['end_time'] = $end_time;
 
                                $cart_service_staff['staff_member_name'] = $services_staff->staff_member_name;
                                $cart_service_staff['staff_member_name_ar'] = $services_staff->staff_member_name_ar;
                                $cart_service_staff['image'] = $services_staff->image;
                                CartServiceStaff::create($cart_service_staff); //Cart Service Entery entery

                            }






                            $packege = ProductPackage::select('id', 'shop_id', 'attribute_id', 'item_id', 'packege_id')->where('packege_id','=',$input['product_id'])->with('getProductPackage:pro_id,pro_title,pro_title_ar,pro_Img,pro_desc,pro_desc_ar,pro_price,attribute_id')->get();
                            if(!empty($packege)){


                            if($input['time']!='' && $input['date']!='')
                            {
                            $end_time = date('H:i', strtotime($input['time'].
                            '+'.$product->service_hour.
                            'hour')); //add time duration 
                            $cart_service_staff = array();
                            $cart_service_staff['cart_id'] = $cart->id;
                            $cart_service_staff['cart_type'] = $input['cart_type'];
                            $cart_service_staff['shop_id'] = $input['vendor_id'];
                            $cart_service_staff['service_id'] = $input['product_id'];
                            $cart_service_staff['staff_id'] = 0;
                            if($input['home'] == 1){
                            $cart_service_staff['booking_place'] = 'home';   
                            } else if($input['home'] == 2){
                            $cart_service_staff['booking_place'] = 'resort';  
                            } else if($input['home'] == 3){
                            $cart_service_staff['booking_place'] = 'shop';
                            } 
                            $cart_service_staff['booking_date'] = date("Y-m-d", strtotime($input['date']));
                            $cart_service_staff['start_time'] = $input['time'];
                            $cart_service_staff['end_time'] = $end_time;

                            CartServiceStaff::create($cart_service_staff); //Cart Service Entery entery 
                            }
                 



                                $cart_package = array();
                                foreach ($packege as $key => $value) {
                                    
                                $cart_package['cart_id'] = $cart->id;
                                $cart_package['cart_type'] = $input['cart_type'];
                                $cart_package['product_id'] = $input['product_id'];
                                $cart_package['package_id'] = $value->getProductPackage->pro_id;
                                $cart_package['pro_title'] = $value->getProductPackage->pro_title;
                                $cart_package['pro_title_ar'] = $value->getProductPackage->pro_title_ar;
                                $cart_package['pro_desc'] = $value->getProductPackage->pro_desc;
                                $cart_package['pro_desc_ar'] = $value->getProductPackage->pro_desc_ar;
                                $cart_package['pro_price'] = $value->getProductPackage->pro_price;
                                $cart_package['pro_img'] = $value->getProductPackage->pro_Img;
                                CartProductPackage::create($cart_package); //Cart Service Entery entery
                                }
                                
                            }

                            $response_array = array('data' => ['success' => true, 'message' => trans($locale.
                        '_mob_lang.APPOINTMENT_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                            $response = Response::json($response_array, 200);
                        } else {
                            $response_array = array('data' => ['success' => false, 'message' => trans($locale.
                        '_mob_lang.CART_ERROR'), 'code' => 500]);
                            $response = Response::json($response_array, 500);
                        }
                } else {

                    $response_array = array('data' => ['success' => false, 'error_code' => 200, 'message' => trans($locale.
                        '_mob_lang.WORKER_ERROR')]);
                    $response = Response::json($response_array, 200);
                }
                } else {

                    $response_array = array('data' => ['success' => false, 'error_code' => 200, 'message' => trans($locale.
                        '_mob_lang.SHOP_ERROR')]);
                    $response = Response::json($response_array, 200);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => trans($locale.
                        '_mob_lang.PROFILE_UPDATE_ERROR')]);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Set Makeup Data in add to Cart
    public function setMakeupAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'cart_type' => 'required',
            'cart_sub_type' => 'required',//Get cart sub type
            'attribute_id' => 'required',
            'product_qty' => 'required',
            'total_price' => 'required'
        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $product = Products::where('pro_id', '=', $input['product_id'])->first();
                
                if($product->pro_qty >= $input['product_qty']){
                $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                    if (!empty($cart)) {
                        $cart_pro_qnty = CartProduct::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('product_id','=', $input['product_id'])->first();
                    if(!empty($cart_pro_qnty)){
                        $qnty = $product->pro_qty + $cart_pro_qnty->quantity;
                       //Products::where('pro_id','=', $input['product_id'])->update(['pro_qty'=> $qnty]);
                    }
                        
                       /* $cart_pro = CartProduct::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('product_id','=', $input['product_id'])->delete();*/

                          $cart_pro =  CartProduct::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('product_id','=', $input['product_id'])->count();
                        
                        if($cart_pro!='') {
                           $cartquantity =  CartProduct::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('product_id','=', $input['product_id'])->first();

                           $quantity1 = $cartquantity->quantity;
                           $totalpricepro = $cartquantity->total_price;
                          // $proprice =  $cartquantity->price;

                           $inputquantity = $input['product_qty'];

                           $totalquantity =  $quantity1+$inputquantity;

                           $totalprice = $input['total_price'];

                           $totalproprice =($totalprice)+($totalpricepro); 
                         
                         CartProduct::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('product_id','=', $input['product_id'])
                          ->update([
                            'quantity' => $totalquantity,
                            'total_price' =>  $totalproprice

                            ]);
                      }

                    /*$cart_service_attribute = CartServiceAttribute::where('cart_type', '=', $input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();*/


                     $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                           
                        /*$cart_attribute = CartServiceAttribute::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('product_id','=', $input['product_id'])->where('attribute_id','=', $input['attribute_id'])->delete();*/

                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                    if(!empty($input['attribute_id'])){
                    $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first();     
                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $input['cart_type'];
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['product_id'] = $input['product_id'];
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['attribute_id'] = $input['attribute_id'];
                    $cart_attribute_data['attribute_title'] = $menu->attribute_title;
                    $cart_attribute_data['attribute_title_ar'] = $menu->attribute_title_ar;
                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry 
                    }
                    
                    if($cart_pro==0) { 
                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $input['product_id'];
                    $cart_product_data['status'] = 1;
                    $cart_product_data['quantity'] = $input['product_qty'];
                    $cart_product_data['total_price'] = $input['total_price']; 
                    $cart_product_data['shop_vendor_id'] = $category->parent_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['pro_price'] = $product->pro_price;
                    
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry
                    $updated_pro = Products::where('pro_id', '=', $input['product_id'])->first();
                    $dta = $updated_pro->pro_qty - $input['product_qty'];
                    //Products::where('pro_id','=', $input['product_id'])->update(['pro_qty'=> $dta]);

                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.
                        '_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                }
                    
                }
                
                else if ($product->pro_qty <= $input['product_qty'] && $product->pro_qty > 0)
                {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.
                        '_mob_lang.QUANTITY_ERROR'). $product->pro_qty, 'code' => 200]);
                    $response = Response::json($response_array, 200);
                    

                } 
                else if ($product->pro_qty == 0) 
                {
                    $response_array = array('data' => ['success' => false, 'message' =>trans($locale.
                        '_mob_lang.PRODUCT_ERROR'), 'code' => 500]);
                    $response = Response::json($response_array, 500);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.
                        '_mob_lang.PRODUCT_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }

    /**Get Makeup, Beauty Centers, Makeup Artist, Spa and Men's Saloon data from add to cart**/
    public function getMakeupAddToCart(Request $request) {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                $catg_data = array();
                $cart_pro = CartProduct::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,pro_disprice')->first();

                //get food product  product 
                $catg_data['experience'] = '';
                if (!empty($cart) && !empty($cart_pro)) {

                       // get Date(type) data from add to cart  
                        $attribute = CartServiceAttribute::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->with('getProductServiceAttribute:id,services_id,attribute_title,attribute_title_ar')->first();

                        $category = Category::select('mc_id','mc_img', 'mc_name', 'mc_name_ar','address','address_ar', 'mc_status','city_id','home_visit_charge')->where('mc_id', '=', $cart_pro->getProduct[0]->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();

                      
                        $catg_data['pro_id'] = $cart_pro->getProduct[0]->pro_id;
                        if ($input['language_type'] == 'en') {
                            if(!empty($category)){
                               $catg_data['category_name'] = $category->mc_name;
                               $catg_data['category_img'] = $category->mc_img;
                               $catg_data['address'] = $category->address;
                               $catg_data['city_name'] = $category->getCity->ci_name;

                            } 
                         
                            if(!empty($attribute) && count($attribute->getProductServiceAttribute) >=1){
                               $catg_data['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title; 
                           }    



                            $catg_data['pro_title'] = $cart_pro->getProduct[0]->pro_title;
                            $catg_data['pro_desc'] = $cart_pro->getProduct[0]->pro_desc;
                        } else {
                            if(!empty($category)){
                               $catg_data['category_name'] = $category->mc_name_ar;
                               $catg_data['category_img'] = $category->mc_img;
                               $catg_data['address'] = $category->address_ar;
                               $catg_data['city_name'] = $category->getCity->ci_name_ar;  
                            } 
                            if(!empty($attribute) && count($attribute->getProductServiceAttribute) >=1){
                              $catg_data['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title_ar;  
                            }
                            
                            $catg_data['pro_title'] = $cart_pro->getProduct[0]->pro_title_ar;
                            $catg_data['pro_desc'] = $cart_pro->getProduct[0]->pro_desc_ar;
                        }
 
                       // $catg_data['pro_qty'] = $cart_pro->getProduct[0]->pro_qty;
                        $catg_data['pro_qty'] = $cart_pro->quantity;
                         
                          if($cart_pro->getProduct[0]->pro_disprice=='' || $cart_pro->getProduct[0]->pro_disprice==0) {
                                 $catg_data['pro_price'] = $cart_pro->getProduct[0]->pro_price;

                               } else {

                                  $catg_data['pro_price'] = $cart_pro->getProduct[0]->pro_disprice;
                               }


                       // $catg_data['pro_price'] = $cart_pro->getProduct[0]->pro_price;



                        $catg_data['home_visit_charge'] = $category->home_visit_charge;
                        $catg_data['pro_Img'] = $cart_pro->getProduct[0]->pro_Img;
                        $catg_data['pro_status'] = $cart_pro->getProduct[0]->pro_status;
                        if(!empty($cart_pro->quantity)){
                           $catg_data['total_price'] = $cart_pro->total_price; 
                       }else{
                        $catg_data['total_price'] = $cart_pro->total_price; 
                       }

$cart_service_staffchk = CartServiceStaff::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('service_id', $input['product_id'])->first();

 
  $cart_service_staff = CartServiceStaff::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('service_id', $input['product_id'])->with('getStaff:id,staff_member_name,staff_member_name_ar,staff_member_address,staff_member_address_ar,image,experience,availability_status')->first();  

      

                       if (!empty($cart_service_staff)) {
                        //$catg_data['booking_place'] = $cart_service_staff->booking_place;
                                if($cart_service_staff->booking_place =='home'){
                                $catg_data['cus_address'] = $User->cus_address1; 
                                if ($input['language_type'] == 'en') {
                                $catg_data['booking_place'] = 'home';
                                }
                                else
                                {
                                $catg_data['booking_place'] = 'المنزل';   
                                }

                                }
                                if($cart_service_staff->booking_place =='shop'){
                                $catg_data['cus_address'] = $User->cus_address1; 
                                if ($input['language_type'] == 'en') {
                                $catg_data['booking_place'] = 'shop';
                                }
                                else
                                {
                                $catg_data['booking_place'] = 'متجر';   
                                }

                                }

                                
 
                        if(isset($cart_service_staff->getStaff->id) && $cart_service_staff->getStaff->id!='')
                        {
                           
                        $catg_data['staff_id'] = $cart_service_staff->getStaff->id;   
                        if ($input['language_type'] == 'en') {
                            
                        $catg_data['member_name'] = $cart_service_staff->getStaff->staff_member_name;
                        $catg_data['staff_member_address'] = $cart_service_staff->getStaff->staff_member_address;
                        }else{
                        $catg_data['member_name_ar'] = $cart_service_staff->getStaff->staff_member_name_ar;
                        $catg_data['staff_member_address_ar'] = $cart_service_staff->getStaff->staff_member_address;

                        }
                         
                        $catg_data['image'] = $cart_service_staff->getStaff->image;
                        $catg_data['experience'] = $cart_service_staff->getStaff->experience; 
                        $catg_data['availability_status'] = $cart_service_staff->getStaff->availability_status;
 
                        }
                        else
                        {

                        $catg_data['staff_id'] = '0';
                        $catg_data['staff_member_name_ar'] = '';
                        $catg_data['staff_member_name'] = '';
                        $catg_data['staff_member_address_ar'] = '';
                        $catg_data['staff_member_address'] = '';
                        
                        if(isset($cart_service_staff->getStaff->experience) && $cart_service_staff->getStaff->experience!='') {
                        $catg_data['experience'] = $cart_service_staff->getStaff->experience;
                         } 
                        $catg_data['availability_status'] = '';
                        }
              
                        $catg_data['booking_date'] = date("d-m-Y", strtotime($cart_service_staff->booking_date));
                        $catg_data['start_time'] = $cart_service_staff->start_time;
                        $catg_data['end_time'] = $cart_service_staff->end_time; 
                       
                        }



                        if ($input['language_type'] == 'en') {
                            $packege = CartProductPackage::select('package_id','pro_title','pro_desc','pro_price','pro_img')->where('cart_type','=','beauty')->where('product_id','=',$input['product_id'])->get();
                            if(!empty($packege)){

 

                        $cart_service_staff = CartServiceStaff::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('service_id', $input['product_id'])->first();

                        if(isset($cart_service_staff->booking_place) && $cart_service_staff->booking_place!='') {
                        //$catg_data['booking_place'] = $cart_service_staff->booking_place;

                            if($cart_service_staff->booking_place=='shop')
                            {
                            if ($input['language_type'] == 'en') {
                            $catg_data['booking_place'] = 'Shop';
                            }
                            else
                            {
                            $catg_data['booking_place'] = 'متجر';   
                            }

                            }
                            if($cart_service_staff->booking_place=='home')
                            {
                            if ($input['language_type'] == 'en') {
                            $catg_data['booking_place'] = 'Home';
                            }
                            else
                            {
                            $catg_data['booking_place'] = 'المنزل';   
                            }

                            }
                           



                         }

                        $catg_data['cus_address'] = $User->cus_address1; 
                        $catg_data['staff_member_name'] = '';
                        $catg_data['staff_member_address_ar'] = '';
                        if(isset($cart_service_staff->booking_date) && $cart_service_staff->booking_date!='' ) {
                        $catg_data['booking_date'] = date("d-m-Y", strtotime($cart_service_staff->booking_date));
                         }
                         if(isset($cart_service_staff->start_time) && $cart_service_staff->start_time!='') {
                        $catg_data['start_time'] = $cart_service_staff->start_time;
                         }
                      if(isset($cart_service_staff->end_time) && $cart_service_staff->end_time!='') {
                        $catg_data['end_time'] = $cart_service_staff->end_time; 
                       }
                     // $catg_data['experience'] = $cart_service_staff->getStaff->experience; 
                        
                        $catg_data['availability_status'] = '';
                        $catg_data['product_packege'] = $packege; 
                            }

                        } else{
                      



                     $packege = CartProductPackage::select('package_id','pro_title_ar as pro_title','pro_desc_ar as  pro_desc','pro_price','pro_img')->where('cart_type','=','beauty')->where('product_id','=',$input['product_id'])->get();
                            if(!empty($packege)){

 

                        $cart_service_staff = CartServiceStaff::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('service_id', $input['product_id'])->first();

                        if(isset($cart_service_staff->booking_place) && $cart_service_staff->booking_place!='') {
                        //$catg_data['booking_place'] = $cart_service_staff->booking_place;

                            if($cart_service_staff->booking_place=='shop')
                            {
                            if ($input['language_type'] == 'en') {
                            $catg_data['booking_place'] = 'Shop';
                            }
                            else
                            {
                            $catg_data['booking_place'] = 'متجر';   
                            }

                            }
                            if($cart_service_staff->booking_place=='home')
                            {
                            if ($input['language_type'] == 'en') {
                            $catg_data['booking_place'] = 'Home';
                            }
                            else
                            {
                            $catg_data['booking_place'] = 'المنزل';   
                            }

                            }
                           



                         }

                        $catg_data['cus_address'] = $User->cus_address1; 
                        //$catg_data['staff_member_name'] = '';
                        $catg_data['staff_member_address_ar'] = '';
                        if(isset($cart_service_staff->booking_date) && $cart_service_staff->booking_date!='' ) {
                        $catg_data['booking_date'] = date("d-m-Y", strtotime($cart_service_staff->booking_date));
                         }
                         if(isset($cart_service_staff->start_time) && $cart_service_staff->start_time!='') {
                        $catg_data['start_time'] = $cart_service_staff->start_time;
                         }
                      if(isset($cart_service_staff->end_time) && $cart_service_staff->end_time!='') {
                        $catg_data['end_time'] = $cart_service_staff->end_time; 
                       }
                     // $catg_data['experience'] = $cart_service_staff->getStaff->experience; 
                        
                        $catg_data['availability_status'] = '';
                        $catg_data['product_packege'] = $packege; 
                            }
                        }
                    
                        $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'cart_id' => $cart->id,'makeup_cart_data' => $catg_data);
                    } else {
                        $response_array = array('data' => ['success' => false, 'message' => 'cart data not found & cart type must be beauty and elegance', 'code' => 500]);
                    }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }


     /**delete to add to cart  data**/
    public function deleteBeautyData(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'token' => 'required'
        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();

                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                $cart_pro = CartProduct::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->first();
                $product = Products::where('pro_id','=', $input['product_id'])->first();
                $qnty = $product->pro_qty + $cart_pro->quantity;
                //Products::where('pro_id','=', $input['product_id'])->update(['pro_qty'=> $qnty]);
                if (!empty($cart) && !empty($cart_pro)) {

                    $cart_attribute_data= CartServiceStaff::where('service_id', '=', $input['product_id'])->where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->delete();
                    //delete serice staff data
                   

                    $cart_attribute_data= CartServiceAttribute::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->delete();
                    //delete attribute data

                    $cart_pro = CartProduct::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->delete();
                    //delete product

                    $response_array = array('data' => ['success' => true, 'message' =>
                    trans($locale.
                        '_mob_lang.DELETE_CART_DATA'), 'code' => 200]);

                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.
                        '_mob_lang.PRODUCT_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }
    

}
<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\JWT;
use Tymon\JWTAuth\JWTGuard;
use JWTAuth;
use App\User;
use App\CustomerShipping;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\CartServiceAttribute;
use App\CartProductRent;
use App\CartServiceStaff;
use App\CartHospitality;
use App\CartInvitation;
use App\CartExternalFoodDish;
use App\CartBodyMeasurement;
use App\CartProductAttribute;
use App\CartProductPackage;
use App\ShippingDescriptions;
use App\OrderProduct;
use App\OrderSingleProduct;
use App\OrderOption;
use App\OrderOptionValue;
use App\OrderProductRent;
use App\OrderServiceAttribute;
use App\OrderServiceStaff;
use App\OrderHospitality;
use App\OrderInvitation;
use App\OrderInternalFoodDish;
use App\OrderExternalFoodDish;
use App\OrderBodyMeasurement;
use App\OrderProductAttribute;
use App\OrderProductPackage;
use App\InsuranceAmount;
use App\SearchOccasion;
use App\Currency;
use App\ServiceInquiry;
use Auth; 
use App\Products;
use Response;
use Validator;
use Exception;
use Storage;
use App\Country;
use App\City;
use App\BusinessOccasionType;
use App\Merchant;
use DB;


class CheckoutController extends Controller {

     public $confirmation_user_email;
    public $confirmation_user_name;
    public $confirmation_merchant_email;
    public $confirmation_merchant_name;

    //Get User Shipping Profile Detail
    public function getShippingProfileInfo(Request $request) {

        $id = Auth::id(); 

        $returnuser = CustomerShipping::select('id','customer_id','ship_name','ship_address1','ship_address2','ship_city_id','ship_state','ship_country','ship_postalcode','ship_phone','ship_email')
            ->where('customer_id','=',$id)
            ->with('getShippingCountry:co_id,co_name,co_name_ar')
            ->with('getShippingCity:ci_id,ci_name,ci_name_ar')
            ->first();         
        

        $response_array = array('data' => ['success' => true,'code' => 200, 'shipping_profile' => $returnuser]);
        $response = Response::json($response_array);
        return $response;

    }

    

    //Edit User Shipping Profile
    public function editShippingProfile(Request $request) {

        $validator = Validator::make($request->all(), [
            'ship_name'    => 'required', 
            'ship_address1' => 'required',
            'ship_city_id'      => 'required', 
            'ship_country'  => 'required',
            'ship_phone'  => 'required',
            'ship_postalcode'   => 'required'
        ]);        

        $input = $request->all();
         $locale='';
        if(isset($request->lang) && $request->lang=='ar' )
        {
            $locale='ar';
        }
        else
        {
            $locale='en';
        }
        app()->setLocale($locale); 

        if ($validator->fails()) {

            $response_array = array('data' => ['success' => false,'error_code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {
            try {

                 $id = Auth::id(); 
                 $shippingUser = CustomerShipping::select('id','customer_id','ship_name','ship_address1','ship_address2','ship_city_id','ship_state','ship_country','ship_postalcode','ship_phone','ship_email')
                    ->where('customer_id','=',$id)
                    ->with('getShippingCountry:co_id,co_name')
                    ->with('getShippingCity:ci_id,ci_name')
                    ->first();     

                unset($input['token']);
             
                $checkcount = CustomerShipping::where('customer_id',$id)->count();
               

                 if($checkcount >0) {

                    unset($input['lang']);
                   CustomerShipping::where('customer_id', '=', $id)->update($input);

            } else {
                   $saveuserdata =  new CustomerShipping;

                    $saveuserdata->ship_name = $input['ship_name'];

                    $saveuserdata->ship_address1 = $input['ship_address1'];

                    $saveuserdata->ship_city_id = $input['ship_city_id'];

                    $saveuserdata->ship_country = $input['ship_country'];

                    $saveuserdata->ship_phone = $input['ship_phone'];

                    $saveuserdata->ship_postalcode = $input['ship_postalcode'];

                    $saveuserdata->customer_id = $id;

                    $saveuserdata->save();



             }

                $response_array = array('data' => ['success' => true,'code' => 200, 'message' =>trans($locale.'_mob_lang.PROFILE_UPDATE_SUCESS')]);
                $response = Response::json($response_array);
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false,'error_code' => 203, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR')]);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Get Country Vat
    public function getCountryVat(Request $request) 
    {
        $country_id = $request->country_id;
        $countrylist = Country::where('co_id', '=', $country_id)->orderBy('co_name', 'ASC')->get();
        $response_array = array('data' => ['success' => true,'code' => 200, 'countrylist' => $countrylist]);
        $response = Response::json($response_array);
        return $response;
    }

    //Get Country Currency
    public function Currency(Request $request) 
    {
        
        $currencylist = Currency::orderBy('cur_id', 'ASC')->get();
        $response_array = array('data' => ['success' => true,'code' => 200, 'currency' => $currencylist]);
        $response = Response::json($response_array);
        return $response;
    }

    //Event Checkout
    public function eventCheckout(Request $request) {

        $validator = Validator::make($request->all(), [
            'token'          => 'required',
            'payment_method' => 'required',
            'payment_status' => 'required',
            'transaction_id' => 'required',
            'total_amount'   => 'required',
            'wallet_amount_deducted' => 'required',
            //'order_id'       => 'required',
            'vat'       => 'required',

        ]);        



        $input = $request->all();
        $locale='';
        if(isset($request->lang) && $request->language_type=='ar' )
        {
            $locale='ar';
        }
        else
        {
            $locale='en';
        }
        app()->setLocale($locale); 

        if ($validator->fails()) {

            $response_array = array('data' => ['success' => false,'error_code' => 204, 'error' => trans($locale.'_mob_lang.INPUT_ERROR')]);
            $response = Response::json($response_array);
        } else {
            try {
               
                $User = Auth::user();
                $cart = Cart::where('user_id',$User->cus_id)->first();
                //get user's detail
                $oreder_lastidcount = OrderProduct::count();
               
                if($oreder_lastidcount>0) {
                 $oreder_lastid = OrderProduct::orderBy('id','DESC')->first();  
                $order_id = $oreder_lastid->order_id+1;
                } else {
                  
                $order_id = 1001;
                }

               
 
                if(!empty($cart)){
                $occasion_search = SearchOccasion::where('user_id',$User->cus_id)->where('order_id', '=', null)->orderBy('updated_at', 'DESC')->first();
                $occasion_order['order_id'] = $order_id;
                SearchOccasion::where('id',$occasion_search->id)->update($occasion_order);
 
                $order_event = array();
                $order_event['order_cus_id'] = $User->cus_id;
                $order_event['order_id'] = $order_id;
                $order_event['transaction_id'] = $input['transaction_id'];
                $order_event['payer_email'] = $User->email;                
                $order_event['payer_name'] = $User->cus_name;
                $order_event['wallet_amount'] = $input['wallet_amount_deducted'];
                $order_event['order_paytype'] = $input['payment_method'];
                $order_event['order_amt'] = $input['total_amount'];
                $order_event['order_status'] = $input['payment_status'];
                if($input['hall_pending_amount']!=''){

                    $order_event['hall_pending_amount'] = $input['hall_pending_amount'];

                }
                else{

                       $order_event['hall_pending_amount'] = 0; 
                }                
                $order_event['search_occasion_id'] = $occasion_search->occasion_id;
                $order_event['order_taxAmt'] = $input['vat'];

                if(isset($input['cid']) && $input['cid']!='')
                {
                $getCvat = DB::table('nm_country')->where('co_id','=',$input['cid'])->first();
                $vat = $getCvat->vat;         
                $order_event['vat_tax'] = $vat;
                }
    
                    $order = OrderProduct::create($order_event);






                $getShipping = DB::table('nm_customer_shipping_details')->where('customer_id',$User->cus_id)->first();

                $ship_address1 = $getShipping->ship_address1;
                $ship_postalcode = $getShipping->ship_postalcode;
                $ship_country = $getShipping->ship_city_id;
                $ship_phone = $getShipping->ship_phone;
                $ship_name = $getShipping->ship_name;


                  OrderProduct::where('order_id',$order_id)->update(['order_shipping_add'=>$ship_address1,'shipping_city'=>$ship_country,'payer_phone'=>$ship_phone,'payer_name'=>$ship_name,]); //cart product entry 
 
                if($input['wallet_amount_deducted']){

                    
                    $dta['wallet'] = $User->wallet  - $input['wallet_amount_deducted'];
                    

                
                User::where('cus_id','=',$User->cus_id)->update($dta);

                }


                DB::table('nm_shipping')->where('cart_id', '=', $cart->id)->update(['ship_order_id'=>$order_id]);
                $cart_pro = CartProduct::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail

                if(!$cart_pro->isEmpty()){
                  foreach ($cart_pro as $key => $value) {
                        $Order_product = array();
                        $insurance_amount = array();
                        $Order_product['cus_id'] = $User->cus_id;
                        $Order_product['order_id'] = $order_id;
                        $Order_product['product_type'] = $value->cart_type;
                        $Order_product['product_sub_type'] = $value->cart_sub_type;                    
                        //$Order_product['category_id'] = $value->category_id;
                         
                        $Order_product['category_id'] = $value->shop_id;
                        $Order_product['product_id'] = $value->product_id;
                        $Order_product['quantity'] = $value->quantity;
                        $Order_product['total_price'] = $value->total_price;
                        $Order_product['paid_total_amount'] = $value->paid_total_amount;
                        $Order_product['product_size'] = $value->product_size;
                        $Order_product['buy_rent'] = $value->buy_rent;
                        if($value->cart_sub_type == 'beauty_centers' || $value->cart_sub_type == 'men_saloon' || $value->cart_sub_type == 'spa' || $value->cart_sub_type == 'makeup_artist')
                        {
                        $Order_product['review_type'] = 'worker';
                        }
                        else
                        {
                         $Order_product['review_type'] = 'shop';   
                        }
  
                        $Order_product['merchant_id'] = $value->merchant_id;
                        $Order_product['shop_vendor_id'] = $value->shop_vendor_id;                    
                        $Order_product['shop_id'] = $value->shop_id;
                        $Order_product['pro_title'] = $value->pro_title;
                        $Order_product['pro_title_ar'] = $value->pro_title_ar;
                        $Order_product['pro_desc'] = $value->pro_desc;
                        $Order_product['pro_desc_ar'] = $value->pro_desc_ar;
                        $Order_product['pro_Img'] = $value->pro_Img;
                        $Order_product['pro_price'] = $value->pro_price;  

                        if($value->hall_booking_date!='' || $value->buy_rent=='rent' || $value->cart_sub_type == 'cosha' )
                        {
                        $Order_product['insurance_amount'] = $value->insurance_amount;
                        }
                        else
                        {
                        $Order_product['insurance_amount'] = 0; 
                        }
 

                        $Order_product['currency'] = $input['currency'];
                        $Order_product['convirson_rate'] = $input['convirson_rate'];

                        $Order_product['status'] = $value->status;
                        //$Order_product['fabric_name'] = $value->fabric_name;


                        $getfabc = DB::table('nm_fabric')->where('id',$value->fabric_name)->count();
                        if($getfabc >=1)
                        {
                        $getfab = DB::table('nm_fabric')->where('id',$value->fabric_name)->first();
                        if (isset($request->language_type) && $request->language_type == 'ar') {
                        $Order_product['fabric_name']  =$getfab->fabric_name_ar;
                        }
                        else
                        {
                        $Order_product['fabric_name']  =$getfab->fabric_name;
                        }
 
                        $Order_product['fabric_id']  =$getfab->id;
                        $Order_product['fabric_img']  =$getfab->image;
                        $Order_product['fabric_price']  =$getfab->price;
                        }
 

                        $Order_product['rose_package_type'] = $value->rose_package_type;
                        $Order_product['location'] = $value->location;
                        $Order_product['hall_booking_date'] = $value->hall_booking_date;
                           $Order_product['showcartproduct'] = $value->showcartproduct;
                           $Order_product['coupon_code'] = $value->coupon_code;
                           $Order_product['coupon_code_amount'] = $value->coupon_code_amount;
                           $Order_product['price_after_coupon_applied'] = $value->price_after_coupon_applied;
                        
                        $product = OrderSingleProduct::create($Order_product); //cart product entry
 
 
                        $pro_ids = $product->id;
                        
                        if($value->insurance_amount > 0){

                        $insurance_amount['merchant_id'] = $value->merchant_id;
                        $insurance_amount['product_type'] = $value->cart_type;
                        $insurance_amount['product_id'] = $value->product_id;
                        $insurance_amount['order_id'] =   $order_id;
                        $insurance_amount['amount'] = $value->insurance_amount;
                        $insurance_amount['requested_date'] = date('Y-m-d H:i:s');
                        $insurance_amount['transation_id'] = $input['transaction_id'];
                        $insurance_amount['pro_title'] = $value->pro_title;
                        $insurance_amount['pro_title_ar'] = $value->pro_title_ar;
                        $insurance_amount['img'] = $value->pro_Img;
                        $insurance_amount['status'] = 0;
                        $product_insurance = InsuranceAmount::create($insurance_amount); //cart product entry 

                        }


 
                         if(isset($value->cart_sub_type) && $value->cart_sub_type  == 'acoustic') {  
                            $AccosticC = DB::table('nm_cart_product')->where('product_id', $value->product_id)->where('cart_id', '=', $cart->id)->count();  
                            $Rentchk = DB::table('nm_cart_product_rent')->where('product_id', $value->product_id)->where('cart_id', '=', $cart->id)->count(); 
                            if($AccosticC >=1 && $Rentchk >=1)
                            {
                            $Rentchkpro = DB::table('nm_cart_product_rent')->where('product_id', $value->product_id)->where('cart_id', '=', $cart->id)->first();
                            $quantity = $Rentchkpro->quantity;
                            $products = DB::table('nm_product_option_value')->where('option_title', 'Quantity in stock')->where('product_id', '=', $value->product_id)->first();
                            $proQty = $products->value;
                            $reduceVal = $proQty- $Rentchkpro->quantity;
                            DB::table('nm_product_option_value')->where('option_title', 'Quantity in stock')->where('product_id', '=', $value->product_id)->update(['value'=>$reduceVal]);
                            }
                            
                         
                        }


                          //Abaya
                         
                           if(isset($value->cart_sub_type) && $value->cart_sub_type=='abaya') {

                            
                           $qtyQuerycount = DB::table('nm_product_option_value')->select('value')->where('product_id',$value->product_id)->where('option_title',$value->product_size)->count();
                           if($qtyQuerycount >0) {
                             $qtyQuery =  DB::table('nm_product_option_value')->where('product_id',$value->product_id)->where('option_title',$value->product_size)->first();
                            $qty = $qtyQuery->value-$value->quantity;
                            
                            $updateqty = DB::table('nm_product_option_value')->where('product_id',$value->product_id)->where('option_title',$value->product_size)                           ->update(['value' => $qty]);
                                
                              }


                            }




                        if(isset($value->cart_sub_type) && $value->cart_sub_type=='dress') {
                        $qtyQuerycount = DB::table('nm_product_option_value')->select('value')->where('product_id',$value->product_id)->where('option_title',$value->product_size)->count();
                        if($qtyQuerycount >0) {
                        $Rentchk = DB::table('nm_cart_product_rent')->where('cart_product_id', $value->id)->where('product_id', $value->product_id)->where('cart_id', '=', $cart->id)->count(); 
                        if($Rentchk < 1){
                        $qtyQuery =  DB::table('nm_product_option_value')->where('product_id',$value->product_id)->where('option_title',$value->product_size)->first();
                        $qty = $qtyQuery->value-$value->quantity;
                        $updateqty = DB::table('nm_product_option_value')->where('product_id',$value->product_id)->where('option_title',$value->product_size)->update(['value' => $qty]);
                        }
                        else
                        {
                        $qtyQuery =  DB::table('nm_product_option_value')->where('product_id',$value->product_id)->where('option_title',$value->product_size)->first();
                        $qty = $qtyQuery->value_ar-$value->quantity;
                        $updateqty = DB::table('nm_product_option_value')->where('product_id',$value->product_id)->where('option_title',$value->product_size)->update(['value_ar' => $qty]);
                        }
                        }

                        }
         
                             


                           if(isset($value->cart_type) && $value->cart_sub_type=='dessert') {
                           $qtyQuerycount = DB::table('nm_product')->select('pro_qty')->where('pro_id',$value->product_id)->count();
                           if($qtyQuerycount!='') {
                             $qtyQuery =  DB::table('nm_product')->where('pro_id',$value->product_id)->first();
                            $qty = $qtyQuery->pro_qty-$value->quantity;
                            
                            $updateqty = DB::table('nm_product')->where('pro_id',$value->product_id)
                           ->update(['pro_qty' => $qty]);
                             
                              }
                            }


  
                           if(isset($value->cart_type) && $value->cart_sub_type=='dates') {
                           $qtyQuerycount = DB::table('nm_product')->select('pro_qty')->where('pro_id',$value->product_id)->count();
                           if($qtyQuerycount!='') {
                             $qtyQuery =  DB::table('nm_product')->where('pro_id',$value->product_id)->first();
                            $qty = $qtyQuery->pro_qty-$value->quantity;
                            
                            $updateqty = DB::table('nm_product')->where('pro_id',$value->product_id)
                           ->update(['pro_qty' => $qty]);
                             
                              }
                            }

                           if(isset($value->cart_type) && $value->cart_type=='shopping') {
                           $qtyQuerycount = DB::table('nm_product')->select('pro_qty')->where('pro_id',$value->product_id)->count();
                           if($qtyQuerycount!='') {
                             $qtyQuery =  DB::table('nm_product')->where('pro_id',$value->product_id)->first();
                            $qty = $qtyQuery->pro_qty-$value->quantity;
                            
                            $updateqty = DB::table('nm_product')->where('pro_id',$value->product_id)
                           ->update(['pro_qty' => $qty]);
                             
                              }
                            }


  
                           if(isset($value->cart_sub_type) && $value->cart_sub_type=='events') {
                           $qtyQuerycount = DB::table('nm_product')->select('pro_qty')->where('pro_id',$value->product_id)->count();
                           if($qtyQuerycount!='') {
                             $qtyQuery =  DB::table('nm_product')->where('pro_id',$value->product_id)->first();
                            $qty = $qtyQuery->pro_qty-$value->quantity;
                            
                            $updateqty = DB::table('nm_product')->where('pro_id',$value->product_id)
                           ->update(['pro_qty' => $qty]);
                             
                              }
                            }

 

                             if(isset($value->cart_type) && $value->cart_type=='beauty') {
                           $qtyQuerycount = DB::table('nm_product')->select('pro_qty')->where('pro_id',$value->product_id)->count();
                           if($qtyQuerycount!='') {
                             $qtyQuery =  DB::table('nm_product')->where('pro_id',$value->product_id)->first();
                            $qty = $qtyQuery->pro_qty-$value->quantity;
                            
                            $updateqty = DB::table('nm_product')->where('pro_id',$value->product_id)
                           ->update(['pro_qty' => $qty]);
                             
                              }
                            }


                            if(isset($value->cart_type) && $value->cart_type=='music') {
                           $qtyQuerycount = DB::table('nm_product')->select('pro_qty')->where('pro_id',$value->product_id)->count();
                           if($qtyQuerycount!='') {
                             $qtyQuery =  DB::table('nm_product')->where('pro_id',$value->product_id)->first();
                            $qty = $qtyQuery->pro_qty-$value->quantity;
                            
                            $updateqty = DB::table('nm_product')->where('pro_id',$value->product_id)
                           ->update(['pro_qty' => $qty]);
                             
                              }
                            }

                            

                          if(isset($value->cart_sub_type) && $value->cart_sub_type=='roses') {
                           $qtyQuerycount = DB::table('nm_product')->select('pro_qty')->where('pro_id',$value->product_id)->count();
                           if($qtyQuerycount!='') {
                             $qtyQuery =  DB::table('nm_product')->where('pro_id',$value->product_id)->first();
                            $qty = $qtyQuery->pro_qty-$value->quantity;                            
                            $updateqty = DB::table('nm_product')->where('pro_id',$value->product_id)
                           ->update(['pro_qty' => $qty]);
                             
                              }
                            }   

                    }   
                }

                $cart_option = CartOption::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_option->isEmpty()){
                  foreach($cart_option as $key => $value) {
                            $product_option = array();
                            $product_option['cus_id'] = $User->cus_id;;
                             $product_option['order_id'] = $order_id;
                            $product_option['product_type'] = $value->cart_type;
                            $product_option['product_option_id'] = $value->product_option_id;
                            $product_option['product_id'] = $value->product_id;
                            OrderOption::create($product_option); //order option entry
                    }  
                } 

                $cart_option_hos = CartHospitality::where('cart_id', '=', $cart->id)->get();  
                        if(!$cart_option_hos->isEmpty()){
                        foreach($cart_option_hos as $key => $value) {
                        $product_option = array();
                        $product_option['cus_id'] = $User->cus_id;;
                        $product_option['order_id'] = $order_id;
                        $product_option['category_id'] = $value->category_id;
                        $product_option['no_of_staff'] = $value->no_of_staff;
                        $product_option['nationality'] = $value->nationality;
                        $product_option['date'] = $value->date;
                        $product_option['time'] = $value->time;
                        $product_option['elocation'] = $value->elocation;
                        $product_option['order_type'] = $value->order_type;
                        OrderHospitality::create($product_option); //order option entry
                        }  
                        }

  

                $cart_option_value = CartOptionValue::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_option_value->isEmpty()){
                  foreach($cart_option_value as $key => $value) {
                            $product_option_value = array();
                            $product_option_value['cus_id'] = $User->cus_id;
                            $product_option_value['order_id'] = $order_id;
                            $product_option_value['product_type'] = $value->cart_type;
                            $product_option_value['product_id'] = $value->product_id;
                            $product_option_value['product_option_id'] = $value->product_option_id;
                            $product_option_value['product_option_value_id'] = $value->product_option_value_id;
                            $product_option_value['value'] = $value->value;
                            $product_option_value['quantity'] = $value->quantity;
                            $product_option_value['status'] = $value->status;
                            $product_option_value['option_title'] = $value->option_title;
                            $product_option_value['option_title_ar'] = $value->option_title_ar;


                            $product_option_value['option_value_title'] = $value->option_value_title;
                            $product_option_value['option_value_title_ar'] = $value->option_value_title_ar;
                            $product_option_value['option_value_price'] = $value->price;
                            
                            OrderOptionValue::create($product_option_value); //order option value

                            //Quantity decrease

                             if(isset($value->cart_type) && $value->music!='') {
                           $qtyQuerycount = DB::table('nm_product_option_value')->select('value')->where('product_id',$value->product_id)->count();
                           if($qtyQuerycount!='') {
                             $qtyQuery =  DB::table('nm_product_option_value')->where('product_id',$value->product_id)->first();
                            $qty = $qtyQuery->value-$value->quantity;
                            
                            $updateqty = DB::table('nm_product_option_value')->where('product_id',$value->product_id)

                           ->update(['value' => $qty]);
                             
                              }
                            }




                    }  
                }
                $cart_service_attribute = CartServiceAttribute::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_service_attribute->isEmpty()){
                  foreach($cart_service_attribute as $key => $value) {
                            $product_service_attribute = array();
                            $product_service_attribute['cus_id'] = $User->cus_id;
                            $product_service_attribute['order_id'] = $order_id;
                            $product_service_attribute['product_type'] = $value->cart_type;
                            $product_service_attribute['category_id'] = $value->category_id;
                            $product_service_attribute['product_id'] = $value->product_id;
                            $product_service_attribute['attribute_id'] = $value->attribute_id;
                            $product_service_attribute['sub_attribute_id'] = $value->sub_attribute_id;
                            $product_service_attribute['price'] = $value->price;
                            $product_service_attribute['status'] = $value->status;
                            $product_service_attribute['attribute_title'] = $value->attribute_title;
                            $product_service_attribute['attribute_title_ar'] = $value->attribute_title_ar;
                            $product_service_attribute['sub_attribute_title'] = $value->sub_attribute_title;
                            $product_service_attribute['sub_attribute_title_ar'] = $value->sub_attribute_title_ar;
                            $product_service_attribute['pro_title'] = $value->pro_title;
                            $product_service_attribute['pro_title_ar'] = $value->pro_title_ar;
                            $product_service_attribute['pro_desc'] = $value->pro_desc;
                            $product_service_attribute['pro_desc_ar'] = $value->pro_desc_ar;
                            $product_service_attribute['pro_img'] = $value->pro_img;
                             if($value->quantity!='') {
                            $product_service_attribute['quantity'] = $value->quantity;
                            $product_service_attribute['total_price'] = $value->price * $value->quantity;
                              } else {
                                 $product_service_attribute['quantity'] = '';
                              } 
                            $product_service_attribute['insurance_amount'] = $value->insurance_amount;



                           $product_service =  OrderServiceAttribute::create($product_service_attribute); //order service attribute entry

                            if(isset($value->cart_type) && $value->cart_type=='occasion') {
                           $qtyQuerycount = DB::table('nm_product')->select('pro_qty')->where('pro_id',$value->product_id)->count();
                           if($qtyQuerycount!='') {
                             $qtyQuery =  DB::table('nm_product')->where('pro_id',$value->product_id)->first();
                            $qty = $qtyQuery->pro_qty-$value->quantity;
                            
                            $updateqty = DB::table('nm_product')->where('pro_id',$value->product_id)

                           ->update(['pro_qty' => $qty]);
                             
                              }
                            }


                            
                            if(isset($value->cart_type) && $value->cart_type=='food') {
                           $qtyQuerycount = DB::table('nm_product')->select('pro_qty')->where('pro_id',$value->product_id)->count();
                           if($qtyQuerycount!='') {
                             $qtyQuery =  DB::table('nm_product')->where('pro_id',$value->product_id)->first();
                            $qty = $qtyQuery->pro_qty-$value->quantity;
                            
                            $updateqty = DB::table('nm_product')->where('pro_id',$value->product_id)

                           ->update(['pro_qty' => $qty]);
                             
                              }
                            }


                    }  
                }
                 

               $cart_internal_food = CartInternalFoodDish::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_internal_food->isEmpty()){
                  foreach($cart_internal_food as $key => $value) {
                            $product_internal_food = array();
                            $product_internal_food['cus_id'] = $User->cus_id;
                            $product_internal_food['order_id'] = $order_id;
                            $product_internal_food['product_type'] = $value->cart_type;
                            $product_internal_food['product_id'] = $value->product_id;
                            $product_internal_food['internal_food_menu_id'] = $value->internal_food_menu_id;
                            $product_internal_food['internal_food_dish_id'] = $value->internal_food_dish_id;
                            $product_internal_food['container_id'] = $value->container_id;
                            $product_internal_food['quantity'] = $value->quantity;
                            $product_internal_food['price'] = $value->price;
                            $product_internal_food['status'] = $value->status;

                            $product_internal_food['container_title'] = $value->container_title;
                            $product_internal_food['dish_image'] = $value->img;
                            $product_internal_food['internal_dish_name'] = $value->dish_name;
                            $product_internal_food['container_image'] = $value->container_image;
                            
                            OrderInternalFoodDish::create($product_internal_food); //Order internal food entry
                    }  
                }

                $cart_rent_product = CartProductRent::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_rent_product->isEmpty()){
                  foreach($cart_rent_product as $key => $value) {
                            $product_rent = array();
                            $product_rent['cus_id'] = $User->cus_id;
                            $product_rent['order_id'] = $order_id;
                            $product_rent['product_type'] = $value->cart_type;
                            $product_rent['service_id'] = $value->service_id;
                            $product_rent['product_id'] = $value->product_id;
                            //$product_rent['rental_date'] = date("d-m-Y", strtotime($value->rental_date));
                             $product_rent['rental_date'] = $value->rental_date;
                             $product_rent['return_date'] = $value->return_date;
                           // $product_rent['rental_date'] = $value->rental_date;
                            $product_rent['rental_time'] = $value->rental_time;
                            //$product_rent['return_date'] = $value->return_date;
                            $product_rent['return_time'] = $value->return_time;                            
                            $product_rent['quantity'] = $value->quantity;
                            $product_rent['insurance_amount'] = $value->insurance_amount;

 

                        $productas = OrderSingleProduct::where('product_id',$value->product_id)->where('insurance_amount','!=','')->where('order_id',$order_id)->count(); //cart product entry 

                        if( $productas >=1)
                        {
                        $producta = OrderSingleProduct::where('product_id',$value->product_id)->where('insurance_amount','!=','')->where('order_id',$order_id)->first(); //cart product entry 
                        $pro_idss = $producta->id;

                        }  
                        else
                        {

                            $productaCC = OrderSingleProduct::where('product_id',$value->product_id)->where('order_id',$order_id)->count(); //cart product entry 
                            if($productaCC >=1)
                            {
                            $productaCAC = OrderSingleProduct::where('product_id',$value->product_id)->where('order_id',$order_id)->first();  
                            $pro_idss = $productaCAC->id;
                            }
                            else
                            {
                              $pro_idss = 0;  
                            }

 
                        }       
 
                             $product_rent['order_product_id'] = $pro_idss;
                            OrderProductRent::create($product_rent); //Order rent product entry


                         
                    }  
                }

                $cart_service_staff = CartServiceStaff::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_service_staff->isEmpty()){
                  foreach($cart_service_staff as $key => $value) {
                            $product_service_staff = array();
                            $product_service_staff['cus_id'] = $User->cus_id;
                            $product_service_staff['order_id'] =$order_id;
                            $product_service_staff['product_type'] = $value->cart_type;
                            $product_service_staff['shop_id'] = $value->shop_id;
                            $product_service_staff['service_id'] = $value->service_id;                            
                            $product_service_staff['file_no'] = $value->file_no;
                            $product_service_staff['staff_id'] = $value->staff_id;
                            $product_service_staff['booking_place'] = $value->booking_place;
                            $product_service_staff['booking_date'] = $value->booking_date;                            
                            $product_service_staff['start_time'] = $value->start_time;
                            $product_service_staff['end_time'] = $value->end_time;
                            $product_service_staff['staff_member_name'] = $value->staff_member_name;                            
                            $product_service_staff['staff_member_name_ar'] = $value->staff_member_name_ar;
                            $product_service_staff['image'] = $value->image;
                            
                            OrderServiceStaff::create($product_service_staff); //Order service staff entry
                    }  
                }

                $cart_hospitality = CartHospitality::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_hospitality->isEmpty()){
                  // foreach($cart_hospitality as $key => $value) {
                  //           $product_hospitality = array();
                  //           $product_hospitality['cus_id'] = $User->cus_id;
                  //           $product_hospitality['order_id'] = $order_id;
                  //           $product_hospitality['category_id'] = $value->category_id;
                  //           $product_hospitality['no_of_staff'] = $value->no_of_staff;
                  //           $product_hospitality['nationality'] = $value->nationality;                            
                  //           $product_hospitality['date'] = $value->date;
                  //           $product_hospitality['time'] = $value->time;

                  //           OrderHospitality::create($product_hospitality); //Order hospitality entry
                  //   }  
                }

                $cart_invitaion = CartInvitation::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_invitaion->isEmpty()){
                  foreach($cart_invitaion as $key => $value) {
                            $product_invitation = array();
                            $product_invitation['customer_id'] = $User->cus_id;
                               $chkID = $value->id;
                            $product_invitation['order_id']    = $order_id;
                            $product_invitation['services_id'] = $value->services_id;
                            $product_invitation['occasion_name'] = $value->occasion_name;
                            $product_invitation['occasion_type'] = $value->occasion_type;                            
                            $product_invitation['venue'] = $value->venue;
                            $product_invitation['date'] = $value->date;
                            $product_invitation['time'] = $value->time;
                            $product_invitation['no_of_invitees'] = $value->no_of_invitees;
                            $product_invitation['invitaion_mode'] = $value->invitaion_mode;
                            $product_invitation['invitation_msg'] = $value->invitation_msg;                            
                            $product_invitation['packge_id'] = $value->packge_id;
                            $product_invitation['total_member_invitation'] = $value->total_member_invitation;
                            $product_invitation['card_price'] = $value->card_price;
                            $product_invitation['status'] = $value->status;
                            $product_invitation['packge_title'] = $value->packge_title;                            
                            $product_invitation['packge_title_ar'] = $value->packge_title_ar;
                            $product_invitation['packge_desc'] = $value->packge_desc;
                            $product_invitation['packge_desc_ar'] = $value->packge_desc_ar;
                            $product_invitation['packge_img'] = $value->packge_img;
                            $product_invitation['pro_label'] = $value->pro_label;
                            $product_invitation['pro_label_ar'] = $value->pro_label_ar;
                            OrderInvitation::create($product_invitation); //Order Invitation entry
                            $EventDate =  date("Y-m-d",strtotime($value->date));

                            $Urls = $value->file_url;
                            $currentdate = date('Y-m-d H:i:s');
                            if(($handle = fopen($Urls,'r'))!== FALSE){
                            while(($data = fgetcsv($handle,1000,',')) !== FALSE){
                            DB::table('nm_invitation_list')->insert(array('customer_id'=>$User->cus_id,'packge_id'=>$value->packge_id,'invite_name'=>$data[0],'invite_email'=>$data[1],'invite_phone'=>$data[2],'address'=>$User->cus_address1,'created_at'=>$currentdate,'updated_at'=>$currentdate,'order_id'=>$order_id,'occasion_date'=>$EventDate));
                            }
                            fclose($handle);
                            }
                    }  
                }

                $cart_external_food = CartExternalFoodDish::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_external_food->isEmpty()){
                  foreach($cart_external_food as $key => $value) {
                            $product_external_food = array();
                            $product_external_food['cus_id'] = $User->cus_id;
                            $product_external_food['order_id'] = $order_id;
                            $product_external_food['category_id'] = $value->category_id;
                            $product_external_food['external_food_menu_id'] = $value->external_food_menu_id;
                            $product_external_food['external_food_dish_id'] = $value->external_food_dish_id;                            
                            $product_external_food['container_id'] = $value->container_id;
                            $product_external_food['quantity'] = $value->quantity;
                            $product_external_food['price'] = $value->price;
                            $product_external_food['status'] = $value->status;
                            
                            $product_external_food['container_title'] = $value->container_title;
                            $product_external_food['img'] = $value->img;
                            $product_external_food['dish_name'] = $value->dish_name;
                            $product_external_food['container_image'] = $value->container_image;

                            OrderExternalFoodDish::create($product_external_food); //Order hospitality entry
                    }  
                } 

                $cart_measurement = CartBodyMeasurement::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_measurement->isEmpty()){
                  foreach($cart_measurement as $key => $value) {
                            $product_measurement = array();
                            $product_measurement['customer_id'] = $User->cus_id;
                            $product_measurement['order_id'] = $order_id;
                            $product_measurement['product_id'] = $value->product_id;
                            $product_measurement['length'] = $value->length;
                            $product_measurement['chest_size'] = $value->chest_size;                            
                            $product_measurement['soulders'] = $value->soulders;
                            $product_measurement['neck'] = $value->neck;
                            $product_measurement['arm_length'] = $value->arm_length;
                            $product_measurement['wrist_diameter'] = $value->wrist_diameter;
                            $product_measurement['cutting'] = $value->cutting;
                            $product_measurement['status'] = $value->status;
                             $product_measurement['waistsize'] = $value->waistsize;

                            OrderBodyMeasurement::create($product_measurement); //Order Measurement entry
                    }  
                } 

                $cart_product = CartProductAttribute::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_product->isEmpty()){
                  foreach($cart_product as $key => $value) {
                            $product_attribute = array();
                            $product_attribute['cus_id'] = $User->cus_id;
                            $product_attribute['product_type'] = $value->cart_type;
                            $product_attribute['order_id'] = $order_id;
                            $product_attribute['product_id'] = $value->product_id;
                            $product_attribute['attribute_title'] = $value->attribute_title;
                            $product_attribute['attribute_title_ar'] = $value->attribute_title_ar;                            
                            $product_attribute['value'] = $value->value;

                            OrderProductAttribute::create($product_attribute); //Order Product Attribute entry
                    }  
                } 

                $cart_pro_pakg = CartProductPackage::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_pro_pakg->isEmpty()){
                  foreach($cart_pro_pakg as $key => $value) {
                            $product_package = array();
                            $product_package['cus_id'] = $User->cus_id;
                            $product_package['product_type'] = $value->cart_type;
                            $product_package['order_id'] =$order_id;
                            $product_package['product_id'] = $value->product_id;
                            $product_package['package_id'] = $value->package_id;
                            $product_package['pro_title'] = $value->pro_title;       
                            $product_package['pro_title_ar'] = $value->pro_title_ar;
                            $product_package['pro_desc'] = $value->pro_desc;       
                            $product_package['pro_desc_ar'] = $value->pro_desc_ar;
                            $product_package['pro_price'] = $value->pro_price;       
                            $product_package['pro_img'] = $value->pro_img;

                            OrderProductPackage::create($product_package); //Order Product Attribute entry
                    }  
                } 

                
                $social_url = DB::table('nm_setting')->first();

                $occasion = $order->search_occasion_id;
                $event =$order_id; 
                $this->confirmation_user_email = $User->email;
                $this->confirmation_user_name = "Golden Cage";


                if($input['payment_method']!='Bank_Transfer') {
                $subject = 'Order Acknowledgement Admin'; 
                Mail::send('emails.mobile_order_admin',
                array(
                'user_id' => $User->cus_id,
                'user_name' => $User->cus_name, 
                'user_email' => $User->email,     
                'user_mobile' => $User->cus_phone, 
                'user_address' => $User->cus_address1,
                'user_map_url' => $User->map_url, 
                'order' => $event,    
                'occasion' => $occasion,
                'getFbLinks' => $social_url->url1,
                'gettwitterLinks' => $social_url->url2, 
                'getgoogleLinks' => $social_url->url3,    
                'getinstagramLinks' => $social_url->url4,
                'getpinterestLinks' => $social_url->url5,    
                'getyoutubeLinks' => $social_url->url6,
                'lang' => 'ar_mob_lang',
                'LANUAGE' => 'en_mob_lang',
                ), function($message) use($subject)
                {
                $message->to('abhishekwisi@gmail.com',$this->confirmation_user_name)->subject($subject);
                });

                $subject = 'Order Acknowledgement User';
                Mail::send('emails.mobile_order_user',
                array(
                'user_id' => $User->cus_id,     
                'user_name' => $User->cus_name, 
                'user_email' => $User->email,     
                'user_mobile' => $User->cus_phone, 
                'user_address' => $User->cus_address1,
                'user_map_url' => $User->map_url, 
                'order' => $event,    
                'occasion' => $occasion,
                //'shipping' => $shippingID, 
                'lang' => 'ar_mob_lang',
                'LANUAGE' => 'en_mob_lang',
                'getFbLinks' => $social_url->url1,
                'gettwitterLinks' => $social_url->url2, 
            'getgoogleLinks' => $social_url->url3,    
                'getinstagramLinks' => $social_url->url4,
                'getpinterestLinks' => $social_url->url5,    
                'getyoutubeLinks' => $social_url->url6,
                ), function($message) use($subject)
                {
                $message->from('info@goldencages.com',$this->confirmation_user_name);
                $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
                });

                $total_product = OrderSingleProduct::select('merchant_id')->where('order_id', '=', $order_id)->groupBy('merchant_id')->get();
                foreach ($total_product as $key => $value) {

                $merchant = Merchant::select('mer_id','mer_fname','mer_lname','mer_email')->where('mer_id','=',$value->merchant_id)->first();
                if(!empty($merchant)){

                $this->confirmation_merchant_name = $merchant->mer_fname.' '.$merchant->mer_lname;
                $this->confirmation_merchant_email = $merchant->mer_email;

                    
                // SEND Notification to merchant start


                if(isset($_REQUEST['device']) && $_REQUEST['device']=='android')
                {
                    #API access key from Google API's Console - ANDROID 
                    define( 'API_ACCESS_KEY', 'AAAAImeIyhY:APA91bFNWxng40slYINxvUCxeMbzIMez_h7IvtTsKGmKRmOd6fhMtx1V_sZZ3l_dKBNs-PstV4q6Asc5KSL9s8DtQigZsV-VMtNDVcYuMbqQAt_4M_jYk6v-wRjzhGpCZRTARU-4Lrl1' );
                    $registrationIds = $_GET['id']; 
                    $msg = array
                    (
                    'body'  => 'New order recieved',
                    'title' => 'New order',
                    'icon'  => 'myicon',
                    'sound' => 'default'
                    );
                    $fields = array
                    (
                    'to'        => $registrationIds,
                    'notification'  => $msg
                    );


                    $headers = array
                    (
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                    );
                    #Send Reponse To FireBase Server    
                    $ch = curl_init();
                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                    curl_setopt( $ch,CURLOPT_POST, true );
                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                    $result = curl_exec($ch );
                    curl_close( $ch );
                    echo $result;
                }
                else
                {

                //====================== iOS send notification ===========================//

                // $url = "https://fcm.googleapis.com/fcm/send";
                // $token = "camzhMKxKGY:APA91bFvmccueOScFJUJxPGzC6kIVGJlsPUeVAV7YCJ23OFxjVUlCT4S13B62hJo63A1cQ_HNG5Yx6fM3affTk362pgTLsDv1Byp1bW8PZHiG6rqlECodUSL7UVZIQMH5Ew49vNN2sol";
                // $serverKey = 'AAAAImeIyhY:APA91bFNWxng40slYINxvUCxeMbzIMez_h7IvtTsKGmKRmOd6fhMtx1V_sZZ3l_dKBNs-PstV4q6Asc5KSL9s8DtQigZsV-VMtNDVcYuMbqQAt_4M_jYk6v-wRjzhGpCZRTARU-4Lrl1';
                // $title = "New order";
                // $body = "New order recieved";
                // $notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');
                // $arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');
                // $json = json_encode($arrayToSend);
                // $headers = array();
                // $headers[] = 'Content-Type: application/json';
                // $headers[] = 'Authorization: key='. $serverKey;
                // $ch = curl_init();
                // curl_setopt($ch, CURLOPT_URL, $url);
                // curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
                // curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                // curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
                // //Send the request
                // $response = curl_exec($ch);
                // //Close request
                // if ($response === FALSE) {
                //  die('FCM Send Error: ' . curl_error($ch));
                // }
                // curl_close($ch);





                }
 
                // SEND Notification to merchant end

                $subject = 'New Order'; 
                Mail::send('emails.mobile_order_merchant',
                array(

                'mer_name' => $merchant->mer_fname.' '.$merchant->mer_lname,
                'mer_id' => $value->merchant_id, 
                'user_id' => $User->cus_id,
                'user_name' => $User->cus_name, 
                'user_email' => $User->email,     
                'user_mobile' => $User->cus_phone, 
                'user_address' => $User->cus_address1,
                'user_map_url' => $User->map_url, 
                'order' => $order_id,    
                'occasion' => $order->search_occasion_id,
                'lang' => 'ar_mob_lang',
                'LANUAGE' => 'en_mob_lang',
                'getFbLinks' => $social_url->url1,
                'gettwitterLinks' => $social_url->url2, 
                'getgoogleLinks' => $social_url->url3,    
                'getinstagramLinks' => $social_url->url4,
                'getpinterestLinks' => $social_url->url5,    
                'getyoutubeLinks' => $social_url->url6,
                ), function($message) use($subject)
                {
                $message->to($this->confirmation_merchant_email,$this->confirmation_merchant_name)->subject($subject);
                });

                }
             }
            }


        
                   
                    $response_array = array('data' => ['success' => true, 'message' => 'Data Successfully Added', 'code' => 200]);
                    $response = Response::json($response_array);
 
      
                
  

                $cart_pro = CartProduct::where('cart_id', '=', $cart->id)->delete();

                $cart_pro_rent = CartOption::where('cart_id', '=', $cart->id)->delete();

                $cart_pro = CartOptionValue::where('cart_id', '=', $cart->id)->delete();

               $cart_pro_rent = CartServiceAttribute::where('cart_id', '=', $cart->id)->delete();

                $cart_pro = CartInternalFoodDish::where('cart_id', '=', $cart->id)->delete();

                $cart_pro_rent = CartProductRent::where('cart_id', '=', $cart->id)->delete();

                $cart_pro = CartServiceStaff::where('cart_id', '=', $cart->id)->delete();

                $cart_pro_rent = CartProductRent::where('cart_id', '=', $cart->id)->delete();

                $cart_pro = CartHospitality::where('cart_id', '=', $cart->id)->delete();

                $cart_pro_rent = CartInvitation::where('cart_id', '=', $cart->id)->delete();

                $cart_pro = CartExternalFoodDish::where('cart_id', '=', $cart->id)->delete();

                $cart_pro_rent = CartBodyMeasurement::where('cart_id', '=', $cart->id)->delete();
                $cart_pro_rent = CartProductAttribute::where('cart_id', '=', $cart->id)->delete();
                $cart = CartProductPackage::where('cart_id', '=', $cart->id)->delete();
                $cart = Cart::where('user_id', '=', $User->cus_id)->delete();
                   
                }else{
                    $response_array = array('data' => ['success' => true, 'message' => 'Something went wrong. please try again', 'code' => 200]);
                    $response = Response::json($response_array);
                } 

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false,'error_code' => 203, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR')]);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Get User Shipping Profile Detail
    public function getShippingDesc(Request $request) {
         $id = Auth::id(); 
        $shipping = ShippingDescriptions::select('shipping_id','shipping','delivery_time','description','image','shipping_charge','status')->get();
        

        $response_array = array('data' => ['success' => true,'code' => 200, 'shipping_detail' => $shipping]);
        $response = Response::json($response_array);
        return $response;

    }

    //Request Checkout
    public function requestCheckout(Request $request) {

        $validator = Validator::make($request->all(), [
            'token'          => 'required',
            'payment_method' => 'required',
            'payment_status' => 'required',
            'transaction_id' => 'required',
            'total_amount'   => 'required',           
            'vat'            => 'required',
            'inquiry_id'     => 'required'

        ]);        

        $input = $request->all();
        $locale='';
        if(isset($request->lang) && $request->language_type=='ar' )
        {
            $locale='ar';
        }
        else
        {
            $locale='en';
        }
        app()->setLocale($locale); 

        if ($validator->fails()) {

            $response_array = array('data' => ['success' => false,'error_code' => 204, 'error' => trans($locale.'_mob_lang.INPUT_ERROR')]);
            $response = Response::json($response_array);
        } else {
            try {

                $User = Auth::user();
                $inquiry = $request->inquiry_id;
                $getinquiry = ServiceInquiry::where('user_id', $User->cus_id)->where('id','=', $inquiry)->first();
                //get user's detail

                $oreder_lastidcount = OrderProduct::count();
               
                if($oreder_lastidcount>0) {
                 $oreder_lastid = OrderProduct::orderBy('id', 'DESC')->first();  
                $order_id = $oreder_lastid->order_id+1;
                } else {
                  
                $order_id = 1001;
                }

                if(!empty($getinquiry)){                
                
                $order_event = array();
                $order_event['order_cus_id'] = $User->cus_id;
                $order_event['order_id'] = $order_id;
                $order_event['transaction_id'] = $input['transaction_id'];
                $order_event['payer_email'] = $User->email;                
                $order_event['payer_name'] = $User->cus_name;
                $order_event['order_paytype'] = $input['payment_method'];
                $order_event['order_amt'] = $input['total_amount'];
                $order_event['order_status'] = $input['payment_status'];
                $order_event['order_taxAmt'] = $input['vat'];

                

                $occasion_type = $request->occasion_type;
                $getOCS = DB::table('nm_business_occasion_type')->where('title',$occasion_type)->count();
                if($getOCS>=1)
                {
                $getOC = DB::table('nm_business_occasion_type')->where('title',$occasion_type)->first(); 
                $OCs = $getOC->id;   
                   $mainCat = str_replace('-','', $getOC->top_category);

                }
                else
                {
                $OCs = 0;
                $mainCat = 1;
                }


 

                $order_event['main_occasion_id'] = $mainCat;

               OrderProduct::create($order_event); //cart product entry


                $getShipping = DB::table('nm_customer_shipping_details')->where('customer_id',$User->cus_id)->first();

                $ship_address1 = $getShipping->ship_address1;
                $ship_postalcode = $getShipping->ship_postalcode;
                $ship_country = $getShipping->ship_city_id;
                $ship_phone = $getShipping->ship_phone;
                $ship_name = $getShipping->ship_name;


                OrderProduct::where('order_id',$order_id)->update(['order_shipping_add'=>$ship_address1,'shipping_city'=>$ship_country,'payer_phone'=>$ship_phone,'payer_name'=>$ship_name,]); //cart product entry 
 
                $Order_product = array();
                $insurance_amount = array();
                $Order_product['cus_id'] = $User->cus_id;
                $Order_product['order_id'] = $order_id;
                //$Order_product['product_type'] = $request->type;
                 $Order_product['product_type'] = $request->type;
                $Order_product['product_sub_type'] =  $request->type;
                $Order_product['merchant_id'] = $request->merchant_id;
                $Order_product['quantity'] = 1;
                $Order_product['total_price'] =  $input['total_amount']-$input['vat'];
                $Order_product['paid_total_amount'] =  $input['total_amount']; 
                if(isset($request->type) && $request->type=='recording'){
                                 $Order_product['shop_vendor_id'] =$request->shop_id;  
                                }else{
                                $Order_product['shop_vendor_id'] =$request->merchant_id; 
                            }

                if($request->type == 'recording')
                {
                  $getSmusic = DB::table('nm_music')->where('vendor_id',$request->merchant_id)->where('category_type','acoustics')->first(); 
                  $Order_product['shop_id'] = $request->shop_id; 
                }
                else
                {
                    $getSmusic = DB::table('nm_music')->where('vendor_id',$request->merchant_id)->where('category_type',$request->type)->first();
                    $Order_product['shop_id'] = $getSmusic->id;
                }
                                 



                $Order_product['enquiry_id'] = $inquiry;

                
                $Order_product['status'] = 1;               
                $Order_product['category_id'] = $getSmusic->category_id;
                $Order_product['product_id'] = $getSmusic->category_id;
                OrderSingleProduct::create($Order_product); //cart product entry 
 



 /*
                $occasion_search = SearchOccasion::where('user_id', '=', $User->cus_id)->where('order_id', '=', null)->orderBy('updated_at', 'DESC')->first();


                $occasion_order['order_id'] = $order_id;
                SearchOccasion::where('id','=',$occasion_search->id)->update($occasion_order);
occasion_type
'id','order_id','user_id','main_cat_id','occasion_id','budget','total_member','city_id','occasion_date','end_date','gender'


*/
  
                
 
                $occasion = array();           
                $occasion['order_id'] = $order_id;
                $occasion['user_id'] = $User->cus_id;
                $occasion['main_cat_id'] = $mainCat;
                $occasion['occasion_id'] =  $OCs; 
                $occasion['budget'] = $input['total_amount'];
                $occasion['total_member'] = 0;
                $occasion['city_id'] =  $ship_country;
                $occasion['occasion_date'] =  date("Y-m-d"); 
                $occasion['end_date'] = date("Y-m-d"); 
                $occasion['gender'] = '';

                SearchOccasion::create($occasion);


               ServiceInquiry::where('user_id', $User->cus_id)->where('id','=', $inquiry)->update(['payment_status'=>'1','status'=>'3']);

            $music_order = OrderProduct::where('order_id', '=', $order_id)->select('order_id','order_date','order_paytype','transaction_id','order_taxAmt','order_amt')->first();

            $merchant = Merchant::select('mer_id','mer_fname','mer_lname','mer_email')->where('mer_id','=',$getinquiry->merchant_id)->first();



            $social_url = DB::table('nm_setting')->first();

                
            $this->confirmation_user_email = $User->email;
            $this->confirmation_user_name = "Golden Cage";

/*
            $subject = 'Order Acknowledgement Admin'; 
                Mail::send('emails.mobile_inquiry_order_admin',
                array(
                'user_id' => $User->cus_id,
                'user_name' => $User->cus_name, 
                'user_id' => $User->cus_id,     
                'user_name' => $User->cus_name, 
                'inquiry_id' => $inquiry,
                'order' => $music_order->order_id,    
                'order_date' => date("j M Y", strtotime($music_order->order_date)),
                'order_paytype' => $music_order->order_paytype,    
                'transaction_id' => $music_order->transaction_id,
                'order_taxAmt' => $music_order->order_taxAmt,    
                'order_amt' => $music_order->order_amt + $music_order->order_taxAmt,

                'lang' => 'ar_mob_lang',
                'LANUAGE' => 'en_mob_lang',
                'getFbLinks' => $social_url->url1,
                'gettwitterLinks' => $social_url->url2, 
                'getgoogleLinks' => $social_url->url3,    
                'getinstagramLinks' => $social_url->url4,
                'getpinterestLinks' => $social_url->url5,    
                'getyoutubeLinks' => $social_url->url6,
                ), function($message) use($subject)
                {
                $message->to('abhishekwisi@gmail.com',$this->confirmation_user_name)->subject($subject);
                });



                $subject = 'Order Acknowledgement User';
                Mail::send('emails.mobile_inquiry_order_user',
                array(
                'user_id' => $User->cus_id,     
                'user_name' => $User->cus_name, 
                'inquiry_id' => $inquiry,
                'order' => $music_order->order_id,    
                'order_date' => date("j M Y", strtotime($music_order->order_date)),
                'order_paytype' => $music_order->order_paytype,    
                'transaction_id' => $music_order->transaction_id,
                'order_taxAmt' => $music_order->order_taxAmt,    
                'order_amt' => $music_order->order_amt + $music_order->order_taxAmt,
                'lang' => 'ar_mob_lang',
                'LANUAGE' => 'en_mob_lang',
                'getFbLinks' => $social_url->url1,
                'gettwitterLinks' => $social_url->url2, 
                'getgoogleLinks' => $social_url->url3,    
                'getinstagramLinks' => $social_url->url4,
                'getpinterestLinks' => $social_url->url5,    
                'getyoutubeLinks' => $social_url->url6,
                ), function($message) use($subject)
                {
                $message->from('info@goldencages.com',$this->confirmation_user_name);
                $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
                });


                $this->confirmation_merchant_name = $merchant->mer_fname.' '.$merchant->mer_lname;
                $this->confirmation_merchant_email = $merchant->mer_email;

 
 
                $subject = 'New Order'; 
                Mail::send('emails.mobile_inquiry_order_merchant',
                array(

                'mer_name' => $merchant->mer_fname.' '.$merchant->mer_lname, 
                'user_id' => $User->cus_id,     
                'user_name' => $User->cus_name, 
                'inquiry_id' => $inquiry,
                'order' => $music_order->order_id,    
                'order_date' => date("j M Y", strtotime($music_order->order_date)),
                'order_paytype' => $music_order->order_paytype,    
                'transaction_id' => $music_order->transaction_id,
                'order_taxAmt' => $music_order->order_taxAmt,    
                'order_amt' => $music_order->order_amt + $music_order->order_taxAmt,

                'lang' => 'ar_mob_lang',
                'LANUAGE' => 'en_mob_lang',
                'getFbLinks' => $social_url->url1,
                'gettwitterLinks' => $social_url->url2, 
                'getgoogleLinks' => $social_url->url3,    
                'getinstagramLinks' => $social_url->url4,
                'getpinterestLinks' => $social_url->url5,    
                'getyoutubeLinks' => $social_url->url6,
                ), function($message) use($subject)
                {
                $message->to($this->confirmation_merchant_email,$this->confirmation_merchant_name)->subject($subject);
                });

*/

                $response_array = array('data' => ['success' => true, 'message' => 'Data Successfully Added', 'code' => 200]);
                    $response = Response::json($response_array);
                }else{
                $response_array = array('data' => ['success' => true, 'message' => 'Something went wrong. please try again', 'code' => 200]);
                    $response = Response::json($response_array);
                } 

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false,'error_code' => 203, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR')]);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

        //Request Checkout
    public function requestCheckouttest(Request $request) {

        $validator = Validator::make($request->all(), [
            'token'          => 'required',
            'product_id' => 'required'
        ]);      


        $input = $request->all();
        $locale='';
        if(isset($request->lang) && $request->language_type=='ar' )
        {
        $locale='ar';
        }
        else
        {
        $locale='en';
        }
        app()->setLocale($locale); 
 
        if ($validator->fails()) {

            $response_array = array('data' => ['success' => false,'error_code' => 204, 'error' => trans($locale.'_mob_lang.INPUT_ERROR')]);
            $response = Response::json($response_array);
        } else {
            try {

            $User = Auth::user();



            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
            $response = Response::json($response_array);


            } catch (ModelNotFoundException $e) {
            $response_array = array('data' => ['success' => false,'error_code' => 203, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR')]);
            $response = Response::json($response_array);
            }

}


 return $response;



}

public function getShippingAmt(Request $request)
{
   $validator = Validator::make($request->all(), [
              'merchant_id' => 'required',
              'cart_sub_type'=> 'required',
              'shipping_type'=> 'required',
              'token'=> 'required'
        ]);   

        $input = $request->all();
        $locale='';
        if(isset($request->lang) && $request->language_type=='ar' )
        {
        $locale='ar';
        }
        else
        {
        $locale='en';
        }
        app()->setLocale($locale);  
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false,'error_code' => 204, 'error' => trans($locale.'_mob_lang.INPUT_ERROR')]);
            $response = Response::json($response_array);
        } else {
            try {

            $User = Auth::user();  
            $merchant_id = $input['merchant_id'];
            $cart_sub_type = $input['cart_sub_type'];
            $shipping_type = $input['shipping_type'];
            $price = 0;
            $cartc = Cart::where('user_id',$User->cus_id)->count();
            if($cartc >=1)
            {


            $cart = Cart::where('user_id',$User->cus_id)->first();

          $getproducts = DB::table('nm_cart_product')->where('cart_id',$cart->id)->where('cart_sub_type',$cart_sub_type)->where('merchant_id',$merchant_id)->get();
            $shop_id =  0;
            $totalWgt = 0; 
          foreach ($getproducts as $key => $value) {
                $getProID =  $value->product_id;
                $getproductsWight = DB::table('nm_product')->select('pro_weight')->where('pro_id',$getProID)->first();
                $totalWgt = $totalWgt + $getproductsWight->pro_weight;
                $shop_id =  $value->shop_id;
          }

                //For Aramx
                if($shipping_type ==1)
                {
                    if($totalWgt <=15)
                    {
                        $price = 33;
                    }
                    else
                    {
                        $Extrawgt = $totalWgt-15;
                        $Extaweight = $Extrawgt * 2.5;
                        $newP = 28 + $Extaweight;
                        if($newP <=33)
                        {
                        $price = 33;   
                        }
                        else
                        {
                        $price = $newP;   
                        }
                    }


                }

                //For Pick
                if($shipping_type ==2)
                {
                 if($totalWgt <=20)
                    {
                        $price = 33;
                    }
                    else
                    {
                        $Extrawgt = $totalWgt-20;
                        $Extaweight = $Extrawgt * 3;
                        $newP = 24 + $Extaweight;
                        if($newP <=33)
                        {
                        $price = 33;   
                        }
                        else
                        {
                        $price = $newP;   
                        }
                    }

                }
 
            /* CLIENT COMMENT 
            // the price should be 33 SAR at minimum, "like add 5 SR to the the returned value -which is 28 SR
            // Pick (Size: 20 kilo) (24 SR) - (3 SR per additional kilo)
            // Aramex (Size: 15 kilo) (28 SR) - (2.5 SR per additional kilo)
            */

            /* ODL CODE
            $ch = curl_init();
            $siteUrl = url('/getaramexrates.php');
            curl_setopt($ch, CURLOPT_URL,$siteUrl);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "productid=$product_id&countryCode=$countryCode&city=$city&address=$address");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec($ch);
            curl_close ($ch); 

            */



            if($price >= 33)
            {
            $pOutput = $price;
            }
            else
            {
            $pOutput = 33;
            }
 

            

            $ChkShip = DB::table('nm_shipping')->where('cart_sub_type',$cart_sub_type)->where('cart_id',$cart->id)->where('shop_id',$shop_id)->where('ship_cus_id',$User->cus_id)->where('ship_order_id','=',0)->count();
            if($ChkShip >=1)
            {
            $ChkShip = DB::table('nm_shipping')->where('cart_sub_type',$cart_sub_type)->where('cart_id',$cart->id)->where('shop_id',$shop_id)->where('ship_cus_id',$User->cus_id)->where('ship_order_id','=',0)->delete();
            }





        DB::table('nm_shipping')->insert(array('cart_sub_type'=>$cart_sub_type,'cart_id'=>$cart->id,'shop_id'=>$shop_id,'ship_cus_id'=>$User->cus_id,'shipping_type'=>$shipping_type,'shipping_price'=>$pOutput));



        $response_array = array('data' => ['success' => true, 'code' => 200, 'status' => 1, 'price' => $pOutput]);
            }
            else
            {
              $response_array = array('data' => ['success' => true,'error_code' => 200, 'error' => 'Cart is empty']);  
            }
            $response = Response::json($response_array);
            } catch (ModelNotFoundException $e) {
            $response_array = array('data' => ['success' => false,'error_code' => 500, 'error' => 'Something went wrong']);
            $response = Response::json($response_array);
            }

}






 return $response;
}




public function chkCouponcode(Request $request)
        {
            try
            {
            $valid = 0; $already=0;$delte = 0; $coupnAmt=0;
            $CCode = $request->couopncode; 
            $del = $request->del; 
            
$lang = $request->language_type;
                $User = Auth::user();


                $cart = Cart::where('user_id',$User->cus_id)->first();
 
            $cart_pro = CartProduct::where('cart_id', '=', $cart->id)->get();
            $curr =  date("Y-m-d");

            foreach($cart_pro as $pro)
            {
            $shopVid = $pro->merchant_id;
            $productId = $pro->product_id;

            
            $carttype = $pro->cart_type;
            $cart_sub_type = $pro->cart_sub_type;


            $coountPro = Products::where('pro_id',$productId)->count();
            if($coountPro >=1)
            {
            $getProinfo = Products::where('pro_id',$productId)->first();
              $getMid = $getProinfo->pro_mc_id;  
               
              if(  $carttype=='clinic'  ){
                $allOffers = DB::table('nm_product_offer')->whereDate('start_date','<=', $curr)->whereDate('end_date', '>=', $curr)->where('coupon',$CCode)->where('vendor_id',$pro->merchant_id)->where('pro_id',$pro->category_id)->count();
               
              }elseif($carttype=='car_rental'  || $carttype=='music' || $carttype=='shopping' || $carttype=='occasion' || $carttype=='travel' || $carttype=='food'){
                $allOffers = DB::table('nm_product_offer')->whereDate('start_date','<=', $curr)->whereDate('end_date', '>=', $curr)->where('coupon',$CCode)->where('vendor_id',$pro->merchant_id)->where('pro_id',$pro->shop_id)->count();
               
              }elseif($cart_sub_type=='makeup_artists' || $cart_sub_type=='beauty_centers' || $cart_sub_type=='men_saloon' || $cart_sub_type=='spa'  || $cart_sub_type=='makeup'){
                    $allOffers = DB::table('nm_product_offer')->whereDate('start_date','<=', $curr)->whereDate('end_date', '>=', $curr)->where('coupon',$CCode)->where('vendor_id',$pro->merchant_id)->where('pro_id',$pro->shop_id)->count();
              }else{
            $allOffers = DB::table('nm_product_offer')->whereDate('start_date','<=', $curr)->whereDate('end_date', '>=', $curr)->where('coupon',$CCode)->where('vendor_id',$shopVid)->where('pro_id',$productId)->count();
           
                 } 
                       
                if($allOffers >=1)
                {
                $valid = 1;  
                if( $carttype=='clinic'){
                $getOffers = DB::table('nm_product_offer')->whereDate('start_date','<=', $curr)->whereDate('end_date', '>=', $curr)->where('coupon',$CCode)->where('vendor_id',$pro->merchant_id)->where('pro_id',$pro->category_id)->first();
              }elseif($carttype=='car_rental'    || $carttype=='music' || $carttype=='shopping' || $carttype=='occasion' || $carttype=='travel' || $carttype=='food'){
                $getOffers = DB::table('nm_product_offer')->whereDate('start_date','<=', $curr)->whereDate('end_date', '>=', $curr)->where('coupon',$CCode)->where('vendor_id',$pro->merchant_id)->where('pro_id',$pro->shop_id)->first();
              }elseif($cart_sub_type=='makeup_artists' || $cart_sub_type=='beauty_centers' || $cart_sub_type=='men_saloon' || $cart_sub_type=='spa'  || $cart_sub_type=='makeup'){
                    $getOffers = DB::table('nm_product_offer')->whereDate('start_date','<=', $curr)->whereDate('end_date', '>=', $curr)->where('coupon',$CCode)->where('vendor_id',$pro->merchant_id)->where('pro_id',$pro->shop_id)->first();
              }else{                
                $getOffers = DB::table('nm_product_offer')->whereDate('start_date','<=', $curr)->whereDate('end_date', '>=', $curr)->where('coupon',$CCode)->where('vendor_id',$shopVid)->where('pro_id',$productId)->first();
                } 

                
                $discount = $getOffers->discount;
                $start_date = $getOffers->start_date;
                $end_date = $getOffers->end_date;
                $total_price = $pro->total_price;
                $cart_type = $pro->cart_type;
                if($cart_type == 'hall')
                {
                   $total_price = $pro->paid_total_amount; 
                }
                                


                $quantity = $pro->quantity;
                if($quantity =='' || $quantity==NULL)
                {
                $quantity = 1;
                }
                $PerQtyAmt = $total_price / $quantity;
                $NewAmount = $PerQtyAmt - ($PerQtyAmt * $discount)/100;
                $AmtAccQty = $NewAmount * $quantity;

                 $NewAmounts = ($PerQtyAmt * $discount)/100;
                 $NewAmountsa =  $NewAmounts * $quantity;
                $getC = CartProduct::where('cart_id', '=', $cart->id)->where('shop_id',$getMid)->where('product_id',$productId)->where('coupon_code',$CCode)->count();
                if($getC >=1)
                { 
                    if($del ==1)
                    {
                    CartProduct::where('cart_id', '=', $cart->id)->where('coupon_code',$CCode)->where('shop_id',$getMid)->where('product_id',$productId)->update(['coupon_code_amount'=>'delete','price_after_coupon_applied'=>'']);
                    CartProduct::where('cart_id', '=', $cart->id)->where('coupon_code_amount','delete')->where('shop_id',$getMid)->where('product_id',$productId)->update(['coupon_code'=>'','price_after_coupon_applied'=>'']);

                    $delte = 1; 
                     }
                     else
                     {
                      $already = 1;  
                     }
                    
                }
                else
                {
                CartProduct::where('cart_id', '=', $cart->id)->where('shop_id',$getMid)->where('product_id',$productId)->update(['coupon_code'=>$CCode,'coupon_code_amount'=>$NewAmountsa,'price_after_coupon_applied'=>$AmtAccQty]);  

                $coupnAmt = $NewAmountsa + $coupnAmt;


                }
                }

            }
            }
 
            if($delte == 1)
            {
            if($lang=='ar'){ $msg='تم حذف رمز القسيمة بنجاح';}else{ $msg='Coupon code deleted successfully'; }
            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => $msg]);
            $response = Response::json($response_array);
 
            return $response;
            }
            elseif($valid == 1)
            {
            $getInfo =  CartProduct::where('cart_id', '=', $cart->id)->where('coupon_code',$CCode)->first();
            $cart_type = $getInfo->cart_type;
            $coupon_code_amount = $getInfo->coupon_code_amount;
            if($already==1){
            if($lang=='ar'){ $msg='رمز القسيمة مطبق بالفعل';}else{ $msg='Coupon code already applied'; }
            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => $msg]);
            $response = Response::json($response_array);
 
            return $response;
            }
            if($coupnAmt < 1)
            {




            $response_array = array('data' => ['success' => true, 'code' => 200, 'cart_type' => $cart_type, 'coupon_code' => $CCode,'price' => 0]);
            $response = Response::json($response_array);
 
            return $response;
 
            }
            else
            {

                $response_array = array('data' => ['success' => true, 'code' => 200, 'cart_type' => $cart_type, 'coupon_code' => $CCode,'price' => $coupnAmt]);
            $response = Response::json($response_array);
 
            return $response;


            }
           
             }
            else
            {
            if($lang=='ar'){ $msg='رقم قسيمه غير صالح';}else{ $msg='Invaild coupon code'; }
            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => $msg]);
            $response = Response::json($response_array);
 
            return $response;
 
            }
 
            }
            catch (ModelNotFoundException $e)
            {
                if($lang=='ar'){ $msg='هناك خطأ ما';}else{ $msg='Something went wrong'; }
            $response_array = array('data' => ['success' => false, 'code' => 500, 'message' => $msg]);
            $response = Response::json($response_array);
 
            return $response;

            }
        }

        public function getCouponlist(Request $request)
        {

            $User = Auth::user();
            $cart = Cart::where('user_id',$User->cus_id)->first();

            $getCartcouponC = CartProduct::where('cart_id', '=', $cart->id)->groupBy('coupon_code')->where('coupon_code','!=','')->count();
            if($getCartcouponC >=1)
            {
            $getCartcoupon = CartProduct::select('coupon_code','coupon_code_amount','price_after_coupon_applied','cart_sub_type','cart_type','product_id')->where('cart_id', '=', $cart->id)->groupBy('coupon_code')->where('coupon_code','!=','')->get();
            $response_array = array('data' => ['success' => true, 'code' => 200, 'coupons' => $getCartcoupon]);   
            }
            else
            {
            $response_array = array('data' => ['success' => true, 'code' => 200, 'coupons' => 'No record found']);
            }

            $response = Response::json($response_array);

            return $response;



        }


}
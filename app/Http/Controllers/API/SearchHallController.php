<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Models;
use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;
use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use Auth;
use App\User;
use Response;
use DB;
use App\HallOffer;

class SearchHallController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |   Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function __construct() {}

    /**Get hall's sub category with pagination**/

    public function searchHallSubCategory(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        $category_id = $input['category_id'];
        
        $catarray = array('37','87');
        if (in_array($category_id, $catarray)){

            $city_id = $input['city_id'];
        } else {
           $city_id =0;

        }
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();

$getHallCats = Category::where('mc_status', '=', 1)->where('parent_id', '=', $input['category_id'])->where('city_id',$city_id)->get();
$chklist = array();
foreach($getHallCats as $cats)
{

$getMc = $cats->mc_id;

$Proc = Products::where('pro_mc_id',$getMc)->where('pro_status',1)->count();
if($Proc >=1)
{
array_push($chklist, $getMc);
}
}




                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first(); //    GET MAIN CATEGORY

                if (!empty($category)) {

                    if ($input['category_id'] != '' && $input['language_type'] == 'ar') {

                        $hallsubcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $input['category_id'])->where('city_id',$city_id)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img','order_no')->orderBy('order_no','ASC')->paginate(9);

                        
                            if($input['category_id'] == 37){
                         //   $hallsubcategory->whereIn('mc_id',$chklist);
                            }

                        //$hallsubcategory-> //GET SUB CATEGORY LIST IN ARABIC



                        if (!$hallsubcategory->isEmpty()) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'تم العثور على البيانات', 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'status' => $category->mc_status], 'subcategory_list' => $hallsubcategory);

                        } else {
                            $response_array = array('data' => ['success' => false, 'message' => 'يبدو لا توجد خدمات متوفرة.', 'code' => 500], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar);
                        }

                    } else {

 

                        //GET SUB CATEGORY LIST IN ENGLISH
                        $hallsubcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $input['category_id'])->where('city_id',$city_id)->select('mc_id', 'mc_name', 'mc_img','order_no')->orderBy('order_no','ASC')->paginate(9);

                            if($input['category_id'] == 37){
                            //$hallsubcategory->whereIn('mc_id',$chklist);
                            }

                         //$hallsubcategory

                        if (!$hallsubcategory->isEmpty()) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found', 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'status' => $category->mc_status], 'subcategory_list' => $hallsubcategory);

                        } else {
                            $response_array = array('data' => ['success' => false, 'message' => 'it seems no services are available.', 'code' => 500], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name);
                        }

                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'Sorry! We could not find the service you looking for.']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Get hall's vendor list
    /*
        subcategory_id become parent_id for next level category table which represent vendor.
    */
    public function searchHallVendor(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'city_id' => 'required',
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first(); //GET PARENT CATEGORY

                if (!empty($category)) {

                    if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {
                        /*
                        FOR ARABIC
                        */
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name_ar')->first(); //GET SUB CATEGORY
                        $vendor = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_img', 'budget_in_percentages','city_id','mc_name as name','vendor_id')->where('mc_status', '=', 1)->Where('parent_id', '=', $input['subcategory_id'])->Where('city_id', '=', $input['city_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->orderBy('name', 'ASC')->paginate(9);
                        
                        if (!$vendor->isEmpty()) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'status' => $category->mc_status, 'message' => 'تم العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'vendor_hall_list' => $vendor);

                        } else {
                            $response_array = array('data' => ['success' => false, 'message' => 'لا يوجد بائع في مدينتك الفرعية', 'code' => 500], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar);
                        }

                    } else {
                        /*
                        FOR ENGLISH
                        */
                        //GET SUB CATEGORY LIST
                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name')->first();
                        //GET VENDOR LIST
                       
                            $vendor = Category::select('mc_id', 'mc_name', 'mc_img', 'budget_in_percentages','city_id','vendor_id')->where('mc_status', '=', 1)->Where('parent_id', '=', $input['subcategory_id'])->Where('city_id', '=', $input['city_id'])->with('getCity:ci_id,ci_name')->orderBy('mc_name', 'ASC')->paginate(9);
                        
                        if (!$vendor->isEmpty()) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'status' => $category->mc_status, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_hall_list' => $vendor);

                        } else {
                            $response_array = array('data' => ['success' => false, 'message' => 'No vendor exists in your branch city.', 'code' => 500], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name);
                        }

                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'Sorry! We could not find the service you looking for.']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    /**Get vendor's branch list**/
    /*
        vendor_id become parent_id for next level category table which represent vendor's branch.
    */
    public function searchVendorBranch(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'vendor_id' => 'required',
            'language_type' => 'required|max:255',
            'city_id' => 'required',
            'token' => 'required'

        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->first(); //GET MAIN CATEGORY

                if (!empty($category)) {

                    if ($input['subcategory_id'] != '' && $input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */

                        $city = City::select('ci_id', 'ci_name_ar as ci_name')->where('ci_id', '=', $input['city_id'])->first();
                        //Get City Name

                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name_ar', 'mc_img', 'budget_in_percentages')->first(); //GET SUB CATEGORY

                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name_ar', 'mc_img', 'budget_in_percentages','vendor_id')->first(); //GET VENDOR

                        $vendor_hall = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_img', 'budget_in_percentages', 'city_id','vendor_id')->where('mc_status', '=', 1)->where('parent_id', '=', $input['vendor_id'])->where('city_id', '=', $input['city_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->orderBy('mc_name', 'ASC')->paginate(9);
                        //GET VENDOR BRANCH LIST
                        if (!$vendor_hall->isEmpty()) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'تم العثور على البيانات'], 'city' => $city, 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name_ar, 'vendor_hall_branch_list' => $vendor_hall);
                        } else {
                            $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => 'معذرة! لم نتمكن من العثور علي ما بحثت عنه. حاول تغيير معايير البحث مره أخرى.'], 'city' => $city, 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name_ar);
                        }

                    } else {
                        /*FOR ENGLISH LANGUAGE*/
                        $city = City::select('ci_id', 'ci_name')->where('ci_id', '=', $input['city_id'])->first();
                        // get city                        

                        $sub_catg = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->select('mc_id', 'mc_name', 'mc_img', 'budget_in_percentages')->first(); //GET SUB CATEGORY
                        $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->select('mc_id', 'mc_name', 'mc_img', 'budget_in_percentages','vendor_id')->first(); //GET VENDOR

                        $vendor_hall = Category::select('mc_id', 'mc_name', 'mc_img', 'budget_in_percentages', 'city_id','vendor_id')->where('mc_status', '=', 1)->where('parent_id', '=', $input['vendor_id'])->where('city_id', '=', $input['city_id'])->with('getCity:ci_id,ci_name')->orderBy('mc_name', 'ASC')->paginate(9); //GET VENDOR BRANCH LIST
                        if (!$vendor_hall->isEmpty()) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'city' => $city, 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name, 'vendor_hall_branch_list' => $vendor_hall);
                        } else {
                            $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => 'Sorry! We could not find what you searched for.Try again changing the search criteria.'], 'city' => $city, 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name);
                        }

                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'Sorry! We could not find the service you looking for.']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    /**Get hall list**/
    public function getHallList(Request $request) {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required', // '1' for business category and '2' for occasion category
            'subcategory_id' => 'required', //subcategory of business/occasion category 
            'vendor_id' => 'required',
            'branch_id' => 'required',
            'number_of_guests' => 'required',
            'language_type' => 'required|max:255', // 'ar' for arabic language and 'en' for english language
            'hall_budget' => 'required',
            'hall_budget_type' => 'required', // '1' for within budget and '2' for above budget and '3' for offer
            'token' => 'required'


        ]);

        $input = $request->all();

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {
            try {
                $User = Auth::user();
                $gendercount = DB::table('nm_search_occasion')->where('user_id',$User->cus_id)->orderBy('id','DESC')->count();
                              
                    $gender = DB::table('nm_search_occasion')->where('user_id',$User->cus_id)->orderBy('id','DESC')->first();

 

                     
                    if(isset($input['gender']) && $input['gender']!='')
                    {
                    $halltype = strtolower($input['gender']);
                    }
                    else
                    {
                         $halltype = strtolower($gender->gender);
                    }
                                      
                                      

                  if($input['top_category']==2)
                  {
                    $category_id = 1;
                  }
                  else
                  {
                    $category_id = 2;
                  }
                                
                  
            $hall_category_type = $input['hall_budget_type'];


            if($hall_category_type ==3){  $hall_category_type = '1'; $hall_category_type1 = '2';}else { $hall_category_type1 = '0'; }

 
                  
                if($halltype =='both') {
 
               $hall_type = array('both');

                  } else if($halltype =='female') {
                   $hall_type = array('women');
                  }else  {
                     $hall_type = array('men');
                  } 

                
                $search_criteria = ['category_id' => $input['category_id'], 'subcategory_id' => $input['subcategory_id'], 'vendor_id' => $input['vendor_id'], 'branch_id' => $input['branch_id'], 'hall_budget' => $input['hall_budget'], 'number_of_guests' => $input['number_of_guests']];

                if ($input['language_type'] == 'ar') { //get data in arabic

                    $hotel = Category::where('mc_id', '=', $input['branch_id'])->select('mc_id', 'mc_name_ar as mc_name', 'mc_img','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name','vendor_id')->where('mc_status', '=', 1)->first();
                    //get hotel 
                    if (isset($hotel)) {
                        if ($input['hall_budget_type'] == 1) { //get halls of particulare hotel within budget
                            
                            // $within_price_hall = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_price', 'pro_disprice', 'pro_discount_percentage','hallcapicity','Insuranceamount','food_type')->where('pro_mc_id', '=', $input['branch_id'])->where('pro_price', '<=', $input['hall_budget'])->where('hallcapicity', '>=', $input['number_of_guests'])->whereRaw("find_in_set($hall_category_type,hall_category_type)")->whereIn('hall_type',$hall_type)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10000);


                              $within_price_hall = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_price','pro_disprice', 'pro_discount_percentage', 'hallcapicity','Insuranceamount','food_type')->where('pro_mc_id', '=', $input['branch_id'])->where('pro_price', '<=', $input['hall_budget'])->where('hallcapicity', '>=', $input['number_of_guests'])->where('pro_status', '=', 1)->whereIn('hall_type',$hall_type)->whereRaw("find_in_set($category_id,hall_category_type)")->orderBy('pro_title', 'ASC')->paginate(10000); 

 


                            if (!$within_price_hall->isEmpty()) {
                                $response_array = array('data' => ['success' => true, 'message' => 'تم العثور على البيانات', 'code' => 200], 'search_criteria' => $search_criteria, 'hotel' => $hotel, 'within_price_hall' => $within_price_hall);
                            } else {
                                $response_array = array('data' => ['success' => false, 'message' => 'لم نتمكن من إيجاد قاعة في ميزانيتك.', 'code' => 500], 'search_criteria' => $search_criteria, 'hotel' => $hotel);
                            }

                        } else if ($input['hall_budget_type'] == 2) { //get halls of particulare hotel above budget
                            $above_price_hall = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_price','pro_disprice', 'pro_discount_percentage', 'hallcapicity','Insuranceamount','food_type')->where('pro_mc_id', '=', $input['branch_id'])->where('pro_price', '>', $input['hall_budget'])->where('hallcapicity', '>=', $input['number_of_guests'])->whereRaw("find_in_set($category_id,hall_category_type)")->whereIn('hall_type',$hall_type)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10000);

                            if (!$above_price_hall->isEmpty()) {
                                $response_array = array('data' => ['success' => true, 'message' => 'تم العثور على البيانات', 'code' => 200], 'search_criteria' => $search_criteria, 'hotel' => $hotel, 'above_price_hall' => $above_price_hall);
                            } else {
                                $response_array = array('data' => ['success' => false, 'message' => 'لم نتمكن من العثور على قاعة فوق الميزانية.', 'code' => 500], 'search_criteria' => $search_criteria, 'hotel' => $hotel);
                            }

                        } else if ($input['hall_budget_type'] == 3) { //get halls of particulare hotel on offer


                            $todaydate = date('Y-m-d');
                            $chkOferHall = HallOffer::where('status',1)->where('start_date', '<=',$todaydate)->where('end_date', '>=',$todaydate)->get();
                            $hallArr = array();
                            foreach ($chkOferHall as $chkOferHallue) {
                            $pro_ida = $chkOferHallue->pro_id;
                            array_push($hallArr, $pro_ida);
                            }

  

                            $offer_price_hall = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_price','pro_disprice', 'pro_discount_percentage', 'services_offer_id', 'hallcapicity','Insuranceamount','food_type')->where('pro_mc_id', '=', $input['branch_id'])->whereIn('hall_type',$hall_type)->where('hallcapicity', '>=', $input['number_of_guests'])->with('getOfferDetail:id,start_date,end_date,price')->whereRaw("find_in_set($category_id,hall_category_type)")->where('pro_status', '=', 1)->whereIn('pro_id', $hallArr)->orderBy('pro_title', 'ASC')->paginate(10000);




                            if (!$offer_price_hall->isEmpty()) {
                                $response_array = array('data' => ['success' => true, 'message' => 'تم العثور على البيانات', 'code' => 200], 'search_criteria' => $search_criteria, 'hotel' => $hotel, 'offer_price_hall' => $offer_price_hall);
                            } else {
                                $response_array = array('data' => ['success' => false, 'message' => 'حاليا لا يوجد عرض متاح. التحقق لاحقا.', 'code' => 500], 'search_criteria' => $search_criteria, 'hotel' => $hotel);
                            }

                        }

                        $response = Response::json($response_array);

                    } else {

                        $response_array = array('data' => ['success' => false, 'error' => 'آسف! لم نتمكن من العثور على قاعة في المدينة المختارة', 'code' => 500]);
                        $response = Response::json($response_array, 500);

                    }

                } else { //get data in english
                    $hotel = Category::where('mc_id', '=', $input['branch_id'])->select('mc_id', 'mc_name', 'mc_img','vendor_id','terms_conditions','terms_condition_name')->where('mc_status', '=', 1)->first();

                    if (isset($hotel)) {
                        if ($input['hall_budget_type'] == 1) { 
                        //get halls of particulare hotel within budget
 
                    if(isset($input['test']) && $input['test'] == 1)
                    {

                    $within_price_hall = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_price','pro_disprice', 'pro_discount_percentage', 'hallcapicity','Insuranceamount','food_type')->where('pro_mc_id', '=', $input['branch_id'])->where('pro_price', '<=', $input['hall_budget'])->where('hallcapicity', '>=', $input['number_of_guests'])->where('pro_status', '=', 1)->whereIn('hall_type11',$hall_type)->whereRaw("find_in_set($category_id,hall_category_type)")->orderBy('pro_title', 'ASC')->paginate(10000); 


                    }
                    else
                    {

                    $within_price_hall = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_price','pro_disprice', 'pro_discount_percentage', 'hallcapicity','Insuranceamount','food_type')->where('pro_mc_id', '=', $input['branch_id'])->where('pro_price', '<=', $input['hall_budget'])->where('hallcapicity', '>=', $input['number_of_guests'])->where('pro_status', '=', 1)->whereIn('hall_type',$hall_type)->whereRaw("find_in_set($category_id,hall_category_type)")->orderBy('pro_title', 'ASC')->paginate(10000); 


                    }

                            if (!$within_price_hall->isEmpty()) {
                                $response_array = array('data' => ['success' => true, 'message' => 'Data found', 'code' => 200], 'search_criteria' => $search_criteria, 'hotel' => $hotel, 'within_price_hall' => $within_price_hall);
                            } else {
                                $response_array = array('data' => ['success' => false, 'message' => 'We could not find a hall in your budget.', 'code' => 500], 'search_criteria' => $search_criteria, 'hotel' => $hotel);
                            }

                        } else if ($input['hall_budget_type'] == 2) { //get halls of particulare hotel above budget
                           
  if(isset($input['test']) && $input['test'] == 1){
                            $above_price_hall = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_price','pro_disprice', 'pro_discount_percentage', 'hallcapicity','Insuranceamount','food_type')->where('pro_mc_id', '=', $input['branch_id'])->where('pro_price232', '>', $input['hall_budget'])->where('hallcapicity', '>=', $input['number_of_guests'])->where('pro_status', '=', 1)->whereIn('hall_type',$hall_type)->whereRaw("find_in_set($category_id,hall_category_type)")->orderBy('pro_title111', 'ASC')->paginate(10000); die;

}


$above_price_hall = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_price','pro_disprice', 'pro_discount_percentage', 'hallcapicity','Insuranceamount','food_type')->where('pro_mc_id', '=', $input['branch_id'])->where('pro_price', '>', $input['hall_budget'])->where('hallcapicity', '>=', $input['number_of_guests'])->where('pro_status', '=', 1)->whereIn('hall_type',$hall_type)->whereRaw("find_in_set($category_id,hall_category_type)")->orderBy('pro_title', 'ASC')->paginate(10000);






                            if (!$above_price_hall->isEmpty()) {
                                $response_array = array('data' => ['success' => true, 'message' => 'Data found', 'code' => 200], 'search_criteria' => $search_criteria, 'hotel' => $hotel, 'above_price_hall' => $above_price_hall);
                            } else {
                                $response_array = array('data' => ['success' => false, 'message' => 'We could not find a hall above the budget.', 'code' => 500], 'search_criteria' => $search_criteria, 'hotel' => $hotel);
                            }

                        } else if ($input['hall_budget_type'] == 3) { //get halls of particulare hotel on offer
                            $todaydate = date('Y-m-d');
                            $chkOferHall = HallOffer::where('status',1)->where('start_date', '<=',$todaydate)->where('end_date', '>=',$todaydate)->get();


                            $hallArr = array();
                            foreach ($chkOferHall as $chkOferHallue) {
                            $pro_ida = $chkOferHallue->pro_id;
                            array_push($hallArr, $pro_ida);
                            }

 
 
 
                            $offer_price_hall = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_price','pro_disprice', 'pro_discount_percentage', 'services_offer_id', 'hallcapicity','Insuranceamount','food_type')->where('pro_mc_id', '=', $input['branch_id'])->where('hallcapicity', '>=', $input['number_of_guests'])->whereIn('hall_type',$hall_type)->with('getOfferDetail:id,start_date,end_date,price')->whereRaw("find_in_set($category_id,hall_category_type)")->where('pro_status', '=', 1)->whereIn('pro_id', $hallArr)->orderBy('pro_title', 'ASC')->paginate(10000);

 

                            if (!$offer_price_hall->isEmpty()) {  
                                $response_array = array('data' => ['success' => true, 'message' => 'Data found', 'code' => 200], 'search_criteria' => $search_criteria, 'hotel' => $hotel, 'offer_price_hall' => $offer_price_hall);
                            } else {
                                $response_array = array('data' => ['success' => false, 'message' => 'Currently no offer available. Check later', 'code' => 500], 'search_criteria' => $search_criteria, 'hotel' => $hotel);
                            }

                        }
                        $response = Response::json($response_array);
                    } else {
                        $response_array = array('data' => ['success' => false, 'error' => 'Sorry! We could not find a hall in the selected city', 'code' => 500]);
                        $response = Response::json($response_array, 500);
                    }

                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }
        }

        return $response;
    }

    /**Get short hall details **/
    public function getShortHallDetail(Request $request) {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required', // '1' for business category and '2' for occasion category
            'subcategory_id' => 'required', //subcategory of business/occasion category 
            'vendor_id' => 'required',
            'branch_id' => 'required',
            'hall_id' => 'required', //selected hall id     
            'language_type' => 'required|max:255', // 'ar' for arabic language and 'en' for english language
            'hall_budget' => 'required',
            'token' => 'required'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $search_criteria = ['category_id' => $input['category_id'], 'subcategory_id' => $input['subcategory_id'], 'vendor_id' => $input['vendor_id'], 'branch_id' => $input['branch_id'], 'hall_budget' => $input['hall_budget'], 'hall_id' => $input['hall_id']];
                if ($input['language_type'] == 'ar') {
                    $hall = Products::select('pro_title_ar as pro_title', 'pro_price', 'pro_Img')->where('pro_id', '=', $input['hall_id'])->where('pro_status', '=', 1)->first();
                    //get detail of selected hall in arabic
                    if (isset($hall)) {
                        $response_array = array('data' => ['success' => true, 'message' => 'تم العثور على البيانات', 'code' => 200], 'search_criteria' => $search_criteria, 'hall' => $hall);
                    } else {
                        $response_array = array('data' => ['success' => false, 'error' => 'server not responding', 'code' => 500]);
                        $response = Response::json($response_array, 500);
                    }

                } else {
                    $hall = Products::select('pro_title', 'pro_price', 'pro_Img')->where('pro_id', '=', $input['hall_id'])->where('pro_status', '=', 1)->first();
                    //get detail of selected hall in english
                    if (isset($hall)) {
                        $response_array = array('data' => ['success' => true, 'message' => 'Data found', 'code' => 200], 'search_criteria' => $search_criteria, 'hall' => $hall);
                    } else {
                        $response_array = array('data' => ['success' => false, 'error' => 'Sorry! We could not find a hall in the selected city', 'code' => 500]);
                        $response = Response::json($response_array, 500);
                    }

                }

                $response_code = 200;
                $response = Response::json($response_array, $response_code);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }
        }
        return $response;
    }

    /**GET FULL HALL DETAIL **/
    public function getFullHallDetail(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'vendor_id' => 'required',
            'branch_id' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                $client_review = Products::get_product_review($input['product_id']); //GET PRODUCT REVIEW
 
            $occasiondates = DB::table('nm_search_occasion')->where('user_id',$User->cus_id)->orderBy('id','DESC')->first();

            $occasiondate = strtotime($occasiondates->occasion_date);  
            $occasiondateform1 = date("Y-m-d", strtotime($occasiondates->occasion_date));  
            $occasiondateform2 = date("d-m-Y", strtotime($occasiondates->occasion_date));  
            $occasiondateform3 = date("d M, Y", strtotime($occasiondates->occasion_date));  


            $heckAlreadyBooking = DB::table('nm_order_product')->where('product_type', '=', 'hall')
            ->where(function($q) use ($occasiondateform1,$occasiondateform2,$occasiondateform3) {
                            $q->where("hall_booking_date",$occasiondateform1)
                            ->orWhere("hall_booking_date",$occasiondateform2)
                            ->orWhere("hall_booking_date",$occasiondateform3);
                            })
            ->where('product_id', $input['product_id'])->count();

            if($heckAlreadyBooking >=1){ $heckAlreadyBooking =1; }

 
                $gallery = Gallery::select('image')->where('product_id', '=', $input['product_id'])->get(); //GET PRODUCT GALLERY
                $search_criteria = ['category_id' => $input['category_id'], 'subcategory_id' => $input['subcategory_id'], 'vendor_id' => $input['vendor_id'], 'branch_id' => $input['branch_id'], 'product_id' => $input['product_id']]; //SEARCH CRITERIA
               
                $hall = Products::where('pro_id', $input['product_id'])->first(); //Get Hall Detail
                $vendor = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['branch_id'])->select('mc_id', 'city_id', 'mc_img','vendor_id','terms_conditions','terms_condition_name','latitude','longitude')->with('getCity:ci_id,ci_name,ci_name_ar')->first(); //GET VENDOR Logo
                
                $vendor_branch_city = Category::where('mc_status', '=', 1)->where('mc_id', '=', $input['branch_id'])->select('mc_id', 'city_id','vendor_id','terms_conditions','terms_condition_name')->with('getCity:ci_id,ci_name,ci_name_ar')->first(); //GET VENDOR Branch City

                $name = explode(',', $hall->option_id);
                if (!empty($hall)) {

                    if ($input['product_id'] != '' && $input['language_type'] == 'ar') {
                        //FOR ARABIC LANGUAGE

                        $serAttribute = ProductAttribute::select('id', 'attribute_title_ar as attribute_title')->where('product_id', '=', $input['product_id'])->get(); //GET PRODUCT FACILITIES
                        $pro_Option = ProductOption::select('id', 'option_title_ar as option_title', 'button_type_id')->whereIn('id', $name)->with('getBoxType:id,option_title_ar as option_title,type')->get();
                         //GET PRODUCT SERVICES
                        foreach($pro_Option as $pro_opt) {
                            $Option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title_ar as option_title', 'image', 'price', 'experience', 'status')->where('product_id', '=', $input['product_id'])->where('product_option_id', '=', $pro_opt->id)->get();
                            $pro_opt->product_option_value = $Option_value;
                        }
                        $response_array = array('data' => ['success' => true, 'code' => 200, 'status' => $hall->pro_status, 'message' => 'تم العثور على البيانات', 'search_criteria' => $search_criteria,'vendor_id' => $vendor->vendor_id, 'vendor_logo' => $vendor->mc_img,'terms_conditions' => $vendor->terms_conditions_ar, 'latitude' => $vendor->latitude,'longitude' => $vendor->longitude, 'hall_image' => $hall->pro_Img, 'pro_disprice' => $hall->pro_disprice, 'video_url' => $hall->video_url, 'mc_video_description_ar as mc_video_description' => $hall->mc_video_description_ar, 'hotel_hall_name' => $hall->pro_title_ar, 'hall_information' => $hall->about_ar, 'hall_dimensions' => $hall->hall_dimension, 'hall_price' => $hall->pro_price, 'hall_capacity' => $hall->hallcapicity, 'Insuranceamount' => $hall->Insuranceamount,'hall_type' => $hall->hall_type,'food' => $hall->food_type, 'city_name' => $vendor_branch_city->getCity->ci_name_ar, 'booking_status' => $heckAlreadyBooking], 'hall_picture_list' => $gallery/*, 'facilities_list' => $serAttribute*/, 'prepaid_services' => $pro_Option, 'client_comment_list' => $client_review);
                    } else {
                        //FOR ENGLISH LANGUAGE

                        $serAttribute = ProductAttribute::select('id', 'attribute_title')->where('product_id', '=', $input['product_id'])->get(); // GET PRODUCT FACILITIES
                        $pro_Option = ProductOption::select('id', 'option_title', 'button_type_id')->whereIn('id', $name)->with('getBoxType:id,option_title,type')->get(); //GET PRODUCT SERVICES
                        foreach($pro_Option as $pro_opt) {
                            $Option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title', 'image', 'price', 'experience', 'status')->where('product_id', '=', $input['product_id'])->where('product_option_id', '=', $pro_opt->id)->get();
                            $pro_opt->product_option_value = $Option_value;
                        }
                        $response_array = array('data' => ['success' => true, 'code' => 200, 'status' => $hall->pro_status, 'message' => 'Data found', 'search_criteria' => $search_criteria,'vendor_id' => $vendor->vendor_id, 'vendor_logo' => $vendor->mc_img, 'terms_conditions' => $vendor->terms_conditions, 'latitude' => $vendor->latitude,'longitude' => $vendor->longitude,'hall_image' => $hall->pro_Img, 'pro_disprice' => $hall->pro_disprice,  'hotel_hall_name' => $hall->pro_title, 'hall_information' => $hall->about,'hall_dimensions' => $hall->hall_dimension, 'hall_price' => $hall->pro_price, 'hall_capacity' => $hall->hallcapicity, 'Insuranceamount' => $hall->Insuranceamount, 'hall_type' => $hall->hall_type,'video_url' => $hall->video_url, 'mc_video_description' => $hall->mc_video_description, 'food' => $hall->food_type, 'city_name' => $vendor_branch_city->getCity->ci_name, 'booking_status' => $heckAlreadyBooking], 'hall_picture_list' => $gallery/*, 'facilities_list' => $serAttribute*/, 'prepaid_services' => $pro_Option, 'client_comment_list' => $client_review);
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'Sorry! We could not find a hall in the selected city.']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    /**Get internal food menu **/
    public function getHallInternalMenu(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'vendor_id' => 'required',
            'branch_id' => 'required',
            'hall_id' => 'required',
            'language_type' => 'required|max:255',
            'hall_budget' => 'required',
            'token' => 'required'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input']);
            $response = Response::json($response_array, 200);
        } else {
            try {
                $User = Auth::user();
                $search_criteria = ['category_id' => $input['category_id'], 'subcategory_id' => $input['subcategory_id'], 'vendor_id' => $input['vendor_id'], 'branch_id' => $input['branch_id'], 'hall_budget' => $input['hall_budget']];
                if ($input['language_type'] == 'ar') {
                    // Get Menu details with Dish and container. 

                     $getfood = InternalFoodDish::select('id', 'relate_with', 'vendor_id', 'internal_food_menu_id', 'dish_name', 'dish_image','price','status')->where('status', '=', 1)->where('relate_with', '=', $input['hall_id'])->get();
                $IntCat =  array();
                foreach ($getfood as $ce) {
                $Ifood = $ce->internal_food_menu_id;
                array_push($IntCat, $Ifood);
                } 

                
                    $menu = InternalFoodMenu::select('id', 'pro_id', 'vendor_id', 'menu_name_ar as menu_name', 'menu_image', 'status')->where('pro_id', '=', $input['hall_id'])->whereIn('id',$IntCat)->get();

                    if (isset($menu)) {
                       foreach($menu as $key => $value) {
                            $food = InternalFoodDish::select('id', 'relate_with', 'vendor_id', 'internal_food_menu_id', 'dish_name_ar as dish_name', 'dish_image','price','status')->where('status', '=', 1)->where('relate_with', '=', $input['hall_id'])->where('internal_food_menu_id', '=', $value->id)->orderBy('dish_name', 'ASC')->paginate(9);
                            
                            $value->food_list = $food;
                            foreach($food as $val) {
                                //Get Container
                                $container = InternalFoodContainer::select('id', 'img', 'title','no_people', 'short_code', 'status','pro_id')->where('pro_id', '=',$input['hall_id'])->where('status', '=', 1)->orderBy('short_code', 'ASC')->get();
                                $val->container_list = $container;
                                foreach($container as $cont_value) {
                                    //Get Container Price
                                $container_price = InternalFoodContainerPrice::select('id', 'dish_id', 'container_id', 'container_price','discount_price','discount')->where('dish_id', '=', $val->id)->where('container_id', '=', $cont_value->id)->get(); 
                                $cont_value->container_price = $container_price;

                                }
                               
                            }

                        }

                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'تم العثور على البيانات'], 'search_criteria' => $search_criteria, 'menu' => $menu);
                    } else {
                        $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'يبدو لا تلبي الطعام من مالك القاعة.']);
                        $response = Response::json($response_array);
                    }

                } else {
                    // Get Menu details with Dish and container.


                $getfood = InternalFoodDish::select('id', 'relate_with', 'vendor_id', 'internal_food_menu_id', 'dish_name', 'dish_image','price','status')->where('status', '=', 1)->where('relate_with', '=', $input['hall_id'])->get();
                $IntCat =  array();
                foreach ($getfood as $ce) {
                $Ifood = $ce->internal_food_menu_id;
                array_push($IntCat, $Ifood);
                } 
                    $menu = InternalFoodMenu::select('id', 'pro_id', 'vendor_id', 'menu_name', 'menu_image', 'status')->where('pro_id', '=', $input['hall_id'])->whereIn('id',$IntCat)->get();



                    if (isset($menu)) {
                        foreach($menu as $key => $value) {
                            $food = InternalFoodDish::select('id', 'relate_with', 'vendor_id', 'internal_food_menu_id', 'dish_name', 'dish_image','price','status')->where('status', '=', 1)->where('relate_with', '=', $input['hall_id'])->where('internal_food_menu_id', '=', $value->id)->orderBy('dish_name', 'ASC')->paginate(9);
                            $value->food_list = $food;
                            foreach($food as $val) {
                                //Get Container
                                $container = InternalFoodContainer::select('id', 'img', 'title', 'no_people', 'short_code', 'status')->where('pro_id', '=',$input['hall_id'])->where('status', '=', 1)->orderBy('short_code', 'ASC')->get();
                                $val->container_list = $container;
                                foreach($container as $cont_value) {
                                    //Get Container Price
                                $container_price = InternalFoodContainerPrice::select('id', 'dish_id', 'container_id', 'container_price','discount_price','discount')->where('dish_id', '=', $val->id)->where('container_id', '=', $cont_value->id)->get(); 
                                $cont_value->container_price = $container_price;

                                }
                               
                            }

                        }
                        $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'search_criteria' => $search_criteria, 'menu' => $menu);
                    } else {
                        $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'It seems no food is catered by the hall owner.']);
                    }
                }

                $response = Response::json($response_array);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found']);
                $response = Response::json($response_array, 200);
            }
        }
        return $response;
    }

   

    //Hall Add To Cart
    public function HallAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'cart_type' => 'required',
            'total_price' => 'required'
        ]);
        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();

              //CHECK ALLREADY BOOKED HALL               
               $occasion_date = '';
              $cart = Cart::where('user_id', '=', $User->cus_id)->first();

             if($cart!='') {
            $c = CartProduct::where('cart_type', '=', 'hall')->where('product_id', $input['product_id'])->where('cart_id', '=', $cart->id)->count();
             } else {
                    $c = 0;

             }



            if($c >=1){
            $cart_pro = CartProduct::where('cart_type', '=', 'hall')->where('product_id', $input['product_id'])->where('cart_id', '=', $cart->id)->delete();
            $cart_option = CartOption::where('cart_type', '=', 'hall')->where('product_id', $input['product_id'])->where('cart_id', '=', $cart->id)->delete();

            $cart_option_value = CartOptionValue::where('cart_type', '=', 'hall')->where('product_id', $input['product_id'])->where('cart_id', '=', $cart->id)->delete();
            $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'hall')->where('product_id', $input['product_id'])->where('cart_id', '=', $cart->id)->delete();
            $cart_food_dish = CartInternalFoodDish::where('cart_id', '=', $cart->id)->where('product_id', $input['product_id'])->delete(); 
            }



              //$userid =  $cart->user_id;


              $getoccasiondatecount = DB::table('nm_search_occasion')->orderBy('id','DESC')->count();

               if($getoccasiondatecount > 0) {

              $getoccasiondate = DB::table('nm_search_occasion')->orderBy('id','DESC')->first();
               $occasion_date = $getoccasiondate->occasion_date;

               }
                $heckAlreadyBooking = DB::table('nm_cart_product')->where('cart_type', '=', 'hall')->where('hall_booking_date', '=', $occasion_date)->where('product_id', $input['product_id'])->count();



                    if($heckAlreadyBooking >=1) {

                     $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.CART_ALLREADY_EXISTS_HOTELS'), 'code' => 200]);
                    $response = Response::json($response_array, 200);

                    return $response;

                    } else {



             $productexternalcount = Products::where('pro_id', '=', $input['product_id'])->where('food_type','external')->count();
            if($productexternalcount > 0)  {
                $product = Products::where('pro_id', '=', $input['product_id'])->where('food_type','external')->first();
            $category = Category::select('mc_id','insurance_amount', 'mc_name', 'mc_name_ar','address','address_ar', 'mc_status','city_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();

               
              
              if (!empty($cart)) {
                   
                   /* $cart_pro = CartProduct::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->delete();
                    $cart_option = CartOption::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->delete();

                    $cart_option_value = CartOptionValue::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->delete();
                    $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->delete();
                    $cart_food_dish = CartInternalFoodDish::where('cart_id', '=', $cart->id)->delete();*/

                    
               
               }else{
                    $cart_data = array();
                    $cart_data['user_id'] = $User->cus_id;
                    $cart = Cart::create($cart_data); //cart entery
                }

                  

                $cart_product_data = array();              
                $cart_product_data['cart_id'] = $cart->id;
                $cart_product_data['cart_type'] = $input['cart_type'];
                $cart_product_data['cart_sub_type'] = 'hall';
                $cart_product_data['product_id'] = $input['product_id'];
                /*$cart_product_data['total_price'] = $product->pro_price +  $category->insurance_amount + $food_price + $option_value;*/

                $getHallprice  = Products::where('pro_id', '=', $input['product_id'])->first();

                $NetP = ($getHallprice->pro_netprice*75)/100;
                $TotalP = $input['total_price'] + $NetP;
                $cart_product_data['total_price'] =$TotalP;




                $cart_product_data['paid_total_amount'] = $input['total_price'];


                $cart_product_data['merchant_id'] = $category->vendor_id;
                $cart_product_data['shop_id'] = $category->mc_id;
                $cart_product_data['insurance_amount'] = $product->Insuranceamount;
                $cart_product_data['status'] = 1;
                $cart_product_data['shop_vendor_id'] = $category->mc_id;
                $cart_product_data['pro_title'] = $product->pro_title;
                $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                $cart_product_data['pro_desc'] = $product->pro_desc;
                $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                $cart_product_data['pro_Img'] = $product->pro_Img;
                $cart_product_data['pro_price'] = $product->pro_price;
                $cart_product_data['hall_booking_date'] = $occasion_date;


                $cart_product_data['category_id'] = $category->mc_id;
                $cart_product_data['review_type'] = 'shop';      
                
                $cart_product = CartProduct::create($cart_product_data); 


                 if (isset($input['product_option_value'])) {
                        $product_options = ProductOptionValue::select('product_option_id')->whereIn('id', $input['product_option_value'])->distinct()->get()->toArray();
                        $product_options_ids = array();
                        foreach($product_options as $key => $value) {
                            $product_options_ids[] = $value['product_option_id'];
                        }

                        foreach($product_options_ids as $key => $value) {
                            $cart_option_data = array();
                            $cart_option_data['cart_type'] = $input['cart_type'];
                            $cart_option_data['product_option_id'] = $value;
                            $cart_option_data['cart_id'] = $cart->id;
                            $cart_option_data['product_id'] = $input['product_id'];
                            CartOption::create($cart_option_data); //cart option entry
                        }

                        $product_options_value = ProductOptionValue::whereIn('id', $input['product_option_value'])->with('getProductOption')->get();

                        foreach($product_options_value as $value) {
                            $cart_option_value = array();
                            $cart_option_value['cart_type'] = $input['cart_type'];
                            $cart_option_value['cart_id'] = $cart->id;
                            $cart_option_value['product_id'] = $input['product_id'];
                            $cart_option_value['product_option_id'] = $value->product_option_id;
                            $cart_option_value['product_option_value_id'] = $value->id;
                            $cart_option_value['value'] = $value->price;
                            $cart_option_value['status'] = 1;
                            $cart_option_value['option_title'] = $value->getProductOption->option_title;
                            $cart_option_value['option_title_ar'] = $value->getProductOption->option_title_ar;
                            $cart_option_value['option_value_title'] = $value->option_title;
                            $cart_option_value['option_value_title_ar'] = $value->option_title_ar;
                            $cart_option_value['price'] = $value->price;
                            $cart_option_value['no_person'] = $value->no_person;
                           $cart_value = CartOptionValue::create($cart_option_value); //cart option value entry
                           
                        }
                    }

                $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);

                    return $response;

             
              }

            $product = Products::where('pro_id', '=', $input['product_id'])->where('food_type', '!=', 'external')->first();
                if (!empty($product)) {
                $category = Category::select('mc_id','insurance_amount', 'mc_name', 'mc_name_ar','address','address_ar', 'mc_status','city_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();
                
                 
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                 if (!empty($cart)) {
                   
                  /*  $cart_pro = CartProduct::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->delete();
                    $cart_option = CartOption::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->delete();

                    $cart_option_value = CartOptionValue::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->delete();
                    $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->delete();
                    $cart_food_dish = CartInternalFoodDish::where('cart_id', '=', $cart->id)->delete();*/

                    
                }else{
                    $cart_data = array();
                    $cart_data['user_id'] = $User->cus_id;
                    $cart = Cart::create($cart_data); //cart entery
                }

                 

                    
                    if (isset($input['product_option_value'])) {
                        $product_options = ProductOptionValue::select('product_option_id')->whereIn('id', $input['product_option_value'])->distinct()->get()->toArray();
                        $product_options_ids = array();
                        foreach($product_options as $key => $value) {
                            $product_options_ids[] = $value['product_option_id'];
                        }

                        foreach($product_options_ids as $key => $value) {
                            $cart_option_data = array();
                            $cart_option_data['cart_type'] = $input['cart_type'];
                            $cart_option_data['product_option_id'] = $value;
                            $cart_option_data['cart_id'] = $cart->id;
                            $cart_option_data['product_id'] = $input['product_id'];
                            CartOption::create($cart_option_data); //cart option entry
                        }

                        $product_options_value = ProductOptionValue::whereIn('id', $input['product_option_value'])->with('getProductOption')->get();

                        foreach($product_options_value as $value) {
                            $cart_option_value = array();
                            $cart_option_value['cart_type'] = $input['cart_type'];
                            $cart_option_value['cart_id'] = $cart->id;
                            $cart_option_value['product_id'] = $input['product_id'];
                            $cart_option_value['product_option_id'] = $value->product_option_id;
                            $cart_option_value['product_option_value_id'] = $value->id;
                            $cart_option_value['value'] = $value->price;
                            $cart_option_value['status'] = 1;
                            $cart_option_value['option_title'] = $value->getProductOption->option_title;
                            $cart_option_value['option_title_ar'] = $value->getProductOption->option_title_ar;
                            $cart_option_value['option_value_title'] = $value->option_title;
                            $cart_option_value['option_value_title_ar'] = $value->option_title_ar;
                            $cart_option_value['price'] = $value->price;
                            $cart_option_value['no_person'] = $value->no_person;
                           $cart_value = CartOptionValue::create($cart_option_value); //cart option value entry
                           
                        }
                    }


                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $input['cart_type'];
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['product_id'] = $input['product_id'];
                    $cart_attribute_data['attribute_id'] = '';
                    $cart_attribute_data['attribute_title'] = '';
                    $cart_attribute_data['attribute_title_ar'] = '';
                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry*/

                    if(isset($input['menu']['item'])){
                    foreach($input['menu']['item'] as $value){
                    $menu = InternalFoodMenu::select('id', 'pro_id', 'menu_name_ar','menu_name')->where('pro_id', '=', $input['product_id'])->where('id', '=', $value['menu_id'])->first();    
                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $input['cart_type'];
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['product_id'] = $input['product_id'];
                    $cart_attribute_data['attribute_id'] = $value['menu_id'];
                    $cart_attribute_data['attribute_title'] = $menu->menu_name;
                    $cart_attribute_data['attribute_title_ar'] = $menu->menu_name_ar;
                    //$cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry*/
                    
                 
                    foreach($value['dish'] as $key=> $val){ 
                       
                    $container_price = InternalFoodContainerPrice::where('dish_id','=', $val['dish_id'])->where('container_id','=',$val['container_id'])->first();

                    $container = InternalFoodContainer::select('id', 'title', 'no_people','pro_id','img')->where('pro_id', '=',$input['product_id'])->where('id', '=',$val['container_id'])->first();


                    $food = InternalFoodDish::select('id', 'internal_food_menu_id', 'dish_name', 'dish_image','dish_name_ar')->where('status', '=', 1)->where('relate_with', '=', $input['product_id'])->where('id', '=', $val['dish_id'])->first();

                    $cart_internal_food = array();
                    $cart_internal_food['cart_id'] = $cart->id;
                    $cart_internal_food['product_id'] = $input['product_id'];
                    $cart_internal_food['internal_food_menu_id'] = $value['menu_id'];
                    $cart_internal_food['internal_food_dish_id'] = $val['dish_id'];
                    $cart_internal_food['container_id'] = $val['container_id'];
                    $cart_internal_food['quantity'] = $val['qty'];
                    $cart_internal_food['price'] = $container_price->container_price * $val['qty'];
                    $cart_internal_food['status'] = 1;
                    $cart_internal_food['dish_name'] = $food->dish_name;
                    $cart_internal_food['dish_name_ar'] = $food->dish_name_ar;
                    $cart_internal_food['container_title'] = $container->title;
                    $cart_internal_food['img'] = $food->dish_image;
                    $cart_internal_food['no_people'] = $container->no_people;
                    $cart_internal_food['container_image'] = $container->img;
                    
                    $cart_value = CartInternalFoodDish::create($cart_internal_food); //internal food entery 


                    }                   
                    
                    }

                    }

                /*$cart_option_value = CartOptionValue::where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->get();
                $option_value = 0;
                foreach($cart_option_value as $key => $value) {
                    $option_value += $value->value;
                }  
                  
                $cart_food_price = CartInternalFoodDish::where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->get();
                $food_price = 0;
                foreach($cart_food_price as $key => $value) {
                    $food_price += $value->price;
                }*/
                
                $cart_product_data = array();              
                $cart_product_data['cart_id'] = $cart->id;
                $cart_product_data['cart_type'] = $input['cart_type'];
                $cart_product_data['cart_sub_type'] = 'hall';
                $cart_product_data['product_id'] = $input['product_id'];
                /*$cart_product_data['total_price'] = $product->pro_price +  $category->insurance_amount + $food_price + $option_value;*/
                //$cart_product_data['total_price'] = $input['total_price'];


 

                $getHallprice  = Products::where('pro_id', '=', $input['product_id'])->first();

                $NetP = ($getHallprice->pro_netprice*75)/100;
                $TotalP = $input['total_price'] + $NetP;
                $cart_product_data['total_price'] =$TotalP;

 

                $cart_product_data['paid_total_amount'] = $input['total_price'];



                $cart_product_data['merchant_id'] = $category->vendor_id;
                $cart_product_data['shop_id'] = $category->mc_id;
                $cart_product_data['insurance_amount'] = $product->Insuranceamount;
                $cart_product_data['status'] = 1;
                $cart_product_data['shop_vendor_id'] = $category->mc_id;
                $cart_product_data['pro_title'] = $product->pro_title;
                $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                $cart_product_data['pro_desc'] = $product->pro_desc;
                $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                $cart_product_data['pro_Img'] = $product->pro_Img;
                $cart_product_data['pro_price'] = $product->pro_price;
                $cart_product_data['hall_booking_date'] = $occasion_date;

                 $cart_product_data['category_id'] = $category->mc_id;
                $cart_product_data['review_type'] = 'shop';


                $cart_product = CartProduct::create($cart_product_data); //cart product entry
                    
                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                    

                
                    } else {
                        $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.CART_ERROR'), 'code' => 500]);
                        $response = Response::json($response_array, 500);
                    }

                   } 
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    /**Get to add to cart  data**/
    public function getHallAddToCart(Request $request) {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();

                $cart = Cart::where('user_id', '=', $User->cus_id)->first();



                $cart_pro = CartProduct::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->first();

                $hall = Products::select('pro_id','pro_mc_id')->where('pro_id', '=', $input['product_id'])->first();
               

                $branch_city = Category::select('mc_id','city_id','insurance_amount')->where('mc_id', '=', $hall->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();

                if (!empty($cart) && $cart_pro->cart_type = 'hall') {
                    $cart_option_value = CartOptionValue::where('cart_id', '=', $cart->id)->where('cart_type', '=', 'hall')->where('product_id', '=', $input['product_id'])->with('getProductOption:id,vendor_id,services_id,option_title,option_title_ar,service_hour')->with('getProductOptionValue:id,vandor_id,product_id,product_option_id,option_title,option_title_ar,short_name,value,image,price,no_person,status')->get();



                    $internal_food = CartInternalFoodDish::where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->with('getServiceAttribute')->with('getFood')->with('getContainer')->get();
                   
                    $hall = CartProduct::where('cart_id', '=', $cart->id)->where('cart_type', '=', 'hall')->where('product_id', '=', $input['product_id'])->with('getHall')->get();
                    $cart_service = array();
                    $internal_foods = array();
                    $hall_data = array();
                    if (isset($cart_option_value)) { //get service
                        foreach($cart_option_value as $key => $value) {
                            
                            if ($input['language_type'] == 'en') {
                                $cart_service[$key]['option_title'] = $value->getProductOption->option_title;

                                 if(isset($value->getProductOptionValue[0]->option_title)){
                                $cart_service[$key]['value_title'] = $value->getProductOptionValue[0]->option_title;
                               }else {
                                  $cart_service[$key]['value_title'] ='';
                               }



                            } else {
                                $cart_service[$key]['option_title'] = $value->getProductOption->option_title_ar;
                                if(isset($value->getProductOptionValue[0]->option_title_ar)){
                                $cart_service[$key]['value_title'] = $value->getProductOptionValue[0]->option_title_ar;
                            } else {
                                  $cart_service[$key]['value_title'] = '';

                            }
                            }
                             if(isset($value->getProductOptionValue[0]->price)){
                            $cart_service[$key]['price'] = $value->getProductOptionValue[0]->price;
                            } else {
                                 $cart_service[$key]['price'] = '';

                            }
                             if(isset($value->getProductOptionValue[0]->no_person)){
                            $cart_service[$key]['no_person'] = $value->getProductOptionValue[0]->no_person;
                          } else {
                               $cart_service[$key]['no_person'] ='';

                          } 
                            if(isset($value->getProductOptionValue[0]->status)){
                            $cart_service[$key]['status'] = $value->getProductOptionValue[0]->status;
                        } else {

                              $cart_service[$key]['status'] ='';
                        }
                        }
                    }
                    if (isset($internal_food)) {


                        foreach($internal_food as $key => $value) { //get internal food
                        $container_price = InternalFoodContainerPrice::where('dish_id', '=', $value->internal_food_dish_id)->where('container_id','=',$value->container_id)->first();



                            if ($input['language_type'] == 'en') {
                                if(isset($value->getServiceAttribute[0]->attribute_title)) {
                                $internal_foods[$key]['menu_name'] = $value->getServiceAttribute[0]->attribute_title;
                                 } else {
                                    $internal_foods[$key]['menu_name'] ='';

                                 }
                                  if(isset($value->getFood[0]->dish_name)) {
                                $internal_foods[$key]['dish_name'] = $value->getFood[0]->dish_name;
                                  } else {
                                        $internal_foods[$key]['dish_name'] ='';

                                  }
                                  if(isset($value->getContainer[0]->title)) { 
                                $internal_foods[$key]['container_title'] = $value->getContainer[0]->title;
                            } else {

                                 $internal_foods[$key]['container_title'] ='';
                            }
                            } else {
                                if(isset($value->getServiceAttribute[0]->attribute_title_ar)) { 
                                $internal_foods[$key]['menu_name'] = $value->getServiceAttribute[0]->attribute_title_ar;
                            } else {
                                    $internal_foods[$key]['menu_name'] ='';

                            }
                                if(isset($value->getFood[0]->dish_name_ar)) { 
                                $internal_foods[$key]['dish_name'] = $value->getFood[0]->dish_name_ar;
                            } else {
                                $internal_foods[$key]['dish_name'] ='';

                            }
                               // $internal_foods[$key]['container_title'] = $value->getContainer[0]->title_ar;
                            
                                $internal_foods[$key]['container_title'] = $value->getContainer[0]->title;
                            }
                            $internal_foods[$key]['dish_id'] = $value->getFood[0]->id;
                            $internal_foods[$key]['quantity'] = $internal_food[$key]->quantity;


                            $Disprice = $container_price->discount_price;
                            if($Disprice!='' && $Disprice!='0'){ 
                            $internal_foods[$key]['container_price'] = $Disprice; 
                            $Cprice = $Disprice; 

                            }else{
                            $internal_foods[$key]['container_price'] = $container_price->container_price;
                            $Cprice = $container_price->container_price;

                            }

                            //$internal_foods[$key]['container_price'] = $container_price->container_price;
                           
                            $internal_foods[$key]['dish_image'] = $value->getFood[0]->dish_image;
                            $internal_foods[$key]['status'] = $value->getFood[0]->status;

                            $internal_foods[$key]['no_people'] = $value->getContainer[0]->no_people;
                           if(!empty($container_price)) {

                            $internal_foods[$key]['final_price'] = $Cprice * $internal_food[$key]->quantity;

                           } 
                            
                        }
                    }

                    if (isset($hall)) {
                        foreach($hall as $key => $value) { //get hall
                            if ($input['language_type'] == 'en') {
                                $hall_data[$key]['pro_title'] = $value->getHall[0]->pro_title;
                                $hall_data[$key]['pro_desc'] = $value->getHall[0]->pro_desc;
                                $hall_data[$key]['city'] = $branch_city->getCity->ci_name;
                            } else {
                                $hall_data[$key]['pro_title'] = $value->getHall[0]->pro_title_ar;
                                $hall_data[$key]['pro_desc'] = $value->getHall[0]->pro_desc_ar;
                                $hall_data[$key]['city'] = $branch_city->getCity->ci_name_ar;
                            }
                            $hall_data[$key]['Insuranceamount'] = $value->getHall[0]->Insuranceamount;

                            $hall_data[$key]['price'] = $value->getHall[0]->pro_price;

                            $hall_data[$key]['pro_disprice'] = $value->getHall[0]->pro_disprice;

                            $hall_data[$key]['total_price'] = $cart_pro->total_price;
                            $hall_data[$key]['pro_qty'] = $value->getHall[0]->pro_qty;
                            $hall_data[$key]['pro_Img'] = $value->getHall[0]->pro_Img;
                            $hall_data[$key]['pro_status'] = $value->getHall[0]->pro_status;
                        }
                    }
                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'cart_id' => $cart->id, 'service', 'service' => $cart_service, 'internal_food' => $internal_foods, 'hall_data' => $hall_data);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'cart data not found & cart type must be hall', 'code' => 500]);
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

     /**delete to add to cart  data**/
    public function deleteAddToCartData(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'token' => 'required',
            'cart_type' => 'required'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();

                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                $cart_pro = CartProduct::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->first();
                if (!empty($cart) && !empty($cart_pro)) {
                    
                    $cart_option = CartOption::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete(); //delete product option 

                    $cart_option_value = CartOptionValue::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();//delete product option value
                    $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    //delete service attribute
                    $cart_food_dish = CartInternalFoodDish::where('cart_id', '=', $cart->id)->delete();
                    //delete internal food dish

                    $cart_pro = CartProduct::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    //delete product

                   $response_array = array('data' => ['success' => true, 'message' => 'Successfully deleted your cart data', 'code' => 200]);

                    
                }
               

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

}
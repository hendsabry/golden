<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Models;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon;
use File;
use App\Reviewrating;
use App\Categorylist;
use Intervention\Image\ImageManagerStatic as Image; 
use Auth;
use Lang;
use  App\City;
use App\NmServicesAttribute;
use App\ProductsMerchant;
use App\HallOffer;
use App\NmProductGallery;
 

class MerchantMakeupController extends Controller
{
      public function __construct(){
            parent::__construct();
            // set admin Panel language
            $this->middleware(function ($request, $next) {
                        $this->setLanguageLocaleMerchant();
                        Session::get('mer_lang_code');
                        return $next($request);
           });
     }
    
 
//Add Shop info

// Start For Makeup Shop Info
    public function makeupShopInfo(Request $request)
     {
        if (Session::has('merchantid')) 
             {
               
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                 $itemid = $request->itemid;
                 $fetchdata = array();
                $city = City::where('ci_con_id',10)->get();
                $getshopid = '';
                $fetchdatacount = Categorylist::where('mc_id',$itemid)->where('vendor_id',$mer_id)->count();
                if($fetchdatacount >0) {
               $fetchdata = Categorylist::where('mc_id',$itemid)->where('vendor_id',$mer_id)->first();
               //$getshopid = $fetchdata->mc_id;
              }

               
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getPagelimit = config('app.paginate');
                
                return view('sitemerchant.makeup.makeup-shop-info', compact('merchantheader','merchantfooter','id','itemid','fetchdata','city'));   

            } else {
                 return Redirect::to('sitemerchant');
            }


     }
// End For Makeup Shop Info

  //Store shop Info
            public function StoremakeupShopInfo(Request $request)

                {
                    $merid  = Session::get('merchantid');
                    $parent_id = $request->parent_id;
                    $itemid = $request->itemid;
                    if($itemid){
                        $savebranch =Categorylist::where('mc_id',$itemid)->first();
                    } else { 
                        $savebranch = new Categorylist;
                   }
                if($request->file('branchimage')){ 
                      $file = $request->branchimage;  
                      $merid  = Session::get('merchantid');
                      $extension = $file->getClientOriginalExtension();
                      $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                      $imageRealPath  =   $file->getRealPath();
                      $thumbName      =   'thumb_'. $Image;       
                      $img = Image::make($imageRealPath); // use this if you want facade style code
                      $thumb_width = 220;
                      list($width,$height) = getimagesize($imageRealPath);
                       $thumb_height = ($thumb_width/$width) * $height;
                       $img->resize($thumb_width,$thumb_height);
                      $img->save('makeup' . '/'. $thumbName);
                      $file->move('makeup/', $Image);
                      $savebranch->mc_img = url('').'/makeup/'.$thumbName;
                }
                //Addressimage
                if($request->file('address_image')){ 
                      $file = $request->address_image;  
                      $merid  = Session::get('merchantid');
                      $extension = $file->getClientOriginalExtension();
                      $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                      $imageRealPath  =   $file->getRealPath();
                      $thumbName      =   'thumb_'. $Image;       
                      $img = Image::make($imageRealPath); // use this if you want facade style code
                      $thumb_width = 150;
                        list($width,$height) = getimagesize($imageRealPath);
                       $thumb_height = ($thumb_width/$width) * $height;
                       $img->resize($thumb_width,$thumb_height);
                      $img->save('makeup' . '/'. $thumbName);
                      $file->move('makeup/', $Image);
                      $savebranch->address_image = url('').'/makeup/'.$thumbName;
                }
              //Termaand condition
               if(!empty($request->mc_tnc)){ 
                      $file = $request->mc_tnc;
                      $orgname =  $file->getClientOriginalName();
                      $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                      $extension = $file->getClientOriginalExtension();
                      $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                      $file->move('./public/assets/storeimage/', $filename_c);                      
                      $Cname =   url('').'/public/assets/storeimage/'.$filename_c;
                      $savebranch->terms_conditions =$Cname;
                  
                } else {
                    $orgname = $request->tmcvalue; 
                }

                   //Termaand condition arabic
                if(!empty($request->mc_tnc_ar)){ 
                      $file = $request->mc_tnc_ar;
                      $orgnamear =  $file->getClientOriginalName();
                      $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                      $extension = $file->getClientOriginalExtension();
                      $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                      $file->move('./public/assets/storeimage/', $filename_c);                      
                      $Cnamear =   url('').'/public/assets/storeimage/'.$filename_c;
                      $savebranch->terms_conditions_ar =$Cnamear;
                  
                } else {
                    $orgnamear = $request->tmcvaluear; 
                }
              


                  $savebranch ->latitude = $request->latitude;
                  $savebranch ->longitude = $request->longitude;
 
                      $savebranch ->mc_name = $request->mc_name;
                      $savebranch ->mc_name_ar = $request->mc_name_ar;
                      $savebranch ->vendor_id = $merid;
                      $savebranch ->google_map_address = $request->google_map_address;
                      $savebranch ->address = $request->address;
                      $savebranch ->address_ar = $request->address_ar;
                      $savebranch ->city_id = $request->city_id;
                      $savebranch ->parent_id = $parent_id;
                      $savebranch ->mc_discription = $request->about;
                      $savebranch ->mc_discription_ar = $request->about_ar;
                      $savebranch ->city_id = $request->city_id;
                      $savebranch ->terms_condition_name = $orgname;
                      $savebranch ->terms_condition_name_ar = $orgnamear;

                 // echo $serviceabailable;die;
                     $savebranch ->mc_status = $request->mc_status;
                   if($savebranch->save()){
                      $id = $request->parent_id;

                    if($itemid==''){
                     $itemid = $savebranch->mc_id;
                    }
                    if (\Config::get('app.locale') == 'ar'){
                        Session::flash('message', "تم حفظ معلومات المتجر بنجاح");
                     }
                     else
                     {
                        Session::flash('message', "Shop information successfully saved");
                     }

                         return redirect()->route('makeup-shop-info',['id' => $id,'itmemid' =>$itemid]);
                        }   
                    }

      //End shop info






// Start For Makeup Picture Video
    public function makeupPictureVideo(Request $request)
     {
        
     if (Session::has('merchantid')) 
                        {
                            $merid  = Session::get('merchantid');
                            $parentid = $request->id;
                            $itemid = $request->itemid;

                            $this->setLanguageLocaleMerchant();
                            $mer_id              = Session::get('merchantid');             
                            $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                            $merchantfooter     = view('sitemerchant.includes.merchant_footer');           
                            
                            $getDbC = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->count();
                            
                            $getVideos = DB::table('nm_category')->where('mc_id',$itemid)->where('vendor_id',$merid)->first();
                            if($getDbC >=1)
                            {
                          
                            $getDb = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->get();

                            }
                            else
                            {
                            $getDb = '';    
                            }

                          return view('sitemerchant.makeup.makeup-picture-video', compact('merchantheader','merchantfooter','getCatlists','GetAlreadyServices','getDb','getDbC','getVideos','parentid','itemid'));       
                        } else {
                            return Redirect::to('sitemerchant');
                    }

           }
// End For Makeup Picture Video  
  //Store picture and  video url
      public function StoreMakeupPictureVideourl(Request $request)
        {
         if (Session::has('merchantid')) 
                {

                    $validatedData = $request->validate([
                          //'image' => 'required',
                         //'youtubevideo' => 'required',
                        // 'image.*' => 'image|mimes:jpg,jpeg,png,gif'
 
                    ]); 

 
                    $this->setLanguageLocaleMerchant();
                    $mer_id              = Session::get('merchantid');  
                    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              
                    //Insert images in folder
                          $catid = $request->parent_id;
                          $sid = $request->itemid;


                    if($files=$request->file('image')){
                    foreach($files as $file){
                    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
                    //$file->move('hallpics',$name); 
                    $extension = $file->getClientOriginalExtension();
                    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
                    //$file->move('hallpics', $fileName);               
                    $imageRealPath  =   $file->getRealPath();
                    $thumbName      =   'thumb_'. $fileName;       
                    $img = Image::make($imageRealPath); // use this if you want facade style code
                     $thumb_width = 100;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);

                  
                    $img->save('makeup' . '/'. $thumbName);
                    $file->move('makeup/', $fileName);
                    $shop_Img = url('/').'/makeup/'.$thumbName; 
                       
                    /*Insert data to db*/
                    DB::table('nm_category_gallery')->insert( [
                    'image'=>   $shop_Img,
                    'category_id' =>$sid,
                    'vendor_id' => $mer_id,
                    ]);
                   }  

                 } 
                   
                    $youtubevideoa = str_replace('youtu.be','www.youtube.com/embed',$request->youtubevideo);
                    $mc_video_description = trim($request->about_video);
                    $mc_video_description_ar = trim($request->about_video_ar);


                    /*Update data to db*/
  DB::table('nm_category')->where('mc_id',$sid)->where('vendor_id',$mer_id)->update( ['mc_video_url'=>$youtubevideoa,'mc_video_description'=> $mc_video_description,'mc_video_description_ar'=> $mc_video_description_ar]);



 

                      // language for display message //
                    if($mc_video_description != '' || $mc_video_description_ar != '' || $youtubevideoa != '' || $request->file('image') !='')
                      {
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تمت إضافة الخدمة");
                    }
                    else
                    {
                    Session::flash('message', "Record saved successfully");
                    }
                  }
                    // language for display message //  




                    return Redirect::back();      
                } else {
                        return Redirect::to('sitemerchant');
                }
      }







// Start For Makeup product
    public function makeupProduct(Request $request)
     {
        
     if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $itemid = $request->itemid;
                $catname = $request->catid;
                $status = $request->status;
                $search = $request->search;
                $getPagelimit = config('app.paginate');
                  $productdata        = ProductsMerchant::where('pro_mc_id',$itemid)->where('pro_mr_id',$mer_id);
                if($catname != '')
                {
                $productdata = $productdata->where('attribute_id',$catname); 
                }
                if($status != '')
                {
                $productdata = $productdata->where('pro_status',$status);
                }
                if($search != '')
                {
                 $mer_selected_lang_code = Session::get('mer_lang_code');   
                 if($mer_selected_lang_code !='en')
                  {   
                $productdata = $productdata->where('pro_title_ar','like', '%'.$search.'%');
                 }
                  $productdata = $productdata->where('pro_title','like', '%'.$search.'%');
                }

                $productdata = $productdata->orderBy('pro_id','desc')->paginate($getPagelimit)->appends(request()->query());
                 $attrcat            = NmServicesAttribute::where('vendor_id',$mer_id)->where('services_id',$itemid)->get();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
 
                return view('sitemerchant.makeup.makeup-product', compact('merchantheader','merchantfooter','id','productdata','status','search','attrcat','catname'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
// End For Makeup product

// Start For Makeup Add product
    public function makeupAddProduct(Request $request)
     {
        
     if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $itemid = $request->itemid;
                $autoid = $request->autoid;
                $fetchfirstdata = ProductsMerchant::where('pro_id',$autoid)->where('pro_mr_id',$mer_id)->first();

                 $productcat =  NmServicesAttribute::where('vendor_id',$mer_id)->where('services_id',$itemid )->get();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $productGallery = NmProductGallery::where('vendor_id',$mer_id)->where('product_id',$autoid)->get();
                 return view('sitemerchant.makeup.makeup-add-product', compact('merchantheader','merchantfooter','id','itemid','productcat','fetchfirstdata','autoid','productGallery'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
// End For Makeup Add product

//Store makeup product
    public function StoreMakeupProduct(Request $request)

    { 
 
        $merid  = Session::get('merchantid');
                $id = request()->id;
                $itemid = $request->itemid;
                $autoid = $request->autoid;
                
              if($autoid){
                  $saveproduct =ProductsMerchant::where('pro_id',$autoid)->first();
                } else { 
                 $saveproduct = new ProductsMerchant;
               }
           if($request->file('proimage')){ 
                  $file = $request->proimage;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                 $img = Image::make($imageRealPath); // use this if you want facade style code
                   $thumb_width = 272;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('makeup' . '/'. $thumbName);
                  $file->move('makeup/', $Image);
                  $saveproduct->pro_Img = url('').'/makeup/'.$thumbName;
                }
                  $saveproduct ->pro_title = $request->mc_name;
                  $saveproduct ->pro_title_ar = $request->mc_name_ar;
                  $saveproduct ->pro_mr_id = $merid;
                  $saveproduct ->attribute_id = $request->category;
                  $saveproduct ->pro_mc_id = $itemid;
                  $saveproduct ->pro_desc = $request->description;
                  $saveproduct ->pro_desc_ar = $request->description_ar;
                  $saveproduct ->pro_price = $request->price;
                  $saveproduct ->pro_qty = $request->quantity;
                  $saveproduct ->pro_discount_percentage = $request->discount;

                  $price =  $request->price;
                  $discount = $request->discount;

                  if($discount!='' && $discount!='0') {
                     $discountprice =   $price-($price * ($discount/100));
                    } else {
                       $discountprice = 0;

                    }

                   $saveproduct ->pro_disprice = $discountprice;  

                  $saveproduct->pro_status  = 1;
                if($saveproduct->save()){



    $getProID = $saveproduct->pro_id;

// FOR PRODUCT MULTIPLE IMAGES UPLOAD //
    $files=$request->file('image');
    $newFiles = array();
    $merid  = Session::get('merchantid');
    if($files=$request->file('image')){
    foreach($files as $key=>$val){
    array_push($newFiles, $key);
    }

    $privius=$request->privius;
    $newFilesMatch = array();
    if(isset($privius) && $privius!='')
    {
    foreach($privius as $key=>$val){
    array_push($newFilesMatch, $key);
    }  
    }

    $CheckInfo = array_intersect($newFiles, $newFilesMatch);
    if(isset($privius) && $privius!='')
    {
    foreach($privius as $key=>$val){
    if(in_array($key, $CheckInfo))
    {
    DB::table('nm_product_gallery')->where('id',$val)->where('vendor_id',$merid)->delete(); 
    }
    }
    } 
    foreach($files as $file){
    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
    $extension = $file->getClientOriginalExtension();
    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
    $imageRealPath  =   $file->getRealPath();
    $thumbName      =   'thumb_'. $fileName;       
    $img = Image::make($imageRealPath); // use this if you want facade style code
    $thumb_width = 272;
    list($width,$height) = getimagesize($imageRealPath);
    $thumb_height = ($thumb_width/$width) * $height;
    $img->resize($thumb_width,$thumb_height);                  
    $img->save('makeup' . '/'. $thumbName);
    $file->move('makeup/', $fileName);
    $shop_Img = url('/').'/makeup/'.$thumbName; 
    /*Insert data to db*/
    DB::table('nm_product_gallery')->insert( [
    'image'=>   $shop_Img,
    'product_id' =>$getProID,
    'status' => 1,
    'vendor_id' => $merid,
    ]);
    }  
        } 


 
// FOR PRODUCT MULTIPLE IMAGES UPLOAD END //






                  if (\Config::get('app.locale') == 'ar'){
                   Session::flash('message', "تم حفظ المنتج بنجاح");
                   }
                   else
                   {
                   Session::flash('message', "Product successfully saved");
                   }

                 return redirect()->route('makeup-product',['id' => $id,'itmemid' =>$itemid]);
                }      
 
     }







// Start For Makeup Order
    public function makeupOrder(Request $request)
    {
       if(Session::has('merchantid')) 
       {
        $mer_id         = Session::get('merchantid');
        $id             = $request->id;
        $hid            = $request->itemid;
        $sid            = $request->sid;
        $searchkeyword  = $request->searchkeyword;
        $date_to        = $request->date_to;
        $End_Date       = $request->from_to;
        $order_days     = $request->order_days;
        $status         = $request->status;                
        $serachfirstfrm = $request->serachfirstfrm;
        $getorderedproducts = DB::table('nm_order_product')->where('product_type','beauty')->where('product_sub_type','makeup');
        if($searchkeyword!='')
        {
          $getorderedproducts = $getorderedproducts->where('order_id',$searchkeyword);
        }
        if($status!='')
        {
          $getorderedproducts = $getorderedproducts->where('status',$status);
        }
        if($date_to!='' && $End_Date!='')
        {
          $getorderedproducts = $getorderedproducts->whereDate('created_at','>=',$date_to)->whereDate('created_at','<=',$End_Date);
        }
        $getorderedproducts = $getorderedproducts->where('merchant_id',$mer_id)->groupBy('order_id')->orderBy('created_at','DESC')->selectRaw('*,sum(nm_order_product.total_price) as sum')->get();
        $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
        $merchantfooter     = view('sitemerchant.includes.merchant_footer');
        return view('sitemerchant.makeup.makeup-order', compact('merchantheader','merchantfooter','id','hid','sid','getorderedproducts','mer_id'));       
      } 
      else 
      {
        return Redirect::to('sitemerchant');
      }
    }

    public function getorderdetail(Request $request)
    {
      if(Session::has('merchantid')) 
      {
         $mer_id         = Session::get('merchantid');
         $id             = $request->id;
         $opid           = $request->opid;
         $oid            = $request->oid;
         $cusid          = $request->cusid;
         $hid            = $request->hid;
         $itemid         = $request->itemid;
         $merchantheader = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
         $merchantfooter = view('sitemerchant.includes.merchant_footer');     
         $productdata = DB::table('nm_order_product')->where('product_type','beauty')->where('order_id',$oid)->where('product_sub_type','makeup')->where('cus_id',$cusid)->where('order_id',$oid)->orderBy('created_at','DESC')->get();

   foreach($productdata as $value)
         {            

          $getShippingC = DB::table('nm_shipping')->where('ship_order_id',$oid)->where('cart_sub_type','makeup')->count();
          $value->shippingMethod = 'N/A';
          $value->shippingPrice = '0';
          if($getShippingC >=1)
          { 
          $getShipping = DB::table('nm_shipping')->where('ship_order_id',$oid)->where('cart_sub_type','makeup')->first();
          $shipping_type = $getShipping->shipping_type;
          if($shipping_type == 1){ $shipping_type = 'Aramex';}else { $shipping_type = 'Pick Logistics'; }
          $shipping_price =  $getShipping->shipping_price;
          $value->shippingMethod = $shipping_type;
          $value->shippingPrice = $shipping_price;
          }
         }




         return view('sitemerchant.makeup.makeup-orderdetail', compact('merchantheader','merchantfooter','id','opid','oid','cusid','hid','itemid','productdata'));       
      } 
      else 
      {
        return Redirect::to('sitemerchant');
      }
  }
// End For Makeup Order 
 
// Start For Makeup Offer
    public function makeupOffer(Request $request)
     {
           if (Session::has('merchantid')) 
             {
                
               if (Session::has('merchantid')) 
             {   $getPagelimit = config('app.paginate');
                  $id = $request->id;
                  $sid = $request->itemid;
                  $autoid = $request->autoid;
                  $status = $request->status;
                  $search = $request->search;

             $mer_id   = Session::get('merchantid');
            $hallofferlist =  HallOffer::where('vendor_id',$mer_id)->where('pro_id',$sid);
            if($status!='')
            {
            $hallofferlist = $hallofferlist->where('status',$status);
            }
            if($search !='')
            {
            $mer_selected_lang_code = Session::get('mer_lang_code'); 
            if($mer_selected_lang_code !='en')
            {
            $hallofferlist = $hallofferlist->where('title_ar','LIKE','%' .$search.'%');
            }
            else
            {
            $hallofferlist = $hallofferlist->where('title','LIKE','%' .$search.'%');
 
            }
            }   
            $hallofferlist =  $hallofferlist->orderBy('id','DESC')->paginate($getPagelimit);

                $this->setLanguageLocaleMerchant();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
            return view('sitemerchant.makeup.makeup-offer',compact('merchantheader','merchantfooter','hallofferlist','status','autoid','sid','id','search'));       
            } else {
                return Redirect::to('sitemerchant');
            }
        }
     }
// End For Makeup Offer    


// Start For Makeup Add Offer
    public function makeupAddOffer(Request $request)
     {
           
           if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $getPagelimit = config('app.paginate');
                $sid = $request->itemid;
                $autoid = $request->autoid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
               $reviewrating = Reviewrating::where('vandor_id',$mer_id)->paginate($getPagelimit)->appends(request()->query());
                $categorysave = HallOffer::where('id',$autoid)->where('vendor_id',$mer_id)->first(); 
 
                return view('sitemerchant.makeup.makeup-add-offer', compact('merchantheader','merchantfooter','id','sid','autoid','reviewrating','categorysave'));       
            } else {
                 return Redirect::to('sitemerchant');
            }

        }
// End For Makeup Add Offer 
    //STORE OFFER
       public function storeMakeOffer(Request $request)
       {
            if (Session::has('merchantid')) 
        {      
        $mer_id   = Session::get('merchantid');
        $id = $request->parentid;
        $sid = $request->sid;
        $autoid = $request->autoid;
        if($autoid =='')
        {
        $categorysave = new HallOffer; 
        }
        else
        {
        $categorysave = HallOffer::where('id',$autoid)->where('vendor_id',$mer_id)->first();
        }
        $categorysave->vendor_id    = $mer_id; 
        $categorysave->title        =$request->title;
        $categorysave->title_ar    = $request->title_ar;
        $categorysave->start_date     = $request->Date_from;
        $categorysave->end_date    = $request->date_to;
        $categorysave->discount   = $request->discount;
        $categorysave->coupon   = $request->coupon;
        $categorysave->pro_id   = $sid;
        $categorysave->status    = 1;
        if (\Config::get('app.locale') == 'ar'){
        Session::flash('message', "تمت إضافة العرض بنجاح  ");
        }
        else
        {
        Session::flash('message', "Offer successfully saved");
        }
        $categorysave->save(); 
        return redirect()->route('makeup-offer',['id' => $id,'itemid' => $sid]);
       } else {
      return Redirect::to('sitemerchant');
      } 
       }
     //END OFFER  









// Start For Makeup Reviews & Comments
    public function makeupReviewsComments(Request $request)
     {
           if(Session::has('merchantid')) 
           {   
                $getPagelimit    = config('app.paginate');
                $mer_id          = Session::get('merchantid');
                $id              = $request->id;
                $itemid          = $request->itemid;
                $merchantheader  = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter  = view('sitemerchant.includes.merchant_footer'); 
                $reviewrating    = Reviewrating::where('vandor_id',$mer_id)->where('shop_id',$itemid)->where('status',1)->orderBy('created_at','desc')->paginate($getPagelimit)->appends(request()->query());
 
                return view('sitemerchant.makeup.makeup-reviews-comments', compact('merchantheader','merchantfooter','id','reviewrating'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
// End For Makeup Reviews & Comments  

// Start For Makeup Service Category      
    public function makeupServiceCategory(Request $request)
     {
          if (Session::has('merchantid')) 
             {
                $parent_id = request()->id;
                $sid = request()->itemid;
                $status = $request->status;
                $search = $request->search;
                $getPagelimit = config('app.paginate');
                $mer_id   = Session::get('merchantid');


$menulist =  NmServicesAttribute::where('vendor_id',$mer_id)->where('services_id',$sid );

              if($status!='')
              {
              $menulist = $menulist->where('status',$status);
              }


             if($search !='')
              {      
              $mc_name='attribute_title';
              $mer_selected_lang_code = Session::get('mer_lang_code'); 
              if($mer_selected_lang_code !='en')
              {

              $menulist = $menulist->where('attribute_title_ar','LIKE','%' .$search.'%');
              }

              $menulist = $menulist->where('attribute_title','LIKE','%' .$search.'%');
              }



$menulist = $menulist->orderBy('id', 'desc')->paginate($getPagelimit)->appends(request()->query());


                $this->setLanguageLocaleMerchant();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
            return view('sitemerchant.makeup.makeup-service-category', compact('merchantheader','merchantfooter','menulist','status','search','sid','parent_id','itemid'));       
            } else {
                return Redirect::to('sitemerchant');
            }  
     }
// End For Makeup Service Category  

// Start For Makeup Service Add Category      
    public function makeupServiceAddCategory(Request $request)
     {
          
          if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $sid = $request->itemid;
                 $autoid = $request->autoid;
                 $datafirst = NmServicesAttribute::where('vendor_id',$mer_id)->where('id',$autoid)->first();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.makeup.makeup-service-add-category', compact('merchantheader','merchantfooter','id','getCities','getSinger','sid','autoid','datafirst'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
           
     }
  // End For Makeup Service Add Category  

     //Store category
       public function storeMakeUpCategory(Request $request)

        {
             $mer_id   = Session::get('merchantid');
             $parent_id = $request->parent_id;
             $sid = $request->sid;
             $autoid = $request->autoid;
             if($autoid!=''){
             $categorysave = NmServicesAttribute::where('id',$autoid)->first();
             } else {  
             $categorysave = new NmServicesAttribute; 
             }

 

  if($request->file('address_image')){ 
                  $file = $request->address_image;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                 $img = Image::make($imageRealPath); // use this if you want facade style code
                   $thumb_width = 272;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('makeup' . '/'. $thumbName);
                  $file->move('makeup/', $Image);
                  $categorysave->image = url('').'/makeup/'.$thumbName;
                }
 
             $categorysave->attribute_title  = $request->category_name;
             $categorysave->attribute_title_ar   = $request->category_name_ar;
             $categorysave->vendor_id   = $mer_id;
             $categorysave->services_id   = $sid;
             $categorysave->parent   = $parent_id;
             $categorysave->status  = 1;
             if($categorysave->save()){ 
              // language for display message //
             if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "تم حفظ الفئة بنجاح");
             }
             else
             {
             Session::flash('message', "Category successfully saved");
             }
     // language for display message //
             
            return redirect()->route('makeup-service-category',['id' =>$parent_id,'itemid' =>$sid]);
             //return redirect()->route('menulist',['hid' => $hid,'bid' =>$bid]);
             }


        }

   //End Category 
     //End category



  public function denyquote(Request $request)
  {
   if(isset($request->id) && $request->id !='')
   {
    $id = $request->id;
    DB::table('nm_services_inquiry')->where('id',$id)->update(['status'=>5]);
   }
    $subject = 'Singer Pricing Quote is Successfully Denied ! Golden Cage'; 
    $status = 'Denied';
    $languagetype ='';
    Mail::send('emails.vendor-reply_deny',
    array(
    'username' => 'ajit',
    'languagetype' => $languagetype,
    'status' => $status
    ), function($message) use($subject)
    {
      $message->to('ajit@wisitech.com','Golden Cage')->subject($subject);
    }); 
   echo 1;
  }

}

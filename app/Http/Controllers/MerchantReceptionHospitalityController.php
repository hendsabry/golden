<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use Session;

use App\Http\Models;

use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Validator;

use Carbon;

use File;

use App\Reviewrating;

use App\Categorylist;

use App\City;

use Intervention\Image\ImageManagerStatic as Image; 

use Auth;

use Lang;

use App\Country;

use App\NmProductGallery;

 



class MerchantReceptionHospitalityController extends Controller

{

      public function __construct(){

            parent::__construct();

            // set admin Panel language

            $this->middleware(function ($request, $next) {

                        $this->setLanguageLocaleMerchant();

                        Session::get('mer_lang_code');

                        return $next($request);

           });

     }

    

 

//Add Shop info

    

    public function receptionAddShopinfo(Request $request)

     {

        if (Session::has('merchantid')) 

             {

                $mer_id   = Session::get('merchantid');

                $city = City::where('ci_status',1)->get();

                $id = $request->id;

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

                $merchantfooter     = view('sitemerchant.includes.merchant_footer');







$storeShopinfoC = Categorylist::where('vendor_id',$mer_id)->where('parent_id',$id)->count();



if($storeShopinfoC >=1)

{



$storeShopinfo = Categorylist::where('vendor_id',$mer_id)->where('parent_id',$id)->first();



    $shopid =  $storeShopinfo->mc_id; 





return redirect()->route('reception-hospitality',['id' => $id,'shopid' => $shopid]); 

}

                







           return view('sitemerchant.reception-add-shopinfo', compact('merchantheader','merchantfooter','id','city'));       

            } else {

                return Redirect::to('sitemerchant');

            }

     }

   

   

   





 //Shop Ajax



    public function receptionShopStatus(Request $request)

    {

     if ($request->ajax()) {

             $id = $request->id;

             $activestatus = $request->activestatus;

             if($activestatus==1) {

                $status = 0;

             } else {

               $status = 1;

             }

             $updatestatus = Categorylist::where('mc_id',$id)->first();

             $updatestatus->mc_status = $status;

              if($updatestatus->save()) {

                 echo 1;die;

              }  



         }

   }  

//Shop list

  public function receptionShopList(Request $request)



  {

            if (Session::has('merchantid')) 

             {



                $mer_id   = Session::get('merchantid');

                $id = $request->id;

                $fetchdata = '';

                $search = $request->search;

                $status = $request->status;

                 $getPagelimit = config('app.paginate');

                 if($search !='')

                {

                     $mc_name='mc_name';

                     $mer_selected_lang_code = Session::get('mer_lang_code'); 

                   if($mer_selected_lang_code !='en')

                   {

                             $mc_name= 'mc_name_ar';

                            

                   }



                  $alldata =   Categorylist::where('vendor_id',$mer_id)->where('nm_category.'.$mc_name,'like', '%' .$search. '%')->where('parent_id',$id)->orderBy('mc_id','DESC')->paginate($getPagelimit)->appends(request()->query()); 

                }

                 else if($status !='')

                {

                $alldata = Categorylist::where('vendor_id',$mer_id)->where('mc_status',$status)->where('parent_id',$id)->orderBy('mc_id','DESC')->paginate($getPagelimit)->appends(request()->query()); ;



                } else {

                       $alldata = Categorylist::where('vendor_id',$mer_id)->where('parent_id',$id)->orderBy('mc_id','DESC')->paginate($getPagelimit)->appends(request()->query()); ;



                }



                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 



if(count($alldata) >=1)

{  

    $ShopId = $alldata[0]->mc_id;

  return redirect()->route('reception-hospitality',['id' => $id,'shopid' => $ShopId]); 

 }

else

{  $ShopId = '';

  return redirect()->route('reception-hospitality',['id' => $id,'shopid' => $ShopId]);

}

              



                  

            } else {

                return Redirect::to('sitemerchant');

            }





  }

     



      public function getreceptionorderdetail(Request $request)

      {

         if(Session::has('merchantid')) 

         {

             $mer_id   = Session::get('merchantid');

             $id       = $request->id;

             $sid       = $request->shopid;

             $opid     = $request->opid;

             $oid      = $request->oid;

             $cusid    = $request->cusid;

             $prodid      = $request->prodid;

             

        

             $merchantheader = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

             $merchantfooter = view('sitemerchant.includes.merchant_footer');  



             $gethospitality = DB::table('nm_order_product')->where('product_type','occasion')->where('order_id',$oid)->where('product_sub_type','hospitality')->where('cus_id',$cusid)->where('order_id',$oid)->orderBy('created_at','DESC')->get();

               

             return view('sitemerchant.reception_hospitality.reception-orderdetail', compact('merchantheader','merchantfooter','id','sid','opid','oid','cusid','prodid','gethospitality'));       

            } 

            else 

            {

                 return Redirect::to('sitemerchant');

            }

  }





    //Reception Sho Info

     public function receptionHospitality(Request $request)



     {

           if (Session::has('merchantid')) 

             {

                $mer_id   = Session::get('merchantid');

                 $parent_id = request()->id;

                 $mcid = request()->shopid;

                 $city = City::where('ci_status',1)->get();

                 

                $fetchdata = '';

                 $fetchdatacount = Categorylist::where('vendor_id',$mer_id)->where('parent_id',$parent_id)->where('mc_id',$mcid)->count();

                 if($fetchdatacount > 0) {

                 $fetchdata = Categorylist::where('vendor_id',$mer_id)->where('parent_id',$parent_id)->where('mc_id',$mcid)->first();

                 }



                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 

           return view('sitemerchant.reception-shop-info', compact('merchantheader','merchantfooter','parent_id','fetchdata','city'));       

            } else {

                return Redirect::to('sitemerchant');

            }





     }



     public function storeReceptionHospitality(Request $request)

     {

           if (Session::has('merchantid')) 

             {

                

              

                $id = request()->id;

                $mer_id   = Session::get('merchantid');

                $storeShopinfo =  new Categorylist;

                $storeShopinfo->mc_name = $request->mc_name;

                $storeShopinfo->mc_name_ar = $request->mc_name_ar;

                $storeShopinfo->google_map_address = $request->google_map_address;

                $storeShopinfo->vendor_id = $mer_id;

                $storeShopinfo->city_id = $request->city_id;

                $storeShopinfo->address = $request->address;



                $storeShopinfo->parent_id = $id;

                $storeShopinfo->mc_status = 1;



                if(!empty($request->mc_tnc)){ 

            $file = $request->mc_tnc;

            $TNCname =   $file->getClientOriginalName();  

            $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                

            $extension = $file->getClientOriginalExtension();

            $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       

            $file->move('./receptionspic/', $filename_c);                      

            $Tname =   url('').'/receptionspic/'.$filename_c;



            $storeShopinfo->terms_condition_name = $TNCname;





            }

            else{

            $Tname = $request->updatemc_tnc;

             

            } 



             $storeShopinfo->terms_conditions = $Tname;

             

                //Insert images in folder

                   if($file=$request->file('mc_img'))

                    {          

                        $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());          

                        $extension = $file->getClientOriginalExtension();

                        $fileName = rand(11111,99999).'_'.time().'.'.$extension;                 

                        $imageRealPath  =   $file->getRealPath();

                        $thumbName      =   'thumb_'. $fileName;       

                        $img = Image::make($imageRealPath);

                      



                    $thumb_width = 150;

                    list($width,$height) = getimagesize($imageRealPath);

                    $thumb_height = ($thumb_width/$width) * $height;

                    $img->resize($thumb_width,$thumb_height);



                        $img->save('receptionspic' . '/'. $thumbName);

                        $file->move('receptionspic/', $fileName);            

                        $shopimage_Img = url('/').'/receptionspic/'.$thumbName; 

                        $storeShopinfo->mc_img = $shopimage_Img;       

                     }



                     //Address image

                      //Insert images in folder

                   if($file=$request->file('address_image'))

                    {          

                        $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());          

                        $extension = $file->getClientOriginalExtension();

                        $fileName = rand(11111,99999).'_'.time().'.'.$extension;                 

                        $imageRealPath  =   $file->getRealPath();

                        $thumbName      =   'thumb_'. $fileName;       

                        $img = Image::make($imageRealPath);

                        

                                $thumb_width = 150;

                    list($width,$height) = getimagesize($imageRealPath);

                    $thumb_height = ($thumb_width/$width) * $height;

                    $img->resize($thumb_width,$thumb_height);





                        $img->save('receptionspic' . '/'. $thumbName);

                        $file->move('receptionspic/', $fileName);            

                        $address_Img = url('/').'/receptionspic/'.$thumbName; 

                        $storeShopinfo->address_image = $address_Img;          

                     }



                 $Shopinfo = $storeShopinfo->save();

             

             $shopid =  $storeShopinfo->mc_id; 

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 

                // language for display message //

                if (\Config::get('app.locale') == 'ar'){

                Session::flash('message', "تمت إضافة المتجر بنجاح");

                }

                else

                {

                Session::flash('message', "Shop successfully added");

                }



          // language for display message //   

            return redirect()->route('reception-hospitality',['id' => $id,'shopid' => $shopid]); 

         //  return view('sitemerchant.reception-shop-info', compact('merchantheader','merchantfooter','id'));       

            } else {

                return Redirect::to('sitemerchant');

            }

 

     }



     

     public function updateShopInfo(Request $request)



     {

            

          



                if (Session::has('merchantid')) 

                {



                $parent_id = request()->id;

                $mcid = $request->shopid;

                $mer_id   = Session::get('merchantid');

                if($mcid=='')

                {

                $storeShopinfo =  new Categorylist; 

                }

                else

                {

                $storeShopinfo = Categorylist::where('vendor_id',$mer_id)->where('parent_id',$parent_id)->where('mc_id',$mcid)->first(); 



                }







                  $storeShopinfo ->longitude =  $request->longitude;

                  $storeShopinfo ->latitude =  $request->latitude;



                $storeShopinfo->mc_name = $request->mc_name;

                $storeShopinfo->mc_name_ar = $request->mc_name_ar;

                $storeShopinfo->google_map_address = $request->google_map_address;

                $storeShopinfo->vendor_id = $mer_id;

                $storeShopinfo->parent_id = $parent_id;



                $storeShopinfo->city_id = $request->city_id;

                



                 $storeShopinfo ->mc_status = $request->mc_status;

                $storeShopinfo->address = $request->address;

                $storeShopinfo->address_ar = $request->address_ar;

                $storeShopinfo->mc_discription = $request->about;

                $storeShopinfo->mc_discription_ar = $request->about_ar;



                    









                if(!empty($request->mc_tnc)){ 



                $file = $request->mc_tnc;

                $TNCname =   $file->getClientOriginalName();  



                $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                

                $extension = $file->getClientOriginalExtension();

                $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       

                $file->move('./receptionspic/', $filename_c);                      

                $Tname =   url('').'/receptionspic/'.$filename_c;



                $storeShopinfo->terms_condition_name = $TNCname;



                }

                else{

                $Tname = $request->updatemc_tnc;



                } 







if(!empty($request->mc_tnc_ar)){ 



                $file = $request->mc_tnc_ar;

                $TNCname_ar =   $file->getClientOriginalName();  



                $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                

                $extension = $file->getClientOriginalExtension();

                $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       

                $file->move('./receptionspic/', $filename_c);                      

                $Tname_ar =   url('').'/receptionspic/'.$filename_c;



                $storeShopinfo->terms_condition_name_ar = $TNCname_ar;



                }

                else{

                $Tname_ar = $request->updatemc_tnc_ar;



                } 



                $storeShopinfo->terms_conditions_ar = $Tname_ar;

                $storeShopinfo->terms_conditions = $Tname;



                //Insert images in folder

                if($file=$request->file('mc_img'))

                {          

                $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());          

                $extension = $file->getClientOriginalExtension();

                $fileName = rand(11111,99999).'_'.time().'.'.$extension;                 

                $imageRealPath  =   $file->getRealPath();

                $thumbName      =   'thumb_'. $fileName;       

                $img = Image::make($imageRealPath);





                $thumb_width = 150;

                list($width,$height) = getimagesize($imageRealPath);

                $thumb_height = ($thumb_width/$width) * $height;

                $img->resize($thumb_width,$thumb_height);



                $img->save('receptionspic' . '/'. $thumbName);

                $file->move('receptionspic/', $fileName);            

                $shopimage_Img = url('/').'/receptionspic/'.$thumbName; 

                $storeShopinfo->mc_img = $shopimage_Img;       

                } else {

                $storeShopinfo->mc_img = $request->updateimage;

                }



                //Address image

                //Insert images in folder

                if($file=$request->file('address_image'))

                {          

                $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());          

                $extension = $file->getClientOriginalExtension();

                $fileName = rand(11111,99999).'_'.time().'.'.$extension;                 

                $imageRealPath  =   $file->getRealPath();

                $thumbName      =   'thumb_'. $fileName;       

                $img = Image::make($imageRealPath);







                $thumb_width = 150;

                list($width,$height) = getimagesize($imageRealPath);

                $thumb_height = ($thumb_width/$width) * $height;

                $img->resize($thumb_width,$thumb_height);







                $img->save('receptionspic' . '/'. $thumbName);

                $file->move('receptionspic/', $fileName);            

                $address_Img = url('/').'/receptionspic/'.$thumbName; 

                $storeShopinfo->address_image = $address_Img;          

                } else {



                $storeShopinfo->address_image = $request->update_add_img;

                }



                $storeShopinfo->save();



                if($mcid=='')

                {

                $shopid = $storeShopinfo->mc_id;

                }

                else

                {

                $shopid = $mcid; 

                }





                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 

                // language for display message //

                if (\Config::get('app.locale') == 'ar'){

                Session::flash('message', "متجر تم تحديثه بنجاح");

                }

                else

                {

                Session::flash('message', "Shop successfully Updated");

                }

return redirect()->route('reception-hospitality',['id' => $parent_id,'shopid' => $shopid]); 



          // language for display message //  

          //  return redirect()->route('reception-shop-list',['id' => $parent_id]); 

         //  return view('sitemerchant.reception-shop-info', compact('merchantheader','merchantfooter','id'));       

            } else {

                return Redirect::to('sitemerchant');

            }  



     }













           //Shop picture and Video Url

           public function receptionShopPicture(Request $request)



             { 



               if (Session::has('merchantid')) 

                        {

                            $merid  = Session::get('merchantid');

                            $catid = $request->id;

                             $shopid = $request->shopid; 





                            // CheckPoint for security by Himanshu //

                            $checkPoints = DB::table('nm_category')->where('mc_id',$shopid)->where('parent_id',$catid)->where('vendor_id',$merid)->count();

                            if($checkPoints < 1)

                            {

                            return Redirect::to('sitemerchant_dashboard');

                            }

                            // CheckPoint for security by Himanshu //





                            $this->setLanguageLocaleMerchant();

                            $mer_id              = Session::get('merchantid');             

                            $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

                            $merchantfooter     = view('sitemerchant.includes.merchant_footer');           

                           

                            $proid = $request->id;

                            $getDbC = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $shopid)->count();

                            

                            $getVideos = DB::table('nm_category')->where('mc_id',$shopid)->where('vendor_id',$merid)->first();

                            if($getDbC >=1)

                            {

                            $getDb = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $shopid)->get(); 

                            $getDb = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $shopid)->get();





                            }

                            else

                            {

                            $getDb = '';    

                            }



                          return view('sitemerchant.reception-shop-picture', compact('merchantheader','merchantfooter','getCatlists','GetAlreadyServices','getDb','getDbC','getVideos','shopid','catid'));       

                        } else {

                            return Redirect::to('sitemerchant');

                    }

             }





      //Store Hall Picture



        public function add_Reception_picture(Request $request)

    {



    if (Session::has('merchantid')) 

                {



                   

 



                    $merid  = Session::get('merchantid');

                    $this->setLanguageLocaleMerchant();

                    $mer_id              = Session::get('merchantid');             

                    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

                    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 

              

                    //Insert images in folder

                 

                            $catid = $request->cat_id;

                            $shopid = $request->pro_id;

                           

                     if(!$request->file('image') && trim($request->youtubevideo)=='' &&   trim($request->about_video)==''){

                $validatedData = $request->validate([

                'youtubevideo' => 'required', 

                ]);

                }







                    if($files=$request->file('image')){

                    foreach($files as $file){

                    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 

                    //$file->move('hallpics',$name); 

                    $extension = $file->getClientOriginalExtension();

                    $fileName = rand(11111,99999).'_'.time().'.'.$extension;

                    //$file->move('hallpics', $fileName);               

                    $imageRealPath  =   $file->getRealPath();

                    $thumbName      =   'thumb_'. $fileName;       

                    $img = Image::make($imageRealPath); // use this if you want facade style code

                  



                    $thumb_width = 100;

                    list($width,$height) = getimagesize($imageRealPath);

                    $thumb_height = ($thumb_width/$width) * $height;

                    $img->resize($thumb_width,$thumb_height);



                    $img->save('receptionspic' . '/'. $thumbName);

                    $file->move('receptionspic/', $fileName);

                    $shop_Img = url('/').'/receptionspic/'.$thumbName; 

                        

                        /*Insert data to db*/

                    DB::table('nm_category_gallery')->insert( [

                    'image'=>   $shop_Img,

                    'category_id' =>$shopid,

                    'vendor_id' => $merid,

                    ]);

                        } 

                            }

                   



                    $youtubevideoa = str_replace('youtu.be','www.youtube.com/embed',$request->youtubevideo);





                    $mc_video_description = $request->about_video;

                     $mc_video_description_ar = $request->about_video_ar;

                    

                    /*Update data to db*/

                    DB::table('nm_category')->where('mc_id',$shopid)->where('vendor_id',$merid)->update( ['mc_video_url'=>  $youtubevideoa,'mc_video_description'=>  $mc_video_description,'mc_video_description_ar'=>  $mc_video_description_ar]);

                   



                    

                    

                   

                      

                    // language for display message //

                    if (\Config::get('app.locale') == 'ar'){

                    Session::flash('message', "تمت إضافة الخدمة");

                    }

                    else

                    {

                    Session::flash('message', "Record successfully saved");

                    }

                    // language for display message //   

                    return Redirect::back();      

                } else {

                        return Redirect::to('sitemerchant');

                }

              }



                public function deleteShopImage(Request $request)



                {

                        $id = $request->id;

                        $products = $request->products;

                        $kosha = $request->kosha;



                        $mer_id   = Session::get('merchantid'); 

                      if($products==1){

                        $GetPics = DB::table('nm_product_gallery')->where('id',$id)->where('vendor_id',$mer_id)->delete();

                        }

                        else

                        {

                         $GetPics = DB::table('nm_category_gallery')->where('id',$id)->where('vendor_id',$mer_id)->delete();   

                        }

                

                    echo 1;die;  



                }







     //Attributes Listing



  public function receptionAttributes(Request $request)



     {

           if (Session::has('merchantid')) 

             {    $getPagelimit = config('app.paginate'); 

                $mer_id   = Session::get('merchantid');

                $id = $request->id;

                $cid = $request->shopid;

               





                            // CheckPoint for security by Himanshu //

                            $checkPoints = DB::table('nm_category')->where('mc_id',$cid)->where('parent_id',$id)->where('vendor_id',$mer_id)->count();

                            if($checkPoints < 1)

                            {

                            return Redirect::to('sitemerchant_dashboard');

                            }

                            // CheckPoint for security by Himanshu //







                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 

                $status = $request->status;

                $search = $request->search;

                $getAttr = DB::table('nm_services_attribute')->where('vendor_id', '=', $mer_id)->where('services_id', '=', $cid);



                if($search !='')

                {

                 $getAttr = $getAttr->where('attribute_title', 'like', '%'.$search.'%');         

                }

                if($status !='')  

                {  

                $getAttr = $getAttr->where('status', '=', $status);        

                }  



                $getAttr = $getAttr->orderBy('id','DESC')->paginate($getPagelimit);





              

           return view('sitemerchant.reception-list-attributes', compact('merchantheader','merchantfooter','id','getAttr'));       

            } else {

                return Redirect::to('sitemerchant');

            }





     }







     //Worker Listing



  public function receptionListWorkers(Request $request)



     {

           if (Session::has('merchantid')) 

             {

                $mer_id   = Session::get('merchantid');

                $id = $request->id;

                 $shopid = $request->shopid;





                            // CheckPoint for security by Himanshu //

                            $checkPoints = DB::table('nm_category')->where('mc_id',$shopid)->where('parent_id',$id)->where('vendor_id',$mer_id)->count();

                            if($checkPoints < 1)

                            {

                            return Redirect::to('sitemerchant_dashboard');

                            }

                            // CheckPoint for security by Himanshu //





                         $countdata = DB::table('nm_service_staff_nationality')->where('vendor_id',$mer_id)->where('shop_id',$shopid)->count();



                          $countrydata = DB::table('nm_service_staff_nationality')->where('vendor_id',$mer_id)->where('shop_id',$shopid)->get()->toArray();





                         if($countdata) {

                        $countryid = DB::table('nm_service_staff_nationality')->select('nation_id')->where('vendor_id',$mer_id)->where('shop_id',$shopid)->get()->toArray();

                        foreach($countryid as $val ) {



                              $natid[]  = $val->nation_id;

                        }

                    } else

                    {    $natid = array();

                          $countryid = '';

                    }







                $countrylist  = Country::orderBy('co_name','ASC')->get();

                 $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

                $merchantfooter     = view('sitemerchant.includes.merchant_footer');





$getWP = DB::table('nm_services_attribute')->where('services_id', $shopid)->where('vendor_id', $mer_id)->where('attribute_title', 'Worker Price')->first(); 





           return view('sitemerchant.reception-list-workers', compact('merchantheader','merchantfooter','id','countrylist','shopid','natid','getWP','countrydata'));       

            } else {

                return Redirect::to('sitemerchant');

            }





     }

  //Store functionality

     public function storenationality(Request $request) 



     {       

//dd($request->all());

           $this->validate($request,[

             'workers_nationality' => 'required'

             

            ]);





      if (Session::has('merchantid')) 

             {

             

             $countryname = $request->workers_nationality;

             

             $shopid = $request->shopid;

             

             $mer_id   = Session::get('merchantid');

                

              DB::table('nm_service_staff_nationality')->where('shop_id',$shopid)->where('vendor_id',$mer_id)->delete();

              

             $i=1;

              foreach($countryname as $val) 

              {



                $WP = 'worker_price'.$i;

                $workerprice = $request->$WP; 

               $vas = trim(strstr($val, '~', true)); 



                if (\Config::get('app.locale') == 'ar'){

                $getCid = DB::table('nm_country')->where('co_name_ar',$vas)->first();

                }

                else

                {

                $getCid = DB::table('nm_country')->where('co_name',$vas)->first(); 

                }

                $nation_id = $getCid->co_id;

                 $co_name_ar = $getCid->co_name_ar;

                 // insert attributes into table

                DB::table('nm_service_staff_nationality')->insert( [

                'vendor_id' => $mer_id,

                'shop_id' =>$shopid,

                'nation_id' =>$nation_id,

                'country_name' =>$vas,

                'country_name_ar' =>$co_name_ar,

                'worker_price' =>$workerprice            

                ]);

              

              $i=$i+1;

          }



$worker_price = $request->worker_price;

// DB::table('nm_services_attribute')->where('services_id',$shopid)->where('vendor_id',$mer_id)->where('attribute_title','Worker Price')->delete();



// DB::table('nm_services_attribute')->insert(['services_id'=>$shopid,'vendor_id'=>$mer_id,'attribute_title'=>'Worker Price','attribute_title_ar'=>'سعر العامل ','value'=>$worker_price]);



 



          // language for display message  reception-list-workers//

                    if (\Config::get('app.locale') == 'ar'){

                    Session::flash('message', "الجنسية بنجاح");

                    }

                    else

                    {

                    Session::flash('message', "Nationality successfully added");

                    }

                    // language for display message //   

                    return Redirect::back();      

                } else {

                        return Redirect::to('sitemerchant');

                }



     }





     //Information Listing



  public function receptionListInformations(Request $request)



     {

           if (Session::has('merchantid')) 

             {  $getPagelimit = config('app.paginate');

                $mer_id   = Session::get('merchantid');

                $id = $request->id;

                $shopid = $request->shopid;







                            // CheckPoint for security by Himanshu //

                            $checkPoints = DB::table('nm_category')->where('mc_id',$shopid)->where('parent_id',$id)->where('vendor_id',$mer_id)->count();

                            if($checkPoints < 1)

                            {

                            return Redirect::to('sitemerchant_dashboard');

                            }

                            // CheckPoint for security by Himanshu //



                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 



                

                 $getcategoryAttr = DB::table('nm_services_attribute')->where('services_id', '=', $shopid)->get();;



 

                $status = $request->status;

                $search = $request->search;

                $catid=$request->catid;

                $getAttr = DB::table('nm_product')->where('pro_mr_id', '=', $mer_id)->where('pro_mc_id', '=', $shopid)->where('pro_type', '=', 'reception_hospitality')->where('packege', '=', 'no');



                if($search !='')

                {

                 $getAttr = $getAttr->where('pro_title', 'like', '%'.$search.'%');         

                }

                if($status !='' && $catid =='')  

                {  

                $getAttr = $getAttr->where('pro_status', '=', $status);        

                } 

                if($catid !='' && $status =='')  

                {  

                $getAttr = $getAttr->where('attribute_id', '=', $catid);        

                } 

                if($catid !='' && $status !='')  

                { 

                    $getAttr = $getAttr->where('pro_status', '=', $status)->where('attribute_id', '=', $catid); 

                }



                $getAttr = $getAttr->orderBy('pro_id','DESC')->paginate($getPagelimit);

 





           return view('sitemerchant.reception-list-informations', compact('merchantheader','merchantfooter','id','getAttr','getcategoryAttr','shopid'));       

            } else {

                return Redirect::to('sitemerchant');

            }





     }





public function receptionAddcategory(Request $request)

{



           if (Session::has('merchantid')) 

             {

                $mer_id   = Session::get('merchantid');

                $id = $request->id;

                $cid = $request->shopid;

                $itemid = $request->itemid;

        





                            // CheckPoint for security by Himanshu //

                            $checkPoints = DB::table('nm_category')->where('mc_id',$cid)->where('parent_id',$id)->where('vendor_id',$mer_id)->count();

                            if($checkPoints < 1)

                            {

                            return Redirect::to('sitemerchant_dashboard');

                            }

                            // CheckPoint for security by Himanshu //









            $getAttr = DB::table('nm_services_attribute')->where('vendor_id', '=', $mer_id)->where('services_id', '=', $cid)->where('id', '=', $itemid)->first();



 



                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 

                return view('sitemerchant.reception-addcategory', compact('merchantheader','merchantfooter','id','getAttr'));       

            } else {

                return Redirect::to('sitemerchant');

            }





}









public function recptionUpdatecategory(Request $request)

{

           if (Session::has('merchantid')) 

             {

                $mer_id   = Session::get('merchantid');



                $id = $request->id;

                $cid = $request->cid;

                $itemid = $request->itemid;



                $categoryname = $request->category_name;

                $categoryname_ar = $request->category_name_ar;

                $categorydescription = $request->category_description;

                $categorydescription_ar = $request->category_description_ar;

                $status = 1;

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

                $merchantfooter     = view('sitemerchant.includes.merchant_footer');



                if($itemid =='')

                {

            // insert attributes into table

                DB::table('nm_services_attribute')->insert( [

                'attribute_title' => $categoryname,

                'attribute_title_ar' => $categoryname_ar,

                'services_id' =>$cid,

                'parent' =>$id,

                'vendor_id' =>$mer_id, 

                'status' =>$status,            

                ]);

            // insert attributes into table





                    // language for display message //

                    if (\Config::get('app.locale') == 'ar'){

                    Session::flash('message', "تم حفظ الفئة بنجاح");

                    }

                    else

                    {

                    Session::flash('message', "Category successfully saved");

                    }

                    // language for display message // 



                } 

                else

                {





                DB::table('nm_services_attribute')->where('id',$itemid)->where('services_id',$cid)->where('vendor_id',$mer_id)->update( ['attribute_title'=>  $categoryname,'attribute_title_ar'=>  $categoryname_ar]);  





                    // language for display message //

                    if (\Config::get('app.locale') == 'ar'){

                    Session::flash('message', "تم تحديث الفئة بنجاح");

                    }

                    else

                    {

                    Session::flash('message', "Category successfully updated");

                    }

                    // language for display message // 



                }









 

                return redirect()->route('reception-list-attributes', ['id' => $id,'cid' =>$cid]);



 

              //  return view('sitemerchant.reception-list-attributes', compact('merchantheader','merchantfooter','id'));       

            } else {

                return Redirect::to('sitemerchant');

            }





}





public function receptionAdditems()

{



             if (Session::has('merchantid')) 

               {

                    $mer_id   = Session::get('merchantid');



                    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

                    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 

                    $alldata = array();

                    $cid = request()->cid;

                    $id = request()->id;

                    $shopid = request()->shopid;   









                    // CheckPoint for security by Himanshu //

                    $checkPoints = DB::table('nm_category')->where('mc_id',$shopid)->where('parent_id',$id)->where('vendor_id',$mer_id)->count();

                    if($checkPoints < 1)

                    {

                    return Redirect::to('sitemerchant_dashboard');

                    }

                    // CheckPoint for security by Himanshu //











                    $getAttrcount = DB::table('nm_product')->where('pro_id',$cid)->where('pro_mr_id',$mer_id)->count();

                    $getAttr = DB::table('nm_product')->where('pro_id',$cid)->where('pro_mr_id',$mer_id)->first();





                    $productGallery = NmProductGallery::where('vendor_id',$mer_id)->where('product_id',$cid)->get();







                    if(isset(request()->cid))

                    {

                    return view('sitemerchant.reception-additems', compact('merchantheader','merchantfooter','alldata','getAttr','shopid','productGallery'));



                    }else

                    {

                    $myArray = array('attribute_id'=>'');

                    $getAttr =(object) $myArray;

                    return view('sitemerchant.reception-additems', compact('merchantheader','merchantfooter','alldata','getAttrcount','shopid','getAttr','productGallery'));



                    }



            } else {

                return Redirect::to('sitemerchant');

            }

}





public function recptionUpdateitem(Request $request)

{



           if (Session::has('merchantid')) 

             {

                $mer_id   = Session::get('merchantid');

                $id = request()->id;

                $cid = $request->cid;

                $pro_mc_id = request()->shopid; 

                $attribute_id = $request->attribute;

                $pro_title = $request->itemname;

                $pro_title_ar = $request->itemname_ar;

                $pro_price = $request->price;

                $discount = $request->discount;

                $price = $request->price;



                 if($discount!='' && $discount!='0') {

                     $discountprice =   $price-($price * ($discount/100));

                    } else {

                       $discountprice = 0;



                    }



                //$pro_desc = $request->itemdescription;

                //$pro_desc_ar = $request->itemdescription_ar;

                $pro_qty  = $request->qty;

                $pro_status = 1;

                $pro_type = 'reception_hospitality';

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

                $merchantfooter     = view('sitemerchant.includes.merchant_footer');

                $pro_Img = '';





  if($file=$request->file('uploadimage'))

      {          

          $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());          

          $extension = $file->getClientOriginalExtension();

          $fileName = rand(11111,99999).'_'.time().'.'.$extension;                 

          $imageRealPath  =   $file->getRealPath();

          $thumbName      =   'thumb_'. $fileName;       

          $img = Image::make($imageRealPath);

          



 $thumb_width = 150;

                    list($width,$height) = getimagesize($imageRealPath);

                    $thumb_height = ($thumb_width/$width) * $height;

                    $img->resize($thumb_width,$thumb_height);



          $img->save('receptionspic' . '/'. $thumbName);

          $file->move('receptionspic/', $fileName);            

          $pro_Img = url('/').'/receptionspic/'.$thumbName;        

       }



     

                if($cid =='')

                {

            // insert attributes into table

              $getQ =  DB::table('nm_product')->insert( [

                'pro_title' => $pro_title,

                'pro_title_ar' => $pro_title_ar,

               //'pro_desc'=> $pro_desc,

                //'pro_desc_ar'=>$pro_desc_ar, 

                'pro_Img' =>$pro_Img,

                'pro_qty' =>$pro_qty, 

                'pro_mc_id' =>$pro_mc_id, 

                'attribute_id' =>$attribute_id, 

                'pro_price' =>$pro_price, 

                'pro_disprice' =>$discountprice,

                'pro_mr_id' =>$mer_id, 

                'pro_status' =>$pro_status, 

                'pro_type' =>$pro_type, 

                'pro_discount_percentage' => $discount,   

                ]);

            // insert attributes into table

 

$cid =  DB::getPdo()->lastInsertId();

                    // language for display message //

                    if (\Config::get('app.locale') == 'ar'){

                    Session::flash('message', "تم حفظ العنصر بنجاح");

                    }

                    else

                    {

                    Session::flash('message', "Item successfully saved");

                    }

                    // language for display message // 



                } 

                else 

                {

                    if($pro_Img=='')  

                    {

                    DB::table('nm_product')->where('pro_id',$cid)->where('pro_type','reception_hospitality')->where('pro_mr_id',$mer_id)->update( ['pro_title'=>  $pro_title,'pro_title_ar'=>  $pro_title_ar,'pro_mc_id'=> $pro_mc_id,'pro_price'=> $pro_price,'pro_status'=> $pro_status,'pro_type'=> $pro_type,'attribute_id' =>$attribute_id,'pro_discount_percentage' => $discount,'pro_disprice' =>$discountprice,'pro_qty' =>$pro_qty]);

                    }

                    else

                    {

                      DB::table('nm_product')->where('pro_id',$cid)->where('pro_type','reception_hospitality')->where('pro_mr_id',$mer_id)->update( ['pro_title'=>  $pro_title,'pro_title_ar'=>  $pro_title_ar,'pro_Img'=> $pro_Img,'pro_mc_id'=> $pro_mc_id,'pro_price'=> $pro_price,'pro_status'=> $pro_status,'pro_type'=> $pro_type,'attribute_id' =>$attribute_id,'pro_discount_percentage' => $discount,'pro_disprice' =>$discountprice,'pro_qty' =>$pro_qty]);

                    }

 









// FOR PRODUCT MULTIPLE IMAGES UPLOAD //

        $files=$request->file('image');

        $newFiles = array();

        $merid  = Session::get('merchantid');

        if($files=$request->file('image')){

        foreach($files as $key=>$val){

        array_push($newFiles, $key);

        }



        $privius=$request->privius;

        $newFilesMatch = array();

        if(isset($privius) && $privius!='')

        {

        foreach($privius as $key=>$val){

        array_push($newFilesMatch, $key);

        }  

        }



        $CheckInfo = array_intersect($newFiles, $newFilesMatch);

        if(isset($privius) && $privius!='')

        {

        foreach($privius as $key=>$val){

        if(in_array($key, $CheckInfo))

        {

        DB::table('nm_product_gallery')->where('id',$val)->where('vendor_id',$merid)->delete(); 

        }

        }

        } 

        foreach($files as $file){

        $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 

        $extension = $file->getClientOriginalExtension();

        $fileName = rand(11111,99999).'_'.time().'.'.$extension;

        $imageRealPath  =   $file->getRealPath();

        $thumbName      =   'thumb_'. $fileName;       

        $img = Image::make($imageRealPath); // use this if you want facade style code

        $thumb_width = 150;

        list($width,$height) = getimagesize($imageRealPath);

        $thumb_height = ($thumb_width/$width) * $height;

        $img->resize($thumb_width,$thumb_height);                  

        $img->save('uploadimage/abaya' . '/'. $thumbName);

        $file->move('uploadimage/abaya/', $fileName);

        $shop_Img = url('/').'/uploadimage/abaya/'.$thumbName; 

        /*Insert data to db*/

        DB::table('nm_product_gallery')->insert( [

        'image'=>   $shop_Img,

        'product_id' =>$cid,

        'status' => 1,

        'vendor_id' => $merid,

        ]);

        }  

        } 





 

// FOR PRODUCT MULTIPLE IMAGES UPLOAD END //







                    // language for display message //

                    if (\Config::get('app.locale') == 'ar'){

                    Session::flash('message', "تم تحديث العنصر بنجاح");

                    }

                    else

                    {

                    Session::flash('message', "Item successfully updated");

                    }

                    // language for display message // 



                }

 

                return redirect()->route('reception-list-informations', ['id' => $id,'shopid'=>$pro_mc_id]);



 

              //  return view('sitemerchant.reception-list-attributes', compact('merchantheader','merchantfooter','id'));       

            } else {

                return Redirect::to('sitemerchant');

            }



}

 

      //Package Listing



  public function receptionListPackages(Request $request)



     {if (Session::has('merchantid')) 

             {  $getPagelimit = config('app.paginate');

                $mer_id   = Session::get('merchantid');

                $id = $request->id;

                $shopid = $request->shopid;







 // CheckPoint for security by Himanshu //

                            $checkPoints = DB::table('nm_category')->where('mc_id',$shopid)->where('parent_id',$id)->where('vendor_id',$mer_id)->count();

                            if($checkPoints < 1)

                            {

                            return Redirect::to('sitemerchant_dashboard');

                            }

                            // CheckPoint for security by Himanshu //















                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 

 

                $status = $request->status;

                $search = $request->search;

                $getAttr = DB::table('nm_product')->where('pro_mr_id', '=', $mer_id)->where('pro_mc_id', '=', $shopid)->where('pro_type', '=', 'reception_hospitality')->where('packege', '=', 'yes');



                if($search !='')

                {

                 $getAttr = $getAttr->where('pro_title', 'like', '%'.$search.'%');         

                }

                if($status !='')  

                {  

                $getAttr = $getAttr->where('pro_status', '=', $status);        

                }  



                $getAttr = $getAttr->orderBy('pro_id','DESC')->paginate($getPagelimit);

           return view('sitemerchant.reception-list-packages', compact('merchantheader','merchantfooter','id','getAttr','shopid'));       

            } else {

                return Redirect::to('sitemerchant');

            }





     }





     public function AddUpdatePackege(Request $request)

{



           if (Session::has('merchantid')) 

             {

                $mer_id   = Session::get('merchantid');



                $getAttr = DB::table('nm_product')->where('pro_mr_id', '=', $mer_id)->where('pro_mc_id', '=', $request->shopid)->where('pro_type', '=', 'reception_hospitality')->where('packege', '=', 'yes');



                $mer_id   = Session::get('merchantid');

                $id = $request->id;

                $cid = $request->cid;

                $pro_mc_id = $request->shopid;

                $no_of_staff = $request->no_of_staff;

                $pro_title = $request->packeg_name;

                $pro_title_ar = $request->packeg_name_ar;

                $staff_nationality =$request->staff_nationality;

                $pro_price = $request->price;             

                $pro_desc = $request->packegdescription;

                $pro_desc_ar = $request->packegdescription_ar;

                $pro_qty  = $request->quantity;

                $pro_status = 1;

                $discount = $request->discount;



$pro_Img ='';



                if($file=$request->file('mc_img'))

                {          

                $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());          

                $extension = $file->getClientOriginalExtension();

                $fileName = rand(11111,99999).'_'.time().'.'.$extension;                 

                $imageRealPath  =   $file->getRealPath();

                $thumbName      =   'thumb_'. $fileName;       

                $img = Image::make($imageRealPath);

 

                $thumb_width = 272;

                list($width,$height) = getimagesize($imageRealPath);

                $thumb_height = ($thumb_width/$width) * $height;

                $img->resize($thumb_width,$thumb_height);



                $img->save('receptionspic' . '/'. $thumbName);

                $file->move('receptionspic/', $fileName);            

                $pro_Img = url('/').'/receptionspic/'.$thumbName; 

                    

                }







                $pro_type = 'reception_hospitality';

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

                $merchantfooter     = view('sitemerchant.includes.merchant_footer');

              

                $itemid='';

                 if($discount!='' && $discount!='0') {

                     $discountprice =   $pro_price-($pro_price * ($discount/100));

                    } else {

                       $discountprice = 0;



                    }





     

                if($cid =='')

                {

            // insert attributes into table

                $packeg_id=DB::table('nm_product')->insert( [

                'pro_title' => $pro_title,

                'pro_title_ar' => $pro_title_ar,

                'pro_desc'=> $pro_desc,

                'pro_desc_ar'=>$pro_desc_ar, 

                'pro_Img' =>$pro_Img,

                'pro_qty' =>$pro_qty, 

                'pro_mc_id' =>$pro_mc_id, 

                'pro_price' =>$pro_price, 

                'pro_mr_id' =>$mer_id, 

                'pro_status' =>$pro_status, 

                'pro_type' =>$pro_type,

                'packege' =>'yes',

                'no_of_staff' =>$no_of_staff,

                'staff_nationality'=>$staff_nationality,

                'pro_discount_percentage' => $discount,

                'pro_disprice' =>$discountprice

                 



                ]);





                if( $request->cid)

                {

                    $packeg_id=$request->cid;



                }

                else

                {

                    $packeg_id=$packeg_id;



                }

                    $count=DB::table('nm_product_to_packeg')->where('packege_id',$packeg_id)->count();

                    if($count>0)

                    {

                            DB::table('nm_product_to_packeg')->where('packege_id',$packeg_id)->delete();

                    }

                        $attribute = DB::table('nm_services_attribute')->where('vendor_id', '=', $mer_id)->where('services_id', '=', $pro_mc_id)->get();  



                        



                foreach ($attribute as $key1 => $value1) {



                         $item='items'. $value1->id;



                        if($request->$item)

                        {

                            $itemid = $request->$item;



                            if(count($itemid)>0)

                            {



                              foreach ($itemid as $key => $value) {



                                DB::table('nm_product_to_packeg')->insertGetId( [

                                'shop_id' => $pro_mc_id,

                                'attribute_id' => $value1->id,

                                'item_id'=> $value,

                                'packege_id'=>$packeg_id, 

                                                                



                                ]);







                                

                              }

                    }







                        }

 

                }



            // insert attributes into table





                    // language for display message //

                    if (\Config::get('app.locale') == 'ar'){

                    Session::flash('message', "تم حفظ العنصر بنجاح");

                    }

                    else

                    {

                    Session::flash('message', "Package successfully saved");

                    }

                    // language for display message // 



                } 

                else 

                {

                    if($pro_Img=='')  

                    {

                    DB::table('nm_product')->where('pro_id',$cid)->where('pro_type','reception_hospitality')->where('pro_mr_id',$mer_id)->update( ['pro_title'=>  $pro_title,'pro_title_ar'=>  $pro_title_ar,'pro_desc'=>  $pro_desc,'pro_desc_ar'=> $pro_desc_ar,'pro_mc_id'=> $pro_mc_id,'pro_price'=> $pro_price,'pro_status'=> $pro_status,'pro_type'=> $pro_type,'no_of_staff'=> $no_of_staff, 'pro_discount_percentage' => $discount,'pro_disprice' =>$discountprice,'pro_qty' =>$pro_qty]);

                    }

                    else

                    {

                      DB::table('nm_product')->where('pro_id',$cid)->where('pro_type','reception_hospitality')->where('pro_mr_id',$mer_id)->update( ['pro_title'=>  $pro_title,'pro_title_ar'=>  $pro_title_ar,'pro_desc'=>  $pro_desc,'pro_desc_ar'=> $pro_desc_ar,'pro_Img'=> $pro_Img,'pro_mc_id'=> $pro_mc_id,'pro_price'=> $pro_price,'pro_status'=> $pro_status,'pro_type'=> $pro_type,'no_of_staff'=> $no_of_staff,'pro_discount_percentage' => $discount,'pro_disprice' =>$discountprice,'pro_qty' =>$pro_qty]);

                    }



 

               if( $request->cid)

                {

                    $packeg_id=$request->cid;



                }

                else

                {

                    $packeg_id=$packeg_id;



                }

                    $count=DB::table('nm_product_to_packeg')->where('packege_id',$packeg_id)->count();

                    if($count>0)

                    {

                            DB::table('nm_product_to_packeg')->where('packege_id',$packeg_id)->delete();

                    }

                        $attribute = DB::table('nm_services_attribute')->where('vendor_id', '=', $mer_id)->where('services_id', '=', $pro_mc_id)->get();  



                        



                foreach ($attribute as $key1 => $value1) {



                         $item='items'. $value1->id;



                        if($request->$item)

                        {

                            $itemid = $request->$item;



                            if(count($itemid)>0)

                            {



                              foreach ($itemid as $key => $value) {



                                DB::table('nm_product_to_packeg')->insertGetId( [

                                'shop_id' => $pro_mc_id,

                                'attribute_id' => $value1->id,

                                'item_id'=> $value,

                                'packege_id'=>$packeg_id, 

                                                                



                                ]);







                                

                              }

                    }







                        }



                       

                    # code...

                }



 

                    // language for display message //

                    if (\Config::get('app.locale') == 'ar'){

                    Session::flash('message', "تم تحديث العنصر بنجاح");

                    }

                    else

                    {

                    Session::flash('message', "Package successfully updated");

                    }

                    // language for display message // 



                }









 

                return redirect()->route('reception-list-packages', ['id' => $id,'shopid'=>$pro_mc_id]);



 

              //  return view('sitemerchant.reception-list-attributes', compact('merchantheader','merchantfooter','id'));       

            } else {

                return Redirect::to('sitemerchant');

            }



}





     public function addPackages(Request $request)
     {
        if(Session::has('merchantid'))
        {  
            $getPagelimit = config('app.paginate');
            $mer_id       = Session::get('merchantid');
            $id           = $request->id;
            $cid          = $request->cid;
            $shopid       = $request->shopid;
            // CheckPoint for security by Himanshu //
            $checkPoints = DB::table('nm_category')->where('mc_id',$shopid)->where('parent_id',$id)->where('vendor_id',$mer_id)->count();
            if($checkPoints < 1)
            {
               return Redirect::to('sitemerchant_dashboard');
            }
            $shopid = $request->shopid;
            $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
            $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
            $packeg=array();
            if($request->cid)

                {

               $packeg = DB::table('nm_product')->where('pro_mr_id', '=', $mer_id)->where('pro_mc_id', '=', $shopid)->where('pro_type', '=', 'reception_hospitality')->where('packege', '=', 'yes')->where('pro_id', '=', $cid)->first();

                }

                    

                    $selecteditems =  DB::table('nm_product_to_packeg')->select('item_id')->where('packege_id',$cid)->get();

                   

                    $selecteditemsdata=array();



                    foreach ($selecteditems as $key => $value) {



                        $selecteditemsdata[]=$value->item_id;

                    }



                 $getAttr = DB::table('nm_services_attribute')->select('*')->where('vendor_id', '=', $mer_id)->where('services_id', '=', $shopid)->where('status',1)->get();



                  $countdata = DB::table('nm_service_staff_nationality')->where('vendor_id',$mer_id)->count();

                 if($countdata) {

                $countryid = DB::table('nm_service_staff_nationality')->select('nation_id')->where('vendor_id',$mer_id)->get()->toArray();
                $natid=array();

                foreach($countryid as $val ) {

                      $natid[]  = $val->nation_id;

                }

                } else

                {    $natid = array();

                      $countryid = '';

                }

                 $countrylist  = Country::whereIn('co_id',$natid)->get();                



           return view('sitemerchant.reception_hospitality.reception-add-packages', compact('merchantheader','merchantfooter','id','getAttr','shopid','countrylist','items','packeg','selecteditemsdata'));       

            } else {

                return Redirect::to('sitemerchant');

            }





     }





  public function addWorkersNationlity(Request $request)



     {

          if (Session::has('merchantid')) 

             {

                $mer_id   = Session::get('merchantid');





                $id = $request->id;

               

                $shopid = $request->shopid;





 // CheckPoint for security by Himanshu //

                            $checkPoints = DB::table('nm_category')->where('mc_id',$shopid)->where('parent_id',$id)->where('vendor_id',$mer_id)->count();

                            if($checkPoints < 1)

                            {

                            return Redirect::to('sitemerchant_dashboard');

                            }

                            // CheckPoint for security by Himanshu //



                

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 

              $alldata = array();

           return view('sitemerchant.reception-addworker-nationlity', compact('merchantheader','merchantfooter','alldata'));       

            } else {

                return Redirect::to('sitemerchant');

            }





     }













 //Order Listing



  public function receptionListOrders(Request $request)

  {

        if(Session::has('merchantid')) 

        {

                $mer_id         = Session::get('merchantid');

                $id             = request()->id;

                $hid            = request()->shopid;

                $searchkeyword  = $request->searchkeyword;

                $date_to        = $request->date_to;

                $End_Date       = $request->from_to;

                $order_days     = $request->order_days;

                $status         = $request->status;                

                $serachfirstfrm = $request->serachfirstfrm;

                //echo $status;die;

                $getorderedproducts = DB::table('nm_order_product')->where('product_type','occasion')->where('product_sub_type','hospitality');

                if($searchkeyword!='')

                {

                  $getorderedproducts = $getorderedproducts->where('order_id',$searchkeyword);

                }

                if($status!='')

                {

                  $getorderedproducts = $getorderedproducts->where('status',$status);

                }

                if($date_to!='' && $End_Date!='')

                {

                  $getorderedproducts = $getorderedproducts->whereDate('created_at', '>=', $date_to)->whereDate('created_at', '<=', $End_Date);

                }

                $getorderedproducts = $getorderedproducts->where('merchant_id',$mer_id)->groupBy('order_id')->orderBy('created_at','DESC')->selectRaw('*,sum(nm_order_product.total_price) as sum')->paginate(10)->appends(request()->query());



                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 

                

                return view('sitemerchant.reception_hospitality.reception-list-orders', compact('merchantheader','merchantfooter','id','hid','mer_id','getorderedproducts'));       

            } else {

                 return Redirect::to('sitemerchant');

            }

     }



     //Package Listing



  public function receptionListCommentsReview(Request $request)

  {            



if (Session::has('merchantid')) 

    { $getPagelimit = config('app.paginate');

    $merid  = Session::get('merchantid');

    $this->setLanguageLocaleMerchant();

    $mer_id              = Session::get('merchantid');             

    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 

    $getId = request()->id;

    $shopId = request()->shopid;





// CheckPoint for security by Himanshu //

$checkPoints = DB::table('nm_category')->where('mc_id',$shopId)->where('parent_id',$getId)->where('vendor_id',$merid)->count();

if($checkPoints < 1)

{

return Redirect::to('sitemerchant_dashboard');

}

// CheckPoint for security by Himanshu //









    $reviewrating = Reviewrating::where('shop_id',$shopId)->paginate($getPagelimit);

    

    return view('sitemerchant.reception-listcommentsreview', compact('merchantheader','merchantfooter','reviewrating'));       

    } else {

    return Redirect::to('sitemerchant');

    }

 

     }













 //Store Value in shopinfo



      public function storeShopinfo(Request $request)

      {

           $serviceid = request()->id;

           





      }  









     //Add Attributes



       public function addAttributes(Request $request)



      {

           if (Session::has('merchantid')) 

             {

                $id = $request->id;

                $mer_id   = Session::get('merchantid');

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");

                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 

           return view('sitemerchant.add-attribute', compact('merchantheader','merchantfooter'));       

            } else {

                return Redirect::to('sitemerchant');

            }





     }





     



 }






<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//

use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;
use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\Formbusinesstype;
//------------wisitech code end -----------//
class CliniccatgeoryController extends Controller
{  
    public function __construct(){
        parent::__construct();
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
     
    public function listallcliniccatgeory(Request $request,$halltype)
    {
 		//print_r($_REQUEST);
		//echo $halltype;
	
		if(Session::get('searchdata.basecategoryid')!=''){
		$basecategory_id=Session::get('searchdata.basecategoryid');
		}else {
		$basecategory_id=Session::get('searchdata.maincategoryid');
		}
		$category_id=$halltype;
		$lang=Session::get('lang_file');
		$Formbusinesstype= new Formbusinesstype();
		  $bussinesstype=$Formbusinesstype->Getbussinesstype($basecategory_id,$lang);	
			 $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $category_id)->first(); 
                if (!empty($category)) {

                    if ($category_id != '' && $lang == 'ar_lang') {

                        $hallsubcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $category_id)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get(); 
                    } else {

                      
                        $hallsubcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $category_id)->select('mc_id', 'mc_name', 'mc_img')->get();
                      

                    }

                

                }
			
			 $value = $request->session()->get('searchdata');
		
          
             $getsubcategories = $hallsubcategory;
			  
			 
        return view('clinics.category', compact('getsubcategories','bussinesstype','category_id'));
 
    }
   
}

<?php
namespace App\Http\Controllers;

use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Coupon;
use App\Http\Requests;

use App\BusinessOccasionType;
use App\City;

use MyPayPal;
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
use App\Formbusinesstype;


use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\JWT;
use Tymon\JWTAuth\JWTGuard;
use JWTAuth;
use App\User;
use Auth;
use Response;

use Exception;
use App\Country;
use Storage;
 

class SigninSignupController extends Controller
{
		public function __construct(){
        parent::__construct();
        // set frontend language 
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });
        
        

    }
	
	
	public function signinsignup(Request $request)
	{		
		 $getCountry = Country::where('co_status', 1)->orderby('co_name')->get();
		return view('signinsignup',compact('getbasecategory','getCountry'));      
    }
	public function adduseraccount(Request $request)
	{	
	    $method = $request->method();
		if($request->isMethod('post')) 
		{		
			 $validator = Validator::make($request->all(), [
            'uname' => 'required',
            'uemail' => 'required|email|unique:nm_customer,email',
            'umobile' => 'required',
            'upassword' => 'required',
        	]);
				 if($validator->fails()) 
				 {
               	   return redirect('login-signup')->with('status', 'some required fields are not filled!');
          	     }
          	     else
          	     {
			  		$name                   = $request->input('uname');
          			$email                  = $request->input('uemail');
            		$password               = bcrypt($request->input('upassword'));
            		$country_code           = $request->input('country_code');
            		$telephone_number       = $request->input('umobile');
					$saveUser               = new User();
           			$saveUser->cus_name     = $name;
            		$saveUser->email        = $email;
            		$saveUser->cus_status   = 1;
           		    $saveUser->password     = $password;
           		    $saveUser->country_code = $country_code;
            	    $saveUser->cus_phone    = '+'.$country_code.'-'.$telephone_number;
            	    $result                 = $saveUser->save();
				   
				 if (!$result){
					return redirect('login-signup')->with('status', 'Error in registration. Please try again');
					}else{
					
					 $credentials = array('email'=>$email, 'password'=>$request->input('upassword'));
						
							 try {
								if (!$token = JWTAuth::attempt($credentials)) { // attempt to verify the credentials and create a token for the user
									return redirect('login-signup')->with('loginstatus', 'Login credentials2 seems incorrect!');
								}
							} catch (JWTException $e) {
								return redirect('login-signup')->with('loginstatus', 'Login credentials3 seems incorrect!');
								// something went wrong whilst attempting to encode the token
							}
						
						   $user = $userid = Auth::id();
						   $User = User::find($user);
				 $returnuser = array('user_id' => $User->cus_id, "user_email" => $User->email, "user_name" => $User->cus_name, "gender" => $User->gender, "address" => $User->cus_address1.
                ' '.$User->cus_address2, "country_code" => $User->country_code, "telephone_number" => $User->cus_phone, 'token' => $token); 
								Session::put('customerdata', $returnuser);
					 if(Session::get('lang_file'))
					{
				
						$lang ='en_lang';
					}
						
					 $send_mail_data = array(
                        'name' => $request->input('uname'),
                        'password' => $request->input('upassword'),
						'email'=> $request->input('uemail'),
						'lang' => $lang
                    );
                    # It will show these lines as error but no issue it will work fine Line no 119 - 122
                    Mail::send('emails.memberregistrationmail', $send_mail_data, function($message)
                    {
						if (Lang::has(Session::get('lang_file').'.BACK_MEMBER_ACCOUNT_CREATED_SUCCESSFULLY')!= '')
						{ 
							$session_message =  trans(Session::get('lang_file').'.BACK_MEMBER_ACCOUNT_CREATED_SUCCESSFULLY');
						}  
			
                        $message->to(Input::get('uemail'), Input::get('uname'))->subject($session_message);
                    });
					
					
						
						
						
					return redirect('/my-account-profile')->with('status', 'registration sucessfully!'); 
				}
				
			  }
				//echo $request->input('uname');
		}else{
		
			return redirect('login-signup')->with('status', 'some required fields are not filled!');
		}
	
	
	}
	
	
	//////////////// check user duplication ////////////////////
	
	public function checkuseraccount(Request $request){
	
			 $method = $request->method();
			 if ($request->isMethod('post')) {
			 		
			 
						$useremail=$request->input('email');
						   $usercount = User::where('email', '=', $useremail)->count();
					    	if ($usercount <1)
							{
							 $m="approved";
							  }
							else
							  {
							 $m="Error";
							
							  }
					}
				echo $m;
			 
	 }
	///////////////// end dupilcation code /////////////////
	
	///////////////////// login customer account check ////////////
	
	public function checkloginaccount(Request $request){
			
			$method = $request->method();
			 if ($request->isMethod('post')) {
			 	 $lang=Session::get('lang_file');

			 	if ($lang == 'ar_lang') {
                  $errormessage="يبدو أن بيانات اعتماد تسجيل الدخول غير صحيحة";
			 	}else{

			 		$errormessage="Login credentials seems incorrect!";
			 	}
						$useremail=$request->input('useremailaddress');
						$userpassword=bcrypt($request->input('userpassword'));
					  $credentials = $request->only('email', 'password');
					 
							 try {
								if (!$token = JWTAuth::attempt($credentials)) { // attempt to verify the credentials and create a token for the user
									return redirect('login-signup?error=invalid_login')->with('loginstatus#loginfrml', $errormessage);
								}
							} catch (JWTException $e) {
								return redirect('login-signup?error=invalid_login')->with('loginstatus', $errormessage);
								// something went wrong whilst attempting to encode the token
							}
						   $user = $userid = Auth::id();

$Usercount = User::where('cus_id',$user)->where('cus_status',1)->count();

if ($Usercount < 1) { // attempt to verify the credentials and create a token for the user
	
		if ($lang == 'ar_lang') {
                  $errormessage="حسابك غير نشط حاليا";
			 	}else{

			 		$errormessage='Your account is currently inactive';
			 	}
									return redirect('login-signup?error=invalid_login')->with('loginstatus#loginfrml', $errormessage);
								}

						   $User = User::find($user);
						   $returnuser = array('user_id' => $User->cus_id, "user_email" => $User->email, "user_name" => $User->cus_name, "gender" => $User->gender, "address" => $User->cus_address1.
                ' '.$User->cus_address2, "telephone_number" => $User->ship_phone, 'token' => $token);
						Session::put('customerdata', $returnuser);
						 if($User->cus_id!=''){
$last_log_in = date('h:i d M, Y');
 						 User::where('cus_id',$User->cus_id)->update(['last_log_in'=>$last_log_in]);

						 	return redirect('/');
						 }else{
						 	return redirect('login-signup?error=invalid_login')->with('loginstatus#loginfrml', $errormessage);
						 }
					}
				//echo $m;
	
	}
	
	///////////// customer logout///////////////
	
	public function logoutuseraccount(Request $request){
			
			Auth::logout();
			Session::flush();
			return redirect('login-signup');
	}
	
	

} // end 



<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//

use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;
use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\Formbusinesstype;
use App\Shophalldetail;
//------------wisitech code end -----------//
class BranchhalldetailController extends Controller
{  
    public function __construct(){
        parent::__construct();
		 
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
     
    public function halldetail(Request $request,$halltype,$typeofhallid,$shopid,$branchid,$prodid)
    {
 		 if(!Session::has('customerdata.token')) 
	    		{
            	return Redirect::to('login-signup');
			}  
	 $category_id=$shopid;
	$buget=Session::get('searchdata.budget');
		 $halltype=$halltype;
		$typeofhallid=$typeofhallid;
		$shopid=$shopid;
	    $branchid=$branchid;
		 $prodid=$prodid;
		  $request->session()->push('searchdata.catgeoryid', $halltype);
		 $request->session()->push('searchdata.branchid', $branchid);
		 $request->session()->push('searchdata.prodid', $prodid);
		 $request->session()->forget('searchdata.hallchoosenservices');
		 $request->session()->push('searchdata.hallchoosenservices', '');
		if(Session::get('searchdata.basecategoryid')!=''){
		$basecategory_id=Session::get('searchdata.basecategoryid');
		}else {
		$basecategory_id=Session::get('searchdata.maincategoryid');
		}
		$category_id=$shopid;
		 $value = $request->session()->get('searchdata');
		 
		 //print_r($value);
		  $codate=Session::get('searchdata.occasiondate');
		  $haid=request()->haid;

		  $occasiondateform1 = date("Y-m-d", strtotime($codate));  
            $occasiondateform2 = date("d-m-Y", strtotime($codate));  
            $occasiondateform3 = date("d M, Y", strtotime($codate));  

		 //$isbooked = DB::table('nm_order_product')->where('product_id',$haid)->where('hall_booking_date',$codate)->count();

            $isbooked = DB::table('nm_order_product')->where('product_type', '=', 'hall')
            ->where(function($q) use ($occasiondateform1,$occasiondateform2,$occasiondateform3) {
                            $q->where("hall_booking_date",$occasiondateform1)
                            ->orWhere("hall_booking_date",$occasiondateform2)
                            ->orWhere("hall_booking_date",$occasiondateform3);
                            })
            ->where('product_id', $haid)->count();

		$lang=Session::get('lang_file');
		$Formbusinesstype= new Formbusinesstype();
		$ShopProductInfo= new Shophalldetail();
		  $bussinesstype=$Formbusinesstype->Getbussinesstype($basecategory_id,$lang);
		  
		   $ShophallbasicInfo=$ShopProductInfo->GetShopProductbasicInfo($prodid,$lang);
		   $shopoptionsid=$ShophallbasicInfo[0]->option_id;
		    $ShophbasicInfo=$ShopProductInfo->GetShopbasicInfo($branchid,$lang);
			 $shopgalleryimage=$ShopProductInfo->getgalleryimage($prodid);	
			 $shopfacilitiess=$ShopProductInfo->GetShopFacilities($prodid,$lang);
			$shopproductoption=$ShopProductInfo->GetShopProductOption($prodid,$shopoptionsid,$lang);
			 $customerReviewOnProduct=$ShopProductInfo->GetproductReviews($prodid);

			 $ShopInfo=$ShopProductInfo->GetShopbasicInfo($shopid,$lang);

//echo "<pre>";print_r($shopfacilities);
			 //print_r($shopproductoption->product_option_value);
					//die;
			 $serviceoptionvalue=$shopproductoption->product_option_value;
			 $shopfacilities=$shopfacilitiess->product_option_value[0];

			 $isinternalfood=DB::table('nm_internal_food_dish')->where('relate_with',$prodid)->count();

			 
			 $product = Products::select('pro_id', 'pro_title', 'pro_title_ar')->where('pro_mc_id', '=', $branchid)->first(); 
			   if (!empty($product)) {
                 if ($branchid != '' && $lang == 'ar_lang') {
                  $branchhalls = Products::where('pro_mc_id', '=', $branchid)->select('pro_id', 'pro_title_ar as pro_title', 'pro_img')->get();       
				             } else {
                    $branchhalls = Products::where('pro_mc_id', '=', $branchid)->select('pro_id', 'pro_title', 'pro_img')->get();
                  }
               }
			
			
            $getbranchhalls = $branchhalls;

            ////////////////////other branch /////////////////


            		$category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $category_id)->first(); 
                if (!empty($category)) {
                 if ($category_id != '' && $lang == 'ar_lang') {

                 	$otherhalls=DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $branchid)->select('pro_id', 'pro_title_ar as pro_title', 'pro_img','pro_mc_id')->get();

                  $hallsubcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $category_id)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get(); 
                  	foreach ($hallsubcategory as $getchildelement) {
                          $cid=$getchildelement->mc_id;
                            $ischildthere[] = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $cid)->where('pro_price', '<=', $buget)->count();
                            $hallsubcategory->ischild=$ischildthere;
                         }


				             } else {
				          $otherhalls=DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $branchid)->select('pro_id', 'pro_title', 'pro_img','pro_mc_id')->get();
                    $hallsubcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $category_id)->select('mc_id', 'mc_name', 'mc_img')->get();

                    	foreach ($hallsubcategory as $getchildelement) {
                          $cid=$getchildelement->mc_id;
                            $ischildthere[] = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $cid)->where('pro_price', '<=', $buget)->count();
                            $hallsubcategory->ischild=$ischildthere;
                         }

                  }
               }
               $otherhalllist=$otherhalls;
              
               $otherbarnch=$hallsubcategory;
              
               
//echo "<pre>";
 //dd($shopproductoption);
            ////////////////////// other barnch end ////////////
			
        return view('newWebsite.hall_page', compact('getbranchhalls','bussinesstype','halltype','typeofhallid','category_id','shopid','branchid','ShophbasicInfo','ShophallbasicInfo','shopgalleryimage','shopfacilities','shopproductoption','customerReviewOnProduct','serviceoptionvalue','otherbarnch','ShopInfo','isinternalfood','isbooked','otherhalllist'));
 
    }
	
	public function setremoveservicessession(Request $request){
			$subserviceid=$_REQUEST['subserviceid'];
			$isadd=$_REQUEST['isadd'];
			if($isadd=='yes'){
			$request->session()->push('searchdata.hallchoosenservices', $subserviceid);
			}else{
			        
					
					$services = Session::get('searchdata.hallchoosenservices');
        			$found = null;
				
						foreach ($services as $key => $value) {
							if($value == $subserviceid) {
								$found = $key;
									$request->session()->forget('searchdata.hallchoosenservices.'.$found);
							}
						}
						
					   
					
					
		
			}
			
			print_r(Session::get('searchdata'));
	}
   
}

<?php

namespace App\Http\Controllers;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use App\User;
use DB;
use Auth;
use Config;
use Response;
use Validator;
use Exception;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\JWT;
use Tymon\JWTAuth\JWTGuard;
use Mail;
use Storage;

class ApiController extends Controller {

    public function login(\Illuminate\Http\Request $request) { 
        $credentials = $request->only('email', 'password'); 
        
        try {
            if (!$token = JWTAuth::attempt($credentials)) { // attempt to verify the credentials and create a token for the user
                 return response()->json(['status'=>'error','message' => 'invalid_credentials']);
            }
        } catch (JWTException $e) {
             return response()->json(['status'=>'error','message' => 'could_not_create_token']);
            // something went wrong whilst attempting to encode the token
        }        
        $user =  $userid = Auth::id(); 
        $User = User::find($user);
        $returnuser=array('user_id'=>$User->cus_id,"user_email"=>$User->email,"user_name"=> $User->cus_name,"address"=>$User->cus_address1.' '.$User->cus_address2,
"telephone_number"=>$User->ship_phone,'token'=>$token );
        return response()->json(['status' => "success",'message'=>'Welcome! You have signed in.','data'=>$returnuser]);
    }

public function userRegister(Request $request)
{
    $validator = Validator::make($request->all(), [
        'email' => 'required',
        'user_name' => 'required',
        'password' => 'required',        
        'telephone_number'=> 'required',

    ]);

    try
    {
        if ($validator->fails())
        {

         return response()->json(['status'=>'error','message' => $validator->errors()]);

        }

        $name = $request->input('user_name');
        $email = $request->input('email');
        $password =  bcrypt($request->input('password'));
        $language_type = $request->input('language_type');
        $telephone_number = $request->input('telephone_number'); 
        
        $user_email = User::where('email','=',$email)->first();
        if($user_email!=null)
        {
            return response()->json(['status'=>'error','message' =>'User with this email is already registered']);

        }

        $saveUser = new User();
        $saveUser->cus_name=$name;
        $saveUser->email=$email;
        $saveUser->password=$password;
        $saveUser->ship_phone=$telephone_number;      

        $result = $saveUser->save();
        if (!$result)

                return response()->json(['status'=>'error','message' =>'Error in registration. Please try again']);

            $credentials = $request->only('email', 'password'); 
        
        try {
            if (!$token = JWTAuth::attempt($credentials)) { // attempt to verify the credentials and create a token for the user
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500); // something went wrong whilst attempting to encode the token
        }      

        $user =  $userid = Auth::id(); 
        $User = User::find($user);
        $returnuser=array('user_id'=>$User->cus_id,"user_email"=>$User->email,"user_name"=> $User->cus_name,"address"=>$User->cus_address1.' '.$User->cus_address2,
        "telephone_number"=>$User->ship_phone,'token'=>$token );
        return response()->json(['status' => "success", 'status_code' => 1,'message'=>'Congratulations! You are our Newest Member.','data'=>$returnuser]);
           // return Response::json(array('status' => false, 'message'=>'User registered successfully'));
             }
    catch(Exception $e)
    {
        $data = [
            'status' => 'fail',
            'status_code'   => $e->getCode(),
            'message'       => $e->getMessage(),            
        ];
        $result = Response::json($data);
        return $result;
    }
}


    public function Forgotpassword(Request $request){

        $email = $request->email;


        $validator = Validator::make($request->all(), [
                'email' => 'required',
                
            ]);

        if ($validator->fails())
        {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => $error_messages, 
                                    'error_code' => 401, 'error_messages' => 'Invalid Input');
            $response_code = 200;

            $response = Response::json($response_array, $response_code);
            return $response;
        }
        else
        {
           
            $user = User::where('email',$email)->first();

            if(!$user)
            {
               $response_array = array('data'=>['success' => false ,'error' => 'Email ID not found' , 'error_code' => 407]);

                $response_code = 200;

                $response = Response::json($response_array, $response_code);
                return $response;
            
            }

            $new_password = time();
            $new_password .= rand();
            $new_password = sha1($new_password);
            $new_password = substr($new_password, 0, 8);
            $user->password = Hash::make($new_password);

            $subject = "Your New Password";
            $email_data['name'] = $user->author_name;
            $email_data['password'] = $new_password;
            $email_data['email'] = $user->email;  

            if($user)
            {
                try
                {
                    Mail::send('emails.password', array('email_data' => $email_data), function ($message) use ($email, $subject) {
                        $message->to($email)->subject($subject);
                    });
                }
                catch(Exception $e)
                {
                    $response_array = array('data'=>['success' => false ,'error' => 'Mail Config is wrong' , 'error_code' => 407]);
                    $response_code = 200;
                }

                $user->save();
                $response_array = array('data'=>['success' => true ,'message' => 'New password send to the email adderss'] );
                $response_code = 200;

            }else{
                 $response_array = array('data'=>['success' => false ,'message' => 'Oops! Your password did not match our records. Please try again.' , 'error_code' => 407]);
                
                $response_code = 200;
            }
            
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    
    }




    public function changePassword(Request $request) {

        $user_id  = Auth::id(); 

        $old_password = $request->old_password;
        $new_password = $request->password;
        $confirm_password = $request->password_confirmation;
        
        $validator = Validator::make($request->all(), [
                'password' => 'required|confirmed',
                'old_password' => 'required',
            ]);

        if($validator->fails()) {

            $error_messages = $validator->messages()->all();

            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages );

            $response_code = 200;

        } else {

            if($user = User::find($user_id)) {

                if(Hash::check($old_password,$user->password))
                {
                    $user->password = Hash::make($new_password);
                    $user->save();

                    $response_array = array('data'=>['success' => true , 'message' => 'Password Changed successfully']);
                    $response_code = 200;
                    
                } else {
                    $response_array = array('data'=>['success' => false , 'error' => "Sorry! Your old password didn't match. Enter it again."]);
                    $response_code = 200;
                }

            } else {

                $response_array = array('data'=>['success' => false , 'error' => 'User ID not found']);
                $response_code = 200;
            }

        }

        $response = Response::json($response_array,$response_code);

        return $response;
    }


    public function editProfile(Request $request)
    {

       $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                        
            ]);

        if($validator->fails()) {
        
             $response_array = array('data'=>['success' => false , 'error' => 'Invalid Input']);
             $response = Response::json($response_array,200);
        }
        else
        {
        try {

                $User = Auth::user();

                if($request->has('name')) 
                    $User->cus_name = $request->name;
                if ($request->has('phone'))
                    $User->ship_phone = $request->phone;                 
                if ($request->has('address'))
                    $User->cus_address1 = $request->address;
                if ($request->has('dob'))
                    $User->dob = $request->dob;
                if ($request->has('gender'))
                    $User->gender = $request->gender;
                if ($request->hasFile('avatar')) {
                    Storage::delete($User->avatar);
                    $User->avatar = $request->avatar->store('User/profile');
                     }

               $User->save();
                $response_code=200;
                $response_array = array('data'=>['success' => true , 'message' => 'Profile update Sucessfully']);
                $response = Response::json($response_array,$response_code);
        } catch (ModelNotFoundException $e) {
             $response_array = array('data'=>['success' => false , 'error' => 'User not found']);
             $response = Response::json($response_array,200);
        }
    }

    return $response;

}



public function getProfile(Request $request)
    {

                $User = Auth::user();
                 $returnuser=array('user_id'=>$User->cus_id,"email"=>$User->email,"name"=> $User->cus_name,"address"=>$User->cus_address1.' '.$User->cus_address2,"telephone_number"=>$User->ship_phone,'gender'=>$User->gender,'avatar'=>$User->avatar,'dob'=>$User->dob);

                $response_array = array('data'=>['success' => true , 'profile' =>$returnuser]);
                $response = Response::json($response_array,200);
                 return $response;
      
    }




 public function socialsignup(Request $request){
        $name = $request->name;
        $email = $request->email;
        $social_unique_id = $request->social_unique_id;
        $gender = $request->gender;
        $dob = $request->dob;
       
        $validator = Validator::make(
            $request->all(),
            [
                'social_unique_id' => 'required'
            ]
        );
        
        if($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->messages()->all()]);
        }
       
        try{

                             $user = User::where('email', '=', $email)->first(); 
                             $usercount = User::where('email', '=', $email)->count(); 


                            if($usercount>0)
                            {
                              try {
                                    if (!$token = JWTAuth::fromUser($user)) { // attempt to verify the credentials and create a token for the user
                                    $AuthUser=new User();
                                    $AuthUser->email=$email;
                                    $AuthUser->cus_name=$name;
                                    $AuthUser->social_unique_id=$request->social_unique_id;
                                    $AuthUser->gender=$gender;
                                    $AuthUser->save();
                                    //$token = JWTAuth::attempt($credentials) ;
                                    $user = User::where('email', '=', $email)->first();
                                    $token = JWTAuth::fromUser($user); 
                                }
                            } catch (JWTException $e) {
                                 return response()->json(['status'=>'error','message' => 'could_not_create_token']);
                                // something went wrong whilst attempting to encode the token
                            } 
                            }
                            else{
                                    $AuthUser=new User();
                                    $AuthUser->email=$email;
                                    $AuthUser->cus_name=$name;
                                    $AuthUser->social_unique_id=$request->social_unique_id;
                                    $AuthUser->gender=$gender;
                                    $AuthUser->save();
                                    //$token = JWTAuth::attempt($credentials) ;
                                    $user = User::where('email', '=', $email)->first();
                                    $token = JWTAuth::fromUser($user); 


                                }    
                
                // $userdetails = User::where('email',$email)->get(); 
                         $User = User::where('email', '=', $email)->first();   
                        

            
            if($User){

                     $returnuser=array('user_id'=>$User->cus_id,
                    "user_email"=>$User->email,
                    "user_name"=> $User->cus_name,
                    "address"=>$User->cus_address1.' '.$User->cus_address2,
                    "telephone_number"=>$User->ship_phone,
                    'token'=>$token );

                   return response()->json(['status' => "success",'message'=>'User loged in successfully','data'=>$returnuser]);

            }else{
                return response()->json(['status'=>false,'message' => "Invalid credentials!"]);
            }  
        } catch (Exception $e) {
            return response()->json(['status'=>false,'message' => $e->getMessage()]);
        }




     }








}






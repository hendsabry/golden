<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//

use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;
use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\CartProductAttribute;
use App\Formbusinesstype;
use App\CartProductRent;
use App\CartBodyMeasurement;
//------------wisitech code end -----------//

class ShoppingController extends Controller
{
	 public function __construct()
     {
        parent::__construct();
         $this->middleware(function ($request, $next) 
         {
                    $this->setLanguageLocaleFront();
                    return $next($request);
        });      
    }
	
	 public function shoppingList($id)
	 {	
	 	$category_id = $id;
        if(Session::has('customerdata.user_id')) 
        {
    	 	if(Session::get('searchdata.basecategoryid')!='')
    	 	{
                 $basecategory_id = Session::get('searchdata.basecategoryid');
            }
            else 
            {
                 $basecategory_id = Session::get('searchdata.maincategoryid');
            }
            $city_id           = Session::get('searchdata.cityid');
    	 	    $lang              = Session::get('lang_file');
    		    $Formbusinesstype  = new Formbusinesstype();
    		    $bussinesstype     = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);	

    		    $category = Category::select('mc_id','mc_name','mc_name_ar','mc_status')->where('mc_id',$category_id)->first(); 
            if(!empty($category)) 
            {
                if($category_id != '' && $lang == 'ar_lang') 
                {
                    $shopping = Category::where('mc_status',1)->where('parent_id',$category_id)->select('mc_id','mc_name_ar as mc_name','mc_img')->get(); //GET SUB CATEGORY LIST IN ARABIC
                } 
                else 
                {                       
                    $shopping = Category::where('mc_status',1)->where('parent_id',$category_id)->select('mc_id','mc_name','mc_img')->get();
                }
            }
    	 	    $getsubcategories = $shopping;
            return view('shopping.shopping',compact('getsubcategories','bussinesstype','category_id')); 
          } 
          else
          {
            return Redirect::to('login-signup');
          }      
   }
	
   public function tailerslist($id,$sid)
   {
      $category_id = $id;
      $subcat_id   = $sid;
      $getPagelimit = config('app.paginate');
        if(Session::has('customerdata.user_id')) 
        {
            if(Session::get('searchdata.basecategoryid')!='')
            {
                 $basecategory_id = Session::get('searchdata.basecategoryid');
            }
            else 
            {
                 $basecategory_id = Session::get('searchdata.maincategoryid');
            }
            $city_id = Session::get('searchdata.cityid');
            $lang = Session::get('lang_file');
            $Formbusinesstype = new Formbusinesstype();
            $bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);            
            if(!empty($subcat_id)) 
            {
                if($category_id != '' && $lang == 'ar_lang') 
                {
                    $vendorinfo = Category::select('mc_id','vendor_id','mc_name_ar as mc_name','mc_discription_ar as mc_discription','city_id','occasion_id','mc_img','mc_video_url','address_ar as address','mc_video_description as mc_video_description_ar')->where('mc_status',1)->where('mer_activity',1)->where('parent_id',$subcat_id)->where('city_id',$city_id)->paginate($getPagelimit)->appends(request()->query());                                     
                } 
                else 
                {  
                    $vendorinfo = Category::select('mc_id','vendor_id','mc_name','mc_discription','city_id','occasion_id','mc_img','mc_video_url','address','mc_video_description')->where('mer_activity',1)->where('parent_id',$subcat_id)->where('mc_status',1)->where('city_id',$city_id)->paginate($getPagelimit)->appends(request()->query());                   
                }
            }
            $allreview = DB::table('nm_review')->where('shop_id',$subcat_id)->where('review_type','product')->where('status',1)->get();
            $vendordetails = $vendorinfo;           
            return view('shopping.tailerslist',compact('vendordetails','bussinesstype','category_id','subcat_id','allreview')); 
        } 
        else
        {
          return Redirect::to('login-signup');
        } 
   }

   function getSizeQuantity(Request $request)
   {
        $product_id     = $request->product_id;
        $product_size   = $request->product_size;
        $qty            = $request->qty;
        $ordertype=$request->ordertype;
        $productinfo    = DB::table('nm_product_option_value')->where('product_id',$product_id)->where('option_title',$product_size)->select('value','value_ar')->first();
        if($ordertype=='rent'){ $dbqty=$productinfo->value_ar; }else{ $dbqty=$productinfo->value;}
        if($qty <= @$dbqty)
        {
          echo 'ok';
        }
        else
        {
            echo "error";
        }
    }


 function getdresssize(Request $request)
   {
        $product_id     = $request->product_id;
        $product_size   = $request->product_size;
        
        $productinfo    = DB::table('nm_product_option_value')->where('product_id',$product_id)->where('option_title',$product_size)->select('value','value_ar')->first();


        if($productinfo->value>0 && $productinfo->value_ar>0)
        {
          echo 'ok';
        }
        elseif($productinfo->value>0 && $productinfo->value_ar < 1)
        {
            echo "rentnotavail";
        }
         elseif($productinfo->value < 1 && $productinfo->value_ar > 0)
        {
            echo "buynotavail";
        }
         else
        {
            echo "error";
        }
    }


   
   public function dresseslist($id,$sid)
   {
        $category_id = $id;
        $subcat_id   = $sid;
        $getPagelimit = config('app.paginate');
        if(Session::has('customerdata.user_id')) 
        {
            if(Session::get('searchdata.basecategoryid')!='')
            {
                 $basecategory_id = Session::get('searchdata.basecategoryid');
            }
            else 
            {
                 $basecategory_id = Session::get('searchdata.maincategoryid');
            }
            $city_id = Session::get('searchdata.cityid');
            $lang = Session::get('lang_file');
            $Formbusinesstype = new Formbusinesstype();
            $bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);  

             $iscatgoryexist = DB::table('nm_services_attribute')->where('parent',$subcat_id)->where('status',1)->get();
               $getparentid = array();
             foreach ($iscatgoryexist as $catpresent) {
                $pro_ida = $catpresent->services_id;
                            array_push($getparentid, $pro_ida);
             }

            if(!empty($subcat_id)) 
            {
                if($category_id != '' && $lang == 'ar_lang') 
                {
                    $vendorinfo = Category::select('mc_id','vendor_id','mc_name_ar as mc_name','mc_discription_ar as mc_discription','city_id','occasion_id','mc_img','mc_video_url','address_ar as address','mc_video_description as mc_video_description_ar')->where('mc_status','=',1)->where('parent_id',$subcat_id)->whereIn('mc_id', $getparentid)->where('mer_activity','=',1)->where('city_id',$city_id)->paginate(9)->appends(request()->query());  

                } 
                else 
                {  
                    $vendorinfo = Category::select('mc_id','vendor_id','mc_name','mc_discription','city_id','occasion_id','mc_img','mc_video_url','address','mc_video_description')->where('parent_id',$subcat_id)->whereIn('mc_id', $getparentid)->where('city_id',$city_id)->where('mer_activity','=',1)->where('mc_status','=',1)->paginate(9)->appends(request()->query()); 


                }
            }



            $allreview = DB::table('nm_review')->where('shop_id',$subcat_id)->where('review_type','product')->where('status',1)->get();
            $vendordetails = $vendorinfo;           
            return view('shopping.dresseslist',compact('vendordetails','bussinesstype','category_id','subcat_id','allreview')); 
        } 
        else
        {
          return Redirect::to('login-signup');
        }
   }

   public function perfumeslist($id,$sid)
   {
        $category_id  = $id;
        $subcat_id    = $sid;
        $getPagelimit = config('app.paginate');
        if(Session::has('customerdata.user_id')) 
        {
            if(Session::get('searchdata.basecategoryid')!='')
            {
                 $basecategory_id = Session::get('searchdata.basecategoryid');
            }
            else 
            {
                 $basecategory_id = Session::get('searchdata.maincategoryid');
            }
            $city_id = Session::get('searchdata.cityid');
            $lang = Session::get('lang_file');
            $Formbusinesstype = new Formbusinesstype();
            $bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);            
            if(!empty($subcat_id)) 
            {
                if($category_id != '' && $lang == 'ar_lang') 
                {
                    $vendorinfo = DB::table('nm_category as b')->select('s.mc_id','s.vendor_id','s.mc_name_ar as mc_name','s.mc_discription_ar as mc_discription','s.city_id','s.occasion_id','s.mc_img','s.mc_video_url','s.address_ar as address','s.mc_video_description as mc_video_description_ar')->join('nm_category AS s', 's.mc_id', '=', 'b.parent_id')->where('b.mc_status', 1)->where('b.city_id', $city_id)->groupBy('s.mc_name')->where('s.parent_id',$subcat_id)->paginate(9)->appends(request()->query());                  
                } 
                else 
                {  
                    $vendorinfo = DB::table('nm_category as b')->select('s.mc_id','s.vendor_id','s.mc_name','s.mc_discription','s.city_id','s.occasion_id','s.mc_img','s.mc_video_url','s.address','s.mc_video_description')->join('nm_category AS s', 's.mc_id', '=', 'b.parent_id')->where('b.city_id', $city_id)->groupBy('s.mc_name')->where('s.parent_id',$subcat_id)->where('b.mc_status', 1)->paginate(9)->appends(request()->query()); 
                }
            }
            $allreview = DB::table('nm_review')->where('shop_id',$subcat_id)->where('review_type','product')->where('status',1)->get();
            $vendordetails = $vendorinfo;           
            return view('shopping.perfumeslist',compact('vendordetails','bussinesstype','category_id','subcat_id','allreview')); 
        } 
        else
        {
          return Redirect::to('login-signup');
        }
   }
   
   public function perfumesbranchlist($id,$sid,$shop_id)
   {
        $category_id = $id;
        $subcat_id   = $sid;
        $shop_id     = $shop_id;
        $getPagelimit = config('app.paginate');
        if(Session::has('customerdata.user_id')) 
        {
            if(Session::get('searchdata.basecategoryid')!='')
            {
                 $basecategory_id = Session::get('searchdata.basecategoryid');
            }
            else 
            {
                 $basecategory_id = Session::get('searchdata.maincategoryid');
            }
            $city_id = Session::get('searchdata.cityid');
            $lang = Session::get('lang_file');
            $Formbusinesstype = new Formbusinesstype();
            $bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);            
            if(!empty($subcat_id)) 
            {
                if($lang == 'ar_lang') 
                {
                    $vendordetails = Category::select('mc_id','vendor_id','mc_name_ar as mc_name','mc_discription_ar as mc_discription','city_id','occasion_id','mc_img','mc_video_url','address_ar as address','mc_video_description as mc_video_description_ar')->where('parent_id',$shop_id)->where('city_id',$city_id)->paginate(9)->appends(request()->query());
                    foreach($vendordetails as $val) 
                    {
                      $isproductcate = DB::table('nm_services_attribute')->where('status',1)->where('services_id',$val->mc_id)->count();

                      if($isproductcate >0){
                      $isproduct = DB::table('nm_product')->where('pro_status',1)->where('pro_mc_id',$val->mc_id)->count();
                      if($isproduct > 0)
                      {
                        $val->isproduct = $isproduct;
                      }
                      else
                      {
                        $val->isproduct = '';
                      }
                       }else{
                          $val->isproduct = '';
                        }
                     }                   
                } 
                else 
                {  
                    $vendordetails = Category::select('mc_id','vendor_id','mc_name','mc_discription','city_id','occasion_id','mc_img','mc_video_url','address','mc_video_description')->where('parent_id',$shop_id)->where('city_id',$city_id)->paginate(9)->appends(request()->query());
                    foreach($vendordetails as $val) 
                    {

                      $isproductcate = DB::table('nm_services_attribute')->where('status',1)->where('services_id',$val->mc_id)->count();

                      if($isproductcate >0){

                      $isproduct = DB::table('nm_product')->where('pro_status',1)->where('pro_mc_id',$val->mc_id)->count();
                      if($isproduct > 0)
                      {
                        $val->isproduct = $isproduct;
                      }
                      else
                      {
                        $val->isproduct = '';
                      }
                     
                        }else{
                          $val->isproduct = '';
                        }

                     } 
                }
            }
            $allreview = DB::table('nm_review')->where('shop_id',$shop_id)->where('review_type','product')->where('status',1)->get();          
            return view('shopping.perfumesbranchlist',compact('vendordetails','bussinesstype','category_id','subcat_id','shop_id','allreview')); 
        } 
        else
        {
          return Redirect::to('login-signup');
        }
   }
   
   public function perfumesbranchdetail($id,$sid,$shop_id,$branch_id)
   {          
      $category_id = $id;
      $subcat_id   = $sid;
      $shop_id     = $shop_id;
      $branch_id   = $branch_id;

        $getPagelimit = config('app.paginate');
        if(Session::has('customerdata.user_id')) 
        {
        if(Session::get('searchdata.basecategoryid')!='')
        {
                 $basecategory_id = Session::get('searchdata.basecategoryid');
        }
        else 
        {
             $basecategory_id = Session::get('searchdata.maincategoryid');
        }
        $city_id = Session::get('searchdata.cityid');
        $lang = Session::get('lang_file');
        $Formbusinesstype = new Formbusinesstype();
        $bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);            
            if(!empty($subcat_id)) 
            {
                $otherbranch = Category::select('mc_id','parent_id','mc_name','mc_name_ar')->where('city_id',$city_id)->where('parent_id',$shop_id)->get();
                if($lang == 'ar_lang') 
                {
                    $vendordetails = Category::select('mc_id','vendor_id','mc_name_ar as mc_name','mc_discription_ar as mc_discription','city_id','occasion_id','mc_img','mc_video_url','address_ar as address','mc_video_description as mc_video_description_ar','terms_conditions_ar as terms_conditions','longitude','latitude')->where('mc_id',$branch_id)->where('parent_id',$shop_id)->first();
                    $vendor_id = $vendordetails->vendor_id;

                    $productlist = DB::table('nm_services_attribute')->select('id','attribute_title_ar as attribute_title')->where('services_id',$branch_id)->where('vendor_id',$vendor_id)->where('status',1)->get();
                    foreach($productlist as $value) 
                    {
                        $productAttribute = Products::where('attribute_id',$value->id)->where('pro_status',1)->where('pro_mr_id',$vendor_id)->where('pro_mc_id',$branch_id)->select('pro_id','pro_title_ar as pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc_ar as pro_desc','about_ar as about','notes_ar as notes','pro_price','pro_qty','Insuranceamount','pro_disprice','attribute_id')->paginate(9)->appends(request()->query());
                         $value->cat_attribute = $productAttribute;
                    }
                } 
                else 
                {   
                    $vendordetails = Category::select('mc_id','vendor_id','mc_name','mc_discription','city_id','occasion_id','mc_img','mc_video_url','address','mc_video_description','terms_conditions','longitude','latitude')->where('mc_id',$branch_id)->where('parent_id',$shop_id)->first(); 
                    $vendor_id = $vendordetails->vendor_id;

                    $productlist = DB::table('nm_services_attribute')->select('id','attribute_title')->where('services_id',$branch_id)->where('vendor_id',$vendor_id)->where('status',1)->get();

                    foreach($productlist as $value) 
                    {
                        $productAttribute = Products::where('attribute_id',$value->id)->where('pro_status',1)->where('pro_mr_id',$vendor_id)->where('pro_mc_id',$branch_id)->select('pro_id','pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc','about','notes','pro_price','Insuranceamount','pro_disprice','pro_qty','attribute_id')->paginate(9)->appends(request()->query());
                         $value->cat_attribute = $productAttribute;                                                
                    }
                }
            }
            $allreview =   DB::table('nm_review')->where('shop_id',$branch_id)->where('review_type','shop')->where('status',1)->orderBy('created_at','desc')->get();
            return view('shopping.perfumesbranchdetail',compact('vendordetails','productlist','bussinesstype','category_id','subcat_id','shop_id','branch_id','allreview','otherbranch')); 
        } 
        else
        {
          return Redirect::to('login-signup');
        }      
   }

   public function abayalist($id,$sid)
   {
        $category_id  = $id;
        $subcat_id    = $sid;
        $getPagelimit = config('app.paginate');
        if(Session::has('customerdata.user_id')) 
        {
            if(Session::get('searchdata.basecategoryid')!='')
            {
                 $basecategory_id = Session::get('searchdata.basecategoryid');
            }
            else 
            {
                 $basecategory_id = Session::get('searchdata.maincategoryid');
            }
            $city_id = Session::get('searchdata.cityid');
 
              //\Helper::checkShopAvilabe($subcat_id, $city_id);


            $lang = Session::get('lang_file');
            $Formbusinesstype = new Formbusinesstype();
            $bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);            
            if(!empty($subcat_id)) 
            {
                if($category_id != '' && $lang == 'ar_lang') 
                {
                    $vendorinfo = Category::select('mc_id','vendor_id','mc_name_ar as mc_name','mc_discription_ar as mc_discription','city_id','occasion_id','mc_img','mc_video_url','address_ar as address','mc_video_description as mc_video_description_ar')->where('parent_id',$subcat_id)->where('city_id',$city_id)->where('mc_status',1)->where('mer_activity',1)->paginate(9)->appends(request()->query());                   
                } 
                else 
                {  
                    $vendorinfo = Category::select('mc_id','vendor_id','mc_name','mc_discription','city_id','occasion_id','mc_img','mc_video_url','address','mc_video_description')->where('parent_id',$subcat_id)->where('city_id',$city_id)->where('mc_status',1)->where('mer_activity',1)->paginate(9)->appends(request()->query()); 
                }

                foreach ($vendorinfo as $isproduct) {                 
                 $isabayaproduct= DB::table('nm_product')->where('pro_mc_id',$isproduct->mc_id)->where('pro_status',1)->count();

                 $isproduct->productavil=$isabayaproduct;

                }
            }
            $allreview = DB::table('nm_review')->where('shop_id',$subcat_id)->where('review_type','product')->where('status',1)->get();
            $vendordetails = $vendorinfo; 
           
                     
            return view('shopping.abayalist',compact('vendordetails','bussinesstype','category_id','subcat_id','allreview')); 
        } 
        else
        {
          return Redirect::to('login-signup');
        }
   }

   public function jewelerylist($id,$sid)
   {
          $category_id  = $id;
          $subcat_id    = $sid;


        $getPagelimit = config('app.paginate');
        if(Session::has('customerdata.user_id')) 
        {
            if(Session::get('searchdata.basecategoryid')!='')
            {
                 $basecategory_id = Session::get('searchdata.basecategoryid');
            }
            else 
            {
                 $basecategory_id = Session::get('searchdata.maincategoryid');
            }
            $city_id = Session::get('searchdata.cityid');
             \Helper::checkShopAvilabe($subcat_id, $city_id);
 
           

            $lang = Session::get('lang_file');
            $Formbusinesstype = new Formbusinesstype();
            $bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);            
            if(!empty($subcat_id)) 
            {
                if($category_id != '' && $lang == 'ar_lang') 
                {
                    $vendordetails = DB::table('nm_category as b')->select('s.mc_id','s.vendor_id','s.mc_name_ar as mc_name','s.mc_discription_ar as mc_discription','s.city_id','s.occasion_id','s.mc_img','s.mc_video_url','s.address_ar as address','s.mc_video_description as mc_video_description_ar')->join('nm_category AS s', 's.mc_id', '=', 'b.parent_id')->where('b.mc_status','=',1)->where('s.mer_activity','=',1)->groupBy('b.parent_id')->where('b.city_id',$city_id)->where('s.parent_id',$subcat_id)->paginate(9)->appends(request()->query());                               
                } 
                else 
                {  
                    $vendordetails = DB::table('nm_category as b')->select('s.mc_id','s.vendor_id','s.mc_name','s.mc_discription','s.city_id','s.occasion_id','s.mc_img','s.mc_video_url','s.address','s.mc_video_description')->join('nm_category AS s', 's.mc_id', '=', 'b.parent_id')->where('b.city_id', $city_id)->where('b.mc_status','=',1)->where('s.mer_activity','=',1)->where('s.parent_id',$subcat_id)->groupBy('b.parent_id')->paginate(9)->appends(request()->query());
                    // $vendordetails = Category::select('mc_id','vendor_id','mc_name','mc_discription','city_id','occasion_id','mc_img','mc_video_url','address','mc_video_description')->where('parent_id',$subcat_id)->where('city_id',$city_id)->where('mc_status2',1)->where('mer_activity',1)->get();  
                    
                }
            }
       
            $allreview = DB::table('nm_review')->where('shop_id',$subcat_id)->where('review_type','product')->where('status',1)->get();          
            return view('shopping.jewelerylist',compact('vendordetails','bussinesstype','category_id','subcat_id','allreview')); 
        } 
        else
        {
          return Redirect::to('login-signup');
        }
   }

   public function jewelerybranchlist($id,$sid,$shop_id)
   {
        $category_id = $id;
        $subcat_id   = $sid;
        $shop_id     = $shop_id;
        $getPagelimit = config('app.paginate');
        if(Session::has('customerdata.user_id')) 
        {
            if(Session::get('searchdata.basecategoryid')!='')
            {
                 $basecategory_id = Session::get('searchdata.basecategoryid');
            }
            else 
            {
                 $basecategory_id = Session::get('searchdata.maincategoryid');
            }
            $city_id = Session::get('searchdata.cityid');
            $lang = Session::get('lang_file');
            $Formbusinesstype = new Formbusinesstype();
            $bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);            
            if(!empty($subcat_id)) 
            {
                if($category_id != '' && $lang == 'ar_lang') 
                {
                    $vendordetails = Category::select('mc_id','vendor_id','mc_name_ar as mc_name','mc_discription_ar as mc_discription','city_id','occasion_id','mc_img','mc_video_url','address_ar as address','mc_video_description as mc_video_description_ar')->where('mc_status',1)->where('parent_id',$shop_id)->where('city_id',$city_id)->paginate(9)->appends(request()->query()); 
                    foreach($vendordetails as $val) 
                    {
                      $isproduct = DB::table('nm_product')->where('pro_status',1)->where('pro_mc_id',$val->mc_id)->count();
                      if($isproduct > 0)
                      {
                        $val->isproduct = $isproduct;
                      }
                      else
                      {
                        $val->isproduct = '';
                      }
                     }                  
                } 
                else 
                {  
                    $vendordetails = Category::select('mc_id','vendor_id','mc_name','mc_discription','city_id','occasion_id','mc_img','mc_video_url','address','mc_video_description')->where('mc_status',1)->where('parent_id',$shop_id)->where('city_id',$city_id)->paginate(9)->appends(request()->query());
                    foreach($vendordetails as $val) 
                    {
                      $isproduct = DB::table('nm_product')->where('pro_status',1)->where('pro_mc_id',$val->mc_id)->count();
                      if($isproduct > 0)
                      {
                        $val->isproduct = $isproduct;
                      }
                      else
                      {
                        $val->isproduct = '';
                      }
                     } 
                }
            }
            $allreview = DB::table('nm_review')->where('shop_id',$shop_id)->where('review_type','product')->where('status',1)->get();          
            return view('shopping.jewelerybranchlist',compact('vendordetails','bussinesstype','category_id','subcat_id','shop_id','allreview')); 
        } 
        else
        {
          return Redirect::to('login-signup');
        }
   }
  
   public function jewelerybranchdetail($id,$sid,$shop_id,$branch_id)
   {          
      $category_id = $id;
      $subcat_id   = $sid;
      $shop_id     = $shop_id;
      $branch_id   = $branch_id;

      $getPagelimit = config('app.paginate');
      if(Session::has('customerdata.user_id')) 
      {
        if(Session::get('searchdata.basecategoryid')!='')
        {
                 $basecategory_id = Session::get('searchdata.basecategoryid');
        }
        else 
        {
             $basecategory_id = Session::get('searchdata.maincategoryid');
        }
        $city_id = Session::get('searchdata.cityid');
        $lang = Session::get('lang_file');
        $Formbusinesstype = new Formbusinesstype();
        $bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);            
            if(!empty($subcat_id)) 
            {
                $otherbranch = Category::select('mc_id','parent_id','mc_name','mc_name_ar')->where('city_id',$city_id)->where('parent_id',$shop_id)->where('mc_status',1)->get();
                if($lang == 'ar_lang') 
                {
                    $vendordetails = Category::select('mc_id','vendor_id','mc_name_ar as mc_name','mc_discription_ar as mc_discription','city_id','occasion_id','mc_img','mc_video_url','address_ar as address','mc_video_description as mc_video_description_ar','terms_conditions_ar as terms_conditions','longitude','latitude')->where('mc_id',$branch_id)->where('parent_id',$shop_id)->first();
                    $vendor_id = $vendordetails->vendor_id;

                    $productlist = DB::table('nm_services_attribute')->select('id','attribute_title_ar as attribute_title','write_on_jewellery')->where('services_id',$branch_id)->where('vendor_id',$vendor_id)->where('status',1)->get();
                    foreach($productlist as $value) 
                    {
                        $productAttribute = Products::where('attribute_id',$value->id)->where('pro_status',1)->where('pro_mr_id',$vendor_id)->where('pro_mc_id',$branch_id)->select('pro_id','pro_title_ar as pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc_ar as pro_desc','notes_ar as notes','pro_price','Insuranceamount','pro_disprice','attribute_id')->paginate(9)->appends(request()->query());
                         $value->cat_attribute = $productAttribute;

                        foreach($productlist[0]->cat_attribute as $serAttr) 
                        {
                           $product_id = $serAttr->pro_id;
                           $productAttribute = DB::table('nm_product_attribute')->select('id','vendor_id','attribute_title_ar as attribute_title','value')->where('product_id',$product_id)->where('vendor_id',$vendor_id)->where('status',1)->get();    
                           $serAttr->prodAttr = $productAttribute; 
                        }
                    }                
                } 
                else 
                {   
                    $vendordetails = Category::select('mc_id','vendor_id','mc_name','mc_discription','city_id','occasion_id','mc_img','mc_video_url','address','mc_video_description','terms_conditions','longitude','latitude')->where('mc_id',$branch_id)->where('parent_id',$shop_id)->first(); 
                    $vendor_id = $vendordetails->vendor_id;
                    $productlist = DB::table('nm_services_attribute')->select('id','attribute_title','write_on_jewellery')->where('services_id',$branch_id)->where('vendor_id',$vendor_id)->where('status',1)->get();
                    foreach($productlist as $value) 
                    {                        
                        $productAttribute = Products::where('attribute_id',$value->id)->where('pro_status',1)->where('pro_mr_id',$vendor_id)->where('pro_mc_id',$branch_id)->select('pro_id','pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc','notes','pro_price','Insuranceamount','pro_disprice','attribute_id')->paginate(9)->appends(request()->query());
                         $value->cat_attribute = $productAttribute;
                        foreach($productlist[0]->cat_attribute as $serAttr) 
                        {
                           $product_id = $serAttr->pro_id;
                           $productAttribute = DB::table('nm_product_attribute')->select('id','product_id','vendor_id','attribute_title','value')->where('product_id',$product_id)->where('vendor_id',$vendor_id)->where('status',1)->get();    
                           $serAttr->prodAttr = $productAttribute; 
                        }                        
                    }
                    //echo '<pre>';print_r($productlist[1]->cat_attribute);die;                                 
                }
            }

            $allreview = DB::table('nm_review')->where('shop_id',$branch_id)->where('review_type','shop')->where('status',1)->get();
            return view('newWebsite.jewelry',compact('vendordetails','productlist','bussinesstype','category_id','subcat_id','shop_id','branch_id','allreview','otherbranch'));
        } 
        else
        {
          return Redirect::to('login-signup');
        }      
     }

	 public function tailersdetail($id,$sid,$shop_id)
	 {	        
	    $category_id = $id;
	    $subcat_id   = $sid;
      $shop_id     = $shop_id;
        $getPagelimit = config('app.paginate');
        if(Session::has('customerdata.user_id')) 
        {
    	 	if(Session::get('searchdata.basecategoryid')!='')
    	 	{
                 $basecategory_id = Session::get('searchdata.basecategoryid');
        }
        else 
        {
             $basecategory_id = Session::get('searchdata.maincategoryid');
        }
        $city_id = Session::get('searchdata.cityid');
        $lang = Session::get('lang_file');
    		$Formbusinesstype = new Formbusinesstype();
    		$bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);            
            if(!empty($subcat_id)) 
            {
                if($lang == 'ar_lang') 
                {
                    $vendorinfo = Category::select('mc_id','vendor_id','mc_name_ar as mc_name','mc_discription_ar as mc_discription','city_id','occasion_id','mc_img','mc_video_url','address_ar as address','mc_video_description_ar as mc_video_description','terms_conditions_ar as terms_conditions','longitude','latitude')->where('mc_id',$shop_id)->where('parent_id',$subcat_id)->first();
                    $vendor_id = $vendorinfo->vendor_id;

                    $alldata = Products::where('pro_status',1)->where('pro_mr_id',$vendor_id)->where('pro_mc_id',$shop_id)->select('pro_id','pro_title','pro_title_ar as pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc','pro_desc_ar','notes','notes_ar','pro_price','Insuranceamount','pro_disprice','deliver_day')->paginate(9)->appends(request()->query()); //GET SUB CATEGORY LIST IN ARABIC
                } 
                else 
                {   
                    $vendorinfo = Category::select('mc_id','vendor_id','mc_name','mc_discription','city_id','occasion_id','mc_img','mc_video_url','address','mc_video_description','terms_conditions','longitude','latitude')->where('mc_id',$shop_id)->where('parent_id',$subcat_id)->first(); 
                    $vendor_id = $vendorinfo->vendor_id;

                    $alldata = Products::where('pro_status',1)->where('pro_mr_id',$vendor_id)->where('pro_mc_id',$shop_id)->select('pro_id','pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc','pro_desc_ar','notes','notes_ar','pro_price','Insuranceamount','pro_disprice','deliver_day')->paginate(9)->appends(request()->query());
                }
            }
            $allreview =   DB::table('nm_review')->where('shop_id',$shop_id)->where('review_type','shop')->where('status',1)->get();
            $vendordetails = $vendorinfo;
            $productlist   = $alldata;
            return view('shopping.tailersdetail',compact('vendordetails','productlist','bussinesstype','category_id','subcat_id','shop_id','allreview')); 
        } 
        else
        {
          return Redirect::to('login-signup');
        }      
     }
     
   public function dressesdetail($id,$sid,$shop_id)
   {          
        $category_id  = $id;
        $subcat_id    = $sid;
        $shop_id      = $shop_id;
        $getPagelimit = config('app.paginate');
        if(Session::has('customerdata.user_id')) 
        {
        if(Session::get('searchdata.basecategoryid')!='')
        {
            $basecategory_id = Session::get('searchdata.basecategoryid');
        }
        else 
        {
            $basecategory_id = Session::get('searchdata.maincategoryid');
        }
        $city_id = Session::get('searchdata.cityid');
        $lang = Session::get('lang_file');
        $Formbusinesstype = new Formbusinesstype();
        $bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);
        //echo $category_id.'-'.$subcat_id.'-'.$shop_id;die;            
        if(!empty($subcat_id)) 
        {
                if($lang == 'ar_lang') 
                {
                    $vendordetails = Category::select('mc_id','vendor_id','mc_name_ar as mc_name','mc_discription_ar as mc_discription','city_id','occasion_id','mc_img','mc_video_url','address_ar as address','mc_video_description as mc_video_description_ar','terms_conditions_ar as terms_conditions','longitude','latitude')->where('mc_id',$shop_id)->where('parent_id',$subcat_id)->first();
                    $vendor_id = $vendordetails->vendor_id;

                    $productlist = DB::table('nm_services_attribute')->select('id','attribute_title_ar as attribute_title')->where('services_id',$shop_id)->where('vendor_id',$vendor_id)->where('status',1)->get();
                    foreach($productlist as $value) 
                    {
                        $productAttribute = Products::where('attribute_id',$value->id)->where('pro_status',1)->where('pro_mr_id',$vendor_id)->where('pro_mc_id',$shop_id)->select('pro_id','pro_title_ar as pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc_ar as pro_desc','notes_ar as notes','pro_price','Insuranceamount','pro_disprice','attribute_id','pro_qty')->paginate(9)->appends(request()->query());
                         $value->cat_attribute = $productAttribute;
                    }
                } 
                else 
                {   
                    $vendordetails = Category::select('mc_id','vendor_id','mc_name','mc_discription','city_id','occasion_id','mc_img','mc_video_url','address','mc_video_description','terms_conditions','longitude','latitude')->where('mc_id',$shop_id)->where('parent_id',$subcat_id)->first(); 
                    $vendor_id = $vendordetails->vendor_id;

                    $productlist = DB::table('nm_services_attribute')->select('id','attribute_title')->where('services_id',$shop_id)->where('vendor_id',$vendor_id)->where('status',1)->paginate(9);

                    foreach($productlist as $value) 
                    {
                        $productAttribute = Products::where('attribute_id',$value->id)->where('pro_status',1)->where('pro_mr_id',$vendor_id)->where('pro_mc_id',$shop_id)->select('pro_id','pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc','notes','pro_price','Insuranceamount','pro_disprice','attribute_id','pro_qty')->paginate(9)->appends(request()->query());
                         $value->cat_attribute = $productAttribute;                                                
                    }
                }
            }
            $allreview = DB::table('nm_review')->where('shop_id',$shop_id)->where('review_type','shop')->where('status',1)->orderBy('created_at','desc')->paginate(2);
            //echo '<pre>';print_r($productlist);die;
            return view('newWebsite.dresses',compact('vendordetails','productlist','bussinesstype','category_id','subcat_id','shop_id','allreview'));
        } 
        else
        {
          return Redirect::to('login-signup');
        }      
   }

   public function abayareadymadedetail($id,$sid,$shop_id)
   {          
      $category_id  = $id;
      $subcat_id    = $sid;
      $shop_id      = $shop_id;
      $getPagelimit = config('app.paginate');
        if(Session::has('customerdata.user_id')) 
        {
        if(Session::get('searchdata.basecategoryid')!='')
        {
          $basecategory_id = Session::get('searchdata.basecategoryid');
        }
        else 
        {
          $basecategory_id = Session::get('searchdata.maincategoryid');
        }
        $city_id = Session::get('searchdata.cityid');
        $lang = Session::get('lang_file');
        $Formbusinesstype = new Formbusinesstype();
        $bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);            
            if(!empty($subcat_id)) 
            {
                if($lang == 'ar_lang') 
                {
                    $vendorinfo = Category::select('mc_id','vendor_id','mc_name_ar as mc_name','mc_discription_ar as mc_discription','city_id','occasion_id','mc_img','mc_video_url','address_ar as address','mc_video_description_ar as mc_video_description','terms_conditions_ar as terms_conditions','longitude','latitude')->where('mc_id',$shop_id)->where('parent_id',$subcat_id)->first();
                    $vendor_id = $vendorinfo->vendor_id;

                    $alldata = Products::where('parent_attribute_id',2)->where('pro_status',1)->where('pro_mr_id',$vendor_id)->where('pro_mc_id',$shop_id)->select('pro_id','pro_title','pro_title_ar as pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc_ar as pro_desc','notes_ar as notes','pro_price','Insuranceamount','pro_disprice')->paginate(9)->appends(request()->query());; //GET SUB CATEGORY LIST IN ARABIC
                } 
                else 
                {   
                    $vendorinfo = Category::select('mc_id','vendor_id','mc_name','mc_discription','city_id','occasion_id','mc_img','mc_video_url','address','mc_video_description','terms_conditions','longitude','latitude')->where('mc_id',$shop_id)->where('parent_id',$subcat_id)->first(); 
                    $vendor_id = $vendorinfo->vendor_id;

                    $alldata = Products::where('parent_attribute_id',2)->where('pro_status',1)->where('pro_mr_id',$vendor_id)->where('pro_mc_id',$shop_id)->select('pro_id','pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc','notes','pro_price','Insuranceamount','pro_disprice')->paginate(9)->appends(request()->query());
                }
            }
            $allreview =   DB::table('nm_review')->where('shop_id',$shop_id)->where('review_type','shop')->where('status',1)->paginate(2);
            $vendordetails = $vendorinfo;
            $productlist   = $alldata;
            return view('newWebsite.abaya-shop',compact('vendordetails','productlist','bussinesstype','category_id','subcat_id','shop_id','allreview'));
        } 
        else
        {
          return Redirect::to('login-signup');
        }      
     }
    
    public function abayatailersdetail($id,$sid,$shop_id)
    {          
      
      $category_id = $id;
      $subcat_id   = $sid;
      $shop_id     = $shop_id;
      $getPagelimit = config('app.paginate');
        if(Session::has('customerdata.user_id')) 
        {
        if(Session::get('searchdata.basecategoryid')!='')
        {
                 $basecategory_id = Session::get('searchdata.basecategoryid');
        }
        else 
        {
             $basecategory_id = Session::get('searchdata.maincategoryid');
        }
        $city_id = Session::get('searchdata.cityid');
        $lang = Session::get('lang_file');
        $Formbusinesstype = new Formbusinesstype();
        $bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);            
            if(!empty($subcat_id)) 
            {
                if($lang == 'ar_lang') 
                {
                    $vendorinfo = Category::select('mc_id','vendor_id','mc_name_ar as mc_name','mc_discription_ar as mc_discription','city_id','occasion_id','mc_img','mc_video_url','address_ar as address','mc_video_description_ar as mc_video_description')->where('mc_id',$shop_id)->where('parent_id',$subcat_id)->first();
                    $vendor_id = $vendorinfo->vendor_id;

                    $alldata = Products::where('parent_attribute_id',1)->where('pro_status',1)->where('pro_mr_id',$vendor_id)->where('pro_mc_id',$shop_id)->select('pro_id','pro_title_ar as pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc_ar as pro_desc','notes_ar as notes','pro_price','Insuranceamount','pro_disprice','pro_no_of_purchase')->paginate(9)->appends(request()->query());; //GET SUB CATEGORY LIST IN ARABIC
                } 
                else 
                {   
                    $vendorinfo = Category::select('mc_id','vendor_id','mc_name','mc_discription','city_id','occasion_id','mc_img','mc_video_url','address','mc_video_description')->where('mc_id',$shop_id)->where('parent_id',$subcat_id)->first(); 
                    $vendor_id = $vendorinfo->vendor_id;

                    $alldata = Products::where('parent_attribute_id',1)->where('pro_status',1)->where('pro_mr_id',$vendor_id)->where('pro_mc_id',$shop_id)->select('pro_id','pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc','notes','pro_price','Insuranceamount','pro_disprice','pro_no_of_purchase')->paginate(9)->appends(request()->query());
                }
            }
            $allreview =   DB::table('nm_review')->where('shop_id',$shop_id)->where('review_type','shop')->where('status',1)->get();
            $vendordetails = $vendorinfo;
            $productlist   = $alldata;
            return view('shopping.abayatailersdetail',compact('vendordetails','productlist','bussinesstype','category_id','subcat_id','shop_id','allreview')); 
        } 
        else
        {
          return Redirect::to('login-signup');
        }      
     }

    function getrentprice(Request $request)
    {
        $product_id     = $request->product_id;
        $vendor_id      = $request->vendor_id;
        $disprice = 0;
        $productinfo    = DB::table('nm_product_option_value')->where('product_id',$product_id)->where('vandor_id',$vendor_id)->select('price','discount_price')->get();
        $finalprice = '';
        foreach($productinfo as $value) 
        {
           if(!empty($value->price))
           {
             $price = $value->price;
           }
           elseif(!empty($value->discount_price) && $value->discount_price>0)
           {
             $disprice = $value->discount_price;
           }           
        }
        $finalprice = $price.'-'.$disprice;
        echo $finalprice;
    }

    function getbuyprice(Request $request)
    {
        $product_id     = $request->product_id;
        $vendor_id      = $request->vendor_id;
        $productinfo    = DB::table('nm_product')->where('pro_id',$product_id)->where('pro_mr_id',$vendor_id)->select('pro_price','pro_disprice')->first();
        if(isset($productinfo->pro_disprice) &&  ($productinfo->pro_disprice < $productinfo->pro_price) && ($productinfo->pro_disprice!='0'))
        {
          echo $productinfo->pro_price.'-'.$productinfo->pro_disprice;
        }
        else
        {
          echo $productinfo->pro_price.'-ok';
        }
    }    

    function getProductQuantity(Request $request)
    {
        $product_id     = $request->product_id;
        $qty            = $request->qty;
        $productinfo    = DB::table('nm_product')->where('pro_id',$product_id)->select('pro_qty')->first();
        if($qty <= @$productinfo->pro_qty)
        {
          echo 'ok';
        }
        else
        {
            echo "error";
        }
    }

    function getShoppingProductInfoWithGold(Request $request)
    {
        $product_id  = $request->product_id;
        $branch_id   = $request->branch_id;
        $vendor_id   = $request->vendor_id;
        $lang        = Session::get('lang_file');
        //echo $product_id.'-'.$branch_id.'-'.$vendor_id;die;
        if($lang == 'ar_lang') 
        {            
            $productdatalist = Products::where('pro_status',1)->where('pro_id',$product_id)->where('pro_mr_id',$vendor_id)->where('pro_mc_id',$branch_id)->select('pro_id','pro_title_ar as pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc_ar as pro_desc','notes_ar as notes','pro_price','Insuranceamount','pro_disprice','attribute_id','pro_qty','about_ar as about','pro_discount_percentage')->get();

            $productAttribute = DB::table('nm_product_attribute')->select('id','vendor_id','attribute_title_ar as attribute_title','value')->where('product_id',$product_id)->where('vendor_id',$vendor_id)->where('status',1)->get();
        } 
        else 
        {            
            $productdatalist = Products::where('pro_status',1)->where('pro_id',$product_id)->where('pro_mr_id',$vendor_id)->where('pro_mc_id',$branch_id)->select('pro_id','pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc','notes','pro_price','pro_qty','Insuranceamount','pro_disprice','attribute_id','about','pro_discount_percentage')->get();

            $productAttribute = DB::table('nm_product_attribute')->select('id','product_id','vendor_id','attribute_title','value')->where('product_id',$product_id)->where('vendor_id',$vendor_id)->where('status',1)->get();
        }
        $productshopfullinfo = array('productdata'=>$productdatalist,'productattribute'=>$productAttribute);
        return response()->json($productshopfullinfo);
    }

function checkqtybysize(Request $request){
      $product_id  = $request->productid;
        $option_title   = $request->sizevalue;
     $productattrsize = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title_ar as option_title','value')->where('product_id',$product_id)->where('option_title',$option_title)->where('value','!=','')->first();
          if($productattrsize->value>0){
          $getavaliableqty=1;
      }else{
          $getavaliableqty=0;
      }
      echo $getavaliableqty;



        //$productquantityavailable = array('productavailablity'=>$getavaliableqty);
        //return response()->json($productquantityavailable);
}
    function getShoppingProductDress(Request $request)
    {
        $product_id  = $request->product_id;
        $vendor_id   = $request->vendor_id;
        $lang       = Session::get('lang_file');
        if($lang == 'ar_lang') 
        {            
            $productdatalist = Products::where('pro_status',1)->where('pro_id',$product_id)->where('pro_mr_id',$vendor_id)->select('pro_id','pro_title_ar as pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc_ar as pro_desc','notes_ar as notes','pro_price','Insuranceamount','pro_disprice','attribute_id')->get();

            $productattrsize = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title_ar as option_title','value')->where('product_id',$product_id)->where('vandor_id',$vendor_id)->where('value','!=','')->orderBy('option_title')->get();
              if(count($productattrsize) > 0)
              {    
                $html ='<select class="checkout-small-box" id="product_size" name="product_size" required required onchange="morefrmoption(this.value,'.$product_id.','.$vendor_id.');"><option value="">تحديد</option>';
                foreach($productattrsize as $val)
                {
                  $html .='<option value="'.$val->option_title.'">Size '. str_replace('Size','',$val->option_title).'</option>';
                }
                $html .='</select>';
                $productattrsize1 = $html;
              }
              else
              {
                $productattrsize1     = 1;
              }

             $getAttr = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title_ar as option_title','value')->where('product_id',$product_id)->where('option_title','Rent')->where('vandor_id',$vendor_id)->first();
        } 
        else 
        {            
            $productdatalist = Products::where('pro_status',1)->where('pro_id',$product_id)->where('pro_mr_id',$vendor_id)->select('pro_id','pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc','notes','pro_price','Insuranceamount','pro_disprice','attribute_id')->get();

            $productattrsize = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title','value')->where('product_id',$product_id)->where('vandor_id',$vendor_id)->where('value','!=','')->orderBy('option_title')->get();
            if(count($productattrsize) > 0)
            {    
              $html ='<select class="checkout-small-box" id="product_size" name="product_size" required onchange="morefrmoption(this.value,'.$product_id.','.$vendor_id.');"><option value="">Select</option>';
              foreach($productattrsize as $val)
              {
                $html .='<option value="'.$val->option_title.'">Size '. str_replace('Size','',$val->option_title).'</option>';
              }
              $html .='</select>';
              $productattrsize1 = $html;
            }
            else
            {
              $productattrsize1 = 1;
            }

            $getAttr = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title','value')->where('product_id',$product_id)->where('option_title','Rent')->where('vandor_id',$vendor_id)->first();

        }
        $productshopfullinfo = array('productdata'=>$productdatalist,'productattrsize'=>$productattrsize1,'productattr'=>$getAttr);
        return response()->json($productshopfullinfo);
    }

    function getShoppingProductInfo(Request $request)
    {
        $id         = $request->product_id;
         $from         = $request->from;
        $vendorId   = $request->vendor_id;
        $lang       = Session::get('lang_file');
        $Current_Currency  = Session::get('currency');
        $html       = '';
        if($id != '' && $lang == 'ar_lang') 
        {
            $productdateshopinfo = DB::table('nm_product')->where('pro_status', 1)->where('pro_id', $id)->select('pro_id','pro_title_ar as pro_title','pro_Img','pro_desc_ar as pro_desc','pro_mr_id','pro_mc_id','pro_status','notes_ar as notes','pro_price','Insuranceamount','pro_disprice','pro_discount_percentage','pro_no_of_purchase')->orderBy('pro_title')->get();   

            $productattrsize = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title_ar as option_title','value','short_name')->where('product_id',$id)->where('vandor_id',$vendorId)->get();
             if(count($productattrsize) > 0)
              {    
                
if($from =='abaya')
{

 $html ='<select class="checkout-small-box" id="product_size" name="product_size" required><option value="">تحديد</option>';
                foreach($productattrsize as $val)
                {
                  $html .='<option value="'.$val->option_title.'">'. $val->option_title.'</option>';
                }
                $html .='</select>';
               


}
else
{
          $html ='<div class="checkout-form-bottom" id="ptattrsize">
          <div id="content-1" class="content mCustomScrollbar">
          <div class="fabs_op">';
          foreach($productattrsize as $val)
          {
          $OT =  $val->option_title;
          $Vid =  $vendorId;
          $getInfoc = DB::table('nm_fabric')->where('fabric_name_ar',$OT)->where('vendor_id',$Vid)->where('status',1)->count();
          if($getInfoc >=1){
          $getInfo = DB::table('nm_fabric')->where('fabric_name_ar',$OT)->where('vendor_id',$Vid)->where('status',1)->first();
          $val->fabric_price = $getInfo->price;
          $val->fabric_img = $getInfo->image;
          $val->fabric_name=$getInfo->fabric_name_ar;
          }
          else
          {
          $val->fabric_price = '';
          $val->fabric_img = ''; 
          $val->fabric_name='';
          }
           if($val->fabric_name!=''){
          $html .='<div class="fabric_block">';
          $html .='<div class="fabric_img_block"> <img src="'.$val->fabric_img.'"> </div>

          <div class="fabric_title_block">'.$val->fabric_name.'</div>
          <div class="fabric_price_block">  '.$Current_Currency.' '.$val->fabric_price.'</div>
          <div class="choose-line-radio"><input type="radio" value="'.$val->short_name.'" name="product_fabrics"   onchange="return showPriceDetail(this.value)">
          <label for="name">&nbsp;</label></div>
          </div>';
        }

          }


          $html .='</div></div>
          </div>
          </div>'; 

}
                




                $productattrsize1 = $html;
              }
              else
              {
                $productattrsize1     = 1;
              }
        }
        else
        {
           $productdateshopinfo = DB::table('nm_product')->where('pro_status',1)->where('pro_id',$id)->select('pro_id','pro_title','pro_Img','pro_desc','pro_mr_id','pro_mc_id','pro_status','notes','pro_price','Insuranceamount','notes','pro_disprice','pro_discount_percentage','pro_no_of_purchase')->orderBy('pro_title')->get();

           $productattrsize = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title','value','short_name')->where('product_id',$id)->where('vandor_id',$vendorId)->get();
             if(count($productattrsize) > 0)
              {    
 if($from =='abaya')
  {      
            $k=0;$jk=0;
          foreach($productattrsize as $val)
              {
                 if(isset($val->value) && $val->value!='' && $val->value>0){ $jk=$k+1; }
              }
              if($jk>0){
     $html ='<select class="checkout-small-box" id="product_size" name="product_size"><option value="">Select</option>';
              foreach($productattrsize as $val)
              {
                if(isset($val->value) && $val->value!='' && $val->value>0){
                $html .='<option value="'.$val->option_title.'">Size '. str_replace('Size', ' ', $val->option_title).'</option>';
                }
              }
              $html .='</select>';
            }else{
              if (Lang::has(Session::get('lang_file').'.Sold_Out')!= ''){ $st=trans(Session::get('lang_file').'.Sold_Out'); } else  { $st=trans($MER_OUR_LANGUAGE.'.Sold_Out'); }
              $html=$st;
            }
 
}
else
{

$html ='<div class="checkout-form-bottom" id="ptattrsize">
                <div id="content-1" class="content mCustomScrollbar">
                <div class="fabs_op">';
                foreach($productattrsize as $val)
                {
                $OT =  $val->option_title;
                $Vid =  $vendorId;
                $getInfoc = DB::table('nm_fabric')->where('fabric_name',$OT)->where('vendor_id',$Vid)->where('status',1)->count();
                if($getInfoc >=1){
                $getInfo = DB::table('nm_fabric')->where('fabric_name',$OT)->where('vendor_id',$Vid)->where('status',1)->first();
               $val->fabric_name=$getInfo->fabric_name;
                $val->fabric_price = $getInfo->price;
                $val->fabric_img = $getInfo->image;
                
                }
                else
                {
                $val->fabric_price = '';
                $val->fabric_img = '';
                $val->fabric_name='';
                }
                if($val->fabric_name!=''){
                $html .='<div class="fabric_block">';
                $html .='<div class="fabric_img_block"> <img src="'.$val->fabric_img.'"> </div>

                <div class="fabric_title_block">'.$val->fabric_name.'</div>
                <div class="fabric_price_block">  '.$Current_Currency.' '.$val->fabric_price.'</div>
                <div class="choose-line-radio"><input type="radio" value="'.$val->short_name.'" name="product_fabrics"   onchange="return showPriceDetail(this.value)">
                <label for="name">&nbsp;</label></div>
                </div>';
                  }
                } 
                $html .='</div></div>
                </div>
                </div>'; 

}








                $productattrsize1 = $html;
              }
            
            else
            {
              $productattrsize1 = 1;
            }      
        }
                  
        $productdatesshopfullinfo = array('productdateshopinfo'=>$productdateshopinfo,'productattrsize'=>$productattrsize1);
        return response()->json($productdatesshopfullinfo);
    }

    function getfabric(Request $request)
    {
       $id          = $request->product_id;
       $fabric_name = $request->product_fabric;
       $productdata = DB::table('nm_product')->where('pro_status', 1)->where('pro_id',$id)->select('pro_id','pro_mr_id','pro_mc_id','pro_price','Insuranceamount','pro_disprice')->first();
       
       if(count($productdata) > 0)
       {
          $fabricdata = DB::table('nm_fabric')->where('id',$fabric_name)->select('price')->first();
          if(isset($productdata->pro_disprice) && $productdata->pro_disprice!='0' && $productdata->pro_disprice!='0.00'){$getPrice = $productdata->pro_disprice;}else{$getPrice = $productdata->pro_price;} 
          $totalprice = ($getPrice+$fabricdata->price); 
          echo $totalprice;
       }   
    }

    function getShoppingProductInfoTailor(Request $request)
    {
        $id         = $request->product_id;
        $vendorId   = $request->vendor_id;
        $lang       = Session::get('lang_file');
        $html       = '';
        if($id != '' && $lang == 'ar_lang') 
        {
            $productdateshopinfo = DB::table('nm_product')->where('pro_status', 1)->where('pro_id', $id)->select('pro_id','pro_title_ar as pro_title','pro_Img','pro_desc_ar as pro_desc','pro_mr_id','pro_mc_id','pro_status','notes_ar as notes','pro_price','Insuranceamount','pro_disprice','deliver_day')->orderBy('pro_title')->get();   

            $productattrsize = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title_ar as option_title','value','short_name')->where('product_id',$id)->where('vandor_id',$vendorId)->orderBy('option_title', 'asc')->get();
              if(count($productattrsize) > 0)
              {    
                

                $html ='<div class="checkout-form-bottom" id="ptattrsize">
                <div id="content-1" class="content mCustomScrollbar">
                <div class="fabs_op">';
                foreach($productattrsize as $val)
                {
                $OT =  $val->option_title;
                $Vid =  $vendorId;
                $getInfoc = DB::table('nm_fabric')->where('fabric_name_ar',$OT)->where('vendor_id',$Vid)->count();
                if($getInfoc >=1){
                $getInfo = DB::table('nm_fabric')->where('fabric_name_ar',$OT)->where('vendor_id',$Vid)->first();
                $val->fabric_price = $getInfo->price;
                $val->fabric_img = $getInfo->image;
                }
                else
                {
                $val->fabric_price = '';
                $val->fabric_img = ''; 
                }

                $html .='<div class="fabric_block">';
                $html .='<div class="fabric_img_block"> <img src="'.$val->fabric_img.'"> </div>

                <div class="fabric_title_block">'.$val->option_title.'</div>
                <div class="fabric_price_block">  '.$val->fabric_price.'</div>
                <div class="choose-line-radio"><input type="radio" value="'.$val->short_name.'" name="product_fabrics"   onchange="return showPriceDetail(this.value)">
                <label for="name">&nbsp;</label></div>
                </div>';

                }


                $html .='</div></div>
                </div>
                </div>'; 




                $productattrsize1 = $html;
              }
             
        }
        else
        {
           $productdateshopinfo = DB::table('nm_product')->where('pro_status',1)->where('pro_id',$id)->select('pro_id','pro_title','pro_Img','pro_desc','pro_mr_id','pro_mc_id','pro_status','notes','pro_price','Insuranceamount','notes','pro_disprice','deliver_day')->orderBy('pro_title')->get();

           $productattrsize = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title','value','short_name')->where('product_id',$id)->where('vandor_id',$vendorId)->orderBy('option_title', 'asc')->get();
            if(count($productattrsize) > 0)
            {    

$html ='<div class="checkout-form-bottom" id="ptattrsize"><div id="content-1" class="content mCustomScrollbar">
                <div class="fabs_op">';
      foreach($productattrsize as $val)
                  {
                  $OT =  $val->option_title;
                  $Vid =  $vendorId;
                  $getInfoc = DB::table('nm_fabric')->where('fabric_name',$OT)->where('vendor_id',$Vid)->count();
                  if($getInfoc >=1){
                  $getInfo = DB::table('nm_fabric')->where('fabric_name',$OT)->where('vendor_id',$Vid)->first();
                  $val->fabric_price = $getInfo->price;
                  $val->fabric_img = $getInfo->image;
                  }
                  else
                  {
                  $val->fabric_price = '';
                  $val->fabric_img = ''; 
                  }
 
        $html .='<div class="fabric_block">';
        $html .='<div class="fabric_img_block"> <img src="'.$val->fabric_img.'"> </div>
       
        <div class="fabric_title_block">'.$val->option_title.'</div>
         <div class="fabric_price_block">  '.$val->fabric_price.'</div>
        <div class="choose-line-radio"><input type="radio" value="'.$val->short_name.'" name="product_fabrics"   onchange="return showPriceDetail(this.value)">
        <label for="name">&nbsp;</label></div>
        </div>';
 
                  }


$html .='</div></div>
</div>
</div>'; 



              $productattrsize1 = $html;
            }
            else
            {
              $productattrsize1 = 1;
            }      
        }
                  
        $productdatesshopfullinfo = array('productdateshopinfo'=>$productdateshopinfo,'productattrsize'=>$productattrsize1);
        return response()->json($productdatesshopfullinfo);
    }


    function addtocartofgoldAndperfume(Request $request)
    {
        //echo '<pre>';print_r($_REQUEST);die;
        $getCartProID = 0;
        $user_id            = Session::get('customerdata.user_id');
        $category_id        = $request->category_id;
        $subcat_id          = $request->subcat_id;
        $shop_id            = $request->shop_id;
        $branch_id          = $request->branch_id;
        $itemqty            = $request->itemqty;
        $actiontype         = $request->actiontype;
        $cart_sub_type      = $request->cart_sub_type;
        $cart_type          = $request->cart_type;
        $attribute_id       = $request->attribute_id;
        $product_id         = $request->product_id;
        $priceId            = $request->priceId;
        $vendor_id          = $request->vendor_id;
        $write_on_your_ring = $request->write_on_your_ring;
        $product            = Products::where('pro_id',$product_id)->first();
        if(!empty($product)) 
        {
            $cart = Cart::where('user_id',$user_id)->first();
            if(!empty($cart)) 
            {
              //$cart_pro = CartProduct::where('cart_type',$cart_type)->where('cart_id',$cart->id)->where('product_id',$product_id)->delete();
               //$cart_service_attribute = CartServiceAttribute::where('cart_type',$cart_type)->where('cart_id',$cart->id)->where('product_id',$product_id)->delete();
            } 
            else 
            {
                $cart_data = array();
                $cart_data['user_id'] = $user_id;
                $cart = Cart::create($cart_data); //cart entery
            }
            $cart_product_data                  = array();
            $cart_product_data['cart_type']     = $cart_type;
            $cart_product_data['cart_sub_type'] = $cart_sub_type;
            $cart_product_data['cart_id']       = $cart->id;
            $cart_product_data['category_id']   = $product->pro_mc_id;
            $cart_product_data['product_id']    = $product_id;
            $cart_product_data['quantity']      = $itemqty;
            $cart_product_data['merchant_id']   = $product->pro_mr_id;
            $cart_product_data['pro_title']     = $product->pro_title;
            $cart_product_data['pro_title_ar']  = $product->pro_title_ar;
            $cart_product_data['pro_Img']       = $product->pro_Img;
            $cart_product_data['pro_price']       = $priceId;
            $cart_product_data['review_type']   = 'shop';
            $cart_product_data['shop_vendor_id']= $product->pro_mr_id;
            $cart_product_data['shop_id']       = $branch_id;
            $cart_product_data['total_price']   = ($priceId*$itemqty);

            if($write_on_your_ring)
            {
              $cart_product_data['product_size'] = $write_on_your_ring;  
            }                    
            $cart_product_data['status']        = 1;
            $cart_pro = CartProduct::where('cart_type',$cart_type)->where('cart_id',$cart->id)->where('product_id',$product_id)->where('shop_id',$branch_id)->first();
            if(isset($cart_pro) && $cart_pro!='')
            {
              $qty         = ($cart_pro->quantity+$itemqty);
              $total_price = ($priceId*$qty);
              $update_cart = CartProduct::find($cart_pro->id)->update(['quantity'=>$qty,'total_price'=>$total_price]);
              $getCartProID = $cart_pro->id; 
            }
            else
            {
              $cart_product = CartProduct::create($cart_product_data); //cart product entry 
              $getCartProID = $cart_product->id;  
              if(isset($cart_sub_type) && $cart_sub_type=='gold'){
                  $product_attr = ProductAttribute::select('id','product_id','attribute_title_ar','attribute_title','value')->where('product_id',$product_id)->get();
                        if(!empty($product_attr)){
                            foreach($product_attr as $key => $value) {
                        
                        $cart_product_attr = array();
                        $cart_product_attr['cart_id'] = $cart->id;
                        $cart_product_attr['cart_type'] = $cart_type;
                        $cart_product_attr['product_id'] = $product_id;
                        $cart_product_attr['attribute_title'] = $value->attribute_title;
                        $cart_product_attr['attribute_title_ar'] = $value->attribute_title_ar;
                        $cart_product_attr['value'] = $value->value;
                        $cart_product_attr['cart_product_id'] = $getCartProID; 
                        CartProductAttribute::create($cart_product_attr); //Product Attribute entry
                            }
                        }
                      }           
            }            
            
            $countData = CartServiceAttribute::where('cart_type',$cart_type)->where('cart_id',$cart->id)->where('product_id',$product_id)->count();
            if($countData > 0){}else{
              $cart_attribute_data                 = array();
              $cart_attribute_data['cart_type']    = $cart_type;
              $cart_attribute_data['cart_id']      = $cart->id;
              $cart_attribute_data['category_id']  = $product->pro_mc_id;
              $cart_attribute_data['product_id']   = $product_id;
              $cart_attribute_data['attribute_id'] = $attribute_id;
              $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry
            } 
            

        }
        if(\Config::get('app.locale') == 'ar')
        {
          Session::flash('status', "وأضاف المنتج في العربة.");
        }
        else
        {
          Session::flash('status', "Product added in the cart.");
        }                  
      return redirect($actiontype.'/'.$category_id.'/'.$subcat_id.'/'.$shop_id.'/'.$branch_id);
    }

    function addtocartofdress(Request $request)
    {

     //   dd($request->all());

        $user_id            = Session::get('customerdata.user_id');       
       // echo '<pre>';print_r($_REQUEST); subcat_id
        $category_id        = $request->category_id;
        $subcat_id          = $request->subcat_id;
        $shop_id            = $request->shop_id;
        $itemqty            = $request->itemqty;
        $actiontype         = $request->actiontype;
        $cart_sub_type      = $request->cart_sub_type;
        $cart_type          = $request->cart_type;
        $attribute_id       = $request->attribute_id;
        $product_id         = $request->product_id;
        $priceId            = $request->priceId;
        $vendor_id          = $request->vendor_id;
        $write_on_your_ring = $request->write_on_your_ring;
        $product_size       = $request->product_size;
        $buy_rent           = $request->buy_rent;
        $insuranceamount    = $request->insuranceamount;
        $rental_time        = $request->rental_time;
        $return_time        = $request->return_time;
        $product            = Products::where('pro_id',$product_id)->first();
        if(!empty($product)) 
        {
            $product_options_value = ProductOptionValue::select('id','product_id','price','product_option_id')->where('vandor_id',$vendor_id)->where('product_id',$product_id)->where('option_title','Rent')->first();
            $cart = Cart::where('user_id',$user_id)->first();
            if(!empty($cart)) 
            {
              //$cart_pro = CartProduct::where('cart_type','shopping')->where('cart_id',$cart->id)->where('product_id',$product_id)->delete();
              //$cart_service_attribute = CartServiceAttribute::where('cart_type','shopping')->where('cart_id',$cart->id)->where('product_id',$product_id)->delete();
              //$cart_pro_rent = CartProductRent::where('cart_type','shopping')->where('cart_id',$cart->id)->where('product_id',$product_id)->delete();
              //$cart_pro_opt = CartOption::where('cart_type','shopping')->where('cart_id',$cart->id)->where('product_id',$product_id)->delete();
              //$cart_pro_val = CartOptionValue::where('cart_type','shopping')->where('cart_id',$cart->id)->where('product_id',$product_id)->delete();
            } 
            else 
            {
                $cart_data = array();
                $cart_data['user_id'] = $user_id;
                $cart = Cart::create($cart_data); //cart entery
            }

            $cart_product_data                     = array();
            $cart_product_data['cart_type']        = $cart_type;
            $cart_product_data['cart_sub_type']    = $cart_sub_type;
            $cart_product_data['cart_id']          = $cart->id;
            $cart_product_data['category_id']      = $product->pro_mc_id;
            $cart_product_data['product_id']       = $product_id;
            $cart_product_data['quantity']         = $itemqty;
            $cart_product_data['buy_rent']         = $buy_rent;
            $cart_product_data['review_type']      = 'shop';

            if(!empty($rental_time)) 
            {
                $date1 = date_create($rental_time);
                $date2 = date_create($return_time);
                $diff  = date_diff($date1,$date2);
                $diff->format("%H:%I"); 
                $hourdiff = round((strtotime($return_time) - strtotime($rental_time))/3600, 1);
              
                $cart_product_data['total_price'] = ($product->Insuranceamount*$itemqty) + ($priceId * $itemqty)*$hourdiff;

            } 
            else 
            {
               $cart_product_data['total_price']      = ($priceId*$itemqty);
            } 
           
            $cart_product_data['product_size']     = $product_size;
            $cart_product_data['status']           = 1;
            $cart_product_data['merchant_id']      = $vendor_id;
            $cart_product_data['shop_id']          = $shop_id;
            $cart_product_data['pro_title']        = $product->pro_title;
            $cart_product_data['pro_title_ar']     = $product->pro_title_ar;
            $cart_product_data['pro_desc']         = $product->pro_desc;
            $cart_product_data['pro_desc_ar']      = $product->pro_desc_ar;
            $cart_product_data['pro_Img']          = $product->pro_Img;
            $cart_product_data['pro_price']        = $product->pro_price;
            $cart_product_data['insurance_amount'] = $product->Insuranceamount;
            $cart_product_data['shop_vendor_id'] = $subcat_id;
            

            $cart_pro = CartProduct::where('cart_type',$cart_type)->where('cart_id',$cart->id)->where('product_id',$product_id)->where('product_size',$product_size)->where('shop_id',$shop_id)->where('buy_rent',$buy_rent)->first();
            if(isset($cart_pro) && $cart_pro!='')
            {
              $qty = ($cart_pro->quantity+$itemqty);
              if(!empty($rental_time)) 
              {
                  $date1 = date_create($rental_time);
                  $date2 = date_create($return_time);
                  $diff  = date_diff($date1,$date2);
                  $diff->format("%H:%I"); 
                  $total_price = $product->Insuranceamount + ($priceId * $qty)*$diff->format("%H");

              } 
              else 
              {
                 $total_price      = ($priceId*$qty);
              } 
              //$total_price = ($priceId*$qty);
              $update_cart = CartProduct::find($cart_pro->id)->update(['quantity'=>$qty,'total_price'=>$total_price]);
            }
            else
            {  
              $cart_product = CartProduct::create($cart_product_data); //cart product entry
              $probaseid=$cart_product->id; 
            } 
              
            $cart_attribute_data                 = array();
            $cart_attribute_data['cart_type']    = $cart_type;
            $cart_attribute_data['cart_id']      = $cart->id;
            $cart_attribute_data['category_id']  = $product->pro_mc_id;
            $cart_attribute_data['product_id']   = $product_id;
            $cart_attribute_data['attribute_id'] = $attribute_id;

            $service_attribute = CartServiceAttribute::where('cart_id',$cart->id)->where('product_id',$product_id)->where('category_id',$product->pro_mc_id)->where('cart_type','shopping')->count();
            if($service_attribute > 0){}else{
             $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry   
            }            

            if(!empty($rental_time))
            {
                $cart_product_rent                     = array();
                $cart_product_rent['cart_id']          = $cart->id;
                $cart_product_rent['cart_type']        = $cart_type;
                $cart_product_rent['service_id']       = $product->pro_mc_id;
                $cart_product_rent['product_id']       = $product_id;
                $cart_product_rent['rental_date']      = date("Y-m-d", strtotime($rental_time));                   
                $cart_product_rent['return_date']      = date("Y-m-d", strtotime($return_time));
                $cart_product_rent['rental_time']      = $rental_time;
                $cart_product_rent['return_time']      = $return_time;
                $cart_product_rent['quantity']         = $itemqty;
                $cart_product_rent['insurance_amount'] = $product->Insuranceamount;
                if(isset($probaseid) && $probaseid!='')
                {
                 $cart_product_rent['cart_product_id'] = $probaseid; 
                }
                

                $cart_rent = CartProductRent::where('cart_id',$cart->id)->where('product_id',$product_id)->where('service_id',$shop_id)->where('cart_type',$cart_type)->first();
                if(isset($cart_rent) && $cart_rent!='')
                {
                  $update_cart = CartProductRent::find($cart_rent->id)->update($cart_product_rent);
                }
                else
                {
                  CartProductRent::create($cart_product_rent); //Product rental date and time entry     
                }

                
            }
           // if(!empty($rental_time)) 
           // {
                $cart_option_data                      = array();
                $cart_option_data['cart_type']         = $cart_type;
                $cart_option_data['product_option_id'] = $product_options_value->product_option_id;
                $cart_option_data['cart_id']           = $cart->id;
                $cart_option_data['product_id']        = $product_id;
                $service_attribute = CartOption::where('cart_id',$cart->id)->where('product_id',$product_id)->where('cart_type',$cart_type)->count();
                if($service_attribute > 0){}else{
                 CartOption::create($cart_option_data); //cart option entry  
                } 
               

                $cart_option_value                            = array();
                $cart_option_value['cart_type']               = $cart_type;
                $cart_option_value['cart_id']                 = $cart->id;
                $cart_option_value['product_id']              = $product_id;
                $cart_option_value['product_option_id']       = $product_options_value->product_option_id;
                $cart_option_value['product_option_value_id'] = $product_options_value->id;
                $cart_option_value['quantity']                = $itemqty;
                $cart_option_value['value']                   = $priceId;
                $cart_option_value['status']                  = 1;
                $cart_option = CartOptionValue::where('cart_id',$cart->id)->where('product_id',$product_id)->where('cart_type',$cart_type)->first();

                
                if(isset($cart_optioncart_option) && $cart_option!='')
                {  
                  $update_cart = CartProductRent::find($cart_option->id)->update($cart_option_value);
                }
                else
                {  
                 $cart_value = CartOptionValue::create($cart_option_value); //cart option value entry
                }
                
           // } 
        }
 
        if(\Config::get('app.locale') == 'ar')
        {
          Session::flash('status', "وأضاف المنتج في العربة.");
        }
        else
        {
          Session::flash('status', "Product added in the cart.");
        }                  
      return redirect($actiontype.'/'.$category_id.'/'.$subcat_id.'/'.$shop_id);
    }
    
    function addtocartforshopping(Request $request)
    {

        $user_id            = Session::get('customerdata.user_id');
        $category_id        = $request->category_id;
        $subcat_id          = $request->subcat_id;
        $shop_id            = $request->shop_id;
        $itemqty            = $request->itemqty;
        if($itemqty==''){ $itemqty=1; }
        $product_id         = $request->product_id;
      
        $vendor_id          = $request->vendor_id;
        $actiontype         = $request->actiontype;
        $cart_type          = $request->cart_type;
        $length             = $request->length;
        $chest_size         = $request->chest_size;
        $waist_size         = $request->waist_size;
        $shoulders          = $request->shoulders;
        $neck               = $request->neck;
        $arm_length         = $request->arm_length;
        $wrist_diameter     = $request->wrist_diameter;
        $cutting            = $request->cutting;
        $cart_sub_type      = $request->cart_sub_type;
        $product_size       = $request->product_size;
        $product_fabrics    = $request->fabric_type;
        $product            = Products::where('pro_id',$product_id)->first();
        $fabricinfoprice=0;
        $fabricinfo            = DB::table('nm_fabric')->where('id',$product_fabrics)->first();
         if(!empty($fabricinfo)){
          $fabricinfoprice=$fabricinfo->price;
        } 
        if($cart_sub_type=='abaya'){
         $priceId            = $request->priceId + $fabricinfoprice;
        }else{
         $priceId            = $request->priceId; 
        }

        //echo '<pre>';print_r($_REQUEST);die;

        //dd($product);
        if(!empty($product)) 
        {
          $cart = Cart::where('user_id',$user_id)->first();
          if(!empty($cart)) 
          {
             // dd($cart);//cart entery

              // $cart_pro = CartProduct::where('cart_type',$cart_type)->where('cart_id',$cart->id)->where('product_id',$product_id)->delete();
             //  $cart_body_measurement = CartBodyMeasurement::where('cart_id',$cart->id)->where('product_id',$product_id)->delete();
          } 
          else 
          {
              $cart_data = array();
              $cart_data['user_id'] = $user_id;
              $cart = Cart::create($cart_data);
           //   dd($cart);//cart entery
          }
          $cart_product_data                  = array();
          $cart_product_data['cart_type']     = $cart_type;
          $cart_product_data['cart_sub_type'] = $cart_sub_type;
          $cart_product_data['cart_id']       = $cart->id;
          $cart_product_data['product_id']    = $product_id;
          $cart_product_data['category_id']   = $product->pro_mc_id;
          $cart_product_data['quantity']      = $itemqty;
          if(isset($itemqty) && $itemqty!='')
          {
            $cart_product_data['total_price']   = ($priceId*$itemqty);
          }
          else
          {
            $cart_product_data['total_price']   = ($priceId*1);
          }          
          $cart_product_data['product_size']  = $product_size;
          $cart_product_data['fabric_name']   = $product_fabrics;
          $cart_product_data['merchant_id']   = $vendor_id;
          $cart_product_data['review_type']   = 'shop';
          $cart_product_data['shop_id']       = $shop_id;
          $cart_product_data['shop_vendor_id']= $product->pro_mr_id;
          $cart_product_data['deliver_day']= $product->deliver_day;
          $cart_product_data['status']        = 1;
          $cart_product_data['pro_title']= $product->pro_title;
          $cart_product_data['pro_title_ar']= $product->pro_title_ar;
          $cart_product_data['pro_desc']= $product->pro_desc;
          $cart_product_data['pro_desc_ar']= $product->pro_desc_ar;
          $cart_product_data['pro_Img']= $product->pro_Img;
          if(isset($product->pro_discount_percentage) && $product->pro_discount_percentage>0){
            $ppric=$product->pro_disprice;
          }else{
            $ppric=$product->pro_price;
          }
          $cart_product_data['pro_price']= $ppric;
            
            $checkinproductsection = CartProduct::where('merchant_id',$vendor_id)->where('shop_id',$shop_id)->where('cart_type',$cart_type)->where('cart_sub_type',$cart_sub_type)->where('cart_id',$cart->id)->where('product_id',$product_id)->where('fabric_name',$product_fabrics)->count();


            $checkinproductc = CartBodyMeasurement::where('cart_id',$cart->id)->where('product_id',$product_id)->where('customer_id',$user_id)->where('length',$length)->where('chest_size',$chest_size)->where('waistsize',$waist_size)->where('soulders',$shoulders)->where('neck',$neck)->where('arm_length',$arm_length)->where('wrist_diameter',$wrist_diameter)->where('status',1)->count();

            if($checkinproductc >=1 && $checkinproductsection >=1)
            {
               $cart_pro = CartProduct::where('cart_type',$cart_type)->where('cart_id',$cart->id)->where('product_id',$product_id)->where('shop_id',$shop_id)->where('fabric_name',$product_fabrics)->first();
               $qty         = ($cart_pro->quantity+$itemqty);

               $total_price = ($priceId*$qty);
               $update_cart = CartProduct::find($cart_pro->id)->update(['quantity'=>$qty,'total_price'=>$total_price]);
            }
            else
            {
              if(!empty($length))
              {
                //  dd($length);
                $cart_product = CartProduct::create($cart_product_data); //cart product entry

                  //dd($cart_product);
              }
              else
              {
                $procount = CartProduct::where('merchant_id',$vendor_id)->where('shop_id',$shop_id)->where('cart_type',$cart_type)->where('cart_sub_type',$cart_sub_type)->where('cart_id',$cart->id)->where('product_id',$product_id)->where('product_size',$product_size)->count();               
               // dd($procount);
                if($procount > 0)
                {
                    $cart_pro = CartProduct::where('cart_type',$cart_type)->where('cart_id',$cart->id)->where('product_id',$product_id)->where('shop_id',$shop_id)->where('product_size',$product_size)->first();
                    $qty         = ($cart_pro->quantity+$itemqty);
                    $total_price = ($priceId*$qty);
                    $update_cart = CartProduct::find($cart_pro->id)->update(['quantity'=>$qty,'total_price'=>$total_price]);
                }
                else
                {
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                }
              }
          if($actiontype=='abayatailersdetail' || $actiontype=='tailersdetail')
          {
            $cart_body_measurement = array();

          // dd($length);
            if(!empty($length))
            {
              
              $cart_body_measurement['cart_id']         = $cart->id;
              $cart_body_measurement['cart_product_id'] = $cart_product->id;
              $cart_body_measurement['customer_id']     = $user_id;
              $cart_body_measurement['product_id']      = $product_id;
              $cart_body_measurement['length']          = $length;                    
              $cart_body_measurement['chest_size']      = $chest_size;                 
              $cart_body_measurement['waistsize']       = $waist_size;                  
              $cart_body_measurement['soulders']        = $shoulders;
              $cart_body_measurement['neck']            = $neck;
              $cart_body_measurement['arm_length']      = $arm_length;                    
              $cart_body_measurement['wrist_diameter']  = $wrist_diameter;    
              $cart_body_measurement['cutting']         = $cutting; 
              $cart_body_measurement['status']          = 1;  
              
              $insert_cart = CartBodyMeasurement::create($cart_body_measurement); //Product rental date and time entry               
               
            }
          } 
            }
            







          /*$cart_pro = CartProduct::where('cart_type',$cart_type)->where('cart_id',$cart->id)->where('product_id',$product_id)->where('shop_id',$shop_id)->first();

          if(isset($cart_pro) && $cart_pro!='')
          {
              $qty         = ($cart_pro->quantity+$itemqty);
              $total_price = ($priceId*$qty);
              $update_cart = CartProduct::find($cart_pro->id)->update(['quantity'=>$qty,'total_price'=>$total_price]);
          }
          else
          {
              $cart_product = CartProduct::create($cart_product_data); //cart product entry 
          }*/         

          
          if(\Config::get('app.locale') == 'ar')
          {
              Session::flash('status', "وأضاف المنتج في العربة.");
          }
          else
          {
              Session::flash('status', "Product added in the cart.");
          }
        }              
        return redirect($actiontype.'/'.$category_id.'/'.$subcat_id.'/'.$shop_id);
    }

    public function checkbookedtime(Request $request)
    {
$docid = $request->product_id;
$shop_id = $request->branchid;
$date = strtotime($request->chk);
 

   
         
       $getClinicInfo =  DB::table('nm_order_services_staff')->where('product_type','clinic')->where('service_id',$docid)->where('shop_id',$shop_id)->get();
      $getContent=array();


      //print_r($getClinicInfo); die;

       foreach ($getClinicInfo as $key => $value) {
          $getBookingD = strtotime($value->booking_date);
          $TimeD = $value->start_time;
          if($getBookingD == $date)
          {
        
 
$time = strtotime($TimeD);
 
$endTime = date("h:i A", strtotime('+29 minutes', $time));


            $TimeDS = $TimeD.'~~'.$endTime;


        array_push($getContent, $TimeDS);
          }
       }

return $getContent;
 






    }
}

<?php
namespace App\Http\Controllers;
use DB;
use Session;
use App\Http\Models;
use App\Register;
use App\Home;
use App\City;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Merchantadminlogin;
use App\AdminModel;
use App\AdminUserGroup;
use App\AdminMenu;
use Lang;
use Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
class AdminController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function __construct(){
        parent::__construct();
        // set admin Panel language
        $this->setLanguageLocaleAdmin();
    }


	public function chart()
	{
	   $result = AdminModel::get_chart_details();
	   return view('siteadmin.chart_view');
	}

	public function admin_settings()
	{
           if(Session::has('userid'))
	   {
		   	if(Lang::has(Session::get('admin_lang_file').'.BACK_SETTINGS')!= '') 
			{ 
				$session_message = trans(Session::get('admin_lang_file').'.BACK_SETTINGS');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SETTINGS');
			}	
	   $adminheader = view('siteadmin.includes.admin_header')->with("routemenu",$session_message);	
	   $adminfooter = view('siteadmin.includes.admin_footer');
	   $admin_setting_details = AdminModel::get_admin_details();
	   $cities  = City::where('ci_con_id','=',$admin_setting_details[0]->adm_co_id)->where('ci_status','=',1)->orderBy('ci_name')->get();
	   $country_return = Merchant::get_country_detail();
	   $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
	   return view('siteadmin.admin_settings')->with('adminheader', $adminheader)->with('adminfooter', $adminfooter)->with('admin_setting_details' , $admin_setting_details)->with('country_details',$country_return)->with('cities',$cities)->with('admin_menu',$admin_menu);
	   }
	   else
           {
           return Redirect::to('siteadmin');
           }
	}
	
	public function admin_profile()
	{
	  if(Session::has('userid'))
	  {
		  	if(Lang::has(Session::get('admin_lang_file').'.BACK_NULL')!= '') 
			{ 
				$session_message = trans(Session::get('admin_lang_file').'.BACK_NULL');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_NULL');
			}	
	  $adminheader = view('admin.common.admin_header')->with("routemenu",$session_message);	
	  $adminfooter = view('siteadmin.includes.admin_footer');
	  $admin_setting_details = AdminModel::get_admin_profile_details();
	  $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
	  return view('siteadmin.admin_profile')->with('adminheader', $adminheader)->with('adminfooter', $adminfooter)->with('admin_setting_details' , $admin_setting_details)->with('admin_menu',$admin_menu);
	  }
	  else
          {
          return Redirect::to('siteadmin');
          }
	}

	public function admin_settings_submit()
	{

	  $data = Input::except(array('_token')) ;
        $rule = array(
          'first_name' => 'required|alpha_dash',
	  'last_name' => 'required|alpha_dash',
      'email' => 'required|email',
	  'phone' => 'required|numeric',
	  'address_one' => 'required',
      'country' => 'required',
	  'city' => 'required'					
           );
          $validator = Validator::make($data,$rule);			
           if ($validator->fails())
           {
	   return Redirect::to('admin_settings')->withErrors($validator->messages())->withInput();
           }
           else
           {          
           	
           $npwd=	Input::get('upassword');
           if(isset($npwd) && $npwd!=''){
           	$entry  =  array(
            'adm_fname' => Input::get('first_name'),
	    'adm_lname' => Input::get('last_name'),
	    'adm_email'	=> Input::get('email'),
	    'adm_phone' => Input::get('phone'),
	    'adm_address1' => Input::get('address_one'),
	    'adm_address2' => Input::get('address_two'),
	    'adm_ci_id' => Input::get('city'),
	    'adm_co_id' => Input::get('country'),
	    'adm_password' => bcrypt($npwd),					
            );

           }else{
	    $entry  =  array(
            'adm_fname' => Input::get('first_name'),
	    'adm_lname' => Input::get('last_name'),
	    'adm_email'	=> Input::get('email'),
	    'adm_phone' => Input::get('phone'),
	    'adm_address1' => Input::get('address_one'),
	    'adm_address2' => Input::get('address_two'),
	    'adm_ci_id' => Input::get('city'),
	    'adm_co_id' => Input::get('country'),					
            );
	    }
          $email_match =  AdminModel::where('adm_email','=',Input::get('email'))->where('adm_id','!=',session::get('userid'))->first();
		    if (isset($email_match)){
		    	if(Lang::has(Session::get('admin_lang_file').'.BACK_THIS_EMAIL_ALREADY_REGISTER')!= '') 
			{ 
				$session_message = trans(Session::get('admin_lang_file').'.BACK_THIS_EMAIL_ALREADY_REGISTER');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_THIS_EMAIL_ALREADY_REGISTER');
			}	
			Session::put('warning', $session_message);
			return Redirect::to('admin_settings');
		    }

	    $country_return = AdminModel::update_admin_details($entry);
		if(Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_UPDATED_SUCCESSFULLY')!= '') 
			{ 
				$session_message = trans(Session::get('admin_lang_file').'.BACK_RECORD_UPDATED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_RECORD_UPDATED_SUCCESSFULLY');
			}	
	    return Redirect::to('admin_settings')->with('success', $session_message);
	   }
	}

	public function siteadmin()
	{
		
       if(Session::has('userid'))
	  {
		  if(Lang::has(Session::get('admin_lang_file').'.BACK_LOGIN_SUCCESS')!= '') 
			{ 
				$session_message = trans(Session::get('admin_lang_file').'.BACK_LOGIN_SUCCESS');
			}  
			else 
			{ 
				$session_message =  trans($this->$ADMIN_OUR_LANGUAGE.'.BACK_LOGIN_SUCCESS');
			}	
	  return Redirect::to('siteadmin_dashboard')->with('login_success',$session_message);
	  }
	  else
	  {
	  return view('siteadmin.admin_login');	
	  }
	}
	
	public function login_check()
	{
      $inputs = Input::all();
	  $uname = Input::get('admin_name');
	  $password = Input::get('admin_pass');	
	  $check = Merchantadminlogin::login_check($uname,$password);
	  if($check == 1)
	  {
	  	$data = array();
	  	$data['last_log_in'] = date("Y-m-d");
	  	AdminModel::where('adm_id','=',Session::get('userid'))->update($data);
  	  return Redirect::to('siteadmin_dashboard')->with('login_success','Login Success');
	  }
	  else if($check == -1)
	  {
	  	   if(Session::get('admin_lang_file') == 'admin_ar_lang'){
	  	   	$err_msg = 'اسم مستخدم غير صالح';
	  	   }else{
	  	   	$err_msg = 'Invalid UserName';
	  	   }

		  
	  return Redirect::to('siteadmin')->with('login_error',$err_msg);
	  }
	  else if($check == -2)
	  {	
	  	if(Session::get('admin_lang_file') == 'admin_ar_lang'){
	  	   	$err_msg = 'رمز مرور خاطئ';
	  	   }else{
	  	   	$err_msg = 'Invalid password';
	  	   }

		    return Redirect::to('siteadmin')->with('login_error',$err_msg)->withInput();
	  }
	  else
	  {
	  	if(Session::get('admin_lang_file') == 'admin_ar_lang'){
	  	   	$err_msg = 'المستخدم ليس موجود!';
	  	   }else{
	  	   	$err_msg = 'User Not Found!';
	  	   }

		   
		   return Redirect::to('siteadmin')->with('login_error',$err_msg);
	  }
	}

	public function forgot_check()
	{
	  $inputs = Input::all(); 
	  $email = Input::get('admin_email');
          $check = Merchantadminlogin::forgot_check($email);
	  if($check > 0)
	  {
	  $forgot_check = Merchantadminlogin::forgot_check_details($email);
	  $email = $forgot_check[0]->adm_email;
	  $first_name= $forgot_check[0]->adm_fname;
                $sender_id=$forgot_check[0]->adm_id;
                $ecode = base64_encode($sender_id);
                $eemail = base64_encode($email);
                $params = 'id='.$ecode.'&e_i='.$eemail;
	  
		  if(Lang::has(Session::get('admin_lang_file').'.BACK_MAIL_SEND_SUCCESSFULLY')!= '') 
			{ 
				$session_message = trans(Session::get('admin_lang_file').'.BACK_MAIL_SEND_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_MAIL_SEND_SUCCESSFULLY');
			}	
                if ($forgot_check[0]->adm_fname != '') {
                	$username = $forgot_check[0]->adm_fname;
                }else{
                	$username = '';
                }

                $confirmation_url = route('home_front', $params);
                 $this->confirmation_user_email = $forgot_check[0]->adm_email;
                $this->confirmation_user_name = $username;
                
                 $subject = 'Reset Password';

                Mail::send('admin.mail.reset_password',
                    array(
                        'user_name' => $username,
                        'user_email' => $forgot_check[0]->adm_email,
                        'confirmation_url' => $confirmation_url,
                    ), function($message) use($subject)
                    {
                        $message->to($this->confirmation_user_email, $this->confirmation_user_name)->subject($subject);
                    });


                $data = array();
                $data['forgotpassword_flag']='1';
                $user = AdminModel::where('adm_email', '=',$forgot_check[0]->adm_email)->update($data);
                if($user==1)
                {


						if(Session::get('admin_lang_file') == 'admin_ar_lang'){
						$session_message = 'رابط إعادة تعيين كلمة المرور ترسل على معرف البريد الإلكتروني الخاص بك السجل';
						}else{
						$session_message = 'Reset password link send on your register email id';
						}
                    Auth::logout();
                    Session::put('login_success', $session_message);
                    return Redirect::to('siteadmin')->with('success',$session_message);
				}else{



						if(Session::get('admin_lang_file') == 'admin_ar_lang'){
						$session_message = 'أرسل رابط إعادة تعيين كلمة المرور بالفعل على معرف البريد الإلكتروني الخاص بك السجل';
						}else{
						$session_message = 'Reset password link already send on your register email id';
						}

				Auth::logout();
				Session::put('forgot_error', $session_message);
				return Redirect::to('siteadmin')->with('forgot_error',$session_message);
				}
  	  return Redirect::to('siteadmin')->with('forgot_success',$session_message);
	  }
	  else 
	  {
			if(Session::get('admin_lang_file') == 'admin_ar_lang'){
			$session_message = 'بريد إلكتروني خاطئ';
			}else{
			$session_message = 'Invalid Email';
			}
	  return Redirect::to('siteadmin')->with('forgot_error',$session_message);
	  }
	}

	public function admin_logout()
	{
       Session::forget('userid');
	  Session::forget('username');
	  if(Session::get('admin_lang_file') == 'admin_ar_lang') 
			{ 
				$session_message = 'نجاح تسجيل الخروج';
			}  
			else 
			{ 
				$session_message =  'Logout Success';
			}
			// Session::flush();
	  return Redirect::to('siteadmin')->with('login_success',$session_message);
	}	

	public function admin_new_change_languages()
	{
		//$this->ADMIN_OUR_LANGUAGE ="admin_en_lang";
		$lang_code = Input::get('language_code');
		Session::put('admin_lang_file','admin_'.$lang_code.'_lang');
		Session::put('admin_lang_code',$lang_code);
		Session::put('lang_file',$lang_code.'_lang');
	}
	
  }

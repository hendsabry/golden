<?php 
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Category;
use App\Products;
use App\City;
use App\ServiceAttribute;
use App\CategoryGallery;
use App\ProductAttribute;
use App\ProductOption;
use App\ProductOptionValue;
use App\ProductPackage;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartProductRent;
use App\CartInvitation;
use App\CartServiceAttribute;
use App\CartHospitality;
use App\StaffNationality;
use Auth;
use App\User;
use Response;
use Storage;
use App\Formbusinesstype;
//------------wisitech code end -----------//
class FrontElectronicController extends Controller
{  
        public function __construct()
        {
            parent::__construct();
            $this->middleware(function ($request, $next) {
            $this->setLanguageLocaleFront();
            return $next($request);
            });      
        }

        public function electronicShopdetails($id,$sid)
        {
            $category_id = $id;
            $subcat_id   = $sid;
            //echo $category_id.'-'.$subcat_id;die;
            if(Session::has('customerdata.user_id')) 
            {
               if(Session::get('searchdata.basecategoryid')!='')
               {
                 $basecategory_id = Session::get('searchdata.basecategoryid');
               }
               else 
               {
                 $basecategory_id = Session::get('searchdata.maincategoryid');
               }

               $city_id = Session::get('searchdata.cityid');
               $lang    = Session::get('lang_file');
               $Formbusinesstype = new Formbusinesstype();
                $occasiontype  = Session::get('searchdata.weddingoccasiontype');
                
               $bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);
               if(!empty($subcat_id)) 
               {
                 if($category_id != '' && $lang == 'ar_lang') 
                 {
                    $vendordetails = Category::select('mc_id','vendor_id','mc_name_ar as mc_name','mc_discription_ar as mc_discription','city_id','occasion_id','mc_img','mc_video_url','address_ar as address','mc_video_description as mc_video_description_ar')->where('mc_id',$subcat_id)->where('parent_id',$category_id)->first();                                     
                 } 
                 else 
                 {  
                    $vendordetails = Category::select('mc_id','vendor_id','mc_name','mc_discription','city_id','occasion_id','mc_img','mc_video_url','address','mc_video_description')->where('mc_id',$subcat_id)->where('parent_id',$category_id)->first();                   
                 }
            }

            $allreview = DB::table('nm_review')->where('shop_id',$subcat_id)->where('review_type','shop')->where('status',1)->paginate(9);
            return view('newWebsite.invitation',compact('vendordetails','bussinesstype','category_id','subcat_id','allreview','occasiontype','basecategory_id'));
            } 
            else
            {
              return Redirect::to('login-signup');
            }
        }

        public function getAllDesignerCard(Request $request)
        {
          $catId           = $request->category_id;
          $invitation_type = $request->invitation_type;
          $texttype        = $request->texttype;
          if($invitation_type == 1)
          {
            $pmessage = Products::select('pro_id','pro_price')->where('pro_title',$texttype)->where('pro_status',1)->where('pro_mc_id',$catId)->first();
            echo $pmessage->pro_id.'-'.$pmessage->pro_price;
          }
          else if($invitation_type == 2)
          {
            $pemail = Products::select('pro_id','pro_price')->where('pro_title',$texttype)->where('pro_status',1)->where('pro_mc_id',$catId)->first();
            echo $pemail->pro_id.'-'.$pemail->pro_price;
          }                
        }
        
      public function getSingleProduct(Request $request)
      {
        $product_id = $request->product_id;
        $singleproduct = Products::select('pro_id','pro_price')->where('pro_id',$product_id)->where('pro_status',1)->first();
        echo $singleproduct->pro_id.'-'.$singleproduct->pro_price;                         
      }

     public function getAllDesignerCardAllProduct(Request $request)
     {
          $catId           = $request->category_id;
          $invitation_type = $request->invitation_type;
          $pid = $request->pid;
          if($invitation_type == 3)
          {
            $allProduct = Products::select('pro_id','pro_title','pro_Img','pro_mc_id','pro_price')->where('pro_title','!=','email')->where('pro_title','!=','message')->where('pro_id',$pid)->where('pro_status',1)->where('pro_mc_id',$catId)->get();
            $productshopfullinfo = array('productdata'=>$allProduct);            
          }  
          return response()->json($productshopfullinfo);     
     }

public function checkfilentry(Request $request)
{
  $path = $request->file('file')->getRealPath();
           $data = array_map('str_getcsv', file($path));
          echo $count_row = count($data);  
        

}
     public function insert_electronic(Request $request)
     {
        $lang          = Session::get('lang_file');
        $user_id       = Session::get('customerdata.user_id');        
        $validatedData = $request->validate([
                  'occassion_name'=>'required|max:100',
                  'occasion'=>'required',
                  'venue'=>'required',
                  'date'=>'required',
                  'time' =>'required',
                  'comments' =>'required']);
        if($request->file('upload_csv'))
        {
           $path = $request->file('upload_csv')->getRealPath();
           $data = array_map('str_getcsv', file($path));
           $count_row = count($data);  
           $file                = $request->upload_csv;                      
           $extension           = $file->getClientOriginalExtension();
           $newfile             = rand(11111,99999).'_'.time().'.'.$extension;               
           //$imageRealPath       = $file->getRealPath(); 
           $file->move('public/electronic_invitations/', $newfile);
           $file_url = url('').'/public/electronic_invitations/'.$newfile;
        }
        else
        {
            $count_row     = 1;
            $file_url      = '';  
        }
        $category_id        = $request->category_id;
        $subcat_id          = $request->subcat_id;
        $actiontype         = $request->actiontype;
        $cart_sub_type      = $request->cart_sub_type;
        $cart_type          = $request->cart_type;
        $occassion_name     = $request->occassion_name;
        $occasion           = $request->occasion;
        $venue              = $request->venue;
        $date               = $request->date;
        $time               = $request->time;
        $priceId            = $request->priceId;
        $comments           = $request->comments;
        $vendor_id          = $request->vendor_id;
        $product_id         = $request->product_id;
        $invitations_type   = $request->invitations_type;
        $product            = Products::where('pro_id',$product_id)->first();
        $categorydetail     = Category::where('mc_id',$subcat_id)->first();

        $occasion_type            = Products::where('pro_id',$occasion)->first();
       
        $cart               = Cart::where('user_id',$user_id)->first();
        if(!empty($cart)) 
        {
            $cart_pro = CartProduct::where('cart_type',$cart_type)->where('cart_id',$cart->id)->where('product_id',$product_id)->delete();
            $cart_invitation = CartInvitation::where('cart_id',$cart->id)->where('services_id',$product_id)->delete();
        } 
        else 
        {
            $cart_data = array();
            $cart_data['user_id'] = $user_id;
            $cart = Cart::create($cart_data); //cart entery
        }

        $cart_invitation_data                     = array();
        $cart_invitation_data['cart_type']        = $cart_type;
        $cart_invitation_data['services_id']      = 16;
        $cart_invitation_data['customer_id']      = $user_id;
        $cart_invitation_data['cart_id']          = $cart->id;
        $cart_invitation_data['occasion_name']    = $occassion_name;
        $cart_invitation_data['occasion_type']    = $occasion;
        $cart_invitation_data['venue']            = $venue;
        $cart_invitation_data['date']             = date("Y-m-d", strtotime($date));
        $cart_invitation_data['time']             = $time;
        $cart_invitation_data['no_of_invitees']   = $count_row;
        $cart_invitation_data['file_url']         = $file_url;
        $cart_invitation_data['invitaion_mode']   = $invitations_type;
        if($invitations_type == 3) 
        {
            $card = Products::select('pro_id','pro_title','pro_title_ar','pro_desc','pro_desc_ar','pro_Img','pro_mc_id','pro_price','pro_label','pro_label_ar')->where('pro_id',$product_id)->first();
            $cart_invitation_data['packge_id']  = $card->pro_id;
            $cart_invitation_data['card_price'] = $card->pro_price * $count_row;
            $cart_invitation_data['invitation_msg']      = $comments;
        }
        else
        {
            $card = Products::select('pro_id','pro_title','pro_title_ar','pro_desc','pro_desc_ar','pro_Img','pro_mc_id','pro_price','pro_label','pro_label_ar')->where('pro_id',$product_id)->first();
            $cart_invitation_data['packge_id']           = $card->pro_id;
            $cart_invitation_data['card_price']          = $card->pro_price * $count_row;
            $cart_invitation_data['invitation_msg']      = $comments;
        }
        $cart_invitation_data['total_member_invitation'] = $count_row;
        $cart_invitation_data['status']                  = 1;
        $cart_invitation_data['packge_title']            = $card->pro_title;
        $cart_invitation_data['packge_title_ar']         = $card->pro_title_ar;
        $cart_invitation_data['packge_desc']             = $card->pro_desc;
        $cart_invitation_data['packge_desc_ar']          = $card->pro_desc_ar;
        $cart_invitation_data['packge_img']              = $card->pro_Img;
        $cart_invitation_data['pro_label']              = $occasion_type->pro_label;
        $cart_invitation_data['pro_label_ar']              = $occasion_type->pro_label_ar;
       

        $cart_product = CartInvitation::create($cart_invitation_data); //cart product attribute entry*/
        $total_price = array();
        if(isset($total_price))
        {
            $total_price['total_price'] = $cart_product->card_price; 
        }
        
        $cart_product_data                       = array();
        $cart_product_data['merchant_id']        = $categorydetail->vendor_id;
        $cart_product_data['shop_id']            = $categorydetail->mc_id;
        $cart_product_data['shop_vendor_id']     = $categorydetail->parent_id;
        $cart_product_data['cart_type']          = $cart_type;
        $cart_product_data['cart_sub_type']      = $cart_sub_type;
        $cart_product_data['cart_id']            = $cart->id;
        $cart_product_data['category_id']        = $subcat_id;
        $cart_product_data['product_id']         = $product_id;
        $cart_product_data['total_price']        = $priceId;
        $cart_product_data['status']             = 1;
        $cart_product_data['pro_title']          = $categorydetail->mc_name;
        $cart_product_data['pro_title_ar']       = $categorydetail->mc_name_ar;
        $cart_product_data['pro_desc']           = $categorydetail->mc_discription;
        $cart_product_data['pro_desc_ar']        = $categorydetail->mc_discription_ar;
        $cart_product_data['pro_Img']            = $categorydetail->mc_img;
        $cart_product_data['file_url']           = $file_url;
        $cart_product = CartProduct::create($cart_product_data); //cart product entry

        if(\Config::get('app.locale') == 'ar')
        {
          Session::flash('status', "وأضاف المنتج في العربة.");
        }
        else
        {
          Session::flash('status', "Product added in the cart.");
        }                  
      return redirect($actiontype.'/'.$category_id.'/'.$subcat_id);
    }

 
}

<?php 
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//

use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;
use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\CartServiceStaff;
use App\Formbusinesstype;
use App\Shoplisthall;
use App\Foodcategorylist;
use App\Beautyshop;
use App\CartProductPackage;
//------------wisitech code end -----------//
class BeautyshopController extends Controller
{  
    public function __construct(){
        parent::__construct();
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
     
    public function beautyshopandservices(Request $request,$halltype,$typeofhallid,$shopid,$branchid)
    {
		$halltype       = $halltype;
		$subcategory_id = $halltype;
		 $shop_id        = $shopid;
    $branchid       = $branchid;
    $servicecategoryAndservices=array();
    $city_id        = Session::get('searchdata.cityid');
		if(Session::get('searchdata.basecategoryid')!='')
    {
		  $basecategory_id=Session::get('searchdata.basecategoryid');
		}
    else 
    {
		  $basecategory_id=Session::get('searchdata.maincategoryid');
		}
		$category_id   = $typeofhallid;
    $subsecondcategoryid = $halltype;
		$typeofhallid  = $typeofhallid;
		$budget        = Session::get('searchdata.budget');
		$lang          = Session::get('lang_file');		
			
		$foodsessioninfo = array('maincategory_id' => $halltype, "category_id" => $category_id, "shop_id" => $shopid); 
		Session::put('fooddata',$foodsessioninfo);
		
		$Formbusinesstype= new Formbusinesstype();
		  $bussinesstype=$Formbusinesstype->Getbussinesstype($basecategory_id,$lang);	
			 $value = $request->session()->get('searchdata');
		       $getsubcategories = '';
			   
			   $foodShoplisthall= new Foodcategorylist();
			   $foodShopdate= new Beautyshop();
			 
			 $foodshopunderbugetincity=$foodShoplisthall->Getfoodshoplistbycity($category_id,$city_id,$budget,$lang);
			
			 $fooddateshopdetails=$foodShopdate->Getfoodshopdate($branchid,$lang);
			  $fooddateshopgallery=$foodShopdate->Getgalleryimages($branchid);
			  $fooddateshopreview=$foodShopdate->GetshopReviews($branchid);
        //dd($fooddateshopreview);			 
			   $fooddateshopproducts=$foodShopdate->Getfoodshopdateproduct($branchid,$lang);
			  $beautyshopleftproduct=$foodShopdate->Getfoodshopdatefirstproduct($branchid,$lang);
               $prod_id=$beautyshopleftproduct->pro_id;
              

 if($typeofhallid==21){$tblfield='service_id';}else{ $tblfield='shop_id'; }

                 

                    if ($branchid != '' && $lang == 'ar_lang') {
                        $servicecategoryAndservices = DB::table('nm_services_attribute')->where('status', '=', 1)->where('services_id', '=', $branchid)->select('id', 'attribute_title_ar as attribute_title','services_id','vendor_id','image')->orderBy('attribute_title')->get(); 
                        $servicestaffexpert = DB::table('nm_staff_experties')->where('product_id', '=', $prod_id)->select('id', 'staff_id')->get();
                       
                   }else{
                         $servicecategoryAndservices = DB::table('nm_services_attribute')->where('status', '=', 1)->where('services_id', '=', $branchid)->select('id', 'attribute_title','services_id','vendor_id','image')->orderBy('attribute_title')->get();
                         $servicestaffexpert = DB::table('nm_staff_experties')->where('product_id', '=', $prod_id)->select('id', 'staff_id')->get();
                          
                }
                
                $getPagelimit = config('app.paginate');
                foreach ($servicecategoryAndservices as $sericesdata) {
                       
                     if ($lang == 'ar_lang') {
                        $productbeautyshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $branchid)->where('attribute_id', '=', $sericesdata->id)->where('packege','!=', 'yes')->select('pro_id', 'pro_title_ar as pro_title', 'pro_price', 'pro_Img','pro_disprice')->orderBy('pro_title')->get();
                   }else{
                         $productbeautyshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $branchid)->where('attribute_id', '=', $sericesdata->id)->where('packege','!=', 'yes')->select('pro_id', 'pro_title', 'pro_Img','pro_price','pro_disprice')->orderBy('pro_title')->get();
                          }
                        $sericesdata->serviceslist=$productbeautyshopinfo;
                }


                foreach ($servicestaffexpert as $staffreviewexp) {
                        if ($branchid != '' && $lang == 'ar_lang') {
                            $servicestaff = DB::table('nm_services_staff')->where('status', '=', 1)->where($tblfield, '=', $branchid)->where('id', '=', $staffreviewexp->staff_id)->select('id', 'staff_member_name_ar as staff_member_name','experience','image')->get();
                             }else{
                                $servicestaff = DB::table('nm_services_staff')->where('status', '=', 1)->where($tblfield, '=', $branchid)->where('id', '=', $staffreviewexp->staff_id)->select('id', 'staff_member_name','experience','image')->get(); 
                             }
                             $staffreviewexp->staffinformation=$servicestaff;

                             foreach ($servicestaff as $staffreview) {                                
                     $servicestaffreview = DB::table('nm_review')->where('status', '=', 1)->where('shop_id', '=', $branchid)->where('worker_id', '=', $staffreview->id)->select('comment_id', 'customer_id','comments','ratings')->get(); 
                    
                     if(count($servicestaffreview)>0){
                         $staffreviewexp->staffreviews=$servicestaffreview;
                        }else{

                            $staffreviewexp->staffreviews='';
                        }

                     $servicestaffratings = DB::table('nm_review')->where('status', '=', 1)->where('shop_id', '=', $branchid)->where('worker_id', '=', $staffreview->id)->avg('ratings'); 

                      if($servicestaffratings>0){
                          $staffreviewexp->staffratingsavg=$servicestaffratings;
                        }else{

                            $staffreviewexp->staffratingsavg='';
                        }
                }
                }
               // echo "<pre>";
             //print_r($servicestaffexpert);
             // die;

                //////////////// manage packages/////////////////
                    if ($lang == 'ar_lang') {
                        $productbeautyshoppackageinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $branchid)->where('packege','=', 'yes')->select('pro_id', 'pro_title_ar as pro_title', 'pro_price', 'pro_Img','pro_disprice')->orderBy('pro_title')->paginate('9')->appends(request()->query());
                   }else{
                         $productbeautyshoppackageinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $branchid)->where('packege','=', 'yes')->select('pro_id', 'pro_title', 'pro_Img','pro_price','pro_disprice')->orderBy('pro_title')->paginate('9')->appends(request()->query());
                          }
                          
                        if(count($productbeautyshoppackageinfo)>0){
                           $shopavailablepackage=$productbeautyshoppackageinfo; 
                        }else{
                            $shopavailablepackage=array();
                        }
                        //print_r($shopavailablepackage);
                /////////////////// end package ///////////

                        ////////////// other branches////////////
                    $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $shop_id)->first(); 
                if (!empty($category)) {
                 if ($category_id != '' && $lang == 'ar_lang') {
                  $hallsubcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $shop_id)->where('city_id','=',$city_id)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get(); 
                    foreach ($hallsubcategory as $getchildelement) {
                          $cid=$getchildelement->mc_id;
                            $ischildthere[] = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $cid)->count();
                            $hallsubcategory->ischild=$ischildthere;
                         }

                             } else {
                    $hallsubcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $shop_id)->where('city_id','=',$city_id)->select('mc_id', 'mc_name', 'mc_img')->get();

                        foreach ($hallsubcategory as $getchildelement) {
                          $cid=$getchildelement->mc_id;
                            $ischildthere[] = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $cid)->count();
                            $hallsubcategory->ischild=$ischildthere;
                         }

                  }
               }
               $otherbarnch=$hallsubcategory;
//echo "<pre>";
//print_r($otherbarnch);
             ////////////////end branches code////////////////



          
        return view('newWebsite.beauty-shop', compact('getsubcategories','bussinesstype','foodshopunderbugetincity','halltype','fooddateshopdetails','category_id','subcategory_id','subsecondcategoryid','fooddateshopgallery','fooddateshopreview','fooddateshopproducts','beautyshopleftproduct','shop_id','servicecategoryAndservices','servicestaff','branchid','shop_id','servicestaffexpert','shopavailablepackage','otherbarnch','shop_id','branchid'));
 
    }


public function workeravailability(Request $request){

      $workerid = $request->staffid;     

      $f_date = date('Y-m-d', strtotime( $request->bookingdate ));
                $bookingdate=$f_date;

      $bookedstaff = DB::table('nm_order_services_staff')->where('booking_date', '=', $bookingdate)->where('staff_id', '=', $workerid)->select('id', 'start_time','end_time')->orderBy('id')->get();

        $isbookingopenslot=array('workerbookingrecord'=>$bookedstaff);
                return response()->json($isbookingopenslot);

}



    public function newbeautyshop(Request $request)
    {
 $page = $request->page;
 $branchid = $request->branchid;
 $attribute_id = $request->attribute_id;
 $tbl_field=$request->tblfield;
 $lang=Session::get('lang_file');
 if ($lang == 'ar_lang') {
 $productbeautyshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $branchid)->where('attribute_id', '=', $attribute_id)->where('packege','!=', 'yes')->select('pro_id', 'pro_title_ar as pro_title', 'pro_price', 'pro_Img','pro_disprice')->orderBy('pro_title')->paginate(9);
}else{
  $productbeautyshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $branchid)->where('attribute_id', '=', $attribute_id)->where('packege','!=', 'yes')->select('pro_id', 'pro_title', 'pro_price', 'pro_Img','pro_disprice')->orderBy('pro_title')->paginate(9);
  //$productbeautyshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('packege','!=', 'yes')->orderBy('pro_title')->paginate(9);
}
 //

$tabid = $request->tabid;

 return view('beautyandelegance.beautypageshop', compact('productbeautyshopinfo','tabid','branchid','tbl_field'));

    }

    function getcartproduct(Request $request){
    			$pro_id = $request->product_id;
                $branchid=$request->branchid;
                $tblfld=$request->tblfld;
    			$lang   = Session::get('lang_file');
    			$Subdatefoodfunc = new Beautyshop();
		$getsubproduct=$Subdatefoodfunc->getajaxdatefoodproduct($pro_id,$lang,$branchid,$tblfld);	
			return response()->json($getsubproduct);

    }

    function staffavailability(Request $request){

                $staffid = $request->staffid;
                $service_duration=$request->service_duration;
                $f_date = date('Y-m-d', strtotime( $request->bookingdate ));
                $bookingdate=$f_date;
                $bookingtime=$request->bookingtime;
                $isstaffavailable=array();
               $bookingappliedtime= strtotime($bookingtime);
				
				$bookingappliedendtime = strtotime($bookingtime) + 60*60*$service_duration;
              // $end_time = date('h:i A', strtotime($bookingtime.
                    //'+'.$service_duration.
                    //'hour'));
					$end_time = date('h:i A', $bookingappliedendtime);
              // $bookingappliedendtime= strtotime($end_time);
			   $timeslots=array('bstart'=>$bookingappliedtime,'bend'=>$bookingappliedendtime,'bookingtime'=>$bookingtime,'endtime'=>$end_time);

                          $bookedstaff = DB::table('nm_order_services_staff')->where('booking_date', '=', $bookingdate)->where('staff_id', '=', $staffid)->select('id', 'start_time','end_time')->orderBy('id')->get();
                $k=1;
                foreach ($bookedstaff as $bookingtimeslot) {
                  $dbstarttime=strtotime($bookingtimeslot->start_time);
                  $dbendtime=strtotime($bookingtimeslot->end_time);



                    if(($bookingappliedtime >= $dbstarttime) && ($bookingappliedtime < $dbendtime)){
                    $isstaffavailable[]=$k;
                    }
                    if(($bookingappliedendtime >= $dbstarttime) && ($bookingappliedendtime < $dbendtime)){
                    $isstaffavailable[]=$k;
                    }
                    if(($bookingappliedtime <= $dbstarttime) && ($bookingappliedendtime > $dbendtime)){
                    $isstaffavailable[]=$k;
                    }
                    // if(($bookingappliedendtime >= $dbstarttime) && ($bookingappliedendtime < $dbendtime)){
                    // $isstaffavailable[]=$k;
                    // }


                    $dtimetimeslot[]=array('dbstart'=>$dbstarttime,'dbend'=>$dbendtime);
                    $k++;
                    }




                $isbookingopen=array('bookingopen'=>$isstaffavailable,'btimeslots'=>$timeslots,'dtimeslot'=>$dtimetimeslot,'bookingrecord'=>$bookedstaff);
                return response()->json($isbookingopen);
    }


 function staffavailabilitychart(Request $request){
                $lang=Session::get('lang_file');
                $branchid = $request->branchid;
                $servicestaffidl=rtrim($request->servicestaffid,',');
                $servicestaffid=explode(',', $servicestaffidl);                           
                $f_date = date('Y-m-d', strtotime( $request->bookingdate ));
                $bookingdate=$f_date;
                $product_id=$request->product_id;
                  if ($lang == 'ar_lang') {
                  $allstaffaccordingtoser=DB::table('nm_services_staff')->whereIn('id', $servicestaffid)->select('id','staff_member_name_ar as staff_member_name','image')->orderBy('id')->get();
                }else{
                  $allstaffaccordingtoser=DB::table('nm_services_staff')->whereIn('id', $servicestaffid)->select('id','staff_member_name','image')->orderBy('id')->get();
                }
                  foreach ($allstaffaccordingtoser as $bookedstaffslot) {
                     $bookedstaff = DB::table('nm_order_services_staff')->where('booking_date', '=', $bookingdate)->where('staff_id', '=', $bookedstaffslot->id)->select('id','staff_id','shop_id','start_time','end_time')->orderBy('id')->get();
                     $bookedstaffslot->bookedslot=$bookedstaff;
                  }
                  $isbookingopen=array('bookingopen'=>$allstaffaccordingtoser);
                return response()->json($isbookingopen);
 }

function getstaffreview(Request $request){
                $lang=Session::get('lang_file');
                $staffid = $request->staffid;               
                $product_id=$request->product_id;
                 $nm_worker_review=DB::table('nm_review')->where('worker_id', $staffid)->where('status', 1)->where('review_type', 'worker')->orderBy('comment_id')->get();
                
                  foreach ($nm_worker_review as $revieweduser) {
                     $bookedstaff = DB::table('nm_customer')->where('cus_id', '=', $revieweduser->customer_id)->get();
                     $revieweduser->revieweduser=$bookedstaff;
                  }
                  $isbookingopen=array('workerreview'=>$nm_worker_review);
                return response()->json($isbookingopen);
 }




    function addcartproduct(Request $request){
        /*echo "<pre>";
    	print_r($request->all());
    	die;*/
     
    	  $branch_id           = $request->branch_id;
    	  $shop_id             = $request->shop_id;
    	  $vendor_id           = $request->vendor_id;
        $product_id          = $request->product_id;
        $product_price       = $request->product_price;
        $staffid             = $request->staffid;
         $getbookingtimename ='bookingtime'.$staffid;
        $appointmentfor      = $request->appointmentfor;
        $bookingdate         = $request->bookingdate;
        $bookingtime         = $request->$getbookingtimename;
          if($bookingtime==''){
            $bookingtime=$request->bookingpackagetime;

          }

        $cart_sub_type       = $request->cart_sub_type;
        $service_duration    = $request->service_duration;
        $category_id         = $request->category_id;
        $subsecondcategoryid = $request->subsecondcategoryid;
    	  $userid              = Session::get('customerdata.user_id');
        $productinfo         = new Beautyshop();
        $lang                = Session::get('lang_file');
        $cartproductdata     = $productinfo->Getproductforcart($product_id,$lang);
        $pro_title           = $cartproductdata->pro_title;
        $pro_Img             = $cartproductdata->pro_Img;
        $pro_title_ar        = $cartproductdata->pro_title_ar;
        $attribute_id        = $cartproductdata->attribute_id;
        $ispackage        = $cartproductdata->packege;
        $pro_disprice        = $cartproductdata->pro_disprice;
        $pro_desc        = $cartproductdata->pro_desc;
        $pro_desc_ar        = $cartproductdata->pro_desc_ar; 
        $pro_img       = $cartproductdata->pro_Img;        

              $cart = Cart::where('user_id',$userid)->first();
                    if (!empty($cart)) {
                      
                        $cart_pro = CartProduct::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('product_id', $product_id)->delete();

                        $cart_option = CartServiceStaff::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('service_id', $product_id)->delete();

                        $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('product_id', '=', $product_id)->delete();
                        $cart_service_package = CartProductPackage::where('cart_type', '=', 'beauty')->where('cart_id', '=', $cart->id)->where('product_id', '=', $product_id)->delete();

                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $userid;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                        if (!empty($cart)) 
                        {
                            $cart_product_data                   = array();
                            $cart_product_data['cart_type']      = 'beauty';
                            $cart_product_data['cart_sub_type']  = $cart_sub_type;
                            $cart_product_data['cart_id']        = $cart->id;
                            $cart_product_data['product_id']     = $product_id;
                            $cart_product_data['pro_title']      = $pro_title;
                            $cart_product_data['pro_Img']        = $pro_Img;
                            $cart_product_data['pro_title_ar']   = $pro_title_ar;
                            $cart_product_data['total_price']    = $product_price; 
                            $cart_product_data['merchant_id']    = $vendor_id;
                            $cart_product_data['category_id']    = $shop_id;
                            $cart_product_data['shop_id']        = $branch_id; 
                            $cart_product_data['shop_vendor_id'] = $branch_id;                    
                            $cart_product_data['status']         = 1;
                            $cart_product_data['review_type']    = 'worker';
                            $cart_product_data['quantity']       = 1;
                            $cart_product = CartProduct::create($cart_product_data); //cart product entry

                            if(isset($ispackage) && $ispackage=='yes'){
                              $cart_package_data                   = array();
                               $cart_package_data['cart_id']        = $cart->id;
                               $cart_package_data['cart_type']      = 'beauty';
                               $cart_package_data['product_id']     = $product_id;
                               $cart_package_data['package_id']     = $product_id;
                               $cart_package_data['pro_title']      = $pro_title;
                               $cart_package_data['pro_title_ar']   = $pro_title_ar;
                               $cart_package_data['pro_desc']      = $pro_desc;
                               $cart_package_data['pro_desc_ar']   = $pro_desc_ar;
                               $cart_package_data['pro_price']    = $product_price;
                               $cart_package_data['pro_disprice']    = $pro_disprice;
                               $cart_package_data['pro_img']    = $pro_img;
                               
                               $cart_package = CartProductPackage::create($cart_package_data); //cart product entry
                            }


                            if(!empty($attribute_id)) 
                            {
                              $cart_attribute_data = array();
                              $cart_attribute_data['cart_type']     = 'beauty';
                              $cart_attribute_data['cart_id']       = $cart->id;
                              $cart_attribute_data['product_id']    = $product_id;
                              $cart_attribute_data['category_id']   = $branch_id;
                              $cart_attribute_data['attribute_id']  = $attribute_id;
                              $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry
                            }

                                $cart_service_staff               = array();
                                $cart_service_staff['cart_id']    = $cart->id;
                                $cart_service_staff['cart_type']  = 'beauty';
                                $cart_service_staff['shop_id']    = $branch_id;
                                $cart_service_staff['service_id'] = $product_id;
                                
                                if (isset($staffid)) {
                                     $cartstaffdata=$productinfo->Getstaffinformation($staffid,$lang);
                                     
                                          $staff_member_name=$cartstaffdata->staff_member_name;
                                          $staff_member_name_ar=$cartstaffdata->staff_member_name_ar;
                                          $staff_member_image=$cartstaffdata->image;
               
                                $cart_service_staff['staff_id'] = $staffid;
                                $cart_service_staff['staff_member_name'] = $staff_member_name;
                                $cart_service_staff['staff_member_name_ar'] = $staff_member_name_ar;
                                $cart_service_staff['image'] = $staff_member_image;
                                  }
                                $cart_service_staff['booking_place'] = $appointmentfor;   
                                
                                $cart_service_staff['booking_date'] = date("Y-m-d", strtotime($bookingdate));
                                $cart_service_staff['start_time'] = $bookingtime;
                                 $time=$bookingtime;
                                  $service_hour=$service_duration;
                                  $newdate= date("H:i", strtotime($time));
                                  $end_time = date('h:i A', strtotime($newdate.
                                                      '+'.$service_hour.
                                                      'hour'));
                               $cart_service_staff['end_time'] = $end_time;
                                CartServiceStaff::create($cart_service_staff); //Cart Service Entery entery

                          if(\Config::get('app.locale') == 'ar')
                    {
                    Session::flash('status', "وأضاف المنتج في العربة.");
                    }
                    else
                    {
                    Session::flash('status', "Service added in the cart.");
                    }
                    // language for display message //                
                        return Redirect::back();
                        return redirect('beautyshop/'.$category_id.'/'.$subsecondcategoryid.'/'.$shop_id.'/'.$branch_id);

                          
                        } 
                
                
//die;

    }
   







}

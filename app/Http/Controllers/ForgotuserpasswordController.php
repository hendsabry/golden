<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//

use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;
use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\Formbusinesstype;
use App\Internalfooditems;


use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\JWT;
use Tymon\JWTAuth\JWTGuard;
use JWTAuth;
use App\User;
use Auth;
use Response;

use Exception;

use Storage;
//------------wisitech code end -----------//
class ForgotuserpasswordController extends Controller
{  
    public function __construct(){
        parent::__construct();
		
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
     
    public function forgotpassword(Request $request)
    {
 		 
		$lang=Session::get('lang_file');
        return view('userforgotpassword', compact('mainmenuitemtype'));
 
    }



    //////////////// check user duplication ////////////////////
    
    public function checkuseraccount(Request $request){
    
             $method = $request->method();
             if ($request->isMethod('post')) {
                    
             
                        $useremail=$request->input('email');
                           $usercount = User::where('email', '=', $useremail)->count();
                            if ($usercount <1)
                            {
                             $m="approved";
                              }
                            else
                              {
                             $m="Error";
                            
                              }
                    }
                echo $m;
             
     }
    ///////////////// end dupilcation code /////////////////


     public function checkloginaccount(Request $request){
            
            $method = $request->method();
             if ($request->isMethod('post')) {
                 $lang=Session::get('lang_file');

                if ($lang == 'ar_lang') {
                  $errormessage="يبدو أن بيانات اعتماد تسجيل الدخول غير صحيحة";
                }else{

                    $errormessage="credentials seems incorrect!";
                }
                        $useremail=$request->input('email');
                               
                              $isuser = DB::table('nm_customer')->where('email', '=', $useremail)->first();
                          
                         if($isuser->cus_id!=''){

                            $encode_email = base64_encode(base64_encode(base64_encode(($useremail))));


                                      $name = 'Member';          
                                    $send_mail_data = array(
                                        'name' => $isuser->cus_name,
                                        'email' => $useremail,
                                        'encodeemail' => $encode_email,
                                        'lang' => $lang,
                                        'LANUAGE' => $this->OUR_LANGUAGE
                                    );
//print_r($send_mail_data);
                                    # It will show these lines as error but no issue it will work fine Line no 119 - 122
                                    Mail::send('emails.member_passwordrecoverymail', $send_mail_data, function($message)
                                    {
                                       if(Lang::has(Session::get('lang_file').'.FORGOT_PASSWORD')!= '')
                                        { 
                                            $session_message =  trans(Session::get('lang_file').'.FORGOT_PASSWORD');
                                        }  
                                        $message->to(Input::get('email'))->subject($session_message);
                                    });
                                    if(Lang::has(Session::get('lang_file').'.MER_MAIL_SEND_SUCCESSFULLY')!= '')
                                    { 
                                     $statusmessage =  trans(Session::get('lang_file').'.MER_MAIL_SEND_SUCCESSFULLY');
                                    }  
                                     
                            return Redirect::to('userforgotpassword#loginfrml')->with('loginstatus', $statusmessage);
                         }else{
                            
                            return Redirect::to('userforgotpassword#loginfrml')->with('loginstatus', $errormessage);
                         }
                    }
                //echo $m;
    
    }
    
   


public function member_forgot_pwd_email(Request $request,$emailaddress)
    {
         //echo  $method = $request->method();
         $email_address=$emailaddress;
        
          //$merchat_decode_email;
        $lang=Session::get('lang_file');
        if ($request->isMethod('post')) {
        
             $validator = Validator::make($request->all(), [
            'password' => 'required',
            'userid' => 'required',
            ]);
                if ($validator->fails()) {

                         return redirect('login-signup')->with('status', 'some required fields are not filled!');

              }else{
                    $userid = $request->input('userid');
                      //$password = $request->input('password');
                       $confirmpwd = bcrypt($request->input('password'));
                     
                   $user_decode_email = base64_decode(base64_decode(base64_decode($userid)));

                     DB::table('nm_customer')->where('email', '=', $user_decode_email)->update(array('password' => $confirmpwd));
        
                 if (Lang::has(Session::get('mer_lang_file').'.MER_PASSWORD_CHANGED_SUCCESSFULLY')!= '')
                    { 
                        $session_message =  trans(Session::get('mer_lang_file').'.MER_PASSWORD_CHANGED_SUCCESSFULLY');
                    }  
                    else 
                    { 
                        $session_message =  trans($this->OUR_LANGUAGE.'.MER_PASSWORD_CHANGED_SUCCESSFULLY');
                    }

                }

                return redirect('login-signup')->with('chstatus', 'Password Changed succesfully! Please login your account'); 

         }
        return view('member_forgot_pwd_email', compact('mainmenuitemtype','email_address'));
 
    }








}

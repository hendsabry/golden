<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class Order extends Model 
{
	
	protected $table = 'nm_order';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['order_cus_id','order_merchant_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	public function getUser()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\User','cus_id','order_cus_id');
    }
    public function getMerchant()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Merchant','mer_id','order_merchant_id');
    }
    public function getProduct()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Products','pro_id','order_pro_id');
    }
	
}

<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class ServiceAttribute extends Model 
{
	
	protected $table = 'nm_services_attribute';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','services_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
    public function getDish()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Products','attribute_id','id');
    }

    public function getProduct()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Products','attribute_id','id');
    }
    public function getBuffet()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\Category','services_id','mc_id');
    }
}

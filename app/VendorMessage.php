<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
class VendorMessage extends Model
{
    
    protected $guarded = array('id');
    protected $table = 'nm_vendor_message';

    protected $fillable = ['message', 'message_ar', 'created_at', 'updated_at']; 
    
    
}

?>

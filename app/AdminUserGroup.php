<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class AdminUserGroup extends Model 
{
	
	protected $table = 'admin_user_group';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['group_name','group_permission','level','status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
}

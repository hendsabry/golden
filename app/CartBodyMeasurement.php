<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class CartBodyMeasurement extends Model 
{
	
	protected $table = 'nm_cart_body_measurement';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','product_id','customer_id','cart_id','length','chest_size','waistsize','soulders','neck','arm_length','wrist_diameter','cutting','status','cart_product_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
	public function getInvitaionCard()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\Products','invitation_card','pro_id');
    }

    public function getInvitaionCategory()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\Category','services_id','mc_id');
    }
}

<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class CartOccasionCoordinator extends Model 
{
	
	protected $table = 'nm_cart_occasion_co-ordinator';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cart_id','cart_type','category_id','attribute_id','sub_attribute_id','product_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	public function getContainer()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\ProductOptionValue','id','container_id');
    }

    public function getFood()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Products','pro_id','external_food_dish_id');
    }

    public function getServiceAttribute()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\ServiceAttribute','id','external_food_menu_id');
    }
	
}

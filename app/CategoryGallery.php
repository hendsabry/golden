<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class CategoryGallery extends Model 
{
	
	protected $table = 'nm_category_gallery';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title','category_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
}

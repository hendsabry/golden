<?php
namespace App;
use DB;
use File;
use Session;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
class MerchantDeal extends Model
{
    protected $guarded = array('id');
    protected $table = 'nm_cms_pages';
    
    public static function get_dealcashon_delivery_details($merid)
    {
        return DB::table('nm_ordercod')
		->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')
		->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
		//->leftjoin('nm_shipping', 'nm_ordercod.cod_id', '=', 'nm_shipping.ship_order_id')
		 ->leftjoin('nm_shipping', 'nm_ordercod.cod_transaction_id', '=', 'nm_shipping.ship_trans_id')
		->groupby('nm_ordercod.cod_transaction_id')
		->orderBy('nm_ordercod.cod_date', 'desc')
		->where('nm_ordercod.cod_merchant_id',$merid)
		->where('nm_ordercod.cod_order_type','=',2)
		->get();
    }
	public static function get_deal_shipping_details($merid)
    {
        return DB::table('nm_order')
		->orderBy('nm_order.order_id', 'desc')
		->groupBy('nm_order.transaction_id')
		->where('nm_order.order_type','=',2)
		->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')
		->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
		->leftjoin('nm_shipping', 'nm_order.transaction_id', '=', 'nm_shipping.ship_trans_id')
		->where('nm_order.order_merchant_id',$merid)
		->get();
    }
    public static function get_deal_payu_details($merid)
    {
        return DB::table('nm_order_payu')
		->orderBy('nm_order_payu.order_id', 'desc')
		->groupBy('nm_order_payu.transaction_id')
		->where('nm_order_payu.order_type','=',2)
		->leftjoin('nm_deals', 'nm_order_payu.order_pro_id', '=', 'nm_deals.deal_id')
		->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')
		->leftjoin('nm_shipping', 'nm_order_payu.transaction_id', '=', 'nm_shipping.ship_trans_id')
		->where('nm_order_payu.order_merchant_id',$merid)
		->get();
    }
}

?>

<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class BusinessOccasionType extends Model 
{
	
	protected $table = 'nm_business_occasion_type';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','top_category','title','title_ar','status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
}

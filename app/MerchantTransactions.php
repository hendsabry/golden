<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
class MerchantTransactions extends Model
{
    protected $guarded = array('id');
    protected $table = 'nm_order_auction';
    
    public static function getproduct_all_orders($merid)
    {
        return DB::table('nm_order')
        ->orderBy('order_date', 'desc')
        //->groupby('transaction_id')
        ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
        ->leftjoin('nm_merchant', 'nm_order.order_merchant_id', '=', 'nm_merchant.mer_id')
        ->where('nm_order.order_type', '=', 1)
        ->where('nm_order.order_merchant_id',$merid)
        ->get();
    }
public static function get_payu_product_all_orders($merid)
    {
        return DB::table('nm_order_payu')
        ->orderBy('order_date', 'desc')
        //->groupby('transaction_id')
        ->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')
        ->leftjoin('nm_merchant', 'nm_order_payu.order_merchant_id', '=', 'nm_merchant.mer_id')
        ->where('nm_order_payu.order_type', '=', 1)
        ->where('nm_order_payu.order_merchant_id',$merid)
        ->get();
    }
    public static function getproduct_success_orders($merid)
    {
        return DB::table('nm_order')
        ->orderBy('order_date', 'desc')
        ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
        ->leftjoin('nm_merchant', 'nm_order.order_merchant_id', '=', 'nm_merchant.mer_id')
        ->where('nm_order.order_status', '=', 1)
        ->where('nm_order.order_type', '=', 1)
        ->where('nm_order.order_merchant_id',$merid)
        ->get();
        
    }
public static function get_payu_product_success_orders($merid)
    {
        return DB::table('nm_order_payu')
        ->orderBy('order_date', 'desc')
        ->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')
        ->leftjoin('nm_merchant', 'nm_order_payu.order_merchant_id', '=', 'nm_merchant.mer_id')
        ->where('nm_order_payu.order_status', '=', 1)
        ->where('nm_order_payu.order_type', '=', 1)
        ->where('nm_order_payu.order_merchant_id',$merid)
        ->get();
        
    }
    public static function getproduct_completed_orders($merid)
    {
        return DB::table('nm_order')
        ->orderBy('order_date', 'desc')
        ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
        ->leftjoin('nm_merchant', 'nm_order.order_merchant_id', '=', 'nm_merchant.mer_id')
        ->where('nm_order.order_status', '=', 2)
        ->where('nm_order.order_type', '=', 1)
        ->where('nm_order.order_merchant_id',$merid)
        ->get();
        
    }

    public static function getproduct_hold_orders($merid)
    {
        return DB::table('nm_order')
        ->orderBy('order_date', 'desc')
        
        ->where('nm_order.order_merchant_id',$merid)
        ->where('nm_order.order_status', '=', 3)
        ->where('nm_order.order_type', '=', 1)
        ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
        ->leftjoin('nm_merchant', 'nm_order.order_merchant_id', '=', 'nm_merchant.mer_id')
        ->get();
    }
public static function get_payu_product_hold_orders($merid)
    {
        return DB::table('nm_order_payu')
        ->orderBy('order_date', 'desc')
        
        ->where('nm_order_payu.order_merchant_id',$merid)
        ->where('nm_order_payu.order_status', '=', 3)
        ->where('nm_order_payu.order_type', '=', 1)
        ->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')
        ->leftjoin('nm_merchant', 'nm_order_payu.order_merchant_id', '=', 'nm_merchant.mer_id')
        ->get();
    }

    
    public static function getproduct_failed_orders($merid)
    {
        return DB::table('nm_order')
        ->orderBy('order_date', 'desc')
        ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
        ->leftjoin('nm_merchant', 'nm_order.order_merchant_id', '=', 'nm_merchant.mer_id')
        ->where('nm_order.order_status', '=', 4)
        ->where('nm_order.order_type', '=', 1)
        ->where('nm_order.order_merchant_id',$merid)
        ->get();
        
    }
    public static function get_payu_product_failed_orders($merid)
    {
        return DB::table('nm_order_payu')
        ->orderBy('order_date', 'desc')
        ->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')
        ->leftjoin('nm_merchant', 'nm_order_payu.order_merchant_id', '=', 'nm_merchant.mer_id')
        ->where('nm_order_payu.order_status', '=', 4)
        ->where('nm_order_payu.order_type', '=', 1)
        ->where('nm_order_payu.order_merchant_id',$merid)
        ->get();
        
    }
    /*Transcation -> Product COD ->all Orders*/
    public static function getcod_all_orders($merid){

        return DB::table('nm_ordercod')
        ->orderBy('cod_date', 'desc')
        ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')
        ->where('nm_ordercod.cod_order_type', '=', 1)
        ->where('nm_ordercod.cod_merchant_id',$merid)
        ->get();

    }
    public static function get_approve_content($delStatus_id)
    {
        return DB::table('nm_order_delivery_status')
        ->where("delStatus_id","=",$delStatus_id)
        ->get();

    }
    public static function pay_to_wallet($order_id,$cust_id)
    {
        //echo $order_id;exit();
        $cust_order=DB::table('nm_order')->where("order_id","=",$order_id)->get();
        $cust_detail=DB::table('nm_customer')->select('wallet')->where("cus_id","=",$cust_id)->get();

        $customer_paid=($cust_order[0]->order_amt+$cust_order[0]->order_taxAmt+$cust_order[0]->order_shipping_amt)-($cust_order[0]->coupon_amount+$cust_order[0]->wallet_amount);
        $total_wallet_amount=$cust_detail[0]->wallet+$customer_paid;
        
        DB::table('nm_customer')->where("cus_id","=",$cust_id)->update(['wallet' => $total_wallet_amount]);
        return 0;
    }
    public static function pay_to_wallet_payu($order_id,$cust_id)
    {
        //echo $order_id;exit();
        $cust_order=DB::table('nm_order_payu')->where("order_id","=",$order_id)->get();
        $cust_detail=DB::table('nm_customer')->select('wallet')->where("cus_id","=",$cust_id)->get();

        $customer_paid=($cust_order[0]->order_amt+$cust_order[0]->order_taxAmt+$cust_order[0]->order_shipping_amt)-($cust_order[0]->coupon_amount+$cust_order[0]->wallet_amount);
        $total_wallet_amount=$cust_detail[0]->wallet+$customer_paid;
        
        DB::table('nm_customer')->where("cus_id","=",$cust_id)->update(['wallet' => $total_wallet_amount]);
        return 0;
    }
     public static function pay_to_wallet_cod($order_id,$cust_id)
    {
        $cust_order=DB::table('nm_ordercod')->where("cod_id","=",$order_id)->get();
        $cust_detail=DB::table('nm_customer')->select('wallet')->where("cus_id","=",$cust_id)->get();

        $customer_paid=($cust_order[0]->cod_amt+$cust_order[0]->cod_taxAmt+$cust_order[0]->cod_shipping_amt)-($cust_order[0]->coupon_amount+$cust_order[0]->wallet_amount);
        $total_wallet_amount=$cust_detail[0]->wallet+$customer_paid;
        
        DB::table('nm_customer')->where("cus_id","=",$cust_id)->update(['wallet' => $total_wallet_amount]);
        return 0;
    }
    public static function approve_cancel($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id)
    {
        DB::table('nm_order_delivery_status')
        ->where('delStatus_id', $delStatus_id)
        ->update(['cancel_status' => 2,"cancel_approved_date"=>date("Y-m-d H:i:s")]);
        DB::table('nm_ordercod')
        ->where('cod_id', $cod_order_id)
        ->update(['delivery_status' => 6]);
         DB::table('delivery_status_chat')->insert(['delStatus_id' => $delStatus_id,'cust_id' => $cust_id,'mer_id' => $mer_id,'admin_id' => $admin_id,'send_by' => "2",'note' => $approve_or_disapprove_note, 'created_date' => date("Y-m-d H:i:s")]);

        return 0;
    }
    public static function disapprove_cancel($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id)
    {
        DB::table('nm_order_delivery_status')
        ->where('delStatus_id', $delStatus_id)
        ->update(['cancel_status' => 4,"cancel_approved_date"=>date("Y-m-d H:i:s")]);
        DB::table('nm_ordercod')
        ->where('cod_id', $cod_order_id)
        ->update(['delivery_status' => 1]);
         DB::table('delivery_status_chat')->insert(['delStatus_id' => $delStatus_id,'cust_id' => $cust_id,'mer_id' => $mer_id,'admin_id' => $admin_id,'send_by' => "2",'note' => $approve_or_disapprove_note, 'created_date' => date("Y-m-d H:i:s")]);
        return 0;
    }

    public static function approve_cancel_paypal($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id)
    {
        DB::table('nm_order_delivery_status')
        ->where('delStatus_id', $delStatus_id)
        ->update(['cancel_status' => 2,"cancel_approved_date"=>date("Y-m-d H:i:s")]);
        DB::table('nm_order')
        ->where('order_id', $cod_order_id)
        ->update(['delivery_status' => 6]);
         DB::table('delivery_status_chat')->insert(['delStatus_id' => $delStatus_id,'cust_id' => $cust_id,'mer_id' => $mer_id,'admin_id' => $admin_id,'send_by' => "2",'note' => $approve_or_disapprove_note, 'created_date' => date("Y-m-d H:i:s")]);

        return 0;
    }
    public static function approve_cancel_payu($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id)
    {
        DB::table('nm_order_delivery_status')
        ->where('delStatus_id', $delStatus_id)
        ->update(['cancel_status' => 2,"cancel_approved_date"=>date("Y-m-d H:i:s")]);
        DB::table('nm_order_payu')
        ->where('order_id', $cod_order_id)
        ->update(['delivery_status' => 6]);
         DB::table('delivery_status_chat')->insert(['delStatus_id' => $delStatus_id,'cust_id' => $cust_id,'mer_id' => $mer_id,'admin_id' => $admin_id,'send_by' => "2",'note' => $approve_or_disapprove_note, 'created_date' => date("Y-m-d H:i:s")]);

        return 0;
    }
    public static function disapprove_cancel_paypal($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id)
    {
        DB::table('nm_order_delivery_status')
        ->where('delStatus_id', $delStatus_id)
        ->update(['cancel_status' => 4,"cancel_approved_date"=>date("Y-m-d H:i:s")]);
        DB::table('nm_order')
        ->where('order_id', $cod_order_id)
        ->update(['delivery_status' => 1]);
         DB::table('delivery_status_chat')->insert(['delStatus_id' => $delStatus_id,'cust_id' => $cust_id,'mer_id' => $mer_id,'admin_id' => $admin_id,'send_by' => "2",'note' => $approve_or_disapprove_note, 'created_date' => date("Y-m-d H:i:s")]);
        return 0;
    }
    public static function disapprove_cancel_payu($delStatus_id,$cod_order_id,$approve_or_disapprove_note,$mer_id,$cust_id,$admin_id)
    {
        DB::table('nm_order_delivery_status')
        ->where('delStatus_id', $delStatus_id)
        ->update(['cancel_status' => 4,"cancel_approved_date"=>date("Y-m-d H:i:s")]);
        DB::table('nm_order_payu')
        ->where('order_id', $cod_order_id)
        ->update(['delivery_status' => 1]);
         DB::table('delivery_status_chat')->insert(['delStatus_id' => $delStatus_id,'cust_id' => $cust_id,'mer_id' => $mer_id,'admin_id' => $admin_id,'send_by' => "2",'note' => $approve_or_disapprove_note, 'created_date' => date("Y-m-d H:i:s")]);
        return 0;
    }
    /*Get all calceled order*/
    public static function get_calceled_order($merid='',$payment_type,$product_type,$from_date,$to_date){

        $sql= DB::table('nm_order_delivery_status')
        ->orderBy('delStatus_id', 'desc')
        ->leftjoin('nm_ordercod', 'nm_order_delivery_status.cod_order_id', '=', 'nm_ordercod.cod_id')
        
        ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id');
         if($product_type==1)
            $sql->join('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id');
        if($product_type==2)
            $sql->join('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id');
         if($merid!='')
        $sql->where('nm_order_delivery_status.mer_id','=',$merid);

        $sql->where('nm_order_delivery_status.order_type','=',$product_type)
        ->where('nm_order_delivery_status.payment_type','=',$payment_type)
        ->where('nm_order_delivery_status.cancel_status','!=','0');
        if($from_date!="" AND $to_date!="")
        {
            $fr_date = date('Y-m-d H:i:s',strtotime($from_date));
            $t_date = date('Y-m-d H:i:s',strtotime($to_date));
            $sql->whereBetween('nm_order_delivery_status.cancel_date',[$fr_date, $t_date]);
        }
        /*$sql->groupby('nm_order_delivery_status.mer_id')*/
        return $sql->get();

    }

    public static function get_calceled_order_alltype($from_date,$to_date,$merid=''){

        $sql= DB::table('nm_ordercod')
                    ->orderBy('cod_id', 'desc')
                    ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
                    ->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')
                    ->leftjoin('nm_merchant', 'nm_ordercod.cod_merchant_id', '=', 'nm_merchant.mer_id')
					->leftjoin('nm_order_delivery_status', 'nm_order_delivery_status.cod_order_id', '=', 'nm_ordercod.cod_id')
                    ->where('nm_ordercod.cod_order_type','=',1)
                    ->where('nm_ordercod.cod_paytype','=',0)
                    ->where('nm_ordercod.cod_merchant_id','=',$merid)
                    ->whereIn('nm_ordercod.delivery_status',array(5,6));
                    
                    $fr_date = date('Y-m-d',strtotime($from_date));
                    $t_date = date('Y-m-d',strtotime($to_date));
                    if($from_date!="" AND $to_date!="")
                    {
                    $sql->whereDate('nm_ordercod.cod_date', '>=' , $fr_date);
                    $sql->whereDate('nm_ordercod.cod_date', '<=' , $t_date);
                    } else if ($from_date == '' & $to_date != '')  {
                    $sql->whereDate('nm_ordercod.cod_date', '<=' , $t_date);
                    } else if ($from_date != '' & $to_date == '')  {
                    $sql->whereDate('nm_ordercod.cod_date', '>=' , $fr_date);   
                    }
                    
                    return $sql->get();
            
    }

    public static function replace_dealorder_allpaypal($from_date,$to_date,$merid){
        
        $sql= DB::table('nm_order')
                    ->orderBy('order_id', 'desc')
                    ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
                    ->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')
                    ->leftjoin('nm_merchant', 'nm_order.order_merchant_id', '=', 'nm_merchant.mer_id')
					->leftjoin('nm_order_delivery_status', 'nm_order_delivery_status.order_id', '=', 'nm_order.order_id')
					->select('nm_order.*', 'nm_customer.*', 'nm_deals.*', 'nm_merchant.*', 'nm_order_delivery_status.order_id as paypal_order_id', 'nm_order_delivery_status.replace_status','nm_order_delivery_status.delStatus_id')
                    ->where('nm_order.order_type','=',2)
                    //->where('nm_order.order_paytype','=',1)
					->whereIn('nm_order.order_paytype',array(1,2)) 
                    ->whereIn('nm_order.delivery_status',array(9,10)); 
                    $fr_date = date('Y-m-d',strtotime($from_date)); 
                    $t_date = date('Y-m-d',strtotime($to_date));  
                    if($from_date!="" & $to_date!="") 
                    { 
                    $sql->whereDate('nm_order.order_date', '>=' , $fr_date);
                    $sql->whereDate('nm_order.order_date', '<=' , $t_date);
                    } else if ($from_date == '' & $to_date != '')  {
                    $sql->whereDate('nm_order.order_date', '<=' , $t_date);
                    } else if ($from_date != '' & $to_date == '')  {
                    $sql->whereDate('nm_order.order_date', '>=' , $fr_date);    
                    }

                    return $sql->get();
            
    }
 public static function replace_dealorder_allpayu($from_date,$to_date,$merid){
        
        $sql= DB::table('nm_order_payu')
                    ->orderBy('order_id', 'desc')
                    ->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')
                    ->leftjoin('nm_deals', 'nm_order_payu.order_pro_id', '=', 'nm_deals.deal_id')
                    ->leftjoin('nm_merchant', 'nm_order_payu.order_merchant_id', '=', 'nm_merchant.mer_id')
                    ->leftjoin('nm_order_delivery_status', 'nm_order_delivery_status.order_id', '=', 'nm_order_payu.order_id')
                    ->select('nm_order_payu.*', 'nm_customer.*', 'nm_deals.*', 'nm_merchant.*', 'nm_order_delivery_status.order_id as paypal_order_id', 'nm_order_delivery_status.replace_status','nm_order_delivery_status.delStatus_id')
                    ->where('nm_order_payu.order_type','=',2)
                    //->where('nm_order.order_paytype','=',1)
                    ->whereIn('nm_order_payu.order_paytype',array(1,2)) 
                    ->whereIn('nm_order_payu.delivery_status',array(9,10)); 
                    $fr_date = date('Y-m-d',strtotime($from_date)); 
                    $t_date = date('Y-m-d',strtotime($to_date));  
                    if($from_date!="" & $to_date!="") 
                    { 
                    $sql->whereDate('nm_order_payu.order_date', '>=' , $fr_date);
                    $sql->whereDate('nm_order_payu.order_date', '<=' , $t_date);
                    } else if ($from_date == '' & $to_date != '')  {
                    $sql->whereDate('nm_order_payu.order_date', '<=' , $t_date);
                    } else if ($from_date != '' & $to_date == '')  {
                    $sql->whereDate('nm_order_payu.order_date', '>=' , $fr_date);    
                    }

                    return $sql->get();
            
    }


    public static function get_return_order_alltype($from_date,$to_date,$merid){
        
        $sql= DB::table('nm_ordercod')
                    ->orderBy('cod_id', 'desc')
                    ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
                    ->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')
                    ->leftjoin('nm_merchant', 'nm_ordercod.cod_merchant_id', '=', 'nm_merchant.mer_id')
					->leftjoin('nm_order_delivery_status', 'nm_order_delivery_status.cod_order_id', '=', 'nm_ordercod.cod_id')
                    ->where('nm_ordercod.cod_order_type','=',1)
                    ->where('nm_ordercod.cod_paytype','=',0)
                     ->where('nm_ordercod.cod_merchant_id','=',$merid)
                    ->whereIn('nm_ordercod.delivery_status',array(7,8));
                    
                    $fr_date = date('Y-m-d',strtotime($from_date));
                    $t_date = date('Y-m-d',strtotime($to_date)); 
                    if($from_date!="" & $to_date!="")
                    {
                    $sql->whereDate('nm_ordercod.cod_date', '>=' , $fr_date);
                    $sql->whereDate('nm_ordercod.cod_date', '<=' , $t_date);
                    } else if ($from_date == '' & $to_date != '')  {
                    $sql->whereDate('nm_ordercod.cod_date', '<=' , $t_date);
                    } else if ($from_date != '' & $to_date == '')  {
                    $sql->whereDate('nm_ordercod.cod_date', '>=' , $fr_date);   
                    }
                    
                    return $sql->get();
            
    }

    public static function get_replace_order_alltype($from_date,$to_date,$merid){
        
        $sql= DB::table('nm_ordercod')
                    ->orderBy('cod_id', 'desc')
                    ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
                    ->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')
                    ->leftjoin('nm_merchant', 'nm_ordercod.cod_merchant_id', '=', 'nm_merchant.mer_id')
					->leftjoin('nm_order_delivery_status', 'nm_order_delivery_status.cod_order_id', '=', 'nm_ordercod.cod_id')
                    ->where('nm_ordercod.cod_order_type','=',1)
                    ->where('nm_ordercod.cod_paytype','=',0)
                     ->where('nm_ordercod.cod_merchant_id','=',$merid)
                    ->whereIn('nm_ordercod.delivery_status',array(9,10));
                    
                    $fr_date = date('Y-m-d',strtotime($from_date)); 
                    $t_date = date('Y-m-d',strtotime($to_date));  
                    if($from_date!="" & $to_date!="") 
                    { 
                    $sql->whereDate('nm_ordercod.cod_date', '>=' , $fr_date);
                    $sql->whereDate('nm_ordercod.cod_date', '<=' , $t_date);
                    } else if ($from_date == '' & $to_date != '')  {
                    $sql->whereDate('nm_ordercod.cod_date', '<=' , $t_date);
                    } else if ($from_date != '' & $to_date == '')  {
                    $sql->whereDate('nm_ordercod.cod_date', '>=' , $fr_date);   
                    }

                    return $sql->get();
            
    }
    public static function calceled_order_allpaypal($from_date,$to_date,$merid){
        
         $sql= DB::table('nm_order')
                    ->orderBy('order_id', 'desc')
                    ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
                    ->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
                    ->leftjoin('nm_merchant', 'nm_order.order_merchant_id', '=', 'nm_merchant.mer_id')
					->leftjoin('nm_order_delivery_status', 'nm_order_delivery_status.order_id', '=', 'nm_order.order_id')
					->select('nm_order.*', 'nm_customer.*', 'nm_product.*', 'nm_merchant.*', 'nm_order_delivery_status.order_id as paypal_order_id', 'nm_order_delivery_status.cancel_status','nm_order_delivery_status.delStatus_id')
                    ->where('nm_order.order_type','=',1)
                    ->whereIn('nm_order.order_paytype',array(1,2))
                    ->where('nm_order.order_merchant_id','=',$merid)
                    ->whereIn('nm_order.delivery_status',array(5,6)); 
                    
                    $fr_date = date('Y-m-d',strtotime($from_date)); 
                    $t_date = date('Y-m-d',strtotime($to_date));  
                    if($from_date!="" & $to_date!="") 
                    { 
                    $sql->whereDate('nm_order.order_date', '>=' , $fr_date);
                    $sql->whereDate('nm_order.order_date', '<=' , $t_date);
                    } else if ($from_date == '' & $to_date != '')  {
                    $sql->whereDate('nm_order.order_date', '<=' , $t_date);
                    } else if ($from_date != '' & $to_date == '')  {
                    $sql->whereDate('nm_order.order_date', '>=' , $fr_date);    
                    }

                    return $sql->get();
                    
            
    }
    public static function calceled_order_allpayu($from_date,$to_date,$merid){
        
         $sql= DB::table('nm_order_payu')
                    ->orderBy('order_id', 'desc')
                    ->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')
                    ->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')
                    ->leftjoin('nm_merchant', 'nm_order_payu.order_merchant_id', '=', 'nm_merchant.mer_id')
                    ->leftjoin('nm_order_delivery_status', 'nm_order_delivery_status.order_id', '=', 'nm_order_payu.order_id')
                    ->select('nm_order_payu.*', 'nm_customer.*', 'nm_product.*', 'nm_merchant.*', 'nm_order_delivery_status.order_id as paypal_order_id', 'nm_order_delivery_status.cancel_status','nm_order_delivery_status.delStatus_id')
                    ->where('nm_order_payu.order_type','=',1)
                    ->whereIn('nm_order_payu.order_paytype',array(1,2))
                    ->where('nm_order_payu.order_merchant_id','=',$merid)
                    ->whereIn('nm_order_payu.delivery_status',array(5,6)); 
                    
                    $fr_date = date('Y-m-d',strtotime($from_date)); 
                    $t_date = date('Y-m-d',strtotime($to_date));  
                    if($from_date!="" & $to_date!="") 
                    { 
                    $sql->whereDate('nm_order_payu.order_date', '>=' , $fr_date);
                    $sql->whereDate('nm_order_payu.order_date', '<=' , $t_date);
                    } else if ($from_date == '' & $to_date != '')  {
                    $sql->whereDate('nm_order_payu.order_date', '<=' , $t_date);
                    } else if ($from_date != '' & $to_date == '')  {
                    $sql->whereDate('nm_order_payu.order_date', '>=' , $fr_date);    
                    }

                    return $sql->get();
                    
            
    }
    public static function get_calceled_order_paypal($merid='',$payment_type,$product_type,$from_date,$to_date){
      // DB::connection()->enableQueryLog();
       // echo $from_date;exit();
        $sql= DB::table('nm_order_delivery_status')
        ->orderBy('delStatus_id', 'desc')
        ->join('nm_order', 'nm_order_delivery_status.order_id', '=', 'nm_order.order_id')
        
        ->join('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id');
        if($product_type==1)
            $sql->join('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id');
        if($product_type==2)
            $sql->join('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id');
        if($merid!='')
            $sql->where('nm_order_delivery_status.mer_id','=',$merid);
        $sql->where('nm_order_delivery_status.order_type','=',$product_type)
        ->where('nm_order_delivery_status.payment_type','=',$payment_type)
        ->where('nm_order_delivery_status.cancel_status','!=','0');
        if($from_date!="" AND $to_date!="")
        {
            $fr_date = date('Y-m-d H:i:s',strtotime($from_date));
            $t_date = date('Y-m-d H:i:s',strtotime($to_date));
            $sql->whereBetween('nm_order_delivery_status.cancel_date',[$fr_date, $t_date]);
        }
        //$sql->groupby('nm_order_delivery_status.mer_id');
        return $sql->get();
        /*$query = DB::getQueryLog();
        print_r($query);exit();*/
    }
    public static function get_calceled_order_payu($merid='',$payment_type,$product_type,$from_date,$to_date){
      // DB::connection()->enableQueryLog();
       // echo $from_date;exit();
        $sql= DB::table('nm_order_delivery_status')
        ->orderBy('delStatus_id', 'desc')
        ->join('nm_order_payu', 'nm_order_delivery_status.order_id', '=', 'nm_order_payu.order_id')
        
        ->join('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id');
        if($product_type==1)
            $sql->join('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id');
        if($product_type==2)
            $sql->join('nm_deals', 'nm_order_payu.order_pro_id', '=', 'nm_deals.deal_id');
        if($merid!='')
            $sql->where('nm_order_delivery_status.mer_id','=',$merid);
        $sql->where('nm_order_delivery_status.order_type','=',$product_type)
        ->where('nm_order_delivery_status.payment_type','=',$payment_type)
        ->where('nm_order_delivery_status.cancel_status','!=','0');
        if($from_date!="" AND $to_date!="")
        {
            $fr_date = date('Y-m-d H:i:s',strtotime($from_date));
            $t_date = date('Y-m-d H:i:s',strtotime($to_date));
            $sql->whereBetween('nm_order_delivery_status.cancel_date',[$fr_date, $t_date]);
        }
        //$sql->groupby('nm_order_delivery_status.mer_id');
        return $sql->get();
        /*$query = DB::getQueryLog();
        print_r($query);exit();*/
    }

 public static function get_calceled_order_paypal_deals($merid='',$payment_type,$product_type,$from_date,$to_date){
      // DB::connection()->enableQueryLog();
       // echo $from_date;exit();
        $sql= DB::table('nm_order_delivery_status')
        ->orderBy('delStatus_id', 'desc')
        ->join('nm_order', 'nm_order_delivery_status.order_id', '=', 'nm_order.order_id')
        
        ->join('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id');
        if($product_type==1)
            $sql->join('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id');
        if($product_type==2)
            $sql->join('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id');
        if($merid!='')
            $sql->where('nm_order_delivery_status.mer_id','=',$merid);
        $sql->where('nm_order_delivery_status.order_type','=',$product_type)
        ->where('nm_order_delivery_status.payment_type','=',$payment_type)
        ->where('nm_order_delivery_status.cancel_status','!=','0');
        if($from_date!="" AND $to_date!="")
        {
            $fr_date = date('Y-m-d H:i:s',strtotime($from_date));
            $t_date = date('Y-m-d H:i:s',strtotime($to_date));
            $sql->whereBetween('nm_order_delivery_status.cancel_date',[$fr_date, $t_date]);
        }
        //$sql->groupby('nm_order_delivery_status.mer_id');
        return $sql->get();
        /*$query = DB::getQueryLog();
        print_r($query);exit();*/
    }

public static function get_payu_calceled_order_deals($merid='',$payment_type,$product_type,$from_date,$to_date){
      // DB::connection()->enableQueryLog();
       // echo $from_date;exit();
        $sql= DB::table('nm_order_delivery_status')
        ->orderBy('delStatus_id', 'desc')
        ->join('nm_order_payu', 'nm_order_delivery_status.order_id', '=', 'nm_order_payu.order_id')
        
        ->join('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id');
        if($product_type==1)
            $sql->join('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id');
        if($product_type==2)
            $sql->join('nm_deals', 'nm_order_payu.order_pro_id', '=', 'nm_deals.deal_id');
        if($merid!='')
            $sql->where('nm_order_delivery_status.mer_id','=',$merid);
        $sql->where('nm_order_delivery_status.order_type','=',$product_type)
        ->where('nm_order_delivery_status.payment_type','=',$payment_type)
        ->where('nm_order_delivery_status.cancel_status','!=','0');
        if($from_date!="" AND $to_date!="")
        {
            $fr_date = date('Y-m-d H:i:s',strtotime($from_date));
            $t_date = date('Y-m-d H:i:s',strtotime($to_date));
            $sql->whereBetween('nm_order_delivery_status.cancel_date',[$fr_date, $t_date]);
        }
        //$sql->groupby('nm_order_delivery_status.mer_id');
        return $sql->get();
        /*$query = DB::getQueryLog();
        print_r($query);exit();*/
    }


    /*return order starts */
    /*Get all return order*/
    public static function get_return_order($merid='',$payment_type,$product_type,$from_date,$to_date){

        $sql= DB::table('nm_order_delivery_status')
        ->orderBy('delStatus_id', 'desc')
        ->leftjoin('nm_ordercod', 'nm_order_delivery_status.cod_order_id', '=', 'nm_ordercod.cod_id')
        
        ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id');
        if($product_type==1)
            $sql->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id');
        if($product_type==2)
            $sql->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id');
        if($merid!='')
            $sql->where('nm_order_delivery_status.mer_id','=',$merid);

        $sql->where('nm_order_delivery_status.order_type','=',$product_type)
        ->where('nm_order_delivery_status.payment_type','=',$payment_type)
        ->where('nm_order_delivery_status.return_status','!=','0');
       
      /*  if($from_date!="" AND $to_date!="")
        {
            $sql->whereBetween('nm_order_delivery_status.return_date',[$from_date, $to_date]);
        }  */ 
        return $sql->get();
       // return $sql->groupby('nm_order_delivery_status.mer_id')
      //  ->get();

    }

    public static function approve_return($delStatus_id,$cod_order_id,$note,$mer_id,$cust_id,$admin_id,$send_by)
    {
        DB::table('nm_order_delivery_status')
        ->where('delStatus_id', $delStatus_id)
        ->update(['return_status' => 2,"return_approved_date"=>date("Y-m-d H:i:s")]);
        DB::table('nm_ordercod')
        ->where('cod_id', $cod_order_id)
        ->update(['delivery_status' => 8]);

        DB::table('delivery_status_chat')->insert(['delStatus_id' => $delStatus_id,'cust_id' => $cust_id,'mer_id' => $mer_id,'admin_id' => $admin_id,'send_by' => $send_by,'note' => $note, 'created_date' => date("Y-m-d H:i:s")]);

        return 0;
    }
    public static function disapprove_return($delStatus_id,$cod_order_id,$note,$mer_id,$cust_id,$admin_id,$send_by)
    {
        DB::table('nm_order_delivery_status')
        ->where('delStatus_id', $delStatus_id)
        ->update(['return_status' => 2,"return_approved_date"=>date("Y-m-d H:i:s")]);

        DB::table('nm_ordercod')
        ->where('cod_id', $cod_order_id)
        ->update(['delivery_status' => 4]); //delivery status reset as delivered

        DB::table('delivery_status_chat')->insert(['delStatus_id' => $delStatus_id,'cust_id' => $cust_id,'mer_id' => $mer_id,'admin_id' => $admin_id,'send_by' => $send_by,'note' => $note, 'created_date' => date("Y-m-d H:i:s")]);
        return 0;

    }

    public static function returned_dealorder_allpaypal($from_date,$to_date,$merid){
        
        $sql= DB::table('nm_order')
                    ->orderBy('order_id', 'desc')
                    ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
                    ->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')
                    ->leftjoin('nm_merchant', 'nm_order.order_merchant_id', '=', 'nm_merchant.mer_id')
					->leftjoin('nm_order_delivery_status', 'nm_order_delivery_status.order_id', '=', 'nm_order.order_id')
					->select('nm_order.*', 'nm_customer.*', 'nm_deals.*', 'nm_merchant.*', 'nm_order_delivery_status.order_id as paypal_order_id', 'nm_order_delivery_status.return_status','nm_order_delivery_status.delStatus_id')
					//->where('nm_order.order_paytype','=',1) //2 for wallet
					->whereIn('nm_order.order_paytype',array(1,2)) 
                    ->where('nm_order.order_type','=',2)
                    ->where('nm_order.order_merchant_id','=',$merid)
                    ->whereIn('nm_order.delivery_status',array(7,8)); 
                    
                    $fr_date = date('Y-m-d',strtotime($from_date)); 
                    $t_date = date('Y-m-d',strtotime($to_date));  
                    if($from_date!="" & $to_date!="") 
                    { 
                    $sql->whereDate('nm_order.order_date', '>=' , $fr_date);
                    $sql->whereDate('nm_order.order_date', '<=' , $t_date);
                    } else if ($from_date == '' & $to_date != '')  {
                    $sql->whereDate('nm_order.order_date', '<=' , $t_date);
                    } else if ($from_date != '' & $to_date == '')  {
                    $sql->whereDate('nm_order.order_date', '>=' , $fr_date);    
                    }

                    return $sql->get();
            
    }

     public static function get_return_order_paypal($merid='',$payment_type,$product_type,$from_date,$to_date){
        //DB::connection()->enableQueryLog();

        //DB::connection()->enableQueryLog();

        $sql= DB::table('nm_order_delivery_status')
        ->orderBy('delStatus_id', 'desc')
        ->leftjoin('nm_order', 'nm_order_delivery_status.order_id', '=', 'nm_order.order_id')
        
        ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id');
        if($product_type==1)
            $sql->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id');
        if($product_type==2)
            $sql->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id');
        if($merid!='')
            $sql->where('nm_order_delivery_status.mer_id','=',$merid);
        $sql->where('nm_order_delivery_status.order_type','=',$product_type)        
        ->where('nm_order_delivery_status.payment_type','=',$payment_type)
        ->where('nm_order_delivery_status.return_status','!=','0');
       /*  if($from_date!="" AND $to_date!="")
        {
            $sql->whereBetween('nm_order_delivery_status.return_date',[$from_date, $to_date]);
        } */
       // $sql->groupby('nm_order_delivery_status.mer_id');
        return $sql->get();

        /*$query = DB::getQueryLog();
        print_r($query);*/

    }
     public static function get_return_order_payu($merid='',$payment_type,$product_type,$from_date,$to_date){
        //DB::connection()->enableQueryLog();

        //DB::connection()->enableQueryLog();

        $sql= DB::table('nm_order_delivery_status')
        ->orderBy('delStatus_id', 'desc')
        ->leftjoin('nm_order_payu', 'nm_order_delivery_status.order_id', '=', 'nm_order_payu.order_id')
        
        ->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id');
        if($product_type==1)
            $sql->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id');
        if($product_type==2)
            $sql->leftjoin('nm_deals', 'nm_order_payu.order_pro_id', '=', 'nm_deals.deal_id');
        if($merid!='')
            $sql->where('nm_order_delivery_status.mer_id','=',$merid);
        $sql->where('nm_order_delivery_status.order_type','=',$product_type)        
        ->where('nm_order_delivery_status.payment_type','=',$payment_type)
        ->where('nm_order_delivery_status.return_status','!=','0');
       /*  if($from_date!="" AND $to_date!="")
        {
            $sql->whereBetween('nm_order_delivery_status.return_date',[$from_date, $to_date]);
        } */
       // $sql->groupby('nm_order_delivery_status.mer_id');
        return $sql->get();

        /*$query = DB::getQueryLog();
        print_r($query);*/

    }

    public static function approve_return_paypal($delStatus_id,$cod_order_id,$note,$mer_id,$cust_id,$admin_id,$send_by)
    {
        DB::table('nm_order_delivery_status')
        ->where('delStatus_id', $delStatus_id)
        ->update(['return_status' => 2,"return_approved_date"=>date("Y-m-d H:i:s")]);
        DB::table('nm_order')
        ->where('order_id', $cod_order_id)
        ->update(['delivery_status' => 8]);

        DB::table('delivery_status_chat')->insert(['delStatus_id' => $delStatus_id,'cust_id' => $cust_id,'mer_id' => $mer_id,'admin_id' => $admin_id,'send_by' => $send_by,'note' => $note, 'created_date' => date("Y-m-d H:i:s")]);

        return 0;
    }
    public static function approve_return_payu($delStatus_id,$cod_order_id,$note,$mer_id,$cust_id,$admin_id,$send_by)
    {
        DB::table('nm_order_delivery_status')
        ->where('delStatus_id', $delStatus_id)
        ->update(['return_status' => 2,"return_approved_date"=>date("Y-m-d H:i:s")]);
        DB::table('nm_order_payu')
        ->where('order_id', $cod_order_id)
        ->update(['delivery_status' => 8]);

        DB::table('delivery_status_chat')->insert(['delStatus_id' => $delStatus_id,'cust_id' => $cust_id,'mer_id' => $mer_id,'admin_id' => $admin_id,'send_by' => $send_by,'note' => $note, 'created_date' => date("Y-m-d H:i:s")]);

        return 0;
    }
    public static function disapprove_return_paypal($delStatus_id,$cod_order_id,$note,$mer_id,$cust_id,$admin_id,$send_by)
    {
        DB::table('nm_order_delivery_status')
        ->where('delStatus_id', $delStatus_id)
        ->update(['return_status' => 2,"return_approved_date"=>date("Y-m-d H:i:s")]);

        DB::table('nm_order')
        ->where('order_id', $cod_order_id)
        ->update(['delivery_status' => 4]); //delivery status reset as delivered

        DB::table('delivery_status_chat')->insert(['delStatus_id' => $delStatus_id,'cust_id' => $cust_id,'mer_id' => $mer_id,'admin_id' => $admin_id,'send_by' => $send_by,'note' => $note, 'created_date' => date("Y-m-d H:i:s")]);
        return 0;

    }
 public static function disapprove_return_payu($delStatus_id,$cod_order_id,$note,$mer_id,$cust_id,$admin_id,$send_by)
    {
        DB::table('nm_order_delivery_status')
        ->where('delStatus_id', $delStatus_id)
        ->update(['return_status' => 2,"return_approved_date"=>date("Y-m-d H:i:s")]);

        DB::table('nm_order_payu')
        ->where('order_id', $cod_order_id)
        ->update(['delivery_status' => 4]); //delivery status reset as delivered

        DB::table('delivery_status_chat')->insert(['delStatus_id' => $delStatus_id,'cust_id' => $cust_id,'mer_id' => $mer_id,'admin_id' => $admin_id,'send_by' => $send_by,'note' => $note, 'created_date' => date("Y-m-d H:i:s")]);
        return 0;

    }
    public static function calceled_dealorder_allpaypal($from_date,$to_date,$merid){
        
        $sql= DB::table('nm_order')
                    ->orderBy('order_id', 'desc')
                    ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
                    ->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')
                    ->leftjoin('nm_merchant', 'nm_order.order_merchant_id', '=', 'nm_merchant.mer_id')
					->leftjoin('nm_order_delivery_status', 'nm_order_delivery_status.order_id', '=', 'nm_order.order_id')
					->select('nm_order.*', 'nm_customer.*', 'nm_deals.*', 'nm_merchant.*', 'nm_order_delivery_status.order_id as paypal_order_id', 'nm_order_delivery_status.cancel_status','nm_order_delivery_status.delStatus_id')
                    ->where('nm_order.order_type','=',2)
                    //->where('nm_order.order_paytype','=',1) //2 for wallet
					->whereIn('nm_order.order_paytype',array(1,2)) 
                    ->where('nm_order.order_merchant_id','=',$merid)
                    ->whereIn('nm_order.delivery_status',array(5,6)); 
                    
                    $fr_date = date('Y-m-d',strtotime($from_date)); 
                    $t_date = date('Y-m-d',strtotime($to_date));  
                    if($from_date!="" & $to_date!="") 
                    { 
                    $sql->whereDate('nm_order.order_date', '>=' , $fr_date);
                    $sql->whereDate('nm_order.order_date', '<=' , $t_date);
                    } else if ($from_date == '' & $to_date != '')  {
                    $sql->whereDate('nm_order.order_date', '<=' , $t_date);
                    } else if ($from_date != '' & $to_date == '')  {
                    $sql->whereDate('nm_order.order_date', '>=' , $fr_date);    
                    }

                    return $sql->get();
            
    }
 public static function calceled_dealorder_all_payu($from_date,$to_date,$merid){
        
        $sql= DB::table('nm_order_payu')
                    ->orderBy('order_id', 'desc')
                    ->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')
                    ->leftjoin('nm_deals', 'nm_order_payu.order_pro_id', '=', 'nm_deals.deal_id')
                    ->leftjoin('nm_merchant', 'nm_order_payu.order_merchant_id', '=', 'nm_merchant.mer_id')
                    ->leftjoin('nm_order_delivery_status', 'nm_order_delivery_status.order_id', '=', 'nm_order_payu.order_id')
                    ->select('nm_order_payu.*', 'nm_customer.*', 'nm_deals.*', 'nm_merchant.*', 'nm_order_delivery_status.order_id as paypal_order_id', 'nm_order_delivery_status.cancel_status','nm_order_delivery_status.delStatus_id')
                    ->where('nm_order_payu.order_type','=',2)
                    //->where('nm_order.order_paytype','=',1) //2 for wallet
                    ->whereIn('nm_order_payu.order_paytype',array(1,2)) 
                    ->where('nm_order_payu.order_merchant_id','=',$merid)
                    ->whereIn('nm_order_payu.delivery_status',array(5,6)); 
                    
                    $fr_date = date('Y-m-d',strtotime($from_date)); 
                    $t_date = date('Y-m-d',strtotime($to_date));  
                    if($from_date!="" & $to_date!="") 
                    { 
                    $sql->whereDate('nm_order_payu.order_date', '>=' , $fr_date);
                    $sql->whereDate('nm_order_payu.order_date', '<=' , $t_date);
                    } else if ($from_date == '' & $to_date != '')  {
                    $sql->whereDate('nm_order_payu.order_date', '<=' , $t_date);
                    } else if ($from_date != '' & $to_date == '')  {
                    $sql->whereDate('nm_order_payu.order_date', '>=' , $fr_date);    
                    }

                    return $sql->get();
            
    }

    /*return order ends */

    /*replace order starts */
    /*Get all replace order*/
    public static function get_replace_order($merid='',$payment_type,$product_type,$from_date,$to_date){

      
        $sql= DB::table('nm_order_delivery_status')
        ->orderBy('delStatus_id', 'desc')
        ->leftjoin('nm_ordercod', 'nm_order_delivery_status.cod_order_id', '=', 'nm_ordercod.cod_id')
        
        ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id');
         if($product_type==1)
            $sql->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id');
         if($product_type==2)
            $sql->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id');
         if($merid!='')
            $sql->where('nm_order_delivery_status.mer_id','=',$merid);

        $sql->where('nm_order_delivery_status.order_type','=',$product_type)
        ->where('nm_order_delivery_status.payment_type','=',$payment_type)
        ->where('nm_order_delivery_status.replace_status','!=','0');
        
        /* if($from_date!="" AND $to_date!="")
        {
            $sql->whereBetween('nm_order_delivery_status.replace_date',[$from_date, $to_date]);
        } */ return $sql->get();
      //  return $sql->groupby('nm_order_delivery_status.mer_id')
       // ->get();

    }

    public static function approve_replace($delStatus_id,$cod_order_id,$note,$mer_id,$cust_id,$admin_id,$send_by)
    {
        DB::table('nm_order_delivery_status')
        ->where('delStatus_id', $delStatus_id)
        ->update(['replace_status' => 2,"replace_approved_date"=>date("Y-m-d H:i:s")]);
        DB::table('nm_ordercod')
        ->where('cod_id', $cod_order_id)
        ->update(['delivery_status' => 10]);

        DB::table('delivery_status_chat')->insert(['delStatus_id' => $delStatus_id,'cust_id' => $cust_id,'mer_id' => $mer_id,'admin_id' => $admin_id,'send_by' => $send_by,'note' => $note, 'created_date' => date("Y-m-d H:i:s")]);

        return 0;
    }
    public static function disapprove_replace($delStatus_id,$cod_order_id,$note,$mer_id,$cust_id,$admin_id,$send_by)
    {
        DB::table('nm_order_delivery_status')
        ->where('delStatus_id', $delStatus_id)
        ->update(['replace_status' => 2,"replace_approved_date"=>date("Y-m-d H:i:s")]);

        DB::table('nm_ordercod')
        ->where('cod_id', $cod_order_id)
        ->update(['delivery_status' => 4]); //delivery status reset as delivered

        DB::table('delivery_status_chat')->insert(['delStatus_id' => $delStatus_id,'cust_id' => $cust_id,'mer_id' => $mer_id,'admin_id' => $admin_id,'send_by' => $send_by,'note' => $note, 'created_date' => date("Y-m-d H:i:s")]);
        return 0;

    }/*Get all replace order*/
   public static function get_replace_order_paypal($merid='',$payment_type,$product_type,$from_date,$to_date){

        $sql= DB::table('nm_order_delivery_status')
        ->orderBy('delStatus_id', 'desc')
        ->leftjoin('nm_order', 'nm_order_delivery_status.order_id', '=', 'nm_order.order_id')
        
        ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id');
        if($product_type==1)
            $sql->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id');
        if($product_type==2)
            $sql->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id');
         if($merid!='')
            $sql->where('nm_order_delivery_status.mer_id','=',$merid);
        $sql->where('nm_order_delivery_status.order_type','=',$product_type)
        ->where('nm_order_delivery_status.payment_type','=',$payment_type)
        ->where('nm_order_delivery_status.replace_status','!=','0');
        /* if($from_date!="" AND $to_date!="")
        {
            $sql->whereBetween('nm_order_delivery_status.replace_date',[$from_date, $to_date]);
        } */ return $sql->get();
        //return $sql->groupby('nm_order_delivery_status.mer_id')
       // ->get();

    }
    public static function get_replace_order_payu($merid='',$payment_type,$product_type,$from_date,$to_date){

        $sql= DB::table('nm_order_delivery_status')
        ->orderBy('delStatus_id', 'desc')
        ->leftjoin('nm_order_payu', 'nm_order_delivery_status.order_id', '=', 'nm_order_payu.order_id')
        
        ->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id');
        if($product_type==1)
            $sql->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id');
        if($product_type==2)
            $sql->leftjoin('nm_deals', 'nm_order_payu.order_pro_id', '=', 'nm_deals.deal_id');
         if($merid!='')
            $sql->where('nm_order_delivery_status.mer_id','=',$merid);
        $sql->where('nm_order_delivery_status.order_type','=',$product_type)
        ->where('nm_order_delivery_status.payment_type','=',$payment_type)
        ->where('nm_order_delivery_status.replace_status','!=','0');
        /* if($from_date!="" AND $to_date!="")
        {
            $sql->whereBetween('nm_order_delivery_status.replace_date',[$from_date, $to_date]);
        } */ return $sql->get();
        //return $sql->groupby('nm_order_delivery_status.mer_id')
       // ->get();

    }
     public static function get_payu_replace_order_paypal($merid='',$payment_type,$product_type,$from_date,$to_date){

        $sql= DB::table('nm_order_delivery_status')
        ->orderBy('delStatus_id', 'desc')
        ->leftjoin('nm_order_payu', 'nm_order_delivery_status.order_id', '=', 'nm_order_payu.order_id')
        
        ->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id');
        if($product_type==1)
            $sql->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id');
        if($product_type==2)
            $sql->leftjoin('nm_deals', 'nm_order_payu.order_pro_id', '=', 'nm_deals.deal_id');
         if($merid!='')
            $sql->where('nm_order_delivery_status.mer_id','=',$merid);
        $sql->where('nm_order_delivery_status.order_type','=',$product_type)
        ->where('nm_order_delivery_status.payment_type','=',$payment_type)
        ->where('nm_order_delivery_status.replace_status','!=','0');
        /* if($from_date!="" AND $to_date!="")
        {
            $sql->whereBetween('nm_order_delivery_status.replace_date',[$from_date, $to_date]);
        } */ return $sql->get();
        //return $sql->groupby('nm_order_delivery_status.mer_id')
       // ->get();

    }
    public static function approve_replace_paypal($delStatus_id,$cod_order_id,$note,$mer_id,$cust_id,$admin_id,$send_by)
    {
        DB::table('nm_order_delivery_status')
        ->where('delStatus_id', $delStatus_id)
        ->update(['replace_status' => 2,"replace_approved_date"=>date("Y-m-d H:i:s")]);
        DB::table('nm_order')
        ->where('order_id', $cod_order_id)
        ->update(['delivery_status' => 10]);

        DB::table('delivery_status_chat')->insert(['delStatus_id' => $delStatus_id,'cust_id' => $cust_id,'mer_id' => $mer_id,'admin_id' => $admin_id,'send_by' => $send_by,'note' => $note, 'created_date' => date("Y-m-d H:i:s")]);

        return 0;
    }
    public static function approve_replace_payu($delStatus_id,$cod_order_id,$note,$mer_id,$cust_id,$admin_id,$send_by)
    {
        DB::table('nm_order_delivery_status')
        ->where('delStatus_id', $delStatus_id)
        ->update(['replace_status' => 2,"replace_approved_date"=>date("Y-m-d H:i:s")]);
        DB::table('nm_order_payu')
        ->where('order_id', $cod_order_id)
        ->update(['delivery_status' => 10]);

        DB::table('delivery_status_chat')->insert(['delStatus_id' => $delStatus_id,'cust_id' => $cust_id,'mer_id' => $mer_id,'admin_id' => $admin_id,'send_by' => $send_by,'note' => $note, 'created_date' => date("Y-m-d H:i:s")]);

        return 0;
    }
    public static function disapprove_replace_paypal($delStatus_id,$cod_order_id,$note,$mer_id,$cust_id,$admin_id,$send_by)
    {
        DB::table('nm_order_delivery_status')
        ->where('delStatus_id', $delStatus_id)
        ->update(['replace_status' => 2,"replace_approved_date"=>date("Y-m-d H:i:s")]);

        DB::table('nm_order')
        ->where('order_id', $cod_order_id)
        ->update(['delivery_status' => 4]); //delivery status reset as delivered

        DB::table('delivery_status_chat')->insert(['delStatus_id' => $delStatus_id,'cust_id' => $cust_id,'mer_id' => $mer_id,'admin_id' => $admin_id,'send_by' => $send_by,'note' => $note, 'created_date' => date("Y-m-d H:i:s")]);
        return 0;

    }
    public static function disapprove_replace_payu($delStatus_id,$cod_order_id,$note,$mer_id,$cust_id,$admin_id,$send_by)
    {
        DB::table('nm_order_delivery_status')
        ->where('delStatus_id', $delStatus_id)
        ->update(['replace_status' => 2,"replace_approved_date"=>date("Y-m-d H:i:s")]);

        DB::table('nm_order_payu')
        ->where('order_id', $cod_order_id)
        ->update(['delivery_status' => 4]); //delivery status reset as delivered

        DB::table('delivery_status_chat')->insert(['delStatus_id' => $delStatus_id,'cust_id' => $cust_id,'mer_id' => $mer_id,'admin_id' => $admin_id,'send_by' => $send_by,'note' => $note, 'created_date' => date("Y-m-d H:i:s")]);
        return 0;

    }
    /*replace order ends */

    /*Transcation -> Product COD ->Complete Orders*/
    public static function getcod_success_orders($merid)
    {
        return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')
        ->where('nm_ordercod.cod_merchant_id',$merid)
        ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')
        ->where('nm_ordercod.cod_status', '=', 1)
        ->where('nm_ordercod.cod_order_type', '=', 1)
        ->get();
    }
    /*Transcation -> Product COD ->Hold Orders*/
    public static function getcod_hold_orders($merid)
    {
        return DB::table('nm_ordercod')
        ->orderBy('cod_date', 'desc')
        ->where('nm_ordercod.cod_merchant_id',$merid)
        ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')
        ->where('nm_ordercod.cod_status', '=', 3)
        ->where('nm_ordercod.cod_order_type', '=', 1)
        ->get();
     }
    /*Transcation -> Product COD ->Failed Orders*/
    public static function getcod_failed_orders($merid)
    {
        return DB::table('nm_ordercod')
        ->orderBy('cod_date', 'desc')
        ->where('nm_ordercod.cod_merchant_id',$merid)
        ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')
        ->where('nm_ordercod.cod_status', '=', 4)
        ->where('nm_ordercod.cod_order_type', '=', 1)
        ->get();
    }
    
    public static function get_deals_all_orders($productlist)
    {
        return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_type', '=', 2)->whereIn('nm_order.order_pro_id', array(
            $productlist
        ))->get();
        
    }
    
    public static function getdeals_all_orders($merid)
    {
       return DB::select(DB::raw("SELECT pro_mr_id, GROUP_CONCAT(pro_id SEPARATOR ', ') as proid FROM nm_product GROUP BY pro_mr_id having pro_mr_id=$merid"));
 
    }
    
    public static function get_transaction_details($productlist)
    {
        return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order.order_type', '=', 2)->whereIn('nm_order.order_pro_id', array(
            $productlist
        ))->get();
        
    }
    
    public static function getdeals_success_orders($merid)
    {
       


        /**return DB::table('nm_order')->orderBy('order_date', 'desc')
        ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')
        ->where('nm_order.order_status', '=', 1)
        ->where('nm_order.order_type', '=', 2)
        ->where('nm_order.order_pro_id', array($productlist))
        ->get(); **/
        

     
        return DB::table('nm_order')
        ->orderBy('order_date', 'desc')
        ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')
        ->leftjoin('nm_merchant', 'nm_order.order_merchant_id', '=', 'nm_merchant.mer_id')
        ->where('nm_order.order_status', '=', 1)
        ->where('nm_order.order_type', '=', 2)
        ->where('nm_order.order_merchant_id',$merid)
        ->get();
    }
     public static function get_payu_deals_success_orders($merid)
    {
       

        return DB::table('nm_order_payu')
        ->orderBy('order_date', 'desc')
        ->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_deals', 'nm_order_payu.order_pro_id', '=', 'nm_deals.deal_id')
        ->leftjoin('nm_merchant', 'nm_order_payu.order_merchant_id', '=', 'nm_merchant.mer_id')
        ->where('nm_order_payu.order_status', '=', 1)
        ->where('nm_order_payu.order_type', '=', 2)
        ->where('nm_order_payu.order_merchant_id',$merid)
        ->get();
    }

    public static function getdeals_completed_orders($productlist)
    {
        return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 2)->where('nm_order.order_type', '=', 2)->whereIn('nm_order.order_pro_id', array(
            $productlist
        ))->get();
        
    }

    public static function getdeals_hold_orders($merid)
    {
        return DB::table('nm_order')
        ->orderBy('order_date', 'desc')
        ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')
        ->leftjoin('nm_merchant', 'nm_order.order_merchant_id', '=', 'nm_merchant.mer_id')
        ->where('nm_order.order_status', '=', 3)
        ->where('nm_order.order_type', '=', 2)
        ->where('nm_order.order_merchant_id',$merid)
        ->get();


        
    }
    public static function get_payu_deals_hold_orders($merid)
    {
        return DB::table('nm_order_payu')
        ->orderBy('order_date', 'desc')
        ->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_deals', 'nm_order_payu.order_pro_id', '=', 'nm_deals.deal_id')
        ->leftjoin('nm_merchant', 'nm_order_payu.order_merchant_id', '=', 'nm_merchant.mer_id')
        ->where('nm_order_payu.order_status', '=', 3)
        ->where('nm_order_payu.order_type', '=', 2)
        ->where('nm_order_payu.order_merchant_id',$merid)
        ->get();


        
    }
    
    public static function getdeals_failed_orders($productlist)
    {
        return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 4)->where('nm_order.order_type', '=', 2)->whereIn('nm_order.order_pro_id', array(
            $productlist
        ))->get();
        
    }
     
    public static function getdealscod_all_orders($productlist)
    {
        return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')->where('nm_ordercod.cod_order_type', '=', 2)->whereIn('nm_ordercod.cod_pro_id', array(
            $productlist
        ))->get();
    }
    public static function getdeal_cod_all_orders($merid){

        
        return DB::table('nm_ordercod')
        ->orderBy('cod_id', 'desc')
        ->where('cod_merchant_id',$merid)
        ->where('nm_ordercod.cod_order_type', '=', 2)
        ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')
        ->get();
    }
    
    public static function getdealscod_success_orders($mer_id)
    {
        return DB::table('nm_ordercod')
        ->orderBy('cod_id', 'desc')
        ->where('nm_ordercod.cod_merchant_id',$mer_id)
        ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')
        ->where('nm_ordercod.cod_status', '=', 1)
        ->where('nm_ordercod.cod_order_type', '=', 2)
        ->get();
    }

    public static function getdealscod_hold_orders($productlist)
    {
        return DB::table('nm_ordercod')
        ->orderBy('cod_id', 'desc')
        ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')->where('nm_ordercod.cod_status', '=', 3)->where('nm_ordercod.cod_order_type', '=', 2)->whereIn('nm_ordercod.cod_pro_id', array(
            $productlist
        ))->get();
        
    }
    public static function getdeals_cod_hold_orders($merid){
        return DB::table('nm_ordercod')
        ->orderBy('cod_id', 'desc')
        ->where('nm_ordercod.cod_merchant_id','=', $merid)
        ->where('nm_ordercod.cod_status', '=', 3)
        ->where('nm_ordercod.cod_order_type', '=', 2)
        ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')
        ->get();
    }
    
    public static function getdealscod_failed_orders($mer_id){
        return DB::table('nm_ordercod')
        ->orderBy('cod_id', 'desc')
        ->where('nm_ordercod.cod_merchant_id','=',$mer_id)
        ->where('nm_ordercod.cod_status', '=', 4)
        ->where('nm_ordercod.cod_order_type', '=', 2)
        ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
        ->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')
        ->get();
    }

    public static function get_producttransaction()
    {
        return DB::table('nm_order')->where('order_type', '=', 1)->count();
        
    }

    public static function get_dealstransaction()
    {
        return DB::table('nm_order')->where('order_type', '=', 2)->count();
    }

    public static function get_auctiontransaction()
    {
        return DB::table('nm_order_auction')->count();
    }

    public static function get_producttoday_order()
    {
        return DB::select(DB::raw("SELECT count(order_id) as count,sum(order_amt) as amt  from nm_order where order_type=1 and DATEDIFF(DATE(order_date),DATE(NOW()))=0"));
        
    }

    public static function get_product7days_order()
    {
        return DB::select(DB::raw("select count(order_id) as count,sum(order_amt) as amt from nm_order WHERE order_type=1 and (DATE(order_date) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY))"));
    }

    public static function get_product30days_order()
    {
        return DB::select(DB::raw("select  count(order_id) as count,sum(order_amt) as amt from nm_order WHERE order_type=1 and (DATE(order_date) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY))"));
    }

    public static function get_dealstoday_order()
    {
        return DB::select(DB::raw("SELECT count(order_id) as count,sum(order_amt) as amt  from nm_order where order_type=2 and DATEDIFF(DATE(order_date),DATE(NOW()))=0"));
        
    }

    public static function get_deals7days_order()
    {
        return DB::select(DB::raw("select count(order_id) as count,sum(order_amt) as amt from nm_order WHERE order_type=2 and (DATE(order_date) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY))"));
    }

    public static function get_deals30days_order()
    {
        return DB::select(DB::raw("select  count(order_id) as count,sum(order_amt) as amt from nm_order WHERE order_type=2 and (DATE(order_date) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY))"));
    }

    public static function get_auctiontoday_order()
    {
        return DB::select(DB::raw("SELECT count(oa_id) as count,sum(oa_original_bit_amt) as amt  from nm_order_auction where   DATEDIFF(DATE(oa_bid_date),DATE(NOW()))=0"));
        
    }

    public static function get_auction7days_order()
    {
        return DB::select(DB::raw("select count(oa_id) as count,sum(oa_original_bit_amt) as amt from nm_order_auction WHERE  (DATE(oa_bid_date) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY))"));
    }

    public static function get_auction30days_order()
    {
        return DB::select(DB::raw("select  count(oa_id) as count,sum(oa_original_bit_amt) as amt from nm_order_auction WHERE  (DATE(oa_bid_date) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY))"));
    }

    public static function get_chart_product_details($productlist,$merid)
    {   //only product transaction of cod & online payment which is success only 
    
        $chart_count = "";
        $current_year = date("Y");
        $current_month = date("m");
        for ($i = 1; $i <= 12; $i++) {
            
                $results   = DB::select(DB::raw("SELECT count(*) as count FROM nm_order WHERE order_pro_id in ($productlist) and  order_merchant_id =$merid  and order_type=1 and order_status!=4 and YEAR(`order_date`) = $current_year and MONTH( `order_date` ) = " . $i));
                $results_payu   = DB::select(DB::raw("SELECT count(*) as count FROM nm_order_payu WHERE order_pro_id in ($productlist) and  order_merchant_id =$merid  and order_type=1 and order_status!=4 and YEAR(`order_date`) = $current_year and MONTH( `order_date` ) = " . $i));
                
                $order_cod = DB::select(DB::raw("SELECT count(*) as count FROM nm_ordercod WHERE cod_pro_id in($productlist) and  cod_merchant_id in($merid)  and cod_order_type=1 and cod_status!=4 and YEAR(`cod_date`) = $current_year and MONTH( `cod_date` ) = " . $i));
                
            
            $total     = (($results[0]->count)+($results_payu[0]->count)+($order_cod[0]->count));
            $chart_count .= $total. ",";
           
        }
        return $chart_count1 = trim($chart_count, ",");
    }

    public static function get_chart_deals_details($productlist,$merid)
    {


        
      
        $chart_count = "";
        $current_year = date("Y");
        $current_month = date("m");

        for($i = 1; $i <= 12; $i++) {
           
                $results   = DB::select(DB::raw("SELECT count(*) as count FROM nm_order WHERE order_pro_id in ($productlist) and  order_merchant_id =$merid  and order_type=2 and order_status!=4 and YEAR(`order_date`) = $current_year and MONTH( `order_date` ) = " . $i));
                $results_payu   = DB::select(DB::raw("SELECT count(*) as count FROM nm_order_payu WHERE order_pro_id in ($productlist) and  order_merchant_id =$merid  and order_type=2 and order_status!=4 and YEAR(`order_date`) = $current_year and MONTH( `order_date` ) = " . $i));
                
                $order_cod = DB::select(DB::raw("SELECT count(*) as count FROM nm_ordercod WHERE cod_pro_id in($productlist) and  cod_merchant_id in($merid)  and cod_order_type=2 and cod_status!=4 and YEAR(`cod_date`) = $current_year and MONTH( `cod_date` ) = " . $i));
                

            
            $total     = (($results[0]->count)+($results_payu[0]->count)+($order_cod[0]->count));
            $chart_count .= $total. ",";
        }
       
        $chart_count1 = trim($chart_count, ",");
        return $chart_count1;
    }

    public static function get_chart_auction_details($getauctionidlist)
    {
        $chart_count = "";
        for ($i = 1; $i <= 12; $i++) {
            $results = DB::select(DB::raw("SELECT count(*) as count FROM nm_order_auction WHERE oa_pro_id in($getauctionidlist) and MONTH( `oa_bid_date` ) = " . $i));
            $chart_count .= $results[0]->count . ",";
        }
        $chart_count1 = trim($chart_count, ",");
        return $chart_count1;
    }
    
    public static function update_cod_status($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_ordercod')->where('cod_id', '=', $orderid)->where('cod_pro_id', '=', $proid)->update(array(
            'delivery_status' => $status
            ));
        } else {
            return DB::table('nm_ordercod')->where('cod_transaction_id', '=', $orderid)->where('cod_pro_id', '=', $proid)->update(array(
            'delivery_status' => $status
            ));
        }
    }
    public static function update_cod_status_and_delivery($status, $orderid,$proid,$pay_status)
    {
        if(is_int($orderid)) {
            return DB::table('nm_ordercod')->where('cod_id', '=', $orderid)->where('cod_pro_id', '=', $proid)->update(array(
            'delivery_status' => $status,
            'cod_status' => $pay_status
            ));
        } else {
            return DB::table('nm_ordercod')->where('cod_transaction_id', '=', $orderid)->where('cod_pro_id', '=', $proid)->update(array(
            'delivery_status' => $status,
            'cod_status' => $pay_status
            ));
        }
    }
    
    public static function update_cod_status_and_cancel($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_ordercod')->where('cod_id', '=', $orderid)->where('cod_pro_id', '=', $proid)->update(array(
            'delivery_status' => $status,
            ));
        } else {
            return DB::table('nm_ordercod')->where('cod_transaction_id', '=', $orderid)->where('cod_pro_id', '=', $proid)->update(array(
            'delivery_status' => $status,
            ));
        }
    }
    
    public static function update_cod_status_and_returned($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_ordercod')->where('cod_id', '=', $orderid)->where('cod_pro_id', '=', $proid)->update(array(
            'delivery_status' => $status,
            ));
        } else {
            return DB::table('nm_ordercod')->where('cod_transaction_id', '=', $orderid)->where('cod_pro_id', '=', $proid)->update(array(
            'delivery_status' => $status,
            ));
        }
    }
    
    public static function update_cod_status_and_replaced($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_ordercod')->where('cod_id', '=', $orderid)->where('cod_pro_id', '=', $proid)->update(array(
            'delivery_status' => $status,
            ));
        } else {
            return DB::table('nm_ordercod')->where('cod_transaction_id', '=', $orderid)->where('cod_pro_id', '=', $proid)->update(array(
            'delivery_status' => $status,
            ));
        }
    }
    
    public static function update_paypal_status($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_order')->where('order_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 1)->update(array(
            'delivery_status' => $status
            ));
        } else {
            return DB::table('nm_order')->where('transaction_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 1)->update(array(
            'delivery_status' => $status
            ));
        }
    }
     public static function update_payu_status($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_order_payu')->where('order_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 1)->update(array(
            'delivery_status' => $status
            ));
        } else {
            return DB::table('nm_order_payu')->where('transaction_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 1)->update(array(
            'delivery_status' => $status
            ));
        }
    }
    public static function update_paypal_status_cancel($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_order')->where('order_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 1)->update(array(
            'delivery_status' => $status
            ));
        } else {
            return DB::table('nm_order')->where('transaction_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 1)->update(array(
            'delivery_status' => $status
            ));
        }
    }
    public static function update_payu_status_cancel($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_order_payu')->where('order_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 1)->update(array(
            'delivery_status' => $status
            ));
        } else {
            return DB::table('nm_order_payu')->where('transaction_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 1)->update(array(
            'delivery_status' => $status
            ));
        }
    }
    public static function update_paypal_status_returned($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_order')->where('order_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 1)->update(array(
            'delivery_status' => $status
            ));
        } else {
            return DB::table('nm_order')->where('transaction_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 1)->update(array(
            'delivery_status' => $status
            ));
        }
    }
    public static function update_payu_status_returned($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_order_payu')->where('order_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 1)->update(array(
            'delivery_status' => $status
            ));
        } else {
            return DB::table('nm_order_payu')->where('transaction_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 1)->update(array(
            'delivery_status' => $status
            ));
        }
    }
    public static function update_paypal_status_replaced($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_order')->where('order_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 1)->update(array(
            'delivery_status' => $status
            ));
        } else {
            return DB::table('nm_order')->where('transaction_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 1)->update(array(
            'delivery_status' => $status
            ));
        }
    }
    public static function update_payu_status_replaced($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_order_payu')->where('order_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 1)->update(array(
            'delivery_status' => $status
            ));
        } else {
            return DB::table('nm_order_payu')->where('transaction_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 1)->update(array(
            'delivery_status' => $status
            ));
        }
    }
    
    public static function update_deal_paypal_status($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_order')->where('order_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 2)->update(array(
            'delivery_status' => $status
            ));
        } else {
            return DB::table('nm_order')->where('transaction_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 2)->update(array(
            'delivery_status' => $status
            ));
        }
    }
    public static function update_deal_payu_status($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_order_payu')->where('order_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 2)->update(array(
            'delivery_status' => $status
            ));
        } else {
            return DB::table('nm_order_payu')->where('transaction_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 2)->update(array(
            'delivery_status' => $status
            ));
        }
    }
    public static function update_deal_paypal_status_cancel($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_order')->where('order_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 2)->update(array(
            'delivery_status' => $status
            ));
        } else {
            return DB::table('nm_order')->where('transaction_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 2)->update(array(
            'delivery_status' => $status
            ));
        }
    }
     public static function update_deal_payu_status_cancel($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_order_payu')->where('order_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 2)->update(array(
            'delivery_status' => $status
            ));
        } else {
            return DB::table('nm_order_payu')->where('transaction_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 2)->update(array(
            'delivery_status' => $status
            ));
        }
    }
    public static function update_deal_paypal_status_returned($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_order')->where('order_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 2)->update(array(
            'delivery_status' => $status
            ));
        } else {
            return DB::table('nm_order')->where('transaction_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 2)->update(array(
            'delivery_status' => $status
            ));
        }
    }
    public static function update_deal_payu_status_returned($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_order_payu')->where('order_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 2)->update(array(
            'delivery_status' => $status
            ));
        } else {
            return DB::table('nm_order_payu')->where('transaction_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 2)->update(array(
            'delivery_status' => $status
            ));
        }
    }
    public static function update_deal_paypal_status_replaced($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_order')->where('order_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 2)->update(array(
            'delivery_status' => $status
            ));
        } else {
            return DB::table('nm_order')->where('transaction_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 2)->update(array(
            'delivery_status' => $status
            ));
        }
    }
    public static function update_deal_payu_status_replaced($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_order_payu')->where('order_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 2)->update(array(
            'delivery_status' => $status
            ));
        } else {
            return DB::table('nm_order_payu')->where('transaction_id', '=', $orderid)->where('order_pro_id', '=', $proid)->where('order_type', '=', 2)->update(array(
            'delivery_status' => $status
            ));
        }
    }
    
    public static function getmerchant_dealreports($from_date, $to_date, $mer_id)
    {
        $from = date("Y-m-d H:i:s", strtotime($from_date));
        $to   = date("Y-m-d H:i:s", strtotime($to_date));
        
        if ($from != '' & $to != '') {
            
            return DB::table('nm_order')
            ->orderBy('order_date', 'desc')
            ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
            ->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')
            ->where('nm_order.order_type', '=', 2)
            ->whereBetween('nm_order.order_date', array(
                $from,
                $to
            ))
            ->where('nm_order.order_merchant_id',$mer_id)
            ->get();
        }
        
    }
    public static function getmerchant_payu_dealreports($from_date, $to_date, $mer_id)
    {
        $from = date("Y-m-d H:i:s", strtotime($from_date));
        $to   = date("Y-m-d H:i:s", strtotime($to_date));
        
        if ($from != '' & $to != '') {
            
            return DB::table('nm_order_payu')
            ->orderBy('order_date', 'desc')
            ->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')
            ->leftjoin('nm_deals', 'nm_order_payu.order_pro_id', '=', 'nm_deals.deal_id')
            ->where('nm_order_payu.order_type', '=', 2)
            ->whereBetween('nm_order_payu.order_date', array(
                $from,
                $to
            ))
            ->where('nm_order_payu.order_merchant_id',$mer_id)
            ->get();
        }
        
    }
    
    
    public static function getsuccess_dealreports($from_date, $to_date, $mer_id)
    {
         $from = date("Y-m-d H:i:s", strtotime($from_date));
         $to   = date("Y-m-d H:i:s", strtotime($to_date));
           if ($from_date != '' & $to_date == '') {
            return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 1)->where('nm_order.order_type', '=', 2)->where('nm_order.order_date', $from)->where('nm_order.order_merchant_id',$mer_id)->get();
        }
        
        elseif ($from_date != '' & $to_date != '') {
            return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 1)->where('nm_order.order_type', '=', 2)->whereBetween('nm_order.order_date', array(
                $from,
                $to))->where('nm_order.order_merchant_id',$mer_id)->get();
         
        } else {

            
        }

        
    }
    public static function get_payu_success_dealreports($from_date, $to_date, $mer_id)
    {
         $from = date("Y-m-d H:i:s", strtotime($from_date));
         $to   = date("Y-m-d H:i:s", strtotime($to_date));
           if ($from_date != '' & $to_date == '') {
            return DB::table('nm_order_payu')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order_payu.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order_payu.order_status', '=', 1)->where('nm_order_payu.order_type', '=', 2)->where('nm_order_payu.order_date', $from)->where('nm_order_payu.order_merchant_id',$mer_id)->get();
        }
        
        elseif ($from_date != '' & $to_date != '') {
            return DB::table('nm_order_payu')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order_payu.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order_payu.order_status', '=', 1)->where('nm_order_payu.order_type', '=', 2)->whereBetween('nm_order_payu.order_date', array(
                $from,
                $to))->where('nm_order_payu.order_merchant_id',$mer_id)->get();
         
        } else {

            
        }

        
    }

    
    public static function getcomp_dealreports($from_date, $to_date, $productlist)
    {
        
        if ($from_date != '' & $to_date == '') {
            
            return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 2)->where('nm_order.order_type', '=', 2)->where('nm_order.created_date', $from_date)->whereIn('nm_order.order_pro_id', array(
                $productlist
            ))->get();
     
        }
        
        elseif ($from_date != '' & $to_date != '') {
            
            
            return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 2)->where('nm_order.order_type', '=', 2)->whereBetween('nm_order.created_date', array(
                $from_date,
                $to_date
            ))->whereIn('nm_order.order_pro_id', array(
                $productlist
            ))->get();
            
        } else {
            
        }
        
    }
    
    public static function allhold_reports($from_date, $to_date, $merid)
    {
        $from = date("Y-m-d H:i:s", strtotime($from_date));
         $to   = date("Y-m-d H:i:s", strtotime($to_date));

        
        if ($from_date != '' & $to_date == '') {
            
            return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 3)->where('nm_order.order_type', '=', 2)->where('nm_order.order_date', $from)->where('nm_order.order_merchant_id',$merid)->get();


            
        }
        
        elseif ($from_date != '' & $to_date != '') {
            
            return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 3)->where('nm_order.order_type', '=', 2)->whereBetween('nm_order.order_date', array(
                $from,
                $to))
            ->where('nm_order.order_merchant_id',$merid)->get();
            
        } else {
            
        }
        
    }
public static function payu_allhold_reports($from_date, $to_date, $merid)
    {
        $from = date("Y-m-d H:i:s", strtotime($from_date));
         $to   = date("Y-m-d H:i:s", strtotime($to_date));

        
        if ($from_date != '' & $to_date == '') {
            
            return DB::table('nm_order_payu')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order_payu.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order_payu.order_status', '=', 3)->where('nm_order_payu.order_type', '=', 2)->where('nm_order_payu.order_date', $from)->where('nm_order_payu.order_merchant_id',$merid)->get();


            
        }
        
        elseif ($from_date != '' & $to_date != '') {
            
            return DB::table('nm_order_payu')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order_payu.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order_payu.order_status', '=', 3)->where('nm_order_payu.order_type', '=', 2)->whereBetween('nm_order_payu.order_date', array(
                $from,
                $to))
            ->where('nm_order_payu.order_merchant_id',$merid)->get();
            
        } else {
            
        }
        
    }
    public static function returned_dealorder_allcod($from_date,$to_date,$merid){
        
         $sql= DB::table('nm_ordercod')
                    ->orderBy('cod_id', 'desc')
                    ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
                    ->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')
                    ->leftjoin('nm_merchant', 'nm_ordercod.cod_merchant_id', '=', 'nm_merchant.mer_id')
					->leftjoin('nm_order_delivery_status', 'nm_order_delivery_status.cod_order_id', '=', 'nm_ordercod.cod_id')
                    ->where('nm_ordercod.cod_order_type','=',2)
                    ->where('nm_ordercod.cod_paytype','=',0)
                     ->where('nm_ordercod.cod_merchant_id',$merid)
                    ->whereIn('nm_ordercod.delivery_status',array(7,8)); 
                    
                    $fr_date = date('Y-m-d',strtotime($from_date)); 
                    $t_date = date('Y-m-d',strtotime($to_date));  
                    if($from_date!="" & $to_date!="") 
                    { 
                    $sql->whereDate('nm_ordercod.cod_date', '>=' , $fr_date);
                    $sql->whereDate('nm_ordercod.cod_date', '<=' , $t_date);
                    } else if ($from_date == '' & $to_date != '')  {
                    $sql->whereDate('nm_ordercod.cod_date', '<=' , $t_date);
                    } else if ($from_date != '' & $to_date == '')  {
                    $sql->whereDate('nm_ordercod.cod_date', '>=' , $fr_date);   
                    }

                    return $sql->get();
            
    }
    
    public static function replace_dealorder_allcod($from_date,$to_date,$merid){
        
        $sql = DB::table('nm_ordercod')
                    ->orderBy('cod_id', 'desc')
                    ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
                    ->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')
                    ->leftjoin('nm_merchant', 'nm_ordercod.cod_merchant_id', '=', 'nm_merchant.mer_id')
					->leftjoin('nm_order_delivery_status', 'nm_order_delivery_status.cod_order_id', '=', 'nm_ordercod.cod_id')
                    ->where('nm_ordercod.cod_order_type','=',2)
                    ->where('nm_ordercod.cod_paytype','=',0)
                     ->where('nm_ordercod.cod_merchant_id',$merid)
                    ->whereIn('nm_ordercod.delivery_status',array(9,10)); 
                    
                    $fr_date = date('Y-m-d',strtotime($from_date)); 
                    $t_date = date('Y-m-d',strtotime($to_date));  
                    if($from_date!="" & $to_date!="") 
                    { 
                    $sql->whereDate('nm_ordercod.cod_date', '>=' , $fr_date);
                    $sql->whereDate('nm_ordercod.cod_date', '<=' , $t_date);
                    } else if ($from_date == '' & $to_date != '')  {
                    $sql->whereDate('nm_ordercod.cod_date', '<=' , $t_date);
                    } else if ($from_date != '' & $to_date == '')  {
                    $sql->whereDate('nm_ordercod.cod_date', '>=' , $fr_date);   
                    }

                    return $sql->get();
            
    }
    public static function calceled_dealorder_allcod($from_date,$to_date,$merid){
        
         $sql= DB::table('nm_ordercod')
                    ->orderBy('cod_id', 'desc')
                    ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
                    ->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')
                    ->leftjoin('nm_merchant', 'nm_ordercod.cod_merchant_id', '=', 'nm_merchant.mer_id')
					->leftjoin('nm_order_delivery_status', 'nm_order_delivery_status.cod_order_id', '=', 'nm_ordercod.cod_id')
                    ->where('nm_ordercod.cod_order_type','=',2)
                    ->where('nm_ordercod.cod_paytype','=',0)
                    ->where('nm_ordercod.cod_merchant_id',$merid)
                    ->whereIn('nm_ordercod.delivery_status',array(5,6)); 
                    
                    $fr_date = date('Y-m-d',strtotime($from_date)); 
                    $t_date = date('Y-m-d',strtotime($to_date));  
                    if($from_date!="" & $to_date!="") 
                    { 
                    $sql->whereDate('nm_ordercod.cod_date', '>=' , $fr_date);
                    $sql->whereDate('nm_ordercod.cod_date', '<=' , $t_date);
                    } else if ($from_date == '' & $to_date != '')  {
                    $sql->whereDate('nm_ordercod.cod_date', '<=' , $t_date);
                    } else if ($from_date != '' & $to_date == '')  {
                    $sql->whereDate('nm_ordercod.cod_date', '>=' , $fr_date);   
                    }

                    return $sql->get();
            
    }
    public static function replace_order_allpaypal($from_date,$to_date,$merid){
        
         $sql= DB::table('nm_order')
                    ->orderBy('order_id', 'desc')
                    ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
                    ->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
                    ->leftjoin('nm_merchant', 'nm_order.order_merchant_id', '=', 'nm_merchant.mer_id')
					->leftjoin('nm_order_delivery_status', 'nm_order_delivery_status.order_id', '=', 'nm_order.order_id')
					->select('nm_order.*', 'nm_customer.*', 'nm_product.*', 'nm_merchant.*', 'nm_order_delivery_status.order_id as paypal_order_id', 'nm_order_delivery_status.replace_status','nm_order_delivery_status.delStatus_id')
                    ->where('nm_order.order_type','=',1)
                    //->where('nm_order.order_paytype','=',1)  //2 for wallet
					->whereIn('nm_order.order_paytype',array(1,2)) 
                    ->where('nm_order.order_merchant_id',$merid)
                    ->whereIn('nm_order.delivery_status',array(9,10));
                    
                    $fr_date = date('Y-m-d',strtotime($from_date)); 
                    $t_date = date('Y-m-d',strtotime($to_date));  
                    if($from_date!="" & $to_date!="") 
                    { 
                    $sql->whereDate('nm_order.order_date', '>=' , $fr_date);
                    $sql->whereDate('nm_order.order_date', '<=' , $t_date);
                    } else if ($from_date == '' & $to_date != '')  {
                    $sql->whereDate('nm_order.order_date', '<=' , $t_date);
                    } else if ($from_date != '' & $to_date == '')  {
                    $sql->whereDate('nm_order.order_date', '>=' , $fr_date);    
                    }

                    return $sql->get();
            
    }
    public static function replace_order_allpayu($from_date,$to_date,$merid){
        
         $sql= DB::table('nm_order_payu')
                    ->orderBy('order_id', 'desc')
                    ->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')
                    ->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')
                    ->leftjoin('nm_merchant', 'nm_order_payu.order_merchant_id', '=', 'nm_merchant.mer_id')
                    ->leftjoin('nm_order_delivery_status', 'nm_order_delivery_status.order_id', '=', 'nm_order_payu.order_id')
                    ->select('nm_order_payu.*', 'nm_customer.*', 'nm_product.*', 'nm_merchant.*', 'nm_order_delivery_status.order_id as paypal_order_id', 'nm_order_delivery_status.replace_status','nm_order_delivery_status.delStatus_id')
                    ->where('nm_order_payu.order_type','=',1)
                    //->where('nm_order.order_paytype','=',1)  //2 for wallet
                    ->whereIn('nm_order_payu.order_paytype',array(1,2)) 
                    ->where('nm_order_payu.order_merchant_id',$merid)
                    ->whereIn('nm_order_payu.delivery_status',array(9,10));
                    
                    $fr_date = date('Y-m-d',strtotime($from_date)); 
                    $t_date = date('Y-m-d',strtotime($to_date));  
                    if($from_date!="" & $to_date!="") 
                    { 
                    $sql->whereDate('nm_order_payu.order_date', '>=' , $fr_date);
                    $sql->whereDate('nm_order_payu.order_date', '<=' , $t_date);
                    } else if ($from_date == '' & $to_date != '')  {
                    $sql->whereDate('nm_order_payu.order_date', '<=' , $t_date);
                    } else if ($from_date != '' & $to_date == '')  {
                    $sql->whereDate('nm_order_payu.order_date', '>=' , $fr_date);    
                    }

                    return $sql->get();
            
    }

    public static function returned_order_allpaypal($from_date,$to_date,$merid){
        
         $sql= DB::table('nm_order')
                    ->orderBy('order_id', 'desc')
                    ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
                    ->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
                    ->leftjoin('nm_merchant', 'nm_order.order_merchant_id', '=', 'nm_merchant.mer_id')
					->leftjoin('nm_order_delivery_status', 'nm_order_delivery_status.order_id', '=', 'nm_order.order_id')
					->select('nm_order.*', 'nm_customer.*', 'nm_product.*', 'nm_merchant.*', 'nm_order_delivery_status.order_id as paypal_order_id', 'nm_order_delivery_status.return_status','nm_order_delivery_status.delStatus_id')
                    ->where('nm_order.order_type','=',1)
                    //->where('nm_order.order_paytype','=',1) //2 for wallet
					->whereIn('nm_order.order_paytype',array(1,2))
                    ->where('nm_order.order_merchant_id',$merid)
                    ->whereIn('nm_order.delivery_status',array(7,8)); 
                    
                    $fr_date = date('Y-m-d',strtotime($from_date)); 
                    $t_date = date('Y-m-d',strtotime($to_date));  
                    if($from_date!="" & $to_date!="") 
                    { 
                    $sql->whereDate('nm_order.order_date', '>=' , $fr_date);
                    $sql->whereDate('nm_order.order_date', '<=' , $t_date);
                    } else if ($from_date == '' & $to_date != '')  {
                    $sql->whereDate('nm_order.order_date', '<=' , $t_date);
                    } else if ($from_date != '' & $to_date == '')  {
                    $sql->whereDate('nm_order.order_date', '>=' , $fr_date);    
                    }

                    return $sql->get();
            
    }
    public static function returned_order_allpayu($from_date,$to_date,$merid){
        
         $sql= DB::table('nm_order_payu')
                    ->orderBy('order_id', 'desc')
                    ->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')
                    ->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')
                    ->leftjoin('nm_merchant', 'nm_order_payu.order_merchant_id', '=', 'nm_merchant.mer_id')
                    ->leftjoin('nm_order_delivery_status', 'nm_order_delivery_status.order_id', '=', 'nm_order_payu.order_id')
                    ->select('nm_order_payu.*', 'nm_customer.*', 'nm_product.*', 'nm_merchant.*', 'nm_order_delivery_status.order_id as paypal_order_id', 'nm_order_delivery_status.return_status','nm_order_delivery_status.delStatus_id')
                    ->where('nm_order_payu.order_type','=',1)
                    //->where('nm_order.order_paytype','=',1) //2 for wallet
                    ->whereIn('nm_order_payu.order_paytype',array(1,2))
                    ->where('nm_order_payu.order_merchant_id',$merid)
                    ->whereIn('nm_order_payu.delivery_status',array(7,8)); 
                    
                    $fr_date = date('Y-m-d',strtotime($from_date)); 
                    $t_date = date('Y-m-d',strtotime($to_date));  
                    if($from_date!="" & $to_date!="") 
                    { 
                    $sql->whereDate('nm_order_payu.order_date', '>=' , $fr_date);
                    $sql->whereDate('nm_order_payu.order_date', '<=' , $t_date);
                    } else if ($from_date == '' & $to_date != '')  {
                    $sql->whereDate('nm_order_payu.order_date', '<=' , $t_date);
                    } else if ($from_date != '' & $to_date == '')  {
                    $sql->whereDate('nm_order_payu.order_date', '>=' , $fr_date);    
                    }

                    return $sql->get();
            
    }
    public static function allfailed_reports($from_date, $to_date, $merid)
    {
        $from = date("Y-m-d H:i:s", strtotime($from_date));
         $to   = date("Y-m-d H:i:s", strtotime($to_date));
        if ($from_date != '' & $to_date == '') {
            
            return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 4)->where('nm_order.order_type', '=', 2)->where('nm_order.order_date', $from)->where('nm_order.order_merchant_id',$merid)->get();
            
        }
        
        elseif ($from_date != '' & $to_date != '') {
            
            return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order.order_status', '=', 4)->where('nm_order.order_type', '=', 2)->whereBetween('nm_order.order_date', array(
                $from,
                $to
            ))->where('nm_order.order_merchant_id',$merid)->get();
        } else {
            
        }
        
    }
    public static function payu_deal_allfailed_reports($from_date, $to_date, $merid)
    {
        $from = date("Y-m-d H:i:s", strtotime($from_date));
         $to   = date("Y-m-d H:i:s", strtotime($to_date));
        if ($from_date != '' & $to_date == '') {
            
            return DB::table('nm_order_payu')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order_payu.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order_payu.order_status', '=', 4)->where('nm_order_payu.order_type', '=', 2)->where('nm_order_payu.order_date', $from)->where('nm_order_payu.order_merchant_id',$merid)->get();
            
        }
        
        elseif ($from_date != '' & $to_date != '') {
            
            return DB::table('nm_order_payu')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_order_payu.order_pro_id', '=', 'nm_deals.deal_id')->where('nm_order_payu.order_status', '=', 4)->where('nm_order_payu.order_type', '=', 2)->whereBetween('nm_order_payu.order_date', array(
                $from,
                $to
            ))->where('nm_order_payu.order_merchant_id',$merid)->get();
        } else {
            
        }
        
    }
    
    public static function allcod_reports($from_date, $to_date, $productlist)
    {
        
        if ($from_date != '' & $to_date == '') {
            
            return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')->where('nm_ordercod.cod_order_type', '=', 2)->where('nm_ordercod.created_date', $from_date)->whereIn('nm_ordercod.cod_pro_id', array(
                $productlist
            ))->get();
            
        }
        
        elseif ($from_date != '' & $to_date != '') {
            return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')->where('nm_ordercod.cod_order_type', '=', 2)->whereBetween('nm_ordercod.created_date', array(
                $from_date,
                $to_date
            ))->whereIn('nm_ordercod.cod_pro_id', array(
                $productlist
            ))->get();
        } else {
            
        }
        
    }

    public static function all_cod_reports($from_date, $to_date, $merid)
    {
        
         $from = date("Y-m-d H:i:s", strtotime($from_date));
         $to   = date("Y-m-d H:i:s", strtotime($to_date));

        if($from_date != '' & $to_date != '') {
            DB::connection()->enableQueryLog();
            return DB::table('nm_ordercod')
            ->where('nm_ordercod.cod_merchant_id','=',$merid)
            ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
            ->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')
            ->where('nm_ordercod.cod_order_type', '=', 2)
            ->whereBetween('nm_ordercod.cod_date', array(
                $from,
                $to
            ))
           ->get();
           
        } 
        
    }
    
    public static function allcodsuccess_reports($from_date, $to_date, $mer_id)   
    {
        $from_date = date("Y-m-d", strtotime($from_date));
        $to_date   = date("Y-m-d", strtotime($to_date));

        if ($from_date != '' & $to_date != '') {
            
            return DB::table('nm_ordercod')
            ->orderBy('cod_date', 'desc')
            ->where('cod_merchant_id','=',$mer_id)
            ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
            ->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')
            ->where('nm_ordercod.cod_status', '=', 1)
            ->where('nm_ordercod.cod_order_type', '=', 2)
            ->whereBetween('nm_ordercod.cod_date', array(
                $from_date,
                $to_date
            ))
            ->get();
        }
    }   
    
    
    public static function allcodhold_reports($from_date, $to_date, $mer_id)
    {
       $from_date = date("Y-m-d", strtotime($from_date));
       $to_date   = date("Y-m-d", strtotime($to_date));
        
        if($from_date != '' & $to_date != '') {
            return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')
            ->where('nm_ordercod.cod_merchant_id','=',$mer_id)
            ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
            ->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')
            ->where('nm_ordercod.cod_status', '=', 3)
            ->where('nm_ordercod.cod_order_type', '=', 2)
            ->whereBetween('nm_ordercod.cod_date', array(
                $from_date,
                $to_date
            ))->get();
        }
    }
    
    
    public static function allcodfailed_reports($from_date, $to_date, $mer_id)
    {
        $from_date = date("Y-m-d", strtotime($from_date));
        $to_date   = date("Y-m-d", strtotime($to_date));

        if ($from_date != '' & $to_date != '') {
            
            return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')
            ->where('nm_ordercod.cod_merchant_id',$mer_id)
            ->where('nm_ordercod.cod_status', '=', 4)
            ->where('nm_ordercod.cod_order_type', '=', 2)
            ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
            ->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')
            ->whereBetween('nm_ordercod.cod_date', array(
                $from_date,
                $to_date
            ))
            ->get();
        }
    }
    
    public static function alltrans_reports($from_date, $to_date, $merid)
    {
        $from_date = date("Y-m-d", strtotime($from_date));
        $to_date   = date("Y-m-d", strtotime($to_date));
        if ($from_date != '' & $to_date == '') {
            
            return DB::table('nm_order')
            ->orderBy('order_date', 'desc')
            ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
            ->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
            ->where('nm_order.order_type', '=', 1)
            ->where('nm_order.order_merchant_id',$merid)
            ->where('nm_order.order_date', $from_date)->get();
            
        }
        
        elseif ($from_date != '' & $to_date != '') {
            return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order.order_type', '=', 1)->where('nm_order.order_merchant_id',$merid)->whereBetween('nm_order.order_date', array(
                $from_date,
                $to_date
            ))->get();
        } else {
            
        }
        
    }
    public static function payu_alltrans_reports($from_date, $to_date, $merid)
    {
        $from_date = date("Y-m-d", strtotime($from_date));
        $to_date   = date("Y-m-d", strtotime($to_date));
        if ($from_date != '' & $to_date == '') {
            
            return DB::table('nm_order_payu')
            ->orderBy('order_date', 'desc')
            ->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')
            ->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')
            ->where('nm_order_payu.order_type', '=', 1)
            ->where('nm_order_payu.order_merchant_id',$merid)
            ->where('nm_order_payu.order_date', $from_date)->get();
            
        }
        
        elseif ($from_date != '' & $to_date != '') {
            return DB::table('nm_order_payu')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order_payu.order_type', '=', 1)->where('nm_order_payu.order_merchant_id',$merid)->whereBetween('nm_order_payu.order_date', array(
                $from_date,
                $to_date
            ))->get();
        } else {
            
        }
        
    }
    
    public static function allsucessprod_reports($from_date, $to_date, $merid)
    {
         $from_date = date("Y-m-d", strtotime($from_date));
        $to_date   = date("Y-m-d", strtotime($to_date));
        
        if ($from_date != '' & $to_date == '') {
            
            return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order.order_status', '=', 1)->where('nm_order.order_type', '=', 1)->where('nm_order.order_date', $from_date)
            ->where('nm_order.order_merchant_id',$merid)->get();
            
        }
        
        elseif ($from_date != '' & $to_date != '') {
            
            return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order.order_status', '=', 1)->where('nm_order.order_type', '=', 1)->whereBetween('nm_order.order_date', array(
                $from_date,
                $to_date
            ))
            ->where('nm_order.order_merchant_id',$merid)
            ->get();
            
            
        } else {
            
        }
        
    }
    public static function payu_allsucessprod_reports($from_date, $to_date, $merid)
    {
         $from_date = date("Y-m-d", strtotime($from_date));
        $to_date   = date("Y-m-d", strtotime($to_date));
        
        if ($from_date != '' & $to_date == '') {
            
            return DB::table('nm_order_payu')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order_payu.order_status', '=', 1)->where('nm_order_payu.order_type', '=', 1)->where('nm_order_payu.order_date', $from_date)
            ->where('nm_order_payu.order_merchant_id',$merid)->get();
            
        }
        
        elseif ($from_date != '' & $to_date != '') {
            
            return DB::table('nm_order_payu')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order_payu.order_status', '=', 1)->where('nm_order_payu.order_type', '=', 1)->whereBetween('nm_order_payu.order_date', array(
                $from_date,
                $to_date
            ))
            ->where('nm_order_payu.order_merchant_id',$merid)
            ->get();
            
            
        } else {
            
        }
        
    }
    public static function allcompletedprod_reports($from_date, $to_date, $merid)
    {
         $from_date = date("Y-m-d", strtotime($from_date));
        $to_date   = date("Y-m-d", strtotime($to_date));
        
        if ($from_date != '' & $to_date == '') {
            
            return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order.order_status', '=', 2)->where('nm_order.order_type', '=', 1)->where('nm_order.created_date', $from_date)->where('nm_order.order_merchant_id',$merid)->get();
            
        }
        
        elseif ($from_date != '' & $to_date != '') {
            
            return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order.order_status', '=', 2)->where('nm_order.order_type', '=', 1)->whereBetween('nm_order.created_date', array(
                $from_date,
                $to_date
            ))->where('nm_order.order_merchant_id',$merid)
            ->get();
            
        } else {
            
        }
        
    }
    
    
    public static function allholdprod_reports($from_date, $to_date, $merid)
    {
        
         $from_date = date("Y-m-d", strtotime($from_date));
        $to_date   = date("Y-m-d", strtotime($to_date));
        if ($from_date != '' & $to_date == '') {
            
            
            return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order.order_status', '=', 3)->where('nm_order.order_type', '=', 1)->where('nm_order.order_date', $from_date)->where('nm_order.order_merchant_id',$merid)->get();
            
            
        }
        
        elseif ($from_date != '' & $to_date != '') {
            
            return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order.order_status', '=', 3)->where('nm_order.order_type', '=', 1)->whereBetween('nm_order.order_date', array(
                $from_date,
                $to_date
            ))->where('nm_order.order_merchant_id',$merid)->get();
            
        } else {
            
        }
        
    }
    public static function payu_allholdprod_reports($from_date, $to_date, $merid)
    {
        
         $from_date = date("Y-m-d", strtotime($from_date));
        $to_date   = date("Y-m-d", strtotime($to_date));
        if ($from_date != '' & $to_date == '') {
            
            
            return DB::table('nm_order_payu')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order_payu.order_status', '=', 3)->where('nm_order_payu.order_type', '=', 1)->where('nm_order_payu.order_date', $from_date)->where('nm_order_payu.order_merchant_id',$merid)->get();
            
            
        }
        
        elseif ($from_date != '' & $to_date != '') {
            
            return DB::table('nm_order_payu')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order_payu.order_status', '=', 3)->where('nm_order_payu.order_type', '=', 1)->whereBetween('nm_order_payu.order_date', array(
                $from_date,
                $to_date
            ))->where('nm_order_payu.order_merchant_id',$merid)->get();
            
        } else {
            
        }
        
    }
    public static function allfailedprod_reports($from_date, $to_date, $merid)
    {
         $from_date = date("Y-m-d", strtotime($from_date));
        $to_date   = date("Y-m-d", strtotime($to_date));

        
        if ($from_date != '' & $to_date == '') {
            return DB::table('nm_order')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order.order_status', '=', 4)->where('nm_order.order_type', '=', 1)->where('nm_order.order_date', $from_date)->where('nm_order.order_merchant_id',$merid)->get();
            
        }
        
        elseif ($from_date != '' & $to_date != '') {
            
            
            return DB::table('nm_order')
            ->orderBy('order_date', 'desc')
            ->leftjoin('nm_customer', 'nm_order.order_cus_id', '=', 'nm_customer.cus_id')
            ->leftjoin('nm_product', 'nm_order.order_pro_id', '=', 'nm_product.pro_id')
            ->leftjoin('nm_merchant', 'nm_order.order_merchant_id', '=', 'nm_merchant.mer_id')
            ->where('nm_order.order_status', '=', 4)->where('nm_order.order_type', '=', 1)
            ->whereBetween('nm_order.order_date', array(
                $from_date,
                $to_date
            ))
            ->where('nm_order.order_merchant_id',$merid)->get();
            
        } else {
            
        }

         
        
    }
    public static function payu_allfailedprod_reports($from_date, $to_date, $merid)
    {
         $from_date = date("Y-m-d", strtotime($from_date));
        $to_date   = date("Y-m-d", strtotime($to_date));

        
        if ($from_date != '' & $to_date == '') {
            return DB::table('nm_order_payu')->orderBy('order_date', 'desc')->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order_payu.order_status', '=', 4)->where('nm_order_payu.order_type', '=', 1)->where('nm_order_payu.order_date', $from_date)->where('nm_order_payu.order_merchant_id',$merid)->get();
            
        }
        
        elseif ($from_date != '' & $to_date != '') {
            
            
            return DB::table('nm_order_payu')
            ->orderBy('order_date', 'desc')
            ->leftjoin('nm_customer', 'nm_order_payu.order_cus_id', '=', 'nm_customer.cus_id')
            ->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')
            ->leftjoin('nm_merchant', 'nm_order_payu.order_merchant_id', '=', 'nm_merchant.mer_id')
            ->where('nm_order_payu.order_status', '=', 4)->where('nm_order_payu.order_type', '=', 1)
            ->whereBetween('nm_order_payu.order_date', array(
                $from_date,
                $to_date
            ))
            ->where('nm_order_payu.order_merchant_id',$merid)->get();
            
        } else {
            
        }

         
        
    }
    public static function allpro_codsuccess_reports($from_date, $to_date, $mer_id)
    {
         $from_date = date("Y-m-d", strtotime($from_date));
        $to_date   = date("Y-m-d", strtotime($to_date));
        if ($from_date != '' & $to_date != '') {
            
            return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')
            ->where('nm_ordercod.cod_merchant_id','=',$mer_id)
            ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
            ->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')
            ->where('nm_ordercod.cod_status', '=', 1)
            ->where('nm_ordercod.cod_order_type', '=', 1)
            ->whereBetween('nm_ordercod.cod_date', array(
                $from_date,
                $to_date
            ))->get();
            
            
        } else {
            
        }
        
    }
    
    public static function allprod_codreports($from_date, $to_date, $mer_id)
    {
         $from_date = date("Y-m-d", strtotime($from_date));
        $to_date   = date("Y-m-d", strtotime($to_date));
        
        if ($from_date != '' & $to_date != '') {
            
            return DB::table('nm_ordercod')->orderBy('cod_date', 'desc')
            ->where('nm_ordercod.cod_merchant_id','=',$mer_id)
            ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
            ->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')
            ->where('nm_ordercod.cod_order_type', '=', 1)
            ->whereBetween('nm_ordercod.cod_date', array(
                $from_date,
                $to_date
            ))->get();
            
            
        }
        
    }

    public static function allprod_holdreports($from_date, $to_date, $mer_id)
    {
         $from_date = date("Y-m-d", strtotime($from_date));
        $to_date   = date("Y-m-d", strtotime($to_date));
        if ($from_date != '' & $to_date != '') {
         
            return DB::table('nm_ordercod')
            ->orderBy('cod_date', 'desc')
            ->where('nm_ordercod.cod_merchant_id','=',$mer_id)
            ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
            ->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')
            ->where('nm_ordercod.cod_status', '=', 3)
            ->where('nm_ordercod.cod_order_type', '=', 1)
            ->whereBetween('nm_ordercod.cod_date', array(
                $from_date,
                $to_date
            ))->get();
            
            
        }
    }
    
    
    public static function allprod_failedreports($from_date, $to_date, $mer_id)
    {
        $from_date = date("Y-m-d", strtotime($from_date));
        $to_date   = date("Y-m-d", strtotime($to_date));
        if ($from_date != '' & $to_date != '') {
            return DB::table('nm_ordercod')
            ->orderBy('cod_date', 'desc')
            ->where('nm_ordercod.cod_merchant_id','=',$mer_id)
            ->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
            ->leftjoin('nm_product', 'nm_ordercod.cod_pro_id', '=', 'nm_product.pro_id')
            ->where('nm_ordercod.cod_status', '=', 4)->where('nm_ordercod.cod_order_type', '=', 1)
            ->whereBetween('nm_ordercod.cod_date', array(
                $from_date,
                $to_date
            ))
            ->get();
            
        }
    }
	
	public static function getdetails_customer($orderid)
	{
		return DB::table('nm_ordercod')->Join('nm_product', 'nm_product.pro_id', '=', 'nm_ordercod.cod_pro_id')->Join('nm_customer', 'nm_customer.cus_id', '=', 'nm_ordercod.cod_cus_id')->where('nm_ordercod.cod_transaction_id', '=', $orderid)->get();
	}
	
	public static function getdetails_customer_deal($orderid)
	{
		return DB::table('nm_ordercod')->Join('nm_deals', 'nm_deals.deal_id', '=', 'nm_ordercod.cod_pro_id')->Join('nm_customer', 'nm_customer.cus_id', '=', 'nm_ordercod.cod_cus_id')->where('nm_ordercod.cod_transaction_id', '=', $orderid)->get();
	}
	
	public static function getdetails_customer_paypal($orderid)
	{
		return DB::table('nm_order')->Join('nm_product', 'nm_product.pro_id', '=', 'nm_order.order_pro_id')->Join('nm_customer', 'nm_customer.cus_id', '=', 'nm_order.order_cus_id')->where('nm_order.transaction_id', '=', $orderid)->get();
	}
    public static function getdetails_customer_payu($orderid)
    {
        return DB::table('nm_order_payu')->Join('nm_product', 'nm_product.pro_id', '=', 'nm_order_payu.order_pro_id')->Join('nm_customer', 'nm_customer.cus_id', '=', 'nm_order_payu.order_cus_id')->where('nm_order_payu.transaction_id', '=', $orderid)->get();
    }
	
	public static function getdetails_customer_paypal_deal($orderid)
	{
		return DB::table('nm_order')->Join('nm_deals', 'nm_deals.deal_id', '=', 'nm_order.order_pro_id')->Join('nm_customer', 'nm_customer.cus_id', '=', 'nm_order.order_cus_id')->where('nm_order.transaction_id', '=', $orderid)->get();
	}
    public static function getdetails_customer_payu_deal($orderid)
    {
        return DB::table('nm_order_payu')->Join('nm_deals', 'nm_deals.deal_id', '=', 'nm_order_payu.order_pro_id')->Join('nm_customer', 'nm_customer.cus_id', '=', 'nm_order_payu.order_cus_id')->where('nm_order_payu.transaction_id', '=', $orderid)->get();
    }
    

     public static function update_cod_status_admin($status, $orderid,$proid)
    {
        if(is_int($orderid)) {
            return DB::table('nm_ordercod')->where('cod_id', '=', $orderid)->where('cod_pro_id', '=', $proid)->update(array(
            'delivery_status' => $status
            ));
        } else {
            return DB::table('nm_ordercod')->where('cod_transaction_id', '=', $orderid)->where('cod_pro_id', '=', $proid)->update(array(
            'delivery_status' => $status
            ));
        }
    }
    
}

?>

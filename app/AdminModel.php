<?php
namespace App;
use DB;
use Session;
use File;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Auth\Authenticatable; 

class AdminModel extends Eloquent
{
    protected $guarded = array('id');
    protected $table = 'nm_admin';

    protected $fillable = ['adm_fname','adm_lname','adm_email','adm_password','access_group_id','adm_phone','adm_address1','admin_type','gender','dob','adm_ci_id','adm_co_id','image','status','target','age','sales_manager_id','forgotpassword_flag','last_log_in','country_code'];
    
    public static function get_admin_details()
    {
        return DB::table('nm_admin')->where('adm_id', '=', Session::get('userid'))->get();
    }
    
    public static function get_admin_profile_details()
    {
        return DB::table('nm_admin')->where('adm_id', '=', Session::get('userid'))->LeftJoin('nm_city', 'nm_city.ci_id', '=', 'nm_admin.adm_ci_id')->LeftJoin('nm_country', 'nm_country.co_id', '=', 'nm_admin.adm_co_id')->get();
    }
    
    public static function update_admin_details($entry)
    {
        return DB::table('nm_admin')->where('adm_id', '=', session::get('userid'))->update($entry);
    }
    
    public static function get_chart_details()
    {
        $chart_count = "";
        for ($i = 1; $i <= 12; $i++) {
            $results = DB::select(DB::raw("SELECT count(*) as count FROM nm_deals WHERE MONTH( `deal_posted_date` ) = " . $i));
            $chart_count .= $results[0]->count . ",";
            
        }
        $chart_count1 = trim($chart_count, ",");
        return $chart_count1;
        
    }

     public function getCity()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\City','ci_id','adm_ci_id');
    }


     public function getCountry()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Country','co_id','adm_co_id');
    }

    public function get_role()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\AdminUserGroup','id','access_group_id');
    }
    public function offer()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Offer','vendor_list' , 'adm_id');
    }
    public function plan()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Plan','vendor_list' , 'adm_id');
    }
}

?>

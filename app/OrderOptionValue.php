<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class OrderOptionValue extends Model 
{
	
	protected $table = 'nm_order_option_value';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cus_id','product_type','order_id','product_id','product_option_id','product_option_value_id','value','status','quantity','option_title','option_title_ar','option_value_title','option_value_title_ar','option_value_price','no_person','file'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	 public function getProductOptionValue()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\ProductOptionValue','id','product_option_value_id');
    }
    public function getProductOption()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\ProductOption','product_option_id','id');
    }
    public function getProduct()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\Products','product_id','pro_id');
    }
	
}

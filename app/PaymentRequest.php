<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class PaymentRequest extends Model 
{
	
	protected $table = 'nm_payment_requests';
	 protected $fillable = ['order_id', 'vendor_id', 'is_paid','amount', 'created_at', 'updated_at'];

	
	
	
}

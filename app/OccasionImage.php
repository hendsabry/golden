<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class OccasionImage extends Model 
{
	
	protected $table = 'nm_occasions_gallery';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = ['occasion_id','image_url'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
}

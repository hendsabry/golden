<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class ProductOptionValue extends Model 
{
	
	protected $table = 'nm_product_option_value';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','product_option_id','option_title','value','discount','discount_price'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	public function getProductOption()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\ProductOption','product_option_id','id');
    }

	
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorylist extends Model
{
    protected $table = 'nm_category';
    protected $primaryKey = 'mc_id';

    protected $guarded = ['id'];

    public function categories(){
		    $this->belongsToMany('App\Categorylist');
		}
	
}

<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class OrderServiceAttribute extends Model 
{
	
	protected $table = 'nm_order_services_attribute';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cus_id','product_type','order_id','category_id','product_id','attribute_id','sub_attribute_id','price','attribute_title','attribute_title_ar','sub_attribute_title','sub_attribute_title_ar','pro_title','pro_title_ar','pro_desc','pro_desc_ar','pro_price','pro_img','quantity','total_price','shop_vendor_id','shop_id','worker_price','insurance_amount'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	public function getServiceAttribute()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\CartInternalFoodDish','id','attribute_id');
    }

    public function getProductServiceAttribute()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\ServiceAttribute','id','attribute_id');
    }
    public function getSubProductAttribute()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\ServiceAttribute','id','sub_attribute_id');
    }
	public function getProducts()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Products','pro_id','product_id');
    }
	
}

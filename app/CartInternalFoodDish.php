<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class CartInternalFoodDish extends Model 
{
	
	protected $table = 'nm_cart_internal_food_dish';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cart_id','product_id','internal_food_menu_id','internal_food_dish_id','container_id','quantity','price','status','dish_name','dish_name_ar','container_title','img','no_people','container_image'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	public function getServiceAttribute()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\ServiceAttribute','id','internal_food_menu_id');
    }
    public function getFood()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\InternalFoodDish','id','internal_food_dish_id');
    }
	public function getContainer()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\InternalFoodContainer','id','container_id');
    }
	
}

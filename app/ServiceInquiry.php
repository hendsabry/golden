<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class ServiceInquiry extends Model 
{
	
	protected $table = 'nm_services_inquiry';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','relate_with','user_id','hall','occasion_type','city_id','location','duration','date','quation_for','time','user_comment','vendor_comment','price','the_groooms_name','recording_secation','music_type','singer_name','status','quote_for','song_name','merchant_id','mc_name','mc_name_ar','mc_discription','mc_discription_ar','mc_city','mc_img','mc_address','mc_address_ar','language_type','occasion_type_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	public function getVendorComment()
    {
        /* 2nd argument is foreign key in child (this!) table */
         return $this->hasMany('App\VendorReply','enq_id','id');
    }
	
	
}

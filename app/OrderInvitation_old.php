<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class OrderInvitation extends Model 
{
	
	protected $table = 'nm_order_invitation';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['packge_id','customer_id','vendor_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	public function getUser()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\User','customer_id');
    }
    public function getPackage()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Products','pro_id','packge_id');
    }
	
}

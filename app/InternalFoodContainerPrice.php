<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class InternalFoodContainerPrice extends Model 
{
	
	protected $table = 'nm_internal_food_container_price';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['dish_id','container_id','container_price'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	public function getContainerList()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\InternalFoodContainer','container_id','id');
    }
	
}

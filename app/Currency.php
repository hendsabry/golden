<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model 
{
	
	protected $table = 'nm_currency';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cur_id', 'cur_name','cur_code', 'cur_conversion_rate', 'cur_symbol', 'cur_status', 'cur_default', 'updated_at', 'created_at'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	 public function country()
    {

        return $this->belongsTo('App\Country', 'cur_name','co_id');
    }
}

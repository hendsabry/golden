<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model 
{
	
	protected $table = 'nm_product_option';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title','button_type_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	public function getBoxType()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\ProductButtonType','button_type_id');
    }
    public function OptionValue()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\ProductOptionValue','product_option_id','id');
    }
    public function getEnOptionValue()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\ProductOptionValue','product_option_id')->select('id','vandor_id','product_id','product_option_id','option_title','short_name','value','price','no_person','image');
    }

    public function getArOptionValue()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\ProductOptionValue','product_option_id')->select('id','vandor_id','product_id','product_option_id','option_title_ar as option_title','short_name_ar as short_name','value_ar as value','price','no_person','image');
    }

	
}

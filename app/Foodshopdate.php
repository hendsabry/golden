<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Foodshopdate extends Model
{
		protected $table = 'nm_category';
		


	public function Getfoodshopdate($id,$lang){
	
				   if ($id != '' && $lang == 'ar_lang') {
					   	$categoryinfo = DB::table('nm_category')->where('mc_status', '=', 1)->where('mc_id', '=', $id)->select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription','address_ar as address', 'mc_img','city_id','mc_video_url','mc_video_description','terms_conditions_ar as terms_conditions','longitude','latitude')->get(); 
				   }else{
						 $categoryinfo = DB::table('nm_category')->where('mc_status', '=', 1)->where('mc_id', '=', $id)->select('mc_id', 'mc_name', 'mc_img','mc_discription','city_id','mc_video_url','address','mc_video_description','terms_conditions','longitude','latitude')->get();
				}
				return $categoryinfo;
	
	}

 public function Getgalleryimages($id){
 				$categorygallery = DB::table('nm_category_gallery')->where('category_id', '=', $id)->select('id','image')->get(); 
				return $categorygallery;

 }
 
  public function GetshopReviews($id)
  {
  				
				$shopreview=DB::table('nm_review')->orderby('comment_id','desc')->Leftjoin('nm_category','nm_review.shop_id','=','nm_category.mc_id')->Leftjoin('nm_customer','nm_review.customer_id','=','nm_customer.cus_id')->where('nm_review.shop_id','!=','NULL')->where('nm_review.shop_id','=',$id)->where('nm_review.review_type','=','shop')->where('nm_review.status','=',1)->paginate(2);
				return $shopreview;
  }
  

  public function Getfoodshopdateproduct($id,$lang){
					$getPagelimit = config('app.paginate');
				   if ($id != '' && $lang == 'ar_lang') {
					   	$productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', 34)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title_ar as pro_title', 'pro_price', 'pro_Img','pro_disprice')->orderBy('pro_title')->paginate('9')->appends(request()->query());
				   }else{
						 $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', 34)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title', 'pro_Img','pro_price','pro_disprice')->orderBy('pro_title')->paginate('9')->appends(request()->query());
				}
				return $productdateshopinfo;
	
	}



public function Getfoodshopdatedishproduct($id,$lang,$attr){
					$getPagelimit = config('app.paginate');


  	$productdateshopinfoC = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', $attr)->where('pro_mc_id', '=', $id)->count();
  	if($productdateshopinfoC >=1)
  	{
  if ($id != '' && $lang == 'ar_lang') {
					   	$productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', $attr)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title_ar as pro_title', 'pro_price', 'pro_Img','pro_disprice')->orderBy('pro_title')->paginate('9')->appends(request()->query());
				   }else{
						 $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', $attr)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title', 'pro_Img','pro_price','pro_disprice')->orderBy('pro_title')->paginate('9')->appends(request()->query());
				}
  	}
  	else
  	{
  		$productdateshopinfo = array();
  	}

				 


				return $productdateshopinfo;
	
	}


	public function Getfoodshopdessertproduct($id,$lang,$attr){
					$getPagelimit = config('app.paginate');

$productdateshopinfoC = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', $attr)->where('pro_mc_id', '=', $id)->where('attribute_id', '=', $attr)->count();

if($productdateshopinfoC >=1)
{
if ($id != '' && $lang == 'ar_lang') {
					   	$productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $id)->where('attribute_id', '=', $attr)->select('pro_id', 'pro_title_ar as pro_title', 'pro_price', 'pro_Img','pro_disprice')->orderBy('pro_title')->paginate('9')->appends(request()->query());
				   }else{
						 $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', $attr)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title', 'pro_Img','pro_price','pro_disprice')->orderBy('pro_title')->paginate('9')->appends(request()->query());
				}	
}
else
{
	$productdateshopinfo = array();
}
				   
				return $productdateshopinfo;
	
	}

	public function Getfoodshopdessertdishproduct($id,$lang,$attr){
					$getPagelimit = config('app.paginate');


	$productdateshopinfos = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $id)->where('attribute_id', '=', $attr)->count();

if($productdateshopinfos >=1)
{
	 if ($id != '' && $lang == 'ar_lang') {
					   	$productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $id)->where('attribute_id', '=', $attr)->select('pro_id', 'pro_title_ar as pro_title', 'pro_price', 'pro_Img','pro_disprice')->orderBy('pro_title')->paginate('9')->appends(request()->query());
				   }else{
						 $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', $attr)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title', 'pro_Img','pro_price','pro_disprice')->orderBy('pro_title')->paginate('9')->appends(request()->query());
				}
}
else
{
	$productdateshopinfo = array();
}

				  
				return $productdateshopinfo;
	
	}

public function Getfoodshopdatefirstproduct($id,$lang,$getAttID){


					$getPagelimit = config('app.paginate');
 $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', $getAttID)->where('pro_mc_id', '=', $id)->where('attribute_id', '=', $getAttID)->count();
  if($productdateshopinfo >=1)
  {
  	 if ($id != '' && $lang == 'ar_lang') {
					  $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', $getAttID)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title_ar as pro_title', 'pro_price','pro_desc_ar as pro_desc', 'pro_Img','pro_disprice','pro_qty')->orderBy('pro_title')->first();

				   }else{
					$productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', $getAttID)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title', 'pro_Img','pro_price','pro_disprice','pro_desc','pro_qty')->orderBy('pro_title')->first();
				}
				
				$productdateshopproductprice = DB::table('nm_product_option_value')->where('product_id', '=', $productdateshopinfo->pro_id)->select('id', 'option_title_ar as option_title', 'product_option_id','value as product_option_value_id','discount','discount_price')->orderBy('id')->get();
						$productdateshopinfo->product_price = $productdateshopproductprice;
  }
  else
  {
  	$productdateshopinfo =array();
  }
 
				  
					//$productdateshopinfo->product_price = array();	
				return $productdateshopinfo;
				
	
	}


public function Getfoodshopdatedishfirstproduct($id,$lang,$attr){
					$getPagelimit = config('app.paginate');

$productdateshopinfoC = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', $attr)->where('pro_mc_id', '=', $id)->count();
if($productdateshopinfoC >=1)
{
  if ($id != '' && $lang == 'ar_lang') {
					  $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', $attr)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title_ar as pro_title', 'pro_price','pro_desc_ar as pro_desc', 'pro_Img','pro_disprice')->orderBy('pro_title')->first();

				   }else{
					$productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', $attr)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title', 'pro_Img','pro_price','pro_disprice','pro_desc')->orderBy('pro_title')->first();
				}
				
				$productdateshopproductprice = DB::table('nm_product_option_value')->where('product_id', '=', $productdateshopinfo->pro_id)->select('id', 'option_title_ar as option_title', 'product_option_id','value as product_option_value_id','discount','discount_price')->orderBy('id')->get();
						$productdateshopinfo->product_price = $productdateshopproductprice;
			 

}
else
{
	$productdateshopinfo = array();
}
				 


				return $productdateshopinfo;
				
	
	}

public function Getfoodshopdatedessertdishfirstproduct($id,$lang,$attr){
					$getPagelimit = config('app.paginate');


$productdateshopinfos = DB::table('nm_product')->where('attribute_id', '=', $attr)->where('pro_status', '=', 1)->where('pro_mc_id', '=', $id)->select('attribute_id')->orderBy('pro_title')->count();

 


if($productdateshopinfos >=1)
{
	   if ($id != '' && $lang == 'ar_lang') {
					  $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', $attr)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title_ar as pro_title', 'pro_price','pro_desc_ar as pro_desc', 'pro_Img','pro_disprice')->orderBy('pro_title')->first();

				   }else{
					$productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', $attr)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title', 'pro_Img','pro_price','pro_disprice','pro_desc')->orderBy('pro_title')->first();
				}

				$productdateshopproductprice = DB::table('nm_product_option_value')->where('product_id', '=', $productdateshopinfo->pro_id)->select('id', 'option_title_ar as option_title', 'product_option_id','value as product_option_value_id','discount','discount_price')->orderBy('id')->get();
						
				$productdateshopinfo->product_price = $productdateshopproductprice;
}
else
{
	$productdateshopinfo=array();
}
				
					
				return $productdateshopinfo;
				
	
	}
	
	 public function getajaxdatefoodproduct($id,$lang=''){
					//$id='107';
				   if ($id != '' && $lang == 'ar_lang') {
					   	$productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_id', '=', $id)->select('pro_id', 'pro_title_ar as pro_title','pro_desc_ar as pro_desc', 'pro_price', 'pro_Img','pro_disprice','pro_qty')->orderBy('pro_title')->get();
					   	
					   	$productdateshopproductprice = DB::table('nm_product_option_value')->where('product_id', '=', $id)->select('id', 'option_title_ar as option_title', 'product_option_id','value as product_option_value_id','discount','discount_price')->orderBy('id')->get();
						 $productdateshopinfo->product_price = $productdateshopproductprice;
				   }else{
						 $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_id', '=', $id)->select('pro_id', 'pro_title', 'pro_Img','pro_mc_id','attribute_id','pro_price','pro_disprice','option_id','pro_desc','pro_qty')->orderBy('pro_title')->get();
						 //echo $id;
						 
						 $productdateshopproductprice = DB::table('nm_product_option_value')->where('product_id', '=', $id)->select('id', 'option_title', 'product_option_id','value as product_option_value_id','discount','discount_price')->orderBy('id')->get();
						 $productdateshopinfo->product_price = $productdateshopproductprice;

						 
				}

				$productdatesshopfullinfo=array('productdateshopinfo'=>$productdateshopinfo,'productdateshopprice'=>$productdateshopproductprice);
				//echo "<pre>";
				//print_r($productdateshopinfo);
				return $productdatesshopfullinfo;
	
	}

}
?>
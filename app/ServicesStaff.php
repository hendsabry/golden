<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class ServicesStaff extends Model 
{
	
	protected $table = 'nm_services_staff';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','shop_id','service_id','staff_member_name','staff_member_name_ar','image','experience'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
	
}

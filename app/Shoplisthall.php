<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Session;
class Shoplisthall extends Model
{
		protected $table = 'nm_category';


 		

		public function Gethalllistbycityunderbugetgender($id,$city_id,$buget,$lang,$gender){
			$shopwithinbugetcity=array();
			if($gender=='male'){
				$searchgender='men,both';
			}elseif($gender=='female'){
				$searchgender='women,both';
			}elseif($gender=='Both'){
				$searchgender='men,women,both';
			}else{
				$searchgender='men,women,both';
			}
			$searchgender=explode(',', $searchgender);
			$basecategory_ids =Session::get('searchdata.mainselectedvalue');
			if($basecategory_ids ==1 ){ $basecategory_id = 2; }
 			if($basecategory_ids ==2 ){ $basecategory_id = 1; }

		$category = DB::table('nm_category')->select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $id)->first(); 
                if (!empty($category)) {

                    if ($id != '' && $lang == 'ar_lang') {

                        $shopwithinbugetcity1 = DB::table('nm_category')->where('mc_status', '=', 1)->where('parent_id', '=', $id)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get(); 
					foreach($shopwithinbugetcity1 as $winbc){
					 $subid=$winbc->mc_id;


					 $shopwithinbugetcitycount=DB::table('nm_category')->leftJoin('nm_product', 'nm_category.mc_id', '=', 'nm_product.pro_mc_id')->where('nm_category.mc_status', '=', 1)->whereIn('nm_product.hall_type', $searchgender)->where('nm_category.parent_id', '=', $subid)->where('nm_category.city_id', '=', $city_id)->where('nm_product.pro_price', '<=', $buget)->whereRaw("find_in_set($basecategory_id,nm_product.hall_category_type)")->count();

 

					   if($shopwithinbugetcitycount>0){
					   		$shopwithinbugetcity[] = DB::table('nm_category')->where('mc_status', '=', 1)->where('mc_id', '=', $subid)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get()->toArray();
					   }
					} 	
						
						
						
                    } else {
                        $shopwithinbugetcity1 = DB::table('nm_category')->where('mc_status', '=', 1)->where('parent_id', '=', $id)->select('mc_id', 'mc_name', 'mc_img')->get();
					
				foreach($shopwithinbugetcity1 as $winbc){
					 $subid=$winbc->mc_id;
					 $shopwithinbugetcitycount=DB::table('nm_category')->leftJoin('nm_product', 'nm_category.mc_id', '=', 'nm_product.pro_mc_id')->where('nm_category.mc_status', '=', 1)->where('nm_category.parent_id', '=', $subid)->whereIn('nm_product.hall_type', $searchgender)->where('nm_category.city_id', '=', $city_id)->where('nm_product.pro_price', '<=', $buget)->whereRaw("find_in_set($basecategory_id,nm_product.hall_category_type)")->count();
					   if($shopwithinbugetcitycount>0){
					   		$shopwithinbugetcity[] = DB::table('nm_category')->where('mc_status', '=', 1)->where('mc_id', '=', $subid)->select('mc_id', 'mc_name', 'mc_img')->get()->toArray();
					   }
					} 

                    }

                

                }
			return $shopwithinbugetcity;
		}






		
	public function Getshoplistbycityunderbuget($id,$city_id,$buget,$lang){
			$shopwithinbugetcity=array();
		$category = DB::table('nm_category')->select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $id)->first(); 
                if (!empty($category)) {

                    if ($id != '' && $lang == 'ar_lang') {

                        $shopwithinbugetcity1 = DB::table('nm_category')->where('mc_status', '=', 1)->where('parent_id', '=', $id)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get(); 
					foreach($shopwithinbugetcity1 as $winbc){
					 $subid=$winbc->mc_id;
					 $shopwithinbugetcitycount=DB::table('nm_category')->leftJoin('nm_product', 'nm_category.mc_id', '=', 'nm_product.pro_mc_id')->where('nm_category.mc_status', '=', 1)->where('nm_category.parent_id', '=', $subid)->where('nm_category.city_id', '=', $city_id)->where('nm_product.pro_price', '<=', $buget)->count();
					   if($shopwithinbugetcitycount>0){
					   		$shopwithinbugetcity[] = DB::table('nm_category')->where('mc_status', '=', 1)->where('mer_activity',1)->where('mc_id', '=', $subid)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get()->toArray();
					   }
					} 	
						
						
						
                    } else {
                        $shopwithinbugetcity1 = DB::table('nm_category')->where('mc_status', '=', 1)->where('parent_id', '=', $id)->select('mc_id', 'mc_name', 'mc_img')->get();
					
				foreach($shopwithinbugetcity1 as $winbc){
					 $subid=$winbc->mc_id;
					 $shopwithinbugetcitycount=DB::table('nm_category')->leftJoin('nm_product', 'nm_category.mc_id', '=', 'nm_product.pro_mc_id')->where('nm_category.mc_status', '=', 1)->where('nm_category.parent_id', '=', $subid)->where('nm_category.city_id', '=', $city_id)->where('nm_product.pro_price', '<=', $buget)->count();
					   if($shopwithinbugetcitycount>0){
					   		$shopwithinbugetcity[] = DB::table('nm_category')->where('mc_status', '=', 1)->where('mer_activity',1)->where('mc_id', '=', $subid)->select('mc_id', 'mc_name', 'mc_img')->get()->toArray();
					   }
					} 

                    }

                

                }
			return $shopwithinbugetcity;
		}

		public function Getshoplistbyall($id,$city_id,$buget,$lang,$within,$above,$offer)
		{

		$shopabovebugetcity=array();
		$category = DB::table('nm_category')->select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $id)->first(); 
                if (!empty($category)) {

                    if ($id != '' && $lang == 'ar_lang') {

                        $shopabovebugetcity1 = DB::table('nm_category')->where('mc_status', '=', 1)->where('parent_id', '=', $id)->where('city_id', '=', $city_id)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get(); 
					foreach($shopabovebugetcity1 as $winbc){
					 $subid=$winbc->mc_id;
 
						 if($within==1 && $above==1 && $offer==1)
						{
						$shopabovebugetcitycount=DB::table('nm_category')->leftJoin('nm_product', 'nm_category.mc_id', '=', 'nm_product.pro_mc_id')->where('nm_category.mc_status', '=', 1)->where('nm_category.parent_id', '=', $subid)->where('nm_category.city_id', '=', $city_id)->count();
						}
						elseif($within=='' && $above==1 && $offer==1)
						{
						$shopabovebugetcitycount=DB::table('nm_category')->leftJoin('nm_product', 'nm_category.mc_id', '=', 'nm_product.pro_mc_id')->where('nm_category.mc_status', '=', 1)->where('nm_category.parent_id', '=', $subid)->where('nm_category.city_id', '=', $city_id)->where('nm_product.pro_price', '>', $buget)->count();
						}
						elseif($within==1 && $above=='' && $offer==1)
						{
						$shopabovebugetcitycount=DB::table('nm_category')->leftJoin('nm_product', 'nm_category.mc_id', '=', 'nm_product.pro_mc_id')->where('nm_category.mc_status', '=', 1)->where('nm_category.parent_id', '=', $subid)->where('nm_category.city_id', '=', $city_id)->where('nm_product.pro_price', '<=', $buget)->count();
						}
						else 
						{
						$shopabovebugetcitycount=DB::table('nm_category')->leftJoin('nm_product', 'nm_category.mc_id', '=', 'nm_product.pro_mc_id')->where('nm_category.mc_status', '=', 1)->where('nm_category.parent_id', '=', $subid)->where('nm_category.city_id', '=', $city_id)->count();
						}


					 
					   if($shopabovebugetcitycount>0){
					   		$shopabovebugetcity[] = DB::table('nm_category')->where('mer_activity',1)->where('mc_status', '=', 1)->where('mc_id', '=', $subid)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get()->toArray();
					   }
					} 	
						
                    } else {
                        $shopabovebugetcity1 = DB::table('nm_category')->where('mc_status', '=', 1)->where('parent_id', '=', $id)->select('mc_id', 'mc_name', 'mc_img')->get();
					DB::enableQueryLog();
				foreach($shopabovebugetcity1 as $winbc){
					 $subid=$winbc->mc_id;
					 if($within==1 && $above==1 && $offer==1)
					{
					$shopabovebugetcitycount=DB::table('nm_category')->leftJoin('nm_product', 'nm_category.mc_id', '=', 'nm_product.pro_mc_id')->where('nm_category.mc_status', '=', 1)->where('nm_category.parent_id', '=', $subid)->where('nm_category.city_id', '=', $city_id)->count();
					}
					elseif($within=='' && $above==1 && $offer==1)
					{
					$shopabovebugetcitycount=DB::table('nm_category')->leftJoin('nm_product', 'nm_category.mc_id', '=', 'nm_product.pro_mc_id')->where('nm_category.mc_status', '=', 1)->where('nm_category.parent_id', '=', $subid)->where('nm_category.city_id', '=', $city_id)->where('nm_product.pro_price', '>', $buget)->count();
					}
					elseif($within==1 && $above=='' && $offer==1)
					{
					$shopabovebugetcitycount=DB::table('nm_category')->leftJoin('nm_product', 'nm_category.mc_id', '=', 'nm_product.pro_mc_id')->where('nm_category.mc_status', '=', 1)->where('nm_category.parent_id', '=', $subid)->where('nm_category.city_id', '=', $city_id)->where('nm_product.pro_price', '<=', $buget)->count();
					}
					else 
					{
					$shopabovebugetcitycount=DB::table('nm_category')->leftJoin('nm_product', 'nm_category.mc_id', '=', 'nm_product.pro_mc_id')->where('nm_category.mc_status', '=', 1)->where('nm_category.parent_id', '=', $subid)->where('nm_category.city_id', '=', $city_id)->count();
					}
					 
					   if($shopabovebugetcitycount>0){
					   		$shopabovebugetcity[] = DB::table('nm_category')->where('mer_activity',1)->where('mc_status', '=', 1)->where('mc_id', '=', $subid)->select('mc_id', 'mc_name', 'mc_img')->get()->toArray();
					   }
					} 

                    }

                

                }
			return $shopabovebugetcity;



		} 

public function Getshoplistbyoffer($id,$city_id,$buget,$lang)
{ 
$shopabovebugetcity=array();
//$basecategory_id=Session::get('searchdata.mainselectedvalue');

$basecategory_ids =Session::get('searchdata.mainselectedvalue');
			if($basecategory_ids ==1 ){ $basecategory_id = 2; }
 			if($basecategory_ids ==2 ){ $basecategory_id = 1; }

		$category = DB::table('nm_category')->select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $id)->first(); 
                if (!empty($category)) {

                    if ($id != '' && $lang == 'ar_lang') {

                        $shopabovebugetcity1 = DB::table('nm_category')->where('mc_status', '=', 1)->where('parent_id', '=', $id)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get(); 
					foreach($shopabovebugetcity1 as $winbc){ 
					 $subid=$winbc->mc_id;

					 $shopabovebugetcitycount=DB::table('nm_category')->leftJoin('nm_product', 'nm_category.mc_id', '=', 'nm_product.pro_mc_id')->where('nm_category.mc_status', '=', 1)->where('nm_category.parent_id', '=', $subid)->where('nm_category.city_id', '=', $city_id)->count();


					   if($shopabovebugetcitycount>0){
							$shopC =DB::table('nm_category')->select('mc_id')->where('parent_id', '=', $subid)->where('city_id', '=', $city_id)->get();
							$hallArry = array();
							foreach ($shopC  as $key => $value) {
							$mc_id = $value->mc_id;
							$getpro = DB::table('nm_product')->whereRaw("find_in_set($basecategory_id,hall_category_type)")->select('pro_id')->where('pro_mc_id', '=', $mc_id)->get();

							foreach ($getpro as $key => $value) {
								 $proid = $value->pro_id;
								 array_push($hallArry, $proid);
							}
														



							}
							$todaydate = date('Y-m-d');
							$chkOferHall = DB::table('nm_product_offer')->whereIn('pro_id', $hallArry)->where('status',1)->where('start_date', '<=',$todaydate)->where('end_date', '>=',$todaydate)->count();

							if($chkOferHall >=1)
							{
							$shopabovebugetcity[] = DB::table('nm_category')->where('mer_activity',1)->where('mc_status', '=', 1)->where('mc_id', '=', $subid)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get()->toArray();	
							}
		   		
					   }
					} 	
						
                    } else {
                        $shopabovebugetcity1 = DB::table('nm_category')->where('mc_status', '=', 1)->where('parent_id', '=', $id)->select('mc_id', 'mc_name', 'mc_img')->get();
				

				foreach($shopabovebugetcity1 as $winbc){
					 $subid=$winbc->mc_id;

					 $shopabovebugetcitycount=DB::table('nm_category')->leftJoin('nm_product', 'nm_category.mc_id', '=', 'nm_product.pro_mc_id')->where('nm_category.mc_status', '=', 1)->where('nm_category.parent_id', '=', $subid)->whereRaw("find_in_set($basecategory_id,nm_product.hall_category_type)")->where('nm_category.city_id', '=', $city_id)->count();


					   if($shopabovebugetcitycount>0){

							$shopC =DB::table('nm_category')->select('mc_id')->where('parent_id', '=', $subid)->where('city_id', '=', $city_id)->get();
							
							foreach ($shopC  as $key => $value) {
							$mc_id = $value->mc_id;
							$getpro = DB::table('nm_product')->whereRaw("find_in_set($basecategory_id,hall_category_type)")->select('pro_id')->where('pro_mc_id', '=', $mc_id)->get();



							$hallArry = array();
							foreach ($getpro as $key => $value) {
								 $proid = $value->pro_id;
								 array_push($hallArry, $proid);
							}
														



							}
							 
							$todaydate = date('Y-m-d');
							$chkOferHall = DB::table('nm_product_offer')->whereIn('pro_id', $hallArry)->where('status',1)->where('start_date', '<=',$todaydate)->where('end_date', '>=',$todaydate)->count();

							if($chkOferHall >=1)
							{
							$shopabovebugetcity[] = DB::table('nm_category')->where('mer_activity',1)->where('mc_status', '=', 1)->where('mc_id', '=', $subid)->select('mc_id', 'mc_name', 'mc_img')->get()->toArray();	
							}
		   		
					   }
					} 

                    }

                

                }
              //  print_r($shopabovebugetcity); die;
			return $shopabovebugetcity;

}

public function Getshoplistbycityunderabovebuget($id,$city_id,$buget,$lang){
$shopabovebugetcity=array();
		$category = DB::table('nm_category')->select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $id)->first(); 
                if (!empty($category)) {

                    if ($id != '' && $lang == 'ar_lang') {

                        $shopabovebugetcity1 = DB::table('nm_category')->where('mc_status', '=', 1)->where('parent_id', '=', $id)->where('city_id', '=', $city_id)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get(); 
					foreach($shopabovebugetcity1 as $winbc){
					 $subid=$winbc->mc_id;
					 $shopabovebugetcitycount=DB::table('nm_category')->leftJoin('nm_product', 'nm_category.mc_id', '=', 'nm_product.pro_mc_id')->where('nm_category.mc_status', '=', 1)->where('nm_category.parent_id', '=', $subid)->where('nm_category.city_id', '=', $city_id)->where('nm_product.pro_price', '>=', $buget)->count();
					   if($shopabovebugetcitycount>0){
					   		$shopabovebugetcity[] = DB::table('nm_category')->where('mer_activity',1)->where('mc_status', '=', 1)->where('mc_id', '=', $subid)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get()->toArray();
					   }
					} 	
						
                    } else {
                        $shopabovebugetcity1 = DB::table('nm_category')->where('mc_status', '=', 1)->where('parent_id', '=', $id)->select('mc_id', 'mc_name', 'mc_img')->get();
					DB::enableQueryLog();
				foreach($shopabovebugetcity1 as $winbc){
					 $subid=$winbc->mc_id;
					 $shopabovebugetcitycount=DB::table('nm_category')->leftJoin('nm_product', 'nm_category.mc_id', '=', 'nm_product.pro_mc_id')->where('nm_category.mc_status', '=', 1)->where('nm_category.parent_id', '=', $subid)->where('nm_category.city_id', '=', $city_id)->where('nm_product.pro_price', '>=', $buget)->count();
					   if($shopabovebugetcitycount>0){
					   		$shopabovebugetcity[] = DB::table('nm_category')->where('mer_activity',1)->where('mc_status', '=', 1)->where('mc_id', '=', $subid)->select('mc_id', 'mc_name', 'mc_img')->get()->toArray();
					   }
					} 

                    }

                

                }
			return $shopabovebugetcity;
}

		public function Getshoplistbycityabovebuget($id,$city_id,$buget,$lang){
			$shopabovebugetcity=array();

			 //$basecategory_id=Session::get('searchdata.mainselectedvalue');
			 $basecategory_ids =Session::get('searchdata.mainselectedvalue');
			if($basecategory_ids ==1 ){ $basecategory_id = 2; }
 			if($basecategory_ids ==2 ){ $basecategory_id = 1; }


		$category = DB::table('nm_category')->select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $id)->first(); 
                if (!empty($category)) {

                    if ($id != '' && $lang == 'ar_lang') {

                        $shopabovebugetcity1 = DB::table('nm_category')->where('mc_status', '=', 1)->where('parent_id', '=', $id)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get(); 
					foreach($shopabovebugetcity1 as $winbc){
					 $subid=$winbc->mc_id;
					 $shopabovebugetcitycount=DB::table('nm_category')->leftJoin('nm_product', 'nm_category.mc_id', '=', 'nm_product.pro_mc_id')->where('nm_category.mc_status', '=', 1)->where('nm_category.parent_id', '=', $subid)->where('nm_category.city_id', '=', $city_id)->where('nm_product.pro_price', '>', $buget)->whereRaw("find_in_set($basecategory_id,nm_product.hall_category_type)")->count();



					   if($shopabovebugetcitycount>0){
					   		$shopabovebugetcity[] = DB::table('nm_category')->where('mer_activity',1)->where('mc_status', '=', 1)->where('mc_id', '=', $subid)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get()->toArray();
					   }
					} 	
						
                    } else {
                        $shopabovebugetcity1 = DB::table('nm_category')->where('mc_status', '=', 1)->where('parent_id', '=', $id)->select('mc_id', 'mc_name', 'mc_img')->get();
					DB::enableQueryLog();
				foreach($shopabovebugetcity1 as $winbc){
					 $subid=$winbc->mc_id;
					 $shopabovebugetcitycount=DB::table('nm_category')->leftJoin('nm_product', 'nm_category.mc_id', '=', 'nm_product.pro_mc_id')->where('nm_category.mc_status', '=', 1)->where('nm_category.parent_id', '=', $subid)->where('nm_category.city_id', '=', $city_id)->where('nm_product.pro_price', '>', $buget)->whereRaw("find_in_set($basecategory_id,nm_product.hall_category_type)")->count();
					   if($shopabovebugetcitycount>0){
					   		$shopabovebugetcity[] = DB::table('nm_category')->where('mer_activity',1)->where('mc_status', '=', 1)->where('mc_id', '=', $subid)->select('mc_id', 'mc_name', 'mc_img')->get()->toArray();
					   }
					} 

                    }

                

                }
			return $shopabovebugetcity;
		}



}
?>
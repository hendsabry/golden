<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class StaffExperties extends Model 
{
	
	protected $table = 'nm_staff_experties';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','staff_id','product_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
	/*public function getStaff()
    {
         2nd argument is foreign key in child (this!) table 
        return $this->hasMany('App\ServicesStaff','id','staff_id');
    }*/
    public function getStaff()
    {
         /*2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\ServicesStaff','staff_id','id');
    }
}

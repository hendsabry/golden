<?php
include('wisitech.php'); 
Route::get('getbussinessbycategoryid', 'FrontendController@getbussinessbycategoryid');
Route::post('search-results', 'ShopController@searchresults')->name('search-results');
Route::get('search-results', 'ShopController@searchresults')->name('search-results');
Route::get('type-of-halls/{id}', 'HalltypeController@listallhalls');
Route::post('type-of-halls/{id}', 'HalltypeController@listallhalls');
Route::get('vendorlistbycity/{hid}/{id}', 'VendorlistController@vendorlistbycity');
Route::post('vendorlistbycity/{hid}/{id}', 'VendorlistController@vendorlistbycity');
Route::get('branchlist/{hid}/{id}/{sid}', 'ShopbranchlistController@branchlist');
Route::post('branchlist/{hid}/{id}/{sid}', 'ShopbranchlistController@branchlist');
Route::get('branchhallslist/{hid}/{id}/{sid}/{bid}', 'BranchhallslistController@hallslist');
Route::post('branchhallslist/{hid}/{id}/{sid}/{bid}', 'BranchhallslistController@hallslist');
Route::get('checkflxslider/{hid}/{id}/{sid}/{bid}/{showData_One}/{showData_Two}/{showData_three}', 'BranchhallslistController@checkflxslider');
Route::get('branchhalldetail/{hid}/{id}/{sid}/{bid}/{haid}', 'BranchhalldetailController@halldetail');
Route::post('branchhalldetail/{hid}/{id}/{sid}/{bid}/{haid}', 'BranchhalldetailController@halldetail');

Route::get('/branchhalldetail/setremoveservicessession', array('uses' => 'BranchhalldetailController@setremoveservicessession'));
Route::post('/branchhalldetail/setremoveservicessession', array('uses' => 'BranchhalldetailController@setremoveservicessession'));

Route::get('login-signup', 'SigninSignupController@signinsignup');
Route::post('login-signup', 'SigninSignupController@signinsignup');

Route::get('thankyou', 'ThankyouController@thanks');
Route::post('thankyou', 'ThankyouController@thanks');

Route::get('userforgotpassword', 'ForgotuserpasswordController@forgotpassword');
Route::post('userforgotpassword', 'ForgotuserpasswordController@forgotpassword');
Route::get('/userforgotpassword/checkuseraccount', array('uses' => 'ForgotuserpasswordController@checkuseraccount'));
Route::post('/userforgotpassword/checkuseraccount', array('uses' => 'ForgotuserpasswordController@checkuseraccount'));

Route::get('/userforgotpassword/checkloginaccount', array('uses' => 'ForgotuserpasswordController@checkloginaccount'));
Route::post('/userforgotpassword/checkloginaccount', array('uses' => 'ForgotuserpasswordController@checkloginaccount'));

Route::get('member_forgot_pwd_email/{eid}', array('uses' => 'ForgotuserpasswordController@member_forgot_pwd_email'));
Route::post('member_forgot_pwd_email/{eid}', array('uses' => 'ForgotuserpasswordController@member_forgot_pwd_email'));


///////// register user///////////////
Route::get('/login-signup/adduseraccount', array('uses' => 'SigninSignupController@adduseraccount'));
Route::post('/login-signup/adduseraccount', array('uses' => 'SigninSignupController@adduseraccount'));

Route::get('/login-signup/checkuseraccount', array('uses' => 'SigninSignupController@checkuseraccount'));
Route::post('/login-signup/checkuseraccount', array('uses' => 'SigninSignupController@checkuseraccount'));

Route::get('/login-signup/checkloginaccount', array('uses' => 'SigninSignupController@checkloginaccount'));
Route::post('/login-signup/checkloginaccount', array('uses' => 'SigninSignupController@checkloginaccount'));

Route::get('/login-signup/logoutuseraccount', array('uses' => 'SigninSignupController@logoutuseraccount'));
Route::post('/login-signup/logoutuseraccount', array('uses' => 'SigninSignupController@logoutuseraccount'));



/////////////// hall menu items ///////////////////
Route::get('/foodmenuitem/{id}', array('uses' => 'FoodmenuitemController@menuitemlist'));
Route::post('/foodmenuitem/{id}', array('uses' => 'FoodmenuitemController@menuitemlist'));


Route::get('additemstocart', array('uses' => 'AddtocartController@savecartitem'));
Route::post('additemstocart', array('uses' => 'AddtocartController@savecartitem'));

Route::get('mycart', array('uses' => 'MycartController@savecartitem'))->name('mycart');
Route::post('mycart', array('uses' => 'MycartController@savecartitem'));


///////////////////// food section ///////////////////

Route::get('foodcatgeory/{id}', 'FoodcatgeoryController@listallfoodcatgeory');
Route::post('foodcatgeory/{id}', 'FoodcatgeoryController@listallfoodcatgeory');

Route::get('foodsubcategory/{hid}/{id}', 'FoodsubcategoryController@listallfoodcatgeorybycity');
Route::post('foodsubcategory/{hid}/{id}', 'FoodsubcategoryController@listallfoodcatgeorybycity');

Route::get('foodshopbranch/{mcid}/{cid}/{bid}', 'FoodshopbranchController@listallfoodshopbranch');
Route::post('foodshopbranch/{mcid}/{cid}/{bid}', 'FoodshopbranchController@listallfoodshopbranch');

Route::get('foodshopbuffetmenu/{mcid}/{cid}/{bid}/{sid}', 'FoodshopbuffetmenuController@menuitemlist');
Route::post('foodshopbuffetmenu/{mcid}/{cid}/{bid}/{sid}', 'FoodshopbuffetmenuController@menuitemlist');

Route::get('foodshopbuffetmenu/addcartproduct', 'FoodshopbuffetmenuController@addcartproduct');
Route::post('foodshopbuffetmenu/addcartproduct', 'FoodshopbuffetmenuController@addcartproduct');






Route::get('datesshop/{mcid}/{cid}/{bid}', 'FoodshopdateController@fooddateshopandmenu');
Route::post('datesshop/{mcid}/{cid}/{bid}', 'FoodshopdateController@fooddateshopandmenu');

Route::get('datesdishshop/{mcid}/{cid}/{bid}', 'FoodshopdateController@fooddatedishshopandmenu');
Route::post('datesdishshop/{mcid}/{cid}/{bid}', 'FoodshopdateController@fooddatedishshopandmenu');


Route::get('datesshop/getcartproduct', 'FoodshopdateController@getcartproduct');
Route::post('datesshop/getcartproduct', 'FoodshopdateController@getcartproduct');

Route::get('datesshop/addcartproduct', 'FoodshopdateController@addcartproduct');
Route::post('datesshop/addcartproduct', 'FoodshopdateController@addcartproduct');


Route::get('dessertshop/{mcid}/{cid}/{bid}/{sid}', 'FoodshopdessertController@fooddessertshopandmenu');
Route::post('dessertshop/{mcid}/{cid}/{bid}/{sid}', 'FoodshopdessertController@fooddessertshopandmenu');

Route::get('dessertdishshop/{mcid}/{cid}/{bid}/{sid}', 'FoodshopdessertController@fooddessertshopanddishmenu');
Route::post('dessertdishshop/{mcid}/{cid}/{bid}/{sid}', 'FoodshopdessertController@fooddessertshopanddishmenu');


Route::get('dessertshop/getcartproduct', 'FoodshopdessertController@getcartproduct');
Route::post('dessertshop/getcartproduct', 'FoodshopdessertController@getcartproduct');


Route::get('dessertshop/addcartproduct', 'FoodshopdessertController@addcartproduct');
Route::post('dessertshop/addcartproduct', 'FoodshopdessertController@addcartproduct');

//////////////////end food section ///////////////////

////////////////////// clinic /////////////////

Route::get('cliniccatgeory/{id}', 'CliniccatgeoryController@listallcliniccatgeory');
Route::post('cliniccatgeory/{id}', 'CliniccatgeoryController@listallcliniccatgeory');

Route::get('clinicsubcategory/{hid}/{id}', 'ClinicsubcategoryController@listallclinicbycity');
Route::post('clinicsubcategory/{hid}/{id}', 'ClinicsubcategoryController@listallclinicbycity');

Route::get('clinicbranch/{hid}/{id}/{bid}', 'ClinicbranchController@listclinicenterbranch');
Route::post('clinicbranch/{hid}/{id}/{bid}', 'ClinicbranchController@listcliniccenterbranch');


Route::get('clinicshop/{mcid}/{cid}/{sid}/{bid}', 'ClinicshopController@clinicshopandservices');
Route::post('clinicshop/{mcid}/{cid}/{sid}/{bid}', 'ClinicshopController@clinicshopandservices');

Route::get('clinicshop/getcartproduct', 'ClinicshopController@getcartproduct');
Route::post('clinicshop/getcartproduct', 'ClinicshopController@getcartproduct');

Route::get('clinicshop/addcartproduct', 'ClinicshopController@addcartproduct');
Route::post('clinicshop/addcartproduct', 'ClinicshopController@addcartproduct');

//////////////// beauty and elegance////////////////

Route::get('beautyandelegancecatgeory/{id}', 'BeautyandelegancecatgeoryController@listallbeautyandelegancecatgeory');
Route::post('beautyandelegancecatgeory/{id}', 'BeautyandelegancecatgeoryController@listallbeautyandelegancecatgeory');

Route::get('beautyandelegancesubcategory/{hid}/{id}', 'BeautyandelegancesubcategoryController@listallbeautyandelegancecatgeorybycity');
Route::post('beautyandelegancesubcategory/{hid}/{id}', 'BeautyandelegancesubcategoryController@listallbeautyandelegancecatgeorybycity');

Route::get('beautycenterbranch/{hid}/{id}/{bid}', 'BeautycenterbranchController@listbeautycenterbranch');
Route::post('beautycenterbranch/{hid}/{id}/{bid}', 'BeautycenterbranchController@listbeautycenterbranch');

Route::get('beautyshop/{mcid}/{cid}/{sid}/{bid}', 'BeautyshopController@beautyshopandservices');
Route::post('beautyshop/{mcid}/{cid}/{sid}/{bid}', 'BeautyshopController@beautyshopandservices');

Route::get('beautyshop/getcartproduct', 'BeautyshopController@getcartproduct');
Route::post('beautyshop/getcartproduct', 'BeautyshopController@getcartproduct');

Route::get('beautyshop/staffavailability', 'BeautyshopController@staffavailability');
Route::post('beautyshop/staffavailability', 'BeautyshopController@staffavailability');

Route::get('beautyshop/addcartproduct', 'BeautyshopController@addcartproduct');
Route::post('beautyshop/addcartproduct', 'BeautyshopController@addcartproduct');

/////////////// saloon shop / Makeup artist ///////////////////

Route::get('beautyandeleganceshop/{mcid}/{cid}/{sid}', 'BeautyandeleganceshopController@beautyshopandservices');
Route::post('beautyandeleganceshop/{mcid}/{cid}/{sid}', 'BeautyandeleganceshopController@beautyshopandservices');

Route::get('beautyandeleganceshop/getcartproduct', 'BeautyandeleganceshopController@getcartproduct');
Route::post('beautyandeleganceshop/getcartproduct', 'BeautyandeleganceshopController@getcartproduct');

Route::get('beautyandeleganceshop/addcartproduct', 'BeautyandeleganceshopController@addcartproduct');
Route::post('beautyandeleganceshop/addcartproduct', 'BeautyandeleganceshopController@addcartproduct');

/////////////// makeup costmetic /////////////////////


Route::get('beautycosmeticshop/{mcid}/{cid}/{sid}', 'BeautycosmeticshopController@beautyshopandservices');
Route::post('beautycosmeticshop/{mcid}/{cid}/{sid}', 'BeautycosmeticshopController@beautyshopandservices');

Route::get('beautycosmeticshop/getcartproduct', 'BeautycosmeticshopController@getcartproduct');
Route::post('beautycosmeticshop/getcartproduct', 'BeautycosmeticshopController@getcartproduct');

Route::get('beautycosmeticshop/addcartproduct', 'BeautycosmeticshopController@addcartproduct');
Route::post('beautycosmeticshop/addcartproduct', 'BeautycosmeticshopController@addcartproduct');



/////////////// end makeup costmetic ////////////

/////////////// end shop ////////

/////////////////// end beauty and elegance////////



// START Occasion Co-ordinator ROUTES //

Route::get('occasion-co-ordinator/{id}', 'FrontOccasioncoController@occasionCoordinator')->name('occasion-co-ordinator');
Route::get('kosha-shops/{id}/{sid}', 'FrontOccasioncoController@manageVendors')->name('kosha-shops');
Route::get('photography-studio/{id}/{sid}', 'FrontOccasioncoController@manageVendors')->name('photography-studio');
Route::get('reception-and-hospitality/{id}/{sid}', 'FrontOccasioncoController@manageVendors')->name('reception-and-hospitality');
Route::get('roses/{id}/{sid}', 'FrontOccasioncoController@manageVendors')->name('roses');
Route::get('special-events/{id}/{sid}', 'FrontOccasioncoController@manageVendors')->name('special-events');
 Route::get('koshaservice', 'FrontOccasioncoController@koshaService')->name('koshaservice');
Route::post('kosha-add-to-cart', 'FrontOccasioncoController@setCoshaAddToCart')->name('kosha-add-to-cart');
Route::post('photography-add-to-cart', 'FrontOccasioncoController@setPhotographyStudioAddToCart')->name('photography-add-to-cart');
Route::get('kosha-redymadeshop-details/{id}/{sid}/{vid}/{type?}', 'FrontOccasioncoController@koshaShopdetails')->name('kosha-redymadeshop-details');
Route::get('kosha-shop-details/{id}/{sid}/{vid}/{type?}', 'FrontOccasioncoController@koshaShopdetails')->name('kosha-shop-details');
Route::get('photography-studio-details/{id}/{sid}/{vid}/{type?}', 'FrontOccasioncoController@photographyShopdetails')->name('photography-studio-details');
Route::get('videography-studio-details/{id}/{sid}/{vid}/{type?}', 'FrontOccasioncoController@photographyShopdetails')->name('videography-studio-details');
Route::post('roses-package-add-to-cart', 'FrontOccasioncoController@setRosesPackageAddToCart')->name('roses-package-add-to-cart');
Route::post('roses-single-add-to-cart', 'FrontOccasioncoController@setRosesPackageAddToCart')->name('roses-single-add-to-cart');
Route::get('getsingleProductInfo','FrontOccasioncoController@getsingleProductInfo');
Route::get('reception-and-hospitality-details/{id}/{sid}/{vid}/{type?}', 'FrontOccasioncoController@receptionShopdetails')->name('reception-and-hospitality-details');
Route::get('package-reception-and-hospitality-details/{id}/{sid}/{vid}/{type?}', 'FrontOccasioncoController@receptionShopdetails')->name('package-reception-and-hospitality-details');

Route::get('getcountryWrkerprice','FrontOccasioncoController@getcountryWrkerprice');
Route::get('single-roses-details/{id}/{sid}/{vid}/{type?}', 'FrontOccasioncoController@singleRosesdetails')->name('single-roses-details');
Route::get('getProductInfo','FrontOccasioncoController@getProductInfo');
Route::get('roses-details/{id}/{sid}/{vid}/{type?}', 'FrontOccasioncoController@rosesShopdetails')->name('roses-details');
Route::get('special-events-details/{id}/{sid}/{vid}', 'FrontOccasioncoController@specialeventsShopdetails')->name('special-events-details');
Route::post('specialevent-add-to-cart', 'FrontOccasioncoController@setSpecialEventAddToCart')->name('specialevent-add-to-cart');
Route::get('getpagination','FrontOccasioncoController@getpagination');
Route::post('recieption_addtoCart', 'FrontOccasioncoController@setRecieptionaddtoCart')->name('recieption_addtoCart');


// END Occasion Co-ordinator ROUTES // 




// START CHECKOUT ROUTES //
Route::get('checkout', 'FrontCheckoutController@checkout')->name('checkout');
Route::get('getcitylist', 'FrontCheckoutController@getcitylist')->name('getcitylist');
Route::get('getVat', 'FrontCheckoutController@getVat')->name('getVat');
Route::get('order-payment-success', 'FrontCheckoutController@paymentSuccess')->name('order-payment-success');
Route::get('order-payment-error', 'FrontCheckoutController@paymentError')->name('order-payment-error');
Route::get('thank-you', 'FrontCheckoutController@checkoutThanks')->name('thank-you');
Route::post('chkoutinfo', 'FrontCheckoutController@checkoutCalc')->name('chkoutinfo');
// END CHECKOUT ROUTES //
<?php
Route::post('add_singerinfo','MerchantSingerController@addsingerinfo')->name('add_singerinfo');
Route::post('add_acousticsinfo','MerchantAcousticsController@addsingerinfo')->name('add_acousticsinfo');
Route::post('add_singerpicture','MerchantSingerController@addsingerpicture')->name('add_singerpicture');
Route::post('add_acousticspicture','MerchantAcousticsController@addsingerpicture')->name('add_acousticspicture');
Route::post('storebuffetmanager','MerchantBuffetDashboardController@storebuffetmanager')->name('storebuffetmanager');
Route::get('editbuffetuser','MerchantBuffetDashboardController@editbuffetuser')->name('editbuffetuser');
Route::post('savebeautyservice','MerchantBeautyController@savebeautyservice')->name('savebeautyservice'); 
Route::post('savespaservice','MerchantSpaController@savespaservice')->name('savespaservice'); 
Route::post('storebeautyservice','MerchantBeautyController@storebeautyservice')->name('storebeautyservice');
Route::post('storespaservice','MerchantSpaController@storespaservice')->name('storespaservice');
Route::post('storebeautypackageservice','MerchantBeautyController@storebeautypackageservice')->name('storebeautypackageservice');
Route::post('storespapackageservice','MerchantSpaController@storespapackageservice')->name('storespapackageservice');
Route::post('storebeautyoffer','MerchantBeautyController@storebeautyoffer')->name('storebeautyoffer');
Route::post('storespaoffer','MerchantSpaController@storespaoffer')->name('storespaoffer');
Route::post('saveoudandperfumesservice','MerchantOudandPerfumesController@saveoudandperfumesservice')->name('saveoudandperfumesservice'); 
Route::post('storeoudandperfumesservice','MerchantOudandPerfumesController@storeoudandperfumesservice')->name('storeoudandperfumesservice');
Route::post('storeoudandperfumesoffer','MerchantOudandPerfumesController@storeoudandperfumesoffer')->name('storeoudandperfumesoffer');
Route::post('storeoudandperfumespackageservice','MerchantOudandPerfumesController@storeoudandperfumespackageservice')->name('storeoudandperfumespackageservice');
Route::post('oudandperfumes-updatecategory', 'MerchantOudandPerfumesController@oudandperfumesUpdatecategory')->name('oudandperfumes-updatecategory'); 
Route::post('addoudandperfumesbranch','MerchantOudandPerfumesController@addoudandperfumesbranch')->name('addoudandperfumesbranch');
Route::post('storeoudandperfumespictures','MerchantOudandPerfumesController@beautyShopPicture')->name('storeoudandperfumespictures');
Route::post('savegoldandjewelryservice','MerchantGoldandJewelryController@savegoldandjewelryservice')->name('savegoldandjewelryservice'); 
Route::post('storegoldandjewelryservice','MerchantGoldandJewelryController@storegoldandjewelryservice')->name('storegoldandjewelryservice');
Route::post('storegoldandjewelryoffer','MerchantGoldandJewelryController@storegoldandjewelryoffer')->name('storegoldandjewelryoffer');
Route::post('storegoldandjewelrypackageservice','MerchantGoldandJewelryController@storegoldandjewelrypackageservice')->name('storegoldandjewelrypackageservice');
Route::post('goldandjewelry-updatecategory', 'MerchantGoldandJewelryController@goldandjewelryUpdatecategory')->name('goldandjewelry-updatecategory'); 
Route::post('addgoldandjewelrybranch','MerchantGoldandJewelryController@addgoldandjewelrybranch')->name('addgoldandjewelrybranch');
Route::post('storegoldandjewelrypictures','MerchantGoldandJewelryController@beautyShopPicture')->name('storegoldandjewelrypictures');

Route::get('mobilogin','HomeController@mobilogin')->name('mobilogin');

Route::get('requestpayment','PaymentrequestController@requestpayment');
Route::post('requestpayment','PaymentrequestController@requestpayment');

/////////////// order management route///////////
////// abaya//////////////
Route::get('abaya-order/getproductstatus/{id?}/{sid?}','MerchantAbayaController@getproductstatus');
Route::post('abaya-order/getproductstatus/{id?}/{sid?}','MerchantAbayaController@getproductstatus');

Route::get('abaya-order/requestpayment','MerchantAbayaController@requestpayment');
Route::post('abaya-order/requestpayment','MerchantAbayaController@requestpayment');

Route::get('abaya-order/abayaorderdetail/{id?}/{sid?}/{opid?}/{prodid?}/{cusid?}/{oid?}','MerchantAbayaController@getabayaorderdetail');
Route::post('abaya-order/abayaorderdetail/{id?}/{sid?}/{opid?}/{prodid?}/{cusid?}/{oid?}','MerchantAbayaController@getabayaorderdetail');

///////////acoustic////////////


Route::get('acoustic-order/getproductstatus/{id?}/{sid?}','MerchantAcousticsController@getproductstatus');
Route::post('acoustic-order/getproductstatus/{id?}/{sid?}','MerchantAcousticsController@getproductstatus');

Route::get('acoustic-order/orderdetail/{id?}/{hid?}/{Cid?}/{cusid?}/{oid?}','MerchantAcousticsController@getorderdetail');
Route::post('acoustic-order/orderdetail/{id?}/{hid?}/{Cid?}/{cusid?}/{oid?}','MerchantAcousticsController@getorderdetail');



Route::get('tailor-order/orderdetail/{id?}/{itemid?}/{opid?}/{prodid?}/{cusid?}/{oid?}','MerchantMakucpTailorController@getorderdetail');
Route::post('tailor-order/orderdetail/{id?}/{itemid?}/{opid?}/{prodid?}/{cusid?}/{oid?}','MerchantMakucpTailorController@getorderdetail');

Route::get('makeup-order/orderdetail/{id?}/{itemid?}/{opid?}/{cusid?}/{oid?}','MerchantMakeupController@getorderdetail');
Route::post('makeup-order/orderdetail/{id?}/{itemid?}/{opid?}/{cusid?}/{oid?}','MerchantMakeupController@getorderdetail');

Route::get('makeup-artist-order/orderdetail/{id?}/{sid?}/{opid?}/{cusid?}/{oid?}/{pid?}','MerchantMakucpArtistsController@getorderdetail');
Route::post('makeup-artist-order/orderdetail/{id?}/{sid?}/{opid?}/{cusid?}/{oid?}','MerchantMakucpArtistsController@getorderdetail');


Route::get('spa-order/orderdetail/{id?}/{hid?}/{itemid?}/{opid?}/{cusid?}/{oid?}/{pid?}','MerchantSpaController@getorderdetail');
Route::post('spa-order/orderdetail/{id?}/{hid?}/{itemid?}/{opid?}/{cusid?}/{oid?}','MerchantSpaController@getorderdetail');


Route::get('mens-order/orderdetail/{id?}/{sid?}/{opid?}/{cusid?}/{oid?}/{pid?}','MerchantMensController@getorderdetail');
Route::post('mens-order/orderdetail/{id?}/{sid?}/{opid?}/{cusid?}/{oid?}','MerchantMensController@getorderdetail');

Route::get('beauty-order/orderdetail/{id?}/{hid?}/{itemid?}/{opid?}/{cusid?}/{oid?}/{pid?}','MerchantBeautyController@getorderdetail');
Route::post('beauty-order/orderdetail/{id?}/{hid?}/{itemid?}/{opid?}/{cusid?}/{oid?}','MerchantBeautyController@getorderdetail');

Route::get('dates-order/orderdetail/{id?}/{sid?}/{opid?}/{cusid?}/{oid?}','MerchantDatesController@getorderdetail');
Route::post('dates-order/orderdetail/{id?}/{sid?}/{opid?}/{cusid?}/{oid?}','MerchantDatesController@getorderdetail');

Route::get('dessert-order/orderdetail/{services_id?}/{shopid?}/{itemid?}/{opid?}/{cusid?}/{oid?}','MerchantDessertController@getorderdetail');
Route::post('dessert-order/orderdetail/{services_id?}/{shopid?}/{itemid?}/{opid?}/{cusid?}/{oid?}','MerchantDessertController@getorderdetail');


Route::get('buffet-orders/orderdetail/{id?}/{sid?}/{itemid?}/{opid?}/{cusid?}/{oid?}','MerchantBuffetDashboardController@getorderdetail');
Route::post('buffet-orders/orderdetail/{id?}/{sid?}/{itemid?}/{opid?}/{cusid?}/{oid?}','MerchantBuffetDashboardController@getorderdetail');


Route::get('photography-order/orderdetail/{id?}/{sid?}/{opid?}/{prodid?}/{cusid?}/{oid?}','MerchantPhotographyController@getorderdetail');
Route::post('photography-order/orderdetail/{id?}/{sid?}/{opid?}/{prodid?}/{cusid?}/{oid?}','MerchantPhotographyController@getorderdetail');




Route::get('event-order/orderdetail/{id?}/{shopid?}/{opid?}/{prodid?}/{cusid?}/{oid?}','MerchantEventController@getorderdetail');
Route::post('event-order/orderdetail/{id?}/{shopid?}/{opid?}/{prodid?}/{cusid?}/{oid?}','MerchantEventController@getorderdetail');

//////////////// end order mangement route ////////

Route::any('cosha-order-details/{id?}/{sid?}/{opid?}/{cusid?}/{oid?}','MerchantCoshaController@getorderdetail');

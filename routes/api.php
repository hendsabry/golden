<?php
  use Illuminate\Http\Request;
  /*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
  | Discription : Controller created for Golden Cage API
  | Created On : 26th April 2018
  | Created By : 
  */
  Route::get('/user', function (Request $request) {
  return $request->user();
  })->middleware('auth:api');
  /******************** All Common Routes ***************/
  Route::post('auth/login', 'API\ApiController@login');
  Route::post('register', 'API\ApiController@userRegister');
  Route::post('checkuserstatus', 'API\CheckuserstatusController@getUserstatus');
  Route::post('forgotpassword', 'API\ApiController@Forgotpassword');
  Route::post('socialsignup', 'API\ApiController@socialsignup');
  Route::post('userMobileVerification', 'API\ApiController@userMobileVerification');
  Route::group(['middleware' => 'jwt-auth'], function () {
  /***** User Edit **********************/
  Route::get('user', 'API\ApiController@getAuthUser');   
  Route::post('editprofile', 'API\ApiController@editProfile');
  Route::post('changepassword', 'API\ApiController@changePassword');
  /***** Search Common Controller**********************/
  Route::post('getSearchOption', 'API\SearchController@getSearchOption');
  Route::post('getSearchWeddingOccasion', 'API\SearchController@getSearchWeddingOccasion');
  Route::post('getSearchBusinessMeeting', 'API\SearchController@getSearchBusinessMeeting');
  Route::post('searchSubCategory', 'API\SearchController@searchSubCategory');
  Route::post('searchVendor', 'API\SearchController@searchVendor');

  Route::post('searchHallFilter', 'API\SearchController@searchHallFilter');
  //Him
  Route::post('searchVendordata', 'API\SearchController@searchVendordata');
  Route::post('searchBranch', 'API\SearchController@searchBranch');
  Route::post('viewCart', 'API\SearchController@viewCart');
  Route::post('viewCartVendorList', 'API\SearchController@viewCartVendorList');
  Route::post('searchSubVendor', 'API\SearchController@searchSubVendor');
  //Him
  Route::post('viewCartSubcategory', 'API\SearchController@viewCartSubcategory');
  Route::post('viewCartList', 'API\SearchController@viewCartList');
  Route::post('viewCartDetails', 'API\SearchController@viewCartDetails');
  Route::post('UpdateQuantity', 'API\SearchController@UpdateQuantity');
  /******************** Search Hall's Sub Category ***************/
  Route::post('searchHallSubCategory', 'API\SearchHallController@searchHallSubCategory');
  Route::post('searchHallVendor', 'API\SearchHallController@searchHallVendor');
  Route::post('searchVendorBranch', 'API\SearchHallController@searchVendorBranch');
  Route::post('getHallList', 'API\SearchHallController@getHallList');
  Route::post('getShortHallDetail', 'API\SearchHallController@getShortHallDetail');
  Route::post('getHallInternalMenu', 'API\SearchHallController@getHallInternalMenu');
  Route::post('getFullHallDetail', 'API\SearchHallController@getFullHallDetail');
  Route::post('setHallAddToCart', 'API\SearchHallController@HallAddToCart');
  Route::post('getHallAddToCart', 'API\SearchHallController@getHallAddToCart');
  Route::post('deleteAddToCartData', 'API\SearchHallController@deleteAddToCartData');
  /******************** Food Controller ***************/
  Route::post('getDateFoodShopInfo', 'API\FoodController@getDateFoodShopInfo');
  //Him
  Route::post('ProductGalleryImage', 'API\FoodController@ProductGalleryImage');
  Route::post('updateQuantity', 'API\FoodController@updateQuantity');
  Route::post('updateQuantityPackage', 'API\FoodController@updateQuantityPackage');
  Route::post('getDessertFoodShopInfo', 'API\FoodController@getDessertFoodShopInfo');
  Route::post('getFoodMenu', 'API\FoodController@getFoodMenu');
  Route::post('setFoodAddToCart', 'API\FoodController@FoodAddToCart');
  Route::post('setExternalFoodAddToCart', 'API\FoodController@externalFoodAddToCart');
  Route::post('getFoodAddToCart', 'API\FoodController@getFoodAddToCart');
  Route::post('deleteFoodData', 'API\FoodController@deleteFoodData');
  Route::post('deleteBuffetData', 'API\FoodController@deleteBuffetData');
  //Himanshu
  Route::post('getBuffeFoodShopInfo', 'API\FoodController@getBuffeFoodShopInfo');
  Route::post('updateDishQuantity', 'API\FoodController@updateDishQuantity');
  /********************Beauty And Elegance Controller***************/
  Route::post('getSaloonInfo', 'API\BeautyAndEleganceController@getSaloonInfo');
  Route::post('getBeautyAndSpaInfo', 'API\BeautyAndEleganceController@getBeautyAndSpaInfo');
  Route::post('getMakeupArtistInfo', 'API\BeautyAndEleganceController@getMakeupArtistInfo');
  Route::post('getWorkerList', 'API\BeautyAndEleganceController@getWorkerList');
  //Himanshu
  Route::post('getUserReviewDetails', 'API\BeautyAndEleganceController@getUserReviewDetails');
  Route::post('setBookAppointment', 'API\BeautyAndEleganceController@setBookAppointment');
  Route::post('setMakeupAddToCart', 'API\BeautyAndEleganceController@setMakeupAddToCart');
  Route::post('getMakeupAddToCart', 'API\BeautyAndEleganceController@getMakeupAddToCart');
  Route::post('deleteBeautyData', 'API\BeautyAndEleganceController@deleteBeautyData');
  Route::post('staffworkerlist', 'API\BeautyAndEleganceController@staffworkerlist'); 
  Route::post('workerDuration', 'API\BeautyAndEleganceController@workerDuration'); 
  Route::post('staffavailability', 'API\BeautyAndEleganceController@staffavailability'); 
  /******************** Clinics Controller ***************/
  Route::post('getClinicInfo', 'API\ClinicsController@getClinicInfo');
  Route::post('getDoctorBookingList', 'API\ClinicsController@getDoctorBookingList');
  Route::post('setBookDoctorAppointment', 'API\ClinicsController@setBookDoctorAppointment');
  Route::post('getClinicAddToCartData', 'API\ClinicsController@getClinicAddToCartData');
  Route::post('deleteClinicData', 'API\ClinicsController@deleteClinicData');
  /******************** Reviving Concerts Controller ***************/
  Route::post('getMusicAndBandList', 'API\RevivingConcertsController@getMusicAndBandList');
  Route::post('getMusicAndBandInfo', 'API\RevivingConcertsController@getMusicAndBandInfo');
  Route::post('serviceInquiry', 'API\RevivingConcertsController@Inquiry');
  Route::post('getMusicAcousticInfo', 'API\RevivingConcertsController@getMusicAcousticInfo');
  Route::post('getMusicAcousticInquiry', 'API\RevivingConcertsController@getMusicAcousticInquiry');
  Route::post('MusicAcousticAddToCart', 'API\RevivingConcertsController@MusicAcousticAddToCart');
  Route::post('getMusicAcousticAddToCart', 'API\RevivingConcertsController@getMusicAcousticAddToCart'); 
  Route::post('deleteMusicAcousticData', 'API\RevivingConcertsController@deleteMusicAcousticData'); 
  /*****Occasion Co-ordinator Controller**********************/
  Route::post('getCoshaInfo', 'API\OccasionCoordinatorController@getCoshaInfo');
  Route::post('getPhotographyStudioInfo', 'API\OccasionCoordinatorController@getPhotographyStudioInfo');
  Route::post('getRosesPackageInfo', 'API\OccasionCoordinatorController@getRosesPackageInfo');
  Route::post('getSpecialEventInfo', 'API\OccasionCoordinatorController@getSpecialEventInfo');
  Route::post('getElectronicInvitations', 'API\OccasionCoordinatorController@getElectronicInvitations');
  Route::post('getReceptionAndHospitalityInfo', 'API\OccasionCoordinatorController@getReceptionAndHospitalityInfo');
  Route::post('setCoshaAddToCart', 'API\OccasionCoordinatorController@setCoshaAddToCart');
  Route::post('setPhotographyStudioAddToCart', 'API\OccasionCoordinatorController@setPhotographyStudioAddToCart');
  Route::post('setRosesPackageAddToCart', 'API\OccasionCoordinatorController@setRosesPackageAddToCart');
  Route::post('setSpecialEventAddToCart', 'API\OccasionCoordinatorController@setSpecialEventAddToCart');
  Route::post('setElectronicInvitationAddToCart', 'API\OccasionCoordinatorController@setElectronicInvitationAddToCart');
  Route::post('getDesignerCard', 'API\OccasionCoordinatorController@getDesignerCard');
  Route::post('getEventAddToCartData', 'API\OccasionCoordinatorController@getEventAddToCartData');
  Route::post('getInvitationAddToCartData', 'API\OccasionCoordinatorController@getInvitationAddToCartData');
  Route::post('getRoseAddToCartData', 'API\OccasionCoordinatorController@getRoseAddToCartData');
  Route::post('getPhotographyAddToCartData', 'API\OccasionCoordinatorController@getPhotographyAddToCartData');
  Route::post('getCoshaAddToCartData', 'API\OccasionCoordinatorController@getCoshaAddToCartData');
  Route::post('setHospitalityAddToCart', 'API\OccasionCoordinatorController@setHospitalityAddToCart');
  Route::post('getHospitalityAddToCartData', 'API\OccasionCoordinatorController@getHospitalityAddToCartData');
  Route::post('deleteOccasionData', 'API\OccasionCoordinatorController@deleteOccasionData');
  Route::post('uploadCSV', 'API\OccasionCoordinatorController@uploadCSV');
  //Himanshu
  Route::post('updateQuantityAttributes', 'API\OccasionCoordinatorController@updateQuantityAttributes');
   /*****Car Rental Controller**********************/
  Route::post('getCarRentalInfo', 'API\CarRentalController@getCarRentalInfo');
  Route::post('setCarRentalAddToCart', 'API\CarRentalController@setCarRentalAddToCart');
  Route::post('getCarRentalAddToCartData', 'API\CarRentalController@getCarRentalAddToCartData');
  Route::post('deleteCarRentalData', 'API\CarRentalController@deleteCarRentalData');
  /*****Travel Agencies Controller**********************/
  Route::post('getTravelAgenciesInfo', 'API\TravelAgenciesController@getTravelAgenciesInfo');
  Route::post('setTravelAgenciesAddToCart', 'API\TravelAgenciesController@setTravelAgenciesAddToCart');
  Route::post('getTravelAgenciesAddToCartData', 'API\TravelAgenciesController@getTravelAgenciesAddToCartData');
  Route::post('deleteTravelData', 'API\TravelAgenciesController@deleteTravelData');
  /*****Shopping Controller**********************/
  Route::post('getTailorsInfo', 'API\ShoppingController@getTailorsInfo');
  Route::post('getDressesInfo', 'API\ShoppingController@getDressesInfo');
  Route::post('getAbayaInfo', 'API\ShoppingController@getAbayaInfo');
  Route::post('getOudAndPerfumesInfo', 'API\ShoppingController@getOudAndPerfumesInfo');
  Route::post('getGoldAndJewelryInfo', 'API\ShoppingController@getGoldAndJewelryInfo');
  Route::post('setTailorsAddToCart', 'API\ShoppingController@setTailorsAddToCart');
  Route::post('setDressAddToCart', 'API\ShoppingController@setDressAddToCart');
  Route::post('setAbayaAddToCart', 'API\ShoppingController@setAbayaAddToCart');
  Route::post('setOudAndPerfumesAddToCart', 'API\ShoppingController@setOudAndPerfumesAddToCart');
  Route::post('setGoldAndJewelryAddToCart', 'API\ShoppingController@setGoldAndJewelryAddToCart');
  Route::post('getShoppingAddToCartData', 'API\ShoppingController@getShoppingAddToCartData');
  Route::post('getDressAddToCartData', 'API\ShoppingController@getDressAddToCartData');
  Route::post('deleteShoppingData', 'API\ShoppingController@deleteShoppingData');
  Route::post('AddShippingCharge', 'API\ShoppingController@AddShippingCharge');
  /**************Checkout Controller**************/
  Route::post('getShippingProfileInfo', 'API\CheckoutController@getShippingProfileInfo');
  Route::post('editShippingProfile', 'API\CheckoutController@editShippingProfile');
  Route::post('eventCheckout', 'API\CheckoutController@eventCheckout');
  Route::post('getCountryVat', 'API\CheckoutController@getCountryVat');
  Route::post('getShippingDesc', 'API\CheckoutController@getShippingDesc');
  Route::post('requestCheckout', 'API\CheckoutController@requestCheckout');
  /**************My Account Controller**************/
  Route::post('getProfileInfo', 'API\MyAccountController@getProfileInfo');
  Route::post('getOccasionInfo', 'API\MyAccountController@getOccasionInfo');

    Route::post('getInsurenceInfo', 'API\MyAccountController@getInsurenceInfo');
  Route::post('getOccasionProduct', 'API\MyAccountController@getOccasionProduct');
  Route::post('getOccasionType', 'API\MyAccountController@getOccasionType');
  Route::post('getStudioInfo', 'API\MyAccountController@getStudioInfo');
  Route::post('uploadPhoto', 'API\MyAccountController@uploadPhoto');
  Route::post('deletestudio', 'API\MyAccountController@deletestudio');
  Route::post('getWalletInfo', 'API\MyAccountController@getWalletInfo');
  Route::post('addWalletAmount', 'API\MyAccountController@addWalletAmount');
  Route::post('getReviewInfo', 'API\MyAccountController@getReviewInfo');
  Route::post('addReview', 'API\MyAccountController@addReview');
  Route::post('getRequestaQuote', 'API\MyAccountController@getRequestaQuote');
  //Himanshu Return quote request
  Route::post('returnUserQuoteforMusic', 'API\MyAccountController@returnUserQuoteforMusic');
  Route::post('updateRequestaQuote', 'API\MyAccountController@updateRequestaQuote');
  Route::post('getInsuranceAmountProduct', 'API\MyAccountController@getInsuranceAmountProduct');
  Route::post('getInsuranceAmountProduct', 'API\MyAccountController@getInsuranceAmountProduct');
  Route::post('singerReview','API\MyAccountController@singerReview'); 
  /**************Order Product Controller**************/
  Route::post('getHallOrder', 'API\OrderProductController@getHallOrderData');
  Route::post('getFoodOrderData', 'API\OrderProductController@getFoodOrderData');
  Route::post('getMakeupOrderData', 'API\OrderProductController@getMakeupOrderData');
  Route::post('getClinicOrderData', 'API\OrderProductController@getClinicOrderData');
  Route::post('getMusicAcousticOrderData', 'API\OrderProductController@getMusicAcousticOrderData');
  Route::post('getCarRentalOrderData', 'API\OrderProductController@getCarRentalOrderData');
  Route::post('getTravelOrderData', 'API\OrderProductController@getTravelOrderData');
  Route::post('getShoppingOrderData', 'API\OrderProductController@getShoppingOrderData');
  Route::post('getOccasionOrderData', 'API\OrderProductController@getOccasionOrderData');
   Route::post('chkCouponcode', 'API\CheckoutController@chkCouponcode');
   Route::post('getCouponlist', 'API\CheckoutController@getCouponlist');
     Route::post('getShippingAmt', 'API\CheckoutController@getShippingAmt');
     Route::post('getInvitionlist', 'API\MyAccountController@getInvitionlist');
  });
   /******************** Wisitech Controller ***************/
  Route::post('getcountry', 'API\ApiController@getCountry');
  Route::post('getcity', 'API\ApiController@getCity');
  Route::post('merchantLogin', 'API\ApiController@merchantLogin');
  Route::post('currency', 'API\CheckoutController@Currency');

  Route::post('apisendContact', 'API\MyAccountController@apisendContact');

  Route::post('checkdressrent', 'API\ShoppingController@checkDressrent');
  Route::post('checkacosticsrent', 'API\RevivingConcertsController@checkAcosticsrent');
  Route::post('hashoccasion', 'API\ApiController@hashOccasion');
  Route::post('staticontent', 'API\ApiController@staticContent');
  Route::post('checkqty', 'API\ApiController@checkQty');
  Route::post('gedocbookedtime', 'API\ApiController@docBookedtime');



  Route::post('getelectroniccatgeory', 'API\ElectronicInvitationCategoryController@getElectronicCatgeory');
<?php
// Ajit Start
Route::get('my-account', 'MyAccountController@myaccount')->name('my-account');
Route::get('my-account-profile', 'MyAccountController@myaccountprofile')->name('my-account-profile');
Route::post('update-my-profile','MyAccountController@updatemyprofile')->name('update-my-profile');
Route::get('change-password', 'MyAccountController@changepassword')->name('change-password');
Route::post('change-password-update','MyAccountController@changepasswordupdate')->name('change-password-update');
Route::get('my-account-ocassion', 'MyAccountController@myaccountocassion')->name('my-account-ocassion');
Route::post('my-account-ocassion', 'MyAccountController@myaccountocassion')->name('my-account-ocassion');
Route::get('my-account-studio', 'MyAccountController@myaccountstudio')->name('my-account-studio');
Route::get('my-account-security', 'MyAccountController@myaccountsecurity')->name('my-account-security');
Route::get('my-account-wallet', 'MyAccountController@myaccountwallet')->name('my-account-wallet');
Route::get('my-account-review', 'MyAccountController@myaccountreview')->name('my-account-review');
Route::get('my-account-ocassion-detail', 'MyAccountController@myaccountocassiondetail')->name('my-account-ocassion-detail');
Route::post('insert_review','MyAccountController@insertreview')->name('insert_review');
Route::get('my-request-a-quote', 'MyAccountController@myrequestaquote')->name('my-request-a-quote');
Route::post('my-request-a-quote', 'MyAccountController@myrequestaquote')->name('my-request-a-quote');
Route::get('serviceorder/{cus_id}/{product_type}/{product_sub_type}/{order_id}/{category_id}/{product_id}/{merchant_id}/{shop_id}/', 'MyAccountController@serviceorder')->name('serviceorder');
//Route::get('my-account-ocassion-withajax', 'MyAccountController@myaccountocassionwithajax');
Route::get('invitation-attended-list/{id}','MyAccountController@invitationattendedlist')->name('invitation-attended-list');
Route::get('order-details/{id}','MyAccountController@orderDetails')->name('order-details');
Route::post('insert_studio','MyAccountController@insertstudio');
Route::get('delete-my-occasion-imagewithajax', 'MyAccountController@deletemyoccasionimagewithajax');
Route::get('delete-my-occasion-one-imagewithajax', 'MyAccountController@deletemyoccasiononeimagewithajax');
Route::get('ocassion-more-image/{id}','MyAccountController@ocassionmoreimage')->name('ocassion-more-image');
Route::get('change-pass-imagewithajax', 'MyAccountController@changepassimagewithajax');
//Reviving Concerts start
Route::get('revivingconcerts/{id}', 'SingerController@revivingconcerts')->name('revivingconcerts');
Route::get('singers-list/{id}/{sid}', 'SingerController@singerslist')->name('singers-list');
Route::get('popular-bands-list/{id}/{sid}', 'SingerController@popularbandslist')->name('popular-bands-list');
Route::get('acoustic-list/{id}/{sid}', 'SingerController@acousticlist')->name('acoustic-list');
Route::get('singerdetail/{id}/{sid}/{lid}/', 'SingerController@singerdetail')->name('singerdetail');
Route::get('popularbandsdetail/{id}/{sid}/{lid}/', 'SingerController@popularbandsdetail')->name('popularbandsdetail');
Route::get('acousticrecording/{id}/{sid}/{lid}/', 'SingerController@acousticrecording')->name('acousticrecording');
Route::get('acousticequipment/{id}/{sid}/{lid}/', 'SingerController@acousticequipment')->name('acousticequipment');
Route::post('insert_singer','SingerController@insertsinger');
Route::post('insert_acoustic','SingerController@insertacoustic');
Route::post('updatewalletdashboard','MyAccountController@updatewalletdashboard')->name('updatewalletdashboard');
Route::get('my-account-wallet-success','MyAccountController@myaccountwalletsuccess')->name('my-account-wallet-success');
Route::get('getproductdetail', 'SingerController@getproductdetail');
Route::post('addcartsingerproduct', 'SingerController@addcartsingerproduct');
Route::get('requestaquoteview/{id}','MyAccountController@requestaquoteview')->name('requestaquoteview');
Route::post('updaterequestaquoteview','MyAccountController@updaterequestaquoteview');
//Reviving Concerts End

//Travel Agency & Car Rental start
Route::get('travelagency/{id}', 'TravelAgencyController@travelagency')->name('travelagency');
Route::get('travelagencydetail/{id}/{sid}', 'TravelAgencyController@travelagencydetail')->name('travelagencydetail');
Route::get('getTravelAgencyInfo', 'TravelAgencyController@getTravelAgencyInfo');
Route::get('carrental/{id}', 'CarRentalController@carrental')->name('carrental');
Route::get('carrentaldetail/{id}/{sid}', 'CarRentalController@carrentaldetail')->name('carrentaldetail');
Route::get('reviewdisplay/{shopid}/{orderid}/{vendorid}', 'MyAccountController@reviewdisplay')->name('reviewdisplay');
Route::get('getCarRentalInfo', 'CarRentalController@getCarRentalInfo');
Route::post('addtocarttravelandcarproduct', 'TravelAgencyController@addtocarttravelandcarproduct');
//Travel Agency & Car Rental End

//Shopping start
Route::get('shoppingList/{id}','ShoppingController@shoppingList')->name('shopping');
Route::get('tailerslist/{id}/{sid}','ShoppingController@tailerslist')->name('tailerslist');
Route::get('tailersdetail/{id}/{sid}/{lid}','ShoppingController@tailersdetail')->name('tailersdetail');

Route::get('dresseslist/{id}/{sid}','ShoppingController@dresseslist')->name('dresseslist');
Route::get('dressesdetail/{id}/{sid}/{lid}','ShoppingController@dressesdetail')->name('dressesdetail');
Route::get('getdresssize', 'ShoppingController@getdresssize');




Route::get('perfumeslist/{id}/{sid}','ShoppingController@perfumeslist')->name('perfumeslist');
Route::get('perfumesbranchlist/{id}/{sid}/{shopid}','ShoppingController@perfumesbranchlist')->name('perfumesbranchlist');
Route::get('perfumesbranchdetail/{id}/{sid}/{shopid}/{bid}','ShoppingController@perfumesbranchdetail')->name('perfumesbranchdetail');

Route::get('abayalist/{id}/{sid}','ShoppingController@abayalist')->name('abayalist');
Route::get('abayareadymadedetail/{id}/{sid}/{lid}','ShoppingController@abayareadymadedetail')->name('abayareadymadedetail');
Route::get('abayatailersdetail/{id}/{sid}/{lid}','ShoppingController@abayatailersdetail')->name('abayatailersdetail');

Route::get('jewelerylist/{id}/{sid}','ShoppingController@jewelerylist')->name('jewelerylist');
Route::get('jewelerybranchlist/{id}/{sid}/{shopid}','ShoppingController@jewelerybranchlist')->name('jewelerybranchlist');
Route::get('jewelerybranchdetail/{id}/{sid}/{shopid}/{bid}','ShoppingController@jewelerybranchdetail')->name('jewelerybranchdetail');

Route::get('getShoppingProductInfo', 'ShoppingController@getShoppingProductInfo');
Route::get('getfabric', 'ShoppingController@getfabric');
Route::get('getShoppingProductInfoTailor', 'ShoppingController@getShoppingProductInfoTailor');
Route::post('addtocartforshopping', 'ShoppingController@addtocartforshopping');
Route::get('getSizeQuantity', 'ShoppingController@getSizeQuantity');
Route::get('checkQuantity', 'SingerController@checkQuantity');

Route::get('checksoldout', 'SingerController@checksoldout');
Route::get('getrentprice', 'ShoppingController@getrentprice');
Route::get('getbuyprice', 'ShoppingController@getbuyprice');
Route::get('getProductQuantity', 'ShoppingController@getProductQuantity');
Route::get('getShoppingProductInfoWithGold', 'ShoppingController@getShoppingProductInfoWithGold');
Route::post('addtocartofgoldAndperfume', 'ShoppingController@addtocartofgoldAndperfume');
Route::post('addtocartofdress', 'ShoppingController@addtocartofdress');
Route::get('getShoppingProductDress', 'ShoppingController@getShoppingProductDress');
Route::get('checkqtybysize', 'ShoppingController@checkqtybysize');

Route::get('checkbookedtime','ShoppingController@checkbookedtime');
//Shopping end

// Start Occasion Co-ordinator // 
Route::get('electronic-invitations/{id}/{sid}', 'FrontElectronicController@electronicShopdetails')->name('electronic-invitations');
Route::post('insert_electronic','FrontElectronicController@insert_electronic');
Route::post('checkfilentry','FrontElectronicController@checkfilentry')->name('checkfilentry');



Route::get('getAllDesignerCard','FrontElectronicController@getAllDesignerCard');
Route::get('getAllDesignerCardAllProduct','FrontElectronicController@getAllDesignerCardAllProduct');
Route::get('getsinglesroduct','FrontElectronicController@getSingleProduct');
// END Occasion Co-ordinator // 
